//
//  Reports.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 1/27/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class Reports: Stakeholder
{
    var relationsReport: [Stakeholder]
    
    override init(json: JSON)
    {
        if let relationsInReport = json["Relations"].array{
            self.relationsReport = relationsInReport.map({Stakeholder(json:$0)})
        }
        else
        {
            self.relationsReport = [Stakeholder]()
        }
        
        super.init(json: json)
    }
    
    override init(isDefault: Bool)
    {
        self.relationsReport = []
        
        super.init(isDefault: isDefault)
    }
    
    override func getWSObject() -> [String : Any]
    {
        var wsDict = super.getWSObject()
        wsDict += ["Relations": relationsReport.map({ $0.getWSObject() })]
        
        return wsDict
    }
}
