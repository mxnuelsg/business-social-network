//
//  MemberCountriey.swift
//  SHM
//
//  Created by Manuel Salinas on 10/13/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class MemberCountry: Entity
{
    var id: Int
    var ext: String
    var name: String
    
    
    init(json: JSON)
    {
        self.id = json["CountryId"].int ?? 0
        self.ext = json["Extension"].string ?? ""
        self.name = json["Name"].string ?? ""
    }
    
    func getWSObject() -> [String : Any]{
        return [
            "CountryId" : id,
            "Extension" : ext,
            "Name" : name
        ]
    }

}
