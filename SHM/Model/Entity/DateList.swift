//
//  DateList.swift
//  SHM
//
//  Created by Manuel Salinas on 9/15/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class DateList: Entity
{
    var date: Date
    var posts: [Post]
    var relevantDates = [RelevantDate]()
    
    //Custom properties
    var arrayEvents = [AnyObject]()
    
    override init()
    {
        date = Date()
        posts =  []
        relevantDates = []
    }
    
    
    init(json: JSON)
    {
        let dateString = json["Date"].string ?? ""
        date = dateString.toDate() ?? Date()
        
        /**Post*/
        if let post = json["Posts"].array
        {
            self.posts = post.map({ Post(json:$0) })
        }
        else
        {
            self.posts = [Post]()
        }
        
        /**Relevant Dates*/
        if let relevantDate = json["RelevantDates"].array
        {
            let arrayRelevants = relevantDate.map({ RelevantDate(json:$0) })
            
            for relevant in arrayRelevants
            {
                relevant.date = self.date 
                self.relevantDates.append(relevant)
            }
        }
        else
        {
            self.relevantDates = [RelevantDate]()
        }
        
        //Join Events
        for relevant in self.relevantDates
        {
            self.arrayEvents.append(relevant)
        }
        
        for post in self.posts
        {
             self.arrayEvents.append(post)
        }
    }
    
    func joinEvents()
    {
        self.arrayEvents.removeAll()
        
        //Join Events
        for relevant in self.relevantDates
        {
            self.arrayEvents.append(relevant)
        }
        
        for post in self.posts
        {
            self.arrayEvents.append(post)
        }
    }
    
    func getWSObject() -> [String : Any]{
        return [
            "Date" : date,
            "Posts" : posts.map({ $0.getWSObject() }),
            "RelevantDates" : relevantDates.map({ $0.getWSObject() })
        ]
    }
    
}
