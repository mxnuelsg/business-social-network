//
//  Request.swift
//  SHM
//
//  Created by Manuel Salinas on 6/2/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class Request: Entity
{
    var id: Int
    var stakeholderId: Int
    var actor: String
    var comments: String
    var publishComments: String
    var sendLinkUpdatedCard: Bool
    var sendCardAsAttachedFile: Bool
    var responsible: Member
    var stakeholderDetail: Stakeholder
    var requesters: [Member]
    var members: [Member]

    
    init(json: JSON)
    {
        self.id = json["RequestId"].int ?? 0
        self.stakeholderId = json["StakeholderId"].int ?? 0
        self.actor = json["Actor"].string ?? ""
        self.comments = json["Comments"].string ?? ""
        self.publishComments = json["PublishComments"].string ?? ""
        self.sendCardAsAttachedFile = json["SendCardAsAttachedFile"].boolValue
        self.sendLinkUpdatedCard = json["SendLinkUpdatedCard"].boolValue
        self.responsible = Member(json: json["Responsible"])
        self.stakeholderDetail = Stakeholder(json: json["StakeholderDetail"])
        
        //Members
        if let arrayAccounts = json["Accounts"].array {
            
            self.members = arrayAccounts.map({ Member(json:$0) })
        }
        else
        {
            self.members = [Member]()
        }

        
        //requesters
        if let arrayRequesters = json["Requesters"].array {
            
            self.requesters = arrayRequesters.map({ Member(json:$0) })
        }
        else
        {
            self.requesters = [Member]()
        }

        
    }
    
    override init()
    {
        self.id = 0
        self.stakeholderId = 0
        self.actor = String()
        self.comments = String()
        self.publishComments = String()
        self.sendCardAsAttachedFile = false
        self.sendLinkUpdatedCard = false
        self.responsible = Member(isDefault: true)
        self.stakeholderDetail = Stakeholder(isDefault: true)
        self.members = [Member]()
        self.requesters = [Member]()
        
        super.init()
    }
    
    func getWSObject() -> [String : Any]{
        return [
            "RequestId" : self.id,
            "StakeholderId" : self.stakeholderId,
            "Actor" : self.actor,
            "Comments" : self.comments,
            "publishComments" : self.publishComments,
            "SendCardAsAttachedFile" : self.sendCardAsAttachedFile == true ? "true" : "false",
            "SendLinkUpdatedCard" : self.sendLinkUpdatedCard == true ? "true" : "false",
            "Responsible" : self.responsible.getWSObject(),
            "StakeholderDetail" : self.stakeholderDetail.getWSObject(),
            "Accounts" : self.members.map({$0.getWSObject()}),
            "Requesters" : self.requesters.map({$0.getWSObject()})
        ]
    }
}
