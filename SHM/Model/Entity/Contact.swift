//
//  Contact.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/13/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import Foundation
import SwiftyJSON

class Contact: Entity
{
    var id: Int
    var name: String
    var lastName: String
    var username: String
    var jobPosition: KeyValueObject?
    var companyName: String
    var postsCount: Int
    var thumbnailUrl: String
    var birthday: Date?
    var occupation: KeyValueObject?
    var modifiedDate: Date?
    var relationsLevel1: [KeyValueObjectWithKeyValueArray]
    var isStakeholder: Bool
    var relationType1: KeyValueObject?
    var relationType2: KeyValueObject?

    
    var fullName: String {
        
        get {
            return UserDefaultsManager.getContactOrder() == true ? name + " " + lastName : lastName + " " + name
        }
    }
    
    init(isDefault: Bool)
    {
        self.id = 0
        self.name = String()
        self.lastName = String()
        self.username = String()
        self.thumbnailUrl = String()
        self.jobPosition = KeyValueObject()
        self.occupation = KeyValueObject()
        self.companyName = String()
        self.postsCount = 0
        self.relationsLevel1 = [KeyValueObjectWithKeyValueArray]()
        self.isStakeholder = false
        self.relationType1 = KeyValueObject()
        self.relationType2 = KeyValueObject()
    }
    
    init(json: JSON)
    {
        self.id = json["StakeholderId"].int ?? json["AccountId"].int ?? 0
        self.username = json["UserName"].string ?? json["Username"].string ?? ""
        self.name = (json["FirstName"].string ?? "").trim()
        self.lastName = (json["LastName"].string ?? "").trim()
        self.jobPosition = KeyValueObject(json: json["Position"])
        self.companyName = json["Company"].string ?? ""
        self.postsCount = json["postsCount"].int ?? 0
        self.occupation = KeyValueObject(json: json["Occupation"])
        self.isStakeholder = json["IsStakeholder"].boolValue
        self.relationType1 = KeyValueObject(json: json["RelationType1"])
        self.relationType2 = KeyValueObject(json: json["RelationType2"])

        
        let dateStringBirthday = json["Birthdate"].string ?? ""
        self.birthday = (dateStringBirthday.isEmpty) ? nil : dateStringBirthday.toDate()
        
        
        let dateStringModifiedDate = json["ModifiedDate"].string ?? ""
        self.modifiedDate = (dateStringModifiedDate.isEmpty) ? nil : dateStringModifiedDate.toDate()
        
        if let thumbnailUrl = json["ThumbnailUrl"].string {
            
//            if let apiRange = thumbnailUrl.rangeOfString("api/") {
//                
//                self.thumbnailUrl = ERMServer.info().defaultServer + thumbnailUrl.substringFromIndex(apiRange.startIndex)
//            }
//            else
//            {
//                self.thumbnailUrl = thumbnailUrl
//            }
            
            self.thumbnailUrl = thumbnailUrl
        }
        else
        {
            self.thumbnailUrl = String()
        }
        
        //Relations level 1
        if let catalogRelations = json["RelationLevel1"].array {
            
            self.relationsLevel1 = catalogRelations.map({KeyValueObjectWithKeyValueArray(json: $0)})
        }
        else
        {
            self.relationsLevel1 = [KeyValueObjectWithKeyValueArray]()
        }
    }
    
    func getWSObject() -> [String : Any] {
        return [
            "StakeholderId" : self.id,
            "FirstName" : self.name,
            "UserName" : self.username,
             "LastName" : self.lastName,
            "JobPosition" : self.jobPosition?.getWSObject() ?? "null",
            "Company" : self.companyName,
            "ThumbnailUrl" : self.thumbnailUrl,
            "Birthdate" : self.birthday?.convertToRequestString() ?? "",
            "Occupation" : self.occupation?.getWSObject() ?? "null",
            "IsStakeholder" : self.isStakeholder ? "true" : "false",
            "RelationType1" : self.relationType1?.getWSObject() ?? "null",
            "RelationType2" : self.relationType2?.getWSObject() ?? "null"
        ]
    }
}
