//
//  Date.swift
//  SHM
//
//  Created by Manuel Salinas on 9/9/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class Event: Entity
{
    var id: Int
    var date: Date
    var allDay: Bool = false
    
    init(json: JSON)
    {
        self.id = json["DateId"].int ?? 0
        
        let dateString = json["Date"].string ?? ""
        date = dateString.toDate()!
        
        if let allDay = json["AllDay"].int {
            self.allDay = Bool(allDay as NSNumber)
        }
    }
    
    override init()
    {
        id = 0
        date = Date()
        allDay = false
    }
    
    func getWSObject() -> [String : Any]{
        return [
            "DateId" : id,
            "Date" : date.convertToRequestString(),
            "AllDay" : Int(NSNumber(value: allDay))
        ]
    }
}
