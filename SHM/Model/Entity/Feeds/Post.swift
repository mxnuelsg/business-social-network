//
//  Feeds.swift
//  SHM
//
//  Created by Manuel Salinas on 8/6/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class Post: Entity
{
    var id: Int
    var title: String
    var modifiedDate: Date?
    var createdDate: Date?
    var isPrivate: Bool
    var isFiled: Bool
    var stakeholders: [Stakeholder]
    var attachments: [Attachment]
    var events: [Event]
    var projects: [Project]
    var author: Member
    var totalAttachments: Int
    var modifiedDateString: String
    var createdDateString: String
    var geographies: [ClusterItem]
    init(json: JSON)
    {
        self.id = json["PostId"].int ?? 0
        self.title = json["Body"].string ?? ""
        self.isPrivate = json["IsPrivate"].boolValue
        self.isFiled = json["Filed"].boolValue
        self.totalAttachments = json["TotalAttachments"].int ?? 0
        
        /**Author*/
        self.author = Member(isDefault: true)
        self.author.id = json["AccountId"].int ?? 0
        self.author.name = json["FirstName"].string ?? ""
        self.author.lastName = json["LastName"].string ?? ""
        self.author.thumbnailUrl = json["ThumbnailUrl"].string ?? ""
        self.author.jobPosition = KeyValueObject(json: json["Position"])
        
        /**Dates*/
        modifiedDateString = json["ModifiedDate"].string ?? ""
        modifiedDate = (modifiedDateString.isEmpty) ? nil : modifiedDateString.toDate()
        
        createdDateString = json["CreatedDate"].string ?? ""
        createdDate = (createdDateString.isEmpty) ? nil : createdDateString.toDate()
        
        /**Stakeholders*/
        if let stakeholder = json["Stakeholders"].array
        {
            // The expression means that matches all the id from the title in the order who whas tagged
            let regex = try! NSRegularExpression(pattern: "([s])=(\\d.....)", options:NSRegularExpression.Options.caseInsensitive)
            let matches = regex.matches(in: self.title, options: [], range: NSMakeRange(0, self.title.utf16.count))
            let stakeholderArray = stakeholder.map({ Stakeholder(json:$0) })
            self.stakeholders = [Stakeholder]()
            
            for match in matches as [NSTextCheckingResult]
            {
                let id = (self.title as NSString).substring(with: match.range)
                
                if let idInt = Int(id.replacingOccurrences(of: "s=", with: ""))
                {
                    for sh in stakeholderArray where sh.id == idInt
                    {
                        self.stakeholders.append(sh)
                    }
                }
            }
        }
        else
        {
            self.stakeholders = [Stakeholder]()
        }
        
        /**Files*/
        if let files = json["Attachments"].array
        {
            self.attachments = files.map({ Attachment(json:$0) })
        }
        else
        {
            self.attachments = [Attachment]()
        }
        
        /**Events*/
        if let events = json["Events"].array
        {
            self.events = events.map({ Event(json:$0) })
        }
        else
        {
            self.events = [Event]()
        }
        
        /**Projects*/
        if let project = json["Projects"].array
        {
            self.projects = project.map({ Project(json:$0) })
        }
        else
        {
            self.projects = [Project]()
        }
        
        //Geographies
        if let geoItems = json["Geographies"].array {
            
            self.geographies = geoItems.map({ClusterItem(json:$0)})
        }
        else
        {
            self.geographies = [ClusterItem]()
        }
    }
    
    override init()
    {
        self.id = -1
        self.title = String()
        self.totalAttachments = 0
        self.modifiedDate = Date()
        self.createdDate = Date()
        self.stakeholders = [Stakeholder]()
        self.attachments = [Attachment]()
        self.events = [Event]()
        self.projects = [Project]()
        self.author = Member(isDefault: true)
        self.modifiedDateString = String()
        self.createdDateString = String()
        self.isPrivate = false
        self.isFiled = false
        self.geographies = [ClusterItem]()
    }
    
    func getWSObject() -> [String : Any]{
        return [
            "PostId":id,
            "Body": title,
            "TotalAttachments": totalAttachments,
            "IsPrivate": isPrivate,
            "Filed": isFiled,
            "ModifiedDate": modifiedDateString,
            "CreatedDate": createdDateString,
            "AccountId":author.id,
            "FirstName":author.name,
            "LastName":author.lastName ,
            "ThumbnailUrl":author.thumbnailUrl ,
            "JobTitle":author.jobPosition?.value ?? "",
            "Events":events.map({ $0.getWSObject() }),
            "Attachments":attachments.map({ $0.getWSObject() }),
            "Stakeholders":stakeholders.map({ $0.getWSObject() }),
            "Projects": projects.map({ $0.getWSObject() })
        ]
    }
}

