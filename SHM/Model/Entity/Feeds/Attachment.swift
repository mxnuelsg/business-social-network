//
//  File.swift
//  SHM
//
//  Created by Manuel Salinas on 8/6/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class Attachment: Entity
{
    var id: Int
    var file: String
    var name: String
    var type: String
    var token: String
    var localUrl: URL?
   
    
    init(json: JSON)
    {
        if let file = json["FileName"].string {
            
            if let apiRange = file.range(of: "api/") {
                
                if let fileName = file.substring(from: apiRange.lowerBound).addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: "+=\n").inverted) {
                    
                    self.file = ERMServer.info().defaultServer + fileName
                }
                else
                {
                    self.file = file
                }
            }
            else
            {
                self.file = file
            }
        }
        else
        {
            self.file = ""
        }

        self.id = json["AttachmentId"].int ?? 0
        self.name = json["Title"].string ?? ""
        self.type = json["Extension"].string ?? ""
        self.token = json["Token"].string ?? ""
    }
    
    override init()
    {
        self.id = 0
        self.file = String()
        self.name = String()
        self.type = String()
        self.token = String()
        self.localUrl = nil
    }
    
    func getWSObject() -> [String : Any]{
        return [
            "AttachmentId" : id,
            "FileName" : file,
            "Title" : name,
            "Extension" : type,
            "Token" : token
        ]
    }
}
