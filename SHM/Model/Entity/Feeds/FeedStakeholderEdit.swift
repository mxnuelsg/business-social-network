//
//  FeedStakeholderEdit.swift
//  SHM
//
//  Created by Manuel Salinas on 5/20/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class FeedStakeholderEdit: Entity
{
    var events: [Event]
    var posts: [Post]
    var hasMoreFeeds: Bool
    
    init(json: JSON)
    {
        self.hasMoreFeeds = json["HasMoreFeeds"].boolValue
        
        //Post
        if let Posts = json["Feeds"].array {
            
            self.posts = Posts.map({ Post(json:$0) })
        }
        else
        {
            self.posts = [Post]()
        }
        //Events
        if let Events = json["Events"].array
        {
            self.events = Events.map({ Event(json:$0) })
        }
        else
        {
            self.events = [Event]()
        }
    }
    
    override init()
    {
        self.events = [Event]()
        self.posts = [Post]()
        self.hasMoreFeeds = false
    }
}
