//
//  KeyValueObject.swift
//  SHM
//
//  Created by Manuel Salinas on 9/28/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class KeyValueObject: Entity
{
    var key: Int
    var valueEn: String
    var valueEs: String
    
    //Calculated var
    var value: String {
        get
        {
            //Override value for value if i't's necessay
            let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
            
            if language == "es"
            {
                return self.valueEs
            }
            else
            {
                return self.valueEn
            }
        }
        
        set(newValue)
        {
            //Override value for value if i't's necessay
            let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
            
            if language == "es"
            {
                self.valueEs = newValue
            }
            else
            {
                self.valueEn = newValue
            }
        }
    }
    
    init(json: JSON)
    {
        self.key = json["Key"].int ?? 0
        self.valueEn = json["Value"].string ?? ""
        self.valueEs = json["ValueEs"].string ?? ""
    }
    
    override init()
    {
        self.key =  0
        self.valueEn = ""
        self.valueEs = ""
    }
    
    func getWSObject() -> [String : Any]{
        return [
            "Key" : key,
            "Value" : valueEn,
            "ValueEs" : valueEs
        ]
    }
}

extension Sequence where Iterator.Element == KeyValueObject
{
    func collapseValues() -> String
    {
        var collapsedString = ""
        let values = self.map({ $0.value })
        collapsedString += values.joined(separator: ", ")
        
        return collapsedString
    }
    
    func collapseValuesEs() -> String
    {
        var collapsedString = ""
        let values = self.map({ $0.valueEs })
        collapsedString += values.joined(separator: ", ")
        
        return collapsedString
    }
}

class StringKeyValueObject: Entity
{
    var key: String
    var value: String
    var language: String
    
    init(json: JSON)
    {
        self.key = json["Key"].string ?? ""
        self.value = json["Value"].string ?? ""
        self.language = json["Language"].string ?? ""
    }
    
    override init()
    {
        self.key = String()
        self.value = String()
        self.language  = String()
    }
    
    func getWSObject() -> [String : Any]{
        return [
            "Key" : self.key,
            "Value" : self.value,
            "Language" : self.language
        ]
    }
}



extension Sequence where Iterator.Element == StringKeyValueObject {
    func collapseValues() -> String {
        var collapsedString = ""
        let values = self.map({ $0.value })
        collapsedString += values.joined(separator: ", ")
        
        return collapsedString
    }
}





//New class for key valye objects in RelationLevel1 
class KeyValueObjectWithKeyValueArray: Entity
{
    var key: Int
    var valueEn: String
    var valueEs: String
    var keyValues: [KeyValueObject]
    
    //Calculated var
    var value: String {
        
        get
        {
            //Override value for value if i't's necessay
            let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
            
            if language == "es"
            {
                return self.valueEs
            }
            else
            {
                return self.valueEn
            }
        }
        
        set
        {
            //Override value for value if i't's necessay
            let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
            
            if language == "es"
            {
                self.valueEs = self.value
            }
            else
            {
                self.valueEn = self.value
            }
        }
    }

    
    init(json: JSON)
    {
        self.key = json["Key"].int ?? 0
        self.valueEn = json["Value"].string ?? ""
        self.valueEs = json["ValueEs"].string ?? ""
        
        if let KV = json["KeyValues"].array {
            
            self.keyValues = KV.map({ KeyValueObject(json: $0) })
        }
        else
        {
            self.keyValues = []
        }
    }
    
    override init()
    {
        self.key =  0
        self.valueEn = ""
        self.valueEs = ""
        self.keyValues = [KeyValueObject]()
    }
    
    func getWSObject() -> [String : Any]{
        return [
            "Key":key,
            "Value":valueEn,
            "ValueEs": valueEs,
            "KeyValues": keyValues.map({$0.getWSObject()})
        ]
    }
}


