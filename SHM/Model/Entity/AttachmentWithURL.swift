//
//  AttachmentWithURL.swift
//  SHM
//
//  Created by Manuel Salinas on 10/20/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

///This class is specifically created for Upload Image
class AttachmentWithToken: Entity
{
    var picture: UIImage!
    var file : String
    var token: String
    var isRemote: Bool
    
    override init()
    {
        self.picture = nil
        self.file = String()
        self.token = String()
        self.isRemote = false
    }
    
    init(token: String, image:UIImage)
    {
        self.picture = image
        self.token = token
        
        self.file = String()
        self.isRemote = false
    }
}
