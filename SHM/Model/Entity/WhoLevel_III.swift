//
//  WhoLevel_III.swift
//  SHM
//
//  Created by Manuel Salinas on 10/4/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class WhoLevel_III: WhoLevels
{
    var grandParentId: Int
    
    override init(json: JSON)
    {
        self.grandParentId = json["GParentId"].int ?? 0
        
        super.init(json: json)
    }
    
    override init()
    {
        self.grandParentId = 0
        
        super.init()
    }
    
    override func getWSObject() -> [String : Any] {
        var wsDict = super.getWSObject()
        wsDict +=  [
            "GParentId" : self.grandParentId
        ]
        
        return wsDict
    }
}
