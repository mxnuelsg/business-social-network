//
//  StakeholderReference.swift
//  SHM
//
//  Created by Manuel Salinas on 3/1/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class StakeholderReference: Entity
{
    var ordinalIndicator: Int
    var isPrivate: Bool
    var value: String
    var language: String
    
    init(json: JSON)
    {
        self.ordinalIndicator = json["OrdinalIndicator"].int ?? 0
        self.value = json["Value"].string ?? String()
        self.isPrivate = json["IsPrivate"].boolValue
        self.language = json["Language"].string ?? String()
    }
    
    override init()
    {
        self.ordinalIndicator = 0
        self.value = String()
        self.isPrivate = false
        self.language = String()
    }
    
    func getWSObject() -> [String : Any]{
        return [
            "OrdinalIndicator" : self.ordinalIndicator,
            "Value" : self.value,
            "IsPrivate" : self.isPrivate,
            "Language" : self.language
        ]
    }
    
}
