//
//  Hashtag.swift
//  SHM
//
//  Created by Manuel Salinas on 8/18/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class Hashtag: Entity
{
    var id: Int
    var title: String
   
    init(json: JSON)
    {
        self.id = json["id"].int ?? 0
        self.title = json["Title"].string ?? ""
    }
    
    func getWSObject() -> [String : Any]{
        return [
            "id" : id,
            "Title" : title
        ]
    }

}
