//
//  Paging.swift
//  SHM
//
//  Created by Manuel Salinas on 10/5/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class Paging: Entity
{
    var pageSize: Int = 20
    var pageNumber: Int = 1
    var orderBy: String
    var descending: Bool = false
    var pagination: Bool = false
    var totalPages: Int = 0
    var totalItems: Int = 0 {
        didSet{
        
            let result: Double = Double(self.totalItems) / Double(self.pageSize)
            let elements = "\(result)".components(separatedBy: ".")
            
            if elements.count ==  2
            {
                let entire: Int = Int(elements[0])!
                let decimal: Int = Int(elements[1])!
                
                if decimal > 0
                {
                    self.totalPages = entire + 1
                }
                else
                {
                     self.totalPages = entire
                }
            }
            else
            {
                self.totalPages = Int(result)
            }
        }
    }
    
    override init()
    {
        self.pageSize = 20
        self.pageNumber = 1
        self.orderBy = ""
        self.descending = false
        self.pagination = false
    }

    init(json: JSON)
    {
        self.pageSize = json["PageSize"].int ?? 0
        self.pageNumber = json["PageNumber"].int ?? 0
        self.orderBy = json["OrderBy"].string ?? ""
        
        if let isDescending = json["Descending"].int {
            
            self.descending = Bool(isDescending as NSNumber)
        }
        
        if let isPaginable = json["Pagination"].int {
            
            self.pagination = Bool(isPaginable as NSNumber)
        }
        
        if let totalItems = json["TotalItems"].int {
            
            self.totalItems = totalItems
        }
    }
    
    func getWSObject() -> [String : Any]{
        return [
            "PageSize" : pageSize,
            "PageNumber" : pageNumber,
            "OrderBy" : orderBy,
            "Descending" : Int(NSNumber(value: descending)),
            "Pagination" : Int(NSNumber(value: pagination))
        ]
    }
    
    func getWSString() -> String {
        let descendingString = descending ? "true" : "false"
        let paginationString = pagination ? "true" : "false"
        var wsString = "{"
        wsString += "\"PageSize\":\(pageSize),"
        wsString += "\"PageNumber\":\(pageNumber),"
        wsString += "\"OrderBy\":\"\(orderBy)\","
        wsString += "\"Descending\":\(descendingString),"
        wsString += "\"Pagination\":\(paginationString)"
        wsString += "}"
        print(wsString)
        return wsString
    }
}
