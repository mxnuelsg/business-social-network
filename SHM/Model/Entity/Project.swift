//
//  Project.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 10/08/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import Foundation
import SwiftyJSON

class Project: Entity, NSCopying
{
    var id: Int
    var title: String
    var modifiedDate: Date?
    var createdDate: Date?
    var stakeholders: [Stakeholder]
    var files: [Attachment]
    var author: Member?
    var coordinator: Member
    var resume: String
    var progress: Int
    var totalAttachments: Int
    var members: [Member]
    var posts: [Post]
    var followingStatus: Bool = false
    var isDetail:Bool = true
    var site: Site?
    var mitigationMeasure: MitigationMeasure?
    var isEnable: Bool
    var geographies:[ClusterItem]
    
    //custom variables
    var modifiedDateString: String
    var createdDateString: String
    
    init(json: JSON)
    {
        self.id = json["ProjectId"].int ?? 0
        self.title = json["Title"].string ?? ""
        self.title = title.trim()
        
        //Date
        self.modifiedDateString = json["ModifiedDate"].string ?? ""
        self.modifiedDate = (modifiedDateString.isEmpty) ? nil : modifiedDateString.toDate()
        
        self.createdDateString = json["CreatedDate"].string ?? ""
        self.createdDate = (createdDateString.isEmpty) ? nil : createdDateString.toDate()

        self.author = Member(json:json["Modified"])
        self.coordinator = Member(json: json["Coordinator"])
        self.resume = json["Description"].string ?? ""
        self.progress = json["Progress"].int ?? 0
        self.totalAttachments = json["TotalAttachments"].int ?? 0
        self.site = Site(json: json["SiteMapping"])
        self.isEnable = json["Enabled"].boolValue
        
        self.mitigationMeasure = MitigationMeasure(json: json["MitigationMeasure"])
        
        if let following = json["IsFollowed"].int {
            followingStatus = Bool(NSNumber(value: following))
        }
        if let isInDetail = json["IsDetail"].int {
            isDetail = Bool(NSNumber(value: isInDetail))
        }
        /**StakeHolders*/
        if let stakeholders = json["Stakeholders"].array {
            
            self.stakeholders = stakeholders.map({ Stakeholder(json:$0) })
        }
        else
        {
            self.stakeholders = [Stakeholder]()
        }
        
        /**Files*/
        if let files = json["Attachments"].array {
            
            self.files = files.map({ Attachment(json:$0) })
        }
        else
        {
            self.files = [Attachment]()
        }
        
        /**Members*/
        if let members = json["Members"].array {
            
            self.members = members.map({ Member(json:$0) })
        }
        else
        {
            self.members = [Member]()
        }
        
        /**Posts*/
        if let posts = json["Posts"].array {
            
            self.posts = posts.map({ Post(json:$0) })
        }
        else
        {
            self.posts = [Post]()
        }
        
        if let geoItems = json["Geographies"].array {
            
            self.geographies = geoItems.map( {
                ClusterItem(json:$0)
            })
        }
        else
        {
            self.geographies = [ClusterItem]()
        }
    }
    
    init(isDefault: Bool)
    {
        self.id = -1
        self.title = ""
        self.resume = ""
        self.modifiedDate = nil
        self.modifiedDateString = ""
        self.createdDate = nil
        self.createdDateString = ""
        self.totalAttachments = 0
        self.stakeholders = []
        self.files = []
        self.progress = 0
        self.members = []
        self.posts = []
        self.followingStatus = false
        self.isDetail = true
        self.author = Member(isDefault: true)
        self.coordinator = Member(isDefault: true)
        self.site = Site()
        self.mitigationMeasure = MitigationMeasure()
        self.isEnable = false
        self.geographies = [ClusterItem]()
    }
    
    func getWSObject() -> [String : Any] {
        return [
            "ProjectId":id,
            "Title":title,
            "Description": resume,
            "ModifiedDate": modifiedDateString,
            "CreatedDate": createdDateString,
            "Progress": progress,
            "TotalAttachments" : totalAttachments,
            "Enabled": self.isEnable ? "true" : "false",
            "IsFollowed" : Int(followingStatus as NSNumber),
            "isDetail":isDetail,
            "Members": members.map({ $0.getWSObject() }),
            "Attachments": files.map({ $0.getWSObject() }),
            "StakeHolders": stakeholders.map({ $0.getWSObject() }),
            "Coordinator" : coordinator.getWSObject(),
            "Posts": posts.map({ $0.getWSObject() }),
            "SiteMapping": self.site?.getWSObject() ?? "null",
            "MitigationMeasure": self.mitigationMeasure?.getWSObject() ?? "null",
            "Geographies": geographies.map({ $0.getWSObject() }),
        ]
    }
    
    func copy(with zone: NSZone?) -> Any {
       
        let copy = Project(isDefault: true)
        copy.id = self.id
        copy.title = self.title
        copy.modifiedDate = self.modifiedDate
        copy.createdDate = self.createdDate
        copy.stakeholders = self.stakeholders
        copy.files = self.files
        copy.author = self.author
        copy.coordinator = self.coordinator
        copy.resume = self.resume
        copy.totalAttachments = self.totalAttachments
        copy.isEnable = self.isEnable
        copy.progress = self.progress
        copy.members = self.members
        copy.posts = self.posts
        copy.followingStatus = self.followingStatus
        copy.isDetail = self.isDetail
        copy.modifiedDateString = self.modifiedDateString
        copy.createdDateString = self.createdDateString
        copy.geographies = self.geographies
        
        return copy
    }
}
