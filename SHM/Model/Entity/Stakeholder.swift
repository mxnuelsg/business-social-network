//
//  Stakeholder.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/4/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class Stakeholder: Contact
{
    var placeOfBirth: String
    var coordinator: Member?
    var representative: Member?
    
    var summaryPublic: String
    var personalInformationPublic: String
    var professionalPublic: String
    var generalInformationPublic: String
    
    var summaryPrivate: String
    var personalInformationPrivate: String
    var professionalPrivate: String
    var generalInformationPrivate: String
    
    var summaryPublicEsp: String
    var personalInformationPublicEsp: String
    var professionalPublicEsp: String
    var generalInformationPublicEsp: String
    
    var summaryPrivateEsp: String
    var personalInformationPrivateEsp: String
    var professionalPrivateEsp: String
    var generalInformationPrivateEsp: String
    var isCompany: Bool
    var isEnable: Bool
    var isVisiblePrivateFields: Bool
    
    var charactersEng: Int
    var charactersEsp: Int
    
    var additionalFields = [StringKeyValueObject]()
    
    var relevantDates = [RelevantDate]()
    var recurrentDates = [RelevantDate]()
    var projects: [Project]
    var relations: [ContactRelation]
    var alias = [String]()
    var aliasString = String()
    var isFollow: FollowStatus = .notFollowing
    var IsAnonymous: Bool = false
    var country: KeyValueObject?
    var maritalStatus: KeyValueObject?
    var industrySector: KeyValueObject
    var nationalities = [KeyValueObject]()
    var references = [StakeholderReference]()
    var referencesEsp = [StakeholderReference]()
    
    var phone: String
    var email: String
    var age: Int?
    var dueDate: Date?
    var comments: String
    var sendRequest: Bool
    
    //SHM worldwide
    var site: Site?
    var sites = [Site]()
    var canEdit: Bool
    var whosSiteId = -1
    //relations stakeholder
    var relationshipType1_id: Int?
    var relationshipType1_name: String?
    var relationshipType1_namEs: String?
    var relationshipType2_id: Int?
    var relationshipType2_name: String?
    var relationshipType2_namEs: String?
    var levelId: Int?
    var accountId: Int?
    
    //Member property (Just Conexion Report use this)
    var jobTitle: String
    var PhoneNumber: String = String()
    
    //Edit
    var canEditPrivate: Bool
    var totalPosts: Int
    var totalAttachments: Int
    var versionId: Int
    var requestId: Int
    var versionNumber: Int
    var createdDate: Date?
    var posts: [Post]
    var members: [Member]
    var events: [Event]
    var countries: [KeyValueObject]
    var maritalStatuses: [KeyValueObject]
    var businessTypes: [KeyValueObject]
    var attachments: [Attachment]
    var specialCharacters: [StringKeyValueObject]
    var professionalTimeline: [RelevantDate]
    
    //Integration
    var whosId: [Int]
    var whos: [WhoLevel_I]
    var whosLevel2: [WhoLevel_II]

    //Geography
    var geographies:[ClusterItem]
    
    //Rating
    var userRating: Float?
    var adminRating: Float?
    
    //BirthDate free text
    var birthDateString: String?
    
    override init(json: JSON)
    {
        self.isCompany = json["IsCompany"].boolValue
        self.isVisiblePrivateFields = json["ViewPrivateFields"].boolValue
        self.isEnable = json["Enable"].boolValue
        self.coordinator = Member(json: json["Coordinator"])
        self.representative = Member(json: json["Representative"])
        self.IsAnonymous = json["IsAnonymous"].bool ?? false
        self.summaryPublic = json["ResumePublic"].string ?? ""
        self.personalInformationPublic = json["PersonalInformationPublic"].string ?? ""
        self.professionalPublic = json["ProfessionalPublic"].string ?? ""
        self.generalInformationPublic = json["GeneralInformationPublic"].string ?? ""
        
        self.summaryPrivate = json["ResumePrivate"].string ?? ""
        self.personalInformationPrivate = json["PersonalInformationPrivate"].string ?? ""
        self.professionalPrivate = json["ProfessionalPrivate"].string ?? ""
        self.generalInformationPrivate = json["GeneralInformationPrivate"].string ?? ""
        
        self.summaryPublicEsp = json["ResumePublicES"].string ?? ""
        self.personalInformationPublicEsp = json["PersonalInformationPublicES"].string ?? ""
        self.professionalPublicEsp = json["ProfessionalPublicES"].string ?? ""
        self.generalInformationPublicEsp = json["GeneralInformationPublicES"].string ?? ""
        
        self.summaryPrivateEsp = json["ResumePrivateES"].string ?? ""
        self.personalInformationPrivateEsp = json["PersonalInformationPrivateES"].string ?? ""
        self.professionalPrivateEsp = json["ProfessionalPrivateES"].string ?? ""
        self.generalInformationPrivateEsp = json["GeneralInformationPrivateES"].string ?? ""
        
        self.charactersEng = json["EnglishCharacters"].int ?? 0
        self.charactersEsp = json["SpanishCharacters"].int ?? 0
        
        self.maritalStatus = KeyValueObject(json: json["MaritalStatus"])
        self.country =   KeyValueObject(json: json["Country"])
        self.industrySector = KeyValueObject(json: json["Position"])
        self.placeOfBirth = json["BirthPlace"].string ?? String()
        self.phone = json["Phone"].string ?? ""
        self.email = json["Email"].string ?? ""
        
        self.relationshipType1_id = json["RelationshipType1"].int ?? 0
        self.relationshipType1_name = json["RelationshipType1Name"].string ?? ""
        self.relationshipType1_namEs = json["RelationshipType1NameEs"].string ?? ""
        self.relationshipType2_id = json["RelationshipType2"].int ?? 0
        self.relationshipType2_name = json["RelationshipType2Name"].string ?? ""
        self.relationshipType2_namEs = json["RelationshipType2NameEs"].string ?? ""
        self.levelId = json["LevelId"].int ?? 0
        self.accountId = json["AccountId"].int ?? 0
        self.age = json["Age"].int ?? 0
        self.jobTitle = json["JobTitle"].string ?? ""
        self.site = Site(json: json["SiteMapping"])
        self.whosSiteId = json["WhosSiteId"].int ?? -1
        self.comments = json["Comments"].string ?? ""
        self.sendRequest = json["SendRequest"].boolValue
        
        if let siteArray = json["SiteMappings"].array {
            
            self.sites = siteArray.map({Site(json:$0)})
        }
        if let siteArray = json["WhosSites"].array {
            
            self.sites = siteArray.map({Site(json:$0)})
        }

        
        self.canEdit = json["CanEdit"].boolValue
        
        if let jsonExtraFields = json["AdditionalFields"].array {
            
            self.additionalFields = jsonExtraFields.map({ StringKeyValueObject(json: $0) })
        }
        
        if let jsonRelevantDates = json["ImportantsDate"].array {
            
            self.relevantDates = jsonRelevantDates.map({ RelevantDate(json: $0) })
        }
        
        if let jsonRecurrentDates = json["RecurrentDates"].array {
            
            self.recurrentDates = jsonRecurrentDates.map({ RelevantDate(json: $0) })
        }
        
        if let jsonProjects = json["CurrentProjects"].array {
            
            self.projects = jsonProjects.map({ Project(json: $0) })
        }
        else
        {
             self.projects = [Project]()
        }
        
        if let jsonRelations = json["Relations"].array {
            
            self.relations = jsonRelations.map({ ContactRelation(json: $0) })
        }
        else
        {
            self.relations = [ContactRelation]()
        }
        
        if let isFollow = json["IsFollow"].bool {
            
            self.isFollow = isFollow == true ? FollowStatus.following : FollowStatus.notFollowing
        }
        
        if let jsonNationalities = json["Nationalities"].array {
            
            self.nationalities =  jsonNationalities.map({ KeyValueObject(json: $0) })
        }
        
        if let references = json["References"].array {
            
            let ref = references.map({ StakeholderReference(json: $0) })
            
            self.references = ref.filter({ $0.language.lowercased() == "en"})
            self.referencesEsp = ref.filter({ $0.language.lowercased() == "es"})
        }
        
        if let allAlias = json["Alias"].array {
            
            self.alias = allAlias.map({String(describing: $0)})
            
            //String alias
            if self.alias.count > 0
            {
                self.aliasString = self.alias.collapseWithCommas()
            }
        }
        
        let dateStringDueDate = json["DueDate"].string ?? ""
        self.dueDate = (dateStringDueDate.isEmpty) ? nil : dateStringDueDate.toDate()
        
        /* * * EDIT SH * * */
        self.canEditPrivate = json["CanEditPrivate"].boolValue
        self.totalPosts = json["PostsTotal"].int ?? 0
        self.totalAttachments = json["TotalAttachments"].int ?? 0
        self.versionId = json["VersionId"].int ?? 0
        self.requestId = json["RequestId"].int ?? 0
        self.versionNumber = json["VersionNumber"].int ?? 0
        
        //Date
        let dateStringCreatedDate = json["CreatedDate"].string ?? ""
        self.createdDate = (dateStringCreatedDate.isEmpty) ? nil : dateStringCreatedDate.toDate()
        
        //Post
        if let Posts = json["Posts"].array {
            
            self.posts = Posts.map({ Post(json:$0) })
        }
        else
        {
            self.posts = []
        }
        
        //Members
        if let Members = json["Members"].array {
            
            self.members = Members.map({ Member(json: $0) })
        }
        else
        {
            self.members = []
        }
        
        //Events
        if let Events = json["Events"].array
        {
            self.events = Events.map({ Event(json:$0) })
        }
        else
        {
            self.events = []
        }
        
        //Countries
        if let Countries = json["Countries"].array {
            
            self.countries = Countries.map({ KeyValueObject(json:$0) })
        }
        else
        {
            self.countries = []
        }
        
        //Marital Status
        if let MaritalStatus = json["MaritalStatuses"].array {
            
            self.maritalStatuses = MaritalStatus.map({ KeyValueObject(json:$0) })
        }
        else
        {
            self.maritalStatuses = []
        }
        
        //Business Types
        if let BusinessTypes = json["BusinessTypes"].array {
            
            self.businessTypes = BusinessTypes.map({ KeyValueObject(json:$0) })
        }
        else
        {
            self.businessTypes = []
        }
        
        //Attachments
        if let Attachments = json["Attachments"].array {
            
            self.attachments = Attachments.map({ Attachment(json:$0) })
        }
        else
        {
            self.attachments = []
        }
        
        //Special Characters
        if let specialChar = json["SpecialCharacters"].array {
            
            self.specialCharacters = specialChar.map({ StringKeyValueObject(json:$0) })
        }
        else
        {
            self.specialCharacters = []
        }
        
        if let timeline = json["ProfessionalTimeline"].array {
            
            self.professionalTimeline = timeline.map({ RelevantDate(json: $0) })
        }
        else
        {
            self.professionalTimeline = []
        }
        
        if let whosID = json["WhosId"].array {
            
            self.whosId = whosID.map({$0.intValue})
        }
        else
        {
            self.whosId = []
        }
        
        if let whos1 = json["Whos"].array {
            
             self.whos = whos1.map({ WhoLevel_I(json: $0) })
        }
        else
        {
            self.whos = []
        }
        
        if let whos2 = json["WhoLevel11Sorted"].array {
            
            self.whosLevel2 = whos2.map({ WhoLevel_II(json: $0) })
        }
        else
        {
            self.whosLevel2 = []
        }

        if let geoItems = json["Geographies"].array {
        
            self.geographies = geoItems.map({ClusterItem(json:$0)})
        }
        else
        {
            self.geographies = [ClusterItem]()
        }
        
        
        //Rating
        self.userRating = json["UserRating"].float ?? 0.0
        self.adminRating = json["AdministratorRating"].float ?? 0.0
        self.birthDateString = json["BirthdateStringValue"].string
        
        super.init(json: json)
    }
    
    override init(isDefault: Bool)
    {
        self.summaryPublic = String()
        self.summaryPrivateEsp = String()
        self.summaryPrivate = String()
        self.summaryPublicEsp = String()
        self.generalInformationPrivate = String()
        self.generalInformationPrivateEsp = String()
        self.generalInformationPublic = String()
        self.generalInformationPublicEsp = String()
        self.professionalPrivate = String()
        self.professionalPrivateEsp = String()
        self.professionalPublic = String()
        self.professionalPublicEsp = String()
        self.personalInformationPrivate = String()
        self.personalInformationPrivateEsp = String()
        self.personalInformationPublic = String()
        self.personalInformationPublicEsp = String()
        self.isEnable = false
        self.isCompany = false
        self.isVisiblePrivateFields = false
        self.relationshipType1_id = 1
        self.relationshipType1_name = String()
        self.relationshipType1_namEs = String()
        self.relationshipType2_id = 0
        self.relationshipType2_name = String()
        self.relationshipType2_namEs = String()
        self.phone = String()
        self.email = String()
        self.aliasString = String()
        self.alias = [String]()
        self.nationalities = [KeyValueObject]()
        self.references = [StakeholderReference]()
        self.referencesEsp = [StakeholderReference]()
        self.additionalFields = [StringKeyValueObject]()
        self.relevantDates = [RelevantDate]()
        self.recurrentDates = [RelevantDate]()
        self.dueDate = nil
        self.canEditPrivate = false
        self.totalPosts = 0
        self.totalAttachments = 0
        self.versionId = 0
        self.requestId = 0
        self.versionNumber = 0
        self.createdDate = Date()
        self.posts = [Post]()
        self.members = [Member]()
        self.relations = [ContactRelation]()
        self.projects = [Project]()
        self.events = [Event]()
        self.countries = [KeyValueObject]()
        self.maritalStatuses = [KeyValueObject]()
        self.businessTypes = [KeyValueObject]()
        self.attachments =  [Attachment]()
        self.specialCharacters = [StringKeyValueObject]()
        self.professionalTimeline = [RelevantDate]()
        self.site = Site()
        self.canEdit = false
        self.whosId = []
        self.whos = []
        self.whosLevel2 = []
        self.placeOfBirth = String()
        self.charactersEng = 0
        self.charactersEsp = 0
        self.jobTitle = String()
        self.sites = []
        self.references = []
        self.referencesEsp = []
        self.PhoneNumber = String()
        self.comments = String()
        self.sendRequest = false
        self.industrySector = KeyValueObject()
        self.geographies = [ClusterItem]()
        self.adminRating = 0.0
        self.userRating = 0.0
        
        super.init(isDefault: isDefault)
    }

    override func getWSObject() -> [String : Any]
    {
        var superDict = super.getWSObject()
        superDict["IsCompany"] = isCompany ? "true" : "false"
        superDict["Position"] = jobPosition?.getWSObject() ?? "null"
        superDict["MaritalStatus"] = maritalStatus?.getWSObject() ?? "null"
        superDict["Country"] = country?.getWSObject() ?? "null"
        superDict["BirthPlace"] = placeOfBirth
        superDict["RelationshipType1"] = relationshipType1_id ?? 0
        superDict["RelationshipType1Name"] = relationshipType1_name ?? ""
        superDict["RelationshipType1NameEs"] = relationshipType1_namEs ?? ""
        superDict["RelationshipType2"] = relationshipType2_id ?? 0
        superDict["RelationshipType2Name"] = relationshipType2_name ?? ""
        superDict["RelationshipType2NameEs"] = relationshipType2_namEs ?? ""
        superDict["Enable"] = isEnable ? "true" : "false"
        superDict["Phone"] = phone
        superDict["Email"] = email
        superDict["Alias"] = alias
        superDict["DueDate"] = dueDate?.convertToRequestString() ?? ""
        superDict["CanEditPrivate"] = self.canEditPrivate ? "true" : "false"
        superDict["PostsTotal"] = self.totalPosts
        superDict["TotalAttachments"] = self.totalAttachments
        superDict["VersionId"] = self.versionId
        superDict["RequestId"] = self.requestId
        superDict["VersionNumber"] = self.versionNumber
        superDict["CreatedDate"] = self.createdDate?.convertToRequestString() ?? ""
        superDict["CanEdit"] = self.canEdit ? "true" : "false"
        superDict["Comments"] = self.comments
        superDict["SendRequest"] = self.sendRequest ? "true" : "false"
        superDict["IsAnonymous"] = self.IsAnonymous ? "true" : "false"
        superDict["WhosSiteId"] = self.whosSiteId
        
        
        //Sub process to reduce work in mapping
        if self.isCompany == true
        {
             superDict += ["Position" : industrySector.getWSObject()]
        }
        else
        {
             superDict += ["Position" : jobPosition?.getWSObject() ?? "null"]
        }
        
        superDict += ["Representative" : self.representative?.getWSObject() ?? "null"]
        superDict += ["Coordinator" : self.coordinator?.getWSObject() ?? "null"]
        superDict += ["Members" : self.members.map({$0.getWSObject()})]
        superDict += ["CurrentProjects" : self.projects.map({$0.getWSObject()})]
        superDict += ["Posts" : self.posts.map({$0.getWSObject()})]
        superDict += ["Attachments" : self.attachments.map({$0.getWSObject()})]
        superDict += ["Relations" : self.relations.map({$0.getWSObject()})]
        superDict += ["ResumePublic" : self.summaryPublic]
        superDict += ["ResumePrivateES" : self.summaryPrivateEsp]
        superDict += ["ResumePrivate" : self.summaryPrivate]
        superDict += ["ResumePublicES" : self.summaryPublicEsp]
        superDict += ["GeneralInformationPrivate" : self.generalInformationPrivate]
        superDict += ["GeneralInformationPrivateES" : self.generalInformationPrivateEsp]
        superDict += ["GeneralInformationPublic" : self.generalInformationPublic]
        superDict += ["GeneralInformationPublicES" : self.generalInformationPublicEsp]
        superDict += ["ProfessionalPrivate" : self.professionalPrivate]
        superDict += ["ProfessionalPrivateES" : self.professionalPrivateEsp]
        superDict += ["ProfessionalPublic" : self.professionalPublic]
        superDict += ["ProfessionalPublicES" : self.professionalPublicEsp]
        superDict += ["PersonalInformationPrivate" : self.personalInformationPrivate]
        superDict += ["PersonalInformationPrivateES" : self.personalInformationPrivateEsp]
        superDict += ["PersonalInformationPublic" : self.personalInformationPublic]
        superDict += ["PersonalInformationPublicES" : self.personalInformationPublicEsp]
        superDict += ["SiteMapping": self.site?.getWSObject() ?? "null"]
        superDict += ["WhosId" : self.whosId]
        superDict += ["AdditionalFields" : self.additionalFields.map({ $0.getWSObject() })]
        superDict += ["References" : self.references.map({ $0.getWSObject() })]
        superDict += ["ImportantsDate" : self.relevantDates.map({ $0.getWSObject() })]
        superDict += ["RecurrentDates" : self.recurrentDates.map({ $0.getWSObject() })]
        superDict += ["Nationalities" : self.nationalities.map({ $0.getWSObject() })]
        superDict += ["ProfessionalTimeline" : self.professionalTimeline.map({ $0.getWSObject() })]
        superDict["Geographies"] = self.geographies.map { $0.getWSObject() }
        superDict["AdministratorRating"] = self.adminRating ?? 0
        superDict["BirthdateStringValue"] = self.birthDateString
        
        return superDict
    }
}


class CustomField: Entity {
    var name: String
    var value: String
    
    init(json: JSON)
    {
        name = json["FirstName"].string ?? ""
        value = json["value"].string ?? ""
        super.init()
    }
    
    
    func getWSObject() -> [String : Any]{
        return [
            "FirstName":name,
            "value":value
        ]
    }
}

class RelevantDate: Entity
{
    var id: Int?
    var isFreeDateString: Bool
    var date: Date?
    var stringValue: String?
    var title: String
    var type: Int?
    var language: String
    
    init(json: JSON)
    {
        self.id = json["Id"].int ?? 0
        
        let dateStringBirthday = json["Date"].string ?? ""
        self.date = dateStringBirthday.toDate() ?? nil
        
        self.title = json["Description"].string ?? ""
        self.type = json["Type"].int ?? 0
        
        self.isFreeDateString = json["IsFree"].boolValue
        self.stringValue = json["StringValue"].string ?? ""
        self.language = json["Language"].string ?? ""

    }
    
    override init()
    {
        self.id = 0
        self.isFreeDateString = false
        self.date = Date()
        self.stringValue = Date().convertToRequestString()
        self.title = ""
        self.type = 0
        self.language = String()
    }
    
    func getWSObject() -> [String : Any]{
        return [
            "Id": id ?? 0,
            "Date":date?.convertToRequestString() ?? "",
            "Description":title,
            "Type":type ?? 0,
            "IsFree": String(isFreeDateString),
            "StringValue": stringValue ?? "",
             "Language" : self.language
        ]
    }
}

class StakeholderDefaultData: Entity
{
    var countries: [KeyValueObject]
    var maritalStatus: [KeyValueObject]
    var businessTypes: [KeyValueObject]
    var maxHoursToDueDateInRequests: Int
    var hasRoleDueDateValidation: Bool
    var sites:[Site]
    init(json: JSON)
    {
        //Countries
        if let arrayCountries = json["Countries"].array {
            self.countries = arrayCountries.map({ KeyValueObject(json:$0) })
        }
        else
        {
            self.countries = [KeyValueObject]()
        }
        
        //Marital Status
        if let arrayMaritalStatus = json["MaritalStatus"].array {
            self.maritalStatus = arrayMaritalStatus.map({ KeyValueObject(json:$0) })
        }
        else
        {
            self.maritalStatus = [KeyValueObject]()
        }
        
        //Business Types
        if let arrayBusinessTypes = json["BusinessTypes"].array {
            self.businessTypes = arrayBusinessTypes.map({ KeyValueObject(json:$0) })
        }
        else
        {
            self.businessTypes = [KeyValueObject]()
        }
        
        //Site Mappings
        if let sitesArray = json["SiteMappings"].array {
            
            self.sites = sitesArray.map({Site(json:$0)})
        }
        else if let sitesArray = json["WhosSites"].array {
            
            self.sites = sitesArray.map({Site(json:$0)})
        }
        else
        {
            self.sites = [Site]()
        }
        
        self.maxHoursToDueDateInRequests = json["MaxHoursToDueDateInRequests"].intValue 
        self.hasRoleDueDateValidation = json["HasRoleDueDateValidation"].boolValue 
    }
}

class RateRequest: Entity
{
    var administratorRating: Float
    var userRating: Float
    var givenRating: Float
    var stakeholderId: Int
    
    init(json: JSON)
    {
        self.administratorRating = json["AdministratorRating"].float ?? 0
        self.userRating = json["UserRating"].float ?? 0
        self.givenRating = json["GivenRating"].float ?? 0
        self.stakeholderId = json["StakeholderId"].int ?? -1
    }
    
    override init()
    {
        self.administratorRating = 0.0
        self.userRating = 0.0
        self.givenRating = 0.0
        self.stakeholderId = -1
    }
    
    func getWSObject() -> [String : Any]
    {
        return [
            "AdministratorRating":self.administratorRating,
            "UserRating":self.userRating,
            "GivenRating": self.givenRating,
            "StakeholderId": self.stakeholderId
        ]
    }
}

