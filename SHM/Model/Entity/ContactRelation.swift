//
//  ContactRelation.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 9/3/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class ContactRelation: Contact
{
    var relationshipLevel1: KeyValueObject!
    var relationshipLevel2: KeyValueObject!
    
    override init(json: JSON) {
        super.init(json: json)
        
        self.relationshipLevel1 = KeyValueObject(json: json["RelationType1"])
        self.relationshipLevel2 = KeyValueObject(json: json["RelationType2"])
    }
    
    init()
    {
        self.relationshipLevel1 = KeyValueObject()
        self.relationshipLevel2 = KeyValueObject()
        
        super.init(isDefault: true)
    }
    
    override func getWSObject() -> [String : Any] {
        
        var wsDict = super.getWSObject()
        wsDict += [
            "RelationshipLevel1" : self.relationshipLevel1.getWSObject(),
            "RelationshipLevel2" : self.relationshipLevel2.getWSObject(),
        ]
        return wsDict
    }
}
