//
//  MitigationMeasure.swift
//  SHM
//
//  Created by Manuel Salinas on 10/3/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class MitigationMeasure: Entity
{
    var id: Int
    var title: String
    
    ///Read only variable
    var isNull: Bool {
    
        get { return self.id == 0 && self.title.trim().isEmpty == true ? true : false}
    }
    
    init(json: JSON)
    {
        self.id = json["MitigationMeasureId"].int ?? 0
        self.title = json["Title"].string ?? String()
    }
    
    override init()
    {
        self.id = 0
        self.title = String()
    }

    func getWSObject() -> [String : Any] {
        return [
            "MitigationMeasureId" : self.id,
            "Title" : self.title
        ]
    }
}
