//
//  WhoLevel_I.swift
//  SHM
//
//  Created by Manuel Salinas on 10/4/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class WhoLevel_I: Entity
{
    var id: Int
    var name: String
    var flag: Bool
    var aditionalName: String
    var whoLevel_II:[WhoLevel_II]
    
    init(json: JSON)
    {
        self.id = json["Id"].int ?? 0
        self.name = json["Name"].string ?? String()
        self.flag = json["Flag"].bool ?? false
        self.aditionalName = json["AditionalName"].string ?? String()
        
        if let whosIII =  json["WhoLevel11"].array {
        
            self.whoLevel_II = whosIII.map({ WhoLevel_II(json:$0) })
        }
        else
        {
            self.whoLevel_II = []
        }
    }
    
    override init()
    {
        self.id = 0
        self.name = String()
        self.flag = false
        self.aditionalName = String()
        self.whoLevel_II = []
    }
    
    func getWSObject() -> [String : Any] {
        return [
            "Id" : self.id,
            "Name" : self.name,
            "Flag" : self.flag,
            "AditionalName" : self.aditionalName,
            "WhoLevel111" : self.whoLevel_II.map({$0.id})
        ]
    }


}
