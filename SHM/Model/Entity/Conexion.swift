//
//  Conexion.swift
//  SHM
//
//  Created by Definity First on 2/12/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

public enum NodeType: Int
{
   case stakeholder = 1
   case account = 2
}

class Conexion
{
    var totalNodes: Int
    var totalWeight: Int
    var nodes: [Node] = []
    var edges: [KeyValueObject] = []
    
    init(json: JSON)
    {
        var aux: [Node] = []

        if let nodesResponse = json["Nodes"].array
        {
            aux = nodesResponse.map({ Node(json: $0) })
        }
        
        if let edgeResponse = json["Edges"].array
        {
            self.edges = edgeResponse.map({ KeyValueObject(json: $0) })
        }
        
        for (index, node) in aux.enumerated()
        {
            self.nodes.append(node)
            
            if index < aux.count - 1
            {
                let auxNode: Node = Node(json: nil)
                auxNode.isEdge = true
                auxNode.edge = self.edges[index]
                
                self.nodes.append(auxNode)
            }
        }
        
        self.totalNodes = json["TotalNodes"].int ?? 0
        self.totalWeight = json["TotalWeight"].int ?? 0
    }
}

class Node
{
    var idNode: Int
    var typeNode: Int
    var data: Stakeholder
    var edge: KeyValueObject
    var isEdge: Bool
    
    init(json:JSON)
    {
        self.idNode = json["Id"].int ?? 0
        self.data = Stakeholder(json: json["Data"])
        self.typeNode = json["Type"].int ?? 0
        self.edge = KeyValueObject()
        self.isEdge = false
        self.data.jobPosition?.value = data.jobTitle 
    }
}

class Input
{
    var nodeType: Int
    var start: Int
    var end: Int
    
    init()
    {
        self.nodeType = 0
        self.start = 0
        self.end = 0
    }
    
    func getWSObject() -> [String : Any]
    {
        return ["NodeType": self.nodeType,
            "Start": self.start,
            "End": self.end
        ]
    }
}
