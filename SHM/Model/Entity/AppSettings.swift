//
//  AppSettings.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 6/13/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class AppSettings: NSObject
{
    var maxHoursToDueDateInRequests:Int
    
    init(json:JSON)
    {
        self.maxHoursToDueDateInRequests = json["MaxHoursToDueDateInRequests"].int ?? 48
    }
    
    override init()
    {
        self.maxHoursToDueDateInRequests = 48
    }
    
    func getWSObject() -> [String : Any]{
        return [ "MaxHoursToDueDateInRequests" : self.maxHoursToDueDateInRequests]
    }
}
