//
//  UserPostsReport.swift
//  SHM
//
//  Created by Definity First on 1/28/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class UserPostsReport
{
    // MARK: PROPERTIES
    // Response
    var privacys: [KeyValueObject]
    var timeRanges: [KeyValueObject]
    var projects: [Project]
    var stakeholders: [Stakeholder]
    var geographies: [ClusterItem]
    var posts: [Post]
    var chartPoints:[KeyValueObject]
    
    // Request
    var timeRange: KeyValueObject
    var from: Date
    var to: Date
    var selectedStakeholders: [Stakeholder]
    var selectedProjects: [Project]
    var selectedGeographies: [ClusterItem]
    var privacy: KeyValueObject
    
    // MARK: INIT
    init (json: JSON)
    {
        self.to = Date()
        self.from = self.to.dateByAddingDays(-30)
        
        if let privacy = json["Privacy"].array
        {
            self.privacys = privacy.map({ KeyValueObject (json: $0)} )
            
            self.privacy = self.privacys.count > 0 ? self.privacys[0] : KeyValueObject()
        }
        else
        {
            self.privacys = [KeyValueObject]()
            self.privacy = KeyValueObject()
        }
        
        if let timeRangeResponse = json["Time"].array
        {
            self.timeRanges = timeRangeResponse.map({ KeyValueObject (json: $0)} )
            self.timeRange = self.timeRanges.count > 0 ? self.timeRanges[0] : KeyValueObject()
        }
        else
        {
            self.timeRanges = [KeyValueObject]()
            self.timeRange = KeyValueObject()
        }
        
        if let project = json["Projects"].array
        {
            self.projects = project.map({ Project (json: $0)} )
            self.selectedProjects = [Project]()
            
            for (index, projectFor) in self.projects.enumerated() where index == 0
            {
                self.selectedProjects.append(projectFor)
            }
        }
        else
        {
            self.projects = [Project]()
            self.selectedProjects = [Project]()
        }
        
        if let stakeholder = json["Stakeholders"].array {
            self.stakeholders = stakeholder.map({ Stakeholder (json: $0)} )
            self.selectedStakeholders = [Stakeholder]()
            
            for (index, stakeholderFor) in self.stakeholders.enumerated() where index == 0
            {
                self.selectedStakeholders.append(stakeholderFor)
            }
        }
        else
        {
            self.stakeholders = [Stakeholder]()
            self.selectedStakeholders = [Stakeholder]()
        }
        
        if let geoItems = json["Geographies"].array {
         
            self.geographies = geoItems.map({ClusterItem(json: $0)})
            self.selectedGeographies = [ClusterItem]()
            for (index, geography) in self.geographies.enumerated() where index == 0
            {
                self.selectedGeographies.append(geography)
            }
        }
        else
        {
            self.geographies = [ClusterItem]()
            self.selectedGeographies = [ClusterItem]()
        }
        if let post = json["Posts"].array {
            self.posts = post.map({ Post (json: $0)} )
        }
        else
        {
            self.posts = [Post]()
        }
        
        if let chartPoint = json["ChartPoints"].array {
            self.chartPoints = chartPoint.map({ KeyValueObject (json: $0)} )
        }
        else
        {
            self.chartPoints = [KeyValueObject]()
        }
    }
    
    init()
    {
        self.privacys = [KeyValueObject]()
        self.timeRanges = [KeyValueObject]()
        self.projects = [Project]()
        self.stakeholders = [Stakeholder]()
        self.geographies = [ClusterItem]()
        self.selectedGeographies = [ClusterItem]()
        self.posts = [Post]()
        self.chartPoints = [KeyValueObject]()
        self.timeRange = KeyValueObject()
        self.from = Date()
        self.to = Date()
        self.selectedStakeholders = [Stakeholder]()
        self.selectedProjects = [Project]()
        self.privacy = KeyValueObject()
    }
    
    // MARK Functions
    func getKeyValueStakeholder() -> [KeyValueObject]
    {
        var result = [KeyValueObject]()
        
        for stakeholder in self.stakeholders
        {
            let newResponseStakeholder = KeyValueObject()
            newResponseStakeholder.value = stakeholder.fullName
            newResponseStakeholder.key = stakeholder.id
            result.append(newResponseStakeholder)
        }
        
        return result
    }
    
    func getKeyValueProject() -> [KeyValueObject]
    {
        var result = [KeyValueObject]()
        
        for project in self.projects
        {
            let newResponseProject = KeyValueObject()
            newResponseProject.value = project.title
            newResponseProject.key = project.id
            result.append(newResponseProject)
        }
        
        return result
    }
    
    func getKeyValueForGeography() -> [KeyValueObject]
    {
        var result = [KeyValueObject]()
        for geography in self.geographies
        {
            let newResponseGeography = KeyValueObject()
            newResponseGeography.value = geography.title
            newResponseGeography.key = geography.id
            result.append(newResponseGeography)
        }
        
        return result
    }
    
    func getFormatDateString(_ date: Date) -> String
    {
        let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        if language == "en"
        {
            dateFormatter.locale = Locale(identifier: "en_US")
        }
        else
        {
            dateFormatter.locale = Locale(identifier: "es_MX")
        }
        
        return dateFormatter.string(from: date)
    }
    
    func getFormatDateStringWithMonthName(_ date: Date) -> String
    {
        let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM yyyy"
        if language == "en"
        {
            dateFormatter.locale = Locale(identifier: "en_US")
        }
        else
        {
            dateFormatter.locale = Locale(identifier: "es_MX")
        }

        
        return dateFormatter.string(from: date)
    }
    
    func getWSObject() -> [String : Any]
    {
        return [
            "StartDate": self.from.convertToRequestString(),
            "FinishDate": self.to.convertToRequestString(),
            "PrivacyOption": self.privacy.key,
            "Projects": self.selectedProjects.map({ $0.getWSObject() }),
            "Stakeholders": self.selectedStakeholders.map({ $0.getWSObject() }),
            "Geographies" : self.selectedGeographies.map({$0.getWSObject()})            
        ]
    }
}
