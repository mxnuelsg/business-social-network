//
//  Member.swift
//  SHM
//
//  Created by Manuel Salinas on 8/7/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON


class Member: Contact
{
    var accountName: String
    var email: String
    var phoneNumberArea: String
    var phoneNumberLocal: String
    var phoneNumberCountryName: String
    var phone: String
    var department: String
    var position: String
    var phoneCountries: [MemberCountry]
    var stakeHolderAccountRelations : [Stakeholder]
    var isVip: Bool
    var sites: [Site]
    var isActive: Bool
    
     override init(json: JSON)
    {
        self.accountName = json["Name"].string ?? ""
        self.email = json["Email"].string ?? ""
        self.phone = json["PhoneNumber"].string ?? ""
        self.phoneNumberArea = json["PhoneNumberArea"].string ?? ""
        self.phoneNumberLocal = json["PhoneNumberLocal"].string ?? ""
        self.phoneNumberCountryName = json["PhoneNumberCountryName"].string ?? ""
        self.department = json["Department"].string ?? ""
        self.position = json["JobTitle"].string ?? ""
        self.isVip = json["IsVip"].bool ?? false
        self.isActive = json["IsActive"].bool ?? false
        /** Relations*/
        if let relations = json["AccountStakeholderRelations"].array {
            
            self.stakeHolderAccountRelations = relations.map({Stakeholder(json:$0)})
        }
        else
        {
            self.stakeHolderAccountRelations = [Stakeholder]()
        }
        
        /** Sites */
        if let siteMappings = json["SiteMappings"].array {
            
            self.sites = siteMappings.map({ Site(json:$0) })
        }
        else
        {
            self.sites = [Site]()
        }
        
        /** Phone Countries*/
        if let countries = json["Countries"].array {
            
            self.phoneCountries = countries.map({ MemberCountry(json:$0) })
        }
        else
        {
            self.phoneCountries = [MemberCountry]()
        }
        
        if self.phoneNumberCountryName.isEmpty == false
        {
            let arrayCountryNumber  = self.phoneNumberCountryName.components(separatedBy: "+")
            self.phoneNumberCountryName = "+" + arrayCountryNumber[1]
        }
        
        super.init(json: json)
    }
    
    override init(isDefault: Bool)
    {
        self.accountName = ""
        self.email = ""
        self.phone = ""
        self.phoneNumberArea = ""
        self.phoneNumberLocal = ""
        self.phoneNumberCountryName = ""
        self.department = ""
        self.position = ""
        self.phoneCountries = []
        self.stakeHolderAccountRelations = []
        self.isVip = false
        self.isActive = false
        self.sites = []
        
        super.init(isDefault: isDefault)
    }
    
    override func getWSObject() -> [String : Any] {
        var wsDict = super.getWSObject()
        wsDict += [
            "Name" : self.accountName,
            "Email" : self.email,
            "PhoneNumber" : self.phone,
            "PhoneNumberArea" : self.phoneNumberArea,
            "PhoneNumberLocal" :self.phoneNumberLocal,
            "PhoneNumberCountryName" : self.phoneNumberCountryName,
            "JobTitle" : self.position,
            "AccountId" : self.id,
            "Department" : self.department,
            "AccountStakeholderRelations" : self.stakeHolderAccountRelations.map({ $0.getWSObject() })
            
        ]
        
        return wsDict
    }
    
    func getAttributedName() -> NSMutableAttributedString
    {
        if self.fullName.isEmpty == false
        {
            let strInactive = "Inactive".localized()
            let memberName = self.fullName 
            let strActivity = self.isActive == false ? " (\(strInactive))" : String()
            let attStr = NSMutableAttributedString(string: memberName + strActivity)
            attStr.setColor(strActivity, color: UIColor.red)
            
            return attStr
        }
        else
        {
            return NSMutableAttributedString(string: String())
        }
    }
}

extension Sequence where Iterator.Element == Member {
    func collapseNames() -> String {
        var collapsedString = ""
        let values = self.map({ $0.fullName })
        collapsedString += values.joined(separator: ", ")
        
        return collapsedString
    }
}
