//
//  Site.swift
//  SHM
//
//  Created by Manuel Salinas on 8/9/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class Site: Equatable, Hashable
{
    var hashValue: Int {
        return siteId.hashValue
    }
    
    var siteId: Int
    var parentId: Int
    var title: String
    var email: String
    var levelId: Int
    
    //custom
    var isCentral = false
    var level = 0
    init(json: JSON)
    {
        self.siteId = json["SiteMappingId"].int ?? 0
        self.parentId = json["ParentId"].int ?? 0
        self.levelId = json["LevelId"].int ?? 0
        self.title = json["Title"].string ?? String()
        self.email = json["SiteEmail"].string ?? String()
        
        self.isCentral = self.levelId == 1 ? true : false
    }
    
    init()
    {
        self.siteId = 0
        self.parentId = 0
        self.levelId = 0
        self.title = String()
        self.email = String()
    }
    
    func getWSObject() -> [String : Any]{
        return [
            "SiteMappingId" : self.siteId,
            "ParentId" : self.parentId,
            "LevelId" : self.levelId,
            "Title" : self.title,
            "SiteEmail" : self.email
        ]
    }
    
    func getTitleByLevel() -> String
    {
        switch self.level
        {
        case 0:
            return "\(self.title)"
        case 1:
            return "   \(self.title)"
        case 2:
            return "      \(self.title)"
        default:
            return String()
        }
    }
    static func sortSites(_ sitesArray:[Site]?) -> [Site]
    {
        var sites = sitesArray
        
        //Order sites by regions and countries
        var regions = [Site]()
        var countries = [[Site]]()
        sites?.forEach({ (parent) in
            
            if let children = sites?.filter({parent.siteId == $0.parentId}) {
                
                if children.count > 0
                {
                    //Append regions
                    regions.append(parent)
                    //Append countries
                    countries.append(children)
                }
            }
        })
        
        //if exist, remove CENTRAL from the regions array
        var idxCentral: Int?
        regions.forEach { (region) in
            let array = regions.filter({region.siteId == $0.parentId})
            //Central will be the only one site with children within the region array
            if array.count > 0
            {
                idxCentral = regions.index(of: region)
            }
        }
        var central: Site?
        if let idx = idxCentral {
            central = regions[idx]
            regions.remove(at: idx)
            countries.remove(at: idx)
        }
        
        //Assign hierarchy level to sites
        central?.level = 0
        regions.forEach({$0.level = 1})
        countries.forEach({$0.forEach({$0.level = 2})})
        
        //Order array
        sites?.removeAll()
        if let centralSite = central {
            sites?.append(centralSite)
        }
        for (idx, site) in regions.enumerated()
        {
            sites?.append(site)
            sites?.append(contentsOf: countries[idx])
        }
        
        return sites ?? [Site]()
    }
}

func ==(leftHashValue: Site, rightHashValue: Site) -> Bool
{
    return leftHashValue.siteId == rightHashValue.siteId
}
