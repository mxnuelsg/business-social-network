//
//  CalendarItem.swift
//  SHM
//
//  Created by Manuel Salinas on 10/22/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class CalendarItem: Entity
{
    //Properties
    var id: Int
    var isPrivate: Bool
    var thumbnail: String
    var itemDescription: String
    var createdDate: String
    var allDay: Bool = false
    var isStakeholder: Bool = false
    var startDate: Date
    var type: String
    var title: String
    var createdBy: Member
    var stakeholders: [Stakeholder]
    var events: [Event]
    var projects: [Project]
    var attachments: [Attachment]
    var geographies:[ClusterItem]
    //custom property
    var titleFormated = ""
    
    init(json: JSON)
    {
        id = json["Id"].int ?? json["id"].int ?? 0
        self.isPrivate = json["IsPrivate"].boolValue
        thumbnail = json["Thumbnail"].string ?? ""
        itemDescription = json["Description"].string ?? ""
        type = json["Type"].string ?? String()
        title = json["Title"].string ?? json["title"].string ?? ""
        createdDate = json["CreatedDate"].string ?? ""
        createdBy = Member(json: json["CreatedBy"])
        
        let dateString = json["StartsAt"].string ?? json["startsAt"].string ?? ""
        startDate = dateString.toDate()!
        
        //Boolean values
        if let AllDay = json["AllDay"].int {
            allDay = Bool(AllDay as NSNumber)
        }
        
        if let IsStakeholder = json["IsStakeholder"].int {
            isStakeholder = Bool(IsStakeholder as NSNumber)
        }
        
        /**StakeHolders*/
        if let stakeholders = json["Stakeholders"].array {
            self.stakeholders = stakeholders.map({ Stakeholder(json:$0) })
        }
        else
        {
            self.stakeholders = [Stakeholder]()
        }
        if let geoItems = json["Geographies"].array {
            
            self.geographies = geoItems.map({ClusterItem(json: $0)})
        }
        else
        {
            self.geographies = [ClusterItem]()
        }
        /**Events*/
        if let events = json["Events"].array
        {
            self.events = events.map({ Event(json:$0) })
        }
        else
        {
            self.events = [Event]()
        }
        
        /**Projects*/
        if let project = json["Projects"].array
        {
            self.projects = project.map({ Project(json:$0) })
        }
        else
        {
            self.projects = [Project]()
        }
        
        /**Attachments*/
        if let files = json["Attachments"].array
        {
            self.attachments = files.map({ Attachment(json:$0) })
        }
        else
        {
            self.attachments = [Attachment]()
        }
        
        //Custom property fill
        if type.lowercased() == "post"
        {
            let customPost = Post()
            customPost.title = title
            customPost.stakeholders = stakeholders
            customPost.projects = projects
            customPost.events = events
            
            titleFormated = title.getEditedTitleForPost(customPost, truncate: false)
        }
        else
        {
            titleFormated = title
        }
    }
}
