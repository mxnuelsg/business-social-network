//
//  WhoLevels.swift
//  SHM
//
//  Created by Manuel Salinas on 10/6/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class WhoLevels: Entity
{
    var id: Int
    var name: String
    var parentId: Int
    var url: String
    var flag: Bool
    var aditionalName: String
    var level: String
    
    
    init(json: JSON)
    {
        self.id = json["Id"].int ?? 0
        self.name = json["Name"].string ?? String()
        self.parentId = json["ParentId"].int ?? 0
        self.url = json["Url"].string ?? String()
        self.flag = json["Flag"].bool ?? false
        self.aditionalName = json["AditionalName"].string ?? String()
        self.level = json["Lvl"].string ?? String()
        
    }
    
    override init()
    {
        self.id = 0
        self.name = String()
        self.parentId = 0
        self.url = String()
        self.flag = false
        self.aditionalName = String()
        self.level = String()
        
    }
    
    func getWSObject() -> [String : Any] {
        return [
            "Id" : self.id,
            "Name" : self.name,
            "ParentId" : self.parentId,
            "Url" : self.url,
            "Flag" : self.flag,
            "AditionalName" : self.aditionalName,
            "Lvl" : self.level
        ]
    }

}
