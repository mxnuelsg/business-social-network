//
//  ProjectsBySite.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 8/15/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit



class ProjectsBySite: Project
{
    var arraySectionTitles = [NSMutableAttributedString]()
    var arrayRegions = [String]()
    var arrayProjectsBySiteAndFollowStatus = [[Project]()]
    var projects:[Project]!
    
    init(projects:[Project])
    {
        self.projects = projects
        super.init(isDefault: true)
        self.groupProjectsByRegion()
    }
    
    func groupProjectsByRegion()
    {
        self.clear()
        
        //Get the regions present at current project list
        for project in self.projects
        {
            if let siteTitle = project.site?.title, self.arrayRegions.contains(siteTitle) == false
            {
                self.arrayRegions.append(siteTitle)
            }
        }
        //Divide the projects by following status
        let followedProjects = self.projects.filter({$0.followingStatus == true})
        let notFollowedProjects = self.projects.filter({$0.followingStatus == false})
        
        //Order the followed projects
        for site in self.arrayRegions
        {
            var followedInSite = followedProjects.filter({$0.site?.title == site})
            followedInSite = followedInSite.sorted(by: {$0.title < $1.title})
            
            if followedInSite.count > 0
            {
                //Add ordered projects array (for cells)
                self.arrayProjectsBySiteAndFollowStatus.append([Project]())
                self.arrayProjectsBySiteAndFollowStatus[self.arrayProjectsBySiteAndFollowStatus.count - 1] = followedInSite
                //Add a title description for this array (for section)
                let sectionTitleFollow = NSMutableAttributedString(string: "\(SiteSubtitle.Following.rawValue.localized())\n  ⚑ \(site)")
                sectionTitleFollow.setColor(SiteSubtitle.Following.rawValue.localized(), color: UIColor.blackAsfalto())
                self.arraySectionTitles.append(sectionTitleFollow)
            }
        }
        
        //Order the NOT Followed projects
        
        for site in self.arrayRegions
        {
            var notFollowedInSite = notFollowedProjects.filter({$0.site?.title == site})
            notFollowedInSite = notFollowedInSite.sorted(by: {$0.title < $1.title})
            
            if notFollowedInSite.count > 0
            {
                //At ordered stakeholder array (for cells)
                self.arrayProjectsBySiteAndFollowStatus.append([Project]())
                self.arrayProjectsBySiteAndFollowStatus[self.arrayProjectsBySiteAndFollowStatus.count - 1] = notFollowedInSite
                //At a title description of this array (for section)
                let sectionTitleNotFollow = NSMutableAttributedString(string: "\(SiteSubtitle.NotFollowing.rawValue.localized())\n    ⚑ \(site)")
                sectionTitleNotFollow.setColor(SiteSubtitle.NotFollowing.rawValue.localized(), color: UIColor.blackAsfalto())
                self.arraySectionTitles.append(sectionTitleNotFollow)
            }
        }
    }
    
    func updateProjects(_ projects:[Project]?)
    {
        self.projects = projects
        self.groupProjectsByRegion()
    }
    
    func clear()
    {
        self.arraySectionTitles.removeAll()
        self.arrayRegions.removeAll()
        self.arrayProjectsBySiteAndFollowStatus.removeAll()
    }
    
    func isEmpty() -> Bool
    {
        return self.arrayProjectsBySiteAndFollowStatus.count == 0 ? true:false
    }
}
