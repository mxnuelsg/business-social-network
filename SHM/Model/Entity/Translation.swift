//
//  Translation.swift
//  SHM
//
//  Created by Infrastructure Support on 6/15/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class Translation: Entity
{
    var file: Data?
    var version: Int
    var shouldRefresh: Bool
    
    init(json: JSON)
    {
        self.version = json["Version"].int ?? 0
        self.shouldRefresh = json["IsUpdate"].boolValue 
       
        if self.shouldRefresh == true
        {
            let filePath = json["FilePath"].string ?? String()
            
            if filePath.isEmpty == false
            {
               self.file = LibraryAPI.shared.customLocale.getJson(filePath)
            }
            else
            {
               self.file = nil
            }
        }
        else
        {
            self.file = nil
        }
    }
}
