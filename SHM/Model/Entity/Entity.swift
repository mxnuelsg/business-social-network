//
//  Entity.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/4/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

class Entity: NSObject
{
    
}

enum FollowStatus: Int
{
    case all = 1
    case following = 2
    case notFollowing = 3
}
