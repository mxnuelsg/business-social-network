//
//  User.swift
//  SHM
//
//  Created by Manuel Salinas on 8/11/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

enum UserRole : Int
{
    case none = 0
    case privateUser = 1
    case publicUser = 2
    case cardManager = 3
    case cardCreator = 4
    case globalManager = 5
    case vipUser = 6
    case localManager = 7
}

class User : Member
{
    
    //Access info
    var password: String
    var accessToken: String
    var refreshToken: String
    var clientId: String
    var clientSecret: String
    var tokenType: String
    var cemexToken: String
    
    var userName: String?
    var sessionCookie: String?
    var u: String?
    var p: String?
    var deviceToken: String?
    var role = UserRole.none
    var userSettings: UserSettings
    var unReadInbox = 0
    var lastLogin: Date?
    var isLoggedIn: Bool = false
    var authToken: String?
    init ()
    {
        //Access info
        self.password = String()
        self.accessToken = String()
        self.refreshToken = String()
        self.clientId = String()
        self.clientSecret = String()
        self.tokenType = String()
        self.cemexToken = String()
        self.authToken = String()
        self.userSettings = UserSettings()
        super.init(isDefault: true)
    }
}

// DEFAULT SETTINGS
class UserSettings
{
    var notificationCardUpdateActive: Bool
    var notificationStakeholderEditActive: Bool
    var notificationProjectEditActive: Bool
    var notificationNewPostActive: Bool
    var stakeholderNotice: Int
    var appSettings: AppSettings
    var language: Int
    
    init()
    {
        self.notificationCardUpdateActive = false
        self.notificationStakeholderEditActive = false
        self.notificationProjectEditActive = false
        self.notificationNewPostActive = false
        self.stakeholderNotice = 0
        self.appSettings = AppSettings()
        self.language = 1
    }
    
    init(json: JSON)
    {
        self.notificationCardUpdateActive = json["PushPublishedUpdate"].boolValue
        self.notificationStakeholderEditActive = json["PushEditStakeholder"].boolValue
        self.notificationProjectEditActive = json["PushEditProject"].boolValue
        self.notificationNewPostActive = json["PushNewPost"].boolValue
        self.stakeholderNotice = json["StakeholderNotice"].int ?? 0
        self.language = json["Language"].int ?? 1
        
        let stakeholderOption = json["StakeholderNameOrder"].int ?? 1
        let projectOption = json["ProjectListOrder"].int ?? 1
        
        if stakeholderOption == 1
        {
            UserDefaultsManager.setContactOrder(false)
        }
        else
        {
            UserDefaultsManager.setContactOrder(true)
        }
        
        if projectOption == 1
        {
            UserDefaultsManager.setProjectOrder(false)
        }
        else
        {
            UserDefaultsManager.setProjectOrder(true)
        }
        
        self.appSettings = AppSettings(json:json["AppSettings"])        
    }
}
