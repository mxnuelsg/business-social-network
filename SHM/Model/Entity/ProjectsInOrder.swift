//
//  ProjectsInOrder.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 8/19/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

enum ProjectFollowStatus:String
{
    case Following = "  ✔︎ Following"
    case NotFollowing = "  Not Following"
}

enum ProjectsOrderedBy:Int
{
    case byFollowStatus = 0
    case byRegionFollowStatus = 1
}

class ProjectsInOrder: Project
{
    var arraySectionTitles = [NSMutableAttributedString]()
    var arrayRegions = [String]()
    var arrayProjectsBySiteAndFollowStatus = [[Project]()]
    var projects:[Project]!
    var orderType = ProjectsOrderedBy.byFollowStatus.rawValue
    
    init(projects:[Project], orderBy:Int = ProjectsOrderedBy.byFollowStatus.rawValue)
    {
        self.projects = projects
        self.orderType = orderBy
        super.init(isDefault: true)
        
        switch orderBy
        {
        case ProjectsOrderedBy.byFollowStatus.rawValue:
            self.groupProjectsByFollowStatus()
        case ProjectsOrderedBy.byRegionFollowStatus.rawValue:
            self.groupProjectsByRegion()
        default:
            break
        }
        
    }
    
    func groupProjectsByFollowStatus()
    {
        self.clear()
        
        //Get Two Groups by follow status
        var followedProjects = self.projects.filter({$0.followingStatus == true})
        followedProjects = followedProjects.sorted(by: {$0.title < $1.title})
        
        var notFollowedProjects = self.projects.filter({$0.followingStatus == false})
        notFollowedProjects = notFollowedProjects.sorted(by: {$0.title < $1.title})
        
        if followedProjects.count > 0
        {
            //Add array of followed projects
            self.arrayProjectsBySiteAndFollowStatus.append([Project]())
            self.arrayProjectsBySiteAndFollowStatus[self.arrayProjectsBySiteAndFollowStatus.count - 1] = followedProjects
            
            //Add a title description for this array (for section)
            let sectionTitleFollow = NSMutableAttributedString(string: "\(ProjectFollowStatus.Following.rawValue.localized())")
            sectionTitleFollow.setColor(ProjectFollowStatus.Following.rawValue.localized(), color: UIColor.blackAsfalto())
            self.arraySectionTitles.append(sectionTitleFollow)
        }
        
        if notFollowedProjects.count > 0
        {
            //Add array of followed projects
            self.arrayProjectsBySiteAndFollowStatus.append([Project]())
            self.arrayProjectsBySiteAndFollowStatus[self.arrayProjectsBySiteAndFollowStatus.count - 1] = notFollowedProjects
            //Add a title description for this array (for section)
            let sectionTitleFollow = NSMutableAttributedString(string: "\(ProjectFollowStatus.NotFollowing.rawValue.localized())")
            sectionTitleFollow.setColor(ProjectFollowStatus.NotFollowing.rawValue.localized(), color: UIColor.blackAsfalto())
            self.arraySectionTitles.append(sectionTitleFollow)
        }
    }
    
    func groupProjectsByRegion()
    {
        self.clear()
        
        //Get the regions present at current project list
        for project in self.projects
        {
            if let siteTitle = project.site?.title, self.arrayRegions.contains(siteTitle) == false
            {
                self.arrayRegions.append(siteTitle)
            }
        }
        //Divide the projects by following status
        let followedProjects = self.projects.filter({$0.followingStatus == true})
        let notFollowedProjects = self.projects.filter({$0.followingStatus == false})
        
        //Order the followed projects
        for site in self.arrayRegions
        {
            var followedInSite = followedProjects.filter({$0.site?.title == site})
            followedInSite = followedInSite.sorted(by: {$0.title < $1.title})
            
            if followedInSite.count > 0
            {
                //Add ordered projects array (for cells)
                self.arrayProjectsBySiteAndFollowStatus.append([Project]())
                self.arrayProjectsBySiteAndFollowStatus[self.arrayProjectsBySiteAndFollowStatus.count - 1] = followedInSite
                //Add a title description for this array (for section)
                let sectionTitleFollow = NSMutableAttributedString(string: "\(ProjectFollowStatus.Following.rawValue.localized())\n  ⚑ \(site)")
                sectionTitleFollow.setColor(ProjectFollowStatus.Following.rawValue.localized(), color: UIColor.blackAsfalto())
                self.arraySectionTitles.append(sectionTitleFollow)
            }
        }
        
        //Order the NOT Followed projects
        
        for site in self.arrayRegions
        {
            var notFollowedInSite = notFollowedProjects.filter({$0.site?.title == site})
            notFollowedInSite = notFollowedInSite.sorted(by: {$0.title < $1.title})
            
            if notFollowedInSite.count > 0
            {
                //At ordered stakeholder array (for cells)
                self.arrayProjectsBySiteAndFollowStatus.append([Project]())
                self.arrayProjectsBySiteAndFollowStatus[self.arrayProjectsBySiteAndFollowStatus.count - 1] = notFollowedInSite
                //At a title description of this array (for section)
                let sectionTitleNotFollow = NSMutableAttributedString(string: "\(ProjectFollowStatus.NotFollowing.rawValue.localized())\n    ⚑ \(site)")
                sectionTitleNotFollow.setColor(ProjectFollowStatus.NotFollowing.rawValue.localized(), color: UIColor.blackAsfalto())
                self.arraySectionTitles.append(sectionTitleNotFollow)
            }
        }
    }
    
    func updateProjects(_ projects:[Project]?)
    {
        self.projects = projects
        switch self.orderType
        {
        case ProjectsOrderedBy.byFollowStatus.rawValue:
            self.groupProjectsByFollowStatus()
        case ProjectsOrderedBy.byRegionFollowStatus.rawValue:
            self.groupProjectsByRegion()
        default:
            break
        }
    }
    
    func clear()
    {
        self.arraySectionTitles.removeAll()
        self.arrayRegions.removeAll()
        self.arrayProjectsBySiteAndFollowStatus.removeAll()
    }
    
    func isEmpty() -> Bool
    {
        return self.arrayProjectsBySiteAndFollowStatus.count == 0 ? true:false
    }
}
