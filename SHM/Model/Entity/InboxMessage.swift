//
//  InboxMessage.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 9/21/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class InboxMessage: Entity {
    var id: Int
    var subject: String
    var body: String
    var shortBody: String
    var from: Member
    var date: Date
    var viewed: Bool = false
    var hasPrevious: Bool = false
    var isCollapsed = true
    var conversationId: Int
    var viewedBy: [KeyValueObject]
    var recipients: [Member]
    var hasReply: Bool
    var expiration: Int
    var daysLeft: Int
    
    init(json: JSON)
    {
        self.id = json["MessageId"].int ?? 0
        self.conversationId = json["ConversationId"].int ?? 0
        self.expiration = json["Expiration"].int ?? -1
        self.daysLeft = json["DaysLeft"].int ?? 0
        self.subject = json["Subject"].string ?? ""
        self.body = json["Body"].string ?? ""
        self.shortBody = json["ShortBody"].string ?? ""
        self.from = Member(json: json["From"])
        self.hasReply = json["HasReply"].boolValue
        
        let dateString = json["RawCreatedDate"].string ?? ""
        date = dateString.toDate() ?? Date()
        
        self.viewed = json["Viewed"].boolValue
        self.hasPrevious = json["HasPrevious"].boolValue
        
        if let viewedBy = json["ViewedBy"].array {
            self.viewedBy = viewedBy.map({ KeyValueObject(json: $0) })
        }
        else {
            self.viewedBy = []
        }
        
        if let recipientsJson = json["Recipients"].array {
            self.recipients = recipientsJson.map({ Member(json: $0) })
        }
        else {
            self.recipients = []
        }
        
        super.init()
    }
    
    override init()
    {
        self.id = 0
        self.subject = ""
        self.body = ""
        self.shortBody = ""
        self.from = Member(isDefault: true)
        self.date = Date()
        self.viewed = false
        self.hasPrevious = false
        self.isCollapsed = false
        self.conversationId = 0
        self.viewedBy = []
        self.recipients = []
        self.hasReply = false
        self.expiration = -1
        self.daysLeft = -1
        
        super.init()
    }
    
    func getWSObject() -> [String : Any]{
        return [
            "MessageId":id,
            "ConversationId":conversationId,
            "Subject":subject,
            "Body":body,
            "HasReply": hasReply,
            "From": from.getWSObject(),
            "Expiration": expiration,
            "DaysLeft":  daysLeft,
            "Recipients":recipients.map({ $0.getWSObject() })
        ]
    }
}
