//
//  ClusterItem.swift
//  SHM
//
//  Created by Manuel Salinas on 7/10/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON
class ClusterItem: NSObject, GMUClusterItem
{
    var position: CLLocationCoordinate2D
    var title: String
    var parentTitle: String
    var stakeholders: [Stakeholder]
    var projects: [Project]
    var posts: [Post]
    var events: [CalendarItem]
    var createdDate: Date?
    var modifiedDate: Date?
    var createdById: Int
    var id: Int
    var modifiedById: Int
    var isFollowed: Bool
    var titleComplete: String
    init(position: CLLocationCoordinate2D)
    {
        self.position = position
        self.title = String()
        self.parentTitle = String()
        self.titleComplete = String()
        self.createdById = -1
        self.id = -1
        self.isFollowed = false
        self.modifiedById = -1
        self.stakeholders = [Stakeholder]()
        self.projects = [Project]()
        self.posts = [Post]()
        self.events = [CalendarItem]()
    }
    
    init(json: JSON)
    {
        self.id = json["GeographyId"].int ?? -1
        self.title = json["Title"].string ?? String()
        self.parentTitle = json["ParentTitle"].string ?? String()
        self.createdById = json["CreatedById"].int ?? -1
        self.modifiedById = json["ModifiedById"].int ?? -1
        self.isFollowed = json["IsFollowed"].bool ?? false
        
        var completeTitle = self.title
        if self.parentTitle.isEmpty == false
        {
            if self.parentTitle.characters.first == ","
            {
                self.parentTitle.characters.removeFirst()
            }
            completeTitle += "," + self.parentTitle
        }
        self.titleComplete = completeTitle
        //Item's location
        if let jsonLocation = json["Location"].dictionary {
            
            self.position = CLLocationCoordinate2D(latitude: jsonLocation["lat"]?.double ?? 0.0, longitude: jsonLocation["lng"]?.double ?? 0.0)
        }
        else
        {
            self.position = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
        }
        
        //Projects, stakeholders, posts
        if let stakeholderArray =  json["Stakeholders"].array {
            
            self.stakeholders = stakeholderArray.map({Stakeholder(json: $0)})
        }
        else
        {
            self.stakeholders = [Stakeholder]()
        }
        
        if let projectArray =  json["Projects"].array {
            
            self.projects = projectArray.map({Project(json: $0)})
        }
        else
        {
            self.projects  = [Project]()
        }
        
        if let postArray =  json["Posts"].array {
            
            self.posts = postArray.map({Post(json: $0)})
        }
        else
        {
            self.posts  = [Post]()
        }
        if let eventArray = json["Events"].array {
            
            self.events = eventArray.map({CalendarItem(json:$0)})
        }
        else
        {
            self.events = [CalendarItem]()
        }
        
        //Dates
        if let creationDateStr = json["CreatedDate"].string {
            
            self.createdDate = creationDateStr.toDate()
        }
        if let modificationDateStr = json["ModifiedDate"].string {
            
            self.modifiedDate = modificationDateStr.toDate()
        }
    }
    
    func getWSObject() -> [String: Any]
    {
        let location = ["lat": self.position.latitude,
                        "lng": self.position.longitude]
        
        var wsobject: [String: Any] = [:]
        
        wsobject["Title"] = self.title
        wsobject["Location"] = location
        wsobject["Stakeholders"] = self.stakeholders.map({$0.getWSObject()})
        wsobject["Projects"] = self.projects.map({$0.getWSObject()})
        wsobject["Posts"] = self.posts.map({$0.getWSObject()})
        wsobject["CreatedDate"] = self.createdDate?.convertToRequestString() ?? String()
        wsobject["ModifiedDate"] = self.modifiedDate?.convertToRequestString() ?? String()
        wsobject["CreatedById"] = self.createdById
        wsobject["GeographyId"] = self.id
        wsobject["ModifiedById"] = self.modifiedById
        wsobject["IsFollowed"] = self.isFollowed
        wsobject["ParentTitle"] = self.parentTitle
        
        return wsobject
    }
    
    
    func getTitleForDetail()-> NSMutableAttributedString
    {
        let subtitleArray = self.parentTitle.components(separatedBy: ",")
        var title = self.title
        for subtitle in subtitleArray
        {
            title = title + "\n" + subtitle.trim()
        }
        let titleAttr = NSMutableAttributedString(string: title)
        titleAttr.setBold(self.title, size: 16)
        
        return titleAttr
    }
}

class GeographyQuery
{
    var searchText: String?
    var geographyId: Int?
    var pageIndex: Int?
    var orderBy: String?
    var descending: Bool?
    var pagination: Bool?
    var isOrderedByTitle = false
    var searchByLetter = false
    var pageNumber: Int = 1
    var pageSize: Int = 20
    var totalPages: Int = 0
    var totalItems: Int = 0 {
        didSet{
            let result: Double = Double(self.totalItems) / Double(self.pageSize)
            let elements = "\(result)".components(separatedBy: ".")
            
            if elements.count ==  2
            {
                let entire: Int = Int(elements[0])!
                let decimal: Int = Int(elements[1])!
                
                if decimal > 0
                {
                    self.totalPages = entire + 1
                }
                else
                {
                    self.totalPages = entire
                }
            }
            else
            {
                self.totalPages = Int(result)
            }
        }
    }
    
    func clear()
    {
        self.searchText = String()
        self.geographyId = -1
        self.pageIndex = 0
        self.orderBy = String()
        self.descending = false
        self.pagination = false
        self.pageNumber = 1
        self.pageSize = 20
        self.totalPages = 0
        self.totalItems = 0
    }
    
    func getAllWithPagination(pageSize:Int = 20)
    {
        self.searchText = String()
        self.geographyId = -1
        self.pageIndex = 0
        self.orderBy = String()
        
        self.descending = false
        self.pagination = true
        self.pageNumber = 1
        self.pageSize = pageSize
        
        self.totalPages = 0
        self.totalItems = 0        
    }
    
    func setSearchText(_ text: String)
    {
        self.searchText = text
        self.geographyId = -1
        self.pageIndex = 0
        self.orderBy = String()
        
        self.descending = false
        self.pagination = true
        self.pageNumber = 1
        self.pageSize = 20
        
        self.totalPages = 0
        self.totalItems = 0
    }
    
    func getWSObject() -> [String: Any]
    {
        var wsobject: [String: Any] = [:]


        wsobject["request.searchText"] = self.searchText ?? String()
        wsobject["request.totalItems"] = self.totalItems
        wsobject["request.totalPages"] = self.totalPages
        wsobject["request.pageNumber"] = self.pageNumber
        wsobject["request.pageSize"] = self.pageSize
        wsobject["request.pagination"] = (self.pagination ?? false) == true ? "true" : "false"        
        wsobject["request.isOrderedByTitle"] = self.isOrderedByTitle == false ? "false" : "true"
        wsobject["request.searchByLetter"] = "false"
        wsobject["request.descending"] = (self.descending ?? false) == true ? "true" : "false"
        
        return wsobject
    }
    
    func getWSObject2() -> [String: Any]
    {
        var wsobject: [String: Any] = [:]
        
        wsobject["request.isOrderedByTitle"] = self.isOrderedByTitle == false ? "false" : "true"
        wsobject["request.searchByLetter"] = "false"
        wsobject["request.descending"] = "false"
        wsobject["request.pageNumber"] = self.pageNumber
        wsobject["request.pageSize"] = self.pageSize
        wsobject["request.searchText"] = self.searchText ?? String()
        wsobject["request.letterToSearch"] =  String()
        
        return wsobject
    }

}
