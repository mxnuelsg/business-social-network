//
//  StakeholderInOrder.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 8/19/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

enum FollowStatusSection:String
{
    case Following = "  ✔︎ Following"
    case NotFollowing = "  Not Following"
}

enum StakeholderOrderedBy:Int
{
    case byFollowStatus = 0
    case byRegionFollowStatus = 1
}

class StakeholderInOrder: Stakeholder
{
    var arraySectionTitles = [NSMutableAttributedString]()
    var arrayRegions = [String]()
    var arrayStakeholdersBySiteAndFollowStatus = [[Stakeholder]()]
    var stakeholders:[Stakeholder]!
    var orderType = StakeholderOrderedBy.byFollowStatus.rawValue
    
    init(stakeholders:[Stakeholder], orderBy:Int = StakeholderOrderedBy.byFollowStatus.rawValue)
    {
        self.stakeholders = stakeholders
        self.orderType = orderBy
        super.init(isDefault: true)
        
        switch orderBy
        {
        case StakeholderOrderedBy.byFollowStatus.rawValue:
            self.groupStakeholdersByFollowStatus()
        case StakeholderOrderedBy.byRegionFollowStatus.rawValue:
            self.groupStakeholdersByRegion()
        default:
            break
        }
    }
    
    func groupStakeholdersByFollowStatus()
    {
        //clear arrays
        self.clear()
        
        //Get Two Groups by follow status
        var followedStakeholders = self.stakeholders.filter({$0.isFollow == FollowStatus.following})
        followedStakeholders = followedStakeholders.sorted(by: {$0.fullName < $1.fullName})
        
        var notFollowedStakeholders = self.stakeholders.filter({$0.isFollow == FollowStatus.notFollowing})
        notFollowedStakeholders = notFollowedStakeholders.sorted(by: {$0.fullName < $1.fullName})
        
        if followedStakeholders.count > 0
        {
            //Add stakeholder array
            self.arrayStakeholdersBySiteAndFollowStatus.append([Stakeholder]())
            self.arrayStakeholdersBySiteAndFollowStatus[self.arrayStakeholdersBySiteAndFollowStatus.count-1] =  followedStakeholders
            //Add Section title
            let sectionTitleFollow = NSMutableAttributedString(string: "\(FollowStatusSection.Following.rawValue.localized())")
            sectionTitleFollow.setColor(FollowStatusSection.Following.rawValue.localized(), color: UIColor.blackAsfalto())
            self.arraySectionTitles.append(sectionTitleFollow)
        }
        
        if notFollowedStakeholders.count > 0
        {
            //Add stakeholder array
            self.arrayStakeholdersBySiteAndFollowStatus.append([Stakeholder]())
            self.arrayStakeholdersBySiteAndFollowStatus[self.arrayStakeholdersBySiteAndFollowStatus.count-1] =  notFollowedStakeholders
            //Add section title
            let sectionTitleNotFollow = NSMutableAttributedString(string: "\(SiteSubtitle.NotFollowing.rawValue.localized())")
            sectionTitleNotFollow.setColor(SiteSubtitle.NotFollowing.rawValue.localized(), color: UIColor.blackAsfalto())
            self.arraySectionTitles.append(sectionTitleNotFollow)
        }
    }
    
    func groupStakeholdersByRegion()
    {
        //clear arrays
        self.clear()
        
        //Get the regions present at current stakeholder list
        for stakeholder in self.stakeholders
        {
            if let siteTitle = stakeholder.site?.title, self.arrayRegions.contains(siteTitle) == false {
                
                self.arrayRegions.append(siteTitle)
            }
        }
        
        //Divide the Stakeholders By FollowStatus
        let followedStakeholders = self.stakeholders.filter({$0.isFollow == FollowStatus.following})
        let notFollowedStakeholders = self.stakeholders.filter({$0.isFollow == FollowStatus.notFollowing})
        
        //Order the followed stakeholders
        for site in self.arrayRegions
        {
            var followedInSite = followedStakeholders.filter({$0.site?.title == site})
            followedInSite = followedInSite.sorted(by: {$0.fullName < $1.fullName})
            
            if followedInSite.count > 0
            {
                //Add ordered stakeholder array (for cells)
                self.arrayStakeholdersBySiteAndFollowStatus.append([Stakeholder]())
                self.arrayStakeholdersBySiteAndFollowStatus[self.arrayStakeholdersBySiteAndFollowStatus.count-1] =  followedInSite
                //Add a title description of this array (for section)
                let sectionTitleFollow = NSMutableAttributedString(string: "\(SiteSubtitle.Following.rawValue.localized())\n  ⚑ \(site)")
                sectionTitleFollow.setColor(SiteSubtitle.Following.rawValue.localized(), color: UIColor.blackAsfalto())
                self.arraySectionTitles.append(sectionTitleFollow)
            }
        }
        
        //Order the NOT Followed stakeholders
        for site in self.arrayRegions
        {
            var notFollowedInSite = notFollowedStakeholders.filter({$0.site?.title == site})
            notFollowedInSite = notFollowedInSite.sorted(by: {$0.fullName < $1.fullName})
            
            if notFollowedInSite.count > 0
            {
                //At ordered stakeholder array (for cells)
                self.arrayStakeholdersBySiteAndFollowStatus.append([Stakeholder]())
                self.arrayStakeholdersBySiteAndFollowStatus[self.arrayStakeholdersBySiteAndFollowStatus.count-1] =  notFollowedInSite
                //At a title description of this array (for section)
                let sectionTitleNotFollow = NSMutableAttributedString(string: "\(SiteSubtitle.NotFollowing.rawValue.localized())\n    ⚑ \(site)")
                sectionTitleNotFollow.setColor(SiteSubtitle.NotFollowing.rawValue.localized(), color: UIColor.blackAsfalto())
                self.arraySectionTitles.append(sectionTitleNotFollow)
            }
        }
    }
    
    
    func updateStakeholders(_ stakeholders:[Stakeholder])
    {
        self.stakeholders = stakeholders
        
        switch self.orderType
        {
        case StakeholderOrderedBy.byFollowStatus.rawValue:
                self.groupStakeholdersByFollowStatus()
        case StakeholderOrderedBy.byRegionFollowStatus.rawValue:
                self.groupStakeholdersByRegion()
        default:
            break
        }
    }
    
    func isEmpty() -> Bool
    {
        return self.arrayStakeholdersBySiteAndFollowStatus.count == 0 ? true:false
    }
    
    func clear()
    {
        self.arraySectionTitles.removeAll()
        self.arrayRegions.removeAll()
        self.arrayStakeholdersBySiteAndFollowStatus.removeAll()
    }
}
