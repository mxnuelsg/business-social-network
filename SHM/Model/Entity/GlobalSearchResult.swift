//
//  GlobalSearchResult.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 9/30/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class GlobalSearchResult: Entity {
    var stakeholders: [Stakeholder] = []
    var projects: [Project] = []
    var posts: [Post] = []
    
    var stakeholdersTotal: Int
    var projectsTotal: Int
    var postsTotal: Int
    
    init(json: JSON)
    {
        if let arrayStakeholders = json["Stakeholders"].array {
            stakeholders = arrayStakeholders.map({ Stakeholder(json:$0) })
        }
        
        if let arrayProjects = json["Projects"].array {
            projects = arrayProjects.map({ Project(json:$0) })
        }
        
        if let arrayPosts = json["Posts"].array {
            posts = arrayPosts.map({ Post(json:$0) })
        }
        
        stakeholdersTotal = json["StakeholdersTotal"].intValue
        projectsTotal = json["ProjectsTotal"].intValue
        postsTotal = json["PostsTotal"].intValue
        
        super.init()
    }

}
