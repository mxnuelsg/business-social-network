//
//  ConexionReport.swift
//  SHM
//
//  Created by Definity First on 2/9/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class ConexionReport: Stakeholder
{
    var stakeholderId: Int
    
    override init(json: JSON)
    {
        self.stakeholderId = 0
        super.init(json: json)
        self.stakeholderId = json["StakeholderId"].int ?? 0
        self.PhoneNumber = json["PhoneNumber"].string ?? ""
        
        if self.jobPosition?.value == ""
        {
            let keyValue = KeyValueObject()
            keyValue.value = json["Position"].string ?? ""
            keyValue.valueEs = keyValue.value
            self.jobPosition = keyValue
        }
    }
}
