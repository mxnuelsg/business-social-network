//
//  WhoLevel_II.swift
//  SHM
//
//  Created by Manuel Salinas on 10/4/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class WhoLevel_II: WhoLevels
{
    var whoLocal: [WhoLevel_III]
    
    override init(json: JSON)
    {
        
        if let whosLocal =  json["WhoLocal"].array {
            
            self.whoLocal = whosLocal.map({ WhoLevel_III(json:$0) })
        }
        else
        {
            self.whoLocal = []
        }
        
        super.init(json: json)
    }
    
    override init()
    {
        self.whoLocal = []
        
        super.init()
    }
    
    override func getWSObject() -> [String : Any] {
        var wsDict = super.getWSObject()
        wsDict +=  [
            "WhoLocal" : self.whoLocal.map({$0.id})
        ]
        
        return wsDict
    }
}
