//
//  StakeholderBySite.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 8/11/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

/*
 This class is meant to use for the ContactSearchViewController when displaying stakeholders.
 This happens when property: isSearchingSH == true
 
 The initializer of this class receives an array of stakeholders and orders it by Follow status - region:
 
 
 Follow Status: Following                        [Section]
 Region....
 stakeholders......(alphabetical order)      [Rows]
 
 Follow Status: Not Following                    [Section]
 Region....
 stakeholders......(alphabetical order)          [Rows]
 
 
 The stakeholders are stored in arrayStakeholdersBySiteAndFollowStatus which will be used by ContactSearchViewController
 to display results of a search
 */
import UIKit
enum SiteSubtitle:String
{
    case Following = "  ✔︎ Following"
    case NotFollowing = "  Not Following"
}

class StakeholderBySite: Stakeholder
{
    var arraySectionTitles = [NSMutableAttributedString]()
    var arrayRegions = [String]()
    var arrayStakeholdersBySiteAndFollowStatus = [[Stakeholder]()]
    var stakeholders:[Stakeholder]!
    
    init(stakeholders:[Stakeholder])
    {
        self.stakeholders = stakeholders
        super.init(isDefault: true)
        self.groupStakeholdersByRegion()
    }
    
    func groupStakeholdersByRegion()
    {
        //clear arrays
        self.clear()
        
        //Get the regions present at current stakeholder list
        for stakeholder in self.stakeholders
        {
            if let siteTitle = stakeholder.site?.title, self.arrayRegions.contains(siteTitle) == false {
                
                self.arrayRegions.append(siteTitle)
            }
        }
        
        //Divide the Stakeholders By FollowStatus
        let followedStakeholders = self.stakeholders.filter({$0.isFollow == FollowStatus.following})
        let notFollowedStakeholders = self.stakeholders.filter({$0.isFollow == FollowStatus.notFollowing})
        
        //Order the followed stakeholders
        for site in self.arrayRegions
        {
            var followedInSite = followedStakeholders.filter({$0.site?.title == site})
            followedInSite = followedInSite.sorted(by: {$0.fullName < $1.fullName})
            
            if followedInSite.count > 0
            {
                //Add ordered stakeholder array (for cells)
                self.arrayStakeholdersBySiteAndFollowStatus.append([Stakeholder]())
                self.arrayStakeholdersBySiteAndFollowStatus[self.arrayStakeholdersBySiteAndFollowStatus.count-1] =  followedInSite
                //Add a title description of this array (for section)
                let sectionTitleFollow = NSMutableAttributedString(string: "\(SiteSubtitle.Following.rawValue.localized())\n  ⚑ \(site)")
                sectionTitleFollow.setColor(SiteSubtitle.Following.rawValue.localized(), color: UIColor.blackAsfalto())
                self.arraySectionTitles.append(sectionTitleFollow)
            }
        }
        
        //Order the NOT Followed stakeholders
        for site in self.arrayRegions
        {
            var notFollowedInSite = notFollowedStakeholders.filter({$0.site?.title == site})
            notFollowedInSite = notFollowedInSite.sorted(by: {$0.fullName < $1.fullName})
            
            if notFollowedInSite.count > 0
            {
                //At ordered stakeholder array (for cells)
                self.arrayStakeholdersBySiteAndFollowStatus.append([Stakeholder]())
                self.arrayStakeholdersBySiteAndFollowStatus[self.arrayStakeholdersBySiteAndFollowStatus.count-1] =  notFollowedInSite
                //At a title description of this array (for section)
                let sectionTitleNotFollow = NSMutableAttributedString(string: "\(SiteSubtitle.NotFollowing.rawValue.localized())\n    ⚑ \(site)")
                sectionTitleNotFollow.setColor(SiteSubtitle.NotFollowing.rawValue.localized(), color: UIColor.blackAsfalto())
                self.arraySectionTitles.append(sectionTitleNotFollow)
            }
        }
    }
    
    
    func updateStakeholders(_ stakeholders:[Stakeholder])
    {
        self.stakeholders = stakeholders
        self.groupStakeholdersByRegion()
    }
    
    func isEmpty() -> Bool
    {
        return self.arrayStakeholdersBySiteAndFollowStatus.count == 0 ? true:false
    }
    
    func clear()
    {
        self.arraySectionTitles.removeAll()
        self.arrayRegions.removeAll()
        self.arrayStakeholdersBySiteAndFollowStatus.removeAll()
    }
}
