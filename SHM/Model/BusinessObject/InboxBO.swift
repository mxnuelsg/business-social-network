//
//  InboxBO.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 9/21/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class InboxBO: BO
{
    func getInbox(parameters: [String : Any]?, onSuccess:@escaping (_ messages: [InboxMessage], _ paging: Paging?) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetInbox,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        if let jsonMessages = jsonData["Messages"].array {
                            
                            let pagingJSON = jsonData["PagingInfo"]
                            let paging = Paging(json: pagingJSON)
                            
                            let messages = jsonMessages.map({ InboxMessage(json: $0) })
                            onSuccess(messages, paging)
                        }
                        else
                        {
                            onSuccess([], nil)
                        }
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        }
        )
    }
    
    func getInboxConversation(parameters: [String : Any]?, onSuccess:@escaping (_ messages: [InboxMessage]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetInboxRelated,
                       methodType: .post,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        var msgs = [InboxMessage]()
                        
                        if let jsonMessages = jsonData["Messages"].array {
                            
                            msgs = jsonMessages.map({ InboxMessage(json: $0) })
                        }
                        
                        onSuccess(msgs)
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        }
        )
    }
    
    func postNewInboxMessage(_ body: String, subject: String, recipients: [Member], expiration:(Int), onSuccess:@escaping (_ message: InboxMessage) -> (), onError: @escaping (_ error: NSError) -> ()) {
        requestService(Constants.RemoteService.NewMessage,
                       methodType: .post,
                       parameters: [
                        "Body" : body,
                        "Subject" : subject,
                        "Recipients" : recipients.map({ $0.getWSObject() }),
                        "Expiration" : expiration
            ],
                       onSuccess: { jsonData in
                        
                        onSuccess(InboxMessage(json: jsonData))
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    func replyMessage(_ message: InboxMessage, onSuccess:@escaping (_ message: InboxMessage) -> (), onError: @escaping (_ error: NSError) -> ()) {
        requestService(Constants.RemoteService.ReplyMessage,
                       methodType: .post,
                       parameters: message.getWSObject(),
                       onSuccess: { jsonData in
                        
                        onSuccess(InboxMessage(json: jsonData))
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    func markasUnRead(parameters: [String : Any]?, onSuccess:@escaping () -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.InboxMarkAsUnRead,
                       methodType: .post,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        onSuccess()
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        }
        )
    }
    
    func getInboxDetail(_ messageId: Int, onSuccess:@escaping (_ message: InboxMessage) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetInboxDetail + "/\(messageId)",
            methodType: .get,
            parameters: nil,
            onSuccess: { jsonData in
                
                onSuccess(InboxMessage(json: jsonData))
                
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        }
        )
    }
    
    func deleteInboxMessage(_ messageId: Int, onSuccess:@escaping (_ message: InboxMessage) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.DeleteInboxMessage + "/\(messageId)",
            methodType: .delete,
            parameters: nil,
            onSuccess: { jsonData in
                
                onSuccess(InboxMessage(json: jsonData))
                
        },  onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        }
        )
    }
    
    func getInboxRecipientsByText(_ text: String, onSuccess:@escaping (_ members: [Member]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        let charsAllowed = CharacterSet(charactersIn: " /+=\n").inverted
        let urlEncodedText = text.addingPercentEncoding(withAllowedCharacters: charsAllowed)!
        
        requestService(Constants.RemoteService.GetRecipients + "/\(urlEncodedText)",
            methodType: .get,
            parameters: nil,
            onSuccess: { jsonData in
                
                print(jsonData)
                var recipients = [Member]()
                
                if let recipientsJSON = jsonData.array {
                    
                    recipients = recipientsJSON.map({ Member(json: $0) })
                }
                
                onSuccess(recipients)
                
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        }
        )
    }
    
    ///Get count of unread messages in Inbox
    func getCountForInbox(_ onSuccess:@escaping (_ count: Int) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetUnreadInbox,
                       methodType: .get,
                       parameters: nil,
                       onSuccess: {  jsonData in
                        
                        var unRead = 0
                        
                        if let unreadInboxCount = jsonData.int {
                            
                            unRead =  unreadInboxCount
                        }
                        
                        onSuccess(unRead)
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    func getExpirationDays(_ onSuccess:@escaping (_ expirations: [KeyValueObject]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetExpirationDays,
                       methodType: .get,
                       parameters: nil,
                       onSuccess: { jsonData in
                        
                        var expirations = [KeyValueObject]()
                        
                        if let jsonExpirations = jsonData.array {
                            
                            expirations = jsonExpirations.map({ KeyValueObject(json: $0) })
                        }
                        
                        onSuccess(expirations)
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            guard error.code != -999 else {
                //-999 is the code for canceled request
                return
            }            
            onError(error)
        })
    }
    
    
}
