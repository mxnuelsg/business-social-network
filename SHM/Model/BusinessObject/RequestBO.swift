//
//  RequestBO.swift
//  SHM
//
//  Created by Manuel Salinas on 6/2/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import Alamofire

class RequestBO: BO
{
    //MARK: GET MEMBER RELATIONS
    func getRequestToPublish(requestId: Int, onSuccess:@escaping (_ request: Request) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetRequest,
                       methodType: .get,
                       parameters: ["requestId" : requestId],
                       onSuccess: { jsonData in
                        
                        let requestObject = Request(json: jsonData["Request"])
                        onSuccess(requestObject)
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    //MARK: GET MEMBER RELATIONS
    func postRequestToPublish(request: Request, onSuccess:@escaping () -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.PostRequestPublish,
                       methodType: .post,
                       parameters: request.getWSObject(),
                       onSuccess: { jsonData in
                        
                        onSuccess()
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    //MARK: CANCEL
    func cancelRequest(service: String)
    {
        Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
            tasks.forEach({task in
                if let components = task.currentRequest?.url?.pathComponents, components.count > 3 {
                    
                    let request = "\(components[1])/\(components[2])/\(components[3])"
                    if request == service
                    {
                        task.cancel()
                        print("\(service) canceled")
                    }
                }
            })
        }
    }
    
    func cancelAllNetworkRequests()
    {
        Alamofire.SessionManager.default.session.getAllTasks { (task) in
            task.forEach({$0.cancel()})
        }
    }
}
