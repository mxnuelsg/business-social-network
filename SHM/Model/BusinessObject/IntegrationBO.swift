//
//  IntegrationBO.swift
//  SHM
//
//  Created by Manuel Salinas on 10/3/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class IntegrationBO: BO
{
    //MARK: PROJECTS  - MITIGATION MEASURES
    //Get Mitigation Measures for Projects
    func getMitigationMeasures(_ parameters: [String : Any], onSuccess:@escaping (_ list: [MitigationMeasure], _ totalRecords: Int) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.postMitigations,
                       methodType: .post,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        let total = jsonData["Paging"]["TotalItems"].int ?? 0
                        var mititgations = [MitigationMeasure]()
                        
                        if let jsonMitigations = jsonData["MitigationMeasures"].array {
                            
                            mititgations = jsonMitigations.map({ MitigationMeasure(json: $0) })
                        }
                        
                        onSuccess(mititgations, total)
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    //MARK: STAKEHOLDERS - WHOS
    ///Get Whos for Stakeholders
    func getWhos(_ parameters: [String : Any], onSuccess:@escaping (_ whos_L1: [WhoLevel_I], _ whos_L2: [WhoLevel_II]) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.getWhos,
                       methodType: .post,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        var whos1 = [WhoLevel_I]()
                        var whos2 = [WhoLevel_II]()
                        
                        if let jsonMitigations = jsonData["Whos"].array {
                            
                            whos1 = jsonMitigations.map({ WhoLevel_I(json: $0) })
                        }
                        
                        if let jsonMitigations = jsonData["WhoLevel11Sorted"].array {
                            
                            whos2 = jsonMitigations.map({ WhoLevel_II(json: $0) })
                        }
                        
                        onSuccess(whos1, whos2)
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
}
