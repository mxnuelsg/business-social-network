//
//  CalendarBO.swift
//  SHM
//
//  Created by Manuel Salinas on 10/16/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class CalendarBO: BO
{
    //MARK: CALENDAR DATA
    func getCalendarData(_ datePage: [String : Any], onSuccess: @escaping (_ dateList: [DateList], _ latestDate: String, _ page: Int, _ loadMore: Bool) ->(), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetCalendar,
                       methodType: .get,
                       parameters: datePage,
                       onSuccess: { jsonData in
                        
                        if let jsonDates = jsonData["Dates"].array, let lastDate = jsonData["LatestDate"].string, let pag = jsonData["Page"].int, let loadMorePages = jsonData["IsPagingEnabled"].int
                        {
                            let dateList = jsonDates.map({ DateList(json: $0) })
                            //Sorting ...
                            let dateListSorted = dateList.sorted(by: {
                                $0.date.compare($1.date) == ComparisonResult.orderedAscending
                            })
                            onSuccess(dateListSorted, lastDate, pag, Bool(loadMorePages as NSNumber))
                        }
                        else
                        {
                            onSuccess( [],  "",  0,  false)
                        }
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    //MARK: CALENDAR DATA BY ID
    func getCalendarDataById(_ datePage: [String : Any], onSuccess: @escaping (_ dateList: [DateList], _ latestDate: String, _ page: Int, _ loadMore: Bool) ->(), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetCalendarById,
                       methodType: .get,
                       parameters: datePage,
                       onSuccess: { jsonData in
                        
                        if let jsonDates = jsonData["Dates"].array, let lastDate = jsonData["LatestDate"].string, let pag = jsonData["Page"].int, let loadMorePages = jsonData["IsPagingEnabled"].int
                        {
                            let dateList = jsonDates.map({ DateList(json: $0) })
                            
                            //Sorting ...
                            let dateListSorted = dateList.sorted(by: {
                                $0.date.compare($1.date) == ComparisonResult.orderedAscending
                            })
                            
                            onSuccess(dateListSorted, lastDate, pag, Bool(loadMorePages as NSNumber))
                        }
                        else
                        {
                            onSuccess([],  "",  0,  false)
                        }
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    //MARK: CALENDAR ALL EVENTS
    func getAllEvents(_ datePage: [String : Any], onSuccess: @escaping (_ items: [CalendarItem]) ->(), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetCalendarAllEvents,
                       methodType: .get,
                       parameters: datePage,
                       onSuccess: { jsonData in
                        
                        var items = [CalendarItem]()
                        if let jsonDates = jsonData.array {
                            
                            items = jsonDates.map({ CalendarItem(json: $0) })
                        }
                        
                        onSuccess(items)
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    //MARK: CALENDAR EVENTS
    func getEvents(_ datePageById: [String : Any], onSuccess:@escaping (_ items: [CalendarItem]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetCalendarEvents,
            methodType: .get,
            parameters: datePageById,
            onSuccess: { jsonData in
                
                var items = [CalendarItem]()
                if let jsonDates = jsonData.array {
                    
                    items = jsonDates.map({ CalendarItem(json: $0) })
                }
                
                  onSuccess(items)
                
            }) { error in
                
                print(error.localizedDescription)
                onError(error)
        }
    }
}
