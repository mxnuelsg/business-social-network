//
//  PushNotificationsBO.swift
//  SHM
//
//  Created by Manuel Salinas on 10/27/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PushNotificationsBO: BO
{
    ///Subscribe to Push Notifications
    func registerDevice(_ token: String, onSuccess:@escaping () ->(), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.PostRegisterDevice,
                       methodType: .post,
                       parameters: "\"\(token)\"",
            onSuccess: { jsonData in
                
                onSuccess()
                
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    ///Unsubscribe to Push Notifications
    func unregisterDevice(_ token: String, onSuccess:@escaping () ->(), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.PostUnregisterDevice,
                       methodType: .post,
                       parameters: "\"\(token)\"",
            onSuccess: { jsonData in
                
                onSuccess()
                
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    ///Reset remote badge icon
    func resetBadgeIcon()
    {
        if let token = UserDefaults.standard.string(forKey: Constants.UserKey.DeviceToken), token.isEmpty == false  {
            
            requestService(Constants.RemoteService.PostResetBadgeIcon,
                           methodType: .post,
                           parameters: "\"\(token)\"",
                onSuccess: { jsonData in
                    //nothing
            }) { error in
                //nothing
            }
        }
    }
}
