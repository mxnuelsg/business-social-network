//
//  ActorBO.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/4/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class StakeholderBO: BO
{
    var actorsFollowed = Set<Int>()
    var actorsTryingToFollow = Set<Int>()
    
    func getAllStakeholders(followStatus: FollowStatus, onSuccess:@escaping (_ stakeholders: [Stakeholder]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetAllStakeholders,
                       methodType: .get,
                       parameters: ["FollowingStatus" : followStatus.rawValue],
                       onSuccess: { jsonData in
                        
                        var stakeholders = [Stakeholder]()
                        if let jsonStakeholders = jsonData.array {
                            
                            stakeholders = jsonStakeholders.map({ Stakeholder(json: $0) })
                        }
                        
                        onSuccess(stakeholders)
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    func getStakeholders(parameters: [String : Any]?, onSuccess:@escaping (_ stakeholders: [Stakeholder], _ sites: [Site], _ totalRecords: Int) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetStakeholderList,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        var stakeholders = [Stakeholder]()
                        var sites = [Site]()
                        let totalItems = jsonData["TotalItems"].int ?? 0
                        
                        if let jsonStakeholders = jsonData["Stakeholders"].array {
                            
                            stakeholders = jsonStakeholders.map({ Stakeholder(json: $0) })
                        }
                        
                        if let jsonSites = jsonData["SiteMappings"].array {
                            
                            sites = jsonSites.map({ Site(json: $0) })
                        }
                        
                        onSuccess(stakeholders, sites, totalItems)
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    func getStakeholdersByProject(_ projectId: Int, onSuccess:@escaping (_ stakeholders: [Stakeholder]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        var parameters:[String:Any] = [:]
        parameters["projectId"] = projectId
        requestService(Constants.RemoteService.GetStakeholdersByProject,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        var stakeholders = [Stakeholder]()

                        if let jsonStakeholders = jsonData.array {
                            
                            stakeholders = jsonStakeholders.map({ Stakeholder(json: $0) })
                        }
                                               
                        onSuccess(stakeholders)
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    func getStakeholdersLight(onSuccess:@escaping (_ stakeholders: [Stakeholder]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetStakeholderLight,
                       methodType: .get,
                       parameters: nil,
                       onSuccess: { jsonData in
                        
                        var stakeholders = [Stakeholder]()
                        if let jsonStakeholders = jsonData.array {
                            
                            stakeholders = jsonStakeholders.map({ Stakeholder(json: $0) })
                        }
                        
                        onSuccess(stakeholders)
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    //MARK: GET PROJECTS
    func getStakeholdersProjects(_ stakeholderId:Int, onSuccess:@escaping (_ projects: [Project]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.StakeholderProjects,
                       methodType: .get,
                       parameters: [ "StakeholderId" : stakeholderId],
                       onSuccess: { jsonData in
                        
                        var projects = [Project]()
                        if let json = jsonData.array {
                            
                            projects = json.map({ Project(json: $0) })
                        }
                        onSuccess(projects)
        },onError: { error in
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    //MARK: GET RELATIONS
    func getStakeholdersRelations(_ stakeholderId:Int, onSuccess:@escaping (_ stakeholders: [ContactRelation]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.StakeholderRelations,
                       methodType: .get,
                       parameters: [ "StakeholderId" : stakeholderId],
                       onSuccess: { jsonData in
                        
                        var relations = [ContactRelation]()
                        if let json = jsonData.array {
                            
                            relations = json.map({ ContactRelation(json: $0) })
                        }
                        onSuccess(relations)
        },onError: { error in
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    //MARK: GET GEOGRAPHIES
    func getStakeholdersGeographies(_ stakeholderId:Int,onSuccess:@escaping (_ geographies: [ClusterItem]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
    
        requestService(Constants.RemoteService.StakeholderGeographies,
                       methodType: .get,
                       parameters: [ "StakeholderId" : stakeholderId],
                       onSuccess: { jsonData in
    
                        var geographies = [ClusterItem]()
                        if let json = jsonData.array {
                            geographies = json.map({ ClusterItem(json: $0) })
                        }
                        onSuccess(geographies)
        },onError: { error in
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    //MARK: FOLLOW & UNFOLLOW
    func followStakeholder(_ stakeholder: Stakeholder, onSuccess: @escaping () -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.FollowStakeholder,
                       methodType: .post,
                       parameters: "\(stakeholder.id)",
            onSuccess: { jsonData in
                
                onSuccess()
                
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    func unfollowStakeholder(_ stakeholder: Stakeholder, onSuccess: @escaping () -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.UnfollowStakeholder,
                       methodType: .post,
                       parameters: "\(stakeholder.id)",
            onSuccess: { jsonData in
                
                onSuccess()
                
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    //MARK: ENABLE & DISABLE
    func enableStakeholder(_ stakeholder: Stakeholder, onSuccess: @escaping () -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.EnableStakeholder,
                       methodType: .post,
                       parameters: "\(stakeholder.id)",
            onSuccess: { jsonData in
                
                onSuccess()
                
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    func disableStakeholder(_ stakeholder: Stakeholder, onSuccess: @escaping () -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.DisableStakeholder,
                       methodType: .post,
                       parameters: "\(stakeholder.id)",
            onSuccess: { jsonData in
                
                onSuccess()
                
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    //MARK: CAN FOLLOW
    func canFollowActor(_ idActor: Int) -> Bool
    {
        return actorsFollowed.contains(idActor) == false && actorsTryingToFollow.contains(idActor) == false
    }
    
    //MARK: DETAIL
    func getStakeholderDetail(_ stakeholderId: Int, onSuccess: @escaping (_ stakeholder: Stakeholder?) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetActorDetail,
                       methodType: .get,
                       parameters: [ "StakeholderId" : stakeholderId],
                       onSuccess: { jsonData in
                        
                        guard let _ = jsonData.dictionary else {
                            onSuccess(nil)
                            return
                        }
                        
                        onSuccess(Stakeholder(json: jsonData))
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    func getStakeholderDetailLight(_ stakeholderId: Int, onSuccess: @escaping (_ stakeholder: Stakeholder) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.StakeholderDetailLight,
                       methodType: .get,
                       parameters: [ "StakeholderId" : stakeholderId],
                       onSuccess: { jsonData in
                        
                        guard let _ = jsonData.dictionary else {
                            onSuccess(Stakeholder(isDefault:true))
                            return
                        }
                        
                        onSuccess(Stakeholder(json: jsonData) ?? Stakeholder(isDefault:true))
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    func searchStakeholder(_ searchTerm: String, onSuccess:@escaping (_ stakeholders: [Stakeholder]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetStakeholderList,
                       methodType: .get,
                       parameters: [
                        "FollowingStatus" : FollowStatus.all.rawValue,
                        "LetterToSearch" : searchTerm],
                       onSuccess: { jsonData in
                        
                        var stakeholders = [Stakeholder]()
                        if let jsonContacts = jsonData.array {
                            
                            stakeholders = jsonContacts.map({ Stakeholder(json: $0) })
                        }
                        
                        onSuccess(stakeholders)
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    func saveStakeholder(_ stakeholder: Stakeholder, onSuccess:@escaping (_ stakeholder: Stakeholder) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.PostNewStakeholder,
                       methodType: .post,
                       parameters: stakeholder.getWSObject(),
                       onSuccess: { jsonData in
                        
                        let stakeholder = Stakeholder(json: jsonData)
                        onSuccess(stakeholder)
                        
        }) { error in
                        
            onError(error)
        }
    }
    
    ///This method gets Default data to AddNEwStakeholder process
    func getStakeholderDefaults(onSuccess:@escaping (_ defaultData: StakeholderDefaultData) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetStakeholderDefaultData,
                       methodType: .get,
                       parameters: [:],
                       onSuccess: { jsonData in
                        
                        let StakeDefaults = StakeholderDefaultData(json: jsonData)
                        onSuccess(StakeDefaults)
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    func requestUpdate(stakeholderId: Int, comments: String, isAnonymous: Bool, onSuccess:@escaping () -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.StakeholderRequestUpdate,
                       methodType: .post,
                       parameters: [
                        "Comments":comments,
                        "StakeholderId":stakeholderId,
                        "IsAnonymous":isAnonymous],
                       onSuccess: { jsonData in
                        
                        onSuccess()
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    func filterStakeholders(_ stakeholders: [Stakeholder], byText text: String) -> [Stakeholder]
    {
        let stakeholdersFiltered = stakeholders.filter({
            ($0.fullName as NSString).localizedCaseInsensitiveContains(text)
        })
        
        return stakeholdersFiltered
    }
    
    func filterRelations(_ relations: [ContactRelation], byText text: String) -> [ContactRelation]
    {
        let relationsFiltered = relations.filter({
            ($0.fullName as NSString).localizedCaseInsensitiveContains(text)
        })
        
        return relationsFiltered
    }
    
    //MARK: EDIT INFO
    func createStakeholderRequest(stakeholderId: Int, onSuccess:@escaping (_ stakeholder: Stakeholder) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.PostCreateRequestForEdit,
                       methodType: .post,
                       parameters: "\(stakeholderId)",
            onSuccess: { jsonData in
                
                let sh = Stakeholder(json: jsonData)
                onSuccess(sh)
                
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    func editStakeholder(stakeholderId: Int, onSuccess:@escaping (_ stakeholder: Stakeholder) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetStakeholderEdit,
                       methodType: .get,
                       parameters: [
                        "StakeholderId" : stakeholderId ],
                       onSuccess: { jsonData in
                        
                        print(jsonData)
                        let sh = Stakeholder(json: jsonData)
                        onSuccess(sh)
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    func getFeedEditingStakeholder(parameters: [String : Any]?, onSuccess:@escaping (_ feed: FeedStakeholderEdit) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetFeedEditingStakeholder,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        let feed = FeedStakeholderEdit(json: jsonData)
                        onSuccess(feed)
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        } )
    }
    
    //MARK: SAVE EDIT STAKEHOLDER
    func saveEditStakeholder(_ stakeholder: Stakeholder, onSuccess:@escaping (_ stakeholder: Stakeholder) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.PutSaveStakeholderEdited,
                       methodType: .put,
                       parameters: stakeholder.getWSObject(),
                       onSuccess: { jsonData in
                        
                        let sh = Stakeholder(json: jsonData)
                        onSuccess(sh)
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    //MARK: RATE STAKEHOLDER
    func rateStakeholder(request: RateRequest,onSuccess:@escaping (_ stakeholder: RateRequest) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.rateStakeholder,
                       methodType: .post,
                       parameters: request.getWSObject(),
                       onSuccess: { jsonData in
                        
                        let rate = RateRequest(json:"RateRequest")
                        onSuccess(rate)
                        MessageManager.shared.showBar(title: "Your rate has been upload".localized(), subtitle: String(), type: .success, containsIcon: false, fromBottom: false)
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
            MessageManager.shared.showBar(title: "There has been a problem, please try later".localized(), subtitle: String(), type: .error, containsIcon: false, fromBottom: false)
        })
    }
}
