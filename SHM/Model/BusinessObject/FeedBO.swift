//
//  FeedBO.swift
//  SHM
//
//  Created by Manuel Salinas on 8/6/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class FeedBO: BO
{
    //MARK: PUBLIC NEW POST
    func publicNewPost(parameters: [String : Any]?, onSuccess:@escaping () -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.NewPost,
                       methodType: .post,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        onSuccess()
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    //MARK: POSTS LIST
    func getFeed(onSuccess:@escaping (_ posts: [Post]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetFeed,
                       methodType: .get,
                       parameters: nil,
                       onSuccess: { jsonData in
                        if let jsonActors = jsonData["Feeds"].array
                        {
                            let postsList = jsonActors.map({ Post(json: $0) })
                            onSuccess(postsList)
                        }
                        else {
                            onSuccess([])
                        }
        }) { error in
            
            print(error.localizedDescription)
            guard error.code != -999 else {
                //-999 is the code for canceled request
                return
            }
            onError(error)
        }
    }
    
    //MARK: POST DETAIL
    func getPostDetail(parameters: [String : Any]?, onSuccess:@escaping (_ postDetail: Post?) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetPostDetail,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        guard let _ = jsonData.dictionary else {
                            onSuccess( nil)
                            return
                        }
                        
                        onSuccess(Post(json: jsonData))
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    func getFeedPreviousTo(postId: Int, onSuccess:@escaping (_ posts: [Post]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetPreviousFeed,
                       methodType: .get,
                       parameters: [ "PostId":postId ],
                       onSuccess: { jsonData in
                        
                        var posts = [Post]()
                        if let jsonActors = jsonData["Feeds"].array {
                            
                            posts = jsonActors.map({ Post(json: $0) })
                        }
                        
                        onSuccess(posts)
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }


    //MARK: STAKEHOLDER POSTS
    func getStakeholderFeed(_ stakeholder: Stakeholder, onSuccess:@escaping (_ posts: [Post]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetFeed,
                       methodType: .get,
                       parameters: ["StakeholderId":stakeholder.id],
                       onSuccess: {  jsonData in
                        
                        var posts = [Post]()
                        if let jsonActors = jsonData["Feeds"].array {
                            
                            posts = jsonActors.map({ Post(json: $0) })
                        }
                        
                        onSuccess(posts)
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    //MARK: PROJECT POSTS
    func getProjectFeed(_ project: Project, onSuccess:@escaping (_ posts: [Post]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetFeed,
                       methodType: .get,
                       parameters: ["ProjectId": project.id],
                       onSuccess: { jsonData in
                        
                        var posts = [Post]()
                        if let jsonActors = jsonData["Feeds"].array {
                            
                            posts = jsonActors.map({ Post(json: $0) })
                        }
                        
                        onSuccess(posts)
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    func filterPosts(_ posts: [Post], byText text: String) -> [Post]
    {
        let postsFiltered = posts.filter({
            findInPost($0, text: text) })
        
        return postsFiltered
    }
    
    func findInPost(_ post: Post, text: String) -> Bool
    {
        return (post.title as NSString).localizedCaseInsensitiveContains(text) ||
            LibraryAPI.shared.projectBO.filterProjects(post.projects, byText: text).count > 0 ||
            LibraryAPI.shared.stakeholderBO.filterStakeholders(post.stakeholders, byText: text).count > 0
    }
    
    //MARK: FILED POST
    func filedPost(postId: Int, onSuccess:@escaping (_ post: Post) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.FiledPost,
                       methodType: .post,
                       parameters: String(postId),
                       onSuccess: { jsonData in
                        
                        let postFiled = Post(json: jsonData)
                        onSuccess(postFiled)
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
}

class FeedbackBO: BO
{
    func postFeedback(_ params: [String : Any], onSuccess: @escaping () ->(), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.PostFeedback,
                       methodType: .post,
                       parameters: params,
                       onSuccess: { jsonData in
                        
                        onSuccess()
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
}
