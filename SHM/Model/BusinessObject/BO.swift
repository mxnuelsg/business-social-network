//
//  BO.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/4/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class BO: NSObject
{
    //MARK: Global Variables
    var alamofireManager: Alamofire.SessionManager?
    var logoutAlert:UIAlertController?
    
    //MARK: Headers
    static var SecurityHeaders: [String: String]?
    static let DefaultHeaders: [String: String] = [
        "User-Agent":"Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile"
    ]
    class var LoginHeaders: [String: String]
    {
        let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let cemexToken = UserDefaultsManager.getJWT() ?? String()
        
        var headers = BO.DefaultHeaders
        headers += [
            "Content-Type":"application/x-www-form-urlencoded",
            "Cookie":"Culture=en; ASP.NET_SessionId=",
            "Culture": language,
            "VersionMobile":appVersion
        ]
        headers["jwt"] = cemexToken
        
        return headers
    }
    
    class var RequestHeaders: [String: String]
    {
        let language = UserDefaults.standard.value(forKey: Constants.UserKey.BackupLanguage) as? String ?? "en"
        var headers = [String:String]()
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        headers += BO.DefaultHeaders
        headers += [
            "Accept":"application/json, text/plain, */*",
            "Accept-Encoding":"gzip, deflate, sdch",
            "Accept-Language":"en-US,en;q=0.8,es;q=0.6",
            "Connection":"keep-alive",
            "Content-Type":"application/json",
            "Culture": language ,
            "VersionMobile":appVersion
        ]
        //For new cemex authentication
        headers["jwt"] = UserDefaultsManager.getJWT() ?? String()
        headers["u"] = LibraryAPI.shared.currentUser?.u ?? String()
        
        //Security headers
        if let securityHeaders = BO.SecurityHeaders {
            headers += securityHeaders
        }
        else {
            print("WARNING: TRYING TO MAKE A REQUEST WITHOUT SECURITY HEADERS")
        }
        return headers
    }
    
    //MARK: Custom String Enconding
    //Encoding for post request when follow/unfollow stakeholders and projects
    struct StringEnconding: ParameterEncoding
    {
        private let stringParameter: String
        init(stringParameter:String)
        {
            self.stringParameter = stringParameter
        }
        func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest
        {
            var urlRequest = try urlRequest.asURLRequest()
            urlRequest.httpBody = stringParameter.data(using: .utf8, allowLossyConversion: false)
            return urlRequest
        }
    }
    
    //MARK: INIT
    override init()
    {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = 90
        alamofireManager = Alamofire.SessionManager(configuration: configuration, serverTrustPolicyManager: nil)
    }
    
    //MARK: REQUEST SERVICE FUNCTIONS
    func requestService(_ service: String, methodType: Alamofire.HTTPMethod, parameters: Any?, onSuccess: @escaping (_ jsonData: JSON) -> (), onError: @escaping (NSError) -> ())
    {
        self.requestServiceHelper(service: service, methodType: methodType, parameters: parameters, onSuccess: onSuccess, onError: onError)
    }
    
    private func requestServiceHelper(service: String, methodType: Alamofire.HTTPMethod, parameters: Any?, isRefresh: Bool = false, onSuccess: @escaping (_ jsonData: JSON) -> (), onError: @escaping (NSError) -> ())
    {
        //Construct URL
        var urlString = UserDefaultsManager.getDefaultServer()
        urlString += service
        let url = URL(string: urlString)!        
                
        //Construct Parameters
        var parameterEncoding: ParameterEncoding? = nil
        var parametersDict: [String: Any]? = nil
        if let existingParameters = parameters as? [String: Any] {
            
            parameterEncoding = methodType == Alamofire.HTTPMethod.get ? URLEncoding.default : JSONEncoding.default
            parametersDict = existingParameters
        }
        else if let plainText = parameters as? String {
            
            let stringEncode = StringEnconding(stringParameter: plainText)
            parameterEncoding = stringEncode
            parametersDict = [:]
        }
        else
        {
            //If the parameter is not a dictionary or string encode a Json default
            print("*** Parameters is not a Dictionary or String ***")
            parametersDict = nil
            parameterEncoding = JSONEncoding.default
        }
        
        print("HEADERS: \(BO.RequestHeaders)")
        print("requestingService ((\(methodType)): \(url))")
        print("parameters: \(parameters ?? String())")
        
        //Request service
        Alamofire.request(url, method: methodType, parameters: parametersDict, encoding: parameterEncoding!, headers: BO.RequestHeaders).responseJSON { (response) -> Void in
            
            guard response.result.error == nil else {
                
                if response.response?.statusCode == 409
                {
                    self.logoutAlertAction()
                }
                print("FAILURE!!!!")
                onError(response.result.error! as NSError)
                return
            }
            //Verify json data response is not empty
            guard let jsonValue = response.result.value else {
                print("FAILURE!!!! nil response!!!")
                onError(NSError(domain: Constants.ErrorDomain, code: Constants.ErrorCode.IntegrityDataError, userInfo: ["Error":"FAILURE!!!! nil response!!!"]))
                return
            }
            
            let swifty = JSON(jsonValue)
            
            print("RESPONSE FROM SERVICE \(service)")
            print(swifty)
            print("ALL OK!!")
            
            // check if responseCode exists and is equal to 0 (the 'OK' Code)
            if swifty["ResponseCode"].int ?? -1 == 0
            {
                if let _ = swifty["Data"].error {
                    // 'data' Node not found, this is OK, in case data note is not needed
                    onSuccess(JSON([:]))
                }
                else {
                    // 'data' Node exists, and it should be used wisely
                    onSuccess(swifty["Data"])
                }
            }
            else if swifty["ResponseCode"].int ?? -1 == 2
            {
                //ResponseCode == 2 means the cemex token has expired, user needs to logout
                NotificationCenter.default.post(name: Notification.Name(rawValue: "performLogout"), object: nil)                
            }
            else
            {
                guard  service != Constants.RemoteService.PostNewStakeholder else {
                    let errorDict:[AnyHashable: Any] = [NSLocalizedDescriptionKey: swifty["ResponseMessage"]]
                    onError(NSError(domain: Constants.ErrorDomain, code: Constants.ErrorCode.ParsingDataError, userInfo: errorDict))
                    return
                }
            
                onError(NSError(domain: Constants.ErrorDomain, code: Constants.ErrorCode.ParsingDataError, userInfo: swifty["ResponseCode"].error?.userInfo))
            }
        }
    }
    
    func requestSession(_ service: String, parameters: [String: Any]?, onSuccess: @escaping (_ loginResponse: [String: Any]) -> (), onError: @escaping (NSError) -> ())
    {
        guard LibraryAPI.shared.offlineModeEnabled == false else {
            print("OFFLINE MODE")
            
            let defaultResponse: [String: Any] = [
                "Cookie": "XX",
                "Username": "XX",
                "Password": "XX",
                "AccountId": 50,
                "Role": 5,
                "Language": 1,
                "StakeholderOrder" : 1,
                "ProjectOrder" : 1]
            
            onSuccess(defaultResponse)
            
            return
        }
        
        disallowRedirect()
        
        let url = URL(string: UserDefaultsManager.getDefaultServer() + service)!
        
        print("LOGIN HEADERS HEADERS")
        print(BO.LoginHeaders)
        
        //Encode Parameters
        let parametersEncodedAndDictionary = self.encodeParameters(methodType: .post, parameters: parameters)
        let parameterEncoding: ParameterEncoding? = parametersEncodedAndDictionary.0
        Alamofire.request(url, method: .post, parameters: parameters, encoding: parameterEncoding!, headers: BO.RequestHeaders).responseData { (response) -> Void in
            
            //Get User Site Mappings from response
            if let jsonValue = response.result.value {
                
                let jsonData = JSON(jsonValue)
                
                let jsonSiteMappings = jsonData["Element"]["SiteMappings"]
                
                if let siteMappingsArray = jsonSiteMappings.array {
                    
                    let siteMappings = siteMappingsArray.map({Site(json:$0)})
                    print(siteMappings.count)
                }
            }
            
            guard response.response != nil && (response.response?.statusCode == 204 || response.response?.statusCode == 200) else {
                
                //Validation for incompatibility between app and server versions
                if response.response?.statusCode == 409
                {
                    var urlToDownloadVersion = String()
                    
                    if let versionUrl = response.response?.allHeaderFields["w"] as? String {
                        urlToDownloadVersion = versionUrl
                    }
                    
                    //In case of incompatibility versions an alert will show up
                    onError(NSError(domain: Constants.ErrorDomain, code: 409, userInfo: ["Message":"Request Session Failed \(response.result.error)", Constants.UserKey.UrlVersion : urlToDownloadVersion]))
                    
                    return
                }
                else // status code: 412 means credentials are wrong
                {
                    print("FAILURE!!!! \(response.result.error)")
                    onError(response.result.error as NSError? ?? NSError(domain: Constants.ErrorDomain, code: -6661, userInfo: ["Message":"Request Session Failed \(response.result.error)"]))
                    
                    return
                }
            }
            print("RESPONSE HEADERS: \(response.response?.allHeaderFields)")
            
            guard let cookie = response.response?.allHeaderFields["Set-Cookie"] as? String else {
                
                onError(NSError(domain: Constants.ErrorDomain, code: 222, userInfo: ["Message":"Request Session came with NO COOKIES!!"]))
                return
            }
            
            guard let username = response.response?.allHeaderFields["u"] as? String else {
                onError(NSError(domain: Constants.ErrorDomain, code: 222, userInfo: ["Message":"Request Session came with NO username"]))
                return
            }
            
            guard let password = response.response?.allHeaderFields["p"] as? String else {
                onError(NSError(domain: Constants.ErrorDomain, code: 222, userInfo: ["Message":"Request Session came with NO password"]))
                return
            }
            
            guard let accountId = response.response?.allHeaderFields["aid"] as? String else {
                onError(NSError(domain: Constants.ErrorDomain, code: 222, userInfo: ["Message":"Request Session came with NO aid D:"]))
                return
            }
            
            guard let roleId = response.response?.allHeaderFields["r"] as? String else {
                onError(NSError(domain: Constants.ErrorDomain, code: 222, userInfo: ["Message":"Request Session came with NO role"]))
                return
            }
            
            guard let language = response.response?.allHeaderFields["l"] as? String else {
                onError(NSError(domain: Constants.ErrorDomain, code: 222, userInfo: ["Message":"Request Session came with NO language"]))
                return
            }
            
            guard let stakeholderOrder = response.response?.allHeaderFields["sno"] as? String else {
                onError(NSError(domain: Constants.ErrorDomain, code: 222, userInfo: ["Message":"Request Session came with NO stakeholder order"]))
                return
            }
            
            guard let projectOrder = response.response?.allHeaderFields["plo"] as? String else {
                onError(NSError(domain: Constants.ErrorDomain, code: 222, userInfo: ["Message":"Request Session came with NO prpject order"]))
                return
            }
            guard let authToken = response.response?.allHeaderFields["requestverificationtoken"] as? String else {
                onError(NSError(domain: Constants.ErrorDomain, code: 222, userInfo: ["Message":"Request Session came with NO prpject order"]))
                return
            }
            
            
            //Fill Data
            let loginData: [String: Any] = [
                "Cookie": cookie,
                "Username": username,
                "Password": password,
                "AccountId": Int(accountId)!,
                "Role": Int(roleId)!,
                "Language": Int(language)!,
                "StakeholderOrder" : Int(stakeholderOrder)!,
                "ProjectOrder" : Int(projectOrder)!,
                "requestverificationtoken": authToken]
            
            onSuccess(loginData)
        }
    }
    
    //MARK: ENCODE HELPER
    private func encodeParameters(methodType: Alamofire.HTTPMethod, parameters: Any?) -> (ParameterEncoding? , [String: Any]?)
    {
        var parameterEncoding: ParameterEncoding? = nil
        var parametersDict: [String: Any]? = nil
        
        //Check if the parameters are made of a dictionary
        if let existingParameters = parameters as? [String : Any] {
            
            parameterEncoding = methodType == Alamofire.HTTPMethod.get ? URLEncoding.default : JSONEncoding.default
            parametersDict = existingParameters
        }
            //Check if the parameters are made of a String
        else if let _ /*plainText*/ = parameters as? String {
            
            parameterEncoding = URLEncoding.httpBody
            parametersDict = [:]
        }
        else
        {
            //If the parameter is not a dictionary or string encode a Json default
            print("*** Parameters is not a Dictionary or String ***")
            parametersDict = nil
            parameterEncoding = JSONEncoding.default
        }
        
        
        return (parameterEncoding, parametersDict)
    }
    
    func disallowRedirect()
    {
        Alamofire.SessionManager.default.delegate.taskWillPerformHTTPRedirection = { session, task, response, request in
            return nil
        }
    }
        
    func downloadLink(_ link: URL, downloadedTo:(URL) -> ())
    {
        /*
         Alamofire.download(Alamofire.HTTPMethod.get, link, headers: BO.RequestHeaders, destination: { (tempURL, response) -> URL in
         let directoryURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
         
         let fileName = response.suggestedFilename!
         let finalPath = directoryURL.URLByAppendingPathComponent(fileName)
         
         return finalPath!
         })
         */
    }
    
    func logoutAlertAction()
    {
        self.logoutAlert = UIAlertController(title: "Incorrect Version".localized(), message: "This app version is not compatible with version in server".localized(), preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK".localized(), style: .cancel, handler: { (UIAlertAction) in
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "logout"), object: nil)
            self.logoutAlert?.dismiss(animated: true, completion: nil)
        })
        self.logoutAlert?.addAction(okAction)
        
        if let vcRoot = UIApplication.shared.windows[0].rootViewController, let vcAlert = self.logoutAlert {
            
            //The window array at index 0 contains the instance of app's UIWindow
            vcRoot.present(vcAlert, animated: true, completion: nil)
        }
        
        print("You need to logout")
    }
}
