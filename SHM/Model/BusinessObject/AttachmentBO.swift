//
//  FileBO.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 9/10/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AttachmentBO: BO
{
    func getFilesByProjectId(_ projectId: Int, onSuccess:@escaping (_ files: [Attachment]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        getFiles(parameters: ["ProjectId": projectId], onSuccess: onSuccess,  onError: onError)
    }
    
    func getFilesByStakeholderId(_ stakeholderId: Int, onSuccess:@escaping (_ files: [Attachment]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        getFiles(parameters: ["StakeholderId" : stakeholderId], onSuccess: onSuccess, onError: onError)
    }
    
    func getFiles(parameters: [String : Any]?, onSuccess:@escaping (_ files: [Attachment]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetAttachments,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        var files = [Attachment]()
                        
                        if let wsFiles = jsonData.array {
                            
                            files = wsFiles.map({ Attachment(json: $0) })
                        }
                        
                        onSuccess(files)
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    func downloadFileFromLink(_ link: URL, onDownload: @escaping (_ url:URL) -> (), onError: @escaping (NSError) -> ())
    {
        var localPath: URL?
        let urlDestiny: DownloadRequest.DownloadFileDestination = { _, _ in
        
            var directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let fileName = link.lastPathComponent
            directoryURL = directoryURL.appendingPathComponent("Attachments/\(fileName)")
            localPath = directoryURL
            
            return (localPath!, options: [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(link, to: urlDestiny).responseData(completionHandler: { (response) -> Void in
            
            if response.result.error != nil && response.response?.statusCode != 516
            {
                onError(NSError(domain: Constants.ErrorDomain, code: 001, userInfo: [NSLocalizedDescriptionKey:"Failed to download file"]))
                return
            }
            if let path = localPath  {
            
                print("file is at \(path)")
                onDownload(path)
            }
        })
    }
    
    func delay(_ delay:Double, closure:@escaping () -> ())
    {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    func filterAttachment(_ attachments: [Attachment], byText text: String) -> [Attachment]
    {
        let attachmentsFiltered = attachments.filter({
            (($0.name.isEmpty == true ? "Attachment".localized() : $0.name) as NSString).localizedCaseInsensitiveContains(text)
        })
        
        return attachmentsFiltered
    }
    
    //MARK: UPLOAD PHOTO ATTACHMENT
    func uploadFile(fileSize: Int, fileURL: URL, onSuccess: @escaping (_ token: String) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        //Activity indicator
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        //Construct URL Request
        guard let url = self.getRequestUrl() else {
            
            //Activity Indicator
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            print("Not valid URL")
            
            return
        }
        
        //Construct parameters for javascript framework
        let flowParameters :[String: String] = self.getMultipartFlowParameters(fileSize)
        
        //Play request
        var requestUrlToUpload = URLRequest(url: url)
        requestUrlToUpload.httpMethod = HTTPMethod.post.rawValue
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append(flowParameters["flowIdentifier"]!.data(using: String.Encoding.isoLatin1)!, withName: "flowIdentifier")
            multipartFormData.append(flowParameters["flowChunkSize"]!.data(using: String.Encoding.isoLatin1)!, withName: "flowChunkSize")
            multipartFormData.append(flowParameters["flowChunkNumber"]!.data(using: String.Encoding.isoLatin1)!, withName: "flowChunkNumber")
            multipartFormData.append(flowParameters["flowFilename"]!.data(using: String.Encoding.isoLatin1)!, withName: "flowFilename")
            multipartFormData.append(flowParameters["flowRelativePath"]!.data(using: String.Encoding.isoLatin1)!, withName: "flowRelativePath")
            multipartFormData.append(flowParameters["flowTotalSize"]!.data(using: String.Encoding.isoLatin1)!, withName: "flowTotalSize")
            multipartFormData.append(flowParameters["flowTotalChunks"]!.data(using: String.Encoding.isoLatin1)!, withName: "flowTotalChunks")
            multipartFormData.append(flowParameters["flowCurrentChunkSize"]!.data(using: String.Encoding.isoLatin1)!, withName: "flowCurrentChunkSize")
            
            multipartFormData.append(fileURL, withName: "uploadFile")
        }, with: requestUrlToUpload,  encodingCompletion: { (encodingResult) in
            
            switch encodingResult
            {
            case .success(let upload, _, _):
                
                //Activity Indicator
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                //Manage Response
                upload.responseJSON(completionHandler: { response in
                    
                    let evaluation = self.evaluateValidResponse(response)
                    if evaluation.isValid == true
                    {
                        onSuccess(evaluation.token)
                    }
                    else
                    {
                        onError(evaluation.error ?? NSError())
                    }
                })
                upload.uploadProgress(closure: { progress in
                    
                    print(CGFloat(progress.fractionCompleted) * 100.0)
                })
            case .failure(let encodingError):
                
                //Activity Indicator
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                print(encodingError)
                onError(encodingError as NSError)
            }
        })
    }
    
    private func evaluateValidResponse(_ response: DataResponse<Any>) -> (isValid:Bool, error: NSError?, token: String)
    {
        guard let jsonValue = response.result.value else {
            
            let error = NSError(domain: Constants.ErrorDomain, code: Constants.ErrorCode.IntegrityDataError, userInfo: ["Error":"FAILURE!!!! nil response!!!"])
            return (isValid:false, error: error, token: String())
        }
        
        let swifty = JSON(jsonValue)
        let message = swifty["Message"].string ?? "No Description"
        if let success = swifty["Success"].bool {
            if success == false
            {
                let error = NSError(domain: Constants.ErrorDomain, code: Constants.ErrorCode.IntegrityDataError, userInfo: ["Error": "Server message: \(message)" ])
                return (isValid: false, error: error, token: String())
            }
            else
            {
                let element = swifty["Element"].string ?? String()
                return (isValid: true, error: nil, token: element)
            }
        }
        else
        {
            let error = NSError(domain: Constants.ErrorDomain, code: Constants.ErrorCode.IntegrityDataError, userInfo: ["Error": "FAILURE!!!! nil response!!!" ])
            return (isValid:false, error: error, token: String())
        }
    }
    
    private func getRequestUrl() -> URL?
    {
        //Construct URL Request
        let charsAllowed = CharacterSet(charactersIn: "/+=\n").inverted
        
        //Get encoded password and username
        guard let user = LibraryAPI.shared.currentUser else {
            
            return nil
        }
        let urlEncodedU = user.u!.addingPercentEncoding(withAllowedCharacters: charsAllowed)!
        let urlEncodedP = user.p!.addingPercentEncoding(withAllowedCharacters: charsAllowed)!
        
        //Construct URL with Server + Service
        if let server = ERMServer.info().defaultServer {
            
            let strURL = "\(server)\(Constants.RemoteService.PostAttachmentUpload)?u=\(urlEncodedU)&p=\(urlEncodedP)"
            let url = URL(string: strURL)
            return url
        }
        
        return nil
    }
    
    //Returns parameters required by the javascript framework
    fileprivate func getMultipartFlowParameters(_ fileSize: Int) -> [String: String]
    {
        return ["flowIdentifier": "\(fileSize)-mobilepng",
            "flowChunkSize": "1048576",
            "flowChunkNumber": "1",
            "flowFilename": "mobile.png",
            "flowRelativePath": "mobile.png",
            "flowTotalSize": "\(fileSize)",
            "flowTotalChunks": "1",
            "flowCurrentChunkSize": "\(fileSize)"]
    }
    
    //MARK: UPLOAD PROFILE PICTURE (STKAHOLDER)
    func uploadStakeholderProfilePicture(parameters: [String : Any], onSuccess: @escaping (_ thumbnail: String) ->(), onError:@escaping (_ error: NSError) -> ()) {
        requestService(Constants.RemoteService.PostUploadProfilePicture,
                       methodType: .post,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        var url = String()
                        if let stringUrl = jsonData.string {
                            
                            url = stringUrl
                        }                        
                        onSuccess(url)
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    //MARK: GET TRANSLATIONS FILE
    func getTranslationsiOS(_ version: Int,onSuccess: @escaping (_ response: Translation) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.getTranslationsiOS,
                       methodType: .get,
                       parameters: ["version": version],
                       onSuccess: { jsonData in
                        
                        onSuccess(Translation(json: jsonData))
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
}
