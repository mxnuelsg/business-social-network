//
//  GlobalSearch.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 9/30/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class GlobalSearchBO: BO
{
    func searchText(_ text: String, projectPageNumber: Int?, stakeholderPageNumber: Int?, postPageNumber: Int?, onSuccess:@escaping (_ results: GlobalSearchResult) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        var parameters: [String : Any] = [:]
        
        if projectPageNumber != nil
        {
            parameters["ProjectPageIndex"] = projectPageNumber!
        }
        
        if stakeholderPageNumber != nil
        {
            parameters["StakeholderPageIndex"] = stakeholderPageNumber!
        }
        
        if postPageNumber != nil
        {
            parameters["PostPageIndex"] = postPageNumber!
        }
        
        parameters["SearchText"] = text
        
        requestService(Constants.RemoteService.GlobalSearch,
            methodType: .get,
            parameters: parameters,
            onSuccess: { jsonData in
                                
                let results = GlobalSearchResult(json: jsonData)
                onSuccess(results)
            },
            onError: { error in
                
                print(error.localizedDescription)
                onError(error)
            }
        )
    }
}
