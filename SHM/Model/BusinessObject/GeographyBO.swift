//
//  GeographyBO.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 7/11/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class GeographyBO: BO
{
    //MARK: GEOGRAPHY DETAIL
    func getDetailById(_ parameters:[String : Any], onSuccess:@escaping (_ clusterItems: ClusterItem) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.getGeographyDetail,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        let clusterItem = ClusterItem(json:jsonData)
                        onSuccess(clusterItem)
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    func getStakeholdersByGeoId(_ parameters:[String : Any], onSuccess:@escaping (_ stakeholders: [Stakeholder]) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.getStakeholdersByGeoId,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        var stakeholders = [Stakeholder]()
                        if let array = jsonData.array {
                            stakeholders = array.map({Stakeholder(json:$0)})
                        }
                        onSuccess(stakeholders)
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    func getProjectsByGeoId(_ parameters:[String : Any], onSuccess:@escaping (_ projects: [Project]) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.getProjectsByGeoId,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        var projects = [Project]()
                        if let array = jsonData.array {
                            projects = array.map({Project(json:$0)})
                        }
                        onSuccess(projects)
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }

    func getEventsByGeoId(_ parameters:[String : Any], onSuccess:@escaping (_ clusterItem: ClusterItem) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.getEventsByGeoId,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        let clusterItem = ClusterItem(json:jsonData)
                        onSuccess(clusterItem)
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    func getCalendarEventsByGeoId(_ parameters:[String : Any], onSuccess:@escaping (_ clusterItem: ClusterItem) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetGeoCalendarEvents,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        let clusterItem = ClusterItem(json:jsonData)
                        onSuccess(clusterItem)
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    //MARK: GEOGRAPHY
    func getFollowedGeographies(_ parameters:[String : Any], onSuccess:@escaping (_ clusterItems: [ClusterItem]) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.getFollowedGeographies,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        var clusterItems = [ClusterItem]()
                        
                        if let items = jsonData.array {
                            
                            clusterItems = items.map({ClusterItem(json: $0)})
                        }
                        
                        onSuccess(clusterItems)
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    func getFollowedGeographiesLight(_ parameters:[String : Any], onSuccess:@escaping (_ clusterItems: ([ClusterItem], TotalItems: Int)) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.getFollowedGeographiesLight,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        var clusterItems = [ClusterItem]()
                        var totalItems = 0
                        if let items = jsonData["Geographies"].array {
                            
                            clusterItems = items.map({ClusterItem(json: $0)})
                        }
                        if let numberOfItems = jsonData["TotalItems"].int {
                            
                            totalItems = numberOfItems
                        }
                        
                        onSuccess((clusterItems, totalItems))
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    
    func getAvailableGeographies(_ parameters:[String : Any], onSuccess:@escaping (_ clusterItems: [ClusterItem]) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.getAvailableGeographies,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        var clusterItems = [ClusterItem]()
                        
                        if let items = jsonData.array {
                            
                            clusterItems = items.map({ClusterItem(json: $0)})
                        }
                        
                        onSuccess(clusterItems)
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    //MARK: GEOGRAPHY LIGHT
    func getGeographiesLight(_ parameters:[String : Any], onSuccess:@escaping (_ clusterItems: [ClusterItem], _ totalItems:Int) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.getGeographiesLight,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        var clusterItems = [ClusterItem]()
                        var totalItems = 0
                        if let items = jsonData["Geographies"].array {
                            
                            clusterItems = items.map({ClusterItem(json: $0)})
                        }
                        if let numberOfItems = jsonData["TotalItems"].int {
                            totalItems = numberOfItems
                        }
                        
                        onSuccess(clusterItems, totalItems)
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
        
    func getGeographiesByProject(_ projectId:Int, onSuccess:@escaping (_ clusterItems: [ClusterItem]) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        var parameters:[String : Any] = [:]
        parameters["projectId"] = projectId
        requestService(Constants.RemoteService.getGeographiesByProject,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        var clusterItems = [ClusterItem]()
                        
                        if let items = jsonData.array {
                            
                            clusterItems = items.map({ClusterItem(json: $0)})
                        }

                        onSuccess(clusterItems)
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    //MARK: FOLLOW GEOGRAPHY
    func followGeography(_ withId: Int, onSuccess:@escaping (_ clusterItems: ClusterItem) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.followGeography,
                       methodType: .post,
                       parameters: "\(withId)",
            onSuccess: { jsonData in
                
                onSuccess(ClusterItem(json: jsonData))
                
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    //MARK: UNFOLLOW GEOGRAPHY
    func unfollowGeography(_ withId: Int, onSuccess:@escaping (_ clusterItems: ClusterItem) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.unfollowGeography,
                       methodType: .post,
                       parameters: "\(withId)",
                       onSuccess: { jsonData in
                        
                        onSuccess(ClusterItem(json: jsonData))
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
}
