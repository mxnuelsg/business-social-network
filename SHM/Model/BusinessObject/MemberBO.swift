//
//  MemberBO.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 02/09/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class MemberBO: BO
{
    func getMembers(onSuccess:@escaping (_ members: [Member]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetMembers,
                       methodType: .post,
                       parameters: [:],
                       onSuccess: { jsonData in
                        
                        var members = [Member]()
                        if let jsonMembers = jsonData["members"].array {
                            
                            members = jsonMembers.map({ Member(json: $0) })
                        }
                        
                        onSuccess(members)
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    ///Search Members
    func searchMembers(_ searchTerm: String, stakeholderId: Int?, projectId: Int?,currentSiteId: Int? , onSuccess:@escaping (_ members: [Member]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        var parameters: [String : Any] = ["SearchText": searchTerm]
        
        if let stkId = stakeholderId {
            parameters["StakeholderId"] = stkId
        }
        
        if let prjId = projectId {
            parameters["ProjectId"] = prjId
        }
        
        if let siteId = currentSiteId {
            parameters["CurrentSiteId"] = siteId
        }
        
        requestService(Constants.RemoteService.GetMembers,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        var members = [Member]()
                        if let jsonContacts = jsonData.array {
                            
                            members = jsonContacts.map({ Member(json: $0) })
                        }
                        
                        onSuccess(members)
                        
        },  onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    
    ///Search Stakeholders and Members
    func getUsers(_ searchTerm: String, onSuccess:@escaping (_ contacts: [Contact]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetUsersToRelate,
                       methodType: .get,
                       parameters: ["searchText": searchTerm],
                       onSuccess: { jsonData in
                        
                        if let jsonContacts = jsonData.array {
                            
                            var arrayContacts = [Contact]()
                            
                            //Casting
                            for JASON in jsonContacts
                            {
                                let contact = Contact(json: JASON)
                                
                                if contact.isStakeholder == true
                                {
                                    let stakeholder = Stakeholder(json: JASON)
                                    arrayContacts.append(stakeholder)
                                }
                                else
                                {
                                    let member = Member(json: JASON)
                                    arrayContacts.append(member)
                                }
                            }
                            
                            //Sorting
                            arrayContacts = arrayContacts.sorted(by: {$0.name < $1.name})
                            onSuccess(arrayContacts)
                        }
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
}
