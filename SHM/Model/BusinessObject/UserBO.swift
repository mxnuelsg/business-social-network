//
//  UserBO.swift
//  PDI
//
//  Created by Manuel Salinas Gonzalez on 3/26/15.
//  Copyright (c) 2015 PDI. All rights reserved.
//

import UIKit
import Alamofire

class UserBO: BO
{
    var onLanguageChanged:((_ didChange: Bool)->())?
    func save(_ user: User)
    {
        let userDefaults = UserDefaults.standard
        
        userDefaults.setValue(user.isLoggedIn, forKey: Constants.UserKey.IsLoggedIn)
        userDefaults.setValue(user.id, forKey: Constants.UserKey.AId)
        userDefaults.setValue(user.role.rawValue, forKey: Constants.UserKey.Role)
        if let authToken = user.authToken {
        
            userDefaults.setValue(authToken, forKey: Constants.UserKey.AuthToken)
        }
        
        
        if let userName = user.userName {
            
            userDefaults.setValue(userName, forKey: Constants.UserKey.UserName)
        }
        userDefaults.setValue(user.password, forKey: Constants.UserKey.Password)
        
        if let sessionCookie = user.sessionCookie {
            
            userDefaults.setValue(sessionCookie, forKey: Constants.UserKey.Cookie)
        }
        if let u = user.u {
            
            userDefaults.setValue(u, forKey: Constants.UserKey.U)
        }
        if let p = user.p {
            
            userDefaults.setValue(p, forKey: Constants.UserKey.P)
        }
        if let deviceToken = user.deviceToken {
            
            userDefaults.setValue(deviceToken, forKey: Constants.UserKey.DeviceToken)
        }
        
        if let lastLogin = user.lastLogin {
            
            userDefaults.setValue(lastLogin, forKey: Constants.UserKey.LastLogin)
        }

        //language
        if user.userSettings.language == 1
        {
            userDefaults.setValue("en", forKey: Constants.UserKey.BackupLanguage)
        }
        else if user.userSettings.language == 2
        {
            userDefaults.setValue("es", forKey: Constants.UserKey.BackupLanguage)
        }
        
        
        
        userDefaults.set(user.userSettings.notificationCardUpdateActive, forKey: Constants.UserKey.NotificationOnCardUpdate)
        userDefaults.set(user.userSettings.notificationNewPostActive, forKey: Constants.UserKey.NotificationOnNewPost)
        userDefaults.set(user.userSettings.notificationProjectEditActive, forKey: Constants.UserKey.NotificationOnProjectEdit)
        userDefaults.set(user.userSettings.notificationStakeholderEditActive, forKey: Constants.UserKey.NotificationOnStakeholderEdit)
        


        userDefaults.synchronize()
    }
    
    func getUser() -> User?
    {
        let userDefaults = UserDefaults.standard
        
        if (userDefaults.value(forKey: Constants.UserKey.UserName) != nil)
        {
            let user = User()
            
            user.id = userDefaults.integer(forKey: Constants.UserKey.AId)
            user.role = UserRole(rawValue: userDefaults.integer(forKey: Constants.UserKey.Role))!
            user.userName = userDefaults.string(forKey: Constants.UserKey.UserName)
            user.password = userDefaults.string(forKey: Constants.UserKey.Password) ?? String()
            
            if let isLoggedIn = userDefaults.value(forKey: Constants.UserKey.IsLoggedIn) as? Bool
            {
                user.isLoggedIn = isLoggedIn
            }
            else
            {
                user.isLoggedIn = false
            }
            user.deviceToken = userDefaults.string(forKey: Constants.UserKey.DeviceToken)
            user.sessionCookie = userDefaults.string(forKey: Constants.UserKey.Cookie)
            user.u = userDefaults.string(forKey: Constants.UserKey.U)
            user.p = userDefaults.string(forKey: Constants.UserKey.P)
            user.lastLogin = userDefaults.value(forKey: Constants.UserKey.LastLogin) as? Date
            user.authToken = userDefaults.string(forKey: Constants.UserKey.AuthToken)
            let shouldUpdate = userDefaults.value(forKey: Constants.UserKey.NotificationOnCardUpdate) == nil
            
            user.userSettings.notificationCardUpdateActive = userDefaults.bool(forKey: Constants.UserKey.NotificationOnCardUpdate)
            user.userSettings.notificationNewPostActive = userDefaults.bool(forKey: Constants.UserKey.NotificationOnNewPost)
            user.userSettings.notificationProjectEditActive = userDefaults.bool(forKey: Constants.UserKey.NotificationOnProjectEdit)
            user.userSettings.notificationStakeholderEditActive = userDefaults.bool(forKey: Constants.UserKey.NotificationOnStakeholderEdit)
            
            if let languageStrValue = userDefaults.string(forKey: Constants.UserKey.BackupLanguage) {
                user.userSettings.language = languageStrValue == "en" ? 1 : 2
            }
            
            if shouldUpdate == true
            {
                save(user)
            }
            
            return user
        }
        else
        {
            return nil
        }
    }
    
    func doLogin(_ user: User, onSuccess: @escaping () -> (),  onError: @escaping (_ error: NSError) -> ())
    {
        

        requestSession(Constants.RemoteService.Login,
                       parameters: ["userName" : user.userName!],
                       onSuccess: { loginResponse in

                        //Headers
                        BO.SecurityHeaders = [ Constants.UserKey.Cookie : loginResponse["Cookie"] as! String ]
                        
                        //User data
                        user.isLoggedIn = true
                        user.lastLogin = Date()
                        user.sessionCookie = loginResponse["Cookie"] as? String
                        user.u = loginResponse["Username"] as? String
                        user.p = loginResponse["Password"] as? String
                        user.id = loginResponse["AccountId"] as! Int
                        user.role = UserRole(rawValue: loginResponse["Role"] as! Int)!
                        user.authToken = loginResponse["requestverificationtoken"] as? String
                        //Language
                        user.userSettings.language = loginResponse["Language"] as! Int
                        let langSaved = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage)//LibraryAPI.shared.currentUser!.userSettings.language == 2 ? "es" : "en"
                        let lang = loginResponse["Language"] as! Int == 2 ? "es" : "en"
                        if lang != langSaved
                        {
                            self.onLanguageChanged?(true)
                            print("Language changed")
                            //Language
                            let message = "The language changes will take place next time you load the app. Would you like to close the app now?".localized()
                            let alert = UIAlertController(title:"Language Change".localized(), message: message, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title:"No".localized(), style: .cancel, handler:nil))
                            alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (action) in
                                
                                exit(0)
                            }))
                            
                        }
                        
                        //Stakeholder Order
                        let orderStakeholder = loginResponse["StakeholderOrder"] as! Int
                        UserDefaultsManager.setContactOrder(orderStakeholder == 1 ? false : true)
                        
                        //Project Order
                        let orderProject = loginResponse["ProjectOrder"] as! Int
                        UserDefaultsManager.setProjectOrder(orderProject == 1 ? false : true)
                        
                        //Save in API
                        LibraryAPI.shared.userBO.save(user)
                        LibraryAPI.shared.currentUser = user
                        
                        onSuccess()
                        
        }) { error in
            
            onError(error)
        }
    }
    
    func saveUserSettings(_ parameters: [String : Any], onSuccess: @escaping () -> (), onError:@escaping (NSError) -> ()) {
        requestService(Constants.RemoteService.SaveSettings,
            methodType: .put,
            parameters: parameters,
            onSuccess: { jsonData in

                print(jsonData)
                onSuccess()
                
            }) { error in
                
                onError(error)
        }
    }
    
    func getUserSettings(_ onSuccess: @escaping (_ settings: UserSettings) -> (), onError:@escaping (NSError) -> ())
    {
        requestService(Constants.RemoteService.GetSettings,
            methodType: .get,
            parameters: nil,
            onSuccess: { jsonData in
                
                print(jsonData)
                
                let settings = UserSettings(json: jsonData)
                onSuccess(settings)
                
            }) { error in
                
                onError(error)
        }
    }
    
    
}
