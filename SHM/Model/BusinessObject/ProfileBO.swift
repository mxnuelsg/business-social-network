//
//  ProfileBO.swift
//  SHM
//
//  Created by Manuel Salinas on 10/13/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ProfileBO: BO
{
    //MARK: PROFILE DETAIL
    func getProfileDetail(_ accountId: Int, onSuccess: @escaping (_ member: Member?, _ projects: [Project]) -> (), onError:@escaping (_ error: NSError) -> ()) {
        requestService(Constants.RemoteService.GetMemberDetail,
                       methodType: .get,
                       parameters: ["accountId": accountId ],
                       onSuccess: { jsonData in
                        
                        let member = Member(json: jsonData)
                        var projects = [Project]()
                        if let projectArray = jsonData["AccountProjects"].array {
                            
                            projects = projectArray.map({Project(json:$0)})
                        }
                        onSuccess(member, projects)
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    //MARK: EDIT MEMBER
    func editMember(_ memberEdited: [String : Any], onSuccess:@escaping () ->(), onError:@escaping (_ error: NSError) -> ()) {
        requestService(Constants.RemoteService.PutEditMember,
                       methodType: .put,
                       parameters: memberEdited,
                       onSuccess: { jsonData in
                        onSuccess()
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    //MARK: UPLOAD PHOTO
    func uploadThumbnail(_ photo: [String : Any], onSuccess: @escaping (_ urlPhoto: String) ->(), onError:@escaping (_ error: NSError) -> ()) {
        requestService(Constants.RemoteService.PutUploadPhoto,
                       methodType: .post,
                       parameters: photo,
                       onSuccess: { jsonData in
                        
                        var url = String()
                        
                        if let stringUrl = jsonData.string {
                            
                            url = stringUrl
                        }
                        
                        onSuccess(url)
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
}
