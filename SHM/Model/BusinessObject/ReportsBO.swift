//
//  ReportsBO.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 1/27/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class ReportsBO: BO
{
    //MARK: GET MEMBER RELATIONS
    func getRelatedUsers(_ text: String, onSuccess:@escaping (_ users: [Stakeholder]) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        var parameters: [String : Any] = [:]
        parameters["SearchText"] = text
        
        requestService(Constants.RemoteService.ReportGetRelatedUsers,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        var stakeholders = [Stakeholder]()
                        
                        if let jsonStakeholders = jsonData.array {
                            
                            stakeholders = jsonStakeholders.map({ Stakeholder(json: $0) })
                        }
                        
                        onSuccess(stakeholders)
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        }
        )
    }
    
    func getReportRelations(_ parameters:[String : Any], onSuccess: @escaping (_ report: Reports?) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.ReportGetRelations,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        onSuccess(Reports(json: jsonData))
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        }
        )
    }
    
    // MARK: GETPOSTFILTERS
    func getReportPostFilters(_ parameters:[String : Any], onSuccess: @escaping (_ report: UserPostsReport?) -> (), onError:@escaping (_ error: NSError) -> ()) {
        
        requestService(Constants.RemoteService.ReportGetPostFilters,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        print(jsonData)
                        onSuccess(UserPostsReport(json: jsonData))
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        }
        )
    }
    
    func getPostList(_ parameters:[String : Any], onSuccess: @escaping (_ report: UserPostsReport?) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.ReportGetPostList,
                       methodType: .post,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        onSuccess(UserPostsReport(json: jsonData))
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        }
        )
    }
    
    //search accounts and stakeholders
    func getAccounts(_ searchText: String , onSuccess: @escaping (_ users: [ConexionReport]) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.ReportConexionsGetAccounts,
                       methodType: .get,
                       parameters: ["SearchText": searchText],
                       onSuccess: { jsonData in
                        
                        var conexionsR = [ConexionReport]()
                        
                        if let jsonUsers = jsonData.array {
                            
                            conexionsR = jsonUsers.map({ ConexionReport(json: $0) })
                        }
                        
                        onSuccess(conexionsR)
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    //search accounts and stakeholders
    func getStakeholders(_ searchText: String , onSuccess: @escaping (_ users: [ConexionReport]) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.ReportConexionsGetStakeholders,
                       methodType: .get,
                       parameters: ["SearchText": searchText],
                       onSuccess: { jsonData in
                        
                        var conexionsR = [ConexionReport]()
                        
                        if let jsonUsers = jsonData.array {
                            
                            conexionsR = jsonUsers.map({ ConexionReport(json: $0) })
                        }
                        
                        onSuccess(conexionsR)
                        
        }, onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    //search accounts and stakeholders
    func getStakeholderConnections(_ request: Any, onSuccess: @escaping (_ users: [Conexion]) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.ReportConexionsGetStakeholderConnections,
                       methodType: .get,
                       parameters: request,
                       onSuccess: { jsonData in
                        
                        var conexions = [Conexion]()
                        
                        if let jsonUsers = jsonData.array  {
                            
                            conexions = jsonUsers.map({ Conexion(json: $0) })
                        }
                        
                        onSuccess(conexions)
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    //search accounts and stakeholders
    func getNodeInfo(_ request: [String : Any], onSuccess: @escaping (_ users: Stakeholder?) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.ReportConexionsGetNodeInfo,
                       methodType: .get,
                       parameters: request,
                       onSuccess: { jsonData in
                        
                        onSuccess(Stakeholder(json: jsonData))
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
    
    //Get current account
    func getAccount(_ onSuccess: @escaping (_ user: ConexionReport?) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.ReportConexionsGetAccount,
                       methodType: .get,
                       parameters: nil,
                       onSuccess: { jsonData in
                        
                        onSuccess(ConexionReport(json: jsonData))
                        
        },onError: { error in
            
            print(error.localizedDescription)
            onError(error)
        })
    }
}
