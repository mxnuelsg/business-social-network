//
//  TipicBO.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 10/08/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ProjectBO: BO
{
    //MARK: PROJECT LIST
    func getProjects(parameters: [String : Any]?, onSuccess:@escaping (_ projects: [Project], _ sites: [Site], _ totalRecords: Int) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetProjectList,
                       methodType: .get,
                       parameters: parameters,
                       onSuccess: { jsonData in
                        
                        var projects = [Project]()
                        var sites = [Site]()
                        
                        let totalItems = jsonData["TotalItems"].int ?? 0
                        
                        if let jsonProjects = jsonData["Projects"].array {
                            
                            projects = jsonProjects.map({ Project(json: $0) })
                        }
                        
                        if let jsonSites = jsonData["SiteMappings"].array {
                            
                            sites = jsonSites.map({ Site(json: $0) })
                        }
                        
                        onSuccess(projects, sites, totalItems)
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    //MARK: SEARCH PROJECT
    func searchProject(_ searchTerm: String, onSuccess:@escaping (_ projects:[Project], _ totalItems: Float) -> (), onError: @escaping (_ error: NSError) -> ()) {
        requestService(Constants.RemoteService.GetProjectsSearch,
                       methodType: .get,
                       parameters: [
                        "FollowingStatus":1,
                        "descending": false,
                        "orderBy": "Title",
                        "pageNumber": 1,
                        "pageSize" : 20,
                        "searchText" : searchTerm],
                       onSuccess: { jsonData in
                        
                        if let jsonProjects = jsonData["Projects"].array {
                            
                            let projectList = jsonProjects.map({ Project(json: $0) })
                            let items = jsonData["TotalItems"].float ?? 0
                            
                            onSuccess(projectList, items)
                        }
                        else
                        {
                            onSuccess([], 0)
                        }
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    //MARK: LIGHT PROJECT LIST
    func getProjectsLightList(onSuccess:@escaping (_ projects:[Project]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetProjectsLight,
                       methodType: .get,
                       parameters: nil,
                       onSuccess: { jsonData in
                        
                        var projects = [Project]()
                        
                        if let jsonProjects = jsonData.array {
                            
                            projects = jsonProjects.map({ Project(json: $0) })
                        }
                        
                        onSuccess(projects)
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    //MARK: SEARCH ALL PROJECTS
    func searchAllProjects(_ searchTerm: String, onSuccess:@escaping (_ projects:[Project]) -> (), onError: @escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetProjectsLightComplete,
                       methodType: .get,
                       parameters: ["searchText" : searchTerm],
                       onSuccess: { jsonData in
                        
                        var projects = [Project]()
                        
                        if let jsonProjects = jsonData.array {
                            
                            projects = jsonProjects.map({ Project(json: $0) })
                        }
                        onSuccess(projects)
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    //MARK: PROJECT DETAIL
    func getProjectDetailById(_ idProject: Int, isDetail: String, onSuccess: @escaping (_ project: Project) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetProjectDetail,
                       methodType: .get,
                       parameters: [
                        "projectId": idProject,
                        "isDetail": isDetail],
                       onSuccess: { jsonData in
                        
                        guard let _ = jsonData.dictionary else {
                            onSuccess(Project(isDefault: true))
                            return
                        }
                        
                        onSuccess(Project(json: jsonData))
        },
                       onError: { error in
                        
                        print(error.localizedDescription)
                        onError(error)
        }
        )
    }
    
    func getProjectDetailLight(_ idProject: Int, onSuccess: @escaping (_ project: Project) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetProjectDetailLight,
                       methodType: .get,
                       parameters: [
                        "projectId": idProject,
                        "isDetail": "true"],
                       onSuccess: { jsonData in
                        
                        guard let _ = jsonData.dictionary else {
                            onSuccess(Project(isDefault: true))
                            return
                        }
                        
                        onSuccess(Project(json: jsonData))
        },
                       onError: { error in
                        
                        print(error.localizedDescription)
                        onError(error)
        }
        )
    }
    //MARK: FOLLOW PROJECT
    func followProject(_ idProject: Int, onSuccess: @escaping (_ success: Bool) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.FollowProject,
                       methodType: .post,
                       parameters: "\(idProject)",
            onSuccess: { jsonData in
                
                onSuccess(true)
                
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    //MARK: UNFOLLOW PROJECT
    func unfollowProject(_ idProject: Int, onSuccess: @escaping (_ success: Bool) -> (), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.UnfollowProject,
                       methodType: .post,
                       parameters: "\(idProject)",
            onSuccess: { jsonData in
                
                onSuccess(true)
                
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    //MARK: POST NEW PROJECT
    func projectPost(_ projectPost: [String : Any], onSuccess: @escaping (_ project: Project?) ->(), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.PostNewProject,
                       methodType: .post,
                       parameters: projectPost,
                       onSuccess: { jsonData in
                        
                        guard let _ = jsonData.dictionary else {
                            onSuccess(nil)
                            return
                        }
                        
                        onSuccess(Project(json: jsonData))
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    //MARK: GET USER INFO FOR NEW PROJECT
    func getCurrentUserSiteMappings(_ onSuccess: @escaping (_ sites: [Site]?) ->(), onError:@escaping (_ error: NSError) -> ())
    {
        requestService(Constants.RemoteService.GetCurrentUser,
                       methodType: .get,
                       parameters: nil,
                       onSuccess: { jsonData in
                        
                        var sites = [Site]()
                        
                        if let jsonSiteMappings = jsonData["SiteMappings"].array {
                            
                            sites = jsonSiteMappings.map({Site(json:$0)})
                        }
                        
                        onSuccess(sites)
                        
        }) { error in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
    
    //MARK: EDIT PROJECT
    func editProject(_ projectEdited: [String : Any], onSuccess: @escaping (_ project: Project?) ->(), onError:@escaping (_ error: NSError) -> ()) {
        requestService(Constants.RemoteService.PutEditProject,
                       methodType: .put,
                       parameters: projectEdited,
                       onSuccess: { jsonData in
                        
                        guard let _ = jsonData.dictionary else {
                            onSuccess(nil)
                            
                            return
                        }
                        
                        onSuccess(Project(json: jsonData))
                        
        }) { error in
            
            onError(error)
        }
    }
    
    func filterProjects(_ projects: [Project], byText text: String) -> [Project]
    {
        let projectsFiltered = projects.filter({
            ($0.title as NSString).localizedCaseInsensitiveContains(text)
        })
        
        return projectsFiltered
    }
    
    //MARK: ENABLE & DISABLE
    func enable(_ project: Project, enable: Bool, onSuccess: @escaping (_ isEnable: Bool)->(), onError: @escaping (_ error: NSError)->()) {
        requestService(Constants.RemoteService.EnableDisableProject,
                       methodType: .post,
                       parameters: [
                        "projectId" : project.id,
                        "enabled" : enable == true ? "true" : "false"],
                       onSuccess: { jsonData in

                        let project = Project(json: jsonData)
                        onSuccess(project.isEnable)
                        
        }) { (error) -> () in
            
            print(error.localizedDescription)
            onError(error)
        }
    }
}

