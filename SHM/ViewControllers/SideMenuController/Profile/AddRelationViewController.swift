//
//  AddRelationViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 12/16/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit


class AddRelationViewController: UIViewController
{
    // MARK: - OUTLETS AND PROPERTIES
    @IBOutlet weak var imgContact: UIImageView!
    @IBOutlet weak var txtRelationSelector: UITextField!
    @IBOutlet weak var lblContactName: UILabel!
    @IBOutlet weak var lblContactJob: UILabel!
    @IBOutlet weak var btnDeleteContact: UIButton!
    @IBOutlet weak var lblRelationDescription: UILabel!
    
    var isNewRelation: Bool = false
    var stakeholder: Stakeholder!
    var onSaveRelation: ((_ contactRelationed: Stakeholder) -> ())?
    var onRelationDelete:((_ contactRelationed: Stakeholder) -> ())?
    var types: [KeyValueObjectWithKeyValueArray]?
    var compSelector: Int = 0
    var rowSelector: Int = 0
    var defaultRelationStr : String?
    var isPickerFirstLoad = true
    
    // MARK: - LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: - CONFIG
    func loadConfig()
    {
        // Bar Button Items        
        let barBtnSave = UIBarButtonItem(title: "Save".localized(), style: .plain, target: self, action: #selector(self.saveProfileEdited))
        navigationItem.rightBarButtonItem = barBtnSave
        
        //Contact data
        self.imgContact.setImageWith(URL(string: stakeholder.thumbnailUrl), placeholderImage: UIImage(named: "defaultperson"))
        self.imgContact.setBorder()
        self.lblContactName.text = stakeholder.fullName.isEmpty == true ? "...":stakeholder.fullName
        self.lblContactJob.adjustsFontSizeToFitWidth = true
        let strJobPosition = stakeholder.jobPosition?.value ?? ""
        let strCompanyName = stakeholder.companyName 
        self.lblContactJob.attributedText = NSMutableAttributedString().setJobPositionCompany(strJobPosition, company: strCompanyName)
        
        self.lblRelationDescription.text =  "Relationship".localized()
        self.lblRelationDescription.textAlignment = .center
        self.txtRelationSelector.sizeToFit()
        self.txtRelationSelector.setBorder(UIColor.blueBelizeAlphy())
        
        let imgCombo = UIImage(named: "iconCombo")
        self.txtRelationSelector.rightView = UIImageView(image: imgCombo)
        self.txtRelationSelector.rightViewMode = UITextFieldViewMode.always
        
        //Edit configs for new and old contacts
        if(self.isNewRelation)
        {
            self.title = "ADD A RELATION".localized()
            self.btnDeleteContact.isHidden = true
            self.txtRelationSelector.placeholder = "Choose relation".localized()
        }
        else
        {
            self.title = "EDIT RELATION".localized()
            let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
            
            guard let rel1 = self.stakeholder.relationType1?.value, let rel2 = self.stakeholder.relationType2?.value, let rel1_ES = self.stakeholder.relationType1?.valueEs, let rel2_ES = self.stakeholder.relationType2?.valueEs else {
                
                return
            }
            
            self.txtRelationSelector.text = !(language == "es") ? "\(rel1) - \(rel2)" : "\(rel1_ES) - \(rel2_ES)"
        }
    }
    
    // MARK: TOOLBAR KEYBOARD
    func loadToolBar() -> UIToolbar
    {
        let toolBarKeyboard = UIToolbar()
        toolBarKeyboard.barStyle = UIBarStyle.blackOpaque;
        toolBarKeyboard.barTintColor = UIColor.colorForNavigationController()
                
        let barBtnDone = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action:#selector(self.closeKeyboard))

        barBtnDone.tintColor = UIColor.white
        
        toolBarKeyboard.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            barBtnDone]
        
        toolBarKeyboard.sizeToFit()
        
        return toolBarKeyboard
    }
    
    func closeKeyboard()
    {
        view.endEditing(true)
    }
    
    // MARK: ACTION BUTTONS
    func saveProfileEdited()
    {
        //Update changes in relationship
        guard let relation = types else {
            
            return
        }
        self.stakeholder.relationshipType1_id = relation[compSelector].key
        self.stakeholder.relationshipType1_name = relation[compSelector].value
        self.stakeholder.relationshipType1_namEs = relation[compSelector].valueEs
        if self.stakeholder.relationType1 != nil
        {
            self.stakeholder.relationType1!.key = relation[compSelector].key
            self.stakeholder.relationType1!.value = relation[compSelector].value
            self.stakeholder.relationType1!.valueEs = relation[compSelector].valueEs
        }
        self.stakeholder.relationshipType2_id = relation[compSelector].keyValues[rowSelector].key
        self.stakeholder.relationshipType2_name = relation[compSelector].keyValues[rowSelector].value
        self.stakeholder.relationshipType2_namEs = relation[compSelector].keyValues[rowSelector].valueEs
        if self.stakeholder.relationType2 != nil
        {
            self.stakeholder.relationType2!.key = relation[compSelector].keyValues[rowSelector].key
            self.stakeholder.relationType2!.value = relation[compSelector].keyValues[rowSelector].value
            self.stakeholder.relationType2!.valueEs = relation[compSelector].keyValues[rowSelector].valueEs
        }
        
        //Check for empty fields and save
        if self.stakeholder.relationshipType1_name == ""
        {
            MessageManager.shared.showBar(title: "Warning".localized(),
                                                            subtitle: "You haven't chosen a relationship".localized(),
                                                            type: .warning,
                                                            fromBottom: false)
            
        }
        else
        {
            self.onSaveRelation?(stakeholder)
        }
    }
    
    func close()
    {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func deleteContact(_ sender: AnyObject)
    {
        if self.onRelationDelete != nil
        {
            onRelationDelete? (stakeholder)
        }
    }
}

// MARK: - TEXTFIELD DELEGATE
extension AddRelationViewController: UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        navigationItem.rightBarButtonItem?.isEnabled = false
        
        textField.inputAccessoryView = loadToolBar()
        
        let pickerRelationType = UIPickerView()
        pickerRelationType.dataSource = self
        pickerRelationType.delegate = self
        pickerRelationType.backgroundColor = UIColor.groupTableViewBackground
        pickerRelationType.showsSelectionIndicator = true
        pickerRelationType.isMultipleTouchEnabled = false
        pickerRelationType.isExclusiveTouch = true
        textField.inputView  = pickerRelationType
        
        //If relation is being edited, picker components shows current relation
        if self.isNewRelation == false
        {
            if self.isPickerFirstLoad == true
            {
                self.isPickerFirstLoad = false
                if let relationTypes = self.types {
                    
                    //Find the current relation type 1 (Family, friends, work)
                    for (index1, relation1) in relationTypes.enumerated() where relation1.key == self.stakeholder.relationType1?.key
                    {
                        //Update component selector (responsible for loading second column)
                        self.compSelector = index1
                        
                        //Find the current relation type 2
                        pickerRelationType.selectRow(index1, inComponent: 0, animated: false)
                        
                        for (index2, relation2) in relationTypes[index1].keyValues.enumerated() where relation2.key == self.stakeholder.relationType2?.key
                        {
                            //Update second column and select matching row
                            pickerRelationType.reloadComponent(1)
                            pickerRelationType.selectRow(index2, inComponent: 1, animated: false)
                        }
                    }
                }
            }
            else
            {
                pickerRelationType.selectRow(self.compSelector, inComponent: 0, animated: false)
                pickerRelationType.reloadComponent(1)
                pickerRelationType.selectRow(self.rowSelector, inComponent: 1, animated: false)
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {

        
        navigationItem.rightBarButtonItem?.isEnabled = true
    }
}

// MARK: - PICKER DELEGATE & DATA SOURCE
extension AddRelationViewController: UIPickerViewDataSource, UIPickerViewDelegate
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        guard let relation = types else {
            
            return 0
        }
        
        if component == 0
        {
            return relation.count
        }
        else
        {
            guard relation.count > 0 else {
            
                return relation.count
            }
            return relation[compSelector].keyValues.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
        guard let relation = types else {
            
            return ""
        }
        
        if component == 0
        {
            return !(language == "es") ?  relation[row].value : relation[row].valueEs
        }
        else
        {
            return !(language == "es") ? relation[compSelector].keyValues[row].value : relation[compSelector].keyValues[row].valueEs
        }
    }
    

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        guard let relation = self.types else {
            
            return
        }
        
        if component == 0
        {
            compSelector = row
            let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
            
            self.txtRelationSelector.text = !(language == "es") ? "\(relation[compSelector].value) - \(relation[compSelector].keyValues[0].value)" : "\(relation[compSelector].valueEs) - \(relation[compSelector].keyValues[0].valueEs)"
            pickerView.reloadComponent(1)
            pickerView.selectRow(0, inComponent: 1, animated: true)
            self.rowSelector = 0
        }
        else
        {
            rowSelector = row
            let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
            self.txtRelationSelector.text = !(language == "es") ? "\(relation[compSelector].value) - \(relation[compSelector].keyValues[row].value)" : "\(relation[compSelector].valueEs) - \(relation[compSelector].keyValues[row].valueEs)"
        }
    }
}




