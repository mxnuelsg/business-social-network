//
//  TableTextfieldCell.swift
//  SHM
//
//  Created by Manuel Salinas on 9/23/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class TableTextfieldCell: UITableViewCell
{
    //MARK: PROPERTIES & OUTLET
    @IBOutlet weak var txtText: TextfieldBlockedMenu!
    let imgCombo = UIImage(named: "iconCombo")
    
    //MARK: LIFE CYCLE
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        //Textfield
        self.txtText.placeholder = "Enter text here".localized()
        self.txtText.font = UIFont.systemFont(ofSize: 14)
        self.txtText.textColor = UIColor.blackAsfalto()
        self.txtText.autocorrectionType = .no
        self.txtText.clearButtonMode = .never
        self.txtText.contentVerticalAlignment = .center
        self.txtText.autocapitalizationType = .sentences
    }
    
    //MARK: ACTIONS
    func comboStyle(_ active: Bool)
    {
        if active == true
        {
            self.txtText.rightView = UIImageView(image: self.imgCombo)
            self.txtText.rightViewMode = UITextFieldViewMode.always
        }
        else
        {
            self.txtText.rightView = nil
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
}
