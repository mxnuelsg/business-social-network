//
//  RelationCollectionViewCell.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 12/23/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class RelationCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imgCard: UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblJobPosition: UILabel!
    @IBOutlet weak var lblRelation: UILabel!
    @IBOutlet weak var lblUserRating: UILabel!
    @IBOutlet weak var lblAdminRating: UILabel!
    @IBOutlet weak var viewTapRating: UIView!
    var didTapOnRating:(() -> ())?
    override func awakeFromNib()
    {
        super.awakeFromNib()
        //Tap on rating control
        self.viewTapRating.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapOnRating)))
    }
    
    @objc fileprivate func tapOnRating()
    {
        self.didTapOnRating?()
    }
}
