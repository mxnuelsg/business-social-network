//
//  ProfilePictureCell.swift
//  SHM
//
//  Created by Manuel Salinas on 10/13/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ProfilePictureCell: UITableViewCell
{
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!

    override func awakeFromNib()
    {
        super.awakeFromNib()
        let strChangePicture = " \("Change Picture".localized())"
        self.lblTitle.text = strChangePicture
        self.imgProfile.layer.cornerRadius = (imgProfile.frame.size.width) / 2
        self.imgProfile.layer.borderWidth = 0.5
        self.imgProfile.layer.borderColor = UIColor.grayCloudy().cgColor
        self.imgProfile.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
}
