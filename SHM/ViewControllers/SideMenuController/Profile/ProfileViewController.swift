//
//  ProfileViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 7/28/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import MessageUI
import Spruce
import TOCropViewController

class ProfileViewController: UIViewController, UITabBarDelegate, MFMailComposeViewControllerDelegate
{
    //Outlets
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var imgProfilePicture: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var lblTelephone: UILabel!
    @IBOutlet weak var lblMail: UILabel!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnSendMail: UIButton!
    @IBOutlet weak var btnInbox: UIButton!
    @IBOutlet weak var btnItemEditProfile: UITabBarItem!
    @IBOutlet weak var tabBarProfile: UITabBar!
    @IBOutlet weak var lblPlaceACall: UILabel!
    @IBOutlet weak var lblSendAnEmail: UILabel!
    @IBOutlet weak var navigationItemProfile: UINavigationItem!
    
    //Properties
    fileprivate var isFirstTime = true
    
    var member: Member!
    var vcRelationsTable: RelationsProfileTableViewController!
    var vcProfilePages: ProfilePagesViewController!
    var arrayRelations = [Stakeholder]()
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    // MARK: - LIFE CLYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.localize()
        self.vcProfilePages.setBorder()
        self.imgBanner.blur()
        self.loadMemberDetail()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
        
        //Animation config
        if self.isFirstTime == true
        {
            self.imgProfilePicture.transform = CGAffineTransform(translationX: -300, y: self.view.frame.minY)
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        
        //Animation
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
            
            self.imgProfilePicture.transform = CGAffineTransform.identity
            
        }, completion: { complete in
            
            self.isFirstTime = false
        })
    }

    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func localize()
    {
        if DeviceType.IS_ANY_IPAD == false
        {
            self.navigationItemProfile.title = "MY PROFILE".localized()
            self.lblPlaceACall.text = "Place a Call".localized()
            self.lblSendAnEmail.text = "Send an Email".localized()
            self.lblMail.text = "username@mail.com".localized()
            self.btnItemEditProfile.title = "Edit Profile".localized()
        }
    }
    
    // MARK: WEB SERVICE
    func loadMemberDetail()
    {
        //Title in Navigation Bar
        if  LibraryAPI.shared.currentUser?.id == self.member.id
        {
            navigationItem.title = "MY PROFILE".localized()
        }
        else
        {
            navigationItem.title = "PROFILE".localized()
        }
        
        //Indicator
        self.vcProfilePages.vcRelationsTable.tableView.displayBackgroundMessage("Loading...".localized(),
            subMessage: "")
        
        btnItemEditProfile.isEnabled = false

        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        LibraryAPI.shared.profileBO.getProfileDetail(member.id, onSuccess: { member, projects in
            
            MessageManager.shared.hideHUD()
            self.vcProfilePages.vcRelationsTable.refreshed()
            self.vcProfilePages.projects = projects
            self.vcProfilePages.vcRelationsTable.tableView.dismissBackgroundMessage()
            
            
            if member?.id == LibraryAPI.shared.currentUser?.id && LibraryAPI.shared.currentUser?.role == .cardCreator
            {
                self.btnItemEditProfile.title = "Change Picture".localized()
            
                self.btnCall.backgroundColor = .white
                self.btnSendMail.backgroundColor = .white
            
                self.btnCall.isUserInteractionEnabled = false
                self.btnSendMail.isUserInteractionEnabled = false
            }
            else
            {
                self.btnItemEditProfile.title = "Edit Profile".localized()
                
                self.btnCall.backgroundColor = .clear
                self.btnSendMail.backgroundColor = .clear
                
                self.btnCall.isUserInteractionEnabled = true
                self.btnSendMail.isUserInteractionEnabled = true
            }
            
            //Thumbnail
            if let thumbnailUrl = member?.thumbnailUrl {
                
                self.imgBanner.setImageWith(URL(string: thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
                self.imgProfilePicture.setImageWith(URL(string: thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
            }
            
            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                self.member = member
                self.loadConfig()
                self.btnItemEditProfile.isEnabled = true
                self.vcProfilePages.vcRelationsTable.arrayRelations = self.member.stakeHolderAccountRelations
                self.vcProfilePages.vcRelationsTable.tableView.reloadData()
                
                //Spruce framework Animation
                let animation = SpringAnimation(duration: 0.7)
                
                DispatchQueue.main.async {
                    
                    self.vcProfilePages.vcRelationsTable.tableView.spruce.animate([.contract(.moderately)], animationType: animation)
                }
                
                //Empty Data
                if self.member.stakeHolderAccountRelations.count == 0
                {
                    self.vcProfilePages.vcRelationsTable.tableView.displayBackgroundMessage("No Relations".localized(),
                        subMessage: "")
                }
            }
            
            }) { error in
                
                MessageManager.shared.hideHUD()
                self.vcProfilePages.vcRelationsTable.refreshed()
                self.vcProfilePages.vcRelationsTable.tableView.dismissBackgroundMessage()

                let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                    self.btnItemEditProfile.isEnabled = true
                }
        }
    }
    
    // MARK: INITIAL CONFIG
    fileprivate func loadConfig()
    {
        //Inbox
        btnInbox.isHidden = (LibraryAPI.shared.currentUser?.role == UserRole.vipUser) ? false : true
        btnInbox.layer.cornerRadius = 2
        btnInbox.clipsToBounds = true
        btnInbox.addTarget(self, action: #selector(self.openInbox), for: .touchUpInside)
        
        //Profile Info
        lblName.text = LibraryAPI.shared.currentUser?.role == .cardCreator ? member.name : member.fullName
        lblPosition.attributedText = NSMutableAttributedString().setJobPositionDepartment(member.position, deparment: member.department)
        lblPlaceACall.text = "Place a Call".localized()
        lblSendAnEmail.text = "Send an Email".localized()
        lblTelephone.text = (member.phoneNumberCountryName.isEmpty || member.phoneNumberArea.isEmpty || member.phoneNumberLocal.isEmpty) ? String() : "\(member.phoneNumberCountryName) (\(member.phoneNumberArea)) \(member.phoneNumberLocal)"
        lblMail.text = (member.email.isEmpty) ? String() : member.email
        lblMail.numberOfLines = 2
        imgProfilePicture.isUserInteractionEnabled = true
        imgProfilePicture.removeGestureRecognizersOfType(ActionsTapGestureRecognizer.self)
        imgProfilePicture.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
            () -> () in
           
            if let img = self.imgProfilePicture.image as UIImage! {
                let vcViewer = ImageViewer(image: img)
                vcViewer.showRemoveButton = false
                
                let navController = UINavigationController(rootViewController: vcViewer)
                navController.modalPresentationStyle = .overFullScreen
                
                self.present(navController, animated: true, completion: nil)
            }
        }))
        
        //TabBar to Edit profile
        if let sessionId = LibraryAPI.shared.currentUser?.id as Int!, sessionId == member.id{
            tabBarProfile.isHidden = false
        }
        
        //Labels
        lblName.adjustsFontSizeToFitWidth = true
        lblPosition.adjustsFontSizeToFitWidth = true
        lblTelephone.adjustsFontSizeToFitWidth = true
        lblMail.adjustsFontSizeToFitWidth = true
        
        //Action Call
        btnCall.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
            ProfileViewController.callMember(self.member, fromViewController: self)
        }))
    
        //Action Mail
        btnSendMail.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
            
            ProfileViewController.sendMail(member: self.member, delegate: self, navigation: self.navigationController!)
        }))
    }
    
    // MARK: ACTIONS
    class func callMember(_ member: Member, fromViewController viewController: UIViewController)
    {
        guard (member.phoneNumberArea.isEmpty == false && member.phoneNumberLocal.isEmpty == false) || member.phone.isEmpty == false else {
            
            MessageManager.shared.showBar(title: "Info".localized(),
                                                            subtitle: "There's no phone number",
                                                            type: .info,
                                                            fromBottom: false)
            
            return
        }
        
        let telString = (member.phone.isEmpty == true) ? "\(member.phoneNumberCountryName + member.phoneNumberArea + member.phoneNumberLocal)" : "+\(member.phone)"
        let telUrlString = "tel://\(telString)"
        
        if let phoneCallURL:URL = URL(string: telUrlString) {
            
            if UIApplication.shared.canOpenURL(phoneCallURL) == true
            {
                let alertCall = UIAlertController(title: "\(telString)", message: nil, preferredStyle: UIAlertControllerStyle.alert)
                
                let actionCall = UIAlertAction(title: "Call".localized(), style: .default, handler: { (action) in
                    
                    UIApplication.shared.open(phoneCallURL, options: [:], completionHandler: nil)
                })
                
                let actionCancel = UIAlertAction(title: "Cancel".localized(), style: .default, handler: { (action) in
                    
                })
                
                alertCall.addAction(actionCancel)
                alertCall.addAction(actionCall)
                
                viewController.present(alertCall, animated: true, completion: nil)
            }
            else
            {
                MessageManager.shared.showBar(title: "Info".localized(),
                                                                subtitle: "It wasn't possible to make the call".localized(),
                                                                type: .info,
                                                                fromBottom: false)
            }
        }
    }
    
    class func sendMail(member: Member, delegate: MFMailComposeViewControllerDelegate, navigation: UINavigationController)
    {
        guard member.email.isEmpty == false else
        {
            MessageManager.shared.showBar(title: "Info".localized(),
                subtitle: "There's no email account".localized(),
                type: .info,
                fromBottom: false)
            return
        }
        
        let mailComposeViewController = ProfileViewController.configuredMailComposeViewController(member: member, delegate: delegate)
        
        if MFMailComposeViewController.canSendMail()
        {
            navigation.present(mailComposeViewController, animated: true, completion: nil)
        }
        else
        {
            MessageManager.shared.showBar(title: "Error",
                subtitle: "It wasn't possible to send the mail".localized(),
                type: .error,
                fromBottom: false)
        }
    }
    
    class func sendMailWithProject(member: Member, project: Project, delegate: MFMailComposeViewControllerDelegate, navigation: UINavigationController)
    {
        guard member.email.isEmpty == false else
        {
            MessageManager.shared.showBar(title: "Info".localized(),
                subtitle: "There's no email account".localized(),
                type: .info,
                fromBottom: false)
            return
        }
        
        let mailComposeViewController = ProfileViewController.configuredMailComposeViewControllerWithProject(member: member, project: project, delegate: delegate)
        
        if MFMailComposeViewController.canSendMail()
        {
            navigation.present(mailComposeViewController, animated: true, completion: nil)
        }
        else
        {
            MessageManager.shared.showBar(title: "Error",
                subtitle: "It wasn't possible to send the mail".localized(),
                type: .error,
                fromBottom: false)
        }
    }
    
    class func sendInbox(member: Member, navigation: UINavigationController)
    {
        let vcNewMessage = Storyboard.getInstanceOf(NewMessageTableViewController.self)
        vcNewMessage.recipientsSelected = [member]
        
        let navController = NavyController(rootViewController: vcNewMessage)
        navController.modalPresentationStyle = .formSheet
        
        navigation.present(navController, animated: true, completion: nil)
    }
    

    func openInbox()
    {
        ProfileViewController.sendInbox(member: self.member, navigation: self.navigationController!)
    }
    
    func editPicture()
    {
        let alertSheet = UIAlertController(title: DeviceType.IS_ANY_IPAD == false ? nil : "Change Picture".localized(), message: nil, preferredStyle: DeviceType.IS_ANY_IPAD == false ? .actionSheet : .alert)
        alertSheet.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler:nil))
        
        alertSheet.addAction(UIAlertAction(title: "Take Photo".localized(), style: .default, handler: {
            (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                imagePicker.cameraDevice = UIImagePickerControllerCameraDevice.front
                imagePicker.allowsEditing = false
                imagePicker.modalPresentationStyle = .overCurrentContext
                
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        alertSheet.addAction(UIAlertAction(title: "Choose Existing".localized(), style: .default, handler: {
            (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) {
                
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                imagePicker.allowsEditing = false
                imagePicker.modalPresentationStyle = DeviceType.IS_ANY_IPAD == false ? .overCurrentContext : .formSheet
                
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        self.present(alertSheet, animated: true, completion: nil)
    }
    
    // MARK: TABBAR DELEGATE (FeedTableCell)
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        //Selected item color equal not selected color
        tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        
        switch item.tag
        {
        case 1:
            
            if self.member.id == LibraryAPI.shared.currentUser?.id && LibraryAPI.shared.currentUser?.role == .cardCreator
            {
                //Card Creator way
                self.editPicture()
            }
            else
            {
                performSegue(withIdentifier: "EditProfileViewController", sender: nil)
            }
            
        default:
            break
        }
    }
    
    // MARK: - NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier! == "EditProfileViewController"
        {
            let navController = segue.destination as! NavyController
            if let vcEditProfile = navController.viewControllers.first as? EditProfileViewController
            {
                vcEditProfile.memberEditing = member
                vcEditProfile.arrayRelations = member.stakeHolderAccountRelations
                vcEditProfile.onMemberEdited = { [weak self] Void in
                    
                    self?.loadMemberDetail()
                }
            }
        }
        
        if segue.identifier! == "ProfilePagesViewController"
        {
            self.vcProfilePages = segue.destination as! ProfilePagesViewController
            self.vcProfilePages.didUpdateRelations = {[weak self] void in
            
                self?.loadMemberDetail()
            }
        }
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    class func configuredMailComposeViewController(member: Member, delegate: MFMailComposeViewControllerDelegate) -> MFMailComposeViewController
    {
        let vcMailComposer = MFMailComposeViewController()
        vcMailComposer.mailComposeDelegate = delegate
        vcMailComposer.setToRecipients([member.email])
        vcMailComposer.setSubject(member.fullName)
        
        return vcMailComposer
    }
    
    class func configuredMailComposeViewControllerWithProject(member: Member, project: Project, delegate: MFMailComposeViewControllerDelegate) -> MFMailComposeViewController
    {
        let vcMailComposer = MFMailComposeViewController()
        vcMailComposer.mailComposeDelegate = delegate
        vcMailComposer.setToRecipients([member.email])
        vcMailComposer.setSubject(project.title)
        
        return vcMailComposer
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        controller.dismiss(animated: true) { () -> Void in
        }
    }
}

// MARK: IMAGE PICKER DELEGATE
extension ProfileViewController:  UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        dismiss(animated: true) { () -> Void in
            
            if let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
            {
                UIApplication.shared.isStatusBarHidden = true
                
                let cropViewController = TOCropViewController(image: selectedImage)
                cropViewController?.delegate = self
                self.present(cropViewController!, animated: true, completion: nil)
            }
        }
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: NSErrorPointer?, contextInfo:UnsafeRawPointer)
    {
        if error != nil
        {
            let alert = UIAlertController(title: "Save Failed", message: "Failed to save image",  preferredStyle: UIAlertControllerStyle.alert)
            let cancelAction = UIAlertAction(title: "OK",  style: .cancel, handler: nil)
            alert.addAction(cancelAction)
        }
    }
}

// MARK: TOCROPVIEW DELEGATE
extension ProfileViewController : TOCropViewControllerDelegate
{
    func cropViewController(_ cropViewController: TOCropViewController!, didCropTo image: UIImage!, with cropRect: CGRect, angle: Int)
    {
        var editedImage = image
        UIApplication.shared.isStatusBarHidden = false
        self.presentedViewController?.dismiss(animated: false, completion: {
            
            //Indicator
            MessageManager.shared.showLoadingHUD()
            
            if (editedImage?.size.width)! > CGFloat(1000)
            {
                editedImage = image.resizeToWidth(1000)
            }
            
            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                self.uploadImage(editedImage!)
            }
        })
    }
    
    fileprivate func uploadImage(_ image:UIImage)
    {
        //Create Request Object
        var wsObject: [String : Any]  = [:]
        wsObject["OldFile"] = String()
        wsObject["VersionId"] = 1
        
        if let imageData = UIImagePNGRepresentation(image) as Data! {
            
            let base64String = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            wsObject["NewFile"] = "data:image/png;base64,\(base64String)"
        }
        
        //Upload Photo
        LibraryAPI.shared.profileBO.uploadThumbnail(wsObject, onSuccess: { urlPhoto in
            
            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                MessageManager.shared.hideHUD()
                
                
                self.member.thumbnailUrl = urlPhoto
                
                if self.member.thumbnailUrl.isEmpty == false
                {
                    self.imgBanner.setImageWith(URL(string: self.member.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
                    self.imgProfilePicture.setImageWith(URL(string: self.member.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
                }
                else
                {
                    MessageManager.shared.showBar(title: "Error",
                                                  subtitle: "It wasn't possible to update the profile picture. Try again".localized(),
                                                  type: .error,
                                                  fromBottom: false)
                }
            }
            
        }, onError: { error in
            
            MessageManager.shared.showBar(title: "Error",
                                          subtitle: "It wasn't possible to update the profile picture. Try again".localized(),
                                          type: .error,
                                          fromBottom: false)
            
            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                MessageManager.shared.hideHUD()
            }
        })
    }
    
    func cropViewController(_ cropViewController: TOCropViewController!, didFinishCancelled cancelled: Bool)
    {
        UIApplication.shared.isStatusBarHidden = false
        self.presentedViewController?.dismiss(animated: true, completion: nil)
    }
}


// MARK: UITABLEVIEW DATASOURCE AND DELEGATE
extension ProfileViewController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrayRelations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RelationCell") as! RelationTableViewCell


        let stakeholder = self.arrayRelations[indexPath.row]
        let strJobPosition = stakeholder.jobPosition?.value ?? ""
        let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"

        cell.lblFullName.text = stakeholder.fullName
        cell.lblJobTitle.text = strJobPosition
        cell.imgThumbnail.setImageWith(URL(string: stakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        cell.lblRelation.text = !(language == "es") ? "\(stakeholder.relationshipType1_name!) - \(stakeholder.relationshipType2_name!)" : "\(stakeholder.relationshipType1_namEs!) - \(stakeholder.relationshipType2_namEs!)"

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 86
    }
}


