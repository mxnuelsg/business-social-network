//
//  ProfilePagesViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 8/10/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class ProfilePagesViewController: UIViewController
{
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var tableProjects: UITableView!
    @IBOutlet weak var containerRelations: UIView!
    var vcRelationsTable: RelationsProfileTableViewController!
    var didUpdateRelations:(()->())?
    var projects = [Project]() {
        didSet {
            self.tableProjects.reloadData()
        }
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    fileprivate func loadConfig()
    {
        self.segmentControl.setTitle("Relations".localized(), forSegmentAt: 0)
        self.segmentControl.setTitle("My Projects".localized(), forSegmentAt: 1)
        self.tableProjects.isHidden = true
        self.containerRelations.isHidden = false
        
        //Projects Table
        self.tableProjects.delegate = self
        self.tableProjects.dataSource = self
        //Register project cell
        self.tableProjects.register(UINib(nibName: "ProjectTableCell", bundle: nil), forCellReuseIdentifier: "ProjectTableCell")
    }
    
    @IBAction func segmentChanged(_ sender: Any)
    {
        if self.segmentControl.selectedSegmentIndex == 0
        {
            self.tableProjects.isHidden = true
            self.containerRelations.isHidden = false
        }
        else
        {
            self.tableProjects.isHidden = false
            self.containerRelations.isHidden = true
        }
    }

    func pushActor(_ actor: Stakeholder)
    {
        let actorDetailVC : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
        actorDetailVC.stakeholder = actor
        navigationController?.pushViewController(actorDetailVC, animated: true)
    }
    
    func pushProject(_ project: Project)
    {
        let vcProjectDetail: DetailProjectViewController = Storyboard.getInstanceFromStoryboard("Projects")
        vcProjectDetail.project = project
        self.navigationController?.pushViewController(vcProjectDetail, animated: true)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {            
        if segue.identifier! == "RelationsProfileTableViewController"
        {
            self.vcRelationsTable = segue.destination as! RelationsProfileTableViewController
            self.vcRelationsTable.enableRefreshAction {
                
                self.didUpdateRelations?()
            }
            self.vcRelationsTable.didUpdateRelations = {
                self.didUpdateRelations?()
            }
        }
    }
}

extension ProfilePagesViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.projects.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 155
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProjectTableCell") as! ProjectTableCell
        let project = self.projects[indexPath.row]
        let selectedColor = UIView()
        selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
        cell.selectedBackgroundView = selectedColor
        cell.ivSwipeDots.isHidden = true
        cell.cellForProject(cell, project: project)
        
        //Closures
        cell.onStakeholderTap = { [weak self] stakeholder in
            
            //Enabled or Disabled
            guard stakeholder.isEnable == true else {
                
                MessageManager.shared.showBar(title: stakeholder.fullName, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                
                return
            }
            
            self?.pushActor(stakeholder)
        }
        
        cell.onAllStakeholdersTap = { [weak self] Void in
            
            let stakeholdersTableVC = StakeholdersTableViewController.getNewInstance()
            stakeholdersTableVC.stakeholders = project.stakeholders
            
            let vcModal = ModalViewController(subViewController: stakeholdersTableVC)
            self?.present(vcModal, animated:true, completion: nil)
        }
        cell.onFilesTap = { [weak self] Void in
            
            let vcFileList = FileListViewController(attachments: project.files)
            let vcModal = ModalViewController(subViewController: vcFileList)
            vcModal.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            vcModal.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            
            self?.present(vcModal, animated:true, completion: nil)
        }
        cell.onTextViewTap = { [weak self] Void in
            
            self?.pushProject(project)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {    
        let project = self.projects[indexPath.row]
        self.pushProject(project)
    }
}
