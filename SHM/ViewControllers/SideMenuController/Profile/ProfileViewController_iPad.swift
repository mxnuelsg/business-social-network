//
//  ProfileViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas on 11/18/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import MessageUI
import Spruce


class ProfileViewController_iPad: ProfileViewController
{
    //MARK: OUTLETS & VARIABLES
    @IBOutlet weak var collectionRelations: UICollectionView!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblPhoneIndicator: UILabel!
    @IBOutlet weak var lblEmailIndicator: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var projectsContainerView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var isModal = false
    var layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    
    //MARK: LIFE CYCLE
     override func viewDidLoad()
    {
        self.loadIpadConfig()
        self.loadMemberDetail()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        self.loadLayoutForCollectionView()
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    //MARK: LAYOUT FOR COLLECTION VIEW
    func loadLayoutForCollectionView()
    {
        if (UIDeviceOrientationIsPortrait(UIDevice.current.orientation))
        {
            self.layout.scrollDirection =  .vertical
            self.layout.minimumInteritemSpacing = 10
            self.layout.minimumLineSpacing = 10
            self.layout.sectionInset = UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 20)
            self.layout.headerReferenceSize = CGSize(width: 320, height: 24)
            self.layout.itemSize = CGSize(width: 355, height: 85)
        }
        else
        {
            self.layout.scrollDirection =  .vertical
            self.layout.minimumInteritemSpacing = 1
            self.layout.minimumLineSpacing = 11
            self.layout.sectionInset = UIEdgeInsets(top: 5, left: 1, bottom: 5, right: 1)
            self.layout.headerReferenceSize = CGSize(width: 320, height: 24)
            self.layout.itemSize = CGSize(width: 340, height: 85)
        }
    }
    
    //MARK: LOAD CONFIG
    func loadIpadConfig()
    {
        self.localize()
        
        self.collectionRelations.setBorder()
        
        //Bar Button Items
        if self.isModal == true
        {
            let barBtnClose = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.close))
            navigationItem.leftBarButtonItem = barBtnClose
        }
        
        let barBtnEdit = UIBarButtonItem(image: UIImage(named: "EditUser"), style: .plain, target: self, action: #selector(self.editProfile))
        navigationItem.rightBarButtonItem = barBtnEdit
        
        //Phone
        self.lblPhone.text = "Phone".localized()
        
        //Inbox button
        btnInbox.backgroundColor = UIColor.blackAsfalto()
        btnInbox.layer.cornerRadius = 2
        btnInbox.clipsToBounds = true
        btnInbox.addTarget(self, action: #selector(ProfileViewController.openInbox), for: .touchUpInside)
        
        //Background
        imgBanner.setImageWith(URL(string: member.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        imgBanner.blur()
        
        //Collection View
        self.layout = UICollectionViewFlowLayout()
        self.loadLayoutForCollectionView()
        
        if let collectionViewRelations = collectionRelations {
            
            collectionViewRelations.collectionViewLayout = self.layout
            collectionViewRelations.dataSource = self
            collectionViewRelations.delegate = self
            collectionViewRelations.allowsMultipleSelection = false
            collectionViewRelations.register(UINib(nibName: "RelationCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RelationCollectionViewCell")
            collectionViewRelations.backgroundColor = UIColor.clear
        }
    }
    
    fileprivate func localize()
    {
        self.lblPhoneIndicator.text = "Phone Number".localized()
        self.lblEmailIndicator.text = "E-mail".localized()
        self.btnInbox.setTitle("  Inbox".localized(), for: UIControlState())
        self.segmentedControl.setTitle("Relations".localized(), forSegmentAt: 0)
        self.segmentedControl.setTitle("Projects".localized(), forSegmentAt: 1)
    }
    
    // MARK: WEB SERVICE
    override func loadMemberDetail()
    {
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        LibraryAPI.shared.profileBO.getProfileDetail(member.id, onSuccess: { member, projects in
            
            MessageManager.shared.hideHUD()
            
            if member?.id == LibraryAPI.shared.currentUser?.id && LibraryAPI.shared.currentUser?.role == .cardCreator
            {
                self.lblPhoneIndicator.isHidden = true
                self.lblEmailIndicator.isHidden  = true
                self.lblPhone.isHidden = true
                self.lblEmail.isHidden = true
                self.lblPhone.isUserInteractionEnabled = false
                self.lblEmail.isUserInteractionEnabled = false
                
            }
            else
            {
                self.lblPhoneIndicator.isHidden = false
                self.lblEmailIndicator.isHidden  = false
                self.lblPhone.isHidden = false
                self.lblEmail.isHidden  = false
                self.lblPhone.isUserInteractionEnabled = true
                self.lblEmail.isUserInteractionEnabled = true
            }
            
            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) { [weak self] in
                
                self?.member = member
                self?.loadInformation()
                
                self?.collectionRelations.reloadData()
                
                //Spruce framework Animation
                DispatchQueue.main.async {
                    
                    self?.collectionRelations.spruce.animate([.fadeIn, .expand(.slightly)], sortFunction: LinearSortFunction(direction: .topToBottom, interObjectDelay: 0.2))
                }
            }
            
            let projectsVC: ProjectsTableViewController = Storyboard.getInstanceFromStoryboard("Main")
            projectsVC.showsFromProfile = true
            projectsVC.projects = projects
            projectsVC.didUpdateProjectList = {[weak self] void in
            
                self?.loadMemberDetail()
            }
            self.projectsContainerView.addSubViewController(projectsVC, parentVC: self)
            
            
            }) { error in
                
                let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    MessageManager.shared.hideHUD()
                    
                }
        }
    }
    
    // MARK: INITIAL CONFIG
    func loadInformation()
    {
        //Title in Navigation Bar
        if  LibraryAPI.shared.currentUser?.id == self.member.id
        {
            navigationItem.title = "MY PROFILE".localized()
        }
        else
        {
            navigationItem.title = "PROFILE".localized()
        }
        
        //Inbox
        btnInbox.isHidden = (LibraryAPI.shared.currentUser?.role == UserRole.vipUser) ? false : true
        
        //Member Info
        lblName.text = LibraryAPI.shared.currentUser?.role == .cardCreator ? member.name : member.fullName
        lblName.adjustsFontSizeToFitWidth = true
        lblPosition.attributedText = NSMutableAttributedString().setJobPositionDepartment(member.position, deparment: member.department)
        lblPosition.adjustsFontSizeToFitWidth = true
        self.lblPhone.text = (member.phoneNumberCountryName.isEmpty || member.phoneNumberArea.isEmpty || member.phoneNumberLocal.isEmpty) ? String() : "\(member.phoneNumberCountryName) (\(member.phoneNumberArea)) \(member.phoneNumberLocal)"
        self.lblPhone.adjustsFontSizeToFitWidth = true
        lblEmail.text = (member.email.isEmpty) ? String() : member.email
        lblEmail.numberOfLines = 2
        lblEmail.adjustsFontSizeToFitWidth = true
        imgProfilePicture.setImageWith(URL(string: member.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        imgBanner.setImageWith(URL(string: member.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        imgProfilePicture.isUserInteractionEnabled = true
        imgProfilePicture.removeGestureRecognizersOfType(ActionsTapGestureRecognizer.self)
        imgProfilePicture.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
            () -> () in
            
            if let img = self.imgProfilePicture.image as UIImage!{
                
                let vcViewer = ImageViewer(image: img)
                vcViewer.showRemoveButton = false
                
                let navController = UINavigationController(rootViewController: vcViewer)
                navController.modalPresentationStyle = .formSheet
                vcViewer.view.backgroundColor = UIColor.black
                
                self.navigationController!.present(navController, animated: true, completion: nil)
            }
        }))
        
        //Action Call
        self.lblPhone.isUserInteractionEnabled = true
        self.lblPhone.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
            
            ProfileViewController.callMember(self.member, fromViewController: self)
        }))
        
        //Action Mail
        lblEmail.isUserInteractionEnabled = true
        lblEmail.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
            
            self.sendMail(member: self.member, delegate: self, navigation: self.navigationController!)
        }))
    }
    
    //MARK: ACTIONS
    func close()
    {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func editProfile()
    {
        if self.member.id == LibraryAPI.shared.currentUser?.id && LibraryAPI.shared.currentUser?.role == .cardCreator
        {
            //Card Creator way
            self.editPicture()
        }
        else
        {
            let vcEditProfile = Storyboard.getInstanceOf(EditProfileViewController.self)
            vcEditProfile.memberEditing = member
            vcEditProfile.arrayRelations = member.stakeHolderAccountRelations
            vcEditProfile.onMemberEdited = { [weak self] Void in
                
                self?.loadMemberDetail()
            }
            
            let navController = NavyController(rootViewController: vcEditProfile)
            navController.modalPresentationStyle = .formSheet
            self.navigationController?.present(navController, animated: true, completion: nil)
        }
    }
    
    override func openInbox()
    {
        let vcNewMessage = Storyboard.getInstanceOf(NewMessageTableViewController.self)
        vcNewMessage.recipientsSelected = [member]
        
        let navController = NavyController(rootViewController: vcNewMessage)
        navController.modalPresentationStyle = .formSheet
        
        self.navigationController?.present(navController, animated: true, completion: nil)
    }
    
    func sendMail(member: Member, delegate: MFMailComposeViewControllerDelegate, navigation: UINavigationController)
    {
        guard member.email.isEmpty == false else {
            
            MessageManager.shared.showBar(title: "Info".localized(),
                subtitle: "There's no email account".localized(),
                type: .info,
                fromBottom: false)
            
            return
        }
        
        let mailComposeViewController = ProfileViewController.configuredMailComposeViewController(member: member, delegate: delegate)
        mailComposeViewController.modalPresentationStyle = .formSheet
        
        if MFMailComposeViewController.canSendMail()
        {
            navigation.present(mailComposeViewController, animated: true, completion: nil)
        }
        else
        {
            MessageManager.shared.showBar(title: "Error",
                subtitle: "It wasn't possible to send the mail".localized(),
                type: .error,
                fromBottom: false)
        }
    }
    
    @IBAction func segmentedControlValueChanged(sc: UISegmentedControl) {
        projectsContainerView.isHidden = sc.selectedSegmentIndex != 1
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        super.prepare(for: segue, sender: sender)
        if let id = segue.identifier, id == "RelationsProfileTableViewController" {
            
            self.vcRelationsTable = segue.destination as! RelationsProfileTableViewController
        }
        
    }
}

 //MARK: COLLECTION VIEW DELEGATE AND DATA SOURCE
extension ProfileViewController_iPad: UICollectionViewDelegate, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.member.stakeHolderAccountRelations.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RelationCollectionViewCell", for: indexPath) as! RelationCollectionViewCell
        cell.setBorder()
        cell.lblName.text = self.member.stakeHolderAccountRelations[indexPath.item].fullName
        
        let strJobPosition = self.member.stakeHolderAccountRelations[indexPath.item].jobPosition?.value ?? ""
        cell.lblJobPosition.text = strJobPosition
        
        let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
        cell.lblRelation.text = !(language == "es") ? "\(self.member.stakeHolderAccountRelations[indexPath.item].relationshipType1_name!) - \(self.member.stakeHolderAccountRelations[indexPath.item].relationshipType2_name!)" : "\(self.member.stakeHolderAccountRelations[indexPath.item].relationshipType1_namEs!) - \(self.member.stakeHolderAccountRelations[indexPath.item].relationshipType2_namEs!)"

        cell.imgProfile.setImageWith(URL(string: self.member.stakeHolderAccountRelations[indexPath.item].thumbnailUrl), placeholderImage: UIImage(named: "defaultperson"))
        
        //Rating
        let stakeholder = self.member.stakeHolderAccountRelations[indexPath.item]
        cell.lblUserRating.text = (stakeholder.userRating ?? 0).getString()
        cell.lblAdminRating.text = (stakeholder.adminRating ?? 0).getString()
        cell.didTapOnRating = {
            
            RatingViewController_iPad.present(inController: self,
                                              stakeholder: stakeholder,
                                              sourceView: cell.viewTapRating,
                                              onEndRating: { [weak self] in
                                                self?.loadMemberDetail()
            })
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        guard indexPath.item < self.member.stakeHolderAccountRelations.count else {return}
        let stakeholderSelected = self.member.stakeHolderAccountRelations[indexPath.item]
        if stakeholderSelected.id != 0
        {
            let vcStakeholderDetail = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
            vcStakeholderDetail.stakeholder = stakeholderSelected
            self.navigationController?.pushViewController(vcStakeholderDetail, animated: true)
        }
        else
        {
            MessageManager.shared.showBar(title: "Warning".localized(),
                                          subtitle: "Your selection is not a stakeholder".localized(),
                                          type: .warning,
                                          fromBottom: false)
        }
    }
}
