//
//  EditProfileViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 9/10/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import TOCropViewController

class EditProfileViewController: UITableViewController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    // MARK: - PROPERTIES
    var textFieldIndexPath: IndexPath!
    var memberEditing: Member!

    var onMemberEdited : (() -> ())?
    
    var imgProfile: UIImage!
    var idCountry: Int!

    var strHolderPhoneArea: String!
    var strHolderPhoneLocal: String!
    var strHolderPosition: String!
    var strHolderEmail: String!
    var strHolderPhoneCountry: String!
    
    var backupPhoneArea: String!
    var backupPhoneLocal: String!
    var backupPosition: String!
    var backupEmail: String!
    var backupPhoneCountry: String!
    
    var vcRelationsTable: RelationsProfileTableViewController!
    var arrayRelations:[Stakeholder]? = []


    // MARK: - LIFE CLYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        backButtonArrow()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
   
    // MARK: INITIAL CONFIG
    func loadConfig()
    {
        //Title
        title = "EDIT PROFILE".localized()
        
        //Register Custom Cell
        tableView.register(UINib(nibName: "ProfilePictureCell", bundle: nil), forCellReuseIdentifier: "ProfilePictureCell")
        tableView.register(UINib(nibName: "TableTextfieldCell", bundle: nil), forCellReuseIdentifier: "TableTextfieldCell")
        
        //Validation Strings
        self.strHolderPhoneArea = self.memberEditing.phoneNumberArea 
        self.strHolderPhoneLocal = self.memberEditing.phoneNumberLocal 
        self.strHolderPhoneCountry = self.memberEditing.phoneNumberCountryName 
        self.strHolderEmail = self.memberEditing.email 
        self.strHolderPosition = self.memberEditing.position 
        
        //Backup member settings
        self.backupPhoneArea = self.memberEditing.phoneNumberArea 
        self.backupPhoneLocal = self.memberEditing.phoneNumberLocal 
        self.backupPhoneCountry = self.memberEditing.phoneNumberCountryName 
        self.backupEmail = self.memberEditing.email 
        self.backupPosition = self.memberEditing.position 
        

        
        /**
        Bar Button Items
        */
        let barBtnClose = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.close))
        let barBtnSave = UIBarButtonItem(title: "Save".localized(), style: .plain, target: self, action:#selector(self.saveProfileEdited))
        navigationItem.leftBarButtonItem = barBtnClose
        navigationItem.rightBarButtonItem = barBtnSave
        
        //Searching id auto-set
        if memberEditing.phoneNumberCountryName.isEmpty == false
        {
            for country in memberEditing.phoneCountries
            {
                let arrayCountrySeparated = country.name.components(separatedBy: "+")
                let lada =  memberEditing.phoneNumberCountryName.replacingOccurrences(of: "+", with: "")
            
                if lada == arrayCountrySeparated[1]
                {
                    idCountry = country.id
                }
            }
        }
        
    }
    
    // MARK: TOOLBAR KEYBOARD
    func loadToolBar() -> UIToolbar
    {
        let toolBarKeyboard = UIToolbar()
        toolBarKeyboard.barStyle = UIBarStyle.blackOpaque;
        toolBarKeyboard.barTintColor = UIColor.colorForNavigationController()
        
        let barBtnDone = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(self.closeKeyboard))
        barBtnDone.tintColor = UIColor.white
        
        toolBarKeyboard.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            barBtnDone]
        
        toolBarKeyboard.sizeToFit()
        
        return toolBarKeyboard
    }
    
    func closeKeyboard()
    {
        view.endEditing(true)
    }
    
     // MARK: ACTION BUTTONS
    func close()
    {
        view.endEditing(true)
        dismiss(animated: true) { () -> Void in
        }
    }
    
    func saveProfileEdited()
    {
        view.endEditing(true)
        
        self.memberEditing.position = self.strHolderPosition
        self.memberEditing.email = self.strHolderEmail
        self.memberEditing.phoneNumberArea = self.strHolderPhoneArea
        self.memberEditing.phoneNumberLocal = self.strHolderPhoneLocal
        self.memberEditing.phoneNumberCountryName = self.strHolderPhoneCountry
        
        guard memberEditing.position.isEmpty == false else {
            
            MessageManager.shared.showBar(title: "Warning".localized(),
                subtitle: "What's your job position in the company?".localized(),
                type: .warning,
                fromBottom: false)
            
            self.memberEditing.position = self.backupPosition
            
            return
        }
        
        guard memberEditing.email.isValidEmail() && memberEditing.email.isValidShmEmail() else {
            
            MessageManager.shared.showBar(title: "Warning".localized(),
                subtitle: "Email account is incorrect".localized(),
                type: .warning,
                fromBottom: false)
            
            self.memberEditing.email = self.backupEmail
            
            return
        }
        
        guard memberEditing.phoneNumberCountryName.isEmpty == false else {
            
            MessageManager.shared.showBar(title: "Warning".localized(),
                subtitle: "Phone number country is incorrect".localized(),
                type: .warning,
                fromBottom: false)
            
            self.memberEditing.phoneNumberCountryName = self.backupPhoneCountry
            
            return
        }
        
        guard memberEditing.phoneNumberArea.isEmpty == false else {
            
            MessageManager.shared.showBar(title: "Warning".localized(),
                subtitle: "Phone number area is incorrect".localized(),
                type: .warning,
                fromBottom: false)
            
            self.memberEditing.phoneNumberArea = self.backupPhoneArea
            
            return
        }
        
        guard memberEditing.phoneNumberLocal.isEmpty == false else {
            
            MessageManager.shared.showBar(title: "Warning".localized(),
                subtitle: "Phone number local is incorrect".localized(),
                type: .warning,
                fromBottom: false)
            
            self.memberEditing.phoneNumberLocal = self.backupPhoneLocal
            
            return
        }
        
        //Request Object
        var wsObject: [String : Any]  = [:]
        wsObject["AccountId"] = memberEditing.id
        wsObject["Department"] = memberEditing.department
        wsObject["FirstName"] = memberEditing.name
        wsObject["LastName"] = memberEditing.lastName
        wsObject["Email"] = memberEditing.email
        wsObject["PhoneNumberArea"] = memberEditing.phoneNumberArea
        wsObject["PhoneNumberCountry"] = idCountry
        wsObject["PhoneNumberCountryName"] = memberEditing.phoneNumberCountryName
        wsObject["PhoneNumberLocal"] = memberEditing.phoneNumberLocal
        wsObject["JobTitle"] = memberEditing.position
        wsObject["ThumbnailUrl"] = memberEditing.thumbnailUrl
        wsObject["Username"] = memberEditing.username
        
        let relations = self.vcRelationsTable.arrayRelations.filter({$0.name != "customAdd"})
        wsObject["AccountStakeholderRelations"] = relations.map({$0.getWSObject()})

        //Needs to send an empty array to accomplish with api/member/put
        let stakeholderAccountRelations = [Stakeholder]()
        wsObject["StakeholderAccountRelations"] = stakeholderAccountRelations.map({$0.getWSObject()})

        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        LibraryAPI.shared.profileBO.editMember(wsObject, onSuccess: {
            () -> () in

            MessageManager.shared.hideHUD()
            
            //Reload Profile
            if let memberEdited = self.onMemberEdited {
                memberEdited()
            }
            
            self.dismiss(animated: true) {
                () -> Void in
                MessageManager.shared.showBar(title: "Success".localized(),
                    subtitle:"Profile info has been saved".localized() ,
                    type: .success,
                    fromBottom: false)
            }
            }) { error in

                self.memberEditing.position = self.backupPosition
                self.memberEditing.email = self.backupEmail
                self.memberEditing.phoneNumberArea = self.backupPhoneArea
                self.memberEditing.phoneNumberLocal = self.backupPhoneLocal
                self.memberEditing.phoneNumberCountryName = self.backupPhoneCountry
                
                MessageManager.shared.hideHUD()
                MessageManager.shared.showBar(title: "Error",
                    subtitle: "It wasn't possible to edit member. Try Again".localized(),
                    type: .error,
                    fromBottom: false)
        }
    }

    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 0.1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 7

    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            if indexPath.row == 0 
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilePictureCell") as! ProfilePictureCell
                cell.selectionStyle = .none
                
                //Image
                if imgProfile == nil
                {
                    cell.imgProfile.setImageWith(URL(string: memberEditing.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
                }
                else
                {
                    cell.imgProfile.image = imgProfile
                }
                
                cell.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
                    self.editPicture()
                }))
                
                //Remove leftArrowIcon
                cell.accessoryType = UITableViewCellAccessoryType.none

                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.txtText.delegate = self
 
                switch indexPath.row
                {
                case 1:   //Name
                    cell.txtText.text = memberEditing.username
                    cell.txtText.placeholder = "Name".localized()
                    cell.txtText.keyboardType = UIKeyboardType.default
                    cell.txtText.tag = 1
                    cell.comboStyle(false)
                    
                case 2: //Job Position
                    cell.txtText.text = memberEditing.position
                    cell.txtText.placeholder = "Position".localized()
                    cell.txtText.keyboardType = UIKeyboardType.emailAddress
                    cell.txtText.tag = 2
                    cell.comboStyle(false)
                    
                case 3: //Phone: Country code
                    cell.txtText.text = memberEditing.phoneNumberCountryName
                    cell.txtText.placeholder = "Country".localized()
                    cell.txtText.tag = 3
                    cell.comboStyle(true)
                    
                case 4: //Phone: Area code
                    cell.txtText.text = memberEditing.phoneNumberArea
                    cell.txtText.placeholder = "Phone Number Area (Eg. 81)".localized()
                    cell.txtText.keyboardType = UIKeyboardType.numberPad
                    cell.txtText.tag = 4
                    cell.comboStyle(false)
                    
                case 5: //Phone: Country local number
                    cell.txtText.text = memberEditing.phoneNumberLocal
                    cell.txtText.placeholder = "Phone Number Local".localized()
                    cell.txtText.keyboardType = UIKeyboardType.numberPad
                    cell.txtText.tag = 5
                    cell.comboStyle(false)
                    
                case 6: //Email
                    cell.txtText.text =  memberEditing.email
                    cell.txtText.placeholder = "Email".localized()
                    cell.txtText.keyboardType = UIKeyboardType.emailAddress
                    cell.txtText.tag = 6
                    cell.comboStyle(false)
                    
                default:
                    break
                }
                
                return cell
            }
            
        }
        
        
        let cell = UITableViewCell()
        return cell
 
    }
    
    // MARK: TABLE VIEW DELEGATE
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 44.0
    }
    
    // MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        let pointInTable = textField.convert(textField.bounds.origin, to: self.tableView)
        textFieldIndexPath = self.tableView.indexPathForRow(at: pointInTable)
        textField.keyboardType = .default
        
        if textField.tag == 3
        {
            textField.inputAccessoryView = loadToolBar()
            
            let pickerPhoneCountries = UIPickerView()
            pickerPhoneCountries.delegate = self
            pickerPhoneCountries.dataSource = self
            pickerPhoneCountries.backgroundColor = UIColor.groupTableViewBackground
            pickerPhoneCountries.showsSelectionIndicator = true
            pickerPhoneCountries.isMultipleTouchEnabled = false
            pickerPhoneCountries.isExclusiveTouch = true
            textField.inputView  = pickerPhoneCountries
        }
        

    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField.tag == 2
        {
            if let text = textField.text as String!{
                if text.isEmpty
                {
                    textField.shakeAnimation()
                }
                self.strHolderPosition = text.trimExcessiveSpaces()
            }
        }
        
        else if textField.tag == 3
        {
            if let text = textField.text as String!{
                if text.isEmpty
                {
                    textField.shakeAnimation()
                }
                
                self.strHolderPhoneCountry = text
            }
        }
        else if textField.tag == 4
        {
            if let text = textField.text as String!{
                if text.isEmpty || text.characters.count > 8
                {
                    textField.shakeAnimation()
                }

                self.strHolderPhoneArea = text
            }
        }
        else if textField.tag == 5
        {
            if let text = textField.text as String!{
                if text.isEmpty || text.characters.count > 15
                {
                    textField.shakeAnimation()
                }
                
                self.strHolderPhoneLocal = text
            }
        }
        else if textField.tag == 6
        {
            if let text = textField.text as String!{
                if !text.isValidEmail() || !text.isValidShmEmail()
                {
                    textField.shakeAnimation()
                }
                self.strHolderEmail = text.trimExcessiveSpaces()
            }
        }
    }

    // MARK: DATASOURCE UIPICKERVIEW
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return memberEditing.phoneCountries.count
    }
    
    // MARK: DELEGATE UIPICKERVIEW
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return memberEditing.phoneCountries[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        guard row < self.memberEditing.phoneCountries.count - 1 else {return}
        let cell = tableView.cellForRow(at: textFieldIndexPath) as! TableTextfieldCell
        
        if self.memberEditing.phoneCountries[row].name.isEmpty == false
        {
            let arrayCountryNumber  = memberEditing.phoneCountries[row].name.components(separatedBy: "+")
            cell.txtText.text = "+" + arrayCountryNumber[1]
            idCountry = memberEditing.phoneCountries[row].id
        }
    }
    
    // MARK: CHANGE PICTURE
    func editPicture()
    {
        let alertSheet = UIAlertController(title:nil, message:nil, preferredStyle: .actionSheet)
        alertSheet.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler:nil))
        
        alertSheet.addAction(UIAlertAction(title: "Take Photo".localized(), style: .default, handler: {
            (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                imagePicker.cameraDevice = UIImagePickerControllerCameraDevice.front
                imagePicker.allowsEditing = false
                imagePicker.modalPresentationStyle = .overCurrentContext
                
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        alertSheet.addAction(UIAlertAction(title: "Choose Existing".localized(), style: .default, handler: {
            (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) {
                
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                imagePicker.allowsEditing = false
                imagePicker.modalPresentationStyle = .overCurrentContext
                
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        self.present(alertSheet, animated: true, completion: nil)
    }
    
    // MARK: IMAGE PICKER DELEGATE
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        dismiss(animated: true) { () -> Void in
            
            if let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
            {
                UIApplication.shared.isStatusBarHidden = true
                let cropViewController = TOCropViewController(image: selectedImage)
                cropViewController?.delegate = self
                self.present(cropViewController!, animated: true, completion: nil)
            }
        }
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: NSErrorPointer?, contextInfo:UnsafeRawPointer)
    {
        if error != nil
        {
            let alert = UIAlertController(title: "Save Failed", message: "Failed to save image",  preferredStyle: UIAlertControllerStyle.alert)
            let cancelAction = UIAlertAction(title: "OK",  style: .cancel, handler: nil)
            alert.addAction(cancelAction)
        }
    }
    
    // MARK: - NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier! == "RelationsProfileTableViewController"
        {
            self.vcRelationsTable = segue.destination as! RelationsProfileTableViewController
            self.vcRelationsTable.isEdit = true
            self.vcRelationsTable.arrayRelations = self.arrayRelations ?? []
            self.vcRelationsTable.relationsTypes = self.memberEditing.relationsLevel1
            
            let emptyStakeholder = Stakeholder(isDefault: true)
            emptyStakeholder.name = "customAdd"
            self.vcRelationsTable.arrayRelations.insert(emptyStakeholder, at: 0)
        }
    }
}

extension EditProfileViewController : TOCropViewControllerDelegate
{
    func cropViewController(_ cropViewController: TOCropViewController!, didCropTo image: UIImage!, with cropRect: CGRect, angle: Int)
    {
        var editedImage = image
        UIApplication.shared.isStatusBarHidden = false
        self.presentedViewController?.dismiss(animated: false, completion: {
            
            //Indicator
            MessageManager.shared.showLoadingHUD()
            
            if (editedImage?.size.width)! > CGFloat(1000)
            {
                editedImage = image.resizeToWidth(1000)
            }
            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                self.uploadImage(editedImage!)
            }
        })
    }
    
    fileprivate func uploadImage(_ image:UIImage)
    {
        //Create Request Object
        var wsObject: [String : Any]  = [:]
        wsObject["OldFile"] = String()
        wsObject["VersionId"] = 1
        
        if let imageData = UIImagePNGRepresentation(image) as Data! {
            
            let base64String = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            wsObject["NewFile"] = "data:image/png;base64,\(base64String)"
        }
        
        //Upload Photo
        LibraryAPI.shared.profileBO.uploadThumbnail(wsObject, onSuccess: {
            (urlPhoto) -> () in
            
            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                MessageManager.shared.hideHUD()
                
                
                self.memberEditing.thumbnailUrl = urlPhoto
                
                if self.memberEditing.thumbnailUrl.isEmpty == false
                {
                    //Set image in TableViewCell
                    let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! ProfilePictureCell
                    self.imgProfile = image
                    cell.imgProfile.image = self.imgProfile
                }
                else
                {
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "It wasn't possible to update the profile picture. Try again".localized(),
                        type: .error,
                        fromBottom: false)
                }
            }
            
            }, onError: {
                error in
                
                MessageManager.shared.showBar(title: "Error",
                    subtitle: "It wasn't possible to update the profile picture. Try again".localized(),
                    type: .error,
                    fromBottom: false)
                
                let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    
                    MessageManager.shared.hideHUD()
                }
        })
    }

    func cropViewController(_ cropViewController: TOCropViewController!, didFinishCancelled cancelled: Bool)
    {
        UIApplication.shared.isStatusBarHidden = false
        self.presentedViewController?.dismiss(animated: true, completion: nil)
    }
}
