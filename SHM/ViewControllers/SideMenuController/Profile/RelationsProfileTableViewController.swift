//
//  RelationsProfileTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 12/16/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class RelationsProfileTableViewController: UITableViewController
{
    //MARK PROPERTIES & OUTLETS
    var arrayRelations = [Stakeholder]()
    var isEdit = false
    var relationsTypes: [KeyValueObjectWithKeyValueArray]?
    var didUpdateRelations:(()->())?
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableView.hideEmtpyCells()
        tableView.register(UINib(nibName: "RelationTableViewCell", bundle: nil), forCellReuseIdentifier: "RelationTableViewCell")
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - TABLEVIEW DATA SOURCE
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        guard self.isEdit == true else {
            
            return ""
        }
        
        return "Relations".localized()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.arrayRelations.count == 0 && self.isEdit == true
        {
            return 1
        }
        else if self.arrayRelations.count == 0 && self.isEdit == false
        {
            return 0
        }
        else
        {
            return self.arrayRelations.count
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        guard self.isEdit == true && indexPath.row != 0 else {
            
            return false
        }
        
        return true
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        //ACTIONS
        let actionEdit = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Edit".localized() , handler: { (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
            
            //Prepare AddRelation view Information
            let vcContactRelation = Storyboard.getInstanceOf(AddRelationViewController.self)
            vcContactRelation.stakeholder = self.arrayRelations[indexPath.row]
            vcContactRelation.types = self.relationsTypes
           
            let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
            vcContactRelation.defaultRelationStr = !(language == "es") ? "\(self.arrayRelations[indexPath.row].relationshipType1_name!) - \(self.arrayRelations[indexPath.row].relationshipType2_name!)" : "\(self.arrayRelations[indexPath.row].relationshipType1_namEs!) - \(self.arrayRelations[indexPath.row].relationshipType2_namEs!)"
            
            vcContactRelation.onRelationDelete = { [weak self] contactRelationed in
                
                /*Remove stakeholder from array and from table*/
                print("Element at index \(indexPath.row)")
                self?.arrayRelations.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.none)
                
                _ = self?.navigationController?.popViewController(animated: true)
            }
            
            vcContactRelation.onSaveRelation = { [weak self] contactRelationed in
                
                self?.arrayRelations[indexPath.row] = contactRelationed as Stakeholder
                self?.tableView.reloadData()
                _ = self?.navigationController?.popViewController(animated: true)
                
            }
            self.navigationController?.pushViewController(vcContactRelation, animated: true)
            self.tableView.setEditing(false, animated: true)
        })
        actionEdit.backgroundColor = UIColor.blueColorNewPost()
        
        let actionDelete = UITableViewRowAction(style: .default, title: "Delete".localized() , handler: { (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
            
            self.arrayRelations.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.none)
            
            _ = self.navigationController?.popViewController(animated: true)
            self.tableView.setEditing(false, animated: true)
        })
        actionDelete.backgroundColor = UIColor.redUnfollowAction()
        
        return [actionDelete, actionEdit]
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if self.isEdit == true
        {
            if indexPath.row == 0
            {
                let cell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier:"Cell")
                
                //Color for selected cell
                let selectedColor = UIView()
                selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
                cell.selectedBackgroundView = selectedColor
                
                cell.textLabel?.text = "Add a relation".localized()
                cell.imageView?.image = UIImage(named: "Add")
                cell.textLabel?.textColor = UIColor.darkText
                cell.textLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 13)
                
                return cell
            }
            else
            {                                
                let cellRelation = tableView.dequeueReusableCell(withIdentifier: "RelationTableViewCell") as! RelationTableViewCell
                
                //Color for selected cell
                let selectedColor = UIView()
                selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
                cellRelation.selectedBackgroundView = selectedColor
                cellRelation.accessoryView = UIImageView(image: UIImage(named: "iconSwipeDots"))
                
                let stakeholder = self.arrayRelations[indexPath.row]
                let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
                
                cellRelation.lblFullName.text = stakeholder.fullName
                let strJobPosition = stakeholder.jobPosition?.value ?? ""
                cellRelation.lblJobTitle.text = strJobPosition
                cellRelation.lblRelation.text = !(language == "es") ? "\(stakeholder.relationshipType1_name!) - \(stakeholder.relationshipType2_name!)" : "\(stakeholder.relationshipType1_namEs!) - \(stakeholder.relationshipType2_namEs!)"
                
                cellRelation.imgThumbnail.setImageWith(URL(string: stakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
                cellRelation.setupRating(stakeholder)
                
                return cellRelation
            }
        }
        else
        {
            let cellRelation = tableView.dequeueReusableCell(withIdentifier: "RelationTableViewCell") as! RelationTableViewCell
            
            //Color for selected cell
            let selectedColor = UIView()
            selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
            cellRelation.selectedBackgroundView = selectedColor
            
            let stakeholder = self.arrayRelations[indexPath.row]
            let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
            
            cellRelation.lblFullName.text = stakeholder.fullName
            let strJobPosition = stakeholder.jobPosition?.value
            cellRelation.lblJobTitle.text = strJobPosition
            cellRelation.imgThumbnail.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            cellRelation.imgThumbnail.layer.borderWidth = 0.5
            
            cellRelation.lblRelation.text = !(language == "es") ? "\(stakeholder.relationshipType1_name!) - \(stakeholder.relationshipType2_name!)" : "\(stakeholder.relationshipType1_namEs!) - \(stakeholder.relationshipType2_namEs!)"
            
            cellRelation.imgThumbnail.setImageWith(URL(string: stakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
            
            //Rating
            cellRelation.setupRating(stakeholder)

            return cellRelation
        }
    }
    
    fileprivate func pushRatingControlFor(_ stakeholder: Stakeholder)
    {
        let vcRating : RatingViewController = Storyboard.getInstanceFromStoryboard("Rating")
        vcRating.didEndRating = { [weak self] in
            
            self?.didUpdateRelations?()            
        }
        vcRating.stakeholder = stakeholder
        self.present(vcRating, animated: true, completion: nil)
    }
    
    //MARK: - TABLEVIEW DELEGATE
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if self.isEdit == true
        {
            if indexPath.row == 0
            {
                
                let vcStakeholderSearch = Storyboard.getInstanceOf(ContactSearchViewController.self)
                vcStakeholderSearch.isModalView = false
                vcStakeholderSearch.isSearchingSH = true
                vcStakeholderSearch.addContactToInstance = { [weak self] contact in
                    
                    //Check for repeated relations
                    let contactId = (contact as! Stakeholder).id
                    let counterArray  = self?.arrayRelations.filter({$0.id == contactId})
                    if counterArray?.count > 0
                    {
                        MessageManager.shared.showBar(title: "Warning".localized(),
                                                  subtitle: "You already have a relationship with this person".localized(),
                                                  type: .warning,
                                                  fromBottom: false)
                    }
                    else
                    {
                        let vcContactRelation = Storyboard.getInstanceOf(AddRelationViewController.self)
                        vcContactRelation.types = self?.relationsTypes
                        vcContactRelation.stakeholder = contact as! Stakeholder
                        vcContactRelation.isNewRelation = true
                        vcContactRelation.onSaveRelation = { [weak self] contactRelationed in

                            self?.arrayRelations.insert(contactRelationed as Stakeholder, at:1)
                            _ = self?.navigationController?.popViewController(animated: true)
                            let indexPathInsert = IndexPath(row: 1, section: 0)
                            self?.tableView.insertRows(at: [indexPathInsert], with: .none)
                            
                        }
                        
                        let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                        DispatchQueue.main.asyncAfter(deadline: delayTime){
                            self?.navigationController?.pushViewController(vcContactRelation, animated: true)    
                        }
                    }
                }
                navigationController?.pushViewController(vcStakeholderSearch, animated: true)
            }
        }
        else
        {
            guard indexPath.row < self.arrayRelations.count else {return}
            let stakeholderSelected = self.arrayRelations[indexPath.row]
            if stakeholderSelected.id != 0
            {
                //Workflow iPad / iPhone
                if DeviceType.IS_ANY_IPAD == true
                {
                    let vcStakeholderDetail = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
                    vcStakeholderDetail.stakeholder = stakeholderSelected
                    self.navigationController?.pushViewController(vcStakeholderDetail, animated: true)
                }
                else
                {
                    let vcSHDetail: SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                    vcSHDetail.stakeholder = stakeholderSelected
                    self.navigationController?.pushViewController(vcSHDetail, animated: true)
                }

            }
            else
            {
                MessageManager.shared.showBar(title: "Warning".localized(),
                                              subtitle: "Your selection is not a stakeholder".localized(),
                                              type: .warning,
                                              fromBottom: false)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 82
    }
}

