//
//  CalendarListViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 9/14/15.
//  Copyright © 2015 Definity First. All rights reserved.
//
import UIKit
import Spruce

enum RelevantEventType : String
{
    case Birthday = "BIRTHDAY"
    case Anniversary = "ANNIVERSARY"
}

class CalendarListViewController: UIViewController, UITabBarDelegate, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate
{
    @IBOutlet weak var tableEvents: UITableView!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var constraintBottomTable: NSLayoutConstraint!
    @IBOutlet weak var tabBarItemCalendarView: UITabBarItem!
    @IBOutlet weak var tabBarItemCalendarViewFromiPad: UITabBarItem!
    
    var refreshControl = UIRefreshControl()
    var activityIndicator = UIActivityIndicatorView()
    var arrayCalendarList = [DateList]()
    var arrayRelevantDates = [RelevantDate]()
    var showsGeography = false
    var didReloadGeoList:(()->())?
//    var arrayBackupToSearch = [DateList]()
    
    var ultimateDate = Date().getStartOfDay().convertToRequestString()
    var page = 1
    var loadMorePages = true
    var activeBlueForIpad = false

    
    //Flags
    var shouldResetCalendarList = false
    var areBirthdaysUpdatedInCalendar = false
    var showsInGeography = false
    //Dot Colors
    let dotNormal = UIImage.getImageWithColor(UIColor.yellowPost(), size: CGSize(width: 12, height: 12))
    let dotBirthday = UIImage.getImageWithColor(UIColor.greenBirthday(), size: CGSize(width: 12, height: 12))
    let dotRelevantDate = UIImage.getImageWithColor(UIColor.blueImportantDate(), size: CGSize(width: 12, height: 12))
    
    var onHeightChanged: ((_ newHeight: CGFloat) -> (CGFloat))?
    var tableHeight: CGFloat = 1000
    fileprivate(set) internal var hasFocus = false
    
    var isFullCalendarList = true
    var loadByProjectId: Int {
        didSet{
            isFullCalendarList = false
        }
    }
    var loadByStakeholderId: Int
        {
        didSet{
            isFullCalendarList = false
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.loadByStakeholderId = 0
        self.loadByProjectId = 0
        super.init(coder: aDecoder)
    }
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        
        //Setup for geography detail
        guard self.showsInGeography == false else {
            
            self.tableEvents.reloadData()
            self.tableEvents.dismissBackgroundMessage()
            return
        }
        
        //Get all Calendar events
        if self.isFullCalendarList == true
        {
            self.loadCalendarList()
        }
        else
        {
            self.loadCalendarListById()
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        print("Deinit: CalendarListViewController")
    }
    
    //MARK: CONFIG UI TO DETAIL
    func configureForEmbed()
    {
        self.tabBar.isHidden = true
        self.constraintBottomTable.constant = -49
        //        tableEvents.scrollEnabled = false
        //        tableEvents.bounces = false
    }
    
    func focus()
    {
        self.hasFocus = true
        
        guard self.onHeightChanged != nil else {
        
            return
        }
        
        self.setEventsTableHeight()
    }
    
    func unfocus()
    {
        hasFocus = false
    }
    
    // MARK: INITIAL CONFIG
    func loadConfig()
    {
        self.view.backgroundColor = UIColor.white
        self.localize()
        
        //UI for iPad?
        if activeBlueForIpad == true
        {
            self.view.backgroundColor =  UIColor.blueSky()
            self.setBorder()
        }
        
        //Bar Button Items
        if isFullCalendarList == true
        {
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
            let batBtnActivity = UIBarButtonItem(customView: activityIndicator)
            
            let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
            navigationItem.rightBarButtonItems = [barBtnCompose, batBtnActivity]
        }
        
        //Refresh control
        refreshControl = UIRefreshControl()
        refreshControl.setLastUpdate()
        refreshControl.addTarget(self, action: #selector(self.resetAndLoadCalendar), for: UIControlEvents.valueChanged)
        tableEvents.addSubview(refreshControl)
    
        /**
        Register Custom Cell
        */
        tableEvents.register(UINib(nibName: "CalendarPostTableCell", bundle: nil), forCellReuseIdentifier: "CalendarPostTableCell")
        tableEvents.backgroundColor = (activeBlueForIpad == true) ? UIColor.blueSky() : UIColor.white

        
        /** Loading message */
        tableEvents.hideEmtpyCells()
        tableEvents.displayBackgroundMessage("Loading...".localized(),
            subMessage: "")
        
        //Tab bar in geography
        self.tabBar.isHidden = self.showsInGeography == true ? true : false
    }
    
    fileprivate func localize()
    {
        //Title
        self.title = "CALENDAR".localized()
        
        if DeviceType.IS_ANY_IPAD == false
        {
            self.tabBarItemCalendarView.title = "Calendar View".localized()
        }
        else
        {
            guard self.tabBarItemCalendarViewFromiPad != nil else {return}
            self.tabBarItemCalendarViewFromiPad.title = "Calendar View".localized()
        }
    }
    
    // MARK: ACTIONS
    func resetAndLoadCalendar()
    {
        page = 1
        ultimateDate = Date().getStartOfDay().convertToRequestString()
        shouldResetCalendarList = true
        areBirthdaysUpdatedInCalendar = false
        loadMorePages = true
        arrayRelevantDates = []
        
        guard self.showsGeography == true else {
            
            self.didReloadGeoList?()
            return
        }
        if isFullCalendarList == true
        {
            loadCalendarList()
        }
        else
        {
            loadCalendarListById()
        }
    }
    
    func createNewPost()
    {
        let vcNewPost = NewPostViewController()
        
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = (DeviceType.IS_ANY_IPAD) ?  UIModalPresentationStyle.formSheet :  UIModalPresentationStyle.overFullScreen
        present(navController, animated: true, completion: nil)
    }
    
    // MARK; - TO GO FULL HEIGHT
    
    func reloadEventsTable()
    {
        self.tableEvents.reloadData()
        self.tableEvents.isScrollEnabled = false
        self.tableEvents.bounces = false
        
        guard tableEvents.numberOfSections > 0 && tableEvents.numberOfRows(inSection: tableEvents.numberOfSections) > 0 else {
            
            self.tableHeight = 0
            return
        }
        
        let lastIndexPath = IndexPath(row: self.tableEvents.numberOfRows(inSection: tableEvents.numberOfSections - 1) - 1, section: tableEvents.numberOfSections - 1)
        
        self.tableEvents.scrollToRow(at: lastIndexPath, at: UITableViewScrollPosition.bottom, animated: false)
        
        DispatchQueue.main.async(execute: {
            
            self.tableHeight = self.tableEvents.contentSize.height
            
            if self.hasFocus == true
            {
                self.setEventsTableHeight()
            }
        });
    }
    
    func setEventsTableHeight()
    {
        // TOTAL HEGHT = TABLE HEIGHT + HEADER HEIGHT
        let recommendedHeight = onHeightChanged?(tableHeight) ?? 0
        
        if recommendedHeight > self.tableHeight
        {
            self.tableHeight = recommendedHeight
        }
        
        self.view.frame.size.height = self.tableHeight
        self.view.layoutIfNeeded()
    }
    
    // MARK: GET EVENT
    func loadCalendarList()
    {
        //Indicator
        activityIndicator.startAnimating()
        
        //Request Object
        var wsObject: [String : Any]  = [:]
        wsObject["Page"] = page
        wsObject["LatestDate"] = ultimateDate
        
        LibraryAPI.shared.calendarBO.getCalendarData(wsObject, onSuccess: {
            (dateList, latestDate, page, loadMore) -> () in
            
            var dateList = dateList
            
            self.activityIndicator.stopAnimating()
            self.refreshControl.endRefreshing()
            self.refreshControl.setLastUpdate()
            self.loadMorePages = loadMore
            
            
            //Reset list when pull to refresh
            if self.shouldResetCalendarList == true
            {
                self.arrayCalendarList.removeAll()
                self.shouldResetCalendarList = false
            }
            
            
            //Saving Relevant Dates (first time)
            if self.arrayRelevantDates.count == 0
            {
                for objectDate in dateList
                {
                    for relevant in objectDate.relevantDates where relevant.type == 1
                    {
                        self.arrayRelevantDates.append(relevant)
                    }
                }
            }
            
            //Verify If Stakeholders' birthday are updated in the list
            if self.areBirthdaysUpdatedInCalendar == false
            {
                for birthday in self.arrayRelevantDates
                {
                    //Components Date
                    let calendar = Calendar.current
                    var componentsBirthday = calendar.dateComponents([.year, .month, .day], from: birthday.date!)
                    let componentsToday = calendar.dateComponents([.year, .month, .day], from: Date())
                    componentsBirthday.year = componentsToday.year
                    
                    //Update date
                    birthday.date = calendar.date(from: componentsBirthday)
                    
                    //Verify that the date is the newest
                    if birthday.date!.compare(Date().getStartOfDay()) == ComparisonResult.orderedAscending
                    {
                        //Adding to the next year
                        componentsBirthday.year = componentsToday.year! + 1
                        birthday.date = calendar.date(from: componentsBirthday)
                    }
                    
                    //Create custom object
                    let customItem = DateList()
                    let customEvent = RelevantDate()
                    
                    customEvent.isFreeDateString = birthday.isFreeDateString
                    customEvent.type = birthday.type
                    customEvent.title = birthday.title
                    customEvent.date = birthday.date
                    customEvent.stringValue = "\(customEvent.date!)"
                    
                    customItem.date = customEvent.date!
                    customItem.relevantDates.append(customEvent)
                    
                    customItem.joinEvents()
                    dateList.append(customItem)
                }
                
                self.areBirthdaysUpdatedInCalendar = true
            }
            
            //Filtering to show event since Today
            dateList = dateList.filter({
                $0.date.compare(Date().getStartOfDay()) == ComparisonResult.orderedDescending
                    || $0.date.compare(Date().getStartOfDay()) == ComparisonResult.orderedSame
            })
            
            //Sorting ...
            dateList = dateList.sorted(by: {
                $0.date.compare($1.date) == ComparisonResult.orderedAscending
            })
            
            self.page = page
            self.ultimateDate = latestDate
            self.arrayCalendarList += dateList
            self.tableEvents.reloadData()
            
            //Spruce framework Animation
            DispatchQueue.main.async {
                
                self.tableEvents.spruce.animate([.contract(.moderately)], animationType: SpringAnimation(duration: 0.7))
            }
            
            //Empty State
            if self.arrayCalendarList.count > 0
            {
                self.tableEvents.dismissBackgroundMessage()
            }
            else
            {
                self.tableEvents.displayBackgroundMessage("No Events Found".localized(),
                                                          subMessage: "")
            }
            
        }) { error in
            
            self.activityIndicator.stopAnimating()
            self.refreshControl.endRefreshing()
            self.refreshControl.setLastUpdate()
            self.tableEvents.displayBackgroundMessage("Error",
                                                      subMessage: "Pull to refesh".localized())
        }
    }
    
    func loadCalendarListById()
    {
        //Indicator
        activityIndicator.startAnimating()
        
        //Request Object
        var wsObject: [String : Any]  = [:]
        wsObject["Page"] = page
        wsObject["LatestDate"] = ultimateDate
        wsObject["StakeholderId"] = loadByStakeholderId
        wsObject["ProjectId"] = loadByProjectId
        
        LibraryAPI.shared.calendarBO.getCalendarDataById(wsObject, onSuccess: {
            (dateList, latestDate, page, loadMore) -> () in
            
            var dateList = dateList
            
            self.activityIndicator.stopAnimating()
            self.refreshControl.endRefreshing()
            self.refreshControl.setLastUpdate()
            self.loadMorePages = loadMore
            
            
            //Reset list when pull to refresh
            if self.shouldResetCalendarList == true
            {
                self.arrayCalendarList.removeAll()
                self.shouldResetCalendarList = false
            }
            
            
            //Saving Relevant Dates (first time)
            if self.arrayRelevantDates.count == 0
            {
                for objectDate in dateList
                {
                    for relevant in objectDate.relevantDates where relevant.type == 1
                    {
                        self.arrayRelevantDates.append(relevant)
                    }
                }
            }
            
            //Verify If Stakeholders' birthday are updated in the list
            if self.areBirthdaysUpdatedInCalendar == false
            {
                for birthday in self.arrayRelevantDates
                {
                    //Components Date
                    let calendar = Calendar.current
                    var componentsBirthday = (calendar as NSCalendar).components([.year, .month, .day], from: birthday.date!)
                    let componentsToday = (calendar as NSCalendar).components([.year, .month, .day], from: Date())
                    componentsBirthday.year = componentsToday.year
                    
                    //Update date
                    birthday.date = calendar.date(from: componentsBirthday)
                    
                    //Verify that the date is the newest
                    if birthday.date!.compare(Date().getStartOfDay()) == ComparisonResult.orderedAscending
                    {
                        //Adding to the next year
                        componentsBirthday.year = componentsToday.year! + 1
                        birthday.date = calendar.date(from: componentsBirthday)
                    }
                    
                    //Create custom object
                    let customItem = DateList()
                    let customEvent = RelevantDate()
                    
                    customEvent.isFreeDateString = birthday.isFreeDateString
                    customEvent.type = birthday.type
                    customEvent.title = birthday.title
                    customEvent.date = birthday.date
                    customEvent.stringValue = "\(customEvent.date!)"
                    
                    customItem.date = customEvent.date!
                    customItem.relevantDates.append(customEvent)
                    
                    customItem.joinEvents()
                    dateList.append(customItem)
                }
                
                self.areBirthdaysUpdatedInCalendar = true
            }

            //Filtering to show event since Today
            dateList = dateList.filter({
                $0.date.compare(Date().getStartOfDay()) == ComparisonResult.orderedDescending
                    || $0.date.compare(Date().getStartOfDay()) == ComparisonResult.orderedSame
            })
            
            //Sorting ...
            dateList = dateList.sorted(by: {
                $0.date.compare($1.date) == ComparisonResult.orderedAscending
            })
            
            self.page = page
            self.ultimateDate = latestDate
            self.arrayCalendarList += dateList
            
//            self.arrayBackupToSearch.removeAll()
//            self.arrayBackupToSearch = self.arrayCalendarList

            if self.isFullCalendarList == false
            {
                self.reloadEventsTable()
            }
            else
            {
                self.tableEvents.reloadData()
            }
            
            if self.arrayCalendarList.count > 0
            {
                self.tableEvents.dismissBackgroundMessage()
            }
            else
            {
                self.tableEvents.displayBackgroundMessage("No Events Found".localized(),
                    subMessage: "")
            }
            
            }) { error in
                
                self.activityIndicator.stopAnimating()
                self.refreshControl.endRefreshing()
                self.refreshControl.setLastUpdate()
                self.tableEvents.displayBackgroundMessage("Error",
                    subMessage: "Pull to refresh".localized())
        }
    }
    
    // MARK: TABLE VIEW DATASOURCE
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.arrayCalendarList.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        guard section < self.arrayCalendarList.count else {return UIView()}
        let lblSectionTitle = UILabel()
        lblSectionTitle.text = arrayCalendarList[section].date.getStringStyleMedium()
        lblSectionTitle.textAlignment = NSTextAlignment.center
        lblSectionTitle.backgroundColor = UIColor.groupTableViewBackground
        lblSectionTitle.textColor = UIColor.blackAsfalto()
        lblSectionTitle.font = UIFont(name: "HelveticaNeue-Light", size: 16)
        
        return lblSectionTitle
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 35.0 
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayCalendarList[section].arrayEvents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let item = arrayCalendarList[indexPath.section].arrayEvents[indexPath.row]
       
        if item.isKind(of: Post.self)
        {
            let eventPost = item as! Post
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarPostTableCell") as! FeedTableCell
            cell.tvText.delegate = self
            
            cell.iconEvent.image = dotNormal
            cell.iconEvent.layer.cornerRadius = 6
            cell.iconEvent.clipsToBounds = true
            
            cell.tvText.text = eventPost.title
            
            cell.cellForPostEvent(cell, post: eventPost)
            cell.onStakeholderTap = { [weak self] stakeholder in
                
                //Enabled or Disabled
                guard stakeholder.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: stakeholder.fullName, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                    
                    return
                }
                
                let actordetailVC : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                actordetailVC.stakeholder = stakeholder
                
                self?.navigationController?.pushViewController(actordetailVC, animated: true)
            }
            
            cell.onAllStakeholdersTap = { [weak self] Void in
                
                let stakeholdersTableVC = StakeholdersTableViewController.getNewInstance()
                stakeholdersTableVC.stakeholders = eventPost.stakeholders
                
                let vcModal = ModalViewController(subViewController: stakeholdersTableVC)
                self?.present(vcModal, animated:true, completion: nil)
            }
            
            cell.onTextViewTap = { [weak self] Void in
                
                let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController.self)
                vcDetailPost.post = eventPost
                
                self?.navigationController?.pushViewController(vcDetailPost, animated: true)
            }
            
            return cell
        }
        else
        {
            let event = item as! RelevantDate
            
            let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier:"Cell")
            
            switch event.type!
            {
            case 1:
                cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
                cell.textLabel?.textColor = UIColor.blackAsfalto()
                cell.textLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
                cell.textLabel?.numberOfLines = 2
                
                let finalText = RelevantEventType.Birthday.rawValue.localized() + " " +  event.title
                cell.textLabel?.text = finalText
                
                /** Style Text */
                let attributedString = NSMutableAttributedString(string: cell.textLabel!.text!)
                attributedString.setBoldAndColor(RelevantEventType.Birthday.rawValue.localized(), color: UIColor.greenBirthday(), size: 10)
                cell.textLabel?.attributedText = attributedString
                
                //type
                cell.imageView?.image = dotBirthday
                cell.imageView?.layer.cornerRadius = 6
                cell.imageView?.clipsToBounds = true
            case 2:
                cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
                cell.textLabel?.textColor = UIColor.blackAsfalto()
                cell.textLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
                cell.textLabel?.numberOfLines = 5
                
                cell.textLabel?.text = event.title
                
                //type
                cell.imageView?.image = dotRelevantDate
                cell.imageView?.layer.cornerRadius = 6
                cell.imageView?.clipsToBounds = true
            default:
                break
            }
            
            return cell
        }
    }
    
    // MARK: TABLE VIEW DELEGATE
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = arrayCalendarList[indexPath.section].arrayEvents[indexPath.row]
        
        if item.isKind(of: Post.self)
        {
            let eventPost = item as! Post

            let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController.self)
            vcDetailPost.post = eventPost
            navigationController?.pushViewController(vcDetailPost, animated: true)
        }
        else
        {
            let event = item as! RelevantDate
            
            if event.type! == 1
            {
                //Birthday
                let alert = UIAlertController(title: "Add Event".localized(), message: "Would you like to save this date in your device calendar".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Save".localized(), style: UIAlertActionStyle.default, handler: {
                    (action) in
                    
                    let finalText = RelevantEventType.Birthday.rawValue.localized() + ": " +  event.title
                    CalendarManager.sharedInstance.createEventWithString(finalText, date:event.date!)
                }))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                //Relevant date
                let alert = UIAlertController(title:"Description".localized(), message:event.title, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Add Event".localized(), style: UIAlertActionStyle.default, handler: {
                    (action) in
                    
                    let finalText = "Relevant Date: ".localized() +  event.title
                    CalendarManager.sharedInstance.createEventWithString(finalText, date:event.date!)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        //Color
        cell.backgroundColor = (activeBlueForIpad == true)  ? UIColor.blueSky() : UIColor.white
        
        //Last Cell
        if (indexPath.section + 1 ==  arrayCalendarList.count)
            && (indexPath.row == arrayCalendarList[indexPath.section].arrayEvents.count - 1)
        {
            print("ultima celda")
            print("Page \(page) Fecha \(ultimateDate)")
            
            if isFullCalendarList == true
            {
                if loadMorePages == true
                {
                    guard self.showsInGeography == false else {return}
                    loadCalendarList()
                }
            }
            else
            {
                if loadMorePages == true
                {
                    loadCalendarListById()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        guard indexPath.section < arrayCalendarList.count else{return 70}
        let item = arrayCalendarList[indexPath.section].arrayEvents[indexPath.row]
        
        if item.isKind(of: Post.self)
        {
            return 115
        }
        else
        {
            let event = item as! RelevantDate
            
            if event.type! == 1
            {
                return 44
            }
            else
            {
                return 70
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return CGFloat.leastNormalMagnitude
    }
    
    // MARK: TABBAR DELEGATE (FeedTableCell)
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        //Selected item color equal not selected color
        tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        
        switch item.tag
        {
        case 1:
            LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.GetCalendar)
            let vcSource = self
            let vcDestination = Storyboard.getInstanceOf(CalendarViewController.self)
            vcDestination.shouldShowColorLabelDescription = true
            let navigationController = vcSource.navigationController!
            
            UIView.beginAnimations("LeftFlip", context: nil)
            UIView.setAnimationDuration(1)
            UIView.setAnimationCurve(.easeInOut)
            UIView.setAnimationTransition(.flipFromLeft, for: vcSource.view.superview!, cache: true)
            UIView.commitAnimations()
            
            var controllerStack = navigationController.viewControllers
            controllerStack.removeLast()
            controllerStack.append(vcDestination)
            navigationController.setViewControllers(controllerStack, animated: true)
        default:
            break
        }
    }
    
    // MARK: TEXTVIEW DELEGATE (FeedTableCell)
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
    {
        let arrayUrl = URL.description.components(separatedBy: "_")
        
        if arrayUrl.count > 1
        {
            let type = arrayUrl[0]
            
            switch type
            {
            case "s":
                //Go to Stakeholder detail
                let stakeholderLinked: Stakeholder! = Stakeholder(isDefault: true)
                stakeholderLinked.id = Int(arrayUrl[1])!
                stakeholderLinked.isEnable = Int(arrayUrl[2])! == 1 ? true : false
                
                let firstname = arrayUrl[3]
                
                //Enabled or Disabled
                guard stakeholderLinked.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: firstname, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                    
                    return true
                }
                
                let vcDetailStakeholders : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                vcDetailStakeholders.stakeholder = stakeholderLinked
                navigationController?.pushViewController(vcDetailStakeholders, animated: true)
            case "p":
                //Go to Project detail
                let projectLinked: Project! = Project(isDefault: true)
                projectLinked.id = Int(arrayUrl[1])!
                projectLinked.isEnable = Int(arrayUrl[2])! == 1 ? true : false
                
                //Enabled or Disabled
                guard projectLinked.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: nil, subtitle: "This project is disabled".localized(), type: .info, fromBottom: false)
                    
                    return true
                }
                
                //Go to detail
                let vcDetailProject : DetailProjectViewController = Storyboard.getInstanceFromStoryboard("Projects")
                vcDetailProject.project = projectLinked
                navigationController?.pushViewController(vcDetailProject, animated: true)
                
            case "d":
                //Add Event to Calendar
                let alert = UIAlertController(title: "Add Event".localized(), message: "Would you like to save this date in your device calendar".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Save".localized(), style: UIAlertActionStyle.default, handler: {
                    (action) in
                    
                    for objectList in self.arrayCalendarList
                    {
                        for thePost in objectList.posts where thePost.id == Int(arrayUrl[2])!
                        {
                                for theEvent in thePost.events where theEvent.id == Int(arrayUrl[1])!
                                {
                                        CalendarManager.sharedInstance.createEvent(thePost.title.getEditedTitleForPost(thePost, truncate: false), event: theEvent)
                                }
                        }
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            case "g":
                let geographyId = Int(arrayUrl[1])!
                if DeviceType.IS_ANY_IPAD == false
                {
                    let vcGeoDetail:GeographyDetailViewController = UIStoryboard(name: "Geography", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetailViewController") as! GeographyDetailViewController
                    vcGeoDetail.geographyId = geographyId
                    self.navigationController?.pushViewController(vcGeoDetail, animated: true)
                }
                else
                {
                    let vcGeoDetail:GeographyDetail_iPad = UIStoryboard(name: "GeographyPad", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetail_iPad") as! GeographyDetail_iPad
                    vcGeoDetail.geographyId = geographyId
                    self.navigationController?.pushViewController(vcGeoDetail, animated: true)
                }
            default:
                break
            }
        }
        
        return true
    }
}
