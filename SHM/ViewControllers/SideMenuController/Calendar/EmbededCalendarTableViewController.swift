//
//  EmbededCalendarTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 11/10/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class EmbededCalendarTableViewController: UITableViewController
{

    //MARK: VARIABLES AND OUTLETS
    var ID = 0{
        didSet{
            self.loadCalendarListById()
        }
    }
    var contentType: CalendarType = .StakeholderDetail
    var refresh = UIRefreshControl()
    var activityIndicator = UIActivityIndicatorView()
    var arrayCalendarList = [DateList]()
    var arrayRelevantDates = [RelevantDate]()
    var backupArrayCalendarList = [DateList]()
    var ultimateDate = Date().getStartOfDay().convertToRequestString()
    var page = 1
    var loadMorePages = true
    
    //Flags
    var areBirthdaysUpdatedInCalendar = false
    var showsInGeography = false
    //Dot Colors
    let dotNormal = UIImage.getImageWithColor(UIColor.yellowPost(), size: CGSize(width: 12, height: 12))
    let dotBirthday = UIImage.getImageWithColor(UIColor.greenBirthday(), size: CGSize(width: 12, height: 12))
    let dotRelevantDate = UIImage.getImageWithColor(UIColor.blueImportantDate(), size: CGSize(width: 12, height: 12))
    
    
    // MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    // MARK: INITIAL CONFIG
    func loadConfig()
    {
        self.view.backgroundColor = UIColor.white

        //Refresh control
        refresh = UIRefreshControl()
        refresh.setLastUpdate()
        refresh.addTarget(self, action: #selector(self.resetAndLoadCalendar), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(refresh)
        
        /**
         Register Custom Cell
         */
        self.tableView.register(UINib(nibName: "CalendarPostTableCell", bundle: nil), forCellReuseIdentifier: "CalendarPostTableCell")
        self.tableView.backgroundColor = UIColor.white
        
        
        /** Loading message */
        self.tableView.hideEmtpyCells()
        self.displayBackgroundMessage("Loading...".localized(),
                                             subMessage: "")
    }
    
    //MARK: FILTER
    func filterByText(_ text: String)
    {
        let term = text.trim()
        guard term.isEmpty == false else {
            
            //Reload calendar
            self.arrayCalendarList = self.backupArrayCalendarList
            self.tableView.reloadData()
            return
        }
        
        //Filter
        self.arrayCalendarList = self.backupArrayCalendarList.filter({ (item) -> Bool in
            if let relevantDates = (item.arrayEvents as? [RelevantDate])?.filter({$0.title.containsString(text)}) {
                if relevantDates.count > 0
                {
                    return true
                }
                return false
            }
            else if let posts = (item.arrayEvents as? [Post])?.filter({$0.title.containsString(text)}) {
                if posts.count > 0
                {
                    return true
                }
                return false
            }
            return false
        })
        self.tableView.reloadData()
    }
    
    //MARK: WEB SERVICES
    func loadCalendarListById()
    {
        //Indicator
        activityIndicator.startAnimating()
        
        //Request Object
        var wsObject: [String : Any]  = [:]
        wsObject["Page"] = page
        wsObject["LatestDate"] = ultimateDate
        wsObject["StakeholderId"] = self.contentType == .StakeholderDetail ? self.ID : 0
        wsObject["ProjectId"] = self.contentType == .ProjectDetail ? self.ID : 0
        
        LibraryAPI.shared.calendarBO.getCalendarDataById(wsObject, onSuccess: {
            (dateList, latestDate, page, loadMore) -> () in
            
            var dateList = dateList
            
            self.activityIndicator.stopAnimating()
            self.refresh.endRefreshing()
            self.refresh.setLastUpdate()
            self.loadMorePages = loadMore
            
 
            
            self.page = page
            self.ultimateDate = latestDate
            self.arrayCalendarList += dateList
            self.backupArrayCalendarList = self.arrayCalendarList
           self.tableView.reloadData()
            
            if self.arrayCalendarList.count > 0
            {
                self.dismissBackgroundMessage()
            }
            else
            {
                self.displayBackgroundMessage("No Events Found".localized(),
                                                          subMessage: "")
            }
            
        }) { error in
            
            self.activityIndicator.stopAnimating()
            self.refresh.endRefreshing()
            self.refresh.setLastUpdate()
            self.displayBackgroundMessage("Error",
                                                      subMessage: "Pull to refresh".localized())
        }
    }
    
    // MARK: ACTIONS
    func resetAndLoadCalendar()
    {
        page = 1
        ultimateDate = Date().getStartOfDay().convertToRequestString()
        self.arrayCalendarList.removeAll()
        self.tableView.reloadData()
        areBirthdaysUpdatedInCalendar = false
        loadMorePages = true
        arrayRelevantDates = []
        loadCalendarListById()
    }
    
    // MARK: TABLE DATASOURCE/DELEGATES
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.arrayCalendarList.count
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        guard section < self.arrayCalendarList.count else {return UIView()}
        let lblSectionTitle = UILabel()
        lblSectionTitle.text = arrayCalendarList[section].date.getStringStyleMedium()
        lblSectionTitle.textAlignment = NSTextAlignment.center
        lblSectionTitle.backgroundColor = UIColor.groupTableViewBackground
        lblSectionTitle.textColor = UIColor.blackAsfalto()
        lblSectionTitle.font = UIFont(name: "HelveticaNeue-Light", size: 16)
        
        return lblSectionTitle
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 35.0
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayCalendarList[section].arrayEvents.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let item = arrayCalendarList[indexPath.section].arrayEvents[indexPath.row]
        
        if item.isKind(of: Post.self)
        {
            let eventPost = item as! Post
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarPostTableCell") as! FeedTableCell
            cell.tvText.delegate = self
            
            cell.iconEvent.image = dotNormal
            cell.iconEvent.layer.cornerRadius = 6
            cell.iconEvent.clipsToBounds = true
            
            cell.tvText.text = eventPost.title
            
            cell.cellForPostEvent(cell, post: eventPost)
            cell.onStakeholderTap = { [weak self] stakeholder in
                
                //Enabled or Disabled
                guard stakeholder.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: stakeholder.fullName, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                    
                    return
                }
                
                let actordetailVC : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                actordetailVC.stakeholder = stakeholder
                
                self?.navigationController?.pushViewController(actordetailVC, animated: true)
            }
            
            cell.onAllStakeholdersTap = { [weak self] Void in
                
                let stakeholdersTableVC = StakeholdersTableViewController.getNewInstance()
                stakeholdersTableVC.stakeholders = eventPost.stakeholders
                
                let vcModal = ModalViewController(subViewController: stakeholdersTableVC)
                self?.present(vcModal, animated:true, completion: nil)
            }
            
            cell.onTextViewTap = { [weak self] Void in
                
                let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController.self)
                vcDetailPost.post = eventPost
                
                self?.navigationController?.pushViewController(vcDetailPost, animated: true)
            }
            
            return cell
        }
        else
        {
            let event = item as! RelevantDate
            
            let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier:"Cell")
            
            switch event.type!
            {
            case 1:
                cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
                cell.textLabel?.textColor = UIColor.blackAsfalto()
                cell.textLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
                cell.textLabel?.numberOfLines = 2
                
                let finalText = RelevantEventType.Birthday.rawValue.localized() + " " +  event.title
                cell.textLabel?.text = finalText
                
                /** Style Text */
                let attributedString = NSMutableAttributedString(string: cell.textLabel!.text!)
                attributedString.setBoldAndColor(RelevantEventType.Birthday.rawValue.localized(), color: UIColor.greenBirthday(), size: 10)
                cell.textLabel?.attributedText = attributedString
                
                //type
                cell.imageView?.image = dotBirthday
                cell.imageView?.layer.cornerRadius = 6
                cell.imageView?.clipsToBounds = true
            case 2:
                cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
                cell.textLabel?.textColor = UIColor.blackAsfalto()
                cell.textLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
                cell.textLabel?.numberOfLines = 5
                
                cell.textLabel?.text = event.title
                
                //type
                cell.imageView?.image = dotRelevantDate
                cell.imageView?.layer.cornerRadius = 6
                cell.imageView?.clipsToBounds = true
            default:
                break
            }
            
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = arrayCalendarList[indexPath.section].arrayEvents[indexPath.row]
        
        if item.isKind(of: Post.self)
        {
            let eventPost = item as! Post
            
            let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController.self)
            vcDetailPost.post = eventPost
            navigationController?.pushViewController(vcDetailPost, animated: true)
        }
        else
        {
            let event = item as! RelevantDate
            
            if event.type! == 1
            {
                //Birthday
                let alert = UIAlertController(title: "Add Event".localized(), message: "Would you like to save this date in your device calendar".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Save".localized(), style: UIAlertActionStyle.default, handler: {
                    (action) in
                    
                    let finalText = RelevantEventType.Birthday.rawValue.localized() + ": " +  event.title
                    CalendarManager.sharedInstance.createEventWithString(finalText, date:event.date!)
                }))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                //Relevant date
                let alert = UIAlertController(title:"Description".localized(), message:event.title, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Add Event".localized(), style: UIAlertActionStyle.default, handler: {
                    (action) in
                    
                    let finalText = "Relevant Date: ".localized() +  event.title
                    CalendarManager.sharedInstance.createEventWithString(finalText, date:event.date!)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        //Color
        //cell.backgroundColor = (activeBlueForIpad == true)  ? UIColor.blueSky() : UIColor.white
        cell.backgroundColor = UIColor.white
        //Last Cell
        if (indexPath.section + 1 ==  arrayCalendarList.count)
            && (indexPath.row == arrayCalendarList[indexPath.section].arrayEvents.count - 1)
        {
            if loadMorePages == true
            {
                loadCalendarListById()
            }
        
//            print("ultima celda")
//            print("Page \(page) Fecha \(ultimateDate)")
//
//            if isFullCalendarList == true
//            {
//                if loadMorePages == true
//                {
//                    guard self.showsInGeography == false else {return}
//                    loadCalendarList()
//                }
//            }

        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        guard indexPath.section < arrayCalendarList.count else{return 70}
        let item = arrayCalendarList[indexPath.section].arrayEvents[indexPath.row]
        
        if item.isKind(of: Post.self)
        {
            return 115
        }
        else
        {
            let event = item as! RelevantDate
            
            if event.type! == 1
            {
                return 44
            }
            else
            {
                return 70
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return CGFloat.leastNormalMagnitude
    }
}

// MARK: TEXTVIEW DELEGATE (FeedTableCell)
extension EmbededCalendarTableViewController:UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
    {
        let arrayUrl = URL.description.components(separatedBy: "_")
        
        if arrayUrl.count > 1
        {
            let type = arrayUrl[0]
            
            switch type
            {
            case "s":
                //Go to Stakeholder detail
                let stakeholderLinked: Stakeholder! = Stakeholder(isDefault: true)
                stakeholderLinked.id = Int(arrayUrl[1])!
                stakeholderLinked.isEnable = Int(arrayUrl[2])! == 1 ? true : false
                
                let firstname = arrayUrl[3]
                
                //Enabled or Disabled
                guard stakeholderLinked.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: firstname, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                    
                    return true
                }
                
                let vcDetailStakeholders : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                vcDetailStakeholders.stakeholder = stakeholderLinked
                navigationController?.pushViewController(vcDetailStakeholders, animated: true)
            case "p":
                //Go to Project detail
                let projectLinked: Project! = Project(isDefault: true)
                projectLinked.id = Int(arrayUrl[1])!
                projectLinked.isEnable = Int(arrayUrl[2])! == 1 ? true : false
                
                //Enabled or Disabled
                guard projectLinked.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: nil, subtitle: "This project is disabled".localized(), type: .info, fromBottom: false)
                    
                    return true
                }
                
                //Go to detail
                let vcDetailProject : DetailProjectViewController = Storyboard.getInstanceFromStoryboard("Projects")
                vcDetailProject.project = projectLinked
                navigationController?.pushViewController(vcDetailProject, animated: true)
                
            case "d":
                //Add Event to Calendar
                let alert = UIAlertController(title: "Add Event".localized(), message: "Would you like to save this date in your device calendar".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Save".localized(), style: UIAlertActionStyle.default, handler: {
                    (action) in
                    
                    for objectList in self.arrayCalendarList
                    {
                        for thePost in objectList.posts where thePost.id == Int(arrayUrl[2])!
                        {
                            for theEvent in thePost.events where theEvent.id == Int(arrayUrl[1])!
                            {
                                CalendarManager.sharedInstance.createEvent(thePost.title.getEditedTitleForPost(thePost, truncate: false), event: theEvent)
                            }
                        }
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            case "g":
                let geographyId = Int(arrayUrl[1])!
                if DeviceType.IS_ANY_IPAD == false
                {
                    let vcGeoDetail:GeographyDetailViewController = UIStoryboard(name: "Geography", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetailViewController") as! GeographyDetailViewController
                    vcGeoDetail.geographyId = geographyId
                    self.navigationController?.pushViewController(vcGeoDetail, animated: true)
                }
                else
                {
                    let vcGeoDetail:GeographyDetail_iPad = UIStoryboard(name: "GeographyPad", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetail_iPad") as! GeographyDetail_iPad
                    vcGeoDetail.geographyId = geographyId
                    self.navigationController?.pushViewController(vcGeoDetail, animated: true)
                }
            default:
                break
            }
        }
        
        return true
    }
}
