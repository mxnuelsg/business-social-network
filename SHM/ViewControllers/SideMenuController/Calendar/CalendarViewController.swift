//
//  CalendarViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 7/24/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import FSCalendar
import Spruce

class CalendarViewController: UIViewController, FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance, UITableViewDataSource, UITableViewDelegate, UITabBarDelegate, UITextViewDelegate
{
    //Dot Colors
    let dotNormal = UIImage.getImageWithColor(UIColor.yellowPost(), size: CGSize(width: 12, height: 12))
    let dotBirthday = UIImage.getImageWithColor(UIColor.greenBirthday(), size: CGSize(width: 12, height: 12))
    let dotRelevantDate = UIImage.getImageWithColor(UIColor.blueImportantDate(), size: CGSize(width: 12, height: 12))
    
    //OUTLETS & PROPERTY
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var tableEvents: UITableView!
    @IBOutlet weak var constraintTableHeight: NSLayoutConstraint!
    @IBOutlet weak var tabBarContextualMenu: UITabBar!
    @IBOutlet weak var constraintHeightTabBar: NSLayoutConstraint!
    @IBOutlet weak var btnMonthYear: UIButton!
    
    //Header: Label Color description
    @IBOutlet weak var constraintHeadTop: NSLayoutConstraint!
    @IBOutlet weak var constraintCalendarTop: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightHeaderColorLabel: NSLayoutConstraint!
    
    @IBOutlet weak var viewColorLabels: UIView!
    
    
    @IBOutlet weak var lblPostDesc: UILabel!
    
    @IBOutlet weak var lblBirthdayDesc: UILabel!
    @IBOutlet weak var lblImportantDateDesc: UILabel!
    @IBOutlet weak var navigationItemCalendar: UINavigationItem!
    @IBOutlet weak var tabBarItemListview: UITabBarItem!
    @IBOutlet weak var tabBarItemListView_iPad: UITabBarItem!
    @IBOutlet weak var navItemCalendar_iPad: UINavigationItem!
    
    var shouldShowColorLabelDescription = false

    var activeBlueForIpad = false
    var activityIndicator = UIActivityIndicatorView()
    
    var arrayCalendarItems = [CalendarItem](){
        didSet{
            //Extract types from array
            let postItems = self.arrayBackupToSearch.filter({$0.type.lowercased() == "post"})
            let birthdayItems = self.arrayBackupToSearch.filter({$0.type.lowercased() == "birthday"})
            let dateItems = self.arrayBackupToSearch.filter({$0.type.lowercased() == "relevantdate"})
            //Update array according with filters
            self.arrayCalendarItems.removeAll()
            self.arrayCalendarItems.append(contentsOf: self.postFilter == true ? postItems:[CalendarItem]())
            self.arrayCalendarItems.append(contentsOf: self.birthdayFilter == true ? birthdayItems:[CalendarItem]())
            self.arrayCalendarItems.append(contentsOf: self.importantDateFilter == true ? dateItems:[CalendarItem]())
        }
    }
    var arrayDateSelected = [CalendarItem]()

    var onHeightChanged: ((_ newHeight: CGFloat) -> (CGFloat))?
    var tableHeight: CGFloat = 100
    fileprivate(set) internal var hasFocus = false
    
    var arrayBackupToSearch = [CalendarItem]()

    var loadEventsAutomatically = true
    var isFullCalendarView = true
    var showsInGeography = false
    
    var loadByProjectId: Int {
        didSet{
            isFullCalendarView = false
        }
    }
    var loadByStakeholderId: Int
        {
        didSet{
            isFullCalendarView = false
        }
    }
    
    var dataDidLoad: (() -> ())?
    var scrollToBottom:(() -> ())?
    var selectedDate: Date! = Date()
    var onMonthChanged:((_ currentMont:Date)->())?
    
    //FILTER FLAGS
    var postFilter = true
    var birthdayFilter = true
    var importantDateFilter = true
    
    required init?(coder aDecoder: NSCoder) {
        self.loadByStakeholderId = 0
        self.loadByProjectId = 0
        super.init(coder: aDecoder)
    }
    
    // MARK: - LIFE CLYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        
        if let isLoggedIn = LibraryAPI.shared.currentUser?.isLoggedIn {
            
            if isLoggedIn != true
            {
                return
            }
        }
        else
        {
            return
        }
        
        //Setup calendar for geography detail
        guard self.showsInGeography == false else {
            
            //Filling data                        
            self.arrayBackupToSearch.removeAll()
            self.arrayBackupToSearch = self.arrayCalendarItems
            self.calendar.reloadData()
            
            //Auto-selection Today
            self.calendar(self.calendar, didSelect: Date(), at: .current)
            return
        }
        
        //Get all Calendar events
        if isFullCalendarView == true
        {
            if self.loadEventsAutomatically == true
            {
                loadFullCalendar()
            }
        }
        else
        {
            loadFullCalendarById()
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        backButtonArrow()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        print("Deinit: CalendarViewController")
    }
    
    //MARK: CONFIG UI TO DETAIL
    func configureForEmbed()
    {
        tabBarContextualMenu.isHidden = true
        constraintHeightTabBar.constant = 0
    }
    //MARK: FILTER BUTTONS CONFIG AND ACTIONS
    private func setFilterControls()
    {
        //POSTS
        
        self.lblPostDesc.isUserInteractionEnabled = true
        
        self.lblPostDesc.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
            
            MessageManager.shared.showLoadingHUD()
            self.filterByPosts()
            MessageManager.shared.hideHUD()
        }))
        
        //BIRTHDAY
        self.lblBirthdayDesc.isUserInteractionEnabled = true
        
        self.lblBirthdayDesc.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
                
            MessageManager.shared.showLoadingHUD()
            self.filterByBirthdays()
            MessageManager.shared.hideHUD()
            }))
        
        //IMPORTANT DATE
        self.lblImportantDateDesc.isUserInteractionEnabled = true
        
        self.lblImportantDateDesc.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
            
            MessageManager.shared.showLoadingHUD()
            self.filterByImportantDates()
            MessageManager.shared.hideHUD()
        }))
    }
    
    private func filterByPosts()
    {
        //Safety guard
        guard self.activityIndicator.isAnimating == false else {
            MessageManager.shared.showStatusBar(title: "Loading...".localized(), type: .warning, containsIcon: false)
            return
        }

        //Update Filter Flag
        self.lblPostDesc.alpha = self.lblPostDesc.alpha == 1 ? 0.5: 1.0
        self.postFilter = self.lblPostDesc.alpha == 1 ? true : false
        //Trigger filtering
        self.arrayCalendarItems = self.arrayBackupToSearch
        //Reload data in calendar
        self.calendar.reloadData()
        
        //Reload data in table
        self.calendar(self.calendar, didSelect: self.selectedDate, at: .current)
    }
    
    private func filterByBirthdays()
    {
        //Safety guard
        guard self.activityIndicator.isAnimating == false else {
            MessageManager.shared.showStatusBar(title: "Loading...".localized(), type: .warning, containsIcon: false)
            return
        }

        //Update Filter Flag
        self.lblBirthdayDesc.alpha = self.lblBirthdayDesc.alpha == 1 ? 0.5: 1.0
        self.birthdayFilter = self.lblBirthdayDesc.alpha == 1 ? true:false
        
        //Trigger filtering
        self.arrayCalendarItems = self.arrayBackupToSearch
        //Reload data
        self.calendar.reloadData()
        //Reload data in table
       self.calendar(self.calendar, didSelect: self.selectedDate, at: .current)
    }
    
    private func filterByImportantDates()
    {
        //Safety guard
        guard self.activityIndicator.isAnimating == false else {
            MessageManager.shared.showStatusBar(title: "Loading...".localized(), type: .warning, containsIcon: false)
            return
        }
        
        //Update Filter Flag
        self.lblImportantDateDesc.alpha = self.lblImportantDateDesc.alpha == 1 ? 0.5 : 1.0
        self.importantDateFilter = self.lblImportantDateDesc.alpha == 1 ? true:false
        //Trigger filtering
        self.arrayCalendarItems = self.arrayBackupToSearch
        //Reload data
        self.calendar.reloadData()
        //Reload data in table
        self.calendar(self.calendar, didSelect: self.selectedDate, at: .current)
    }
    
    //MARK: FILTER
    func filterByText(_ text: String)
    {
        let term = text.trim()
        guard term.isEmpty == false else {
            
            //Reload calendar 
            arrayCalendarItems = arrayBackupToSearch

            calendar.reloadData()
            arrayDateSelected.removeAll()
            tableEvents.reloadData()
            return
        }
        
        //Filter
        arrayCalendarItems = arrayBackupToSearch.filter({
            $0.titleFormated.containsString(text)
        })
        
        calendar.reloadData()
        arrayDateSelected.removeAll()
        tableEvents.reloadData()
    }
    
    // MARK: WEB SERVICE
    func loadFullCalendar()
    {
        //Indicator
        activityIndicator.startAnimating()
        
        let datefrom = calendar.currentPage.firstDayOfMonth().convertToRequestString()
        let dateTo = calendar.currentPage.lastDayOfMonth().convertToRequestString()
        let dateRequestArray =  datefrom.components(separatedBy: "-")
        let yearRequest = Int(dateRequestArray[0])!

        //Object to request
        var wsObject: [String : Any]  = [:]
        wsObject["FromDate"] = datefrom
        wsObject["ToDate"] = dateTo        
        
        LibraryAPI.shared.calendarBO.getAllEvents(wsObject, onSuccess: {
            (items) -> () in
            
            //Modifying birthdates year
            for object in items where object.type.lowercased() == "birthday"
            {
                let calendar = Calendar.current
//                let componentsToday = calendar.components([.Year, .Month, .Day], fromDate: NSDate())
                var componentsEvent = calendar.dateComponents([.year, .month, .day], from: object.startDate)
                componentsEvent.year = yearRequest
                
                object.startDate = calendar.date(from: componentsEvent)!
            }
            
            //Filling data
            self.arrayBackupToSearch.removeAll()
            self.arrayBackupToSearch = items
            
            self.arrayCalendarItems.removeAll()
            self.arrayCalendarItems = items
            
            
            self.calendar.reloadData()
            
            self.activityIndicator.stopAnimating()
            
            //Auto-selection Today
            self.calendar(self.calendar, didSelect: Date(), at: .current)
            self.dataDidLoad?()
            
            }) { error in
                
                self.activityIndicator.stopAnimating()
                self.tableEvents.displayBackgroundMessage("Error".localized(),
                    subMessage: "Try again".localized())
        }
    }
    
    func loadFullCalendarById()
    {
        //Object to request
        var wsObject: [String : Any]  = [:]
        wsObject["StakeholderId"] = loadByStakeholderId
        wsObject["ProjectId"] = loadByProjectId
        wsObject["Month"] = calendar.currentPage.month
        wsObject["Year"] = calendar.currentPage.year
        
        LibraryAPI.shared.calendarBO.getEvents(wsObject , onSuccess: {
            (items) -> () in
            
            //Modifying birthdates year
            for object in items where object.type.lowercased() == "birthday"
            {
                let calendar = Calendar.current
                var componentsEvent = (calendar as NSCalendar).components([.year, .month, .day], from: object.startDate)
                componentsEvent.year = self.calendar.currentPage.year
                object.startDate = calendar.date(from: componentsEvent)!
            }
            
            //Filling data
            self.arrayBackupToSearch.removeAll()
            self.arrayBackupToSearch = items
            
            self.arrayCalendarItems.removeAll()
            self.arrayCalendarItems = items

            self.calendar.reloadData()
            
            
            //Auto Selection only when Lateral control in iPad was implemented
            if self.loadEventsAutomatically == false
            {
                //Auto-selection Today
                self.calendar(self.calendar, didSelect: Date(), at: .current)
                self.dataDidLoad?()
            }
            
            }) { error in
                guard error.code != -999 else {return}
                self.tableEvents.displayBackgroundMessage("Error".localized(),
                    subMessage: "Try again".localized())
                
                MessageManager.shared.showBar(title: "Error",
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
        }
    }
    
    //MARK: FOCUS
    func focus()
    {
        hasFocus = true
        
        guard onHeightChanged != nil else {
            
            return
        }
        
        setEventsTableHeight()
    }
    
    func unfocus()
    {
        hasFocus = false
    }
    
    // MARK: INITIAL CONFIG
    func loadConfig()
    {
        //UI for iPad
        self.view.backgroundColor = (self.activeBlueForIpad == true) ? .blueSky() : .white
        self.btnMonthYear.backgroundColor = (self.activeBlueForIpad == true) ? .blueSky() : .white
        self.setBorder()
       
        if DeviceType.IS_ANY_IPAD == false
        {
            self.setColorDescriptionHeader()
            self.navigationItemCalendar.title = "CALENDAR".localized()
            self.tabBarItemListview.title = "List View".localized()
            //Configure Event Filters
            self.setFilterControls()
        }
        else
        {
            if self.showsInGeography == false
            {
                self.navItemCalendar_iPad.title = "CALENDAR".localized()
                self.tabBarItemListView_iPad.title = "List View".localized()
            }
            else
            {
                self.viewColorLabels.isHidden = true
                self.constraintHeightHeaderColorLabel.constant = 0
                self.constraintHeadTop.constant = 0
            }
        }
        //Full Calendar
        if isFullCalendarView == true
        {
            //Bar Button Items
            activityIndicator.activityIndicatorViewStyle = .white
            let batBtnActivity = UIBarButtonItem(customView: activityIndicator)
            
            let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
            navigationItem.rightBarButtonItems = [barBtnCompose, batBtnActivity]
        }
        
        //Calendar
        self.calendar.today = Date()
        self.calendar.scrollDirection = .horizontal
        self.calendar.appearance.headerMinimumDissolvedAlpha = 0.0
        self.calendar.scope = .month
        //self.calendar.firstWeekday = 2
        self.calendar.allowsMultipleSelection = false
        self.btnMonthYear.setTitle(calendar.currentPage.getMonthYear(), for: .normal)

        
        //Register Cell
        tableEvents.register(UINib(nibName: "CalendarPostTableCell", bundle: nil), forCellReuseIdentifier: "CalendarPostTableCell")
        tableEvents.backgroundColor = (activeBlueForIpad == true) ? UIColor.blueSky() : UIColor.white
        
        //Tab bar for geography
        self.tabBarContextualMenu.isHidden = self.showsInGeography == true ? true : false
    }
    
    func setColorDescriptionHeader()
    {
        if self.shouldShowColorLabelDescription == false
        {
            self.constraintHeadTop.constant = 0
            self.constraintCalendarTop.constant = 0
            self.constraintHeightHeaderColorLabel.constant = 0
            
            
            self.lblPostDesc.isHidden = true
            
            self.lblBirthdayDesc.isHidden = true
            
            self.lblImportantDateDesc.isHidden = true
        }
        
        
        self.lblPostDesc.text = "Posts".localized()
        self.lblPostDesc.textColor = .white
        self.lblPostDesc.backgroundColor = UIColor.yellowPost()
        
        self.lblPostDesc.cornerRadius()
        
        self.lblBirthdayDesc.text = "Birthday".localized()
        self.lblBirthdayDesc.textColor =  .white
        self.lblBirthdayDesc.backgroundColor = UIColor.greenBirthday()
        self.lblBirthdayDesc.cornerRadius()
        
        self.lblImportantDateDesc.text = "Important Date".localized()
        self.lblImportantDateDesc.textColor = .white
        self.lblImportantDateDesc.backgroundColor = UIColor.blueImportantDate()
        self.lblImportantDateDesc.adjustsFontSizeToFitWidth = true
        self.lblImportantDateDesc.cornerRadius()
    }

    // MARK; - TO GO FULL HEIGHT
    func reloadEventsTable()
    {
        self.tableEvents.reloadData()
        self.tableEvents.isScrollEnabled = false
        self.tableEvents.bounces = false
        
        guard tableEvents.numberOfRows(inSection: 0) > 0 else {
            self.tableHeight = 100
            return
        }
        
        let lastIndexPath = IndexPath(row: self.tableEvents.numberOfRows(inSection: 0) - 1, section: 0)
        
        self.tableEvents.scrollToRow(at: lastIndexPath, at: .bottom, animated: false)
        
        DispatchQueue.main.async(execute: {
            let tableHeight = self.tableEvents.contentSize.height
            
            self.tableHeight = tableHeight
            
            if self.hasFocus == true {
                self.setEventsTableHeight()
            }
        })
        
        //Spruce framework Animation
        DispatchQueue.main.async {
            
            self.tableEvents.spruce.animate([.contract(.moderately)], animationType: SpringAnimation(duration: 0.7))
        }
    }
    
    func setEventsTableHeight()
    {
        // TOTAL HEIGHT = TABLE HEIGHT + CALENDAR HEIGHT + HEADER HEIGHT
        var totalHeight = tableHeight + 288//308
        let recommendedHeight = onHeightChanged?(totalHeight) ?? totalHeight
        
        if recommendedHeight > totalHeight
        {
            totalHeight = recommendedHeight
        }
        
        self.view.frame.size.height = totalHeight
        constraintTableHeight.constant = tableHeight
        self.view.layoutIfNeeded()
    }
    
    // MARK: ACTIONS
    func createNewPost()
    {
        let vcNewPost = NewPostViewController()
        vcNewPost.dateTagged = self.selectedDate
        
        vcNewPost.onViewControllerClosed = { [weak self] Void in
        
            self?.loadFullCalendar()
        }
        
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = (DeviceType.IS_ANY_IPAD) ?  .formSheet :  .overFullScreen
        
        present(navController, animated: true, completion: nil)
    }
    
    private func dotsFor(_ date: Date) -> (dots: Int, colors: [UIColor]?)
    {
        //Dots
        var isBirthday = false
        var isRelevantDate = false
        var isPost = false
        
        //Verify Date has event
        for item in self.arrayCalendarItems where date.getStringStyleMedium() == item.startDate.getStringStyleMedium()
        {
            let arrayEvents = self.arrayCalendarItems.filter({
                $0.startDate.getStringStyleMedium() == date.getStringStyleMedium()
            })
            
            //Get type of events contained
            for event in arrayEvents
            {
                if event.type.lowercased() == "birthday" { isBirthday = true }
                if event.type.lowercased() == "post" { isPost = true }
                if event.type.lowercased() == "relevantdate" { isRelevantDate = true }
            }
        }
        
        //Draw dots
        if isBirthday == true && isPost == false && isRelevantDate == false
        {
            //Only Birthday
            return (1, [UIColor.greenBirthday()])
        }
        else if isBirthday == false && isPost == true && isRelevantDate == false
        {
            //Only Post
            return (1, [UIColor.yellowPost()])
        }
        else if isBirthday == false && isPost == false && isRelevantDate == true
        {
            //Only relevant date
            return (1,[UIColor.blueImportantDate()])
        }
        else if isBirthday == true && isPost == true && isRelevantDate == false
        {
            //birthday and post
            return (2, [UIColor.greenBirthday(), UIColor.yellowPost()])
        }
        else if isBirthday == true && isPost == false && isRelevantDate == true
        {
            //birthday and relevant date
            return (2, [UIColor.greenBirthday(), UIColor.blueImportantDate()])
        }
        else if isBirthday == false && isPost == true && isRelevantDate == true
        {
            //Post and relevant date
            return (2,[UIColor.yellowPost(), UIColor.blueImportantDate()])
        }
        else if isBirthday == true && isPost == true && isRelevantDate == true
        {
            //All Birthday, Post and relevant date
            return (3, [UIColor.greenBirthday(), UIColor.yellowPost(), UIColor.blueImportantDate()])
        }
        else
        {
            return (0,  nil)
        }
    }
    
    // MARK: NAVIGATION CALENDAR
    @IBAction func previousMonth()
    {
        self.calendar.currentPage = calendar.currentPage.dateByAddingMonths(-1)
        self.onMonthChanged?(self.calendar.currentPage)
    }
    
    @IBAction func nextMonth()
    {
        self.calendar.currentPage = calendar.currentPage.dateByAddingMonths(1)
        self.onMonthChanged?(self.calendar.currentPage)
    }
    
    @IBAction func currentMonthTapped()
    {
        let vcCalendar: CalendarPickerViewController! = CalendarPickerViewController()
        vcCalendar.isPickerForNewPost = false
        vcCalendar.onDate = { [weak self] dateSelected in
            
            self?.calendar.currentPage = dateSelected
        }
        
        let vcModal = ModalHalfViewController(subViewController: vcCalendar)
        present(vcModal, animated:true, completion: nil)
    }
    
    // MARK: FSCalendar DATASOURCE
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int
    {
        return self.dotsFor(date).dots
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]?
    {
        return self.dotsFor(date).colors
    }
    
    // MARK: FSCalendar DELEGATE
    func calendarCurrentPageDidChange(_ calendar: FSCalendar)
    {
        print("change page to \(calendar.currentPage.getStringStyleMedium()))")
        self.btnMonthYear.setTitle(calendar.currentPage.getMonthYear(), for: .normal)
        
        var oldPage = String()
        
        if oldPage == calendar.currentPage.getStringStyleMedium() {
            print("not really a page change")
            return
        }
        
        oldPage = calendar.currentPage.getStringStyleMedium()
        
        //Reload calendar (Current Month)
        guard self.showsInGeography == false else {return}
        if isFullCalendarView == true
        {
            loadFullCalendar()
        }
        else
        {
            loadFullCalendarById()
        }
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition)
    {
        //Searching items for date selected
        let arrayEvents = arrayCalendarItems.filter({
            $0.startDate.getStringStyleMedium() == date.getStringStyleMedium()
        })
        
        self.selectedDate = date
        
        arrayDateSelected = arrayEvents
        
        //Verify if it's necessary change the height in calendar details
        if isFullCalendarView == false
        {
            self.reloadEventsTable()
        }
        else
        {
            self.tableEvents.reloadData()
            
            //Spruce framework Animation
            DispatchQueue.main.async {
                
                self.tableEvents.spruce.animate([.contract(.moderately)], animationType: SpringAnimation(duration: 0.7))
            }
        }
        
        //Go to bottom if records exist
        if arrayDateSelected.count > 0
        {
            self.scrollToBottom?()
        }
        
        //Empty State
        if arrayDateSelected.count == 0
        {
            tableEvents.displayBackgroundMessage("No events".localized(),
                                                 subMessage: "")
        }
        else
        {
            tableEvents.dismissBackgroundMessage()
        }
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool)
    {
        calendar.frame = CGRect(origin: calendar.frame.origin, size: bounds.size)
        self.view.layoutIfNeeded()
    }
    
    // MARK: TABBAR DELEGATE (FeedTableCell)
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        //Selected item color equal not selected color
        tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        
        switch item.tag
        {
        case 1:
            LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.GetCalendarAllEvents)
            let vcSource = self
            let vcDestination = Storyboard.getInstanceOf(CalendarListViewController.self)
            let navigationController = vcSource.navigationController!
            
            UIView.beginAnimations("LeftFlip", context: nil)
            UIView.setAnimationDuration(1)
            UIView.setAnimationCurve(.easeInOut)
            UIView.setAnimationTransition(.flipFromLeft, for: vcSource.view.superview!, cache: true)
            UIView.commitAnimations()
            
            var controllerStack = navigationController.viewControllers
            controllerStack.removeLast()
            controllerStack.append(vcDestination)
            navigationController.setViewControllers(controllerStack, animated: true)
        default:
            break
        }
    }
    
      // MARK: TABLE VIEW DATASOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayDateSelected.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let item = arrayDateSelected[indexPath.row]
        
        if item.type.lowercased() == "post"
        {
            //create custom post
            let customPost = Post()
            customPost.id = item.id
            customPost.title = item.title
            customPost.stakeholders = item.stakeholders
            customPost.geographies = item.geographies
            customPost.attachments = item.attachments
            customPost.projects = item.projects
            customPost.events = item.events
            customPost.createdDateString = item.createdDate
            customPost.createdDate = item.createdDate.toDate()!
            customPost.author = item.createdBy
            customPost.isPrivate  = item.isPrivate
            
            //Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarPostTableCell") as! FeedTableCell
            cell.tvText.delegate = self
            
            //Color for selected cell
            if DeviceType.IS_ANY_IPAD == false
            {
                let selectedColor = UIView()
                selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
                cell.selectedBackgroundView = selectedColor
            }
        
            cell.iconEvent.image = dotNormal
            cell.iconEvent.layer.cornerRadius = 6
            cell.iconEvent.clipsToBounds = true

            
            cell.tvText.text = item.title
            
            cell.cellForPostEvent(cell, post: customPost)
            cell.onStakeholderTap = { [weak self] stakeholder in
                
                //Enabled or Disabled
                guard stakeholder.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: stakeholder.fullName, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                    
                    return
                }
                
                //Workflow iPad / iPhone
                if DeviceType.IS_ANY_IPAD == true
                {
                    let vcStakeholderDetail = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
                    vcStakeholderDetail.stakeholder = stakeholder
                    self?.navigationController?.pushViewController(vcStakeholderDetail, animated: true)
                }
                else
                {
                    let vcStakeholderDetail : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                    vcStakeholderDetail.stakeholder = stakeholder
                    self?.navigationController?.pushViewController(vcStakeholderDetail, animated: true)
                }
            }
            
            cell.onAllStakeholdersTap = { [weak self] Void in
                
                let stakeholdersTableVC = StakeholdersTableViewController.getNewInstance()
                stakeholdersTableVC.stakeholders = customPost.stakeholders
                
                let vcModal = ModalViewController(subViewController: stakeholdersTableVC)
                self?.present(vcModal, animated:true, completion: nil)
            }
            
            cell.onTextViewTap = { [weak self] Void in
                
                if DeviceType.IS_ANY_IPAD == true
                {
                    let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController_iPad.self)
                    vcDetailPost.post = customPost
                    self?.navigationController?.pushViewController(vcDetailPost, animated: true)
                }
                else
                {
                    let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController.self)
                    vcDetailPost.post = customPost
                    self?.navigationController?.pushViewController(vcDetailPost, animated: true)
                }
            }
            
            return cell
        }
        else if item.type.lowercased() == "birthday"
        {
            let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier:"Cell")
            
            //Color for selected cell
            if (DeviceType.IS_ANY_IPAD == false)
            {
                let selectedColor = UIView()
                selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
                cell.selectedBackgroundView = selectedColor
            }
            
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            cell.textLabel?.textColor = UIColor.blackAsfalto()
            cell.textLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
            cell.textLabel?.numberOfLines = 2
            
            let finalText = RelevantEventType.Birthday.rawValue.localized() + " " +  item.title
            cell.textLabel?.text = finalText
            
            /** Style Text */
            let attributedString = NSMutableAttributedString(string: cell.textLabel!.text!)
            attributedString.setBoldAndColor(RelevantEventType.Birthday.rawValue.localized(), color: UIColor.greenBirthday(), size: 10)
            cell.textLabel?.attributedText = attributedString
            
            //type
            cell.imageView?.image = dotBirthday
            cell.imageView?.layer.cornerRadius = 6
            cell.imageView?.clipsToBounds = true
            
            return cell
        }
        else
        {
            //Relevant Dates
            let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier:"Cell")
            
            //Color for selected cell
            if (DeviceType.IS_ANY_IPAD == false)
            {
                let selectedColor = UIView()
                selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
                cell.selectedBackgroundView = selectedColor
            }
            
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            cell.textLabel?.textColor = UIColor.blackAsfalto()
            cell.textLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
            cell.textLabel?.numberOfLines = 5
            
            cell.textLabel?.text = item.title
            
            //type
            cell.imageView?.image = dotRelevantDate
            cell.imageView?.layer.cornerRadius = 6
            cell.imageView?.clipsToBounds = true
            
            return cell
        }
    }

    // MARK: TABLE VIEW DELEGATE
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = arrayDateSelected[indexPath.row]
        
        if item.type.lowercased() == "post"
        {
            let customPost = Post()
            customPost.id = item.id
            customPost.title = item.title
            customPost.stakeholders = item.stakeholders
            customPost.attachments = item.attachments
            customPost.projects = item.projects
            customPost.events = item.events
            customPost.createdDateString = item.createdDate
            customPost.createdDate = item.createdDate.toDate()!
            customPost.author = item.createdBy
            
            if DeviceType.IS_ANY_IPAD == true
            {
                let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController_iPad.self)
                vcDetailPost.post = customPost
                navigationController?.pushViewController(vcDetailPost, animated: true)
                
            }
            else
            {
                let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController.self)
                vcDetailPost.post = customPost
                navigationController?.pushViewController(vcDetailPost, animated: true)
            }
        }
        else
        {
            let event = RelevantDate()
            event.id = item.id
            event.isFreeDateString = false //demo duda
            event.date = item.startDate
            event.stringValue = item.startDate.convertToRequestString()
            event.title = item.title
            event.type = (item.type.lowercased() == "birthday") ? 1 : 2
            
            if event.type == 1
            {
                //Birthday
                let alert = UIAlertController(title: "Add Event".localized(), message: "Would you like to save this date in your device calendar".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Save".localized(), style: UIAlertActionStyle.default, handler: {
                    (action) in
                    
                    let finalText = RelevantEventType.Birthday.rawValue.localized() + ": " +  event.title
                    CalendarManager.sharedInstance.createEventWithString(finalText, date:event.date!)
                }))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                //Relevant date
                let alert = UIAlertController(title:"Description".localized(), message:item.title, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Add Event".localized(), style: UIAlertActionStyle.default, handler: {
                    (action) in
                    
                    let finalText = "Relevant Date: ".localized() +  event.title
                    CalendarManager.sharedInstance.createEventWithString(finalText, date:event.date!)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        cell.backgroundColor = (activeBlueForIpad == true)  ? UIColor.blueSky() : UIColor.white
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        guard indexPath.row < arrayDateSelected.count else {return 0.0}
        if arrayDateSelected[indexPath.row].type.lowercased() == "birthday"
        {
            return 44
        }
        else if arrayDateSelected[indexPath.row].type.lowercased() == "post"
        {
            return 115
        }
        else
        {
            return 70
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return CGFloat.leastNormalMagnitude
    }
    
    // MARK: TEXTVIEW DELEGATE (FeedTableCell)
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
    {
        let arrayUrl = URL.description.components(separatedBy: "_")
        
        if arrayUrl.count > 1
        {
            let type = arrayUrl[0]
            
            switch type
            {
            case "s":
                //Go to Stakeholder detail
                let stakeholderLinked: Stakeholder! = Stakeholder(isDefault: true)
                stakeholderLinked.id = Int(arrayUrl[1])!
                stakeholderLinked.isEnable = Int(arrayUrl[2])! == 1 ? true : false
                
                let firstname = arrayUrl[3]
                
                //Enabled or Disabled
                guard stakeholderLinked.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: firstname, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                    
                    return true
                }
                
                //Workflow iPad / iPhone
                if DeviceType.IS_ANY_IPAD == true
                {
                    let vcStakeholderDetail = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
                    vcStakeholderDetail.stakeholder = stakeholderLinked
                    navigationController?.pushViewController(vcStakeholderDetail, animated: true)
                }
                else
                {
                    let vcStakeholderDetail : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                    vcStakeholderDetail.stakeholder = stakeholderLinked
                    navigationController?.pushViewController(vcStakeholderDetail, animated: true)
                }
            case "p":
                //Go to Project detail
                let projectLinked: Project! = Project(isDefault: true)
                projectLinked.id = Int(arrayUrl[1])!
                projectLinked.isEnable = Int(arrayUrl[2])! == 1 ? true : false
                
                //Enabled or Disabled
                guard projectLinked.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: nil, subtitle: "This project is disabled".localized(), type: .info, fromBottom: false)
                    
                    return true
                }
                
                //Go to detail
                if DeviceType.IS_ANY_IPAD == true
                {
                    let vcDetailProject = Storyboard.getInstanceOf(ProjectDetailViewController_iPad.self)
                    vcDetailProject.project = projectLinked
                    navigationController?.pushViewController(vcDetailProject, animated: true)
                }
                else
                {
                    let vcDetailProject : DetailProjectViewController = Storyboard.getInstanceFromStoryboard("Projects")
                    vcDetailProject.project = projectLinked
                    navigationController?.pushViewController(vcDetailProject, animated: true)
                }
                
                
            case "d":
                //Add Event to Calendar
                let alert = UIAlertController(title: "Add Event".localized(), message: "Would you like to save this date in your device calendar".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Save".localized(), style: UIAlertActionStyle.default, handler: {
                    (action) in

                    for item in self.arrayDateSelected where item.type.lowercased() == "post"
                    {
                        for theEvent in item.events where theEvent.id == Int(arrayUrl[1])!
                        {
                            //Create post
                            let customPost = Post()
                            customPost.id = item.id
                            customPost.title = item.title
                            customPost.stakeholders = item.stakeholders
                            customPost.attachments = item.attachments
                            customPost.projects = item.projects
                            customPost.events = item.events
                            customPost.createdDateString = item.createdDate
                            customPost.createdDate = item.createdDate.toDate()!
                            customPost.author = item.createdBy

                            //Save Event to iPhone Calendar
                            CalendarManager.sharedInstance.createEvent(customPost.title.getEditedTitleForPost(customPost, truncate: false), event: theEvent)
                        }
                    }
                    
                }))
                self.present(alert, animated: true, completion: nil)
            case "g":
                let geographyId = Int(arrayUrl[1])!
                if DeviceType.IS_ANY_IPAD == false
                {
                    let vcGeoDetail:GeographyDetailViewController = UIStoryboard(name: "Geography", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetailViewController") as! GeographyDetailViewController
                    vcGeoDetail.geographyId = geographyId
                    self.navigationController?.pushViewController(vcGeoDetail, animated: true)
                }
                else
                {
                    let vcGeoDetail:GeographyDetail_iPad = UIStoryboard(name: "GeographyPad", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetail_iPad") as! GeographyDetail_iPad
                    vcGeoDetail.geographyId = geographyId
                    self.navigationController?.pushViewController(vcGeoDetail, animated: true)
                }
            default:
                break
            }
        }
        
        return true
    }
}
