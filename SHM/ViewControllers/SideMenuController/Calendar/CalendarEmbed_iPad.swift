//
//  CalendarEmbed_iPad.swift
//  SHM
//
//  Created by Manuel Salinas on 1/4/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import FSCalendar

class CalendarEmbed_iPad: CalendarViewController_iPad
{
    //MARK: OUTLETS & VARUABLES
    @IBOutlet weak var theCalendar: FSCalendar!
    
    //MARK LIFE CYCLE
    override func viewDidLoad()
    {
        //super.viewDidLoad()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
