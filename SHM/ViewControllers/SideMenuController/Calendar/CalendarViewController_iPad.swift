//
//  CalendarViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 12/30/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import FSCalendar
import Spruce

class CalendarViewController_iPad: UIViewController, FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance
{
    // MARK: PROPERTIES AND OUTLETS
    @IBOutlet weak var lblCurrentDateSelected: UILabel!
    @IBOutlet weak var tableEvents: UITableView!
    @IBOutlet weak var btnCentralCalendar1: UIButton!
    @IBOutlet weak var btnCentralCalendar2: UIButton!
    
    //Color Description Labels
    @IBOutlet weak var lblPostDesc: UILabel!
    @IBOutlet weak var lblBirthdayDesc: UILabel!    
    @IBOutlet weak var lblImportantDateDesc: UILabel!
    
    // Dot Colors
    let dotNormal = UIImage.getImageWithColor(UIColor.yellowPost(), size: CGSize(width: 12, height: 12))
    let dotBirthday = UIImage.getImageWithColor(UIColor.greenBirthday(), size: CGSize(width: 12, height: 12))
    let dotRelevantDate = UIImage.getImageWithColor(UIColor.blueImportantDate(), size: CGSize(width: 12, height: 12))
    
    //Calendar 1
    var vcCalendarEmbed_1: CalendarEmbed_iPad!
    var arrayCalendarItems1 = [CalendarItem](){
        didSet{
            //Extract types from array
            let postItems = self.arrayBackupToSearch1.filter({$0.type.lowercased() == "post"})
            let birthdayItems = self.arrayBackupToSearch1.filter({$0.type.lowercased() == "birthday"})
            let dateItems = self.arrayBackupToSearch1.filter({$0.type.lowercased() == "relevantdate"})
            //Update array according with filters
            self.arrayCalendarItems1.removeAll()
            self.arrayCalendarItems1.append(contentsOf: self.postFilter == true ? postItems:[CalendarItem]())
            self.arrayCalendarItems1.append(contentsOf: self.birthdayFilter == true ? birthdayItems:[CalendarItem]())
            self.arrayCalendarItems1.append(contentsOf: self.importantDateFilter == true ? dateItems:[CalendarItem]())
        }
    }
    var arrayDateSelected1 = [CalendarItem]()
    
    //Calendar 2
    var vcCalendarEmbed_2: CalendarEmbed_iPad!
    var arrayCalendarItems2 = [CalendarItem](){
        didSet{
            //Extract types from array
            let postItems = self.arrayBackupToSearch2.filter({$0.type.lowercased() == "post"})
            let birthdayItems = self.arrayBackupToSearch2.filter({$0.type.lowercased() == "birthday"})
            let dateItems = self.arrayBackupToSearch2.filter({$0.type.lowercased() == "relevantdate"})
            //Update array according with filters
            self.arrayCalendarItems2.removeAll()
            self.arrayCalendarItems2.append(contentsOf: self.postFilter == true ? postItems:[CalendarItem]())
            self.arrayCalendarItems2.append(contentsOf: self.birthdayFilter == true ? birthdayItems:[CalendarItem]())
            self.arrayCalendarItems2.append(contentsOf: self.importantDateFilter == true ? dateItems:[CalendarItem]())
        }
    }

    var arrayDateSelected2 = [CalendarItem]()
    
    var arrayDateSelected = [CalendarItem]()
    var currentSelectedCalendar: Int = 1
    var wasChangedByButton = false
    
    //Arrays
    var activityIndicator = UIActivityIndicatorView()
    var tableHeight: CGFloat = 1000
    fileprivate(set) internal var hasFocus = false
    
    var arrayBackupToSearch1 = [CalendarItem]()
    var arrayBackupToSearch2 = [CalendarItem]()
    
    var dataDidLoad: (() -> ())?
    var scrollToBottom:(() -> ())?
    var selectedDate: Date! = Date()
    
    //FILTER FLAGS
    var postFilter = true
    var birthdayFilter = true
    var importantDateFilter = true
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    // MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.loadConfig()
        self.loadFullCalendar()
        self.setColorDescriptionHeader()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: LOAD CONFIG
    func loadConfig()
    {
        //Title
        self.title = "CALENDAR".localized()
        
        self.view.backgroundColor = UIColor.blueSky()
        self.tableEvents.setBorder()
        self.lblCurrentDateSelected.setBorder()
        self.lblCurrentDateSelected.adjustsFontSizeToFitWidth = true
        
        //Bar Button Items
        self.activityIndicator.activityIndicatorViewStyle = .white
        let batBtnActivity = UIBarButtonItem(customView: activityIndicator)
        
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        navigationItem.rightBarButtonItems = [barBtnCompose, batBtnActivity]
        
        //Calendar1
        self.vcCalendarEmbed_1.theCalendar.tag = 1
        self.vcCalendarEmbed_1.theCalendar.today = Date()
        self.vcCalendarEmbed_1.theCalendar.scrollDirection = .horizontal
        self.vcCalendarEmbed_1.theCalendar.appearance.headerMinimumDissolvedAlpha = 0.0
        self.vcCalendarEmbed_1.theCalendar.scope = .month
        self.vcCalendarEmbed_1.theCalendar.firstWeekday = 2
        self.vcCalendarEmbed_1.theCalendar.allowsMultipleSelection = false
        self.vcCalendarEmbed_1.theCalendar.dataSource = self
        self.vcCalendarEmbed_1.theCalendar.delegate = self
        
        //Calendar2
        self.vcCalendarEmbed_2.theCalendar.tag = 2
        self.vcCalendarEmbed_2.theCalendar.today = Date()
        self.vcCalendarEmbed_2.theCalendar.currentPage = self.vcCalendarEmbed_2.theCalendar.currentPage.dateByAddingMonths(1)
        self.vcCalendarEmbed_2.theCalendar.scrollDirection = .horizontal
        self.vcCalendarEmbed_2.theCalendar.appearance.headerMinimumDissolvedAlpha = 0.0
        self.vcCalendarEmbed_2.theCalendar.scope = .month
        self.vcCalendarEmbed_2.theCalendar.firstWeekday = 2
        self.vcCalendarEmbed_2.theCalendar.allowsMultipleSelection = false
        self.vcCalendarEmbed_2.theCalendar.dataSource = self
        self.vcCalendarEmbed_2.theCalendar.delegate = self
        
        //Register Cell
        self.tableEvents.register(UINib(nibName: "CalendarPostTableCell", bundle: nil), forCellReuseIdentifier: "CalendarPostTableCell")
        self.tableEvents.backgroundColor = UIColor.blueSky()
        
        //SET FILTER ACTIONS
        self.setFilterControls()
    }
    
    // MARK; - TO GO FULL HEIGHT
    func reloadEventsTable()
    {
        self.tableEvents.reloadData()
        
        //Spruce framework Animation
        DispatchQueue.main.async {
            
            self.tableEvents.spruce.animate([.contract(.moderately)], animationType: SpringAnimation(duration: 0.7))
        }
    }
    
    func setColorDescriptionHeader()
    {
        self.lblPostDesc.text = "Posts".localized()
        self.lblPostDesc.adjustsFontSizeToFitWidth = true
        self.lblPostDesc.backgroundColor = UIColor.yellowPost()
        self.lblPostDesc.textColor = .white
        self.lblPostDesc.cornerRadius()
        
        self.lblBirthdayDesc.text = "Birthday".localized()
        self.lblBirthdayDesc.adjustsFontSizeToFitWidth = true
        self.lblBirthdayDesc.backgroundColor = UIColor.greenBirthday()
        self.lblBirthdayDesc.textColor = .white
        self.lblBirthdayDesc.cornerRadius()
        
        self.lblImportantDateDesc.text = "Important Date".localized()
        self.lblImportantDateDesc.adjustsFontSizeToFitWidth = true
        self.lblImportantDateDesc.backgroundColor = UIColor.blueImportantDate()
        self.lblImportantDateDesc.textColor = .white
        self.lblImportantDateDesc.cornerRadius()
    }
    // MARK: FILTERS ACTIONS AND CONFIGURATIONS
    private func setFilterControls()
    {
        //POSTS
        self.lblPostDesc.isUserInteractionEnabled = true
        self.lblPostDesc.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
            MessageManager.shared.showLoadingHUD()
            self.filterByPosts()
            MessageManager.shared.hideHUD()
        }))
        
        //BIRTHDAY
        self.lblBirthdayDesc.isUserInteractionEnabled = true
        self.lblBirthdayDesc.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
            
            MessageManager.shared.showLoadingHUD()
            self.filterByBirthdays()
            MessageManager.shared.hideHUD()
        }))
        
        //IMPORTANT DATE
        self.lblImportantDateDesc.isUserInteractionEnabled = true
        self.lblImportantDateDesc.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
            
            MessageManager.shared.showLoadingHUD()
            self.filterByImportantDates()
            MessageManager.shared.hideHUD()
        }))
    }
    
    private func filterByPosts()
    {
        //Safety guard
        guard self.activityIndicator.isAnimating == false else {
            MessageManager.shared.showStatusBar(title: "Loading...".localized(), type: .warning, containsIcon: false)
            return
        }

        //Update Filter Flag
        self.lblPostDesc.alpha = self.lblPostDesc.alpha == 0.5 ? 1.0 : 0.5
        self.postFilter = self.lblPostDesc.alpha == 1 ? true : false
        //Trigger filtering
        self.arrayCalendarItems1 = self.arrayBackupToSearch1
        self.arrayCalendarItems2 = self.arrayBackupToSearch2
        
        //Reload data in table
        self.vcCalendarEmbed_1.theCalendar.reloadData()
        self.vcCalendarEmbed_2.theCalendar.reloadData()
        let calendar = self.currentSelectedCalendar == 1 ? self.vcCalendarEmbed_1.theCalendar : self.vcCalendarEmbed_2.theCalendar
        self.calendar(calendar!, didSelect: self.selectedDate, at: .current)
    }
    
    private func filterByBirthdays()
    {
        //Safety guard
        guard self.activityIndicator.isAnimating == false else {
            MessageManager.shared.showStatusBar(title: "Loading...".localized(), type: .warning, containsIcon: false)
            return
        }

        //Update Filter Flag
        self.lblBirthdayDesc.alpha = self.lblBirthdayDesc.alpha == 0.5 ? 1.0: 0.5
        self.birthdayFilter = self.lblBirthdayDesc.alpha == 1 ? true:false
        
        //Trigger filtering
        self.arrayCalendarItems1 = self.arrayBackupToSearch1
        self.arrayCalendarItems2 = self.arrayBackupToSearch2
        
        //Reload data in table
        self.vcCalendarEmbed_1.theCalendar.reloadData()
        self.vcCalendarEmbed_2.theCalendar.reloadData()
        let calendar = self.currentSelectedCalendar == 1 ? self.vcCalendarEmbed_1.theCalendar : self.vcCalendarEmbed_2.theCalendar
        self.calendar(calendar!, didSelect: self.selectedDate, at: .current)
    }
    
    private func filterByImportantDates()
    {
        //Safety guard
        guard self.activityIndicator.isAnimating == false else {
            MessageManager.shared.showStatusBar(title: "Loading...".localized(), type: .warning, containsIcon: false)
            return
        }

        //Update Filter Flag
        self.lblImportantDateDesc.alpha = self.lblImportantDateDesc.alpha == 0.5 ? 1.0 : 0.5
        self.importantDateFilter = self.lblImportantDateDesc.alpha == 1 ? true:false
        //Trigger filtering
        self.arrayCalendarItems1 = self.arrayBackupToSearch1
        self.arrayCalendarItems2 = self.arrayBackupToSearch2
        //Reload data in table
        self.vcCalendarEmbed_1.theCalendar.reloadData()
        self.vcCalendarEmbed_2.theCalendar.reloadData()
        let calendar = self.currentSelectedCalendar == 1 ? self.vcCalendarEmbed_1.theCalendar : self.vcCalendarEmbed_2.theCalendar
        self.calendar(calendar!, didSelect: self.selectedDate, at: .current)
    }
    // MARK: WEB SERVICE
    func loadFullCalendar()
    {
        //Indicator
        self.activityIndicator.startAnimating()
        
        let datefrom = self.vcCalendarEmbed_1.theCalendar.currentPage.firstDayOfMonth().convertToRequestString()
        let dateTo = self.vcCalendarEmbed_2.theCalendar.currentPage.lastDayOfMonth().convertToRequestString()
        let dateRequestArray =  datefrom.components(separatedBy: "-")
        let yearRequest = Int(dateRequestArray[0])!
        
        //Object to request for calendar 1
        var wsObject1: [String : Any]  = [:]
        wsObject1["FromDate"] = datefrom
        wsObject1["ToDate"] = dateTo
        
        LibraryAPI.shared.calendarBO.getAllEvents(wsObject1, onSuccess: {
            (items) -> () in
            
            self.activityIndicator.stopAnimating()
            var itemsCalendar: [CalendarItem] = [CalendarItem]()
            var itemsCalendar2: [CalendarItem] = [CalendarItem]()
            
            //Modifying birthdates year
            for object in items
            {
                if object.type.lowercased() == "birthday"
                {
                    let calendar = Calendar.current
//                    let componentsToday = calendar.components([.Year, .Month, .Day], fromDate: NSDate())
                    var componentsEvent = (calendar as NSCalendar).components([.year, .month, .day], from: object.startDate)
                    componentsEvent.year = yearRequest//componentsToday.year
                    
                    object.startDate = calendar.date(from: componentsEvent)!
                }
                
                
                if object.startDate.compare(self.vcCalendarEmbed_2.theCalendar.currentPage.firstDayOfMonth()) == .orderedAscending
                {
                    itemsCalendar.append(object)
                }
                else
                {
                    itemsCalendar2.append(object)
                }
            }
            
            //Filling data calendar 1
            self.arrayBackupToSearch1.removeAll()
            self.arrayBackupToSearch1 = itemsCalendar
            
            self.arrayCalendarItems1.removeAll()
            self.arrayCalendarItems1 = itemsCalendar
            
            
            self.vcCalendarEmbed_1.theCalendar.reloadData()
            
            //Filling data calendar 2
            //Filling data
            self.arrayBackupToSearch2.removeAll()
            self.arrayBackupToSearch2 = itemsCalendar2
            
            self.arrayCalendarItems2.removeAll()
            self.arrayCalendarItems2 = itemsCalendar2
            
            
            self.vcCalendarEmbed_2.theCalendar.reloadData()
            
            
            //Auto-selection Today
            if self.currentSelectedCalendar == 1
            {
                self.dataDidLoad?()
            }
            
            //Auto-selection Today
            if self.currentSelectedCalendar == 2
            {
                self.dataDidLoad?()
            }
            
            }) { error in
                
                self.activityIndicator.stopAnimating()
                self.tableEvents.displayBackgroundMessage("Error",
                    subMessage: "Try again".localized())
        }
    }
    
    // MARK: - ACTIONS
    func createNewPost()
    {
        let vcNewPost = NewPostModal()
        vcNewPost.dateTagged = self.selectedDate
        
        vcNewPost.onViewControllerClosed = { [weak self] Void in
            
            self?.loadFullCalendar()
        }
        
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = .formSheet
        self.present(navController, animated: true, completion: nil)
    }
    
    private func dotsFor(_ calendar: FSCalendar, _ date: Date) -> (dots: Int, colors: [UIColor]?)
    {
        //Dots
        var isBirthday = false
        var isRelevantDate = false
        var isPost = false
        
        //Verify Date has event
        if calendar.tag == 1
        {
            for item in self.arrayCalendarItems1 where date.getStringStyleMedium() == item.startDate.getStringStyleMedium()
            {
                let arrayEvents = arrayCalendarItems1.filter({
                    $0.startDate.getStringStyleMedium() == date.getStringStyleMedium()
                })
                
                //Get type of events contained
                for event in arrayEvents
                {
                    if event.type.lowercased() == "birthday" { isBirthday = true }
                    if event.type.lowercased() == "post" { isPost = true }
                    if event.type.lowercased() == "relevantdate" { isRelevantDate = true }
                }
            }
        }
        else
        {
            for item in self.arrayCalendarItems2 where date.getStringStyleMedium() == item.startDate.getStringStyleMedium()
            {
                let arrayEvents = arrayCalendarItems2.filter({
                    $0.startDate.getStringStyleMedium() == date.getStringStyleMedium()
                })
                
                //Get type of events contained
                for event in arrayEvents
                {
                    if event.type.lowercased() == "birthday" { isBirthday = true }
                    if event.type.lowercased() == "post" { isPost = true }
                    if event.type.lowercased() == "relevantdate" { isRelevantDate = true }
                }
            }
        }
        
        //Draw dots
        if isBirthday == true && isPost == false && isRelevantDate == false
        {
            //Only Birthday
            return (1, [UIColor.greenBirthday()])
        }
        else if isBirthday == false && isPost == true && isRelevantDate == false
        {
            //Only Post
            return (1, [UIColor.yellowPost()])
        }
        else if isBirthday == false && isPost == false && isRelevantDate == true
        {
            //Only relevant date
            return (1,[UIColor.blueImportantDate()])
        }
        else if isBirthday == true && isPost == true && isRelevantDate == false
        {
            //birthday and post
            return (2, [UIColor.greenBirthday(), UIColor.yellowPost()])
        }
        else if isBirthday == true && isPost == false && isRelevantDate == true
        {
            //birthday and relevant date
            return (2, [UIColor.greenBirthday(), UIColor.blueImportantDate()])
        }
        else if isBirthday == false && isPost == true && isRelevantDate == true
        {
            //Post and relevant date
            return (2,[UIColor.yellowPost(), UIColor.blueImportantDate()])
        }
        else if isBirthday == true && isPost == true && isRelevantDate == true
        {
            //All Birthday, Post and relevant date
            return (3, [UIColor.greenBirthday(), UIColor.yellowPost(), UIColor.blueImportantDate()])
        }
        else
        {
            return (0,  nil)
        }
    }
    
    func deselectCalendar(_ tag: Int)
    {
        if tag == 1
        {
            self.vcCalendarEmbed_1.theCalendar.deselect(self.vcCalendarEmbed_1.theCalendar.selectedDate ?? Date())
        }
        else
        {
            self.vcCalendarEmbed_2.theCalendar.deselect(self.vcCalendarEmbed_2.theCalendar.selectedDate ?? Date())
        }        
    }
    
    // MARK: - ACTIONS CALENDAR (HEADER)
    @IBAction func previousMonth(_ sender: AnyObject)
    {
        self.wasChangedByButton = true
        self.vcCalendarEmbed_1.theCalendar.currentPage = self.vcCalendarEmbed_1.theCalendar.currentPage.dateByAddingMonths(-1)
        self.vcCalendarEmbed_2.theCalendar.currentPage = self.vcCalendarEmbed_2.theCalendar.currentPage.dateByAddingMonths(-1)
    }
    
    
    @IBAction func currentMonthTapped(_ sender: AnyObject)
    {
        self.wasChangedByButton = true
        
        let vcCalendar: CalendarPickerViewController! = CalendarPickerViewController()
        vcCalendar.isPickerForNewPost = false
        vcCalendar.onDate = { [weak self] dateSelected in
            
            if sender.tag == 1
            {
                self?.vcCalendarEmbed_1.theCalendar.currentPage = dateSelected
                
                if let date = self?.vcCalendarEmbed_1.theCalendar.currentPage.dateByAddingMonths(1) {
                
                    self?.vcCalendarEmbed_2.theCalendar.currentPage = date
                }
                
            }
            else
            {
                self?.vcCalendarEmbed_2.theCalendar.currentPage = dateSelected
                
                if let date = self?.vcCalendarEmbed_2.theCalendar.currentPage.dateByAddingMonths(-1) {
                 
                    self?.vcCalendarEmbed_1.theCalendar.currentPage = date
                }
            }
        }
        
        let vcModal = ModalHalfViewController(subViewController: vcCalendar)
        self.present(vcModal, animated:true, completion: nil)
    }
    
    
    @IBAction func nextMonth(_ sender: AnyObject)
    {
        self.wasChangedByButton = true
        self.vcCalendarEmbed_1.theCalendar.currentPage = self.vcCalendarEmbed_1.theCalendar.currentPage.dateByAddingMonths(1)
        self.vcCalendarEmbed_2.theCalendar.currentPage = self.vcCalendarEmbed_2.theCalendar.currentPage.dateByAddingMonths(1)
    }
    
    // MARK: FSCalendar Datasource
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int
    {
        return self.dotsFor(calendar, date).dots
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]?
    {
        return self.dotsFor(calendar, date).colors
    }
    
    // MARK: FSCalendar Delegate
    func calendarCurrentPageDidChange(_ calendar: FSCalendar)
    {
        guard self.wasChangedByButton == true else {
            
            //Detect Calendar
            if calendar.tag == 1
            {
                self.vcCalendarEmbed_2.theCalendar.currentPage = self.vcCalendarEmbed_1.theCalendar.currentPage.dateByAddingMonths(1)
            }
            else
            {
                self.vcCalendarEmbed_1.theCalendar.currentPage = self.vcCalendarEmbed_2.theCalendar.currentPage.dateByAddingMonths(-1)
            }
            
            self.loadFullCalendar()
            return
        }
        
        //Change status
        self.wasChangedByButton = false
        self.loadFullCalendar()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition)
    {
        //Set Date Label
        self.lblCurrentDateSelected.text = date.getStringStyleFull()
        self.selectedDate = date
        
        //Calendars
        if calendar.tag == 1
        {
            self.currentSelectedCalendar = 1
            self.deselectCalendar(2)
            
            //Searching items for date selected
            let arrayEvents = self.arrayCalendarItems1.filter({
                $0.startDate.getStringStyleMedium() == date.getStringStyleMedium()
            })
            
            arrayDateSelected1 = arrayEvents
            
            
            /******CHOOSE WHICH CALENDAR EVENTS*********/
            self.arrayDateSelected = arrayDateSelected1
            self.reloadEventsTable()
            
            
            //Empty State
            if self.arrayDateSelected1.count == 0
            {
                self.tableEvents.displayBackgroundMessage("No events".localized(),
                    subMessage: "")
            }
            else
            {
                self.tableEvents.dismissBackgroundMessage()
            }
            
        }
        else
        {
            self.currentSelectedCalendar = 2
            self.deselectCalendar(1)
            
            //Searching items for date selected
            let arrayEvents = self.arrayCalendarItems2.filter({
                $0.startDate.getStringStyleMedium() == date.getStringStyleMedium()
            })
            
            self.arrayDateSelected2 = arrayEvents
            
            
            /******CHOOSE WHICH CALENDAR EVENTS*********/
            self.arrayDateSelected = arrayDateSelected2
            self.reloadEventsTable()
            
            
            //Empty State
            if arrayDateSelected2.count == 0
            {
                self.tableEvents.displayBackgroundMessage("No events".localized(),
                    subMessage: "")
            }
            else
            {
                self.tableEvents.dismissBackgroundMessage()
            }
        }
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool)
    {
        calendar.frame = CGRect(origin: calendar.frame.origin, size: bounds.size)
        self.view.layoutIfNeeded()
    }

    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier! == "CalendarEmbed_container1"
        {
            self.vcCalendarEmbed_1 = segue.destination as! CalendarEmbed_iPad
        }
        
        if segue.identifier! == "CalendarEmbed_container2"
        {
            self.vcCalendarEmbed_2 = segue.destination as! CalendarEmbed_iPad
        }
    }
}

extension CalendarViewController_iPad: UITableViewDataSource, UITableViewDelegate
{
    // MARK: TABLE VIEW DATASOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let count = currentSelectedCalendar == 1 ? arrayDateSelected1.count : arrayDateSelected2.count
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let item = self.arrayDateSelected[indexPath.row]
        
        if item.type.lowercased() == "post"
        {
            //create custom post
            let customPost = Post()
            customPost.id = item.id
            customPost.title = item.title
            customPost.stakeholders = item.stakeholders
            customPost.geographies = item.geographies
            customPost.attachments = item.attachments
            customPost.projects = item.projects
            customPost.events = item.events
            customPost.createdDateString = item.createdDate
            customPost.createdDate = item.createdDate.toDate()!
            customPost.author = item.createdBy
            customPost.isPrivate  = item.isPrivate
            
            //Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarPostTableCell") as! FeedTableCell
            cell.tvText.delegate = self
            
            cell.iconEvent.image = dotNormal
            cell.iconEvent.layer.cornerRadius = 6
            cell.iconEvent.clipsToBounds = true
            
            
            cell.tvText.text = item.title
            
            cell.cellForPostEvent(cell, post: customPost)
            cell.onStakeholderTap = { [weak self] stakeholder in
                
                //Enabled or Disabled
                guard stakeholder.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: stakeholder.fullName, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                    
                    return
                }
                
                let vcStakeholderDetail = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
                vcStakeholderDetail.stakeholder = stakeholder
                
                self?.navigationController?.pushViewController(vcStakeholderDetail, animated: true)
            }
            
            cell.onAllStakeholdersTap = { [weak self] Void in
                
                let stakeholdersTableVC = StakeholdersTableViewController.getNewInstance()
                stakeholdersTableVC.stakeholders = customPost.stakeholders
                
                let vcModal = ModalViewController(subViewController: stakeholdersTableVC)
                
                self?.present(vcModal, animated:true, completion: nil)
            }
            
            cell.onTextViewTap = { [weak self] Void in
                
                let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController_iPad.self)
                vcDetailPost.post = customPost
                
                self?.navigationController?.pushViewController(vcDetailPost, animated: true)
            }
            
            return cell
        }
        else if item.type.lowercased() == "birthday"
        {
            let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier:"Cell")
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            cell.textLabel?.textColor = UIColor.blackAsfalto()
            cell.textLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
            cell.textLabel?.numberOfLines = 2
            
            let finalText = RelevantEventType.Birthday.rawValue.localized() + " " +  item.title
            cell.textLabel?.text = finalText
            
            /** Style Text */
            let attributedString = NSMutableAttributedString(string: cell.textLabel!.text!)
            attributedString.setBoldAndColor(RelevantEventType.Birthday.rawValue.localized(), color: UIColor.greenBirthday(), size: 10)
            cell.textLabel?.attributedText = attributedString
            
            //type
            cell.imageView?.image = dotBirthday
            cell.imageView?.layer.cornerRadius = 6
            cell.imageView?.clipsToBounds = true
            
            return cell
        }
        else
        {
            //Relevant Dates
            let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier:"Cell")
            cell.textLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
            cell.textLabel?.numberOfLines = 5
            
            let strTitle = item.title + " "
            let strDescription = item.itemDescription
            let strItemInfoAtt = (strTitle + strDescription).mutableAttributed
            
            strItemInfoAtt.setAttributes([NSFontAttributeName:UIFont.systemFont(ofSize: 14),NSForegroundColorAttributeName:UIColor.blueBelize()], range: NSRange(location:0, length: strTitle.mutableAttributed.length))
            strItemInfoAtt.setAttributes([NSFontAttributeName:UIFont.systemFont(ofSize: 14),NSForegroundColorAttributeName:UIColor.blackAsfalto()], range: NSRange(location:strTitle.mutableAttributed.length, length: strDescription.mutableAttributed.length))
            
            
            //cell.textLabel?.text = item.title
            cell.textLabel?.attributedText = strItemInfoAtt
            
            //type
            cell.imageView?.image = dotRelevantDate
            cell.imageView?.layer.cornerRadius = 6
            cell.imageView?.clipsToBounds = true
            
            return cell
        }
    }
    
    // MARK: TABLE VIEW DELEGATE
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = arrayDateSelected[indexPath.row]
        
        if item.type.lowercased() == "post"
        {
            let customPost = Post()
            customPost.id = item.id
            customPost.title = item.title
            customPost.stakeholders = item.stakeholders
            customPost.attachments = item.attachments
            customPost.projects = item.projects
            customPost.events = item.events
            customPost.createdDateString = item.createdDate
            customPost.createdDate = item.createdDate.toDate()!
            customPost.author = item.createdBy
            
            let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController_iPad.self)
            vcDetailPost.post = customPost
            navigationController?.pushViewController(vcDetailPost, animated: true)
        }
        else
        {
            let event = RelevantDate()
            event.id = item.id
            event.isFreeDateString = false 
            event.date = item.startDate
            event.stringValue = item.startDate.convertToRequestString()
            event.title = item.title
            event.type = (item.type.lowercased() == "birthday") ? 1 : 2
            
            if event.type == 1
            {
                //Birthday
                let alert = UIAlertController(title: "Add Event".localized(), message: "Would you like to save this date in your device calendar".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Save".localized(), style: UIAlertActionStyle.default, handler: {
                    (action) in
                    
                    let finalText = RelevantEventType.Birthday.rawValue.localized() + ": " +  event.title
                    CalendarManager.sharedInstance.createEventWithString(finalText, date:event.date!)
                }))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                //Relevant date
                let alert = UIAlertController(title: "Description".localized(), message:item.title, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Add Event".localized(), style: UIAlertActionStyle.default, handler: {
                    (action) in
                    
                    let finalText = "Relevant Date: ".localized() +  event.title
                    CalendarManager.sharedInstance.createEventWithString(finalText, date:event.date!)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        cell.backgroundColor = UIColor.blueSky()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        guard self.arrayDateSelected.count > 0 else{
            return 0.0
        }
        
        if self.arrayDateSelected[indexPath.row].type.lowercased() == "birthday"
        {
            return 44
        }
        else if self.arrayDateSelected[indexPath.row].type.lowercased() == "post"
        {
            return 115
        }
        else
        {
            return 70
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return CGFloat.leastNormalMagnitude
    }
}

extension CalendarViewController_iPad: UITextViewDelegate
{
    // MARK: TEXTVIEW DELEGATE (FeedTableCell)
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
    {
        let arrayUrl = URL.description.components(separatedBy: "_")
        
        if arrayUrl.count > 1
        {
            let type = arrayUrl[0]
            
            switch type
            {
            case "s":
                //Go to Stakeholder detail
                let stakeholderLinked: Stakeholder! = Stakeholder(isDefault: true)
                stakeholderLinked.id = Int(arrayUrl[1])!
                stakeholderLinked.isEnable = Int(arrayUrl[2])! == 1 ? true : false
                
                let firstname = arrayUrl[3]
                
                //Enabled or Disabled
                guard stakeholderLinked.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: firstname, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                    
                    return true
                }
                
                let vcStakeholderDetail = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
                vcStakeholderDetail.stakeholder = stakeholderLinked
                navigationController?.pushViewController(vcStakeholderDetail, animated: true)
            case "p":
                //Go to Project detail
                let projectLinked: Project! = Project(isDefault: true)
                projectLinked.id = Int(arrayUrl[1])!
                projectLinked.isEnable = Int(arrayUrl[2])! == 1 ? true : false
                
                //Enabled or Disabled
                guard projectLinked.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: nil, subtitle: "This project is disabled".localized(), type: .info, fromBottom: false)
                    
                    return true
                }
                
                //Go to detail
                let vcDetailProject = Storyboard.getInstanceOf(ProjectDetailViewController_iPad.self)
                vcDetailProject.project = projectLinked
                navigationController?.pushViewController(vcDetailProject, animated: true)
                
            case "d":
                //Add Event to Calendar
                let alert = UIAlertController(title: "Add Event".localized(), message: "Would you like to save this date in your device calendar".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Save".localized(), style: UIAlertActionStyle.default, handler: {
                    (action) in
                    
                    for item in self.arrayDateSelected where item.type.lowercased() == "post"
                    {
                        for theEvent in item.events where theEvent.id == Int(arrayUrl[1])!
                        {
                            //Create post
                            let customPost = Post()
                            customPost.id = item.id
                            customPost.title = item.title
                            customPost.stakeholders = item.stakeholders
                            customPost.attachments = item.attachments
                            customPost.projects = item.projects
                            customPost.events = item.events
                            customPost.createdDateString = item.createdDate
                            customPost.createdDate = item.createdDate.toDate()!
                            customPost.author = item.createdBy
                            
                            //Save Event to iPhone Calendar
                            CalendarManager.sharedInstance.createEvent(customPost.title.getEditedTitleForPost(customPost, truncate: false), event: theEvent)
                        }
                    }
                    
                }))
                self.present(alert, animated: true, completion: nil)
            case "g":
                let geographyId = Int(arrayUrl[1])!
                if DeviceType.IS_ANY_IPAD == false
                {
                    let vcGeoDetail:GeographyDetailViewController = UIStoryboard(name: "Geography", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetailViewController") as! GeographyDetailViewController
                    vcGeoDetail.geographyId = geographyId
                    self.navigationController?.pushViewController(vcGeoDetail, animated: true)
                }
                else
                {
                    let vcGeoDetail:GeographyDetail_iPad = UIStoryboard(name: "GeographyPad", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetail_iPad") as! GeographyDetail_iPad
                    vcGeoDetail.geographyId = geographyId
                    self.navigationController?.pushViewController(vcGeoDetail, animated: true)
                }
            default:
                break
            }
        }
        
        return true
    }
}
