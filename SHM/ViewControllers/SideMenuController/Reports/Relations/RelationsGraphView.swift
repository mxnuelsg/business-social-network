//
//  RelationsGraphView.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 1/28/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class RelationsGraphView: UIView
{
    //MARK: RELATION LEVEL DATA STRUCT
    struct RelationOrbit
    {
        var radius                  :CGFloat
        var relations               :[Stakeholder]?
        var maxNumberOfSatellites   :Int
        var satelliteViews          :[ProfileRoundView]
        var level                   :Int
        var angleDiff               :CGFloat
    }
    
    //MARK: PROPERTIES FOR GEOMETRY
    var width: CGFloat!
    var height: CGFloat!
    var centerPoint: CGPoint!
    var radius: CGFloat!
    var adjustCoeff: CGFloat!
    var imgCentralProfile: UIImage!
    var imgDictionary:[String:UIImage] = [:]
    
    //MARK: PROPERTIES AND OUTLETS
    var SystemOrbits = [RelationOrbit]()
    var isViewBeingRedrawn = false
    var isInitialLoadFinished = false
    var arrayOrbitRadiusCoeff:[CGFloat]!
    var onDrawRect:(() -> ())?
    var imageCache = NSCache<AnyObject, AnyObject>()
    var numberOfSatellitesInFirstOrbit: Int!
    //MARK: LIFE CYCLE
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    required override init(frame: CGRect)
    {
        super.init(frame: frame)
    }
    
    //MARK: DRAWING ROUTINES
    override func draw(_ rect: CGRect)
    {
        if self.isInitialLoadFinished == true
        {
            if self.isViewBeingRedrawn == false
            {
                self.drawGraphOfRelations()
            }
            else
            {
                self.redrawGraphOfRelations()
            }
        }
    }
    
    func drawOrbits()
    {
        let context = UIGraphicsGetCurrentContext()
        context?.saveGState()
        context?.setStrokeColor(UIColor.grayCloudy().cgColor)
        context?.setLineWidth(1)
        context?.addArc(center: CGPoint(x: self.centerPoint.x, y: self.centerPoint.y), radius: self.SystemOrbits[0].radius, startAngle: 0.0, endAngle: self.toRad(360), clockwise: true)
        context?.strokePath()
        context?.addArc(center: CGPoint(x: self.centerPoint.x, y: self.centerPoint.y), radius: self.SystemOrbits[1].radius, startAngle: 0.0, endAngle: self.toRad(360), clockwise: true)
        context?.strokePath()
        context?.addArc(center: CGPoint(x: self.centerPoint.x, y: self.centerPoint.y), radius: self.SystemOrbits[2].radius, startAngle: 0.0, endAngle: self.toRad(360), clockwise: true)
        context?.strokePath()
        context?.addArc(center: CGPoint(x: self.centerPoint.x, y: self.centerPoint.y), radius: self.SystemOrbits[3].radius, startAngle: 0.0, endAngle: self.toRad(360), clockwise: true)
        context?.strokePath()
        context?.restoreGState()
    }

    func redrawGraphOfRelations()
    {
        self.onDrawRect?()
        self.drawOrbits()
    }
    
    func drawGraphOfRelations()
    {
        self.drawOrbits()
        self.placeSatellites()
    }
    
    //MARK: PLACE SATELLITES
    func placeSatellites()
    {
        if self.SystemOrbits.count > 0
        {
            for orbitNumber in 0...self.SystemOrbits.count - 1
            {
                self.placeSatelliteInOrbit(orbitNumber)
            }
        }
    }
    
    func placeSatelliteInOrbit(_ orbitNumber:Int)
    {
        if let relations = self.SystemOrbits[orbitNumber].relations, relations.count > 0 {
            
            //For more relations tham satellites in orbit
            if relations.count > self.SystemOrbits[orbitNumber].maxNumberOfSatellites
            {
                for (index, satelliteView) in self.SystemOrbits[orbitNumber].satelliteViews.enumerated()
                {
                    if index > self.SystemOrbits[orbitNumber].maxNumberOfSatellites - 2
                    {
                        break
                    }
                    
                    self.setProfileImage(relations[index].thumbnailUrl, satelliteView: satelliteView)
                    satelliteView.stakeholders = [relations[index]]
                    satelliteView.layer.masksToBounds = true
                    self.setBorderColorForSatellite(index, inOrbit: orbitNumber, with: relations)
                    self.addSubview(satelliteView)
                    self.setIdCard(satelliteView.frame, isStakeholder: relations[index].isStakeholder)
                }
                
                let satelliteViewEmbedded = self.SystemOrbits[orbitNumber].satelliteViews[self.SystemOrbits[orbitNumber].maxNumberOfSatellites-1]
                let label = UILabel(frame: satelliteViewEmbedded.frame.insetBy(dx: 0,dy: 0))
                label.layer.cornerRadius = label.frame.width/2
                label.layer.backgroundColor = UIColor.white.cgColor
                label.layer.masksToBounds = true
                label.textAlignment = NSTextAlignment.center
                label.text = String(relations.count - (self.SystemOrbits[orbitNumber].maxNumberOfSatellites-1))
                self.addSubview(label)
                
                satelliteViewEmbedded.doesContainEmbeddedProfile = true
                satelliteViewEmbedded.stakeholders = relations
                satelliteViewEmbedded.layer.masksToBounds = true
                satelliteViewEmbedded.layer.borderColor = UIColor.black.cgColor
                self.addSubview(satelliteViewEmbedded)
                satelliteViewEmbedded.setNeedsDisplay()
            }
            else
            {
                //For less relations tham satellites in orbit
                for (index,satelliteView) in self.SystemOrbits[orbitNumber].satelliteViews.enumerated()
                {
                    if index > relations.count - 1
                    {
                        break
                    }
                    
                    self.setProfileImage(relations[index].thumbnailUrl, satelliteView: satelliteView)
                    satelliteView.stakeholders = [relations[index]]
                    satelliteView.layer.masksToBounds = true
                    self.setBorderColorForSatellite(index, inOrbit: orbitNumber, with: relations)
                    self.addSubview(satelliteView)
                    self.setIdCard(satelliteView.frame, isStakeholder: relations[index].isStakeholder)
                }
            }
        }
    }
    
    //MARK: UTILITY FUNCTIONS
    func setBorderColorForSatellite(_ satellite:Int, inOrbit:Int, with relations:[Stakeholder])
    {
        if let relationTypeKey = relations[satellite].relationType1?.key {
            
            switch relationTypeKey
            {
            case 1:
                //family
                self.SystemOrbits[inOrbit].satelliteViews[satellite].layer.borderColor = UIColor.greenFamily().cgColor
            case 2:
                //friends
                self.SystemOrbits[inOrbit].satelliteViews[satellite].layer.borderColor = UIColor.yellowFriends().cgColor
            case 3:
                //work
                self.SystemOrbits[inOrbit].satelliteViews[satellite].layer.borderColor = UIColor.orangeWork().cgColor
            default:
                break
            }
        }
    }
    
    func getProfileImage(_ strURL:String) -> UIImage
    {
        let imgProfile = UIImageView()
        
        if let url = URL(string: strURL) {
            
            imgProfile.setImageWith(url, placeholderImage: UIImage(named: "defaultperson"))
            
            if let image = imgProfile.image {
                return image
            }
        }

        return UIImage()
    }
    
    func setProfileImage (_ thumbnailUrl:String, satelliteView:ProfileRoundView)
    {
        if let profileImage = CacheManager.shared.getCachedImage(thumbnailUrl) {
            
            satelliteView.profileImage  = profileImage 
            satelliteView.setNeedsDisplay()
        }
        else
        {
            satelliteView.layer.contents  = UIImage(named:"defaultperson")?.cgImage
            
            //Prepare dispatch queue
            let queue = DispatchQueue(label: "SHM_RelationsReportSetProfileImage", qos: .userInitiated)
            
            //Work item to be executed withing the queue
            let setProfileImageAndUpdate = DispatchWorkItem {
                
                if let url  = URL(string: thumbnailUrl), let data = try? Data( contentsOf: url)  {
                    
                    let image = UIImage(data: data)
                    DispatchQueue.main.async {
                        
                        if let profileImage = image {
                            
                            CacheManager.shared.imageCache.setObject(profileImage, forKey: thumbnailUrl as AnyObject)
                        }
                        satelliteView.profileImage = image
                        satelliteView.setNeedsDisplay()
                    }
                }
            }
            
            //Execute async operations
            queue.async(execute: setProfileImageAndUpdate)

        }
    }
    
    func setIdCard(_ satelliteFrame:CGRect, isStakeholder:Bool)
    {
        if isStakeholder == false
        {
            var idFrame = CGRect(x: satelliteFrame.origin.x - 7, y: satelliteFrame.origin.y, width: 15, height: 10)
            if DeviceType.IS_ANY_IPAD == true
            {
                idFrame = CGRect(x: satelliteFrame.origin.x-7, y: satelliteFrame.origin.y, width: 30, height: 15)
            }
            
            let idCard = UIImageView(frame: idFrame)
            idCard.image = UIImage(named: "IconIDBlue")
            self.addSubview(idCard)
        }
    }
    
    //MARK: MATH UTILITIES
    /*
    Polar to Cartesian coordinates
    This function returns a point (x,y) at the circunference line of a circle. 
    It needs a center point (of the given circle), a radius (of the given circle)
    and an angle (0-360 degrees)
    */
    func getPointAtCircunferenceOf(radius:CGFloat, WithCenter center:CGPoint, AtAngle angle:CGFloat) -> CGPoint
    {
        let rads = self.toRad(angle)
        let x = center.x + radius * cos(rads)
        let y = center.y - radius * sin(rads)   /* (+ or -) depends of coordinate system */
        
        return CGPoint(x: x, y: y)
    }
    
    /*
    This functions returns the radius of all satellites in the system.
    It takes as a reference:
            - The number of satellites in the first orbit (numberOfSatellites in first orbit)
            - The Radius of the largest orbit in the system (named systemRadius here or 'R')
            - The  inner Radius Coefficient, which is the radius percentage coef (0.0 - 1.0) of the first orbit ('r')
    
        Then it divides de circunference by the number of satellites in the firsr orbit, the result is an angle differencial (dø)

        Finally it obtains the radius of the satellite using:
            satelliteRadius = ( R*r*sin(dø) ) / 2
    
        The adjustment Coefficient (adjustCoeff) controls the space between each satellite in a orbit
    */
    func getSatelliteRadius() -> CGFloat
    {
        //Satellites on first orbit determines the size of all satellites
        let numberOfSatellitesInFirstOrbit:CGFloat = CGFloat(self.numberOfSatellitesInFirstOrbit)
        let systemRadius      = self.getSystemRadius()
        let innerRadiusCoeff  = self.arrayOrbitRadiusCoeff[1]
        let angleDifferencial = 360 / numberOfSatellitesInFirstOrbit
        var radius            = (systemRadius*innerRadiusCoeff * sin(self.toRad(angleDifferencial))) / 2.0
        
        //Prevent negative radius due sine values between 180º-360º
        if radius < 0
        {
            radius *= -1
        }
        
        return radius * self.adjustCoeff
    }
    
    //Obtains an angle differencial (dø in degrees)
    func angleDifferencialWith(_ howManySatellites:CGFloat) -> CGFloat
    {
        return (360 / howManySatellites)
    }
    
    //Transform degrees to Radians
    func toRad(_ degrees:CGFloat) -> CGFloat
    {
        let rads = (CGFloat(M_PI) * degrees) / 180

        return CGFloat(rads)
    }
    
    func getSystemRadius() -> CGFloat
    {
        //Get the best fit radius
        let width:CGFloat = self.frame.width
        let height:CGFloat = self.frame.height
        let radius = (height >= width) ? width/2.0 : height/2.0
        return radius
    }
    
    func getSystemCenter()->(CGPoint)
    {
        return CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
    }
    
}
