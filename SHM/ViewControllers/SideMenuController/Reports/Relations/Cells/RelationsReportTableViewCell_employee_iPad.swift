//
//  RelationsReportTableViewCell_employee_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 2/15/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class RelationsReportTableViewCell_employee_iPad: UITableViewCell
{
    //MARK: PROPERTIES AND OUTLETS
    var onSeeRelationsTapped:(() -> ())?
    var onSeeDetailTapped:(() -> ())?
    var onCloseBtnTapped:(() -> ())?
    
    @IBOutlet weak var imgProfile:      UIImageView!
    @IBOutlet weak var lblFullName:     UILabel!
    @IBOutlet weak var lblJobPosition:  UILabel!
    @IBOutlet weak var lblPhone:        UILabel!
    @IBOutlet weak var lblEmail:        UILabel!
    @IBOutlet weak var lblPhoneInfo:    UILabel!
    @IBOutlet weak var lblEmailInfo:    UILabel!
    @IBOutlet weak var btnSeeDetail:    UIButton!
    @IBOutlet weak var btnSeeRelations: UIButton!
    @IBOutlet weak var btnClose:        UIButton!
    
    @IBOutlet weak var lblRelationType: UILabel!
    //MARK: LIFE CYCLE
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.loadConfig()
    }

    //MARK: LOAD CONFIG
    func loadConfig()
    {
        //Set title and border of cell buttons
        self.btnSeeDetail.setBorder()
        self.btnSeeDetail.layer.cornerRadius = 3
        self.btnSeeDetail.clipsToBounds = true
        
        self.btnSeeRelations.setBorder()
        self.btnSeeRelations.layer.cornerRadius = 3
        self.btnSeeRelations.clipsToBounds = true
        
        self.btnClose.setBorder()
        self.btnClose.layer.cornerRadius = 3
        self.btnClose.clipsToBounds = true
        
        self.setLocalizedString()
    }
    
    //MARK: PRIVATE FUNCTIONS
    func setLocalizedString()
    {
        self.lblPhone.text = "Phone".localized()
        self.lblEmail.text = "Email".localized()
        self.btnSeeRelations.setTitle("See Relations".localized(), for: UIControlState())
    }
    
    func loadCellWith(_ stakeholderSelected:Stakeholder)
    {
        //Prepare a string for not available information
        let attStrNotInfoAvailable = NSMutableAttributedString(string: String("No Information Available".localized()))
        attStrNotInfoAvailable.setColor(String("No Information Available".localized()), color: UIColor.grayConcreto())
        attStrNotInfoAvailable.setBold(String("No Information Available".localized()), size: 11)
        
        //Set profile image
        self.imgProfile.setImageWith(URL(string: stakeholderSelected.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        
        //Set full name
        self.lblFullName.text = stakeholderSelected.fullName
        
        
        //Set JobPosition/Company
        let strJobPosition = stakeholderSelected.jobPosition?.value ?? ""
        self.lblJobPosition.attributedText = NSMutableAttributedString().setJobPositionCompany(strJobPosition, company: stakeholderSelected.companyName)
        
        
        //set phone
        if stakeholderSelected.phone.isEmpty == false {
            
            self.lblPhoneInfo.text = stakeholderSelected.phone
        }
        else
        {
            self.lblPhoneInfo.attributedText = attStrNotInfoAvailable
        }
        
        //Set email
        if stakeholderSelected.email.isEmpty == false {
            
            self.lblEmailInfo.text = stakeholderSelected.email
        }
        else
        {
            self.lblEmailInfo.attributedText = attStrNotInfoAvailable
        }
        
        //Relation type
        let strRelationType1 = stakeholderSelected.relationType1?.value ?? ""
        let strRelationType2 = stakeholderSelected.relationType2?.value ?? ""
        if (strRelationType1 != "" && strRelationType2 != "")
        {
            self.lblRelationType.text = "\(strRelationType1) - \(strRelationType2)"
        }
        else
        {
            self.lblRelationType.isHidden = true
        }
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }

    //MARK: BUTTON ACTIONS
    @IBAction func actionSeeRelations(_ sender: AnyObject)
    {
        self.onSeeRelationsTapped?()        
    }
    
    @IBAction func actionSeeDetail(_ sender: AnyObject)
    {

        self.onSeeDetailTapped?()
    }
    
    @IBAction func actionClose(_ sender: AnyObject)
    {
        self.onCloseBtnTapped?()
    }
    
    //MARK: CELL DELEGATE
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
