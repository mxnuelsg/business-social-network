//
//  RelationsReportTableViewCell_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 2/15/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class RelationsReportTableViewCell_iPad: UITableViewCell
{

    var onSeeRelationsTapped:(() -> ())?
    var onSeeDetailTapped:(() -> ())?
    var onCloseBtnTapped:(() -> ())?
    
    @IBOutlet weak var imgContactProfile: UIImageView!
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var lblJobtTitle: UILabel!
    @IBOutlet weak var lblBirthPlace: UILabel!
    @IBOutlet weak var lblBirthdateAge: UILabel!
    @IBOutlet weak var lblOcuppation: UILabel!
    @IBOutlet weak var lblNationality: UILabel!
    @IBOutlet weak var lblCoordinator: UILabel!
    @IBOutlet weak var lblRepresentative: UILabel!
    @IBOutlet weak var lblBirthPlaceInfo: UILabel!
    @IBOutlet weak var lblBirthAgeInfo: UILabel!
    @IBOutlet weak var lblOccupationInfo: UILabel!
    @IBOutlet weak var lblNationalityInfo: UILabel!
    @IBOutlet weak var lblCoordinatorInfo: UILabel!
    @IBOutlet weak var lblRepresentativeInfo: UILabel!
    
    @IBOutlet weak var imgCoordinatorInbox: UIImageView!
    
    @IBOutlet weak var btnSeeRelations: UIButton!    
    @IBOutlet weak var btnDetail: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblRelationType: UILabel!
    @IBOutlet weak var containerBtnActions: UIView!
    
    
    //Rating
    @IBOutlet weak var lblAdminRatingTitle: UILabel!
    @IBOutlet weak var lblAdminRating: UILabel!
    @IBOutlet weak var lblUserRatingTitle: UILabel!    
    @IBOutlet weak var lblUserRating: UILabel!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        self.loadConfig()
        //self.containerBtnActions.isHidden = true
    }

    
    //MARK: LOAD CONFIG
    func loadConfig()
    {
        //Set title and border of cell buttons
        self.btnDetail.setBorder()
        self.btnDetail.layer.cornerRadius = 3
        self.btnDetail.clipsToBounds = true
        
        self.btnSeeRelations.setBorder()
        self.btnSeeRelations.layer.cornerRadius = 3
        self.btnSeeRelations.clipsToBounds = true
        
        self.btnClose.setBorder()
        self.btnClose.layer.cornerRadius = 3
        self.btnClose.clipsToBounds = true
        
        self.setLocalizedString()
    }

    //MARK: PRIVATE FUNCTIONS
    func setLocalizedString()
    {
        self.lblBirthPlace.text = "Place of Birth".localized()
    
        self.lblBirthdateAge.text = "Birthday (Age)".localized()
        self.lblOcuppation.text = "Occupation".localized()
        self.lblCoordinator.text = "Coordinator".localized()
        self.lblRepresentative.text = "Representative".localized()
        
        self.btnSeeRelations.setTitle("SeeRelations".localized(), for: UIControlState())
        self.btnDetail.setTitle("Stakeholder's Detail".localized(), for: UIControlState())
        
    }
    
    func loadCellWith(_ stakeholderSelected:Stakeholder)
    {
        //Prepare a string for missing information
        let attStrNotInfoAvailable = NSMutableAttributedString(string: String("No Information Available".localized()))
        attStrNotInfoAvailable.setColor(String("No Information Available".localized()), color: UIColor.grayConcreto())
        attStrNotInfoAvailable.setBold(String("No Information Available".localized()), size: 11)
        
        //Set profile image
        self.imgContactProfile.setImageWith(URL(string: stakeholderSelected.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        
        //Name
        self.lblFullName.text = stakeholderSelected.fullName
        
        //JobPosition/Company
        let strJobPosition = stakeholderSelected.jobPosition?.value ?? ""
        self.lblJobtTitle.attributedText = NSMutableAttributedString().setJobPositionCompany(strJobPosition, company: stakeholderSelected.companyName)
        
        //Birth place
        var strBirthPlaceCountry = String()
        
        if stakeholderSelected.placeOfBirth.isEmpty == false
        {
            strBirthPlaceCountry += stakeholderSelected.placeOfBirth
        }
        else
        {
            strBirthPlaceCountry +=  "No Information Available".localized()
        }
        
        if let strCountry = stakeholderSelected.country, strCountry.value != ""{
            
            strBirthPlaceCountry += "/" + strCountry.value
        }
        else
        {
            strBirthPlaceCountry += "/" + "No Information Available".localized()
        }
        
        let attBirthPlace = NSMutableAttributedString(string: strBirthPlaceCountry)
        attBirthPlace.setColor(String("No Information Available".localized()), color: UIColor.grayConcreto())
        attBirthPlace.setBold(String("No Information Available".localized()), size: 11)
        attBirthPlace.setColor("/"+String("No Information Available".localized()), color: UIColor.grayConcreto())
        attBirthPlace.setBold("/"+String("No Information Available".localized()), size:1)
        self.lblBirthPlaceInfo.attributedText = attBirthPlace
        
        //Birthdate and Age
        let strAge = stakeholderSelected.age != nil ? String(stakeholderSelected.age!) : "No Information Available".localized()
        
        let birthday = (stakeholderSelected.birthday != nil && stakeholderSelected.birthday!.isValid()) ? stakeholderSelected.birthday : nil
        let strBirthday = birthday?.getStringStyleMedium()
            ?? stakeholderSelected.birthDateString
            ?? "No Information Available".localized()
        
        let attStrBirthdateAge = birthday != nil ? NSMutableAttributedString(string:strBirthday + " (" + strAge + " " + String("Years old".localized()) + ")") : NSMutableAttributedString(string:strBirthday)
        
        attStrBirthdateAge.setColor(String("No Information Available".localized()), color: UIColor.grayConcreto())
        attStrBirthdateAge.setBold(String("No Information Available".localized()), size: 11)
        self.lblBirthAgeInfo.attributedText = attStrBirthdateAge
        
        //Occupation
        if let strOccupation = stakeholderSelected.occupation?.value, strOccupation != "" {
            
            self.lblOccupationInfo.text = strOccupation
        }
        else
        {
            self.lblOccupationInfo.attributedText = attStrNotInfoAvailable
        }
        
        //Nationalities
        self.lblNationality.text = "Nationality".localized()
        
        if stakeholderSelected.nationalities.count > 0 {
            
            let strNationality = stakeholderSelected.nationalities[0].value
            self.lblNationalityInfo.text = strNationality
        }
        else
        {
            self.lblNationalityInfo.attributedText = attStrNotInfoAvailable
        }
        
        //Coordinator
        //cell.lblCoordinatorInfo.adjustsFontSizeToFitWidth = true
        if let coordinator = stakeholderSelected.coordinator?.fullName, coordinator != ""{
            
            let strAttCoordinatorName = NSMutableAttributedString(string: coordinator)
            strAttCoordinatorName.setUnderline(coordinator, size: 11, color: UIColor.blueBelize(), bold: false)
            self.lblCoordinatorInfo.attributedText = strAttCoordinatorName
            self.lblCoordinatorInfo.isUserInteractionEnabled = true
            
            /* Inbox action is disabled for now */
            self.imgCoordinatorInbox.isHidden = true
            self.imgCoordinatorInbox.isUserInteractionEnabled = false
        }
        else
        {
            self.imgCoordinatorInbox.isHidden = true
            self.lblCoordinatorInfo.attributedText = attStrNotInfoAvailable
        }
        
        //Representative
        
        if let representative = stakeholderSelected.representative?.fullName, representative != ""{
            
            let strAttRepresentativeName = NSMutableAttributedString(string: representative)
            strAttRepresentativeName.setUnderline(representative, size: 11, color: UIColor.blueBelize(), bold: false)
            self.lblRepresentativeInfo.attributedText = strAttRepresentativeName
        }
        else
        {
            self.lblRepresentativeInfo.attributedText = attStrNotInfoAvailable
        }
        
        //Relation type
        let strRelationType1 = stakeholderSelected.relationType1?.value ?? ""
        let strRelationType2 = stakeholderSelected.relationType2?.value ?? ""
        if (strRelationType1 != "" && strRelationType2 != "")
        {
            self.lblRelationType.text = "\(strRelationType1) - \(strRelationType2)"
        }
        else
        {
            self.lblRelationType.isHidden = true
        }
        self.selectionStyle = UITableViewCellSelectionStyle.none
        //Rating
        self.lblUserRatingTitle.adjustsFontSizeToFitWidth = true
        self.lblUserRating.adjustsFontSizeToFitWidth = true
        self.lblUserRatingTitle.text = "User Rating".localized()
        self.lblUserRating.text = (stakeholderSelected.userRating ?? 0.0).getString()
        
        
        self.lblAdminRatingTitle.adjustsFontSizeToFitWidth = true
        self.lblAdminRating.adjustsFontSizeToFitWidth = true
        self.lblAdminRatingTitle.text = "Admin Rating".localized()
        self.lblAdminRating.text = (stakeholderSelected.adminRating ?? 0.0).getString()
    }
    
    //MARK: BUTTON ACTIONS
    
    @IBAction func actionSeeRelations(_ sender: AnyObject)
    {
        self.onSeeRelationsTapped?()
    }
    
    @IBAction func actionDetail(_ sender: AnyObject)
    {
        self.onSeeDetailTapped?()
    }
    
    @IBAction func actionClose(_ sender: AnyObject)
    {
        self.onCloseBtnTapped?()
    }
    
    //MARK: CELL DELEGATE
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
