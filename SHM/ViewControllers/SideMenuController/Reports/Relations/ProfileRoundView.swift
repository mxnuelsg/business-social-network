//
//  ProfileRoundView.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 1/27/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class ProfileRoundView: UIView
{
    //MARK: PROPERTIES
    var profileImage: UIImage?
    var stakeholders: [Stakeholder]!
    var onSatelliteProfileTapped: ((_ stakeholder:[Stakeholder]) -> ())?

    //for ipad redrawing
    var doesContainEmbeddedProfile = false
    
    //MARK: LIFE CYCLE
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        self.layer.cornerRadius = self.frame.size.width/2
        self.layer.borderWidth = 3.0
    }

    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: DRAWING METHODS
    override func draw(_ rect: CGRect)
    {
        let currentContext = UIGraphicsGetCurrentContext();
        guard let context = currentContext  else {
        
            return
        }
        
        if let image = self.profileImage {
            
            context.saveGState()
            context.translateBy(x: 0, y: self.bounds.height)
            context.scaleBy(x: 1.0, y: -1.0)
            
            let img = image.cgImage
            context.draw(img!, in: self.bounds.insetBy(dx: 0, dy: 0))
            context.restoreGState()
        }
    }
    
    //MARK: Utility
    func getEmbeddedProfilesCount() -> Int
    {
        if let relations = self.stakeholders {
            
            return relations.count
        }
        
        return 0
    }
    
    //MARK: TOUCHES LIFE CYCLE
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if let contactStakeholders = self.stakeholders, contactStakeholders.count > 0 {
            
                print("\(contactStakeholders[0].fullName)")
                self.onSatelliteProfileTapped?(contactStakeholders)
        }
    }
}
