//
//  RelationsReportTableViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 2/15/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class RelationsReportTableViewController_iPad: RelationsReportTableViewController
{
    //MARK: LIFE CYCLE
    var orientation = UIDevice.current.orientation
    override func viewDidLoad()
    {
        tableView.register(UINib(nibName: "RelationsReportTableViewCell_iPad", bundle: nil), forCellReuseIdentifier: "RelationsReportTableViewCell_iPad")
        tableView.register(UINib(nibName: "RelationsReportTableViewCell_employee_iPad", bundle: nil), forCellReuseIdentifier: "RelationsReportTableViewCell_employee_iPad")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 265
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.backButtonArrow()
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        //Check that  stakeholder array is not empy
        guard  self.stakeholders.count > 0 else {
            
            return RelationsReportTableViewCell()
        }
        
        //Identify if employee or stakeholder
        if self.stakeholders[indexPath.row].isStakeholder == true
        {
            //Configure the cell for stakeholder
            if let cell = tableView.dequeueReusableCell(withIdentifier: "RelationsReportTableViewCell_iPad", for: indexPath) as? RelationsReportTableViewCell_iPad {
                
                //Set background color for embedded or onMainscreen
                if self.isEmbedded == true
                {
                    cell.contentView.backgroundColor = UIColor.white
                }
                else
                {
                    cell.btnClose.isHidden = true
                    cell.btnDetail.isHidden = true
                    cell.btnSeeRelations.isHidden = true
                }
                
                if self.isRelationReport == false
                {
                    cell.btnSeeRelations.isHidden = true
                }
                var stakeholderSelected = self.stakeholders[indexPath.row]
                cell.loadCellWith(stakeholderSelected)
                if let coordinator = stakeholderSelected.coordinator?.fullName, coordinator != "" {

                    cell.lblCoordinatorInfo.isUserInteractionEnabled = true
                    cell.lblCoordinatorInfo.removeGestureRecognizersOfType(ActorTapGestureRecognizer.self)
                    cell.lblCoordinatorInfo.addGestureRecognizer(ActorTapGestureRecognizer(actor: stakeholderSelected, onTap: { (actor) -> Void in
                        
                        if let coordinator = actor.coordinator {
                            
                            let vcProfile = Storyboard.getInstanceOf(ProfileViewController_iPad.self)
                            vcProfile.member = coordinator
                            if self.isEmbedded == false
                            {
                                self.navigationController?.pushViewController(vcProfile, animated: true)
                            }
                            else
                            {
                                let navController = NavyController(rootViewController:vcProfile)
                                vcProfile.isModal = true
                                self.present(navController, animated: true, completion: nil)
                            }
                        }
                    }))
                    
                    /* Inbox action is disabled for now */
                    cell.imgCoordinatorInbox.isHidden = true
                    cell.imgCoordinatorInbox.isUserInteractionEnabled = false
                    cell.imgCoordinatorInbox.removeGestureRecognizersOfType(ActorTapGestureRecognizer.self)
                    cell.imgCoordinatorInbox.addGestureRecognizer(ActorTapGestureRecognizer(actor: stakeholderSelected, onTap: { (actor) -> Void in
                        
                        if let coordinator = actor.coordinator {
                            
                            let vcNewMessage = Storyboard.getInstanceOf(NewMessageTableViewController.self)
                            vcNewMessage.recipientsSelected = [coordinator]
                            let navController = NavyController(rootViewController:vcNewMessage)
                            self.present(navController, animated: true, completion: nil)
                        }
                    }))
                }
                else
                {
                    cell.imgCoordinatorInbox.isHidden = true
                }
                
                //Representative
                if let representative = stakeholderSelected.representative?.fullName, representative != "" {
                    
                    cell.lblRepresentativeInfo.isUserInteractionEnabled = true
                    cell.lblRepresentativeInfo.removeGestureRecognizersOfType(ActorTapGestureRecognizer.self)
                    cell.lblRepresentativeInfo.addGestureRecognizer(ActorTapGestureRecognizer(actor: stakeholderSelected, onTap: {
                        (actor) -> Void in
                        
                        if let coordinator = actor.representative {
                            
                            let vcProfile = Storyboard.getInstanceOf(ProfileViewController_iPad.self)
                            vcProfile.member = coordinator
                            if self.isEmbedded == false
                            {
                                self.navigationController?.pushViewController(vcProfile, animated: true)
                            }
                            else
                            {
                                let navController = NavyController(rootViewController:vcProfile)
                                vcProfile.isModal = true
                                self.present(navController, animated: true, completion: nil)
                            }
                        }
                    }))
                }
                cell.onSeeRelationsTapped = {
                    
                    self.onSeeRelationsTapped?(stakeholderSelected)
                }
                cell.onSeeDetailTapped = {
                    
                    let vcStakeholderDetail = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
                    vcStakeholderDetail.stakeholder = stakeholderSelected
                    vcStakeholderDetail.isModal = true
                    let navController = NavyController(rootViewController: vcStakeholderDetail)
                    self.present(navController, animated: true, completion: nil)
                }
                cell.onCloseBtnTapped = {
                    
                    self.dismiss(animated: true, completion: nil)
                }
                if self.orientation == .portrait
                {
                    cell.lblUserRatingTitle.isHidden = false
                    cell.lblAdminRatingTitle.isHidden = false
                }
                else
                {
                    cell.lblUserRatingTitle.isHidden = true
                    cell.lblAdminRatingTitle.isHidden = true
                }
                
                return cell
            }
        }
        else
        {
            //Configure the cell for employee
            if let cell = tableView.dequeueReusableCell(withIdentifier: "RelationsReportTableViewCell_employee_iPad", for: indexPath) as? RelationsReportTableViewCell_employee_iPad {
                
                //Set background color for embedded or onMainscreen
                if self.isEmbedded == true
                {
                    cell.contentView.backgroundColor = UIColor.white
                    cell.btnSeeDetail.setTitle("Profile Detail".localized(), for: UIControlState())
                }
                else
                {
                    cell.btnSeeRelations.isHidden = true
                    cell.btnSeeDetail.isHidden = true
                    cell.btnClose.isHidden = true
                }
                if self.isRelationReport == false
                {
                   cell.btnSeeRelations.isHidden = true
                }
                var stakeholderSelected = self.stakeholders[indexPath.row]
                cell.loadCellWith(stakeholderSelected)
                cell.onSeeRelationsTapped = {
                    
                    self.onSeeRelationsTapped?(stakeholderSelected)
                }
                cell.onSeeDetailTapped = {
                    
                    let member = Member(json: nil)
                    member.id = stakeholderSelected.accountId!
                    let vcProfile = Storyboard.getInstanceOf(ProfileViewController_iPad.self)
                    vcProfile.member = member
                    vcProfile.isModal = true
                    let navController = NavyController(rootViewController: vcProfile)
                    self.present(navController, animated: true, completion: nil)
                }
                cell.onCloseBtnTapped = {
                    
                    self.dismiss(animated: true, completion: nil)
                }
                
                return cell
            }
        }
        
        return RelationsReportTableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return tableView.estimatedRowHeight
    }
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {        
        // Remove seperator inset
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset))
        {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))
        {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.responds(to: #selector(setter: UIView.layoutMargins))
        {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }

}
