//
//  EmbeddedRelationsViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 2/3/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class EmbeddedRelationsViewController: UIViewController
{
    //MARK: PROPERTIES AND OUTLETS
    var relationsContactTable: RelationsReportTableViewController!
    var arrayStakeholders: [Stakeholder]!
    var currentOrbitLevel: Int!
    var onSeeRelationsTapped: ((_ stakeholder:Stakeholder) -> ())?
    var onSeeDetailTapped: ((_ stakeholder:Stakeholder) -> ())?
    var isRelationReport = true
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var containerTable: UIView!
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.backButtonArrow()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        UIView.animate(withDuration: 1, animations: { () -> Void in
            
            self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        }) 
        self.navigationController?.navigationBar.isHidden = true
        self.backButtonArrow()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            
            self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        }) 
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    //MARK: LOAD CONFIG
    func loadConfig()
    {
        //UI
        self.btnClose.layer.cornerRadius = 3
        self.btnClose.setTitle("Close".localized(), for: UIControlState())
        self.btnClose.clipsToBounds = true
        self.containerTable.layer.cornerRadius = 3
        self.containerTable.clipsToBounds = true
    }
    
    //MARK: ACTIONS
    @IBAction func closeAction(_ sender: AnyObject)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "RelationsReportTableViewController"
        {
            self.relationsContactTable = segue.destination as! RelationsReportTableViewController
            relationsContactTable.isRelationReport = self.isRelationReport
           
            if let stakeholders = self.arrayStakeholders, stakeholders.count > 0 {
                
                
                self.relationsContactTable.isEmbedded = true
                self.relationsContactTable.currentOrbitLevel = self.currentOrbitLevel
                self.relationsContactTable.stakeholders = stakeholders
                self.relationsContactTable.onSeeRelationsTapped = { (stakeholder) in
                    
                    self.onSeeRelationsTapped?(stakeholder)
                }
                
                self.relationsContactTable.onSeeDetailTapped = { (stakeholder) in
                    
                    self.onSeeDetailTapped?(stakeholder)
                }
            }
        }
    }
}
