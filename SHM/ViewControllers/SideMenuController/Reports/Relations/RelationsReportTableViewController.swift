//
//  RelationsReportTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 2/1/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class RelationsReportTableViewController: UITableViewController
{
    //MARK: PROPERTIES AND OUTLETS
    var stakeholders: [Stakeholder] = [Stakeholder](){
        didSet{
            if let orbitLevel = self.currentOrbitLevel {
                
                let offset = self.arrayNumberOfSatellitesInLevel[orbitLevel] - 1
                guard offset <= self.stakeholders.count else {return}
                self.stakeholders.removeFirst(offset)
                self.tableView.reloadData()
            }
        }
    }
    var arrayNumberOfSatellitesInLevel = [1,5,7,9,12]
    var currentOrbitLevel: Int!
    var isEmbedded = false
    var isRelationReport = true
    
    //Action closures
    var onSeeRelationsTapped:       ((_ stakeholder:Stakeholder) -> ())?
    var onSeeDetailTapped:          ((_ stakeholder:Stakeholder) -> ())?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "RelationsReportTableViewCell", bundle: nil), forCellReuseIdentifier: "RelationsReportTableViewCell")
        tableView.register(UINib(nibName: "RelationsReportTableViewCell_employee", bundle: nil), forCellReuseIdentifier: "RelationsReportTableViewCell_employee")
        
        if self.stakeholders.count > 1
        {
            self.tableView.separatorStyle = .singleLine
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: TABLE VIEW DATA SOURCE
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.stakeholders.count;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard   self.stakeholders.count > 0 else {

            return RelationsReportTableViewCell()
        }
        
        //Identify if employee or stakeholder
        if self.stakeholders[indexPath.row].isStakeholder == true
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "RelationsReportTableViewCell", for: indexPath) as? RelationsReportTableViewCell {

                //Type of report
                cell.isRelationReport = self.isRelationReport
                cell.hideBtnSeeRelations()
                if self.isEmbedded == true
                {
                    cell.contentView.backgroundColor = UIColor.white
                }

                var stakeholderSelected = self.stakeholders[indexPath.row]
                cell.loadCellWith(stakeholderSelected)
                
                //Coordinator on tap action
                if let coordinator = stakeholderSelected.coordinator?.fullName, coordinator != "" {
                    
                    cell.lblCoordinatorInfo.removeGestureRecognizersOfType(ActorTapGestureRecognizer.self)
                    cell.lblCoordinatorInfo.addGestureRecognizer(ActorTapGestureRecognizer(actor: stakeholderSelected, onTap: {
                        (actor) -> Void in
                        if let coordinator = actor.coordinator {
                            
                            let vcProfile = Storyboard.getInstanceOf(ProfileViewController.self)
                            vcProfile.member = coordinator
                            self.navigationController?.pushViewController(vcProfile, animated: true)
                        }
                    }))
                    
                    /* Inbox action is disabled for now */
                    cell.imgCoordinatorInbox.isHidden = true
                    cell.imgCoordinatorInbox.isUserInteractionEnabled = false
                    cell.imgCoordinatorInbox.removeGestureRecognizersOfType(ActorTapGestureRecognizer.self)
                    cell.imgCoordinatorInbox.addGestureRecognizer(ActorTapGestureRecognizer(actor: stakeholderSelected, onTap: {
                        (actor) -> Void in
                        
                        if let coordinator = actor.coordinator {
                            
                            let vcNewMessage = Storyboard.getInstanceOf(NewMessageTableViewController.self)
                            vcNewMessage.recipientsSelected = [coordinator]
                           
                            let navController = NavyController(rootViewController:vcNewMessage)
                            self.present(navController, animated: true, completion: nil)
                        }                        
                    }))
                }
               
                //Representative on tap action
                if let representative = stakeholderSelected.representative?.fullName, representative != "" {

                    cell.lblRepresentativeInfo.isUserInteractionEnabled = true
                    cell.lblRepresentativeInfo.removeGestureRecognizersOfType(ActorTapGestureRecognizer.self)
                    cell.lblRepresentativeInfo.addGestureRecognizer(ActorTapGestureRecognizer(actor: stakeholderSelected, onTap: {
                        (actor) -> Void in
                        
                        if let coordinator = actor.representative {
                            
                            let vcProfile = Storyboard.getInstanceOf(ProfileViewController.self)
                            vcProfile.member = coordinator
                            self.navigationController?.pushViewController(vcProfile, animated: true)
                        }
                    }))
                }
                cell.onSeeRelationsTapped = {
                    
                    self.onSeeRelationsTapped?(stakeholderSelected)
                }
                cell.onSeeDetailTapped = {
                    
                    let stakeholderDetail : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                    stakeholderDetail.stakeholder = stakeholderSelected
                    self.navigationController?.pushViewController(stakeholderDetail, animated: true)
                }
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                return cell
            }
        }
        else
        {
            //Configure the cell for employee
            if let cell = tableView.dequeueReusableCell(withIdentifier: "RelationsReportTableViewCell_employee", for: indexPath) as? RelationsReportTableViewCell_employee {
                
                //Type of Report
                cell.isRelationReport = self.isRelationReport
                cell.hideSeeRelationsBtn()
                if self.isEmbedded == true
                {
                    cell.contentView.backgroundColor = UIColor.white
                }
                
                var stakeholderSelected = self.stakeholders[indexPath.row]
                cell.loadCellWith(stakeholderSelected)
                cell.onSeeRelationsTapped = {
                    
                    self.onSeeRelationsTapped?(stakeholderSelected)
                }
                cell.onSeeDetailTapped = {
                    
                    let member = Member(json: nil)
                    member.id = stakeholderSelected.accountId!
                    let vcProfile = Storyboard.getInstanceOf(ProfileViewController.self)
                    vcProfile.member = member
                    self.navigationController?.pushViewController(vcProfile, animated: true)
                }
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                return cell
            }
        }
        
        return RelationsReportTableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if self.isRelationReport == true
        {
            return 265.0
        }
        else
        {
            return 230
        }
    }
}
