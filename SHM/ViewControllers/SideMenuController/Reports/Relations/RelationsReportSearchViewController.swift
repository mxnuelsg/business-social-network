//
//  RelationsReportSearchViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 2/4/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class RelationsReportSearchViewController: UIViewController
{
    //MARK: PROPERTIES AND OUTLETS
    @IBOutlet weak var searchBar: AutoSearchBar!
    @IBOutlet weak var containerSearchResults: UIView!
    var vcRelatedUsers :RelationsReportSearchTableViewController!

    //iPad's Image and welcome Message
    @IBOutlet weak var viewWelcome: UIView!
    @IBOutlet weak var imgWelcome: UIImageView!
    @IBOutlet weak var lblWelcome: UILabel!
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
        self.vcRelatedUsers.stakeholders.removeAll()
        self.searchBar.text = ""
        self.vcRelatedUsers.dismissBackgroundMessage()
        self.viewWelcome.isHidden = false
        
        if let stakeholderSelected = self.vcRelatedUsers.stakeholderSelected {
            
            self.vcRelatedUsers.stakeholders = [stakeholderSelected]
            self.searchBar.isTranslucent = false
            self.searchBar.searchBarStyle = UISearchBarStyle.minimal
            self.searchBar.backgroundColor = UIColor.lightGray
            self.searchBar.isUserInteractionEnabled = false
            self.viewWelcome.isHidden = true
            self.vcRelatedUsers.isTableBeingReloaded = true
            self.vcRelatedUsers.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: LOAD CONFIGS
    fileprivate func loadConfig()
    {

        //initialize AutoSearchBar
        searchBar.initialize(shouldSearchAfterDelay: true,
        onSearchText: { [weak self] (text) in
            self?.getRelatedUsers(text)
            
        }, onClearText: { [weak self] in
            self?.cancelSearch()
                
        }, onCancel: { [weak self] in
            self?.cancelSearch()
            
        })
        
        self.localize()
        
        //New post button
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        navigationItem.rightBarButtonItems = [barBtnCompose]
        
        self.viewWelcome.isHidden = true
    }
    
    fileprivate func localize()
    {
        //Navigation bar title
        navigationItem.title = "REPORTS-RELATIONS".localized()
        
        //Search bar welcome message
        self.searchBar.placeholder = "RelationsReportSearchBar".localized()
        self.lblWelcome.text = "RelationsReportWelcomeMessage".localized()
    }
    
    fileprivate func cancelSearch() {
        self.vcRelatedUsers.tableView.dismissBackgroundMessage()
        self.vcRelatedUsers.stakeholders.removeAll()
        self.vcRelatedUsers.tableView.reloadData()
        self.viewWelcome.isHidden = false
    }
    
    //MARK: WEB SERVICE
    func getRelatedUsers(_ searchText: String)
    {
        self.viewWelcome.isHidden = true
       
        LibraryAPI.shared.reportsBO.getRelatedUsers(searchText, onSuccess: {
            (users) -> () in
            
            self.vcRelatedUsers.stakeholders = users
            self.vcRelatedUsers.tableView.dismissBackgroundMessage()
            self.vcRelatedUsers.tableView.reloadData()
            
            if users.count == 0
            {
                self.vcRelatedUsers.tableView.displayBackgroundMessage("No results found".localized(), subMessage: "")
            }
            
            }) { error in
         
                MessageManager.shared.showBar(title: "Error",
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
                print(error)
        }
    }
        
    //MARK: NAVIGATION BAR ACTIONS
    func createNewPost()
    {
        if DeviceType.IS_ANY_IPAD == true
        {
            let vcNewPost = NewPostModal()
            let navController = NavyController(rootViewController: vcNewPost)
            navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
            self.present(navController, animated: true, completion: nil)
        }
        else
        {
            let vcNewPost = NewPostViewController()
            let navController = NavyController(rootViewController: vcNewPost)
            navController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "RelationsReportSearchTableViewController"
        {
            self.vcRelatedUsers = segue.destination as! RelationsReportSearchTableViewController
            self.vcRelatedUsers.onNewSearch = {
                
                self.searchBar.isUserInteractionEnabled = true
                self.searchBar.searchBarStyle = UISearchBarStyle.default
                self.searchBar.backgroundColor = UIColor.clear
                self.viewWelcome.isHidden = false
            }
            self.vcRelatedUsers.onSelected = {
                
                self.searchBar.resignFirstResponder()
            }
        }
    }
}

