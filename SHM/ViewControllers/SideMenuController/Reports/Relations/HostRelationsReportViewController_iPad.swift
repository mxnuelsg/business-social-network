//
//  HostRelationsReportViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 2/11/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class HostRelationsReportViewController_iPad: UIViewController
{
    //MARK: PROPERTIES AND OUTLETS
    //Containers
    @IBOutlet weak var containerPortrait: UIView!
    @IBOutlet weak var containerLandscape: UIView!

    //Relation Graph Views
    var vcRelationsReport_Portrait:RelationsReportViewController_iPad!
    var vcRelationsReport_Landscape:RelationsReportViewController_iPad!
    
    //User selected from Initial search
    var userSelected:Stakeholder!
    
    //Relations report from web Service
    var report:Reports!
    
    var onStakeholderSelected:((_ stakeholderSelected:Stakeholder) -> ())?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig_iPad()
        guard let user = self.userSelected else {

            return
        }
        self.getRelationsReport(user)
    }

    override func viewWillDisappear(_ animated: Bool)
    {
        self.onStakeholderSelected?(self.userSelected)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: RESIZING FUNCTIONS
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        if UIDevice.current.orientation.isPortrait
        {
            self.containerLandscape.isHidden = true
            self.containerPortrait.isHidden = false
            self.vcRelationsReport_Portrait.relationsGraph.isViewBeingRedrawn = true
            self.vcRelationsReport_Portrait.relationsGraph.setNeedsDisplay()
            let delayTimeAfeterHide = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTimeAfeterHide) {
            
            self.vcRelationsReport_Portrait.tvContactInfo.orientation = UIDeviceOrientation.portrait
            self.vcRelationsReport_Portrait.tvContactInfo.tableView.reloadData()
            }
        }
        else
        {
            //Clean and Reorder satellites
            self.containerLandscape.isHidden = false
            self.containerPortrait.isHidden  = true
            self.vcRelationsReport_Landscape.relationsGraph.isViewBeingRedrawn = true
            self.vcRelationsReport_Landscape.relationsGraph.setNeedsDisplay()
            let delayTimeAfeterHide = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTimeAfeterHide) {
                self.vcRelationsReport_Landscape.tvContactInfo.orientation = UIDeviceOrientation.landscapeLeft
                self.vcRelationsReport_Landscape.tvContactInfo.tableView.reloadData()
            }
            
        }
    }
    
    //MARK: LOAD FUNCTIONS
    func loadConfig_iPad()
    {
        //Bar title
        navigationItem.title = "REPORTS-RELATIONS".localized()
        
        //New post button
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        let barBtnSearch = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(self.goToSearch))
        navigationItem.rightBarButtonItems = [barBtnCompose, barBtnSearch]
    }
    
    //MARK: WEB SERVICES
    func getRelationsReport(_ user:Stakeholder)
    {
        let wsObject: [String : Any] = [
            "AccountId": user.isStakeholder ? user.id : user.accountId!,
            "StakeholderId":user.isStakeholder ? user.id : 0,
            "IsStakeholder":user.isStakeholder ? "true":"false"]
        
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        LibraryAPI.shared.reportsBO.getReportRelations(wsObject, onSuccess:{
            (report) -> () in
            
            guard let contactReport = report else{
                
                return
            }
            
            //Fill user selected for return
            if self.vcRelationsReport_Landscape.relationsGraph.isInitialLoadFinished == false && self.vcRelationsReport_Portrait.relationsGraph.isInitialLoadFinished == false
            {
                self.userSelected.placeOfBirth = contactReport.placeOfBirth
                self.userSelected.country = contactReport.country
                self.userSelected.birthday = contactReport.birthday
                self.userSelected.age = contactReport.age
                self.userSelected.occupation = contactReport.occupation
                self.userSelected.nationalities = contactReport.nationalities
                self.userSelected.coordinator = contactReport.coordinator
                self.userSelected.representative = contactReport.representative
                self.userSelected.phone = contactReport.phone
                self.userSelected.email = contactReport.email
                self.userSelected.birthDateString = contactReport.birthDateString
            }

            self.report = contactReport
            self.vcRelationsReport_Landscape.cleanRelationsGraph()
            self.vcRelationsReport_Portrait.cleanRelationsGraph()
            self.vcRelationsReport_Portrait.loadRelationsGraph(self.report)
            self.vcRelationsReport_Landscape.loadRelationsGraph(self.report)
            MessageManager.shared.hideHUD()
            }){error in
                
                MessageManager.shared.hideHUD()
                MessageManager.shared.showBar(title: "Error", subtitle: "Error.TryAgain".localized(), type: .error, fromBottom: false)
                print(error)
        }
    }
    
    //MARK: FILTER FUNCTIONS
    func filterRelations(_ TypeOfRelation:Int)
    {
        switch TypeOfRelation
        {
        case 1: //Family
            if self.vcRelationsReport_Portrait.isBtnFamilyOn == true || self.vcRelationsReport_Landscape.isBtnFamilyOn == true
            {
                self.vcRelationsReport_Portrait.isBtnFamilyOn = false
                self.vcRelationsReport_Portrait.btnFamily.alpha = 0.5
                self.vcRelationsReport_Landscape.isBtnFamilyOn = false
                self.vcRelationsReport_Landscape.btnFamily.alpha = 0.5
            }
            else
            {
                self.vcRelationsReport_Portrait.isBtnFamilyOn = true
                self.vcRelationsReport_Portrait.btnFamily.alpha = 1.0
                self.vcRelationsReport_Landscape.isBtnFamilyOn = true
                self.vcRelationsReport_Landscape.btnFamily.alpha = 1.0
            }
        case 2: //Fiends
            if self.vcRelationsReport_Portrait.isBtnFriendsOn == true || self.vcRelationsReport_Landscape.isBtnFriendsOn == true
            {
                self.vcRelationsReport_Portrait.isBtnFriendsOn = false
                self.vcRelationsReport_Portrait.btnFriends.alpha = 0.5
                self.vcRelationsReport_Landscape.isBtnFriendsOn = false
                self.vcRelationsReport_Landscape.btnFriends.alpha = 0.5
            }
            else
            {
                self.vcRelationsReport_Portrait.isBtnFriendsOn = true
                self.vcRelationsReport_Portrait.btnFriends.alpha = 1.0
                self.vcRelationsReport_Landscape.isBtnFriendsOn = true
                self.vcRelationsReport_Landscape.btnFriends.alpha = 1.0
            }
        case 3: //Work
            if self.vcRelationsReport_Portrait.isBtnWorkOn == true || self.vcRelationsReport_Landscape.isBtnWorkOn == true
            {
                self.vcRelationsReport_Portrait.isBtnWorkOn = false
                self.vcRelationsReport_Portrait.btnWork.alpha = 0.5
                self.vcRelationsReport_Landscape.isBtnWorkOn = false
                self.vcRelationsReport_Landscape.btnWork.alpha = 0.5
            }
            else
            {
                self.vcRelationsReport_Portrait.isBtnWorkOn = true
                self.vcRelationsReport_Portrait.btnWork.alpha = 1.0
                self.vcRelationsReport_Landscape.isBtnWorkOn = true
                self.vcRelationsReport_Landscape.btnWork.alpha = 1.0
            }
        default:
            break
        }
        
        //Clean and Reorder satellites
        //This happens in the final resize method (when graphview is about to be shown)
        self.vcRelationsReport_Portrait.relationsGraph.subviews.forEach({ $0.removeFromSuperview() })
        self.vcRelationsReport_Portrait.relationsGraph.addSubview(self.vcRelationsReport_Portrait.centralProfileView)
        self.vcRelationsReport_Portrait.updateRelationsArrays()
        self.vcRelationsReport_Portrait.relationsGraph.placeSatellites()
        self.vcRelationsReport_Landscape.relationsGraph.subviews.forEach({ $0.removeFromSuperview() })
        self.vcRelationsReport_Landscape.relationsGraph.addSubview(self.vcRelationsReport_Landscape.centralProfileView)
        self.vcRelationsReport_Landscape.updateRelationsArrays()
        self.vcRelationsReport_Landscape.relationsGraph.placeSatellites()
        MessageManager.shared.hideHUD()
    }
    
    func updateFlagsStakeholderEmployee(_ userType:Int)
    {
        self.vcRelationsReport_Portrait.txtFieldStakeholdersEmployees.text = "    " + self.vcRelationsReport_Portrait.arrayTextField[userType]
        self.vcRelationsReport_Landscape.txtFieldStakeholdersEmployees.text = "    " + self.vcRelationsReport_Landscape.arrayTextField[userType]
        switch userType
        {
        case 0: //Show all types
            self.vcRelationsReport_Portrait.isShowingEmployees = true
            self.vcRelationsReport_Portrait.isShowingStakeholders = true
            self.vcRelationsReport_Landscape.isShowingEmployees = true
            self.vcRelationsReport_Landscape.isShowingStakeholders = true
        case 1: //Show Stakeholder types
            self.vcRelationsReport_Portrait.isShowingStakeholders = true
            self.vcRelationsReport_Portrait.isShowingEmployees = false
            self.vcRelationsReport_Landscape.isShowingStakeholders = true
            self.vcRelationsReport_Landscape.isShowingEmployees = false
        case 2://Show Employee types
            self.vcRelationsReport_Portrait.isShowingStakeholders = false
            self.vcRelationsReport_Portrait.isShowingEmployees = true
            self.vcRelationsReport_Landscape.isShowingStakeholders = false
            self.vcRelationsReport_Landscape.isShowingEmployees = true
        default:
            break
        }
    }
    
    func filterByStakeholderEmployee()
    {
        self.vcRelationsReport_Portrait.relationsGraph.subviews.forEach({ $0.removeFromSuperview() })
        self.vcRelationsReport_Portrait.relationsGraph.addSubview(self.vcRelationsReport_Portrait.centralProfileView)
        self.vcRelationsReport_Portrait.updateRelationsArrays()
        self.vcRelationsReport_Portrait.relationsGraph.placeSatellites()
        self.vcRelationsReport_Landscape.relationsGraph.subviews.forEach({ $0.removeFromSuperview() })
        self.vcRelationsReport_Landscape.relationsGraph.addSubview(self.vcRelationsReport_Landscape.centralProfileView)
        self.vcRelationsReport_Landscape.updateRelationsArrays()
        self.vcRelationsReport_Landscape.relationsGraph.placeSatellites()
    }
    
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "RelationsReportViewController_iPad_portrait"
        {
            self.vcRelationsReport_Portrait = segue.destination as! RelationsReportViewController_iPad
            self.vcRelationsReport_Portrait.userSelected = self.userSelected
            self.vcRelationsReport_Portrait.onSeeRelationsTapped = { (stakeholder) in
                
                //reset  filters
                self.vcRelationsReport_Landscape.resetRelationFilters()
                self.vcRelationsReport_Portrait.resetRelationFilters()                
                //Reset drawing flags
                self.vcRelationsReport_Landscape.relationsGraph.isViewBeingRedrawn = false
                self.vcRelationsReport_Portrait.relationsGraph.isViewBeingRedrawn = false
                //Reload Relations Graph in both modes
                self.getRelationsReport(stakeholder)
            }
            self.vcRelationsReport_Portrait.onFilterAction = {(filterTag) in
                
                self.filterRelations(filterTag)
            }
            self.vcRelationsReport_Portrait.onPickerViewSelected  = { (row) in
                
                self.updateFlagsStakeholderEmployee(row)
            }
            self.vcRelationsReport_Portrait.onTextFieldEditingDone = {
                
                self.filterByStakeholderEmployee()
            }
        }
        
        if segue.identifier == "RelationsReportViewController_iPad_landscape"
        {
            self.vcRelationsReport_Landscape = segue.destination as! RelationsReportViewController_iPad
            self.vcRelationsReport_Landscape.userSelected = self.userSelected
            self.vcRelationsReport_Landscape.onSeeRelationsTapped   = { (stakeholder) in
            
                //reset  filters
                self.vcRelationsReport_Landscape.resetRelationFilters()
                self.vcRelationsReport_Portrait.resetRelationFilters()
                
                //Reset drawing flags
                self.vcRelationsReport_Landscape.relationsGraph.isViewBeingRedrawn = false
                self.vcRelationsReport_Portrait.relationsGraph.isViewBeingRedrawn = false

                //Reload Relations Graph in both modes
                self.getRelationsReport(stakeholder)
            }
            self.vcRelationsReport_Landscape.onFilterAction = {(filterTag) in
                
                self.filterRelations(filterTag)
            }
            
            self.vcRelationsReport_Landscape.onPickerViewSelected = { (row) in
                
                self.updateFlagsStakeholderEmployee(row)
            }
            self.vcRelationsReport_Landscape.onTextFieldEditingDone = {
                
                self.filterByStakeholderEmployee()
            }
        }
        if UIDevice.current.orientation.isPortrait
        {
            self.containerLandscape.isHidden = true
            self.containerPortrait.isHidden = false
        }
        else
        {
            self.containerLandscape.isHidden = false
            self.containerPortrait.isHidden = true
        }
    }


    //MARK: NAVIGATION BAR ACTIONS
    func createNewPost()
    {
        let vcNewPost = NewPostModal()
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = DeviceType.IS_ANY_IPAD == true ?  UIModalPresentationStyle.formSheet :  UIModalPresentationStyle.overFullScreen
        present(navController, animated: true, completion: nil)
    }
    
    func goToSearch()
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
}
