//
//  RelationsReportViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 2/11/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


class RelationsReportViewController_iPad: UIViewController
{
    //MARK: OUTLETS AND PROPERTIES
    //Closures
    var onSeeRelationsTapped: ((_ stakeholder:Stakeholder) -> ())?
    var onFilterAction: ((_ buttonTag:Int) -> ())?
    var onPickerViewSelected: ((_ row:Int) -> ())?
    var onTextFieldEditingDone: (() -> ())?
    
    //For Random satellite location
    var arrayRandomOffset:[CGFloat] = [0.0, 0.0, 0.0, 0.0]

    //Filters
    @IBOutlet weak var btnWork: UIButton!
    @IBOutlet weak var btnFriends: UIButton!
    @IBOutlet weak var btnFamily: UIButton!
    @IBOutlet weak var txtFieldStakeholdersEmployees: TextfieldBlockedMenu!
    var arrayTextField = ["Show Stakeholders And Employess".localized(), "Show Stakeholders Only".localized(), "Show Employees Only".localized()]
    
    //Contact information and Relation Graph
    @IBOutlet weak var containerContactInfo: UIView!
    @IBOutlet weak var containerGraphRelations: UIView!
    @IBOutlet weak var relationsGraph: RelationsGraphView!
    var tvContactInfo: RelationsReportTableViewController_iPad!
    
    //Graph's properties
    var arrayNumberOfSatellitesInLevel: [Int]!
    var arrayOrbitRadiusCoeff: [CGFloat]!
    var centralProfileView: ProfileRoundView!
    
    //User selected from Initial search
    var userSelected:Stakeholder!
    
    //Relations array from web service
    var arrayRelations:[Stakeholder]!
    
    //Flags for filter relation types
    var isBtnFamilyOn = true
    var isBtnFriendsOn = true
    var isBtnWorkOn = true
    var isShowingStakeholders = true
    var isShowingEmployees = true
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig_iPad()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        self.backButtonArrow()
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: RESIZING FUNCTIONS
    func resizeRelationsGraph()
    {
        //Update relations Graph dimensions
        self.relationsGraph.width = self.relationsGraph.frame.width
        self.relationsGraph.height = self.relationsGraph.frame.height
        self.relationsGraph.centerPoint = self.relationsGraph.getSystemCenter()
        self.relationsGraph.radius = self.relationsGraph.getSystemRadius()
        
        //set new radius for orbits
        let newRadius = self.relationsGraph.radius
        self.relationsGraph.SystemOrbits[0].radius = newRadius! * self.arrayOrbitRadiusCoeff[1]
        self.relationsGraph.SystemOrbits[1].radius = newRadius! * self.arrayOrbitRadiusCoeff[2]
        self.relationsGraph.SystemOrbits[2].radius = newRadius! * self.arrayOrbitRadiusCoeff[3]
        self.relationsGraph.SystemOrbits[3].radius = newRadius! * self.arrayOrbitRadiusCoeff[4]
        
        //Update positions of satellites
        self.updateSatellitesPositionInOrbitLevel(1)
        self.updateSatellitesPositionInOrbitLevel(2)
        self.updateSatellitesPositionInOrbitLevel(3)
        self.updateSatellitesPositionInOrbitLevel(4)
        
        //Update according to filters
        self.relationsGraph.subviews.forEach({ $0.removeFromSuperview() })
        self.updateCentralProfile()
        self.updateRelationsArrays()
        self.relationsGraph.isViewBeingRedrawn = false
        self.relationsGraph.placeSatellites()
    }

    func updateCentralProfile()
    {
        self.centralProfileView.frame.origin.x = self.relationsGraph.centerPoint.x - self.centralProfileView.frame.width/2
        self.centralProfileView.frame.origin.y = self.relationsGraph.centerPoint.y - self.centralProfileView.frame.height/2
        self.relationsGraph.addSubview(self.centralProfileView)
    }
    
    func updateSatellitesPositionInOrbitLevel(_ level:Int)
    {
        let satelliteSize = CGSize(width: self.relationsGraph.getSatelliteRadius() * 2, height: self.relationsGraph.getSatelliteRadius() * 2)
        var newSatellitePositions = self.getSatellitePositionsInLevel(level)
        for satellite in 0...arrayNumberOfSatellitesInLevel[level] - 1
        {
            let satelliteView                = self.relationsGraph.SystemOrbits[level - 1].satelliteViews[satellite]
            satelliteView.frame.size.width   = satelliteSize.width
            satelliteView.frame.size.height  = satelliteSize.height
            satelliteView.frame.origin.x     = newSatellitePositions[satellite].x
            satelliteView.frame.origin.y     = newSatellitePositions[satellite].y
            satelliteView.layer.cornerRadius = self.relationsGraph.SystemOrbits[level - 1].satelliteViews[satellite].frame.size.width / 2
            
            //If a satellite with embedded profiles is found, create the appropiate indicator label
            if satelliteView.doesContainEmbeddedProfile == true
            {
                let label = UILabel(frame: satelliteView.frame.insetBy(dx: 5,dy: 5))
                label.layer.backgroundColor = UIColor.white.cgColor
                label.layer.masksToBounds = true
                label.textAlignment = NSTextAlignment.center
                label.text = String(satelliteView.getEmbeddedProfilesCount() - (self.relationsGraph.SystemOrbits[level - 1].maxNumberOfSatellites - 1))
                self.relationsGraph.addSubview(label)
            }
            
            //If the profile round view is empty stop the cycle
            if satelliteView.layer.contents == nil
            {
                break
            }
            
            //Add satellite view
            self.relationsGraph.addSubview(satelliteView)
            
            //The relations array may be updated during re-drawing (if device rotates)
            if satellite >= self.relationsGraph.SystemOrbits[level - 1].relations?.count
            {
                break
            }
            
            //Add ID Blue card if not stakeholder
            if let arrayOfRelations = self.relationsGraph.SystemOrbits[level - 1].relations, arrayOfRelations.count > satellite - 1 {
            
                if arrayOfRelations[satellite].isStakeholder == false
                {
                    let idCard = UIImageView(frame: CGRect(x: satelliteView.frame.origin.x - 7, y: satelliteView.frame.origin.y, width: 50, height: 30))
                    idCard.image = UIImage(named: "IconIDBlue")
                    self.relationsGraph.addSubview(idCard)
                }
            }
        }
    }
    
    //Place the ID card on employee/stakeholder profile
    func setIdCard(_ satellite:ProfileRoundView, isStakeholder:Bool)
    {
        if isStakeholder == false
        {
            let idCard = UIImageView(frame: CGRect(x: satellite.frame.origin.x-7,y: satellite.frame.origin.y,width: 100,height: 20))
            idCard.image = UIImage(named: "IconIDBlue")
            satellite.addSubview(idCard)
        }
    }
    
    //MARK: LOAD CONFIGS
    fileprivate func loadConfig_iPad()
    {
        self.localize()
        
        //Filter Controls
        self.btnFamily.layer.cornerRadius   = 3.0
        self.btnFriends.layer.cornerRadius  = 3.0
        self.btnWork.layer.cornerRadius     = 3.0
        
        self.txtFieldStakeholdersEmployees.setBorder()
        self.txtFieldStakeholdersEmployees.text =  "    " + self.arrayTextField[0]
        self.txtFieldStakeholdersEmployees.rightView = UIImageView(image: UIImage(named: "iconCombo"))
        self.txtFieldStakeholdersEmployees.rightViewMode = .always
        for offset in 0...arrayRandomOffset.count-1
        {
            self.arrayRandomOffset[offset] = CGFloat(arc4random_uniform(90))
        }
    }
    
    fileprivate func localize()
    {
        self.btnFamily.setTitle("FAMILY".localized(), for: UIControlState())
        self.btnFriends.setTitle("FRIENDS".localized(), for: UIControlState())
        self.btnWork.setTitle("WORK".localized(), for: UIControlState())
    }

    //MARK: LOAD RELATIONS REPORT VIEW
    func loadRelationsGraph(_ report:Reports)
    {
        /*Fill Current Contact Information (View's top space)*/
        self.tvContactInfo = RelationsReportTableViewController_iPad()
        self.tvContactInfo.tableView.backgroundColor = UIColor.grayRelationReport()
        if let contactInfo = report as Stakeholder? {
            
            self.tvContactInfo.stakeholders = [contactInfo]
        }
        
        self.tvContactInfo.tableView.hideEmtpyCells()
        self.tvContactInfo.tableView.isScrollEnabled = false
        self.containerContactInfo.addSubViewController(self.tvContactInfo, parentVC: self)
        
        /*Setup Relations Report Graph (View's bottom space)*/
        //Indicate the number of satellites in each orbit
        self.arrayNumberOfSatellitesInLevel = [1, 5, 7, 9, 12]
        self.relationsGraph.numberOfSatellitesInFirstOrbit = self.arrayNumberOfSatellitesInLevel[1]
        //Indicate each Orbit radius (the first value corresponds to the central profile)
        self.arrayOrbitRadiusCoeff = [0.13, 0.25, 0.50, 0.70, 0.90]
        
        //Create central profile round view
        let systemCenter        = self.relationsGraph.getSystemCenter()
        let centralRadius       = self.arrayOrbitRadiusCoeff[0] * self.relationsGraph.getSystemRadius()
        self.centralProfileView = ProfileRoundView(frame: CGRect(x: systemCenter.x - centralRadius,y: systemCenter.y - centralRadius,width: centralRadius * 2,height: centralRadius * 2))
       
        if report.thumbnailUrl.trim().isEmpty == false
        {
            self.centralProfileView.layer.contents = self.relationsGraph.getProfileImage(report.thumbnailUrl).cgImage
            self.centralProfileView.layer.masksToBounds = true
        }
        else
        {
            self.centralProfileView.layer.contents = UIImage(named: "defaultperson")?.cgImage
            self.centralProfileView.layer.masksToBounds = true
        }
        
        self.relationsGraph.addSubview(self.centralProfileView)

        //Construct orbits
        guard report.relationsReport.count > 0 else {
            
            return
        }
        self.arrayRelations = report.relationsReport
        //Filter the elements for each orbit
        let relationsInLevel1 = self.arrayRelations.filter({$0.levelId == 1})
        let relationsInLevel2 = self.arrayRelations.filter({$0.levelId == 2})
        let relationsInLevel3 = self.arrayRelations.filter({$0.levelId == 3})
        let relationsInLevel4 = self.arrayRelations.filter({$0.levelId == 4})
        let relationInLevel = [relationsInLevel1, relationsInLevel2, relationsInLevel3, relationsInLevel4]
        
        //Set up the dimensions
        self.relationsGraph.arrayOrbitRadiusCoeff = self.arrayOrbitRadiusCoeff
        self.relationsGraph.adjustCoeff = 0.80
        self.relationsGraph.width  = self.relationsGraph.frame.width
        self.relationsGraph.height = self.relationsGraph.frame.height
        self.relationsGraph.centerPoint = self.relationsGraph.getSystemCenter()
        self.relationsGraph.radius = self.relationsGraph.getSystemRadius()
        let mainRadius  = self.relationsGraph.getSystemRadius()

        //Construct Orbit data structs
        for orbitNumber in 1 ... 4
        {
            print(orbitNumber)
            let orbitLevel = RelationsGraphView.RelationOrbit(
                radius: mainRadius * self.arrayOrbitRadiusCoeff[orbitNumber],
                relations: relationInLevel[orbitNumber - 1],
                maxNumberOfSatellites: self.arrayNumberOfSatellitesInLevel[orbitNumber],
                satelliteViews: self.getSatelliteViews(orbitNumber),
                level: orbitNumber,
                angleDiff: self.relationsGraph.angleDifferencialWith(CGFloat(self.arrayNumberOfSatellitesInLevel[orbitNumber])))
            self.relationsGraph.SystemOrbits += [orbitLevel]
        }
        
        //Resizing due rotation
        self.relationsGraph.onDrawRect = {
            
            self.resizeRelationsGraph()
        }
        
        /*  The current relationsGraph view was loaded empty (if first time)
        or is not updated (if new contact has been selected). It is necessary
        to reload the view with the previously created set ups
        */
        self.relationsGraph.isInitialLoadFinished = true
        //self.containerGraphRelations.addSubview(self.relationsGraph)
        self.relationsGraph.setNeedsDisplay()
    }
    
    //MARK: WEB SERVICES
    func getRelationsReport(_ user:Stakeholder)
    {
        let wsObject: [String : Any] = [
            "AccountId": user.isStakeholder ? user.id : user.accountId!,
            "StakeholderId":user.isStakeholder ? user.id : 0,
            "IsStakeholder":user.isStakeholder ? "true":"false"]
        
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        LibraryAPI.shared.reportsBO.getReportRelations(wsObject , onSuccess:{
            (report) -> () in
            
            guard let contactReport = report else {
                
                return
            }
            self.loadRelationsGraph(contactReport)
            MessageManager.shared.hideHUD()
            }){ error in
                
                MessageManager.shared.hideHUD()
                print(error)
        }
    }
    
    //MARK: PRIVATE FUNCTIONS
    func getSatelliteViews(_ level:Int) -> [ProfileRoundView]
    {
        //Obtain array of positions (CGPoints) of Satellites in current level
        var positions = self.getSatellitePositionsInLevel(level)
        //Get the size of each satellite view
        let satelliteSize = CGSize(width: self.relationsGraph.getSatelliteRadius()*2, height: self.relationsGraph.getSatelliteRadius() * 2)
        var satelliteViews = [ProfileRoundView]()
        for view in 0 ... positions.count - 1
        {
            let profileView = ProfileRoundView(frame:CGRect(x: positions[view].x, y: positions[view].y, width: satelliteSize.width, height: satelliteSize.height))
            profileView.onSatelliteProfileTapped = {
                (stakeholders) -> () in
                
                let tableVCStakeholder = Storyboard.getInstanceOf(RelationsReportTableViewController_iPad.self)                
                tableVCStakeholder.currentOrbitLevel = level
                tableVCStakeholder.stakeholders = stakeholders
                tableVCStakeholder.tableView.hideEmtpyCells()
                tableVCStakeholder.isEmbedded = true
                tableVCStakeholder.modalPresentationStyle = UIModalPresentationStyle.formSheet
                tableVCStakeholder.preferredContentSize = CGSize(width: 425, height: 250)
                tableVCStakeholder.onSeeRelationsTapped={ (stakeholder) in
                    
                    self.onSeeRelationsTapped?(stakeholder)
                    self.presentedViewController?.dismiss(animated: true, completion: nil)
                }                
                self.present(tableVCStakeholder, animated: true, completion: {tableVCStakeholder.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)})
            }
            
            satelliteViews += [profileView]
        }
        
        return satelliteViews
    }
    
    func getSatellitePositionsInLevel(_ level:Int) -> [CGPoint]
    {
        //Get base parameters
        let numberOfSatellites = CGFloat(self.arrayNumberOfSatellitesInLevel[level])
        let dø = self.relationsGraph.angleDifferencialWith(numberOfSatellites)

        //Create array of CGPoints for each orbit level
        var positions = [CGPoint]()
        for position in 0 ... self.arrayNumberOfSatellitesInLevel[level] - 1
        {
            var positionInLevel = self.relationsGraph.getPointAtCircunferenceOf(radius: self.relationsGraph.getSystemRadius()*self.arrayOrbitRadiusCoeff[level], WithCenter: self.relationsGraph.getSystemCenter(), AtAngle: (dø * CGFloat(position)) + arrayRandomOffset[level - 1])
            //fix position to draw square container
            positionInLevel.x -= self.relationsGraph.getSatelliteRadius()
            positionInLevel.y -= self.relationsGraph.getSatelliteRadius()
            positions += [positionInLevel]
        }
        
        return positions
    }

    func updateRelationsArrays()
    {
        guard let relations = self.arrayRelations, relations.count > 0 else {

            return
        }
        //Filter relations in Orbit 1
        var relationsInLevel1 = self.arrayRelations.filter({$0.levelId == 1})
        var relationsInLevel2 = self.arrayRelations.filter({$0.levelId == 2})
        var relationsInLevel3 = self.arrayRelations.filter({$0.levelId == 3})
        var relationsInLevel4 = self.arrayRelations.filter({$0.levelId == 4})
        if self.isBtnFamilyOn == false
        {
            relationsInLevel1 = relationsInLevel1.filter({$0.relationType1?.key != 1})
            relationsInLevel2 = relationsInLevel2.filter({$0.relationType1?.key != 1})
            relationsInLevel3 = relationsInLevel3.filter({$0.relationType1?.key != 1})
            relationsInLevel4 = relationsInLevel4.filter({$0.relationType1?.key != 1})
        }
        if self.isBtnFriendsOn == false
        {
            relationsInLevel1 = relationsInLevel1.filter({$0.relationType1?.key != 2})
            relationsInLevel2 = relationsInLevel2.filter({$0.relationType1?.key != 2})
            relationsInLevel3 = relationsInLevel3.filter({$0.relationType1?.key != 2})
            relationsInLevel4 = relationsInLevel4.filter({$0.relationType1?.key != 2})
        }
        if self.isBtnWorkOn == false
        {
            relationsInLevel1 = relationsInLevel1.filter({$0.relationType1?.key != 3})
            relationsInLevel2 = relationsInLevel2.filter({$0.relationType1?.key != 3})
            relationsInLevel3 = relationsInLevel3.filter({$0.relationType1?.key != 3})
            relationsInLevel4 = relationsInLevel4.filter({$0.relationType1?.key != 3})
        }
        if (self.isShowingStakeholders == true && self.isShowingEmployees == false)
        {
            relationsInLevel1 = relationsInLevel1.filter({$0.isStakeholder == true})
            relationsInLevel2 = relationsInLevel2.filter({$0.isStakeholder == true})
            relationsInLevel3 = relationsInLevel3.filter({$0.isStakeholder == true})
            relationsInLevel4 = relationsInLevel4.filter({$0.isStakeholder == true})
        }
        if (self.isShowingStakeholders == false && self.isShowingEmployees == true)
        {
            relationsInLevel1 = relationsInLevel1.filter({$0.isStakeholder == false})
            relationsInLevel2 = relationsInLevel2.filter({$0.isStakeholder == false})
            relationsInLevel3 = relationsInLevel3.filter({$0.isStakeholder == false})
            relationsInLevel4 = relationsInLevel4.filter({$0.isStakeholder == false})
        }
        self.relationsGraph.SystemOrbits[0].relations = relationsInLevel1
        self.relationsGraph.SystemOrbits[1].relations = relationsInLevel2
        self.relationsGraph.SystemOrbits[2].relations = relationsInLevel3
        self.relationsGraph.SystemOrbits[3].relations = relationsInLevel4
    }
    
    func resetRelationFilters()
    {
        self.txtFieldStakeholdersEmployees.text = "    " + self.arrayTextField[0]
        self.isShowingStakeholders = true
        self.isShowingEmployees = true
        self.isBtnFamilyOn = true
        self.btnFamily.alpha = 1.0
        self.isBtnFriendsOn = true
        self.btnFriends.alpha = 1.0
        self.isBtnWorkOn = true
        self.btnWork.alpha = 1.0
    }
    
    func cleanRelationsGraph()
    {
        if self.relationsGraph.isInitialLoadFinished == true
        {
            self.relationsGraph.subviews.forEach({ $0.removeFromSuperview() })
            self.relationsGraph.SystemOrbits.removeAll()
            self.arrayRelations.removeAll()
        }
    }

    //MARK: BUTTON ACTIONS
    @IBAction func filterRelations(_ sender: AnyObject)
    {
        self.onFilterAction?(sender.tag)
    }
}

// MARK: TEXTFIELD DELEGATE
extension RelationsReportViewController_iPad: UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        textField.inputAccessoryView = loadToolBar()
        let pickerRelationType = UIPickerView()
        pickerRelationType.dataSource = self
        pickerRelationType.delegate = self
        pickerRelationType.backgroundColor = UIColor.groupTableViewBackground
        pickerRelationType.showsSelectionIndicator = true
        pickerRelationType.isMultipleTouchEnabled = false
        pickerRelationType.isExclusiveTouch = true
        textField.inputView  = pickerRelationType
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.onTextFieldEditingDone?()        
    }
    
    // MARK: TOOLBAR KEYBOARD
    func loadToolBar() -> UIToolbar
    {
        let toolBarKeyboard = UIToolbar()
        toolBarKeyboard.barStyle = UIBarStyle.blackOpaque;
        toolBarKeyboard.barTintColor = UIColor.colorForNavigationController()
        
        let barBtnDone = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action:#selector(self.closeKeyboard))
        barBtnDone.tintColor = UIColor.white
        toolBarKeyboard.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil), barBtnDone]
        toolBarKeyboard.sizeToFit()
        
        return toolBarKeyboard
    }
    
    func closeKeyboard()
    {
        view.endEditing(true)
    }
}

// MARK: PICKERVIEW DATA SOURCE
extension RelationsReportViewController_iPad :UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return self.arrayTextField.count
    }
}

// MARK: PICKERVIEW DELEGATE
extension RelationsReportViewController_iPad :  UIPickerViewDelegate
{
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return self.arrayTextField[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        //Communicate changes to host
        self.onPickerViewSelected?(row)
    }
}

