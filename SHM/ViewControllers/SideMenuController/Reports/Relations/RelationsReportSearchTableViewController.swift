//
//  RelationsReportSearchTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 2/4/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class RelationsReportSearchTableViewController: StakeholdersTableViewController
{
    //MARK: PROPERTIES 
    var isTableBeingReloaded = false
    var onNewSearch:(() -> ())?
    var onSelected:(() -> ())?
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "MemberSearchTableViewCell", bundle: nil), forCellReuseIdentifier: "MemberSearchTableViewCell")
        self.tableView.register(UINib(nibName: "ConnectionCellStakeholderUITableViewCell", bundle: nil), forCellReuseIdentifier: "ConnectionCellStakeholderUITableViewCell")
        self.tableView.register(UINib(nibName: "ConnectionCellMemberUITableViewCell", bundle: nil), forCellReuseIdentifier: "ConnectionCellMemberUITableViewCell")
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: DATA SOURCE AND DELEGATE
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let stakeholder = sectionedStakeholders?[indexPath.section][indexPath.row] ?? stakeholders[indexPath.row]
        
        if self.isTableBeingReloaded == true
        {
            if stakeholder.isStakeholder == true
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ConnectionCellStakeholderUITableViewCell") as! ConnectionCellStakeholderUITableViewCell
                cell.loadStakeholderForRelationsReport(stakeholder)
                cell.onDelete = { (ConexionReport) in
                    
                    self.isTableBeingReloaded = false
                    self.stakeholders.removeAll()
                    self.reload()
                    self.dismissBackgroundMessage()
                    self.onNewSearch?()
                    
                }
                //cell.lblFullname.text = self.stakeholderSelected?.fullName
                
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ConnectionCellMemberUITableViewCell") as! ConnectionCellMemberUITableViewCell
                cell.loadMemberForRelationsReport(stakeholder)
                cell.onDelete = { (ConexionReport) in
                    
                    self.isTableBeingReloaded = false
                    self.stakeholders.removeAll()
                    self.reload()
                    self.dismissBackgroundMessage()
                    self.onNewSearch?()
                }
                
                return cell
            }
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MemberSearchTableViewCell") as! MemberSearchTableViewCell
            cell.loadCellWith(stakeholder)
            
            return cell
        }
        
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let userSelected =  stakeholders[indexPath.row]
        self.stakeholderSelected = userSelected
        self.onSelected?()
        if DeviceType.IS_ANY_IPAD == true
        {
            let relationsReportViewController = Storyboard.getInstanceOf(HostRelationsReportViewController_iPad.self)
            relationsReportViewController.userSelected = userSelected
            relationsReportViewController.onStakeholderSelected = { (stakeholderSelected) in
                
                self.stakeholderSelected = stakeholderSelected
            }
            self.navigationController?.pushViewController(relationsReportViewController, animated: true)
        }
        else
        {
            let relationsReportViewController = Storyboard.getInstanceOf(RelationsReportViewController.self)
            relationsReportViewController.userSelected = userSelected
            relationsReportViewController.onStakeholderSelected = { (stakeholderSelected) in
                
                self.stakeholderSelected = stakeholderSelected
            }
            relationsReportViewController.onDidGetReport = {[weak self] stakeholder in
                
                self?.stakeholders.forEach({ (SHItem) in
                    if SHItem.id == stakeholder.id
                    {
                        SHItem.adminRating = stakeholder.adminRating ?? 0
                        SHItem.userRating = stakeholder.userRating ?? 0
                    }
                })
            }
            self.navigationController?.pushViewController(relationsReportViewController, animated: true)
        }
        
        
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return false
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if self.isTableBeingReloaded == true
        {
            return 177
        }
        else
        {
            return 70
        }
    }
}
