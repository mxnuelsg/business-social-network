//
//  RelationsReportViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 1/27/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class RelationsReportViewController: UIViewController
{
    //MARK: PROPERTIES AND OUTLETS
    //Graphic Relations Report
    @IBOutlet weak var relationsGraph: RelationsGraphView!
    var arrayNumberOfSatellitesInLevel: [Int]!
    var arrayOrbitRadiusCoeff: [CGFloat]!
    
    //Contact Information in UI
    @IBOutlet weak var containerContactInfo: UIView!
    var tvContactInfo: RelationsReportTableViewController!
    var centralProfileView: ProfileRoundView!
    
    //User selected from Initial search
    var userSelected:Stakeholder!
    
    //Relations array from web service
    var arrayRelations:[Stakeholder]!
    var onDidGetReport:((_ stakeholder: Stakeholder)->())?
    var onStakeholderSelected:((_ stakeholderSelected:Stakeholder) -> ())?
    
    //Filter controls
    @IBOutlet weak var btnFamily:UIButton!
    @IBOutlet weak var btnFriends:UIButton!
    @IBOutlet weak var btnWork:UIButton!
    @IBOutlet weak var txtFieldStakeholdersEmployees:UITextField!
    var arrayTextField = ["Show Stakeholders And Employess".localized(), "Show Stakeholders Only".localized(), "Show Employees Only".localized()]
    
    //Boolean Flags for filter relation types
    var isBtnFamilyOn = true
    var isBtnFriendsOn = true
    var isBtnWorkOn = true
    var isShowingStakeholders = true
    var isShowingEmployees = true
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        guard let user = self.userSelected else {

            return
        }
        self.getRelationsReport(user)
    }


    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        print("flag")
        self.onStakeholderSelected?(self.userSelected)
    }
    
    //MARK: LOAD UI CONFIGS
    fileprivate func loadConfig()
    {
        self.localize()
        
        //Appearance of filter buttons
        self.btnFamily.layer.cornerRadius = 3.0
        self.btnFriends.layer.cornerRadius = 3.0
        self.btnWork.layer.cornerRadius = 3.0
        
        self.txtFieldStakeholdersEmployees.setBorder()
        self.txtFieldStakeholdersEmployees.text          =  "    " + self.arrayTextField[0]
        self.txtFieldStakeholdersEmployees.rightView     = UIImageView(image: UIImage(named: "iconCombo"))
        self.txtFieldStakeholdersEmployees.rightViewMode = .always
        
        
        
        //New post button
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        navigationItem.rightBarButtonItems = [barBtnCompose]
    }
    
    fileprivate func localize()
    {
        self.btnFamily.setTitle("FAMILY".localized(), for: UIControlState())
        self.btnFriends.setTitle("FRIENDS".localized(), for: UIControlState())
        self.btnWork.setTitle("WORK".localized(), for: UIControlState())
        
        //Navigation bar title
        navigationItem.title = "REPORTS-RELATIONS".localized()
    }
    
    //MARK: LOAD RELATIONS REPORT GRAPH VIEW
    func loadRelationsGraph(_ report:Reports)
    {
        //CONTACT INFORMATION  (View's top space)
        self.tvContactInfo = RelationsReportTableViewController()
        if let contactInfo = report as Stakeholder? {
            
            self.tvContactInfo.stakeholders = [contactInfo]
        }
        self.tvContactInfo.tableView.hideEmtpyCells()
        self.tvContactInfo.tableView.isScrollEnabled = false
        self.containerContactInfo.addSubViewController(self.tvContactInfo, parentVC: self)
        
        
        //RELATIONS GRAPH     (View's bottom space)
        //Number of satellites and radius for each orbit (the first value corresponds to the central profile)
        self.arrayNumberOfSatellitesInLevel = [1, 5, 7, 9, 12]
        self.relationsGraph.numberOfSatellitesInFirstOrbit = self.arrayNumberOfSatellitesInLevel[1]
        self.arrayOrbitRadiusCoeff = [0.13, 0.25, 0.50, 0.70, 0.90]
        
        //Central profile
        let systemCenter = self.relationsGraph.getSystemCenter()
        let centralRadius = self.arrayOrbitRadiusCoeff[0] * self.relationsGraph.getSystemRadius()
        self.centralProfileView = ProfileRoundView(frame: CGRect(x: systemCenter.x - centralRadius,y: systemCenter.y - centralRadius,width: centralRadius * 2, height: centralRadius * 2))

        if report.thumbnailUrl.trim().isEmpty == false
        {
            
            self.centralProfileView.layer.contents = self.relationsGraph.getProfileImage(report.thumbnailUrl).cgImage
        }
        else
        {
            self.centralProfileView.layer.contents = UIImage(named: "defaultperson")?.cgImage
        }
        
        self.centralProfileView.layer.masksToBounds = true
        self.relationsGraph.addSubview(self.centralProfileView)
        
        ////Filter relations for each orbit
        guard report.relationsReport.count > 0 else {
            
            return
        }
        self.arrayRelations = report.relationsReport
        let relationsInLevel1 = self.arrayRelations.filter({$0.levelId == 1})
        let relationsInLevel2 = self.arrayRelations.filter({$0.levelId == 2})
        let relationsInLevel3 = self.arrayRelations.filter({$0.levelId == 3})
        let relationsInLevel4 = self.arrayRelations.filter({$0.levelId == 4})
        let relationInLevel = [relationsInLevel1,relationsInLevel2,relationsInLevel3,relationsInLevel4]

        //RelationsGraphView's Current dimensions
        let mainRadius = self.relationsGraph.getSystemRadius()
        self.relationsGraph.arrayOrbitRadiusCoeff = self.arrayOrbitRadiusCoeff
        self.relationsGraph.width = self.relationsGraph.frame.width
        self.relationsGraph.height = self.relationsGraph.frame.height
        self.relationsGraph.centerPoint = self.relationsGraph.getSystemCenter()
        self.relationsGraph.radius = self.relationsGraph.getSystemRadius()
        self.relationsGraph.adjustCoeff = 0.85
        
        //Satellites construction
        for orbitNumber in 1 ... 4
        {
            let orbitLevel = RelationsGraphView.RelationOrbit(radius: mainRadius * self.arrayOrbitRadiusCoeff[orbitNumber],
                relations: relationInLevel[orbitNumber - 1],
                maxNumberOfSatellites:  self.arrayNumberOfSatellitesInLevel[orbitNumber],
                satelliteViews: self.getSatelliteViews(orbitNumber),
                level: orbitNumber,
                angleDiff: self.relationsGraph.angleDifferencialWith(CGFloat(self.arrayNumberOfSatellitesInLevel[orbitNumber])))
            
            self.relationsGraph.SystemOrbits += [orbitLevel]
        }
        
        /*  The current relationsGraph view was loaded empty (if first time)
            or is not updated (if new contact has been selected). It is necessary
            to reload the view with the previously created set ups
        */
        self.relationsGraph.isInitialLoadFinished = true
        self.relationsGraph.setNeedsDisplay()
        
    }
    
    //MARK: WEB SERVICES
    func getRelationsReport(_ user:Stakeholder)
    {
        let wsObject: [String : Any] = [
            "AccountId" : user.isStakeholder ? user.id : user.accountId!,
            "StakeholderId" : user.isStakeholder ? user.id : 0,
            "IsStakeholder" : user.isStakeholder ? "true" : "false"]
        
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        LibraryAPI.shared.reportsBO.getReportRelations(wsObject, onSuccess:{
            (report) -> () in
            
            guard let contactReport = report else {
                
                return
            }
            self.onDidGetReport?(report ?? Stakeholder(isDefault: true))
            //Fill user selected for return
            if self.relationsGraph.isInitialLoadFinished == false
            {
                self.userSelected.placeOfBirth = contactReport.placeOfBirth
                self.userSelected.country = contactReport.country
                self.userSelected.birthday = contactReport.birthday
                self.userSelected.age = contactReport.age
                self.userSelected.occupation = contactReport.occupation
                self.userSelected.nationalities = contactReport.nationalities
                self.userSelected.coordinator = contactReport.coordinator
                self.userSelected.representative = contactReport.representative
                self.userSelected.phone = contactReport.phone
                self.userSelected.email = contactReport.email
                self.userSelected.birthDateString = contactReport.birthDateString
            }
            
            self.cleanRelationsGraph()
            self.loadRelationsGraph(contactReport)
            
            MessageManager.shared.hideHUD()
            
            }){error in
                
                //Add message for error and return to
                MessageManager.shared.hideHUD()
               
                MessageManager.shared.showBar(title: "Error",
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
        }
    }
    
    //MARK: PRIVATE FUNCTIONS
    func getSatelliteViews(_ level:Int)->[ProfileRoundView]
    {
        //Get positions and size of Satellites in current level
        let positions = self.getSatellitePositionsInLevel(level)
        let satelliteRadius = self.relationsGraph.getSatelliteRadius()
        let satelliteSize = CGSize(width: satelliteRadius * 2, height: satelliteRadius * 2)
        var satelliteViews = [ProfileRoundView]()
        for position in positions
        {
            let profileView = ProfileRoundView(frame:CGRect(x: position.x, y: position.y, width: satelliteSize.width, height: satelliteSize.height))
            profileView.onSatelliteProfileTapped={
                (stakeholders) -> () in
                
                let tableVCStakeholder = Storyboard.getInstanceOf(EmbeddedRelationsViewController.self)
                tableVCStakeholder.arrayStakeholders = stakeholders
                tableVCStakeholder.currentOrbitLevel = level
                tableVCStakeholder.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                tableVCStakeholder.onSeeRelationsTapped={ (stakeholder) in

                    //reset  filters
                    self.resetRelationFilters()
                    //Reload graph
                    self.getRelationsReport(stakeholder)
                    self.presentedViewController?.dismiss(animated: true, completion: nil)
                }
                let navController = NavyController(rootViewController:tableVCStakeholder)
                navController.modalPresentationStyle = .overCurrentContext
                self.present(navController, animated: true, completion: {tableVCStakeholder.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)})
            }
            satelliteViews += [profileView]
        }
        return satelliteViews
    }
    
    func getSatellitePositionsInLevel(_ level:Int) -> [CGPoint]
    {
        //Get base parameters
        let numberOfSatellites   = CGFloat(self.arrayNumberOfSatellitesInLevel[level])
        let angularDifferencial  = self.relationsGraph.angleDifferencialWith(numberOfSatellites)
        let randomOffset:CGFloat = CGFloat(arc4random_uniform(100))

        //Create array of CGPoints for each orbit level
        var positions = [CGPoint]()
        for position in 0 ... self.arrayNumberOfSatellitesInLevel[level] - 1
        {
            var positionInLevel = self.relationsGraph.getPointAtCircunferenceOf(radius: self.relationsGraph.getSystemRadius() * self.arrayOrbitRadiusCoeff[level],
                WithCenter: self.relationsGraph.getSystemCenter(),
                AtAngle: (angularDifferencial * CGFloat(position)) + randomOffset)
            
            //fix position to draw square container
            positionInLevel.x -= self.relationsGraph.getSatelliteRadius()
            positionInLevel.y -= self.relationsGraph.getSatelliteRadius()
            positions += [positionInLevel]
        }
        
        return positions
    }
    

    
    func updateRelationsArrays()
    {
        guard let relations = self.arrayRelations, relations.count > 0 else {

            return
        }
        
        //Filter relations array
        var relationsInLevel1 = self.arrayRelations.filter({$0.levelId == 1})
        var relationsInLevel2 = self.arrayRelations.filter({$0.levelId == 2})
        var relationsInLevel3 = self.arrayRelations.filter({$0.levelId == 3})
        var relationsInLevel4 = self.arrayRelations.filter({$0.levelId == 4})
        
        if self.isBtnFamilyOn == false
        {
            relationsInLevel1 = relationsInLevel1.filter({$0.relationType1?.key != 1})
            relationsInLevel2 = relationsInLevel2.filter({$0.relationType1?.key != 1})
            relationsInLevel3 = relationsInLevel3.filter({$0.relationType1?.key != 1})
            relationsInLevel4 = relationsInLevel4.filter({$0.relationType1?.key != 1})
        }
        if self.isBtnFriendsOn == false
        {
            relationsInLevel1 = relationsInLevel1.filter({$0.relationType1?.key != 2})
            relationsInLevel2 = relationsInLevel2.filter({$0.relationType1?.key != 2})
            relationsInLevel3 = relationsInLevel3.filter({$0.relationType1?.key != 2})
            relationsInLevel4 = relationsInLevel4.filter({$0.relationType1?.key != 2})
        }
        if self.isBtnWorkOn == false
        {
            relationsInLevel1 = relationsInLevel1.filter({$0.relationType1?.key != 3})
            relationsInLevel2 = relationsInLevel2.filter({$0.relationType1?.key != 3})
            relationsInLevel3 = relationsInLevel3.filter({$0.relationType1?.key != 3})
            relationsInLevel4 = relationsInLevel4.filter({$0.relationType1?.key != 3})
        }
        if (self.isShowingStakeholders == true && self.isShowingEmployees == false)
        {
            relationsInLevel1 = relationsInLevel1.filter({$0.isStakeholder == true})
            relationsInLevel2 = relationsInLevel2.filter({$0.isStakeholder == true})
            relationsInLevel3 = relationsInLevel3.filter({$0.isStakeholder == true})
            relationsInLevel4 = relationsInLevel4.filter({$0.isStakeholder == true})
        }
        if (self.isShowingStakeholders == false && self.isShowingEmployees == true)
        {
            relationsInLevel1 = relationsInLevel1.filter({$0.isStakeholder == false})
            relationsInLevel2 = relationsInLevel2.filter({$0.isStakeholder == false})
            relationsInLevel3 = relationsInLevel3.filter({$0.isStakeholder == false})
            relationsInLevel4 = relationsInLevel4.filter({$0.isStakeholder == false})
        }
        self.relationsGraph.SystemOrbits[0].relations = relationsInLevel1
        self.relationsGraph.SystemOrbits[1].relations = relationsInLevel2
        self.relationsGraph.SystemOrbits[2].relations = relationsInLevel3
        self.relationsGraph.SystemOrbits[3].relations = relationsInLevel4
    }
    

    func resetRelationFilters()
    {
        self.txtFieldStakeholdersEmployees.text = "    " + self.arrayTextField[0]
        self.isShowingStakeholders = true
        self.isShowingEmployees = true
        self.isBtnFamilyOn = true
        self.btnFamily.alpha = 1.0
        self.isBtnFriendsOn = true
        self.btnFriends.alpha = 1.0
        self.isBtnWorkOn = true
        self.btnWork.alpha = 1.0     
    }
    
    func cleanRelationsGraph()
    {
        if self.relationsGraph.isInitialLoadFinished == true
        {
            self.relationsGraph.subviews.forEach({ $0.removeFromSuperview() })
            self.relationsGraph.SystemOrbits.removeAll()
            self.arrayRelations.removeAll()
        }
    }
    
    //MARK: FILTER BUTTONS ACTIONS
    @IBAction func filterRelations(_ sender: AnyObject)
    {
        switch sender.tag
        {
        case 1: //Family
            if self.isBtnFamilyOn == true
            {
                self.isBtnFamilyOn = false
                self.btnFamily.alpha = 0.5
            }
            else
            {
                self.isBtnFamilyOn = true
                self.btnFamily.alpha = 1.0
            }
        case 2: //Fiends
            if self.isBtnFriendsOn == true
            {
                self.isBtnFriendsOn = false
                self.btnFriends.alpha = 0.5
            }
            else
            {
                self.isBtnFriendsOn = true
                self.btnFriends.alpha = 1.0
            }
            
        case 3: //Work
            if self.isBtnWorkOn == true
            {
                self.isBtnWorkOn = false
                self.btnWork.alpha = 0.5
            }
            else
            {
                self.isBtnWorkOn = true
                self.btnWork.alpha = 1.0
            }
        default:
            break
        }
        self.relationsGraph.subviews.forEach({ $0.removeFromSuperview() })
        self.relationsGraph.addSubview(self.centralProfileView)
        self.updateRelationsArrays()
        self.relationsGraph.placeSatellites()
        MessageManager.shared.hideHUD()
    }

    //MARK: NAVIGATION BAR ACTIONS
    func createNewPost()
    {
        if DeviceType.IS_ANY_IPAD == true
        {
            let vcNewPost = NewPostModal()
            let navController = NavyController(rootViewController: vcNewPost)
            navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
            self.present(navController, animated: true, completion: nil)
        }
        else
        {
            let vcNewPost = NewPostViewController()
            let navController = NavyController(rootViewController: vcNewPost)
            navController.modalPresentationStyle = .overFullScreen
            self.present(navController, animated: true, completion: nil)
        }
    }
}

// MARK: TEXTFIELD DELEGATE

extension RelationsReportViewController: UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        textField.inputAccessoryView            = loadToolBar()
        let pickerRelationType                  = UIPickerView()
        pickerRelationType.dataSource           = self
        pickerRelationType.delegate             = self
        pickerRelationType.backgroundColor      = UIColor.groupTableViewBackground
        pickerRelationType.showsSelectionIndicator = true
        pickerRelationType.isMultipleTouchEnabled = false
        pickerRelationType.isExclusiveTouch       = true
        textField.inputView                     = pickerRelationType
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.relationsGraph.subviews.forEach({ $0.removeFromSuperview() })
        self.relationsGraph.addSubview(self.centralProfileView)
        self.updateRelationsArrays()
        self.relationsGraph.placeSatellites()
    }
    
    // MARK: TOOLBAR KEYBOARD
    func loadToolBar() -> UIToolbar
    {
        let toolBarKeyboard               = UIToolbar()
        toolBarKeyboard.barStyle          = UIBarStyle.blackOpaque;
        toolBarKeyboard.barTintColor      = UIColor.colorForNavigationController()
        toolBarKeyboard.sizeToFit()
      
        let barBtnDone = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(self.closeKeyboard))
        
        barBtnDone.tintColor = UIColor.white
        toolBarKeyboard.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            barBtnDone]
        
        return toolBarKeyboard
    }
    
    func closeKeyboard()
    {
        view.endEditing(true)
    }
}

// MARK: PICKERVIEW DATA SOURCE
extension RelationsReportViewController :UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return self.arrayTextField.count
    }
}

// MARK: PICKERVIEW DELEGATE
extension RelationsReportViewController :  UIPickerViewDelegate
{
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return self.arrayTextField[row]
    }
        
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        self.txtFieldStakeholdersEmployees.text = "    " + self.arrayTextField[row]
        switch row
        {
        case 0://Showing employees and stakeholder
            self.isShowingEmployees         = true
            self.isShowingStakeholders      = true
        case 1://Showing stakeholders only
            self.isShowingStakeholders      = true
            self.isShowingEmployees         = false
        case 2://Showing employees only
            self.isShowingStakeholders      = false
            self.isShowingEmployees         = true
        default:
            break
        }
    }
}
