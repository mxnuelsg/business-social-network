//
//  MyPostsReportViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 1/27/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

enum MyPostsReportTime : Int
{
    case last30Days = 1
    case last60Days = 2
    case lastQuarter = 3
    case lastYear = 4
    case mtd = 5
    case ytd = 6
    case all = 7
}

class MyPostsReportViewController: UIViewController
{
    //MARK: PROPERTIES
    @IBOutlet weak var scrollMyPost: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var txtTimeRange: UITextField!
    @IBOutlet weak var txtDateRangeFrom: UITextField!
    @IBOutlet weak var txtDateRangeTo: UITextField!
    @IBOutlet weak var txtPrivacy: UITextField!
    @IBOutlet weak var txtProjects: UITextField!
    @IBOutlet weak var txtStakeholders: UITextField!
    @IBOutlet weak var txtGeographies: UITextField!
    
    @IBOutlet weak var lblDateRange_iPad: UILabel!
    @IBOutlet weak var tabBarCreateReport: UITabBar!
    @IBOutlet weak var tabBarBottomConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var lblSelectDateRange: UILabel!
    @IBOutlet weak var lblFilterBy: UILabel!
    
    @IBOutlet weak var lblFilterBy_iPad: UILabel!
    @IBOutlet weak var tabBarItemCreateReport: UITabBarItem!
   
    @IBOutlet weak var tabBarItemCreateReport_iPad: UITabBarItem!
    
    var report: UserPostsReport = UserPostsReport()
    var arrayStakeholders: [KeyValueObject] = []
    var arrayProjects:[KeyValueObject] = []
    var arrayGeographies:[KeyValueObject] = []
    var filtersReady: (() -> ())?
    
    // MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.loadDefault()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews()
    {
        if DeviceType.IS_IPHONE_4_OR_LESS == true
        {
            self.scrollMyPost.contentSizeToFit()
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    // MARK: LOAD CONFIGS
    fileprivate func loadConfig()
    {
        //Localizations
        self.localize()
        
        let imgMargin = UIImage(named: "iconMarginLeft")
        let imgArrow = UIImage(named: "iconCombo")
        let imgCalendar = UIImage(named:"CalendarReports")
        
        // Setting uiTextfields appers
        self.txtDateRangeFrom.setBorder()
        self.txtDateRangeFrom.leftView = UIImageView(image: imgMargin)
        self.txtDateRangeFrom.leftViewMode = UITextFieldViewMode.always
        self.txtDateRangeFrom.rightView = UIImageView(image: imgCalendar)
        self.txtDateRangeFrom.rightViewMode = UITextFieldViewMode.always
        self.txtDateRangeFrom.layer.cornerRadius = 3
        self.txtDateRangeFrom.textColor = UIColor.blackAsfalto()
        
        self.txtDateRangeTo.setBorder()
        self.txtDateRangeTo.leftView = UIImageView(image: imgMargin)
        self.txtDateRangeTo.leftViewMode = UITextFieldViewMode.always
        self.txtDateRangeTo.rightView = UIImageView(image: imgCalendar)
        self.txtDateRangeTo.rightViewMode = UITextFieldViewMode.always
        self.txtDateRangeTo.layer.cornerRadius = 3
        self.txtDateRangeTo.textColor = UIColor.blackAsfalto()
        
        self.txtPrivacy.setBorder()
        self.txtPrivacy.leftView = UIImageView(image: imgMargin)
        self.txtPrivacy.leftViewMode = UITextFieldViewMode.always
        self.txtPrivacy.rightView = UIImageView(image: imgArrow)
        self.txtPrivacy.rightViewMode = UITextFieldViewMode.always
        self.txtPrivacy.layer.cornerRadius = 3
        self.txtPrivacy.textColor = UIColor.blackAsfalto()
        
        self.txtProjects.setBorder()
        self.txtProjects.leftView = UIImageView(image: imgMargin)
        self.txtProjects.leftViewMode = UITextFieldViewMode.always
        self.txtProjects.rightView = UIImageView(image: imgArrow)
        self.txtProjects.rightViewMode = UITextFieldViewMode.always
        self.txtProjects.layer.cornerRadius = 3
        self.txtProjects.textColor = UIColor.blackAsfalto()
        
        self.txtStakeholders.setBorder()
        self.txtStakeholders.leftView = UIImageView(image: imgMargin)
        self.txtStakeholders.leftViewMode = UITextFieldViewMode.always
        self.txtStakeholders.rightView = UIImageView(image: imgArrow)
        self.txtStakeholders.rightViewMode = UITextFieldViewMode.always
        self.txtStakeholders.layer.cornerRadius = 3
        self.txtStakeholders.textColor = UIColor.blackAsfalto()
        
        self.txtTimeRange.setBorder()
        self.txtTimeRange.leftView = UIImageView(image: imgMargin)
        self.txtTimeRange.leftViewMode = UITextFieldViewMode.always
        self.txtTimeRange.rightView = UIImageView(image:imgArrow)
        self.txtTimeRange.rightViewMode = UITextFieldViewMode.always
        self.txtTimeRange.layer.cornerRadius = 3
        self.txtTimeRange.textColor = UIColor.blackAsfalto()
        
        self.txtGeographies.setBorder()
        self.txtGeographies.leftView = UIImageView(image: imgMargin)
        self.txtGeographies.leftViewMode = UITextFieldViewMode.always
        self.txtGeographies.rightView = UIImageView(image: imgArrow)
        self.txtGeographies.rightViewMode = UITextFieldViewMode.always
        self.txtGeographies.layer.cornerRadius = 3
        self.txtGeographies.textColor = UIColor.blackAsfalto()
        self.txtGeographies.tag = 6
        
        //setting view name
        self.setupNavigation(animated: false)
    }
    
    fileprivate func localize()
    {
        self.title = "REPORTS-MY POSTS".localized()
        self.tabBarController?.tabBarItem.title = "REPORTS-MY POSTS".localized()
        
        if DeviceType.IS_ANY_IPAD == false
        {
            self.lblSelectDateRange.text = "Or select Date Range:".localized()
            self.lblFilterBy.text = "Filter by".localized()
            self.tabBarItemCreateReport.title = "CREATE REPORT".localized()
        }
        else
        {
            self.lblDateRange_iPad.text = "Or select Date Range:".localized()
            self.lblFilterBy_iPad.text = "Filter by".localized()
            self.tabBarItemCreateReport_iPad.title = "CREATE REPORT".localized()
        }
        self.txtStakeholders.text = "All Stakeholders".localized()        
        self.txtProjects.text = "All Projects".localized()
        self.txtPrivacy.text = "Public and Private Posts".localized()
    }
    
    func setupNavigation(animated: Bool)
    {
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        self.navigationItem.setRightBarButtonItems([barBtnCompose], animated: animated)
    }
    
    // MARK: ACTIONS
    func createNewPost()
    {
        let vcNewPost = NewPostViewController()
        
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = .overFullScreen
        
        self.present(navController, animated: true, completion: nil)
    }
    
    func dateValue(_ sender: UIDatePicker)
    {
        let stringDateHuman = self.report.getFormatDateString(sender.date)
        
        if sender.tag == 1
        {
            self.txtDateRangeFrom.text = stringDateHuman
            self.report.from = sender.date
        }
        else
        {
            self.txtDateRangeTo.text = stringDateHuman
            self.report.to = sender.date
        }
    }
    
    // MARK: LOAD DEFAULTS
    func loadDefault()
    {
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        let params = DeviceType.IS_ANY_IPAD == true ?  ["request":"True"]: ["request":"False"]
        LibraryAPI.shared.reportsBO.getReportPostFilters(params, onSuccess: { (report) -> ()
            in
            
            MessageManager.shared.hideHUD()
            // Privacy
            if let responsePrivacys = report?.privacys {
                
                self.report.privacys = responsePrivacys
                self.report.privacy = responsePrivacys[0]
                self.txtPrivacy.text = responsePrivacys[0].value
            }
            
            // TimeRanges
            if let responseTimes = report?.timeRanges {
                
                self.report.timeRanges = responseTimes
                self.report.timeRange = responseTimes[0]
                self.txtTimeRange.text = responseTimes[0].value
            }
            
            // Project
            if let responseProjects = report?.projects {
                
                self.report.projects = responseProjects
                self.report.selectedProjects.append(responseProjects[0])
                self.txtProjects.text = responseProjects[0].title
                self.arrayProjects = self.report.getKeyValueProject()
            }
            else
            {
                self.arrayProjects = [KeyValueObject]()
            }
            
            //Geographies
            if let responseGeography = report?.geographies {
                
                self.report.geographies = responseGeography
                self.report.selectedGeographies.append(responseGeography[0])
                self.txtGeographies.text = responseGeography[0].title
                self.arrayGeographies = self.report.getKeyValueForGeography()
            }
            else
            {
                self.arrayGeographies = [KeyValueObject]()
            }
            
            // Stakeholders
            if let responseStakeholders = report?.stakeholders {
                self.report.stakeholders = responseStakeholders
                self.report.selectedStakeholders.append(responseStakeholders[0])
                self.txtStakeholders.text = responseStakeholders[0].fullName
                self.arrayStakeholders = self.report.getKeyValueStakeholder()
            }
            else
            {
                self.arrayStakeholders = [KeyValueObject]()
            }
            
            self.txtDateRangeFrom.text = report?.getFormatDateString(report!.from)
            self.report.from = report!.from
            self.txtDateRangeTo.text = report?.getFormatDateString(report!.to)
            self.report.to = report!.to
            
            self.filtersReady?()
            
            }) { (error) -> ()
                in
                MessageManager.shared.hideHUD()
                _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    // MARK: INITIAL CONFIG
    func loadToolBar() -> UIToolbar
    {
        let toolBarKeyboard = UIToolbar()
        toolBarKeyboard.barStyle = UIBarStyle.blackOpaque;
        toolBarKeyboard.barTintColor = UIColor.colorForNavigationController()
        
        let barBtnDone = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(self.closeKeyboard))
        barBtnDone.tintColor = UIColor.white
        
        toolBarKeyboard.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            barBtnDone]
        
        toolBarKeyboard.sizeToFit()
        
        return toolBarKeyboard
    }
    
    func closeKeyboard()
    {
        view.endEditing(true)
    }
}

// MARK: EXTENSIONS TAB BAR
extension MyPostsReportViewController: UITabBarDelegate
{
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        //Selected item color equal not selected color
        tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        
        switch item.tag
        {
        case 0:
            let vcReportDetail = Storyboard.getInstanceOf(MyPostsReportDetailViewController.self)
            vcReportDetail.filters = self.report
            self.navigationController?.pushViewController(vcReportDetail, animated: true)
            
        default: break
        }
    }
}

extension MyPostsReportViewController: UITextFieldDelegate
{
    // MARK: TEXTFIELD DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        textField.inputAccessoryView = loadToolBar()
        
        switch textField.tag
        {
        case 0:
            // Date range
            let vcTimeRanges = ListCountries(list: report.timeRanges)
            vcTimeRanges.isMultiselect = false
            
            vcTimeRanges.onSelectedCountry = {
                (timeRange) in
                var date = Date()
                
                switch timeRange.key
                {
                case MyPostsReportTime.last30Days.rawValue:

                    date = date.dateByAddingDays(-30)
                    
                case MyPostsReportTime.last60Days.rawValue:

                    date = date.dateByAddingDays(-60)
                    
                case MyPostsReportTime.lastQuarter.rawValue:

                    date = date.dateByAddingMonths(-4)
                    
                case MyPostsReportTime.lastYear.rawValue:

                    date = date.dateByAddingDays(-365)
                    
                case MyPostsReportTime.mtd.rawValue:
                    
                    let calendar = Calendar.current
                    var calendarComponents = (calendar as NSCalendar).components([.year, .month, .day], from: date)
                    calendarComponents.day = 1
                    date = calendar.date(from: calendarComponents)!
                    
                case MyPostsReportTime.ytd.rawValue:
                    
                    let calendar = Calendar.current
                    var calendarComponents = (calendar as NSCalendar).components([.year, .month, .day], from: date)
                    calendarComponents.month = 1
                    calendarComponents.day = 1
                    date = calendar.date(from: calendarComponents)!
                    
                case MyPostsReportTime.all.rawValue:
                    
                    // Date is 1/1/2015 because the app doesnt exist before that time
                    let calendar = Calendar.current
                    var calendarComponents = (calendar as NSCalendar).components([.year, .month, .day], from: date)
                    calendarComponents.month = 1
                    calendarComponents.day = 1
                    calendarComponents.year = 2015
                    date = calendar.date(from: calendarComponents)!
                    
                default:
                    break
                }
                
                self.report.from = date
                self.txtDateRangeFrom.text = self.report.getFormatDateString(date)
                self.txtTimeRange.text = timeRange.value
                
                textField.resignFirstResponder()
            }
            
            textField.inputView = vcTimeRanges.view
        case 1:
            let pickerDateTime = UIDatePicker()
            pickerDateTime.datePickerMode = .date
            pickerDateTime.tag = 1
            pickerDateTime.backgroundColor = UIColor.groupTableViewBackground
            pickerDateTime.addTarget(self, action: #selector(self.dateValue(_:)), for: .valueChanged)
            pickerDateTime.date = Date()
            pickerDateTime.isMultipleTouchEnabled = false
            pickerDateTime.isExclusiveTouch = true
            pickerDateTime.maximumDate = self.report.to
            textField.inputView = pickerDateTime
        case 2:
            let pickerDateTime = UIDatePicker()
            pickerDateTime.datePickerMode = .date
            pickerDateTime.tag = 2
            pickerDateTime.backgroundColor = UIColor.groupTableViewBackground
            pickerDateTime.addTarget(self, action: #selector(self.dateValue(_:)), for: .valueChanged)
            pickerDateTime.date = Date()
            pickerDateTime.isMultipleTouchEnabled = false
            pickerDateTime.isExclusiveTouch = true
            pickerDateTime.minimumDate = self.report.from
            textField.inputView = pickerDateTime
        case 3:
            // All stakeholders
            let vcStakeholder = ListCountries(list: self.arrayStakeholders)
            vcStakeholder.isMultiselect = true
            vcStakeholder.onSelectedCountries = {
                (selectedStakeholders) in
                
                var finalText = ""
                self.report.selectedStakeholders = []
                
                for (index,txt) in selectedStakeholders.enumerated()
                {
                    let stakeholder = Stakeholder(json: nil)
                    stakeholder.id = txt.key
                    
                    if txt.key == 0
                    {
                        self.report.selectedStakeholders = []
                        self.report.selectedStakeholders.append(stakeholder)
                        finalText = txt.value
                        self.closeKeyboard()
                        break
                    }
                    
                    if selectedStakeholders.count > 1
                    {
                        if index == 0
                        {
                            finalText.append("\(txt.value)")
                        }
                        else
                        {
                            finalText.append(", \(txt.value)")
                        }
                    }
                    else
                    {
                        finalText.append("\(txt.value)")
                    }
                    
                    self.report.selectedStakeholders.append(stakeholder)
                }
                
                textField.text = finalText
            }
            
            textField.inputView = vcStakeholder.view
            
        case 4:
            // All projects
            let vcProject = ListCountries(list: self.arrayProjects)
            vcProject.isMultiselect = true
            vcProject.onSelectedCountries = {
                (selectedProjects) in
                var finalText = ""
                self.report.selectedProjects = []
                
                for (index,txt) in selectedProjects.enumerated()
                {
                    let project = Project(json: nil)
                    project.id = txt.key
                    
                    if txt.key == 0 {
                        self.report.selectedProjects = []
                        self.report.selectedProjects.append(project)
                        finalText = txt.value
                        self.closeKeyboard()
                        break
                    }
                    
                    if selectedProjects.count > 1
                    {
                        if index == 0
                        {
                            finalText.append("\(txt.value)")
                        }
                        else
                        {
                            finalText.append(", \(txt.value)")
                        }
                    }
                    else
                    {
                        finalText.append("\(txt.value)")
                    }
                    
                    self.report.selectedProjects.append(project)
                }
                
                textField.text = finalText
            }
            
            textField.inputView = vcProject.view
        case 5:
            // Privacy
            let vcPrivacy = ListCountries(list: report.privacys)
            vcPrivacy.isMultiselect = false
            vcPrivacy.onSelectedCountry = {
                (privacy) in
                self.txtPrivacy.text = privacy.value
                self.report.privacy = privacy
            }
            
            textField.inputView = vcPrivacy.view
        case 6:
            // Geography
            let vcGeography = ListCountries(list: self.arrayGeographies)
            vcGeography.isMultiselect = true
            vcGeography.onSelectedCountries = {
                (selectedGeographies) in
                var finalText = ""
                self.report.selectedGeographies = []
                
                for (index,txt) in selectedGeographies.enumerated()
                {
                    let geography = ClusterItem(json: nil)
                    geography.id = txt.key
                    
                    if txt.key == 0 {
                        self.report.selectedGeographies = []
                        self.report.selectedGeographies.append(geography)
                        finalText = txt.value
                        self.closeKeyboard()
                        break
                    }
                    
                    if selectedGeographies.count > 1
                    {
                        if index == 0
                        {
                            finalText.append("\(txt.value)")
                        }
                        else
                        {
                            finalText.append(", \(txt.value)")
                        }
                    }
                    else
                    {
                        finalText.append("\(txt.value)")
                    }
                    
                    self.report.selectedGeographies.append(geography)
                }
                
                textField.text = finalText
            }
            
            textField.inputView = vcGeography.view
        default:
            break
        }
    }
}
