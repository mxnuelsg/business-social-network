//
//  MyPostsReportDetailViewController.swift
//  SHM
//
//  Created by Definity First on 2/2/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class MyPostsReportDetailViewController: UIViewController
{
    //MARK: OUTLETS & PROPERTIES
    @IBOutlet weak var lblPostsNumber: UILabel!
    @IBOutlet weak var lblDateRange: UILabel!
    @IBOutlet weak var labelMyResults: UILabel!
    @IBOutlet weak var lblPostByDate: UILabel!
    @IBOutlet weak var lblEmptyState: UILabel!
    
    var filters = UserPostsReport()
    var request = UserPostsReport()
    var vcPostsTable: PostsTableViewController!
    var verticalSpacingTop: CGFloat = 0.0
    
    // MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.loadDefault()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    deinit
    {
        print("Deinit: MyPostsReportDetailViewController")
    }
    
    // MARK: LOAD CONFIGS
    fileprivate func loadConfig()
    {
        self.localize()
        self.setupNavigation(animated: false)

        //Refresh control
        vcPostsTable.enableRefreshAction({ [weak self] in
            
            self?.loadData()
        })
    }
    
    fileprivate func localize()
    {
        self.title = "REPORTS-MY POSTS".localized()
        self.tabBarController?.tabBarItem.title = "REPORTS-MY POSTS".localized()
        self.lblPostByDate.text = "Post by dates".localized()
        self.lblEmptyState.text = "No data to display".localized()
        self.vcPostsTable.displayBackgroundMessage("Loading...".localized(), subMessage: "")
        
        if DeviceType.IS_ANY_IPAD == false
        {
            self.labelMyResults.text = "My Results".localized()
        }
    }
    
    func setupNavigation(animated: Bool)
    {
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        self.navigationItem.setRightBarButtonItems([barBtnCompose], animated: animated)
    }
    
    // MARK: LOAD DEFAULTS
    func loadDefault()
    {
        self.request = self.filters
        self.loadData()
    }

    // MARK: WEB SERVICE
    func loadData()
    {
        LibraryAPI.shared.reportsBO.getPostList(self.request.getWSObject(), onSuccess: { report in
            
            if let reportResponse = report
            {
                self.lblDateRange.text = self.filters.getFormatDateStringWithMonthName(self.request.from) + " - " + self.filters.getFormatDateStringWithMonthName(self.request.to)
                self.filters = reportResponse
                self.vcPostsTable.refreshed()
                self.vcPostsTable.posts = self.filters.posts
                self.vcPostsTable.reload()
                
                let post = self.filters.posts.count > 1 ? "posts of".localized() : "post of".localized()
                let strPostNumber = "\(self.filters.posts.count)" + " " + post + " " + "\(self.filters.posts.count)"
                
                let strAttPostNumber = NSMutableAttributedString(string: String("Showing".localized() + " " + strPostNumber))
                strAttPostNumber.setColor(strPostNumber, color: UIColor.blueImportantDate())
                strAttPostNumber.setBold(strPostNumber, size: 12)

                
                self.lblPostsNumber.attributedText = strAttPostNumber
                
                /*** MY POST GRAPHIC ***/
                //Populate Graph
                var graphData = [Double]()
                var graphLabels = [String]()
                
                for post in self.filters.chartPoints
                {
                    graphData.append(Double(post.key))
                    graphLabels.append(post.value)
                }
                
                //Remove previous Graph
                for subView in self.view.subviews where subView is ScrollableGraphView
                {
                    subView.removeFromSuperview()
                }
                
                //Verify Data to Show
                guard graphData.count > 0 && graphLabels.count > 0 else {
                
                    self.lblEmptyState.isHidden = false
                    return
                }
                
                //Remove Empty State
                self.lblEmptyState.isHidden = true

                //Create GRaph
                let graphView = self.createGraph(self.lblEmptyState.frame)
                graphView.pointSelectedDelegate = self
                graphView.set(data:graphData, withLabels: graphLabels)
                self.view.addSubview(graphView)
            }
            
            }) { error in
                
                self.vcPostsTable.refreshed()
                self.vcPostsTable.posts = []
                self.vcPostsTable.reload()
                
                //Remove previous Graph
                for subView in self.view.subviews where subView is ScrollableGraphView
                {
                    subView.removeFromSuperview()
                }
                
                //Empty State
                self.lblEmptyState.isHidden = false
                    
                MessageManager.shared.showBar(title: "Error",
                                              subtitle: "Error.TryAgain".localized(),
                                              type: .error,
                                              fromBottom: false)
        }
    }
    
    // MARK: ACTIONS
    func createNewPost()
    {
        let vcNewPost = NewPostViewController()
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = .overFullScreen
        
        self.present(navController, animated: true, completion: nil)
    }

    // MARK: PREPARE FOR SEAGUE
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        self.vcPostsTable = segue.destination as! PostsTableViewController
    }
}

//MARK: GRAPHVIEW DELEGATE
extension MyPostsReportDetailViewController: ScrollableGraphViewDelegate
{
    func pointWasSelectedAt(label: String, value: Double, location: CGPoint)
    {
        print("Point selected x:\(label) y:\(value) point:\(location)\n")
        
        let post = Int(value) > 1 ? "Posts".localized() : "OnePost".localized()
        MessageManager.shared.showBar(title: "Date".localized() + ":" + " " + label, subtitle: "\(Int(value)) " + post, type: .info, containsIcon: false, fromBottom: true)
    }
    
    fileprivate func createGraph(_ frame: CGRect) -> ScrollableGraphView
    {
        let graph = ScrollableGraphView(frame: frame)
        graph.backgroundFillColor = .white
        
        //Line
        graph.lineWidth = 1
        graph.lineColor = UIColor.colorFromHex(hexString: "#0065d7")
        graph.lineStyle = .smooth
        
        //Body Fill
        graph.shouldFill = true
        graph.fillType = .gradient
        graph.fillGradientType = .linear
        graph.fillGradientStartColor = UIColor.colorFromHex(hexString: "#bacce1").withAlphaComponent(0.8)
        graph.fillGradientEndColor = UIColor.colorFromHex(hexString: "#bacce1").withAlphaComponent(0.8)
        
        //Dots
        graph.dataPointSpacing = 80
        graph.dataPointSize = 5
        graph.dataPointFillColor = UIColor.colorFromHex(hexString: "#0065d7")
        
        //Left/Y Axis
        graph.referenceLineLabelFont = UIFont.boldSystemFont(ofSize: 9)
        graph.referenceLineLabelColor = .black
        
        //X Axis/ Bottom Labels
        graph.dataPointLabelColor = .black
        
        //Background lines
        graph.numberOfIntermediateReferenceLines = 2
        graph.referenceLineColor = UIColor.groupTableViewBackground.withAlphaComponent(0.7)
        
        //Animations
        graph.shouldAnimateOnStartup = true
        graph.shouldAdaptRange = true
        graph.adaptAnimationType = .elastic
        graph.animationDuration = 1.5
        graph.rangeMax = 50
        graph.shouldRangeAlwaysStartAtZero = true
        
        return graph
    }
}
