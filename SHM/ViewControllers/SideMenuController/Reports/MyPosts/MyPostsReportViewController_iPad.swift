//
//  MyPostsReportViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 2/21/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class MyPostsReportViewController_iPad: MyPostsReportDetailViewController
{
    //MARK: PROPERTIES AND OUTLETS
    @IBOutlet weak var containerResultLabels: UIView!
    @IBOutlet weak var lblDateRange_iPad: UILabel!
    @IBOutlet weak var lblMyResults: UILabel!
    @IBOutlet weak var lblPostsNumber_iPad: UILabel!
    @IBOutlet weak var btnCreateReport: UIButton!
    
    var vcMyPostsReportFilters: MyPostsReportViewController!
    var tvPostsTable_iPad: PostTableViewController_iPad!
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        self.loadConfig_iPad()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: nil, completion: { Void in
            
            // Your code here
            if UIDevice.current.orientation.isLandscape == true
            {
                print("Landscape")
                self.loadData_iPad()
                
            }
            else
            {
                print("Portrait")
                self.loadData_iPad()
            }

        })
    }
    
    deinit
    {
        print("deinit: MyPostsReportViewController_iPad")
    }
    
    //MARK: LOAD CONFIGS
    fileprivate func loadConfig_iPad()
    {
        //Set borders
        self.vcMyPostsReportFilters.setBorder()
        self.tvPostsTable_iPad.setBorder()
        self.containerResultLabels.setBorder()
        
        //Button Appearence
        self.btnCreateReport.layer.cornerRadius = 4.0
        
        //Setting navigation bar
        title = "REPORTS-MY POSTS".localized()
        self.tabBarController?.tabBarItem.title = "REPORTS-MY POSTS".localized()
//        self.setupNavigation(animated: false)

        //Hide bar btn from embedded iPhone version
        self.vcMyPostsReportFilters.tabBarCreateReport.isHidden = true
        self.vcMyPostsReportFilters.tabBarBottomConstrain.constant = 0        
        self.btnCreateReport.setTitle("CREATE REPORT".localized(), for: UIControlState())
        
        //Set result labels
        self.lblMyResults.text = "MY RESULTS".localized()
        lblPostByDate.text = "Post by dates".localized()
        
        //Refresh control
        self.tvPostsTable_iPad.enableRefreshAction({
            
            self.loadData_iPad()
        })
    }
    
    // MARK: WEB SERVICE
    func loadData_iPad()
    {
        LibraryAPI.shared.reportsBO.getPostList(self.request.getWSObject(), onSuccess: { report in
            
            if let reportResponse = report
            {
                MessageManager.shared.hideHUD()
                
                self.lblDateRange_iPad.text = self.filters.getFormatDateStringWithMonthName(self.request.from) + " - " + self.filters.getFormatDateStringWithMonthName(self.request.to)
                self.filters = reportResponse
                self.tvPostsTable_iPad.refreshed()
                self.tvPostsTable_iPad.posts = self.filters.posts
                self.tvPostsTable_iPad.reload()
                
                let post = self.filters.posts.count > 1 ? "posts of".localized() : "post of".localized()
                let strPostNumber = "\(self.filters.posts.count)" + " " + post + " " + "\(self.filters.posts.count)"
                
                let strAttPostNumber = NSMutableAttributedString(string: String("Showing".localized() + " " + strPostNumber))
                strAttPostNumber.setColor(strPostNumber, color: UIColor.blueImportantDate())
                strAttPostNumber.setBold(strPostNumber, size: 12)
                self.lblPostsNumber_iPad.attributedText = strAttPostNumber
                
                /*** MY POST GRAPHIC ***/
                //Populate Graph
                var graphData = [Double]()
                var graphLabels = [String]()
                
                for post in self.filters.chartPoints
                {
                    graphData.append(Double(post.key))
                    graphLabels.append(post.value)
                }
                
                //Remove previous Graph
                for subView in self.view.subviews where subView is ScrollableGraphView
                {
                    subView.removeFromSuperview()
                }
                
                //Verify Data to Show
                guard graphData.count > 0 && graphLabels.count > 0 else {
                    
                    self.lblEmptyState.isHidden = false
                    return
                }
                
                //Remove Empty State
                self.lblEmptyState.isHidden = true
                
                //Create GRaph
                let graphView = self.createGraph(self.lblEmptyState.frame)
                graphView.pointSelectedDelegate = self
                graphView.set(data:graphData, withLabels: graphLabels)
                self.view.addSubview(graphView)
            }
            
            }) { error in
                
                MessageManager.shared.hideHUD()
                self.tvPostsTable_iPad.refreshed()
                self.tvPostsTable_iPad.posts = []
                self.tvPostsTable_iPad.reload()
                
                //Remove previous Graph
                for subView in self.view.subviews where subView is ScrollableGraphView
                {
                    subView.removeFromSuperview()
                }
                
                //Empty State
                self.lblEmptyState.isHidden = false
                
                MessageManager.shared.showBar(title: "Error", subtitle: "Error.TryAgain".localized(), type: .error, fromBottom: false)
        }
    }

    // MARK: BUTTON ACTIONS
    @IBAction func createReport(_ sender: AnyObject)
    {
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        self.request = self.vcMyPostsReportFilters.report
        self.loadData_iPad()
    }
    
    override func createNewPost()
    {
        let vcNewPost = NewPostModal()
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
        present(navController, animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "MyPostsReportViewController"
        {
            self.vcMyPostsReportFilters = segue.destination as! MyPostsReportViewController
            self.vcMyPostsReportFilters.filtersReady = {
                
                self.request = self.vcMyPostsReportFilters.report
                self.loadData_iPad()
            }
        }
        if segue.identifier == "PostTableViewController_iPad"
        {
            self.tvPostsTable_iPad = segue.destination as! PostTableViewController_iPad
            self.tvPostsTable_iPad.posts = []
            self.tvPostsTable_iPad.reload()
        }
    }
    
    override func setupNavigation(animated: Bool)
    {
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(MyPostsReportDetailViewController.createNewPost))
        navigationItem.setRightBarButtonItems([barBtnCompose], animated: animated)
    }
}

//MARK: GRAPHVIEW DELEGATE
extension MyPostsReportViewController_iPad
{
    override func pointWasSelectedAt(label: String, value: Double, location: CGPoint)
    {
        print("Point selected x:\(label) y:\(value) point:\(location)\n")
        
        let post = Int(value) > 1 ? "Posts".localized() : "OnePost".localized()
        MessageManager.shared.showBar(title: "Date".localized() + ":" + " " + label, subtitle: "\(Int(value)) " + post, type: .info, containsIcon: false, fromBottom: true)
    }
    
    fileprivate func createGraph(_ frame: CGRect) -> ScrollableGraphView
    {
        let graph = ScrollableGraphView(frame: frame)
        graph.backgroundFillColor = .white
        
        //Line
        graph.lineWidth = 1
        graph.lineColor = UIColor.colorFromHex(hexString: "#0065d7")
        graph.lineStyle = .smooth
        
        //Body Fill
        graph.shouldFill = true
        graph.fillType = .gradient
        graph.fillGradientType = .linear
        graph.fillGradientStartColor = UIColor.colorFromHex(hexString: "#bacce1").withAlphaComponent(0.8)
        graph.fillGradientEndColor = UIColor.colorFromHex(hexString: "#bacce1").withAlphaComponent(0.8)
        
        //Dots
        graph.dataPointSpacing = 80
        graph.dataPointSize = 5
        graph.dataPointFillColor = UIColor.colorFromHex(hexString: "#0065d7")
        
        //Left/Y Axis
        graph.referenceLineLabelFont = UIFont.boldSystemFont(ofSize: 9)
        graph.referenceLineLabelColor = .black
        
        //X Axis/ Bottom Labels
        graph.dataPointLabelColor = .black
        
        //Background lines
        graph.numberOfIntermediateReferenceLines = 2
        graph.referenceLineColor = UIColor.groupTableViewBackground.withAlphaComponent(0.7)
        
        //Animations
        graph.shouldAnimateOnStartup = true
        graph.shouldAdaptRange = true
        graph.adaptAnimationType = .elastic
        graph.animationDuration = 1.5
        graph.rangeMax = 50
        graph.shouldRangeAlwaysStartAtZero = true
        
        return graph
    }
}



