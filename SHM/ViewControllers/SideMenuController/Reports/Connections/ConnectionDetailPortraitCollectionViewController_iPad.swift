//
//  ConnecctionDetailPortraitCollectionViewController_iPad.swift
//  SHM
//
//  Created by Definity First on 2/23/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class ConnectionDetailPortraitCollectionViewController_iPad: UIViewController
{
    //MARK: PROPERTIES
    var vcConnectionHeader: ConnectionHeaderCollectionViewController!
    var vcConexionDetail: ConexionDetailViewController!
    var arrayConexionHeader: [ConexionReport] = []
    var arrayConexionResult: [Node] = []
    var route: Int = 0
    var nodeWeight: Int = 0
    var totalNodes: Int = 0
    var isConnectionReport: Bool = true
    var showDeleteBtn: Bool = false
    var customTitle: String = ""
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = self.customTitle
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "ConnectionHeaderCollectionViewController"
        {
            self.vcConnectionHeader = segue.destination as! ConnectionHeaderCollectionViewController
            self.vcConnectionHeader.arrayMembers = self.arrayConexionHeader
            self.vcConnectionHeader.showDeleteBtn = self.showDeleteBtn
            self.vcConnectionHeader.isConnectionReport = self.isConnectionReport
        }
        
        if segue.identifier == "ConexionDetailViewController"
        {
            self.vcConexionDetail = segue.destination as! ConexionDetailViewController
            self.vcConexionDetail.arrayConexionHeader = self.arrayConexionHeader
            self.vcConexionDetail.arrayConexionResult = self.arrayConexionResult
            self.vcConexionDetail.totalNodes = self.totalNodes
            self.vcConexionDetail.nodeWeight = self.nodeWeight
            self.vcConexionDetail.route = self.route
            self.vcConexionDetail.isConnectionReport = self.isConnectionReport
        }
    }
}
