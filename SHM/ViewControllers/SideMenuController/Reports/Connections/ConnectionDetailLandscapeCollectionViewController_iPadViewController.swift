//
//  ConnecctionDetailLandscapeCollectionViewController_iPadViewController.swift
//  SHM
//
//  Created by Definity First on 2/23/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class ConnectionDetailLandscapeCollectionViewController_iPad: UIViewController
{
    //MARK: PROPERTIES
    var vcConnectionHeader: ConnectionHeaderCollectionViewController!
    var vcConnectionDetail: ConexionDetailViewController!
    var arrayConexionHeader: [ConexionReport] = []
    var arrayConexionResult: [Node] = []
    var route: Int = 0
    var nodeWeight: Int = 0
    var totalNodes: Int = 0
    var isConnectionReport: Bool = true
    var showDeleteBtn: Bool = false
    @IBOutlet weak var containerHeader: UIView!
    var customTitle: String = ""
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.containerHeader.backgroundColor = UIColor.grayTableBackground()
        self.title = self.customTitle
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "ConnectionHeaderCollectionViewController"
        {
            self.vcConnectionHeader = segue.destination as! ConnectionHeaderCollectionViewController
            self.vcConnectionHeader.arrayMembers = self.arrayConexionHeader
            self.vcConnectionHeader.showDeleteBtn = self.showDeleteBtn
        }
        
        if segue.identifier == "ConexionDetailViewController"
        {
            self.vcConnectionDetail = segue.destination as! ConexionDetailViewController
            self.vcConnectionDetail.arrayConexionHeader = self.arrayConexionHeader
            self.vcConnectionDetail.arrayConexionResult = self.arrayConexionResult
            self.vcConnectionDetail.totalNodes = self.totalNodes
            self.vcConnectionDetail.nodeWeight = self.nodeWeight
            self.vcConnectionDetail.route = self.route
            self.vcConnectionDetail.isConnectionReport = self.isConnectionReport
        }
    }
}
