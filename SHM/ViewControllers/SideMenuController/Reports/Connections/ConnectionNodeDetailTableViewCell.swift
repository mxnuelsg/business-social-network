//
//  ConexionNodeDetailTableViewCell.swift
//  SHM
//
//  Created by Definity First on 2/14/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import Foundation

class ConnectionNodeDetailTableViewCell: UITableViewCell
{
    //MARK: PROPERTIES
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vcArrow: ArrowView!
    @IBOutlet weak var constraintTrailing: NSLayoutConstraint!
    
    //MARK: LIFE CYCLE
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.loadConfig()
    }
    
    //MARK: LOAD CONFIG
    fileprivate func loadConfig()
    {
        self.lblTitle.setBorder()
    }
    
    //MARK: FUNCTIONS
    func setColor(_ relationType: String)
    {
        var color = UIColor()
        //family color
        if relationType.range(of: "amigo", options: NSString.CompareOptions.caseInsensitive, range: nil, locale: nil) != nil
            || relationType.range(of: "friend", options: NSString.CompareOptions.caseInsensitive, range: nil, locale: nil) != nil
        {
            color = UIColor.yellowFriends()
        }
        else if relationType.range(of: "famil", options: NSString.CompareOptions.caseInsensitive, range: nil, locale: nil) != nil
        {
            color = UIColor.greenFamily()
        }
        else if relationType.range(of: "trabajo", options: NSString.CompareOptions.caseInsensitive, range: nil, locale: nil) != nil || relationType.range(of: "work", options: NSString.CompareOptions.caseInsensitive, range: nil, locale: nil) != nil
        {
            color = UIColor.orangeWork()
        }
        
        self.lblTitle.textColor = color
        self.lblTitle.layer.borderColor = color.cgColor
        self.lblTitle.layer.borderWidth = 0.5
        self.lblTitle.text = relationType.uppercased()
        self.lblTitle.layer.cornerRadius = 5
        self.lblTitle.clipsToBounds = true

        self.vcArrow.arrowColor = color
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
}
