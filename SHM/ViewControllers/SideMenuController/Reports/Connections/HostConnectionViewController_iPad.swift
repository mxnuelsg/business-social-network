//
//  HostConnectionViewController.swift
//  SHM
//
//  Created by Definity First on 2/22/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class HostConnectionViewController_iPad: UIViewController
{
    //MARK: PROPERTIES
    @IBOutlet weak var containerLandscape: UIView!
    @IBOutlet weak var containerPortrait: UIView!
    var vcConnectionPortrait: ConectionResultsPortraitViewController_iPad!
    var vcConnectionLandscape: ConectionResultsLandscapeViewController_iPad!
    var arrayMembers: [ConexionReport] = []
    var arrayConexionResult: [Conexion] = []
    var widthLandscape:CGFloat = ScreenSize.SCREEN_HEIGHT / 2
    var widthPortrait:CGFloat = ScreenSize.SCREEN_WIDTH / 2
    var isConnectionReport: Bool = true
    var customTitle: String = ""
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator)
        self.changeRotation()
    }

    //MARK: LOAD CONFIG
    fileprivate func loadConfig()
    {
        self.backButtonArrow()
        //Bar title
        navigationItem.title = self.customTitle
        //New post button
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(HostConnectionViewController_iPad.createNewPost))
        navigationItem.rightBarButtonItems = [barBtnCompose]
    }
    
    // MARK: ACTIONS
    func createNewPost()
    {
        let vcNewPost = NewPostModal()
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
        
        self.present(navController, animated: true, completion: nil)
    }
    
    //MARK: FUNCTIONS
    func changeRotation()
    {
        if UIDevice.current.orientation.isPortrait
        {
            self.containerLandscape.isHidden = true
            self.containerPortrait.isHidden = false
        }
        else
        {
            self.containerLandscape.isHidden = false
            self.containerPortrait.isHidden = true
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) 
    {
        if segue.identifier == "ConectionResultsPortraitViewController_iPad"
        {
            self.vcConnectionPortrait = segue.destination as! ConectionResultsPortraitViewController_iPad
            self.vcConnectionPortrait.arrayMembers = self.arrayMembers
            self.vcConnectionPortrait.arrayConnectionResult = self.arrayConexionResult
            self.vcConnectionPortrait.showDeleteBtn = false
            self.vcConnectionPortrait.isConnectionReport = self.isConnectionReport
            self.vcConnectionPortrait.customTitle = self.customTitle
        }
        
        if segue.identifier == "ConectionResultsLandscapeViewController_iPad"
        {
            self.vcConnectionLandscape = segue.destination as! ConectionResultsLandscapeViewController_iPad
            self.vcConnectionLandscape.arrayMembers = self.arrayMembers
            self.vcConnectionLandscape.arrayConnectionResult = self.arrayConexionResult
            self.vcConnectionLandscape.showDeleteBtn = false
            self.vcConnectionLandscape.isConnectionReport = self.isConnectionReport
            self.vcConnectionLandscape.customTitle = self.customTitle
        }
        
       self.changeRotation()
    }
}
