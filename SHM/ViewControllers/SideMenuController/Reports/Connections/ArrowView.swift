//
//  ArrowView.swift
//  SHM
//
//  Created by Definity First on 2/16/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class ArrowView: UIView
{
    var arrowColor:UIColor?
    var isVertical:Bool = false
    var lineWidth: CGFloat = 1.0
    var arrowWidth: CGFloat = 0.02
    
    //For inner connections views
    var referenceWidth: CGFloat = 0.0
    var referenceHeight: CGFloat = 0.0
    var conexionNodes = [Node]()
    var hiddenConnections = 0
    var onHiddenConnectionsTap:((_ stakeholders:[Stakeholder])->())?
    override func setValue(_ value: Any?, forKeyPath keyPath: String)
    {
        if keyPath == "arrowColor"
        {
            if let color = value as? UIColor
            {
                
                self.arrowColor = color
            }
            else
            {
                self.arrowColor = UIColor.black
            }
        }
    }
    
    override func draw(_ rect: CGRect)
    {
        self.subviews.forEach({$0.removeFromSuperview()})
        let currentContext = UIGraphicsGetCurrentContext()
        guard let context = currentContext  else {
            
            return
        }

        //Global geometry
        if DeviceType.IS_ANY_IPAD == true
        {
            self.drawLineForIpad(context)
        }
        else
        {
            self.drawLineIphone(context)
        }
        self.drawConnectionNodes(context)
    }
    
    //MARK: DRAW CONNECTIONS NODES
    func drawConnectionNodes(_ context: CGContext)
    {
        //Safety check
        guard self.conexionNodes.count != 0 else {return}
        print("Number of connections: \(self.conexionNodes.count)")
        
        //Get the avaialable space
        let totalWidth = self.frame.width
        let arrowWidth = Int(self.arrowWidth * totalWidth)
        let extraSpace = Int(totalWidth) - (Int(self.referenceWidth) * self.self.conexionNodes.count) - arrowWidth
        
        
        //There are two cases of not enough space:
        //1) when there are two many connections for the available space
        if extraSpace <= (DeviceType.IS_ANY_IPAD == true ? 10 : 1)
        {
            self.drawMinimumNodes(context)
            return
        }
        //2) when nodes fix in space but the separation is very small
        let delta = (extraSpace / (self.conexionNodes.count + 1))
        if delta <= (DeviceType.IS_ANY_IPAD == true ? 10 : 1)
        {
            self.drawMinimumNodes(context)
            return
        }
        
        //All nodes can be drawn in the path
        self.drawNodes(delta, self.conexionNodes.count)
    }
    /*
        This function calculates the number of nodes that can be drawn at the given screen.
        It uses a fixed minimum space of separation between nodes.
        Then uses the drawNodes: function to perform the drawing routine
    */
    func drawMinimumNodes(_ context: CGContext)
    {
        //How many nodes we can draw with a confortable space of separation
        let delta =  DeviceType.IS_ANY_IPAD == true ? (self.frame.width * self.arrowWidth) + 10.0 : 8
        let numberOfNodes = Int(self.frame.width / (self.referenceWidth + CGFloat(delta)))
        guard numberOfNodes < self.conexionNodes.count else {return}
        self.drawNodes(Int(delta), numberOfNodes, true)
    }
    
    /*
        This function draws the specified number of nodes separated by a space = delta.
        If hiddenNodes == true it means that the last node will contain hidden connections.
        The node containing hidden connections will have a tap gesture recognizer
    */
    private func drawNodes(_ delta: Int, _ numberOfNodes: Int, _ hiddenNodes: Bool = false)
    {
        var space = delta
        let numbOfNodes = numberOfNodes - 1
        for idx in 0..<Int(numbOfNodes)
        {
            self.addSubview(self.getNodeWithImage(space, idx))
            space += Int(delta) + Int(self.referenceWidth)
        }
        if hiddenNodes == true
        {
            self.addSubview(self.getNodeWithGesture(space, numbOfNodes))
            return
        }
        //Add the last node normally
        self.addSubview(self.getNodeWithImage(space, numbOfNodes))
    }
    
    //This function returns a Node(UIImageView) with the profile picture found at
    //given index of conexionNodes array
    private func getNodeWithImage(_ separationSpace:Int, _ index: Int) -> UIImageView
    {
        let nodeView = UIImageView(frame: CGRect(x: CGFloat(separationSpace), y: 0, width: self.referenceWidth, height: self.referenceHeight))
        nodeView.layer.cornerRadius = self.referenceWidth / 2
        nodeView.layer.masksToBounds = true
        nodeView.layer.borderColor = UIColor.black.cgColor
        nodeView.layer.borderWidth = 2
        let imgURL = conexionNodes[index].data.thumbnailUrl
        print("URL_IMAGE: \(imgURL)")
        //Test: "http://pre15.deviantart.net/2d62/th/pre/f/2013/161/4/7/47045b8c895f8e9cc165ae5c78a70fca-d68kn3o.png"
        nodeView.setImageWith(URL(string: imgURL), placeholderImage:UIImage(named: "defaultperson"))
        
        return nodeView
    }
    //This function returns a node with a label describing the number of hidden connections
    //Also the returned view will have a tap gesture recognizer
    private func getNodeWithGesture(_ separationSpace:Int, _ index: Int) -> UIImageView
    {
        //Construct Node
        let nodeView = UIImageView(frame: CGRect(x: CGFloat(separationSpace), y: 0, width: self.referenceWidth, height: self.referenceHeight))
        nodeView.layer.cornerRadius = self.referenceWidth / 2
        nodeView.layer.masksToBounds = true
        nodeView.layer.borderColor = UIColor.black.cgColor
        nodeView.layer.borderWidth = 2
        let label = UILabel(frame: nodeView.bounds)
        label.layer.cornerRadius = self.referenceWidth / 2
        label.layer.masksToBounds = true
        label.backgroundColor = UIColor.blueBelize()
        
        //Construct label
        self.hiddenConnections = self.conexionNodes.count - (index)
        let labelNumber = NSMutableAttributedString(string: "\(self.hiddenConnections)")
        labelNumber.setBold("\(self.hiddenConnections)", size: 14)
        label.attributedText = labelNumber
        label.textAlignment = .center
        nodeView.addSubview(label)
        
        //Construct gesture recognizer
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.showHidenConnections))
        
        //Prepare and return view
        self.isUserInteractionEnabled = true
        nodeView.isUserInteractionEnabled = true
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(gestureRecognizer)
        
        return nodeView
    }
    
    @objc private func showHidenConnections()
    {
        var nodes = self.conexionNodes
        var stakeholders = [Stakeholder]()
        for i in 0..<self.hiddenConnections
        {
            if let stakeholder = nodes.last?.data {
                stakeholder.accountId = nodes.last?.idNode ?? 0
                stakeholder.isStakeholder = true
                stakeholders.insert(stakeholder, at: 0)
            }
            nodes.removeLast()
        }
        self.onHiddenConnectionsTap?(stakeholders)        
    }
    
    //Transform degrees to Radians
    func toRad(_ degrees:CGFloat) -> CGFloat
    {
        let rads = (CGFloat(Double.pi) * degrees) / 180
        
        return CGFloat(rads)
    }
    
    //MARK: DRAW ARROW LINE
    func drawLineIphone(_ context: CGContext)
    {
        //Global geometry
        let width = self.frame.width
        let height = self.frame.height
        
        //Draw  line
        context.saveGState()
        context.setLineCap(.round)
        context.setLineWidth(self.lineWidth)
        context.setStrokeColor((self.arrowColor?.cgColor)!)
        context.beginPath()
        
        if self.isVertical == false
        {
            context.move(to: CGPoint(x: width/2.0, y: 0))
            context.addLine(to: CGPoint(x: width/2, y: height*0.9))
        }
        else
        {
            context.move(to: CGPoint(x: 0, y: height/2))
            context.addLine(to: CGPoint(x: width, y: height/2))
        }
        
        context.strokePath()
        context.restoreGState()
        
        //Get A Triangle Mask
        UIGraphicsBeginImageContext(self.bounds.size)
        let maskContext = UIGraphicsGetCurrentContext()
        maskContext!.translateBy(x: 0, y: self.bounds.height)
        maskContext!.scaleBy(x: 1.0, y: -1.0)
        maskContext!.beginPath()
        let delta:CGFloat = (width/2)*self.arrowWidth
        
        if self.isVertical == false
        {
            maskContext!.move(to: CGPoint(x: (width/2)-delta, y: height*0.9))
            maskContext!.addLine(to: CGPoint(x: width/2, y: height))
            maskContext!.addLine(to: CGPoint(x: (width/2)+delta, y: height*0.9))
            maskContext!.addLine(to: CGPoint(x: (width/2)-delta, y: height*0.9))
            
        }
        else
        {
            maskContext!.move(to: CGPoint(x: width*0.95, y: (height/2)-delta))
            maskContext!.addLine(to: CGPoint(x: width, y: height/2))
            maskContext!.addLine(to: CGPoint(x: width*0.95, y: (height/2)+delta))
            maskContext!.addLine(to: CGPoint(x: width*0.95, y: (height/2)-delta))
        }
        
        maskContext!.closePath()
        maskContext!.fillPath()
        let mask = maskContext!.makeImage()
        UIGraphicsEndImageContext()
        
        //Clip triangle mask to a blue rectangle and release
        context.saveGState()
        context.setFillColor((self.arrowColor?.cgColor)!)
        context.clip(to: self.bounds, mask: mask!)
        context.fill(self.bounds)
        context.restoreGState()
    }
    
    func drawLineForIpad(_ context: CGContext)
    {
        //Global geometry
        let width = self.frame.width
        let height = self.frame.height
        
        //Draw  line
        context.saveGState()
        context.setLineCap(.round)
        context.setLineWidth(self.lineWidth)
        context.setStrokeColor((self.arrowColor?.cgColor)!)
        context.beginPath()
        
        if self.isVertical == false
        {
            context.move(to: CGPoint(x: width / 2.0, y: 0))
            context.addLine(to: CGPoint(x: width / 2, y: height*0.9))
        }
        else
        {
            context.move(to: CGPoint(x: 0, y: height / 2))
            context.addLine(to: CGPoint(x: width, y: height/2))
        }
        
        context.strokePath()
        context.restoreGState()
        
        //Get A Triangle Mask
        UIGraphicsBeginImageContext(self.bounds.size)
        let maskContext = UIGraphicsGetCurrentContext()
        maskContext!.translateBy(x: 0, y: self.bounds.height)
        maskContext!.scaleBy(x: 1.0, y: -1.0)
        maskContext!.beginPath()
        let delta:CGFloat = (width / 2) * self.arrowWidth
        
        if self.isVertical == false
        {
            if UIDevice.current.orientation.isPortrait
            {
                maskContext!.move(to: CGPoint(x: (width / 2) - (delta * 9), y: height * 0.9))
                maskContext!.addLine(to: CGPoint(x: width / 2, y: height))
                maskContext!.addLine(to: CGPoint(x: (width / 2) + (delta * 9), y: height * 0.9))
                maskContext!.addLine(to: CGPoint(x: (width / 2)-(delta * 9), y: height * 0.9))
            }
            else
            {
                maskContext!.move(to: CGPoint(x: (width / 2)-(delta * 5), y: height * 0.9))
                maskContext!.addLine(to: CGPoint(x: width / 2, y: height))
                maskContext!.addLine(to: CGPoint(x: (width / 2) + (delta * 5), y: height * 0.9))
                maskContext!.addLine(to: CGPoint(x: (width / 2) - (delta * 5), y: height * 0.9))
            }
        }
        else
        {
            maskContext!.move(to: CGPoint(x: width * 0.98, y: (height / 2) - (delta * 0.50)))
            maskContext!.addLine(to: CGPoint(x: width, y: height/2))
            maskContext!.addLine(to: CGPoint(x: width * 0.98, y: (height / 2) + (delta * 0.50)))
            maskContext!.addLine(to: CGPoint(x: width * 0.98, y: (height / 2) - (delta * 0.50)))
        }
        
        maskContext!.closePath()
        maskContext!.fillPath()
        let mask = maskContext!.makeImage()
        UIGraphicsEndImageContext()
        
        //Clip triangle mask to a blue rectangle and release
        context.saveGState()
        context.setFillColor((self.arrowColor?.cgColor)!)
        context.clip(to: self.bounds, mask: mask!)
        context.fill(self.bounds)
        context.restoreGState()
    }
}
