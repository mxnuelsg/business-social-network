//
//  MemberCollectionViewCell_iPad.swift
//  SHM
//
//  Created by Definity First on 2/19/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class MemberCollectionViewCell_iPad: UICollectionViewCell
{
    //MARK: PROPERTIES
    @IBOutlet weak var constraintFullName: NSLayoutConstraint!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lblEmailTitle: UILabel!
    @IBOutlet weak var lblPhoneTitle: UILabel!
    @IBOutlet weak var lblPositiontitle: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var imgContactView: UIImageView!
    var showBtnDelete = true
    var onDelete: ((ConexionReport) -> ())?
    var member: ConexionReport = ConexionReport(json: nil)

    override func awakeFromNib()
    {
        super.awakeFromNib()

        self.localizeStrings()
        self.backgroundColor = UIColor.whiteCloud()
        self.setBorder()
    }
    
    //MARK: LOAD MEMBER
    func loadMember(_ member: ConexionReport?)
    {
        guard let memberSafe = member else {
            
            return
        }
        
        self.member = memberSafe
        self.lblFullName.text = memberSafe.fullName
        let strJobPosition = memberSafe.jobPosition?.value ?? ""
        self.lblPosition.text = strJobPosition
        self.lblPhone.text = memberSafe.phone.isEmpty == true ? memberSafe.PhoneNumber : memberSafe.phone
        self.lblEmail.text = memberSafe.email
        self.imgContactView.setImageWith(URL(string: memberSafe.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
    }
    
    //MARK: HIDE DELETE BUTTON
    func hideBtnDelete()
    {
        self.btnDelete.isHidden = true
        let constraintRefresh = self.constraintFullName.constant - self.btnDelete.frame.size.width
        self.constraintFullName.constant = constraintRefresh
    }
    
     //MARK: ACTIONS
    @IBAction func deleteContact(_ sender: AnyObject)
    {
        self.onDelete?(self.member)
    }
    
    //MARK: FUNCTIONS
    func localizeStrings()
    {
        self.lblPositiontitle.text = "Position".localized()
        self.lblPhoneTitle.text = "Phone".localized()
        self.lblEmailTitle.text = "Email".localized()
    }
}
