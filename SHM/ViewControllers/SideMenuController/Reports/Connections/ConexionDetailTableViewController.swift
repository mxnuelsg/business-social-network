//
//  ConexionDetailTableViewController.swift
//  SHM
//
//  Created by Definity First on 2/16/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class ConexionDetailTableViewController: UITableViewController
{
    //MARK: PROPERTIES
    var arrayNodes: [Node] = []
    var trailing: CGFloat = 0.0
    var customTitle: String = ""
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator)
        self.tableView.reloadData()
    }
    
    //MARK: LOADCONFIG
    func loadConfig()
    {
        self.setupNavigation(animated: false)
        self.title = self.customTitle
        
        self.tableView.separatorColor = UIColor.clear
        self.tableView.hideEmtpyCells()
        self.tableView.register(UINib(nibName: "MemberSearchTableViewCell", bundle: nil), forCellReuseIdentifier: "MemberSearchTableViewCell")
        self.tableView.register(UINib(nibName: "ConnectionNodeDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "ConnectionNodeDetailTableViewCell")
        self.tableView.reloadData()
        self.tableView.backgroundColor = UIColor.grayTableBackground()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConnectionNodeDetailTableViewCell") as! ConnectionNodeDetailTableViewCell
        self.trailing = cell.constraintTrailing.constant
    }
    
    //MARK: NAVIGATION
    func setupNavigation(animated: Bool)
    {
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        navigationItem.setRightBarButtonItems([barBtnCompose], animated: animated)
    }
    
    // MARK: ACTIONS
    func createNewPost()
    {
        let vcNewPost = NewPostViewController()
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        present(navController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: LOAD CONFIG
    func reload()
    {
        self.tableView.reloadData()
    }
    
    func clear()
    {
        self.arrayNodes = []
        self.tableView.reloadData()
    }
    
    //MARK: FUNCTIONS
    //TODO: This function doesn't use anymore
    func showDetail(_ stakeholder: Stakeholder)
    {
        var arrayStakeholder: [Stakeholder] = []
        stakeholder.accountId = stakeholder.id
        arrayStakeholder.append(stakeholder)
        
        if DeviceType.IS_ANY_IPAD == false
        {
            let tableVCStakeholder = Storyboard.getInstanceOf(EmbeddedRelationsViewController.self)
            tableVCStakeholder.arrayStakeholders = arrayStakeholder
            tableVCStakeholder.isRelationReport = false
            tableVCStakeholder.currentOrbitLevel = 1
            tableVCStakeholder.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            tableVCStakeholder.onSeeDetailTapped =
                { (stakeholder) in
                    
                    if stakeholder.isStakeholder == true
                    {
                        let stakeholderDetail : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                        stakeholderDetail.stakeholder = stakeholder
                        self.navigationController?.pushViewController(stakeholderDetail, animated: true)
                        self.presentedViewController?.dismiss(animated: true, completion: nil)
                    }
                    else
                    {
                        let member = Member(json: nil)
                        member.id = stakeholder.id
                        let vcProfile = Storyboard.getInstanceOf(ProfileViewController.self)
                        vcProfile.member = member
                        self.navigationController?.pushViewController(vcProfile, animated: true)
                        self.presentedViewController?.dismiss(animated: true, completion: nil)
                    }
            }
            
            let navController = NavyController(rootViewController:tableVCStakeholder)
            navController.modalPresentationStyle = .overCurrentContext
            
            self.present(navController, animated: true, completion: {
                
                tableVCStakeholder.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
            })
        }
        else
        {
            let tableVCStakeholder = Storyboard.getInstanceOf(RelationsReportTableViewController_iPad.self)
            tableVCStakeholder.stakeholders = arrayStakeholder
            tableVCStakeholder.currentOrbitLevel = 1
            tableVCStakeholder.tableView.hideEmtpyCells()
            tableVCStakeholder.isEmbedded = true
            tableVCStakeholder.modalPresentationStyle = UIModalPresentationStyle.formSheet
            tableVCStakeholder.preferredContentSize = CGSize(width: 425, height: 230)
            tableVCStakeholder.isRelationReport = false
            tableVCStakeholder.onSeeDetailTapped = { (stakeholder) in
                
                if stakeholder.isStakeholder == true
                {
                    let stakeholderDetail : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                    stakeholderDetail.stakeholder = stakeholder
                    self.navigationController?.pushViewController(stakeholderDetail, animated: true)
                    self.presentedViewController?.dismiss(animated: true, completion: nil)
                }
                else
                {
                    let member = Member(json: nil)
                    member.id = stakeholder.id
                    let vcProfile = Storyboard.getInstanceOf(ProfileViewController.self)
                    vcProfile.member = member
                    self.navigationController?.pushViewController(vcProfile, animated: true)
                    self.presentedViewController?.dismiss(animated: true, completion: nil)
                }
            }
            self.present(tableVCStakeholder, animated: true, completion: {tableVCStakeholder.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)})
        }
    }
    
    //MARK: - TABLE VIEW DATASOURCE
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrayNodes.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let node = self.arrayNodes[indexPath.row]
        
        if node.isEdge == false
        {
            return 80
        }
        else
        {
            return 80
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let node = self.arrayNodes[indexPath.row]
        
        if node.isEdge == false
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MemberSearchTableViewCell") as! MemberSearchTableViewCell
            cell.selectionStyle = .none
            cell.clearBorder()
            cell.lblName.text = node.data.fullName
            
            //Position
            //JobPosition/Company
            node.data.isStakeholder = NodeType.stakeholder.rawValue == node.typeNode
            cell.loadCellWith(node.data)
            cell.imgProfile.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            cell.imgProfile.layer.borderWidth = 0.5
            cell.viewContainer.setBorder()
            cell.viewContainer.layer.cornerRadius = 5
            cell.viewContainer.clipsToBounds = true
            cell.backgroundColor = UIColor.clear
            cell.constraingLeadingView.constant = self.trailing + 5
            cell.constraingTrailingView.constant = self.trailing + 5
                        
            return cell
        }
        else
        {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "ConnectionNodeDetailTableViewCell") as! ConnectionNodeDetailTableViewCell
            cell.selectionStyle = .none
            cell.setColor(node.edge.value)
            cell.backgroundColor = UIColor.clear
            cell.vcArrow.setNeedsDisplay()
            
            return cell
        }
    }
    
    //MARK: - TABLE VIEW DELEGATE
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 10
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 10)
        view.layer.backgroundColor = UIColor.grayTableBackground().cgColor
        
        return view
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        /*
         * * * Note:
         Node ID equivalent to > Stakeholder ID / Account ID
         Node TYPE equivalent to > Stakeholder = 1  / Account  = 2
         */
        
        let node = self.arrayNodes[indexPath.row]
        let stakeholder: Stakeholder = node.data
        stakeholder.id = node.idNode
        stakeholder.isStakeholder = node.typeNode == NodeType.stakeholder.rawValue ? true : false
        
        if node.isEdge == false
        {
            if stakeholder.isStakeholder == true
            {
                //Stakeholder Detail
                if DeviceType.IS_ANY_IPAD == true
                {
                    let vcStakeholderDetail = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
                    vcStakeholderDetail.stakeholder = stakeholder
                    
                    self.navigationController?.pushViewController(vcStakeholderDetail, animated: true)
                }
                else
                {
                    let vcStakeholderDetail2 : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                    vcStakeholderDetail2.stakeholder = stakeholder
                    
                    self.navigationController?.pushViewController(vcStakeholderDetail2, animated: true)
                }
            }
            else
            {
                //Account Profile
                let member = Member(isDefault: true)
                member.id = stakeholder.id
                
                let vcProfile = DeviceType.IS_ANY_IPAD == true ? Storyboard.getInstanceOf(ProfileViewController_iPad.self) : Storyboard.getInstanceOf(ProfileViewController.self)
                vcProfile.member = member
                
                self.navigationController?.pushViewController(vcProfile, animated: true)
            }
        }
    }
}
