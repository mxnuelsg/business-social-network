//
//  ConecctionResultsPortraitViewController_iPad.swift
//  SHM
//
//  Created by Definity First on 2/22/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class ConectionResultsPortraitViewController_iPad: UIViewController
{
    //MARK: PROPERTIES
    var arrayMembers: [ConexionReport] = []
    var arrayConnectionResult: [Conexion] = []
    var vcConnecctionHeader: ConnectionHeaderCollectionViewController!
    var vcConnectionResultTable: ConnectionResultTableViewController!
    var showDeleteBtn: Bool = false
    var isConnectionReport: Bool = true
    var customTitle: String = ""

    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = self.customTitle
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "ConnectionHeaderCollectionViewController"
        {
            self.vcConnecctionHeader = segue.destination as! ConnectionHeaderCollectionViewController
            self.vcConnecctionHeader.arrayMembers = self.arrayMembers
            self.vcConnecctionHeader.showDeleteBtn = self.showDeleteBtn
            self.vcConnecctionHeader.isConnectionReport = self.isConnectionReport
            self.vcConnecctionHeader.customTitle = self.customTitle
        }
        
        if segue.identifier == "ConnectionResultTableViewController"
        {
            self.vcConnectionResultTable = segue.destination as! ConnectionResultTableViewController
            self.vcConnectionResultTable.arrayConexionsResult = self.arrayConnectionResult
            self.vcConnectionResultTable.arrayMembers = self.arrayMembers
            self.vcConnectionResultTable.showHeader = false
            self.vcConnectionResultTable.isConnectionReport = self.isConnectionReport
            self.vcConnectionResultTable.customTitle = self.customTitle
        }
    }
}
