//
//  ConexionDetailViewController.swift
//  SHM
//
//  Created by Definity First on 2/17/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class ConexionDetailViewController: UIViewController
{
    //MARK: PROPERTIES
    @IBOutlet weak var lblNumberOfNodes: UILabel!
    @IBOutlet weak var viewArrow: ArrowView!
    @IBOutlet weak var imgTo: UIImageView!
    @IBOutlet weak var imgFrom: UIImageView!
    @IBOutlet weak var lblTitleHeader: UILabel!
    var vcConexionDetailTable: ConexionDetailTableViewController!
    var arrayConexionHeader: [ConexionReport] = []
    var arrayConexionResult: [Node] = []
    var route: Int = 0
    var nodeWeight: Int = 0
    var totalNodes: Int = 0
    var isConnectionReport: Bool = true
    var customTitle: String = ""
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator)
        self.vcConexionDetailTable.reload()
        self.viewArrow.setNeedsDisplay()
    }
    
    //MARK: LOAD CONFIG
    func loadConfig()
    {
        self.localize()
        self.setupNavigation(animated: false)
        self.title = self.customTitle
        
        // setting round border to label
        self.lblNumberOfNodes.textColor = UIColor.whiteCloud()
        self.lblNumberOfNodes.adjustsFontSizeToFitWidth = true
        self.lblNumberOfNodes.textAlignment = NSTextAlignment.center
        self.lblNumberOfNodes.layer.masksToBounds = true
        self.lblNumberOfNodes.layer.cornerRadius = self.lblNumberOfNodes.layer.frame.width / 2
        self.lblNumberOfNodes.backgroundColor = UIColor.blueBelize()
        
        //Setting images containers
        self.imgFrom.layer.cornerRadius = self.imgFrom.frame.size.width / 2
        self.imgFrom.layer.borderColor = UIColor.blueBelize().cgColor
        self.imgFrom.layer.borderWidth = 1.5
        self.imgFrom.layer.masksToBounds = true
        
        self.imgTo.layer.cornerRadius = self.imgFrom.frame.size.width / 2
        self.imgTo.layer.borderColor = UIColor.blackAsfalto().cgColor
        self.imgTo.layer.borderWidth = 1
        self.imgTo.layer.masksToBounds = true
        
        self.viewArrow.arrowColor = UIColor.blackAsfalto()
        self.viewArrow.isVertical = true
        self.viewArrow.arrowWidth = 0.05
        
        self.view.backgroundColor = UIColor.grayTableBackground()
        
        self.loadData()
    }
    
    fileprivate func localize()
    {
        self.lblTitleHeader.text = "Route 1 ".localized()        
    }
    
    //MARK: LOAD DATA
    func loadData()
    {
        guard arrayConexionHeader.count > 0 else {
            return
        }
        
        let from = arrayConexionHeader[0]
        let to = self.isConnectionReport == true ? self.arrayConexionHeader[1] : ConexionReport(json: nil)
        
        if self.isConnectionReport == false
        {
            let toAccount = self.arrayConexionResult[self.arrayConexionResult.count - 1]
            to.thumbnailUrl = toAccount.data.thumbnailUrl
            to.name = toAccount.data.name
            to.lastName = toAccount.data.lastName
            to.email = toAccount.data.email
            to.jobPosition = toAccount.data.jobPosition
            to.jobTitle = toAccount.data.jobTitle
            to.isStakeholder = toAccount.typeNode == NodeType.stakeholder.rawValue
        }
        
        self.lblTitleHeader.text = "\("Route".localized())  \(self.route.description)      \(self.nodeWeight) \("Points".localized())"
        self.imgFrom.setImageWith(URL(string: from.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        self.imgTo.setImageWith(URL(string: to.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        self.lblNumberOfNodes.text = self.totalNodes.description
        self.vcConexionDetailTable.arrayNodes = self.arrayConexionResult
        self.vcConexionDetailTable.reload()
    }
    
    // MARK: ACTIONS
    func createNewPost()
    {
        let vcNewPost = NewPostViewController()
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = .overFullScreen
        present(navController, animated: true, completion: nil)
    }
    
    //MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier! == "ConexionDetailTableViewController"
        {
            self.vcConexionDetailTable = segue.destination as! ConexionDetailTableViewController
            self.vcConexionDetailTable.customTitle = self.customTitle
        }
    }
    
    func setupNavigation(animated: Bool)
    {
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        navigationItem.setRightBarButtonItems([barBtnCompose], animated: animated)
    }
}
