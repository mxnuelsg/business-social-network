//
//  StakeholderCollectionViewCell.swift
//  SHM
//
//  Created by Definity First on 2/19/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class StakeholderCollectionViewCell_iPad: UICollectionViewCell
{
    //MARK: PROPERTIES
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lblPlaceOfBirthTitle: UILabel!
    @IBOutlet weak var lblRepresentativeTitle: UILabel!
    @IBOutlet weak var lblCoordinatorTitle: UILabel!
    @IBOutlet weak var lblNationalityTitle: UILabel!
    @IBOutlet weak var lblOccupationTitle: UILabel!
    @IBOutlet weak var lblBirthdayTitle: UILabel!
    @IBOutlet weak var constraintFullName: NSLayoutConstraint!
    @IBOutlet weak var imgContactView: UIImageView!
    @IBOutlet weak var lblRepresentative: UILabel!
    @IBOutlet weak var lblCoordinator: UILabel!
    @IBOutlet weak var lblOccupation: UILabel!
    @IBOutlet weak var lblBirth: UILabel!
    @IBOutlet weak var lblPlaceOfbirth: UILabel!
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var lblNationality: UILabel!
    @IBOutlet weak var lblFullname: UILabel!
    
    //Rating
    
    
    @IBOutlet weak var lblAdminRatingTitle: UILabel!
    
    @IBOutlet weak var lblUserRatingTitle: UILabel!
    
    @IBOutlet weak var lblAdminRating: UILabel!
    
    
    @IBOutlet weak var lblUserRating: UILabel!
    var stakeholder: ConexionReport = ConexionReport(json: nil)
    var onDelete: ((ConexionReport) -> ())?
    
    //MARK: LIFE CYCLE
    override func awakeFromNib()
    {
        super.awakeFromNib()

        self.backgroundColor = UIColor.whiteCloud()
        self.lblPlaceOfBirthTitle.adjustsFontSizeToFitWidth = true
        self.lblBirthdayTitle.adjustsFontSizeToFitWidth = true
        self.lblOccupationTitle.adjustsFontSizeToFitWidth = true
        self.lblNationalityTitle.adjustsFontSizeToFitWidth = true
        self.lblCoordinatorTitle.adjustsFontSizeToFitWidth = true
        self.lblRepresentativeTitle.adjustsFontSizeToFitWidth = true
        
        self.lblFullname.adjustsFontSizeToFitWidth = true
        self.lblPosition.adjustsFontSizeToFitWidth = true
        self.lblBirth.adjustsFontSizeToFitWidth = true
        self.lblOccupation.adjustsFontSizeToFitWidth = true
        self.lblCoordinator.adjustsFontSizeToFitWidth = true
        self.lblRepresentative.adjustsFontSizeToFitWidth = true
        
        self.localizeStrings()
        self.setBorder()
    }
    
    //MARK: LOAD STAKEHOLDER
    func loadStakeholder(_ stakeholderToLoad: ConexionReport?)
    {
        guard let stake = stakeholderToLoad else {
            
            return
        }
        
        self.stakeholder = stake
        
        let attStrNotInfoAvailable = NSMutableAttributedString(string: String("No Information Available".localized()))
        attStrNotInfoAvailable.setColor(String("No Information Available".localized()), color: UIColor.grayConcreto())
        attStrNotInfoAvailable.setBold(String("No Information Available".localized()), size: 8)
        
        //Fullname
        self.lblFullname.text = stake.fullName
        self.lblFullname.adjustsFontSizeToFitWidth = true
        
        //Values
        //JobPosition/Company
        var strJobPosition = String()
        if let jobPosition = stake.jobPosition?.value, jobPosition != "" {

            strJobPosition = jobPosition
        }
        else
        {
            strJobPosition = "No Information Available".localized()
        }
        
        let strCompanyName = stake.companyName == "" ? "No Information Available".localized() : stake.companyName
        let attJobPosition =  NSMutableAttributedString().setJobPositionCompany(strJobPosition, company: strCompanyName)
        self.lblPosition.attributedText = attJobPosition
        
        //Birth place
        var strBirthPlaceCountry = String()
        
        if stake.placeOfBirth.isEmpty == false
        {
            
            strBirthPlaceCountry += stake.placeOfBirth
        }
        else
        {
            strBirthPlaceCountry +=  "No Information Available".localized()
        }
        
        if let strCountry = stake.country, strCountry.value != "" {
            
            strBirthPlaceCountry += "/" + strCountry.value
        }
        else
        {
            strBirthPlaceCountry += "/" + "No Information Available".localized()
        }
        
        let attBirthPlace = NSMutableAttributedString(string: strBirthPlaceCountry)
        attBirthPlace.setColor(String("No Information Available".localized()), color: UIColor.grayConcreto())
        attBirthPlace.setBold(String("No Information Available".localized()), size: 8)
        attBirthPlace.setColor("/"+String("No Information Available".localized()), color: UIColor.grayConcreto())
        attBirthPlace.setBold("/"+String("No Information Available".localized()), size: 8)
        self.lblPlaceOfbirth.attributedText = attBirthPlace
        
        //Birthdate and Age
        let strAge = stake.age != nil ? String(stake.age!) : "No Information Available".localized()
        
        let birthday = (stake.birthday != nil && stake.birthday!.isValid()) ? stake.birthday : nil
        let strBirthday = birthday?.getStringStyleMedium()
            ?? stake.birthDateString
            ?? "No Information Available".localized()
        
        let attStrBirthdateAge = birthday != nil ? NSMutableAttributedString(string:strBirthday + "(" + strAge + " " + String("Years old".localized()) + ")") : NSMutableAttributedString(string:strBirthday)
        
        attStrBirthdateAge.setColor(String("No Information Available".localized()), color: UIColor.grayConcreto())
        attStrBirthdateAge.setBold(String("No Information Available".localized()), size: 8)
        self.lblBirth.attributedText = attStrBirthdateAge
        
        //Occupation
        if let strOccupation = stake.occupation?.value, strOccupation != "" {
            self.lblOccupation.text = strOccupation
        }
        else
        {
            self.lblOccupation.attributedText = attStrNotInfoAvailable
        }
        
        //Nationalities
        self.lblNationality.text = stake.nationalities.collapseValues()
        self.lblNationality.adjustsFontSizeToFitWidth = true
        
        self.lblCoordinator.text = stake.coordinator?.name ?? ""
        self.lblRepresentative.text = stake.representative?.name ?? ""
        
        //Thumbnail
        self.imgContactView.setImageWith(URL(string: stake.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        
        //Rating
        self.lblUserRatingTitle.adjustsFontSizeToFitWidth = true
        self.lblUserRating.adjustsFontSizeToFitWidth = true
        self.lblUserRatingTitle.text = "User Rating".localized()
        self.lblUserRating.text = (stake.userRating ?? 0.0).getString()
        
        
        self.lblAdminRatingTitle.adjustsFontSizeToFitWidth = true
        self.lblAdminRating.adjustsFontSizeToFitWidth = true
        self.lblAdminRatingTitle.text = "Admin Rating".localized()
        self.lblAdminRating.text = (stake.adminRating ?? 0.0).getString()
    }

    
    //MARK: ACTIONS
    @IBAction func deleteContact(_ sender: AnyObject)
    {
        self.onDelete?(self.stakeholder)
    }
    
    //MARK: FUNCTIONS
    func hideDeleteButton()
    {
        self.btnDelete.isHidden = true
//        let constraitnconstant = self.constraintFullName.constant - self.btnDelete.frame.size.width
//        self.constraintFullName.constant = constraitnconstant
    }
    
    //MARK: FUNCTIONS
    func localizeStrings()
    {
        self.lblPlaceOfBirthTitle.text = "Place of Birth".localized()
        self.lblBirthdayTitle.text =  "Birthday (Age)".localized()
        self.lblOccupationTitle.text =  "Occupation".localized()
        self.lblNationalityTitle.text = "Nationality".localized()
        self.lblCoordinatorTitle.text = "Coordinator".localized()
        self.lblRepresentativeTitle.text = "Representative".localized()
    }
}
