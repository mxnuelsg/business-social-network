//
//  ConexionHeaderCollectionViewController.swift
//  SHM
//
//  Created by Definity First on 2/23/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class ConnectionHeaderCollectionViewController: UICollectionViewController
{
    //MARK: PROPERTIES
    var arrayMembers: [ConexionReport] = []
    var layout: UICollectionViewFlowLayout!
    var showDeleteBtn: Bool = true
    var height: CGFloat = 200
    var onDeleteUser: ((_ user: ConexionReport) -> ())?
    var isConnectionReport: Bool = true
    var customTitle: String = ""
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        //super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        coordinator.animate(alongsideTransition: nil, completion: {
            (context) in
            
            self.loadLayoutForCollectionView()
        })
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: LOAD CONFIG
    fileprivate func loadConfig()
    {
        self.title = self.customTitle
        self.layout = UICollectionViewFlowLayout()
        
        //LOAD CONFIG
        self.layout = UICollectionViewFlowLayout()
        self.loadLayoutForCollectionView()
        self.collectionView!.collectionViewLayout = self.layout
        self.collectionView!.dataSource = self
        self.collectionView!.delegate = self
        self.collectionView!.allowsMultipleSelection = false
        self.collectionView!.isScrollEnabled = false
        //registering cells
        self.collectionView!.register(UINib(nibName: "StakeholderCollectionViewCell_iPad", bundle: nil), forCellWithReuseIdentifier: "StakeholderCollectionViewCell_iPad")
        self.collectionView!.register(UINib(nibName: "MemberCollectionViewCell_iPad", bundle: nil), forCellWithReuseIdentifier: "MemberCollectionViewCell_iPad")
        self.collectionView!.register(UINib(nibName: "EmptyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "EmptyCollectionViewCell")
        self.collectionView!.backgroundColor = UIColor.clear
        self.collectionView!.reloadData()
    }
    
    //MARK: RELOAD COLLECTION VIEW
    func reload()
    {
        self.collectionView?.reloadData()
    }
    
    //MARK: CLEAR DATA OF COLLECTION VIEW
    func clearData()
    {
        self.arrayMembers = []
        self.collectionView?.reloadData()
    }
    
    //MARK: LAYOUT FOR COLLECTION VIEW
    func loadLayoutForCollectionView()
    {
        if (UIDevice.current.orientation.isPortrait == true)
        {
            self.layout.scrollDirection = UICollectionViewScrollDirection.vertical
            self.layout.minimumInteritemSpacing = 1
            self.layout.minimumLineSpacing = 1
            self.layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 1)
            if self.isConnectionReport == true
            {
                let width = UIScreen.main.bounds.width * 0.498
                self.layout.itemSize = CGSize(width: width, height: self.height)
            }
            else
            {
                self.layout.itemSize = CGSize(width: (self.view.frame.size.width) - 2, height: self.height)
            }
        }
        else
        {
            self.layout.scrollDirection = UICollectionViewScrollDirection.vertical
            self.layout.minimumInteritemSpacing = 1
            self.layout.minimumLineSpacing = 1
            self.layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 4)
            let width = UIScreen.main.bounds.width * 0.5
            self.layout.itemSize = CGSize(width: width, height: self.height)
        }
    }
    
    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.arrayMembers.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let member = self.arrayMembers[indexPath.row]
        
        if member.fullName.isEmpty == false
        {
            if member.isStakeholder == true
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StakeholderCollectionViewCell_iPad", for: indexPath) as! StakeholderCollectionViewCell_iPad
                cell.loadStakeholder(member)
                
                if self.showDeleteBtn == false
                {
                    cell.hideDeleteButton()
                }
                
                cell.onDelete = { [weak self] (user) in
                    
                    self?.onDeleteUser?(user)
                }
                
                return cell
            }
            else
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MemberCollectionViewCell_iPad", for: indexPath) as! MemberCollectionViewCell_iPad
                cell.loadMember(member)
                
                if self.showDeleteBtn == false
                {
                    cell.hideBtnDelete()
                }
                
                cell.onDelete = { [weak self] (user) in
                    
                    self?.onDeleteUser?(user)
                }
                
                return cell
            }
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyCollectionViewCell", for: indexPath) as! EmptyCollectionViewCell
            cell.setBorder()
            cell.layer.borderColor = UIColor.clear.cgColor
            cell.layer.backgroundColor = UIColor.clear.cgColor
            
            return cell
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}
