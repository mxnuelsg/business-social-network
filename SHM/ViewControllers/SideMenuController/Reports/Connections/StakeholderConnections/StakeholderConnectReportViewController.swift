//
//  StakeholderConnectReportViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 1/27/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class StakeholderConnectReportViewController: UIViewController
{
    //MARK: PROPERTIES
    var arrayMembers: [ConexionReport] = []
    var vcTableSearch = ConexionsTableViewController()
    var keyboardFrame: CGRect! = CGRect(x: 0, y: 0, width: 320, height: 260)
    @IBOutlet weak var tblMembers: UITableView!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var viewInstrucctions: UIView!
    @IBOutlet weak var lblInstrucctions: UILabel!
    
    @IBOutlet weak var tabBarItemCreateReport: UITabBarItem!
    
    @IBOutlet weak var searchBar: AutoSearchBar!
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    //MARK: LOAD CONFIG
    fileprivate func loadConfig()
    {
        //creation of SearchController
        searchBar.initialize(shouldSearchAfterDelay: true,
        onSearchText: { [weak self] (text) in
            self?.vcTableSearch.view.isHidden = false
            self?.searchData(text)
            self?.lblInstrucctions.isHidden = true
        }, onBeginEditing: { [weak self] (searchBar) in
            if self != nil {
                searchBar.inputAccessoryView = self!.loadToolBar()
                searchBar.backgroundColor = UIColor.groupTableViewBackground
                searchBar.isMultipleTouchEnabled = false
                searchBar.isExclusiveTouch = true
                
                if searchBar.text?.isEmpty == false
                {
                    self!.vcTableSearch.view.isHidden = false
                }
                else
                {
                    self!.vcTableSearch.clear()
                }
                self?.lblInstrucctions.isHidden = true
            }
            
        }, onEndEditing: { [weak self] (searchBar) in
            if searchBar.text?.trim().isEmpty == true
        	{
            	self?.vcTableSearch.view.isHidden = true
            }
            self?.lblInstrucctions.isHidden = false
		}, onClearText: { [weak self] in
            self?.vcTableSearch.view.isHidden = true
        })

        //setup
        self.localize()
        self.lblInstrucctions.adjustsFontSizeToFitWidth = true
        
        self.setupNavigation(animated: false)
        
        self.tblMembers.hideEmtpyCells()
        self.tblMembers.register(UINib(nibName: "ConnectionCellStakeholderUITableViewCell", bundle: nil), forCellReuseIdentifier: "ConnectionCellStakeholderUITableViewCell")
                self.view.addSubview(self.vcTableSearch.view)
        self.vcTableSearch.view.isHidden = true
        self.tblMembers.backgroundColor = UIColor.white
        self.tblMembers.isScrollEnabled = false
        
        /**
        Keyboard Notification
        */
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        self.vcTableSearch.onUsersSelected = { (user) in
            
                self.viewInstrucctions.isHidden = true
                self.vcTableSearch.tableView.isHidden = true
                self.arrayMembers.append(user)
                self.tblMembers.reloadData()
                self.searchBar.text? = ""
                self.searchBar.endEditing(true)
                
                if self.arrayMembers.count == 1
                {
                    self.searchBar.isUserInteractionEnabled = false
                    self.searchBar.isTranslucent = false
                    self.searchBar.searchBarStyle = .minimal
                    self.searchBar.backgroundColor = UIColor.lightGray
                }
        }
    }
    
    fileprivate func localize()
    {
        if DeviceType.IS_ANY_IPAD == false
        {
            self.lblInstrucctions.text = "Select a stakeholder. Different routes, from lowest to highest score to reach the stakeholder will be shown. This from site users".localized()
            self.title = "COMPANY CONNECTIONS".localized()
            self.searchBar.placeholder = "Search a contact to make a connection".localized()
            self.tabBarItemCreateReport.title = "Create Report".localized()
        }
    }
    
    func setupNavigation(animated: Bool)
    {
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        navigationItem.setRightBarButtonItems([barBtnCompose], animated: animated)
    }
    
    // MARK: TOOLBAR SEARCH BAR
    func loadToolBar() -> UIToolbar
    {
        let toolBarKeyboard = UIToolbar()
        toolBarKeyboard.barStyle = UIBarStyle.blackOpaque;
        toolBarKeyboard.barTintColor = UIColor.colorForNavigationController()

        let barBtnDone = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(self.closeKeyboard))
        barBtnDone.tintColor = UIColor.white

        toolBarKeyboard.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            barBtnDone]
        toolBarKeyboard.sizeToFit()

        return toolBarKeyboard
    }

    func closeKeyboard()
    {
        //        self.vcTableSearch.view.isHidden = true
        self.searchBar.dismissKeyboard()
        self.vcTableSearch.view.frame.size.height = tabBar.frame.origin.y - self.vcTableSearch.view.frame.origin.y
    }

    // MARK: ACTIONS
    func createNewPost()
    {
        let vcNewPost = NewPostViewController()
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = .overFullScreen
        present(navController, animated: true, completion: nil)
    }
    
    //MARK: PRIVATE FUNCTIONS
    func searchData(_ searchText: String)
    {
        //calling the webservice to get the members
        self.vcTableSearch.displayBackgroundMessage("Loading...".localized(),
            subMessage: "")
        
        LibraryAPI.shared.reportsBO.getStakeholders(searchText, onSuccess: { (response) -> () in
            
            if let userResponse = response as [ConexionReport]! {
                self.vcTableSearch.arrayUsers = userResponse
                self.vcTableSearch.reload()
                if userResponse.count == 0
                {
                    self.vcTableSearch.displayBackgroundMessage("No results found".localized(),
                                                                subMessage: "")
                }
                else
                {
                    self.vcTableSearch.dismissBackgroundMessage()
                }
            }
            }) { error in
                
                self.vcTableSearch.dismissBackgroundMessage()
                
                MessageManager.shared.showBar(title: "Error",
                                              subtitle: "Error.TryAgain".localized(),
                                              type: .error,
                                              fromBottom: false)
        }
    }
    
    //MARK: REMOVE USER FROM ARRAY
    func removeUserFromArray(_ userToDelete: ConexionReport)
    {
        if let index = self.arrayMembers.index(of: userToDelete) {
            
            self.searchBar.isUserInteractionEnabled = true
            self.searchBar.isTranslucent = true
            self.searchBar.searchBarStyle = .default
            self.searchBar.backgroundColor = UIColor.clear
            self.arrayMembers.remove(at: index)
            self.tblMembers.reloadData()
            self.viewInstrucctions.isHidden = false
        }
        else
        {
            MessageManager.shared.showBar(title: "Error",
                                          subtitle: "Error.TryAgain".localized(),
                                          type: .error,
                                          fromBottom: false)
        }
    }
    
    // MARK: KEYBOARD NOTIFICATION
    func keyboardWillShow(_ notification: Notification)
    {
        var info = notification.userInfo!
        keyboardFrame = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        let rectHeight = keyboardFrame.size.height + self.searchBar.frame.size.height + 50
        
        self.vcTableSearch.view.frame = CGRect(x: 0,y: searchBar.frame.size.height, width: ScreenSize.SCREEN_WIDTH , height: ScreenSize.SCREEN_HEIGHT - rectHeight)
    }
}

//MARK: TABLE VIEW DELEGATES
extension StakeholderConnectReportViewController: UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrayMembers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let user = arrayMembers[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConnectionCellStakeholderUITableViewCell", for: indexPath) as! ConnectionCellStakeholderUITableViewCell
        cell.loadStakeholder(user)
        cell.onDelete = { (user) in
            
                self.removeUserFromArray(user)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 177
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        self.vcTableSearch.view.isHidden = true
    }
}

// MARK: EXTENSIONS TAB BAR
extension StakeholderConnectReportViewController: UITabBarDelegate
{
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        //Selected item color equal not selected color
        tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        
        switch item.tag
        {
        case 0:
            
            guard self.arrayMembers.count == 1  else {
                
                MessageManager.shared.showBar(title: "Warning".localized(),
                                              subtitle: "Search to make a connection".localized(),
                                              type: .warning,
                                              fromBottom: false)
                
                return
            }
            
            //Indicator
            MessageManager.shared.showLoadingHUD()
            
            let request = Input()
            let firstNode = self.arrayMembers[0]
            request.nodeType = 0
            request.start = firstNode.stakeholderId
            request.end = 0
            
            LibraryAPI.shared.reportsBO.getStakeholderConnections(request.getWSObject(), onSuccess: { (users) -> () in
                
                MessageManager.shared.hideHUD()
                
                if users.count > 0
                {
                    let vcReportDetail = Storyboard.getInstanceOf(ConnectionResultTableViewController.self)
                    vcReportDetail.arrayMembers = self.arrayMembers
                    vcReportDetail.isConnectionReport = false
                    vcReportDetail.arrayConexionsResult = users
                    self.navigationController?.pushViewController(vcReportDetail, animated: true)
                }
                else
                {
                    MessageManager.shared.showBar(title: "Warning",
                                                  subtitle: "No connections to display".localized(),
                                                  type: .warning,
                                                  fromBottom: false)
                }
                
                }, onError: { error in
                    
                    MessageManager.shared.hideHUD()
                    MessageManager.shared.showBar(title: "Error",
                                                  subtitle: "Error.TryAgain".localized(),
                                                  type: .error,
                                                  fromBottom: false)
            })
            
        default:
            break
        }
    }
}
