//
//  HostConnectionDetailViewController.swift
//  SHM
//
//  Created by Definity First on 2/24/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class HostConnectionDetailViewController_iPad: UIViewController
{
    //MARK: PROPERTIES
    var isConnectionReport: Bool = true
    var arrayConexionHeader: [ConexionReport] = []
    var arrayConexionResult: [Node] = []
    var route: Int = 0
    var totalNodes: Int = 0
    var nodeWeight: Int = 0
    var vcConnectionPortrait: ConnectionDetailPortraitCollectionViewController_iPad!
    var vcConnectionLandscape: ConnectionDetailLandscapeCollectionViewController_iPad!
    @IBOutlet weak var containerLandscape: UIView!
    @IBOutlet weak var containerPortrait: UIView!
    var customTitle: String = ""
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator)
        self.changeRotation()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: FUNCTIONS
    func changeRotation()
    {
        if UIDevice.current.orientation.isPortrait
        {
            self.containerLandscape.isHidden = true
            self.containerPortrait.isHidden = false
        }
        else
        {
            self.containerLandscape.isHidden = false
            self.containerPortrait.isHidden = true
        }
    }
    
    fileprivate func loadConfig()
    {
        self.backButtonArrow()
        
        //Bar title
        navigationItem.title = self.customTitle
        
        //New post button
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        navigationItem.rightBarButtonItems = [barBtnCompose]
        self.containerLandscape.setBorder()
    }
    
    // MARK: ACTIONS
    func createNewPost()
    {
        let vcNewPost = NewPostModal()
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
        
        present(navController, animated: true, completion: nil)
    }
    
    //MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "ConnectionDetailPortraitCollectionViewController_iPad"
        {
            self.vcConnectionPortrait = segue.destination as! ConnectionDetailPortraitCollectionViewController_iPad
            self.vcConnectionPortrait.arrayConexionHeader = self.arrayConexionHeader
            self.vcConnectionPortrait.arrayConexionResult = self.arrayConexionResult
            self.vcConnectionPortrait.route = self.route
            self.vcConnectionPortrait.nodeWeight = self.nodeWeight
            self.vcConnectionPortrait.totalNodes = self.totalNodes
            self.vcConnectionPortrait.isConnectionReport = self.isConnectionReport
        }
        
        if segue.identifier == "ConnectionDetailLandscapeCollectionViewController_iPad"
        {
            self.vcConnectionLandscape = segue.destination as! ConnectionDetailLandscapeCollectionViewController_iPad
            self.vcConnectionLandscape.arrayConexionHeader = self.arrayConexionHeader
            self.vcConnectionLandscape.arrayConexionResult = self.arrayConexionResult
            self.vcConnectionLandscape.route = self.route
            self.vcConnectionLandscape.nodeWeight = self.nodeWeight
            self.vcConnectionLandscape.totalNodes = self.totalNodes
            self.vcConnectionLandscape.isConnectionReport = self.isConnectionReport
        }
        
        self.changeRotation()
    }
}
