//
//  ConexionNodeTableViewCell.swift
//  SHM
//
//  Created by Definity First on 2/12/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON
class ConnectionNodeTableViewCell: UITableViewCell
{
    //MARK: PROPERTIES
    @IBOutlet weak var lblNumberOfNodes: UILabel!
    @IBOutlet weak var vcCell: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgTo: UIImageView!
    @IBOutlet weak var imgFrom: UIImageView!
    @IBOutlet weak var vcNodeContainer: ArrowView!
    var onDetailTapped:((_ index:Int)->())?
    var onHiddenConnectionsTap:((_ stakeholders:[Stakeholder])->())?
    var IsConexionReport: Bool = true
    var testNum = 0
    
    @IBOutlet weak var detailArrow: UIImageView!
    
    //MARK: LIFE CYCLE
    override func awakeFromNib()
    {
        super.awakeFromNib()
        loadConfig()
        //self.lblNumberOfNodes.isHidden = true
    }
    
    func loadConfig()
    {
        self.vcCell.setBorder()
        
        self.imgFrom.layer.cornerRadius = self.imgFrom.frame.size.width / 2
        self.imgFrom.layer.borderColor = UIColor(red: 0.125490, green: 0.419608, blue: 0.819608, alpha: 1).cgColor
        self.imgFrom.layer.borderWidth = 1.5
        self.imgFrom.layer.masksToBounds = true
        
        self.imgTo.layer.cornerRadius = self.imgFrom.frame.size.width / 2
        self.imgTo.layer.borderColor = UIColor.blackAsfalto().cgColor
        self.imgTo.layer.borderWidth = 1
        self.imgTo.layer.masksToBounds = true
        
        self.vcNodeContainer.arrowColor = UIColor.blackAsfalto()
        self.vcNodeContainer.isVertical = true
        self.vcNodeContainer.arrowWidth = 0.05
        
        //Detail Arrow Action
        self.detailArrow.isUserInteractionEnabled = true
        self.detailArrow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.detailTapped)))
            
        //for inner connections nodes        
        self.vcNodeContainer.referenceWidth = self.imgFrom.frame.width * 0.90
        self.vcNodeContainer.referenceHeight = self.imgFrom.frame.height * 0.90
    }
    
    func loadData(_ conexion: Conexion, routeNumber: Int, arrayMembers: [ConexionReport])
    {
        //Set id
        for node in conexion.nodes where node.isEdge == false
        {
            node.data.id = node.idNode
        }
        
        //Get From and To nodes
        let FROM = conexion.nodes.first!
        let TO = conexion.nodes.last!
        
        //Title
        let route = "\("Route".localized())  \(routeNumber.description)"
        let space = "      "
        let points = "\(conexion.totalWeight) \("Points".localized())"
        let title = route.uppercased() + space + points
        let attributedString = NSMutableAttributedString(string: title)
        attributedString.setColor(route.uppercased(), color: UIColor.grayCloudy(), font: UIFont.boldSystemFont(ofSize: 16))
        
        self.lblTitle.attributedText = attributedString
        
        //Thumbnail
        self.imgFrom.setImageWith(URL(string: FROM.data.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        self.imgTo.setImageWith(URL(string: TO.data.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))

        //Set the connection
        if self.IsConexionReport == false
        {
            self.vcNodeContainer.conexionNodes = conexion.nodes.filter({ $0.isEdge == false && $0.data.id != FROM.data.id && $0.data.id != TO.data.id })
            self.vcNodeContainer.onHiddenConnectionsTap = { [weak self] stakeholders in
                
                self?.onHiddenConnectionsTap?(stakeholders)
            }
        }

        ///////  Previous code: https://definityfirst.atlassian.net/browse/SHM-858
        /*
        
        let from = arrayMembers[0]
        let to = self.IsConexionReport == true ? arrayMembers[1] : ConexionReport(json: nil)
       
        if self.IsConexionReport == false
        {
            //Company Connections
            let userTo = conexion.nodes[conexion.nodes.count - 1]
            to.thumbnailUrl = userTo.data.thumbnailUrl
        }
        
        self.lblTitle.text = "\("Route".localized())  \(routeNumber.description)      \(conexion.totalWeight) \("Points".localized())"
        
        self.imgFrom.setImageWith(URL(string: from.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        self.imgTo.setImageWith(URL(string: to.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        
        //Set the connections for this cell
        self.vcNodeContainer.conexionNodes = conexion.nodes.filter({$0.isEdge == false && $0.idNode != from.id  })
        self.vcNodeContainer.onHiddenConnectionsTap = { [weak self] stakeholders in
            
            self?.onHiddenConnectionsTap?(stakeholders)
        }
        */
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    func detailTapped()
    {
        self.onDetailTapped?(self.tag)
    }
}
