//
//  ConexionsTableViewController.swift
//  SHM
//
//  Created by Definity First on 2/9/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class ConexionsTableViewController: UITableViewController
{
    //MARK: PROPERTIES
    var arrayUsers: [ConexionReport] = [ConexionReport]()
    var showDeleteButtonOnCell = false
    var showDetailButtononCell = false
    var onUsersSelected: ((_ user: ConexionReport) -> ())?
    var userSelected: ConexionReport?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableView.hideEmtpyCells()
        self.tableView.register(UINib(nibName: "MemberSearchTableViewCell", bundle: nil), forCellReuseIdentifier: "MemberSearchTableViewCell")
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    //MARK: PRIVATE METHODS
    func clear()
    {
        self.arrayUsers = []
        self.tableView.reloadData()
    }
    
    //MARK: RELOAD
    func reload()
    {
        self.tableView.dismissBackgroundMessage()
        self.tableView.reloadData()
    }
    
    //MARK: TABLE VIEW DELEGATES
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.arrayUsers.count == 0
        {
            self.tableView.displayBackgroundMessage("No results found".localized(),
                                                    subMessage: "")
        }
        else
        {
            self.tableView.dismissBackgroundMessage()
        }
        
        return self.arrayUsers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MemberSearchTableViewCell") as! MemberSearchTableViewCell
        
        //Color for selected cell
        let selectedColor = UIView()
        selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
        cell.selectedBackgroundView = selectedColor
        
        let user = self.arrayUsers[indexPath.row] as ConexionReport!
        
        cell.lblName.text = user?.fullName
        
        //Position
        //JobPosition/Company
        let strJobPosition = user?.jobPosition?.value ?? ""
        let attJobPosition = NSMutableAttributedString().setJobPositionCompany(strJobPosition, company: user!.companyName)
        cell.lblPosition.attributedText = attJobPosition
        cell.imgProfile.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        cell.imgProfile.layer.borderWidth = 0.5
        
        if user?.isStakeholder == false
        {
            cell.imgCardBadge.image = UIImage(named: "IconIDBlue")
            cell.imgCardBadge.isHidden = false
        }
        else
        {
            cell.imgCardBadge.isHidden = true
        }
        
        if let thumbnailUrl = user?.thumbnailUrl {
            
            cell.imgProfile.setImageWith(URL(string: thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 88
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let userSelected =  arrayUsers[indexPath.row]
        self.onUsersSelected!(userSelected)
    }
}
