//
//  MemberSearchTableViewCell.swift
//  SHM
//
//  Created by Definity First on 2/11/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class MemberSearchTableViewCell: UITableViewCell
{
    //MARK: PROPERTIES
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgCardBadge: UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgDetail: UIImageView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var constraingTrailingView: NSLayoutConstraint!
    @IBOutlet weak var constraingLeadingView: NSLayoutConstraint!
    //MARK: LIFE CYCLE
    override func awakeFromNib()
    {
        super.awakeFromNib()

        self.loadConfig()
    }
    
    //MARK: LOAD CONFIG
    func loadConfig()
    {
        self.lblName.textColor = UIColor.blackAsfalto()
        self.setBorder()
    }
    
    func loadCellWith(_ stakeholder:Stakeholder)
    {
        //Name
        self.lblName.text = stakeholder.fullName
        
        //Job position-Company Name
        let strJobPosition = stakeholder.jobPosition?.value ?? ""
        let strCompanyName = stakeholder.companyName 
        let attJobPosition = NSMutableAttributedString().setJobPositionCompany(strJobPosition, company: strCompanyName)
        self.lblPosition.attributedText = attJobPosition
        self.imgProfile.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        self.imgProfile.layer.borderWidth = 0.5
       
        //Id Card
        if stakeholder.isStakeholder == false
        {
            self.imgCardBadge.image = UIImage(named: "IconIDBlue")
            self.imgCardBadge.isHidden = false
        }
        else
        {
            self.imgCardBadge.isHidden = true
        }
        
        self.imgProfile.setImageWith(URL(string: stakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
    }
    

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: FUNCTIONS
    func showDetailBtn()
    {
        self.imgDetail.isHidden = false
    }
}
