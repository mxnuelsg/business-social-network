//
//  ConexionResultTableViewController.swift
//  SHM
//
//  Created by Definity First on 2/12/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class ConnectionResultTableViewController: UITableViewController
{
    //MARK: PROPERTIES
    var arrayMembers: [ConexionReport] = []
    var arrayConexionsResult: [Conexion] = []
    let lblTitle = UILabel()
    var isConnectionReport: Bool = true
    var showHeader: Bool = true
    var customTitle: String = "STAKEHOLDER RELATIONS MAP".localized()
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.loadData()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator)
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
    }
    
    //MARK: LOAD CONFIG
    func loadConfig()
    {
        self.tableView.hideEmtpyCells()
        self.tableView.separatorColor = UIColor.clear
        
        self.tableView.register(UINib(nibName: "ConnectionCellStakeholderUITableViewCell", bundle: nil), forCellReuseIdentifier: "ConnectionCellStakeholderUITableViewCell")
        self.tableView.register(UINib(nibName: "ConnectionCellMemberUITableViewCell", bundle: nil), forCellReuseIdentifier: "ConnectionCellMemberUITableViewCell")
        self.setupNavigation(animated: false)
        self.tableView.register(UINib(nibName: "ConnectionNodeTableViewCell", bundle: nil), forCellReuseIdentifier: "ConnectionNodeTableViewCell")
        
        self.setupNavigation(animated: false)
        self.title = self.customTitle
        
        //set lblHeaderProperties
        self.lblTitle.text = self.customTitle
        self.lblTitle.backgroundColor = UIColor.whiteColor(0.95)
        self.lblTitle.textColor = UIColor.grayCloudy()
        self.lblTitle.font = UIFont.boldSystemFont(ofSize: 12)
    }
    
    //MARK: LOAD DATA
    func loadData()
    {
        self.tableView.reloadData()
    }
    
    func setupNavigation(animated: Bool)
    {
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        navigationItem.setRightBarButtonItems([barBtnCompose], animated: animated)
    }
    
    // MARK: ACTIONS
    func createNewPost()
    {
        let vcNewPost = NewPostViewController()
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = .overFullScreen
       
        self.present(navController, animated: true, completion: nil)
    }
    
    
    // MARK: TABLE VIEW DELEGATE
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.showHeader == true
        {
            if section == 0
            {
                return self.arrayMembers.count
            }
            else
            {
                return self.arrayConexionsResult.count
            }
        }
        else
        {
            if section == 0
            {
                return 0
            }
            else
            {
                return self.arrayConexionsResult.count
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let user = self.arrayMembers[indexPath.row]
            
            if user.isStakeholder == true
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ConnectionCellStakeholderUITableViewCell", for: indexPath) as! ConnectionCellStakeholderUITableViewCell
                cell.loadStakeholder(user)
                cell.hideDeleteButton()
                cell.selectionStyle = .none
                
                if indexPath.row == 0
                {
                    cell.setBorder()
                    cell.layer.borderColor = UIColor(red: 200/255.0, green: 199/255.0, blue: 204/255.0, alpha: 1.0).cgColor
                }
                
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ConnectionCellMemberUITableViewCell", for: indexPath) as! ConnectionCellMemberUITableViewCell
                cell.loadMember(user)
                cell.hideBtnDelete()
                cell.selectionStyle = .none
                
                if indexPath.row == 0
                {
                    cell.setBorder()
                    cell.layer.borderColor = UIColor(red: 200/255.0, green: 199/255.0, blue: 204/255.0, alpha: 1.0).cgColor
                }
                
                return cell
            }
        }
        else
        {
            let node = self.arrayConexionsResult[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConnectionNodeTableViewCell", for: indexPath) as! ConnectionNodeTableViewCell
            cell.IsConexionReport = self.isConnectionReport
            cell.tag = indexPath.row
            cell.selectionStyle = .none
            cell.loadData(node, routeNumber: indexPath.row + 1, arrayMembers: self.arrayMembers)
            cell.onHiddenConnectionsTap = {[weak self] stakeholders in
                
                print("Showing hidden connetions")
            }
            cell.onDetailTapped = { [weak self] index in
            
                /*
                guard self != nil else {return}
                
                guard let userSelected = self?.arrayConexionsResult[index] else {return}
                
                if DeviceType.IS_ANY_IPAD == true
                {
                    let vcDetail = Storyboard.getInstanceOf(HostConnectionDetailViewController_iPad.self)
                    vcDetail.isConnectionReport = (self?.isConnectionReport)!
                    vcDetail.arrayConexionHeader = (self?.arrayMembers)!
                    vcDetail.arrayConexionResult = userSelected.nodes
                    vcDetail.route = indexPath.row + 1
                    vcDetail.totalNodes = userSelected.totalNodes
                    vcDetail.nodeWeight = userSelected.totalWeight
                    vcDetail.customTitle = (self?.customTitle)!
                    
                    self?.navigationController?.pushViewController(vcDetail, animated: true)
                }
                else
                {
                    let vcDetail = Storyboard.getInstanceOf(ConexionDetailViewController.self)
                    vcDetail.isConnectionReport = (self?.isConnectionReport)!
                    vcDetail.arrayConexionHeader = (self?.arrayMembers)!
                    vcDetail.arrayConexionResult = userSelected.nodes
                    vcDetail.route = indexPath.row + 1
                    vcDetail.totalNodes = userSelected.totalNodes
                    vcDetail.nodeWeight = userSelected.totalWeight
                    vcDetail.customTitle = (self?.customTitle)!
                    
                    self?.navigationController?.pushViewController(vcDetail, animated: true)
                }*/
            }
            let selectedColor = UIView()
            selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
            cell.selectedBackgroundView = selectedColor
            cell.vcNodeContainer.setNeedsDisplay()
            
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            if self.showHeader == true
            {
                let user = self.arrayMembers[indexPath.row]
                
                if user.isStakeholder == true
                {
                    return 177
                }
                else
                {
                    return 108
                }
            }
            else
            {
                return 0
            }
        }
        else
        {
            return 110
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 0
        {
            return CGFloat.leastNormalMagnitude
        }
        else
        {
            return self.tableView.sectionHeaderHeight
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if section == 1
        {
            let lblTitle = UILabel()
            lblTitle.text = "        " + "Connections".localized()
            lblTitle.backgroundColor = UIColor.whiteColor(0.95)
            lblTitle.textColor = UIColor.grayCloudy()
            lblTitle.font = UIFont.boldSystemFont(ofSize: 12)
            
            return lblTitle
        }
        else
        {
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 1
        {
            self.tableView.deselectRow(at: indexPath, animated: true)
            let userSelected = self.arrayConexionsResult[indexPath.row]
            
            if DeviceType.IS_ANY_IPAD == true
            {
                let vcDetail = Storyboard.getInstanceOf(HostConnectionDetailViewController_iPad.self)
                vcDetail.isConnectionReport = self.isConnectionReport
                vcDetail.arrayConexionHeader = self.arrayMembers
                vcDetail.arrayConexionResult = userSelected.nodes
                vcDetail.route = indexPath.row + 1
                vcDetail.totalNodes = userSelected.totalNodes
                vcDetail.nodeWeight = userSelected.totalWeight
                vcDetail.customTitle = self.customTitle
                
                self.navigationController?.pushViewController(vcDetail, animated: true)
            }
            else
            {
                let vcDetail = Storyboard.getInstanceOf(ConexionDetailViewController.self)
                vcDetail.isConnectionReport = self.isConnectionReport
                vcDetail.arrayConexionHeader = self.arrayMembers
                vcDetail.arrayConexionResult = userSelected.nodes
                vcDetail.route = indexPath.row + 1
                vcDetail.totalNodes = userSelected.totalNodes
                vcDetail.nodeWeight = userSelected.totalWeight
                vcDetail.customTitle = self.customTitle
                
                self.navigationController?.pushViewController(vcDetail, animated: true)
            }
        }
    }
}
