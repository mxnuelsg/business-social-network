//
//  ConnectionsReportViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 1/27/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class ConnectionsReportViewController: UIViewController
{
    //MARK: PROPERTIES
    @IBOutlet weak var tblMembers: UITableView!
    @IBOutlet weak var viewInstrucctions: UIView!
    @IBOutlet weak var lblInstrucctions: UILabel!
    @IBOutlet weak var imgInstrucctions: UIImageView!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var tabBarItemCreateReport: UITabBarItem!
    
    @IBOutlet weak var searchBar: AutoSearchBar!
    var firstStep: String = "1. Search and select a contact to start a connection.".localized()
    var secondStep: String = "2. Search and select the contact to make the connection, and see the different routes.".localized()
    var arrayMembers: [ConexionReport] = []
    var vcTableSearch = ConexionsTableViewController()
    var keyboardFrame: CGRect! = CGRect(x: 0, y: 0, width: 320, height: 260)
    var hasPermission: Bool = UserRole.vipUser == LibraryAPI.shared.currentUser?.role || UserRole.globalManager == LibraryAPI.shared.currentUser?.role
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
    }
    
    //MARK: LOADCONFIG
    func loadConfig()
    {
        self.setupNavigation(animated: false)
        self.lblInstrucctions.text = self.firstStep
        
        self.tblMembers.hideEmtpyCells()
        self.tblMembers.register(UINib(nibName: "ConnectionCellStakeholderUITableViewCell", bundle: nil), forCellReuseIdentifier: "ConnectionCellStakeholderUITableViewCell")
        self.tblMembers.register(UINib(nibName: "ConnectionCellMemberUITableViewCell", bundle: nil), forCellReuseIdentifier: "ConnectionCellMemberUITableViewCell")
        self.tblMembers.isScrollEnabled = false
        self.view.addSubview(self.vcTableSearch.view)
        self.vcTableSearch.view.isHidden = true
        self.tblMembers.backgroundColor = UIColor.clear
    
        if self.hasPermission == false
        {
            LibraryAPI.shared.reportsBO.getAccount({ (response) -> () in
                
                if let userResponse = response as ConexionReport! {
                   self.arrayMembers.append(userResponse)
                    self.lblInstrucctions.text = self.secondStep
                    self.imgInstrucctions.image = UIImage(named: "iconSecondStep")
                    self.tblMembers.reloadData()
                }
                
            }) { error in
                
                self.vcTableSearch.dismissBackgroundMessage()
                
                MessageManager.shared.showBar(title: "Error",
                                                                subtitle: "Error.TryAgain".localized(),
                                                                type: .error,
                                                                fromBottom: false)
            }
        }
        
        //creation of SearchController
        self.searchBar.initialize(shouldSearchAfterDelay: true,
                                  onSearchText: { [weak self] text in
                                    
                                    self?.vcTableSearch.view.isHidden = false
                                    self?.searchData(text)
                                    self?.viewInstrucctions.isHidden = true
                                    
            }, onBeginEditing: { [weak self] searchBar in
                
                if self != nil
                {
                    searchBar.inputAccessoryView = self!.loadToolBar()
                    searchBar.backgroundColor = UIColor.groupTableViewBackground
                    searchBar.isMultipleTouchEnabled = false
                    searchBar.isExclusiveTouch = true
                    
                    if searchBar.text?.isEmpty == false
                    {
                        self!.vcTableSearch.view.isHidden = false
                    }
                    else
                    {
                        self!.vcTableSearch.clear()
                    }
                    
                    self?.viewInstrucctions.isHidden = true
                }
                
            }, onEndEditing: { [weak self] searchBar in
                
                if searchBar.text?.trim().isEmpty == true
                {
                    self?.vcTableSearch.view.isHidden = true
                }

                self?.viewInstrucctions.isHidden = false
                
            }, onClearText: { [weak self] in
                
                self?.vcTableSearch.view.isHidden = true
        })
        self.searchBar.onCancel = { [weak self] void in
            
            if self?.searchBar.text?.isEmpty == true
            {
                self?.viewInstrucctions.isHidden = false
            }
            else
            {
                self?.viewInstrucctions.isHidden = true
            }
        }
        self.searchBar.onClearText = { [weak self] void in
            
            self?.viewInstrucctions.isHidden = false
        }
        
        //Localize
        self.localize()
        
        /**
        Keyboard Notification
        */
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        self.vcTableSearch.onUsersSelected =
            { user in
                
                self.vcTableSearch.tableView.isHidden = true
                self.arrayMembers.append(user)
                self.tblMembers.reloadData()
                self.searchBar.text? = ""
                self.searchBar.endEditing(true)
                self.viewInstrucctions.isHidden = false
                
                if self.arrayMembers.count == 0
                {
                    self.lblInstrucctions.text = self.firstStep
                    self.imgInstrucctions.image = UIImage(named: "iconFirstStep")
                    self.viewInstrucctions.isHidden = false
                }
                else if self.arrayMembers.count == 1
                {
                    self.lblInstrucctions.text = self.secondStep
                    self.imgInstrucctions.image = UIImage(named: "iconSecondStep")
                    self.viewInstrucctions.isHidden = false
                }
                else
                {
                    //2 selected
                    self.searchBar.isUserInteractionEnabled = false
                    self.searchBar.isTranslucent = false
                    self.searchBar.searchBarStyle = UISearchBarStyle.minimal
                    self.searchBar.backgroundColor = UIColor.lightGray
                    self.viewInstrucctions.isHidden = true
                }
        }
        
    }
    
    fileprivate func localize()
    {
        if DeviceType.IS_ANY_IPAD == false
        {
            self.title = "CONNECTIONS".localized()
            self.searchBar.placeholder = "Search a contact to make a connection".localized()
            self.tabBarItemCreateReport.title = "Create Report".localized()
        }
    }
    
    func setupNavigation(animated: Bool)
    {
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        self.navigationItem.setRightBarButtonItems([barBtnCompose], animated: animated)
    }
    
    //MARK: TOOLBAR SEARCH BAR
    func loadToolBar() -> UIToolbar
    {
        let toolBarKeyboard = UIToolbar()
        toolBarKeyboard.barStyle = UIBarStyle.blackOpaque;
        toolBarKeyboard.barTintColor = UIColor.colorForNavigationController()

        let barBtnDone = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(self.closeKeyboard))
        barBtnDone.tintColor = UIColor.white

        toolBarKeyboard.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            barBtnDone]

        toolBarKeyboard.sizeToFit()

        return toolBarKeyboard
    }

    func closeKeyboard()
    {
        //        self.vcTableSearch.view.isHidden = true
        self.searchBar.dismissKeyboard()
        self.vcTableSearch.view.frame.size.height = tabBar.frame.origin.y - self.vcTableSearch.view.frame.origin.y
    }
    
    // MARK: ACTIONS
    func createNewPost()
    {
        let vcNewPost = NewPostViewController()
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        
        self.present(navController, animated: true, completion: nil)
    }
    
    
    //MARK: FUNCTIONS
    func searchData(_ searchText: String)
    {
        //calling the webservice to get the members
        self.vcTableSearch.displayBackgroundMessage("Loading...".localized(),
            subMessage: "")
        
        //This is for know, what service are going to be consulted, the first is for accounts and stakeholder, the second is for just stakeholder, in the connection report the rule is, Account/Stakeholder -> Stakeholder
        if arrayMembers.count < 1
        {
            LibraryAPI.shared.reportsBO.getAccounts(searchText, onSuccess: { (response) -> () in
                
                if let userResponse = response as [ConexionReport]! {
                    
                    self.vcTableSearch.arrayUsers = userResponse
                    self.vcTableSearch.reload()
                    if userResponse.count == 0
                    {
                        self.vcTableSearch.displayBackgroundMessage("No results found".localized(),
                                                                    subMessage: "")
                    }
                    else
                    {
                        self.vcTableSearch.dismissBackgroundMessage()
                    }
                }
                
                }) { error in
                    
                    self.vcTableSearch.dismissBackgroundMessage()
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            }
        }
        else
        {
            LibraryAPI.shared.reportsBO.getStakeholders(searchText, onSuccess: { (response) -> () in
                
                if let userResponse = response as [ConexionReport]! {
                    self.vcTableSearch.arrayUsers = userResponse
                    self.vcTableSearch.reload()
                    if userResponse.count == 0
                    {
                        self.vcTableSearch.displayBackgroundMessage("No results found".localized(),
                                                                    subMessage: "")
                    }
                    else
                    {
                        self.vcTableSearch.dismissBackgroundMessage()
                    }
                }
                }) { error in
                    
                    self.vcTableSearch.dismissBackgroundMessage()
                    
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            }
        }
    }
    
    //MARK: REMOVE USER FROM ARRAY
    func removeUserFromArray(_ userToDelete: ConexionReport)
    {
        if let index = self.arrayMembers.index(of: userToDelete) {
            
            // Enabling search bar
            if self.arrayMembers.count == 2
            {
                self.searchBar.isUserInteractionEnabled = true
                self.searchBar.isTranslucent = true
                self.searchBar.searchBarStyle = UISearchBarStyle.default
                self.searchBar.backgroundColor = UIColor.clear
            }
            
            self.arrayMembers.remove(at: index)
            self.tblMembers.reloadData()
            
            if self.arrayMembers.count == 0
            {
                self.lblInstrucctions.text = self.firstStep
                self.imgInstrucctions.image = UIImage(named: "iconFirstStep")
            }
            else
            {
                self.lblInstrucctions.text = self.secondStep
                self.imgInstrucctions.image = UIImage(named: "iconSecondStep")
            }
            
            //show Instructions
            self.viewInstrucctions.isHidden = false
        }
        else
        {
            MessageManager.shared.showBar(title: "Error",
                subtitle: "Error.TryAgain".localized(),
                type: .error,
                fromBottom: false)
        }
    }
    
    // MARK: KEYBOARD NOTIFICATION
    func keyboardWillShow(_ notification: Notification)
    {
        var info = notification.userInfo!
        keyboardFrame = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let rectHeight = keyboardFrame.size.height + self.searchBar.frame.size.height + 50
        self.vcTableSearch.view.frame = CGRect(x: 0,y: searchBar.frame.size.height, width: ScreenSize.SCREEN_WIDTH , height: ScreenSize.SCREEN_HEIGHT - rectHeight)
    }
}

//MARK: TABLE VIEW DELEGATES
extension ConnectionsReportViewController: UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrayMembers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let user = self.arrayMembers[indexPath.row]
        
        if user.isStakeholder == true
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConnectionCellStakeholderUITableViewCell", for: indexPath) as! ConnectionCellStakeholderUITableViewCell
            cell.loadStakeholder(user)
            cell.selectionStyle = .none
            
            cell.onDelete = { (user) in
                
                    self.removeUserFromArray(user)
            }
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConnectionCellMemberUITableViewCell", for: indexPath) as! ConnectionCellMemberUITableViewCell
            cell.loadMember(user)
            cell.selectionStyle = .none
            
            if self.hasPermission == false
            {
                cell.hideBtnDelete()
            }
            
            cell.onDelete = { (user) in
                    self.removeUserFromArray(user)
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let user = self.arrayMembers[indexPath.row]
        
        if user.isStakeholder == true
        {
            return 177
        }
        else
        {
            return 101
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        self.vcTableSearch.view.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return false
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        cell.backgroundColor = UIColor.whiteCloud()
        
        // Remove seperator inset
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset))
        {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))
        {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.responds(to: #selector(setter: UIView.layoutMargins))
        {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
}


// MARK: EXTENSIONS TAB BAR
extension ConnectionsReportViewController: UITabBarDelegate
{
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        //Selected item color equal not selected color
        tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        
        switch item.tag
        {
        case 0:
            
            guard self.arrayMembers.count == 2  else {
                
                MessageManager.shared.showBar(title: "Warning".localized(),
                    subtitle: "Search to make a connection".localized(),
                    type: .warning,
                    fromBottom: false)
                
                return
            }
            
            //Indicator
            MessageManager.shared.showLoadingHUD()
            
            let request = Input()
            let firstNode = self.arrayMembers[0]
            let secondNode = self.arrayMembers[1]
            request.nodeType = firstNode.isStakeholder ? NodeType.stakeholder.rawValue : NodeType.account.rawValue
            request.start = firstNode.isStakeholder ? firstNode.stakeholderId : firstNode.accountId ?? 0
            request.end = secondNode.stakeholderId
            
            LibraryAPI.shared.reportsBO.getStakeholderConnections(request.getWSObject(), onSuccess: { (users) -> () in
                
                MessageManager.shared.hideHUD()
                
                if users.count > 0
                {
                    let vcReportDetail = Storyboard.getInstanceOf(ConnectionResultTableViewController.self)
                    vcReportDetail.arrayMembers = self.arrayMembers
                    vcReportDetail.arrayConexionsResult = users
                    self.navigationController?.pushViewController(vcReportDetail, animated: true)
                }
                else
                {
                    MessageManager.shared.showBar(title: "Warning".localized(),
                        subtitle: "No connections to display".localized(),
                        type: .warning,
                        fromBottom: false)
                }
                
                }, onError: { error in
                    
                    MessageManager.shared.hideHUD()
                    
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            })
            
        default:
            break
        }
    }
}
