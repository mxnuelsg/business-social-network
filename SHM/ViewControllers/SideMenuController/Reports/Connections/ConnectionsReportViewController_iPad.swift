
//
//  ConnectionsReportViewController_Ipad.swift
//  SHM
//
//  Created by Definity First on 2/18/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class ConnectionsReportViewController_iPad: UIViewController
{
    //MARK: PROPERTIES
    @IBOutlet weak var viewInstrucctions: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    var arrayMembers: [ConexionReport] = []
    var vcTableSearch = ConexionsTableViewController()
    var isSearchingStakeholders: Bool = true
    var layout: UICollectionViewFlowLayout!
    var hasPermission: Bool = UserRole.vipUser == LibraryAPI.shared.currentUser?.role || UserRole.globalManager == LibraryAPI.shared.currentUser?.role
    @IBOutlet weak var lblInstrucctionsConnection: UILabel!
    @IBOutlet weak var constraintLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintLeadingSuperView: NSLayoutConstraint!
    
    @IBOutlet weak var searchBarFrom: AutoSearchBar!
    @IBOutlet weak var searchBarTo: AutoSearchBar!
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupNavigation(animated: false)
        self.title = "CONNECTIONS".localized()
        self.vcTableSearch.modalPresentationStyle = .popover
        self.preferredContentSize = CGSize(width: 280, height: 280)
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.backButtonArrow()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: nil, completion: { (context) in
            self.loadLayoutForCollectionView()
            
            if self.hasPermission == false
            {
                guard self.constraintLeading != nil && self.constraintLeadingSuperView != nil
                else
                {
                    return
                }
                
                self.constraintLeading.constant =  (-self.searchBarFrom.frame.width * 2)
                self.constraintLeadingSuperView.constant = 0
            }
        })
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(true)
    }
    
    //MARK: LOAD CONFIG
    fileprivate func loadConfig()
    {
        //creation of searchControllers
        
        //searchBarFrom
        searchBarFrom.initialize(shouldSearchAfterDelay: true,
        onSearchText: { [weak self] (text) in
            self?.searchData(text)
            
        }, onBeginEditing: { [weak self] (searchBar) in
            if (self != nil)
            {
                self!.initSearchBar(searchBar)
                if (searchBar.text?.isEmpty == false)
                {
                    self!.searchData(searchBar.text!)
                }
            }
            
        }, onEndEditing: { [weak self] (searchBar) in
            if searchBar.text?.trim().isEmpty == true
            {
                self?.dismissPopUp()
            }
            
        }, onClearText: { [weak self] in
            self?.dismissPopUp()
        })
        
        //searchBarTo
        searchBarTo.initialize(shouldSearchAfterDelay: true,
        onSearchText: { [weak self] (text) in
            self?.searchDataStakeholder(text)
            
        }, onBeginEditing: { [weak self] (searchBar) in
            if (self != nil)
            {
                self!.initSearchBar(searchBar)
                if (searchBar.text?.isEmpty == false)
                {
                    self!.searchDataStakeholder(searchBar.text!)
                }
            }
            
        }, onEndEditing: { [weak self] (searchBar) in
            if searchBar.text?.trim().isEmpty == true
            {
                self?.dismissPopUp()
            }            
            
        }, onClearText: { [weak self] in
            self?.dismissPopUp()
        })
        
        
        self.localize()
        if self.hasPermission == false
        {
            LibraryAPI.shared.reportsBO.getAccount({ (response) -> () in
                
                if let userResponse = response as ConexionReport! {
                    self.arrayMembers.append(userResponse)
                    self.collectionView.reloadData()
                    self.viewInstrucctions.isHidden = true
                    self.searchBarFrom.isHidden = true
                    self.searchBarFrom.isUserInteractionEnabled = false
                    self.constraintLeading.constant = (self.searchBarFrom.frame.width * 2)
                    self.constraintLeadingSuperView.constant = 0
                }
                
            }) { error in
                
                MessageManager.shared.showBar(title: "Error",
                                                                subtitle: "Error.TryAgain".localized(),
                                                                type: .error,
                                                                fromBottom: false)
            }
        }
        else
        {
            NSLayoutConstraint.deactivate([self.constraintLeadingSuperView])
        }
        
        self.vcTableSearch.onUsersSelected = { (user) in
            
                self.viewInstrucctions.isHidden = true
                self.dismissPopUp()
                self.vcTableSearch.clear()
                
                if self.isSearchingStakeholders == true
                {
                    self.searchBarTo.text? = ""
                    self.searchBarTo.endEditing(true)
                    
                    if self.arrayMembers.count == 0
                    {
                        self.arrayMembers.append(ConexionReport(json: nil))
                        self.arrayMembers.append(user)
                    }
                    else if self.arrayMembers.count == 2
                    {
                        self.arrayMembers[1] = user
                    }
                    else
                    {
                        self.arrayMembers.append(user)
                    }
                    
                    self.searchBarTo.isUserInteractionEnabled = false
                    self.searchBarTo.isTranslucent = false
                    self.searchBarTo.searchBarStyle = UISearchBarStyle.minimal
                    self.searchBarTo.backgroundColor = UIColor.lightGray
                }
                else
                {
                    self.searchBarFrom.text? = ""
                    self.searchBarFrom.endEditing(true)
                    
                    if self.arrayMembers.count == 0
                    {
                        self.arrayMembers.append(user)
                    }
                    else
                    {
                        self.arrayMembers[0] = user
                    }
                    
                    self.searchBarFrom.isUserInteractionEnabled = false
                    self.searchBarFrom.isTranslucent = false
                    self.searchBarFrom.searchBarStyle = UISearchBarStyle.minimal
                    self.searchBarFrom.backgroundColor = UIColor.lightGray
                }
                
                self.collectionView.reloadData()
                
                if self.arrayMembers.count == 2 && self.arrayMembers.filter({ $0.fullName.trim().isEmpty == true }).count == 0
                {
                    self.goToResult()
                }
        }
        
        self.loadConfigCollectionView(self.collectionView)
    }
    
    func initSearchBar(_ searchBar: UISearchBar) {
        searchBar.inputAccessoryView = loadToolBar()
        searchBar.backgroundColor = UIColor.groupTableViewBackground
        searchBar.isMultipleTouchEnabled = false
        searchBar.isExclusiveTouch = true

//        if searchBar.text?.isEmpty == false
//        {
//            self.vcTableSearch.clear()
//            self.presentPopOver(searchBar.frame, sourceView: self.view)
//        }
//        else
//        {
//            self.vcTableSearch.clear()
//        }
    }
    
    func loadToolBar() -> UIToolbar
    {
        let toolBarKeyboard = UIToolbar()
        toolBarKeyboard.barStyle = UIBarStyle.blackOpaque;
        toolBarKeyboard.barTintColor = UIColor.colorForNavigationController()

        let barBtnDone = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action:#selector(ConnectionsReportViewController_iPad.closeKeyboard))
        barBtnDone.tintColor = UIColor.white

        toolBarKeyboard.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            barBtnDone]

        toolBarKeyboard.sizeToFit()
        return toolBarKeyboard
    }

    func closeKeyboard()
    {
        view.endEditing(true)
    }
    
    fileprivate func localize()
    {
        
        self.searchBarTo.placeholder = "Quick search of stakeholder".localized()
        self.searchBarFrom.placeholder = "Quick search of account or stakeholder".localized()
        self.lblInstrucctionsConnection.text = "Search and select the contacts to see their connections.".localized()
    }
    
    func loadConfigCollectionView(_ collectionView: UICollectionView)
    {
        //LOAD CONFIG
        self.layout = UICollectionViewFlowLayout()
        self.loadLayoutForCollectionView()
        
        collectionView.collectionViewLayout = self.layout
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.allowsMultipleSelection = false
        collectionView.isScrollEnabled = false
        //registering cells
        collectionView.register(UINib(nibName: "StakeholderCollectionViewCell_iPad", bundle: nil), forCellWithReuseIdentifier: "StakeholderCollectionViewCell_iPad")
        collectionView.register(UINib(nibName: "MemberCollectionViewCell_iPad", bundle: nil), forCellWithReuseIdentifier: "MemberCollectionViewCell_iPad")
        collectionView.register(UINib(nibName: "EmptyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "EmptyCollectionViewCell")
        collectionView.backgroundColor = UIColor.clear
        collectionView.reloadData()
    }
    
    //MARK: SETUP NAVIGATION
    func setupNavigation(animated: Bool)
    {
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(ConnectionsReportViewController_iPad.createNewPost))
        self.navigationItem.setRightBarButtonItems([barBtnCompose], animated: animated)
    }
    
    // MARK: ACTIONS
    func createNewPost()
    {
        let vcNewPost = NewPostModal()
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
        
        self.present(navController, animated: true, completion: nil)
    }
    
    //MARK: FUNCTIONS
    func searchDataStakeholder(_ searchText: String)
    {
        //indicator
        self.vcTableSearch.arrayUsers.removeAll()
        self.vcTableSearch.tableView.reloadData()
        self.vcTableSearch.displayBackgroundMessage("Loading...".localized(),
                                             subMessage: "")

        LibraryAPI.shared.reportsBO.getStakeholders(searchText, onSuccess: { (response) -> () in
            
            if let userResponse = response as [ConexionReport]! {
                
                self.vcTableSearch.dismissBackgroundMessage()

                self.isSearchingStakeholders = true
                let popOverController = self.presentPopOver(self.searchBarTo.frame, sourceView: self.view)
                
                self.vcTableSearch.arrayUsers = userResponse
                self.vcTableSearch.reload()
                
                if userResponse.count == 0
                {
                    popOverController?.backgroundColor = UIColor.clear
                }
            }
            
            }) { error in
                
                self.dismissPopUp()
                
                MessageManager.shared.showBar(title: "Error",
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
        }
    }
    
    //MARK: PresentPopover
    @discardableResult func presentPopOver(_ sourceRect: CGRect, sourceView: UIView) ->(UIPopoverPresentationController?)
    {
        if self.vcTableSearch.view.window == nil
        {
            self.present(self.vcTableSearch, animated: true, completion: nil)
        }
        
        let popOverController = self.vcTableSearch.popoverPresentationController
        popOverController?.sourceRect = sourceRect
        popOverController?.sourceView = sourceView
        popOverController?.permittedArrowDirections = .any
        
        return popOverController
    }
    
    func dismissPopUp()
    {
        self.vcTableSearch.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Remove user from array
    func removeUserFromArray(_ userToDelete: ConexionReport)
    {
        if let index = self.arrayMembers.index(of: userToDelete) {
            
            if index == 0
            {
                self.searchBarFrom.isUserInteractionEnabled = true
                self.searchBarFrom.isTranslucent = true
                self.searchBarFrom.searchBarStyle = UISearchBarStyle.default
                self.searchBarFrom.backgroundColor = UIColor.clear
            }
            else
            {
                self.searchBarTo.isUserInteractionEnabled = true
                self.searchBarTo.isTranslucent = true
                self.searchBarTo.searchBarStyle = UISearchBarStyle.default
                self.searchBarTo.backgroundColor = UIColor.clear
            }
            
            self.arrayMembers[index] = ConexionReport(json: nil)
            self.collectionView.reloadData()
            
            let numberOfEmptyMembers = self.arrayMembers.filter({$0.fullName == ""}).count
            
            if  numberOfEmptyMembers == 2 || (numberOfEmptyMembers == 1 && self.arrayMembers.count == 1)
            {
                self.viewInstrucctions.isHidden = false
            }
        }
        else
        {
            MessageManager.shared.showBar(title: "Error",
                subtitle: "Error.TryAgain".localized(),
                type: .error,
                fromBottom: false)
        }
    }
    
    func goToResult()
    {
        guard self.arrayMembers.count == 2  && self.arrayMembers.filter({$0.fullName == "" }).count == 0 else {
            MessageManager.shared.showBar(title: "Warning".localized(),
                subtitle: "Search to make a connection".localized(),
                type: .warning,
                fromBottom: false)
            return
        }
        
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        
        let request = Input()
        let firstNode = self.arrayMembers[0]
        let secondNode = self.arrayMembers[1]
        request.nodeType = firstNode.isStakeholder ? NodeType.stakeholder.rawValue : NodeType.account.rawValue
        request.start = firstNode.isStakeholder ? firstNode.stakeholderId : firstNode.accountId ?? 0
        request.end = secondNode.stakeholderId
        
        LibraryAPI.shared.reportsBO.getStakeholderConnections(request.getWSObject(), onSuccess: { (users) -> () in
            
            MessageManager.shared.hideHUD()
            
            if users.count > 0
            {
                let vcDetail =  UIStoryboard(name: "MainPad", bundle: nil).instantiateViewController(withIdentifier: "HostConnectionViewController_iPad") as! HostConnectionViewController_iPad
                
                vcDetail.arrayMembers = self.arrayMembers
                vcDetail.arrayConexionResult = users
                vcDetail.customTitle = self.title!
                self.navigationController?.pushViewController(vcDetail, animated: true)
            }
            else
            {
                MessageManager.shared.showBar(title: "Warning".localized(),
                    subtitle: "No connections to display".localized(),
                    type: .warning,
                    fromBottom: false)
            }
            
            }, onError: { error in
                
                MessageManager.shared.hideHUD()
                
                MessageManager.shared.showBar(title: "Error",
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
        })
    }
    
    //MARK: LAYOUT FOR COLLECTION VIEW
    func loadLayoutForCollectionView()
    {
        self.layout.scrollDirection = UICollectionViewScrollDirection.vertical
        self.layout.minimumInteritemSpacing = 1
        self.layout.minimumLineSpacing = 1
        self.layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.layout.itemSize = CGSize(width: (self.view.frame.width / 2) - 2, height: 200)
    }
    
    func searchData(_ searchText: String)
    {
        //indicator
        self.vcTableSearch.arrayUsers.removeAll()
        self.vcTableSearch.tableView.reloadData()
        self.vcTableSearch.displayBackgroundMessage("Loading...".localized(),
                                                    subMessage: "")
        
        LibraryAPI.shared.reportsBO.getAccounts(searchText, onSuccess: { (response) -> () in
            
            if let userResponse = response as [ConexionReport]! {
                
                self.vcTableSearch.dismissBackgroundMessage()

                self.isSearchingStakeholders = false
                self.vcTableSearch.arrayUsers = userResponse
                self.vcTableSearch.reload()
                self.presentPopOver(self.searchBarFrom.frame, sourceView: self.view)
            }
            }) { error in
                
                self.dismissPopUp()
                
                MessageManager.shared.showBar(title: "Error",
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        super.prepare(for: segue, sender: sender)
    }
}

//MARK: COLLECTION VIEW DELEGATE
extension ConnectionsReportViewController_iPad : UICollectionViewDelegate, UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.arrayMembers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let member = self.arrayMembers[indexPath.row]
        
        if member.fullName.trim().isEmpty == false
        {
            if member.isStakeholder == true
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StakeholderCollectionViewCell_iPad", for: indexPath) as! StakeholderCollectionViewCell_iPad
                cell.loadStakeholder(member)
                cell.onDelete = { (user) in
                    
                    self.removeUserFromArray(user)
                }
                
                return cell
            }
            else
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MemberCollectionViewCell_iPad", for: indexPath) as! MemberCollectionViewCell_iPad
                cell.loadMember(member)
                
                if self.hasPermission == false
                {
                    cell.hideBtnDelete()
                }
                
                cell.onDelete = { (user) in
                    
                    self.removeUserFromArray(user)
                }
                
                return cell
            }
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyCollectionViewCell", for: indexPath) as! EmptyCollectionViewCell
            cell.setBorder()
            cell.layer.borderColor = UIColor.clear.cgColor
            cell.layer.backgroundColor = UIColor.clear.cgColor
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}
