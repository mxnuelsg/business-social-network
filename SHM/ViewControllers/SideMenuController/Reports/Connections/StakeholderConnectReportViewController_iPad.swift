//
//  StakeholderConnectReportViewController_Ipad.swift
//  SHM
//
//  Created by Definity First on 2/18/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class StakeholderConnectReportViewController_iPad: ConnectionsReportViewController_iPad
{
    //MARK: PROPERTIES
    @IBOutlet weak var collectionViewStakeholder: UICollectionView!
    @IBOutlet weak var searchBar: AutoSearchBar!
    @IBOutlet weak var lblInstrucctions: UILabel!
    @IBOutlet weak var viewInstrucctionsStakeholder: UIView!
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func loadLayoutForCollectionView()
    {
        self.layout.scrollDirection = UICollectionViewScrollDirection.vertical
        self.layout.minimumInteritemSpacing = 1
        self.layout.minimumLineSpacing = 1
        self.layout.sectionInset = UIEdgeInsets(top: 0, left: 200, bottom: 0, right: 200)
        self.layout.itemSize = CGSize(width: (self.view.frame.width) - 2, height: 200)
    }
    
    //MARK: LOAD CONFIG
    fileprivate func loadConfig()
    {
        //searchBarFrom
        searchBar.initialize(shouldSearchAfterDelay: true,
            onSearchText: { [weak self] (text) in
                self?.searchData(text)
                                    
            }, onBeginEditing: { [weak self] (searchBar) in
                if (self != nil)
                {
                    self!.initSearchBar(searchBar)
                    if (searchBar.text?.isEmpty == false)
                    {
                        self!.searchData(searchBar.text!)
                    }
                }
                
            }, onEndEditing: { [weak self] (searchBar) in
                if searchBar.text?.trim().isEmpty == true
                {
                    self?.dismissPopUp()
                }
                
            }, onClearText: { [weak self] in
                self?.dismissPopUp()
        })
        
        self.searchBar.placeholder = "Search and select a stakeholder to make a connection".localized()
        self.lblInstrucctions.text = "Select a stakeholder. Different routes, from lowest to highest score to reach the stakeholder will be shown. This from site users".localized()
        self.title = "COMPANY CONNECTIONS".localized()
        
        self.vcTableSearch.onUsersSelected = { (user) in
            
            self.viewInstrucctionsStakeholder.isHidden = true
            self.dismissPopUp()
            self.vcTableSearch.clear()
            self.searchBar.text = ""
            self.searchBar.endEditing(true)
            
            self.searchBar.isUserInteractionEnabled = false
            self.searchBar.isTranslucent = false
            self.searchBar.searchBarStyle = UISearchBarStyle.minimal
            self.searchBar.backgroundColor = UIColor.lightGray
            
            self.arrayMembers.append(user)
            self.collectionViewStakeholder.reloadData()
            
            if self.arrayMembers.count == 1
            {
                self.goToResult()
                print("gotoResults")
            }
        }
        
        self.loadConfigCollectionView(self.collectionViewStakeholder)
        
        self.vcTableSearch.modalPresentationStyle = .popover
        self.preferredContentSize = CGSize(width: 280, height: 280)
    }
    
    //MARK: SEARCH DATA
    override func searchData(_ searchText: String)
    {
        self.vcTableSearch.clear()
        self.vcTableSearch.displayBackgroundMessage("Loading...".localized(),
                                                    subMessage: "")
        LibraryAPI.shared.reportsBO.getStakeholders(searchText, onSuccess: { (response) -> () in
            
            if let userResponse = response as [ConexionReport]! {
                let popOver = self.presentPopOver(self.searchBar.frame, sourceView: self.view)
                self.vcTableSearch.arrayUsers = userResponse
                self.vcTableSearch.reload()
                self.vcTableSearch.dismissBackgroundMessage()
                
                if userResponse.count == 0
                {
                    popOver?.backgroundColor = UIColor.clear
                    self.vcTableSearch.displayBackgroundMessage("No results".localized(),
                                                                subMessage: "")
                }
            }
            
            }) { (error) -> ()  in
                
                self.vcTableSearch.dismissBackgroundMessage()
                self.dismissPopUp()
                
                MessageManager.shared.showBar(title: "Error".localized(),
                    subtitle:"Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
        }
    }
    
    //MARK: GOTORESULTS
    override func goToResult()
    {
        guard self.arrayMembers.count == 1 else {
            
            return
        }
        
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        let request = Input()
        let firstNode = self.arrayMembers[0]
        request.nodeType = 0
        request.start = firstNode.stakeholderId
        request.end = 0
        
        LibraryAPI.shared.reportsBO.getStakeholderConnections(request.getWSObject(), onSuccess: { (users) -> () in
            
            MessageManager.shared.hideHUD()
            
            if users.count > 0
            {
                let vcDetail =  UIStoryboard(name: "MainPad", bundle: nil).instantiateViewController(withIdentifier: "HostConnectionViewController_iPad") as! HostConnectionViewController_iPad
                
                vcDetail.arrayMembers = self.arrayMembers
                vcDetail.isConnectionReport = false
                vcDetail.arrayConexionResult = users
                vcDetail.customTitle = self.title!
                self.navigationController?.pushViewController(vcDetail, animated: true)
            }
            else
            {
                MessageManager.shared.showBar(title: "Warning".localized(),
                    subtitle: "No connections to display".localized(),
                    type: .warning,
                    fromBottom: false)
            }
            
            }, onError: { error in
                
                MessageManager.shared.hideHUD()
                
                MessageManager.shared.showBar(title: "Error".localized(),
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
        })
    }
    
    //MARK: REMOVE FROM ARRAY
    override func removeUserFromArray(_ userToDelete: ConexionReport)
    {
        if let index = self.arrayMembers.index(of: userToDelete) {
            self.searchBar.isUserInteractionEnabled = true
            self.searchBar.isTranslucent = true
            self.searchBar.searchBarStyle = UISearchBarStyle.default
            self.searchBar.backgroundColor = UIColor.clear
            
            self.arrayMembers.remove(at: index)
            self.collectionViewStakeholder.reloadData()
            
            if arrayMembers.count == 0
            {
                self.viewInstrucctionsStakeholder.isHidden = false
            }
        }
        else
        {
            MessageManager.shared.showBar(title: "Error".localized(),
                                                            subtitle: "Error.TryAgain".localized(),
                                                            type: .error,
                                                            fromBottom: false)
        }
    }
}
