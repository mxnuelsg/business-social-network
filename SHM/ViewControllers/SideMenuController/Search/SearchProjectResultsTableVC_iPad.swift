//
//  SearchProjectResultsTableVC_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 1/8/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class SearchProjectResultsTableVC_iPad: UITableViewController
{
    // MARK: OUTLETS AND PROPERTIES
    var arrayProjects = [Project]()
    var projectDidChange:(() -> ())?
    
    // MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    //MARK: LOAD CONFIG
    fileprivate func loadConfig()
    {
        self.tableView.register(UINib(nibName: "ProjectTableCell", bundle: nil), forCellReuseIdentifier: "ProjectTableCell")
    }

    // MARK: PRIVATE FUNCTIONS
    func pushActor(_ stakeholderSelected:Stakeholder)
    {
        let vcStakeholderDetail = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
        vcStakeholderDetail.stakeholder = stakeholderSelected
        navigationController!.pushViewController(vcStakeholderDetail, animated: true)
    }
    


    // MARK: Table view data source and delegate
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        guard self.arrayProjects.count > 0 else {
            
            return 0
        }
        return self.arrayProjects.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProjectTableCell") as! ProjectTableCell
        
        // Configure the cell...
        guard self.arrayProjects.count > 0 else {
            
            return cell
        }
        
        let project = self.arrayProjects[indexPath.row]
        cell.cellForProject(cell, project: project)
        
        // Define tap actions
        cell.onStakeholderTap = { [weak self] stakeholder in
            
            //Enabled or Disabled
            guard stakeholder.isEnable == true else {
                
                MessageManager.shared.showBar(title: stakeholder.fullName, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                
                return
            }
            
            self?.pushActor(stakeholder)
        }
        
        cell.onAllStakeholdersTap = { [weak self] Void in
            
            let stakeholdersTableVC = StakeholdersTableViewController.getNewInstance()
            stakeholdersTableVC.stakeholders = project.stakeholders
            
            let vcModal = ModalViewController(subViewController: stakeholdersTableVC)
            self?.present(vcModal, animated:true, completion: nil)
        }
        
        cell.onFilesTap = { [weak self] Void in
            
            let vcFileList = FileListViewController(attachments: project.files)
            let vcModal = ModalViewController(subViewController: vcFileList)
            vcModal.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            vcModal.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            
            self?.present(vcModal, animated:true, completion: nil)
        }
        
        cell.onTextViewTap = { [weak self] Void in
            
            if let projectSelected = self?.arrayProjects[indexPath.row] {
                
                let vcProjectDetail = Storyboard.getInstanceOf(ProjectDetailViewController_iPad.self)
                vcProjectDetail.project = projectSelected
                
                self?.navigationController?.pushViewController(vcProjectDetail, animated: true)
            }
        }
        
        return cell
    }


    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 129
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let cellToDisplay = cell as! ProjectTableCell
        cellToDisplay.layoutConfForCell(cellToDisplay)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let projectSelected = arrayProjects[indexPath.row]
        let vcProjectDetail = Storyboard.getInstanceOf(ProjectDetailViewController_iPad.self)
        vcProjectDetail.project = projectSelected

        self.navigationController?.pushViewController(vcProjectDetail, animated: true)
    }
    
     override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
     {
        let selectedProject = self.arrayProjects[indexPath.row]
        
        //POST ACTION
        let actionPost = UITableViewRowAction(style: .normal, title: "Post".localized(), handler: {(action, indexPath) -> () in
            tableView.setEditing(false, animated: true)
            
            let vcNewPost = NewPostViewController()
            vcNewPost.projectTagged = selectedProject
            
            let navController = NavyController(rootViewController: vcNewPost)
            navController.modalPresentationStyle = (DeviceType.IS_ANY_IPAD) ?  UIModalPresentationStyle.formSheet :  UIModalPresentationStyle.overFullScreen

            self.present(navController, animated: true, completion: nil)})
        actionPost.backgroundColor = UIColor.blueColorNewPost()
        
        //FOLLOW/UNFOLLOW
        if selectedProject.followingStatus == true
        {
            //Following
            let unfollowAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Unfollow".localized(), handler: { (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
                
                let alertUnfollowConfirmed = UIAlertController(title: "Would you like to stop following this project?".localized(), message: selectedProject.title, preferredStyle: UIAlertControllerStyle.alert)
                
                let unfollow = UIAlertAction(title: "Unfollow".localized(), style: UIAlertActionStyle.destructive, handler: {
                    (action) in
                    
                    LibraryAPI.shared.projectBO.unfollowProject(selectedProject.id, onSuccess: {
                        (success) -> () in
                        
                        let delayTimePrincipal = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                        DispatchQueue.main.asyncAfter(deadline: delayTimePrincipal) {
                            
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadProjectList"), object: nil)
                            
                            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                                
                                let info = "Now, you're not following:".localized() + " " + selectedProject.title
                                
                                MessageManager.shared.showBar(title: "Info".localized(),
                                                              subtitle: info,
                                                              type: .info,
                                                              fromBottom: false)
                                
                                self.projectDidChange?()
                            }
                        }
                        }, onError: { error in
                            
                            MessageManager.shared.showBar(title: "Error".localized(),
                                                          subtitle: "Error.TryAgain".localized(),
                                                          type: .error,
                                                          fromBottom: false)
                    })
                    
                    self.tableView.setEditing(false, animated: true)
                })
                
                let actionCancel = UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler: nil)
                
                alertUnfollowConfirmed.addAction(unfollow)
                alertUnfollowConfirmed.addAction(actionCancel)
                
                self.present(alertUnfollowConfirmed, animated: true, completion: nil)
            })
            
            unfollowAction.backgroundColor = UIColor.redUnfollowAction()
            
            return [unfollowAction, actionPost]
        }
        else
        {
            //Not following
            let followAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Follow".localized(), handler: { (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
                
                let alertFollowConfirmed = UIAlertController(title: "Would you like to follow this project?".localized(), message: selectedProject.title, preferredStyle: UIAlertControllerStyle.alert)
                
                let follow = UIAlertAction(title: "Follow".localized(), style: UIAlertActionStyle.destructive, handler: {
                    (action) in
                    
                    LibraryAPI.shared.projectBO.followProject(selectedProject.id, onSuccess: {
                        (success) -> () in
                        
                        //                            self.projectsFiltered.removeAtIndex(indexPath.row)
                        //                            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                        
                        let delayTimePrincipal = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                        DispatchQueue.main.asyncAfter(deadline: delayTimePrincipal) {
                            
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadProjectList"), object: nil)
                            
                            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                                
                                let info = "Now, you're following:".localized() + " " + selectedProject.title
                                
                                MessageManager.shared.showBar(title: "Success".localized(),
                                    subtitle: info,
                                    type: .success,
                                    fromBottom: false)
                                
                                self.projectDidChange?()
                            }
                        }
                        }, onError: { error in
                            
                            MessageManager.shared.showBar(title: "Error".localized(),
                                                          subtitle: "Error.TryAgain".localized(),
                                                          type: .error,
                                                          fromBottom: false)
                            
                    })
                    
                    self.tableView.setEditing(false, animated: true)
                })
                
                let actionCancel = UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler: nil)
                
                alertFollowConfirmed.addAction(follow)
                alertFollowConfirmed.addAction(actionCancel)
                
                self.present(alertFollowConfirmed, animated: true, completion: nil)
            })
            
            followAction.backgroundColor = UIColor.greenFollowAction()
            
            return [followAction, actionPost]

        }
     }
    
    //MARK: ScrollView delegate
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if bottomEdge >= scrollView.contentSize.height
        {
            guard let parent = parent as? GlobalSearchViewController_iPad else {
                
                return
            }
            
            if let pageNumberInt = parent.projectPageNumber, let totalInt = parent.results?.projectsTotal {
                
                let pageNumberFloat = Float(pageNumberInt)
                let totalFloat = Float(totalInt)
                
                if pageNumberFloat <= (totalFloat/20)
                {
                    print("page=\(pageNumberFloat) total=\((totalFloat/20))")
                    parent.projectPageNumber = pageNumberInt+1
                    parent.updateType = GlobalSearchViewController_iPad.updateResultsOfType.projects
                    parent.search()
                }
            }
        }
    }
}
