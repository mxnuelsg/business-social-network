//
//  SearchStakeHoldersTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 31/08/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import Spruce

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


enum SectionState: Int
{
    case following = 0
    case notFollowing = 1
}

class ContactSearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var searchBar: AutoSearchBar!
    @IBOutlet weak var tvResults: UITableView!
    
    var members: [Member]! = []
    var stakeholders: [Stakeholder]! = []
    
    var addContactToInstance: ((_ contactAdded: AnyObject) -> ())?
    var isSearchingSH = false
    var isModalView = true
    var onModalViewControllerClosed : (() -> ())?
    var onFollowOrUnfollow : (() -> ())?
    
    //Parameters used for search members
    var stakeholderIdSearchParameter:Int?
    var projectIdSearchParameter:Int?
    var siteIdSearchParameter:Int?
    
    //This object arrange stakeholder by site region and follow status
    var stakeholdersBySite : StakeholderInOrder?
    
    //Properties for New Post
    var willBeShowedInNewPost = false
    @IBOutlet weak var constraintTableTop: NSLayoutConstraint!
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
        
        if let indexPaths = self.tvResults.indexPathsForSelectedRows {
            
            for indexPath in indexPaths
            {
                self.tvResults.deselectRow(at: indexPath, animated: true)
            }
        }
        
        self.tvResults.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: LOCALIZATION
    fileprivate func localize()
    {
       self.searchBar.placeholder = "Enter a Name".localized()
    }
    
    //MARK: LOAD CONFIG
    fileprivate func loadConfig()
    {
        //initialize the AutoSearchBar
        searchBar.initialize(shouldSearchAfterDelay: true,
        onSearchText: { [weak self] (text) in
            if (self != nil) {
                self!.members.removeAll(keepingCapacity: false)
                self!.stakeholders.removeAll()
                self!.stakeholdersBySite?.clear()
                
                let term = text.trimmingCharacters(in: CharacterSet.whitespaces)
                if term.isEmpty == false
                {
                    self!.searchContact(term)
                }
            }
            
        }, onBeginEditing: { (searchBar) in
//            searchBar.setShowsCancelButton(true, animated: true)
//            searchBar.tintColor = .white
			searchBar.tintColor = DeviceType.IS_ANY_IPAD == true ? .black : .white
            
//        }, onEndEditing: { [weak self] (searchBar) in
//            self?.cancelSearch()
            
        }, onClearText: { [weak self] in
            self?.cancelSearch()
            
        }, onCancel: { [weak self] in
            self?.cancelSearch()
        })
        
        self.localize()

        self.tvResults.register(UINib(nibName: "ContactListCell", bundle: nil), forCellReuseIdentifier: "ContactListCell")
        
        if willBeShowedInNewPost == false
        {
            self.searchBar.becomeFirstResponder()
        }
        
        self.searchBar.showsCancelButton = false
        self.searchBar.tintColor = UIColor.colorForNavigationController()
        
        
        //Just in case: New Post
        if self.willBeShowedInNewPost == true
        {
            self.searchBar.isHidden = self.willBeShowedInNewPost
            self.constraintTableTop.constant = -44
        }
        
        self.clearState()
        
        if self.presentingViewController != nil
        {
            if self.isModalView == true
            {
                let barBtnClose = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.stop, target: self, action: #selector(self.close))
                navigationItem.leftBarButtonItem = barBtnClose
            }
        }
    }
    
    //MARK: ACTIONS
    fileprivate func cancelSearch() {
        self.searchBar.text = String()
        self.clearState()
        self.tvResults.reloadData()
    }
    
    func clearState()
    {
        if self.isSearchingSH == true
        {
            self.title = "STAKEHOLDER LIST".localized()
            self.stakeholders.removeAll()
            self.stakeholdersBySite?.clear()
            self.tvResults.displayBackgroundMessage("No stakeholders found".localized(),
                subMessage: "")
        }
        else
        {
            self.title = "MEMBER LIST".localized()
            self.members = []
        }
    }
    
    func close()
    {
        self.searchBar.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadStakeholders(_ stakeholders:[Stakeholder])
    {
        self.stakeholders = stakeholders
        self.stakeholdersBySite = StakeholderInOrder(stakeholders:stakeholders)
        if self.stakeholdersBySite?.isEmpty() == true
        {
            if self.tvResults != nil
            {
                self.tvResults.displayBackgroundMessage("No results found".localized(),
                                                        subMessage: "")
            }
        }
        else
        {
            if self.tvResults != nil
            {
                self.tvResults.dismissBackgroundMessage()
            }
        }
    }
    
    //MARK: LOAD STAKEHOLDERS
    func searchContact(_ term: String)
    {
        if self.isSearchingSH == true
        {
            self.tvResults.displayBackgroundMessage("Searching...".localized(),
                subMessage: "")
            
            LibraryAPI.shared.stakeholderBO.getStakeholders(parameters: [
                "FollowingStatus" : FollowStatus.all.rawValue,
                "SearchText" : searchBar.text!,
                "SearchByText" : "true",
                "PageSize" : 20,
                "PageNumber" : 1,
                "Pagination" : "false"
                ],
                onSuccess:{ stakeholders, sites, totalRecords in
                    
                    self.tvResults.dismissBackgroundMessage()
                    
                    self.loadStakeholders(stakeholders)
                    self.tvResults.reloadData()
                    
                    //Spruce framework Animation
                    DispatchQueue.main.async {
                        
                        self.tvResults.spruce.animate([.slide(.left, .severely), .fadeIn], animationType: SpringAnimation(duration: 0.8))
                    }

                }) { error in
                    
                    self.tvResults.dismissBackgroundMessage()
                    self.tvResults.reloadData()
            }
        }
        else
        {
            self.tvResults.displayBackgroundMessage("Searching...".localized(),
                subMessage: "")
            
            LibraryAPI.shared.memberBO.searchMembers(searchBar.text!, stakeholderId: self.stakeholderIdSearchParameter ?? 0, projectId:self.projectIdSearchParameter ?? 0, currentSiteId: self.siteIdSearchParameter ?? 0, onSuccess: {
                (members) -> () in
                
                self.tvResults.dismissBackgroundMessage()
                
                if members.count == 0
                {
                    self.tvResults.displayBackgroundMessage("No contacts found".localized(),
                        subMessage: "Try other words".localized())
                }
                
                self.members = members
                self.tvResults.reloadData()
                
                //Spruce framework Animation
                DispatchQueue.main.async {
                    
                    self.tvResults.spruce.animate([.slide(.left, .severely), .fadeIn], animationType: SpringAnimation(duration: 0.8))
                }
                
                }) { error in
                    
                    self.tvResults.dismissBackgroundMessage()
                    self.tvResults.reloadData()
            }
        }
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return (self.isSearchingSH == true) ? (self.stakeholdersBySite?.arraySectionTitles.count ?? 0) + 1 : 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.isSearchingSH == true
        {
            if section < self.stakeholdersBySite?.arraySectionTitles.count
            {
                return self.stakeholdersBySite?.arrayStakeholdersBySiteAndFollowStatus[section].count ?? 0
            }
            else
            {
                return 1
            }
        }
        else
        {
            return self.members.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        //The table will show Stakeholders
        if self.isSearchingSH == true
        {
            if indexPath.section < self.stakeholdersBySite?.arraySectionTitles.count ?? 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListCell") as! ContactListCell
                let contact = self.stakeholdersBySite?.arrayStakeholdersBySiteAndFollowStatus[indexPath.section][indexPath.row]
                
                //Selected Color
                let selectedColor = UIView()
                selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
                cell.selectedBackgroundView = selectedColor
                cell.accessoryView = UIImageView(image: UIImage(named: "iconSwipeDots"))
                
                cell.cellForStakeholder(contact ?? Stakeholder(isDefault:true))
                
                return cell
            }
            else
            {
                // FOR ADD STAKEHOLDER BUTTON
                let cell = tableView.dequeueReusableCell(withIdentifier: "commonCell", for: indexPath)
                
                //Selected Color
                let selectedColor = UIView()
                selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
                cell.selectedBackgroundView = selectedColor
                
                cell.textLabel?.text = "Add SH".localized()
                cell.imageView?.image = UIImage(named: "Add")
                cell.textLabel?.textColor = UIColor.darkText
                cell.textLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 13)
                
                return cell
            }
        }
        else
        {
            // The table will be use to show Members
            let contact = members[indexPath.row] as Contact
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListCell") as! ContactListCell
            
            //Selected Color
            let selectedColor = UIView()
            selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
            cell.selectedBackgroundView = selectedColor
            
            cell.cellForContact(contact)
            
            return cell
        }
    }
    
    // MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return (self.willBeShowedInNewPost == true) ? 94 + 20 : 76 + 20
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if self.isSearchingSH == true
        {
            //If a stakeholder was selected
            if indexPath.section < self.stakeholdersBySite?.arraySectionTitles.count ?? 0
            {
                let stakeholder = self.stakeholdersBySite?.arrayStakeholdersBySiteAndFollowStatus[indexPath.section][indexPath.row]
                if self.addContactToInstance != nil
                {
                    self.addContactToInstance?(stakeholder ?? Stakeholder(isDefault:true))
                }
                else
                {
                    //Workflow iPad / iPhone
                    if DeviceType.IS_ANY_IPAD == true
                    {
                        let vcStakeholderDetail = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
                        vcStakeholderDetail.isModal = true
                        vcStakeholderDetail.stakeholder = stakeholder //as! Stakeholder
                        
                        let navController = NavyController(rootViewController: vcStakeholderDetail)
                        navController.modalPresentationStyle = .pageSheet
                        
                        self.present(navController, animated: true, completion: nil)
                        
                        return
                    }
                    else
                    {
                        let vcStakeholderDetail : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                        vcStakeholderDetail.stakeholder = stakeholder ?? Stakeholder(isDefault:true)//as! Stakeholder
                        self.navigationController?.pushViewController(vcStakeholderDetail, animated: true)
                        
                        return
                    }
                }
            }
            
            //if + button was selected
            else if indexPath.section == self.stakeholdersBySite?.arraySectionTitles.count ?? 0
            {
                if self.willBeShowedInNewPost == true
                {
                    let vcAddStakeholder = Storyboard.getInstanceOf(AddStakeholderViewController.self)
                    vcAddStakeholder.onStakeholderCreated = { [weak self] new in
                        
                        self?.stakeholders.append(new)
                        
                        if let stakes = self?.stakeholders {
                            
                            self?.loadStakeholders(stakes)
                        }
                        else
                        {
                            self?.loadStakeholders([])
                        }
                        
                        self?.addContactToInstance?(new)
                        self?.searchBar.text = String()
                    }
                    
                    let navController = NavyController(rootViewController: vcAddStakeholder)
                    
                    //Type of Device
                    if DeviceType.IS_ANY_IPAD == true
                    {
                        //FormSheet Style
                        vcAddStakeholder.onStakeholderModalClosed = { [weak self] Void in
                            
                            self?.onModalViewControllerClosed?()
                        }
                        
                        navController.modalPresentationStyle = .formSheet
                        self.present(navController, animated:true, completion: nil)
                    }
                    else
                    {
                        //Modal Style
                        let vcModal = ModalViewController(subViewController: navController)
                        vcModal.onModalViewControllerClosed = { [weak self] Void in
                            
                            self?.onModalViewControllerClosed?()
                        }
                        
                        self.present(vcModal, animated:true, completion: nil)
                    }
                }
                else
                {
                    let vcAddStakeholder = Storyboard.getInstanceOf(AddStakeholderViewController.self)
                    vcAddStakeholder.onStakeholderCreated = { [weak self] new in
                        
                        if let term = self?.searchBar.text as String!, term.trim().isEmpty == false {
                            
                            self?.searchContact(term)
                        }
                        else
                        {
                            self?.searchBar.text = new.fullName
                            self?.searchContact(new.fullName)
                        }
                        
                        self?.searchBar.text = String()
                    }
                    
                    //Type of Presentation
                    let navController = NavyController(rootViewController: vcAddStakeholder)
                    
                    if DeviceType.IS_ANY_IPAD == true
                    {
                        navController.modalPresentationStyle = .formSheet
                    }
                    
                    present(navController, animated: true, completion: nil)
                    
                    return
                }
            }
        }
        else
        {
            let member = members[indexPath.row]
            self.addContactToInstance?(member)
        }
        
        if presentingViewController != nil && navigationController?.viewControllers.count == 1
        {
            dismiss(animated: true, completion: nil)
        }
        else
        {
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        // Remove seperator inset
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset))
        {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))
        {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.responds(to: #selector(setter: UIView.layoutMargins))
        {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let lblTitle = SHMSectionTitleLabel()
        lblTitle.backgroundColor = UIColor.groupTableViewBackground
        lblTitle.font = UIFont.boldSystemFont(ofSize: 12)
        lblTitle.textColor = UIColor.grayConcreto()
        
        if self.isSearchingSH == true
        {
            if section < self.stakeholdersBySite?.arraySectionTitles.count
            {
                lblTitle.numberOfLines = 2
                lblTitle.attributedText = self.stakeholdersBySite?.arraySectionTitles[section]
                lblTitle.backgroundColor = .groupTableViewBackground
            }
            else
            {
                lblTitle.text = ""
            }
        }
        else
        {
            //Nothing to do Diego?
        }
        
        return lblTitle
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if self.isSearchingSH == true
        {
            if section < self.stakeholdersBySite?.arraySectionTitles.count
            {
                return 40
            }
        }
        
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        if self.isSearchingSH == true
        {
            if indexPath.section < self.stakeholdersBySite?.arraySectionTitles.count
            {
                return true
            }
        }
        
        return false
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        if self.isSearchingSH == true
        {
            if self.stakeholdersBySite?.arrayStakeholdersBySiteAndFollowStatus[indexPath.section][indexPath.row].isFollow == FollowStatus.following
            {
                let unfollow = UITableViewRowAction(style: .default, title: "Unfollow".localized(), handler: { (action, indexPath) -> () in
                    let staekholderToUnfollow = self.stakeholdersBySite?.arrayStakeholdersBySiteAndFollowStatus[indexPath.section][indexPath.row] ?? Stakeholder(isDefault:true)
                    tableView.setEditing(false, animated: true)
                    
                    LibraryAPI.shared.stakeholderBO.unfollowStakeholder(staekholderToUnfollow,
                        onSuccess: { () -> () in
                            
                            if self.isSearchingSH == true
                            {
                                //Reload Project List
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadStakeholdersList"), object: nil)
                            }
                            
                            //Update stakeholder status
                            for (index, stakeholder) in self.stakeholders.enumerated() where stakeholder.id == staekholderToUnfollow.id
                            {
                                self.stakeholders[index].isFollow = FollowStatus.notFollowing
                            }
                            self.stakeholdersBySite?.updateStakeholders(self.stakeholders)
                            self.tvResults.reloadData()
                            
                            //Spruce framework Animation
                            DispatchQueue.main.async {
                                
                                self.tvResults.spruce.animate([.slide(.right, .severely), .fadeIn], animationType: SpringAnimation(duration: 0.8))
                            }


                            
                            //Refresh StakeholderList
                            if let unfollow = self.onFollowOrUnfollow {
                                
                                unfollow()
                            }
                            
                    }, onError: { error in
                        
                        MessageManager.shared.showBar(title: "Error",
                                                                        subtitle: "There was an error trying to unfollow this stakeholder".localized(),
                                                                        type: .error,
                                                                        fromBottom: false)
                    })
                })
                unfollow.backgroundColor = UIColor.redUnfollowAction()
                
                return [unfollow]
            }
            else
            {
                let follow = UITableViewRowAction(style: .normal, title: "FOLLOW".localized(), handler: { (action, indexPath) -> () in
                    let staekholderToFollow = self.stakeholdersBySite?.arrayStakeholdersBySiteAndFollowStatus[indexPath.section][indexPath.row] ?? Stakeholder(isDefault:true)
                    tableView.setEditing(false, animated: true)
                    
                    LibraryAPI.shared.stakeholderBO.followStakeholder(staekholderToFollow,
                        onSuccess: { () -> () in
                            
                            if self.isSearchingSH == true
                            {
                                //Reload Project List
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadStakeholdersList"), object: nil)
                            }
                            
                            for (index, stakeholder) in self.stakeholders.enumerated() where stakeholder.id == staekholderToFollow.id
                            {
                                self.stakeholders[index].isFollow = FollowStatus.following
                            }
                            self.stakeholdersBySite?.updateStakeholders(self.stakeholders)
                            self.tvResults.reloadData()
                            
                            //Spruce framework Animation
                            DispatchQueue.main.async {
                                
                                self.tvResults.spruce.animate([.slide(.right, .severely), .fadeIn], animationType: SpringAnimation(duration: 0.8))
                            }

                            
                            //Refresh StakeholderList
                            if let follow = self.onFollowOrUnfollow {
                                
                                follow()
                            }
                            
                        }, onError: { error in
                            
                            MessageManager.shared.showBar(title: "Error",
                                subtitle: "There was an error trying to follow this stakeholder".localized(),
                                type: .error,
                                fromBottom: false)
                    })
                })
                follow.backgroundColor = UIColor.greenFollowAction()
                
                return [follow]
            }
        }
        else
        {
            return nil
        }
    }
    
    // MARK: SCROLL
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if self.willBeShowedInNewPost == false
        {
            self.searchBar.resignFirstResponder()
        }
    }
}
