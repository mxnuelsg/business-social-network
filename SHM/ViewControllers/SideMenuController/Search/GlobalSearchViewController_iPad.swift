//
//  GlobalSearchViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 1/8/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class GlobalSearchViewController_iPad: GlobalSearchViewController
{

    //MARK: Properties and outlets
    //View Controllers
    var collectionStakeholders:SearchStakeholderResultsCollectionVC_iPad!
    var tvProjects:SearchProjectResultsTableVC_iPad!
    var tvPosts:PostTableViewController_iPad!
    
    @IBOutlet weak var containerResults: UIView!

    //Control
    @IBOutlet weak var segmentedControlResults: UISegmentedControl!
    var previousSelectedSegment = 0
    var currentSelectedSegment = 0
    var  updateType = updateResultsOfType.stakeholders
   
    enum updateResultsOfType
    {
        case stakeholders,projects,posts
    }
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        self.loadIpadConfig()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    //MARK: LOCALIZATION
    fileprivate func localize()
    {
        title = "GLOBAL SEARCH".localized()
        searchBar.placeholder = "Search".localized()
    self.segmentedControlResults.setTitle("STAKEHOLDERS".localized(), forSegmentAt: 0)
    self.segmentedControlResults.setTitle("PROJECTS".localized(), forSegmentAt: 1)
    self.segmentedControlResults.setTitle("POSTS".localized(), forSegmentAt: 2)
    }
 
    //MARK: LOAD CONFIG
    fileprivate func loadIpadConfig()
    {
        //initialize AutoSearchBar
        searchBar.initialize(shouldSearchOnlyWithSearchButton: true,
        onSearchText: { [weak self] (text) in
            if (self != nil)
            {
                self!.projectPageNumber = 1
                self!.stakeholderPageNumber = 1
                self!.postPageNumber = 1
                self!.results = nil
                self!.search()
            }
            
        }, onClearText: { [weak self] in
            self?.cancelSearch()
            
        }, onCancel: { [weak self] in
            self?.cancelSearch()
        })
        
        
        self.localize()
        
        //Page numbers
        projectPageNumber = 1
        stakeholderPageNumber = 1
        postPageNumber = 1
        
        
        
        //Bar Button Item
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action:#selector(self.createNewPost))
        navigationItem.rightBarButtonItems = [barBtnCompose]
        
        //Search Bar
        searchBar.text = searchKeyWord ?? ""
        
        //Segmented Control
        segmentedControlResults.addTarget(self, action: #selector(self.reloadCollectionStakeholders(_:)), for:.valueChanged)
        
        //ViewControllers
        self.collectionStakeholders = Storyboard.getInstanceOf(SearchStakeholderResultsCollectionVC_iPad.self)
        self.tvProjects = Storyboard.getInstanceOf(SearchProjectResultsTableVC_iPad.self)
        self.tvProjects.projectDidChange = { [weak self] Void in
            
            self?.projectPageNumber = 1
            self?.stakeholderPageNumber = 1
            self?.postPageNumber = 1
            self?.results = nil
            self?.search()
        }
        
        self.tvPosts = Storyboard.getInstanceOf(PostTableViewController_iPad.self)
        
        self.containerResults.addSubViewController(self.collectionStakeholders, parentVC: self)
        
        //Start searching....
        self.search()
    }
    
    // MARK: ACTIONS
    override func search()
    {   //Prevent empty search
        guard searchBar.text != nil && searchBar.text!.trim() != "" else {
            
            results = nil
            postTotalPageNumber = 0
            projectTotalPageNumber = 0
            stakeholderTotalPageNumber = 0
            
            return
        }
        
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        LibraryAPI.shared.searchBO.searchText(searchBar.text!,
            projectPageNumber: self.projectPageNumber,
            stakeholderPageNumber: self.stakeholderPageNumber,
            postPageNumber: self.postPageNumber,
            onSuccess: { (results) -> () in
                MessageManager.shared.hideHUD()
                
                //First page of results
                if self.results == nil
                {
                    self.results = results
                    self.postTotalPageNumber = Int(ceil(CGFloat(results.postsTotal) / CGFloat(20)))
                    self.projectTotalPageNumber = Int(ceil(CGFloat(results.projectsTotal) / CGFloat(20)))
                    self.stakeholderTotalPageNumber = Int(ceil(CGFloat(results.stakeholdersTotal) / CGFloat(20)))
                    
                    self.addResultsInContainer(self.currentSelectedSegment)
                    
                    //Titles in segmented controller
                    var strTitle = "STAKEHOLDERS".localized()
                    self.segmentedControlResults.setTitle(strTitle + " (\(results.stakeholdersTotal))", forSegmentAt: 0)
                    
                    strTitle = "PROJECTS".localized()
                    self.segmentedControlResults.setTitle(strTitle + " (\(results.projectsTotal))", forSegmentAt: 1)
                    

                    strTitle = "POSTS".localized()
                    self.segmentedControlResults.setTitle(strTitle + "( \(results.postsTotal))", forSegmentAt: 2)
                }
                else
                {   //Reload the collection/tableview
                    switch self.updateType
                    {
                        case .stakeholders:
                            //next page of stakeholders
                            self.collectionStakeholders.arrayStakeholders += results.stakeholders
                            self.collectionStakeholders.collectionView?.reloadData()
                        
                        case .projects:
                            //next page of projects
                            self.tvProjects.arrayProjects += results.projects
                            self.tvProjects.tableView.reloadData()

                        case .posts:
                            //next page of posts
                            self.tvPosts.posts += results.posts
                            self.tvPosts.tableView.reloadData()
                    }
                }
            }) { error in
                
                MessageManager.shared.hideHUD()
                MessageManager.shared.showBar(title: "Error".localized(),
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
        }

    }
    
    fileprivate func cancelSearch() {
        searchBar.text = String()
        self.search()

        //Remove results in arrays and reload table/collection view
        self.collectionStakeholders.arrayStakeholders.removeAll()
        self.collectionStakeholders.collectionView?.reloadData()
        self.tvProjects.arrayProjects.removeAll()
        self.tvProjects.tableView.reloadData()
        self.tvPosts.posts.removeAll()
        self.tvPosts.tableView.reloadData()
        //Reset segmented control text
        self.resetSegmentedControlResults()
    }

    // MARK: - Segmented Control
    func reloadCollectionStakeholders(_ sender: UISegmentedControl)
    {
        self.previousSelectedSegment = self.currentSelectedSegment
        self.currentSelectedSegment = self.segmentedControlResults.selectedSegmentIndex
        self.removeResultsFromContainer(self.previousSelectedSegment)
        self.addResultsInContainer(self.currentSelectedSegment)
    }
    
    fileprivate func resetSegmentedControlResults()
    {
        self.segmentedControlResults.setTitle("STAKEHOLDERS".localized(), forSegmentAt: 0)
        self.segmentedControlResults.setTitle("PROJECTS".localized(), forSegmentAt: 1)
        self.segmentedControlResults.setTitle("POSTS".localized(), forSegmentAt: 2)
    }
    
    //MARK: Private functions
    func sortStakeholderByName(_ stakeholder1:Stakeholder, stakeholder2:Stakeholder)->Bool
    {
            return stakeholder1.name < stakeholder2.name
    }
    
    func removeResultsFromContainer(_ previousSegment:Int)
    {
        switch previousSegment
        {
        case 0:
            //Stakeholders on top of container
            self.collectionStakeholders.arrayStakeholders = [Stakeholder]()
            self.collectionStakeholders.collectionView?.reloadData()
            self.collectionStakeholders.removeFromParentViewController()
        case 1:
            //Projects on top of container
            self.tvProjects.arrayProjects = [Project]()
            self.tvProjects.tableView?.reloadData()
            self.tvProjects.removeFromParentViewController()
        case 2:
            //Post on top of container
            self.tvPosts.posts = [Post]()
            self.tvPosts.tableView?.reloadData()
            self.tvPosts.removeFromParentViewController()
        default:
            break;
        }
    }
    
    func addResultsInContainer(_ currentSegment:Int)
    {
        guard let results = self.results else {
            
            return
        }
        
        self.collectionStakeholders.collectionView?.dismissBackgroundMessage()
        self.tvPosts.dismissBackgroundMessage()
        self.tvProjects.dismissBackgroundMessage()
        
        switch currentSegment
        {
            case 0: //Stakeholder results
                if results.stakeholders.count != 0
                {
                    self.collectionStakeholders.arrayStakeholders = self.sortAndFilterStakeholders(results.stakeholders)
                }
                else
                {
                    self.collectionStakeholders.arrayStakeholders.removeAll()
                    self.collectionStakeholders.collectionView?.displayBackgroundMessage("No results found".localized(),
                        subMessage: nil)
                }
                self.collectionStakeholders.collectionView?.reloadData()
                self.containerResults.addSubViewController(self.collectionStakeholders, parentVC: self)
            
            case 1: //Projects results
                if results.projects.count != 0
                {
                    self.tvProjects.arrayProjects = results.projects
                }
                else
                {
                    self.tvProjects.arrayProjects.removeAll()
                    self.tvProjects.tableView.displayBackgroundMessage("No results found".localized(),
                        subMessage: nil)
                    self.tvProjects.tableView.hideEmtpyCells()

                }
                self.tvProjects.tableView.reloadData()
                self.containerResults.addSubViewController(self.tvProjects, parentVC: self)
            
            case 2://Posts results
                if results.posts.count != 0
                {
                    self.tvPosts.posts = results.posts
                }
                else
                {
                    self.tvPosts.posts.removeAll()
                    self.tvPosts.tableView.displayBackgroundMessage("No results found".localized(),
                        subMessage: nil)
                    self.tvPosts.tableView.hideEmtpyCells()

                }
                self.tvPosts.tableView.reloadData()
                self.containerResults.addSubViewController(self.tvPosts, parentVC: self)
            default:
                break
        }
    }
    
    func sortAndFilterStakeholders(_ stakeholders:[Stakeholder])->[Stakeholder]
    {
        if stakeholders.count != 0
        {
            var arrayStakeholder1 = stakeholders.filter({$0.isFollow == FollowStatus.following})
            arrayStakeholder1 = arrayStakeholder1.sorted(by: {$0.fullName < $1.fullName})
            
            var arrayStakeholder2 = stakeholders.filter({$0.isFollow == FollowStatus.notFollowing})
            arrayStakeholder2 = arrayStakeholder2.sorted(by: {$0.fullName < $1.fullName})
            
            let arrayStakeholderProccessed = arrayStakeholder1 + arrayStakeholder2
            
            return arrayStakeholderProccessed
        }
        else
        {
            return [Stakeholder]()
        }
    }
}

