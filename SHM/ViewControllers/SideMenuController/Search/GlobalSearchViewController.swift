//
//  SearchViewController.swift
//  SHM
//  Created by Manuel Salinas on 8/25/15.
//

import UIKit
import Spruce

private enum GlobalSearchSections: Int
{
    case stakeholders = 0
    case projects = 1
    case posts = 2
}

class GlobalSearchViewController: UIViewController
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var searchBar: AutoSearchBar!
    @IBOutlet weak var tableResults: UITableView!
    
    @IBOutlet weak var navigationItemSearch: UINavigationItem!
    
    var results: GlobalSearchResult?
    var searchKeyWord: String?
    
    var projectPageNumber: Int?
    var stakeholderPageNumber: Int?
    var postPageNumber: Int?
    
    var projectTotalPageNumber = 1
    var stakeholderTotalPageNumber = 1
    var postTotalPageNumber = 1
    
    var tableVCStakeholders: StakeholdersTableViewController?
    var tableVCProjects: ProjectsTableViewController?
    var tableVCPosts: PostsTableViewController?

    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        backButtonArrow()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: LOCALIZATION
    fileprivate func localize()
    {
        title = "GLOBAL SEARCH".localized()
        self.searchBar.placeholder = "Search".localized()
        self.searchBar.setValue("Cancel".localized(), forKey:"_cancelButtonText")
        
        if DeviceType.IS_ANY_IPAD == false
        {
            self.navigationItemSearch.title = "SEARCH".localized()
        }
    }
    
    //MARK: LOAD CONFIG
    fileprivate func loadConfig()
    {
        //initialize AutoSearchBar
        searchBar.initialize(shouldSearchOnlyWithSearchButton: true,
        onSearchText: { [weak self] (text) in
            if (self != nil)
            {
                self!.projectPageNumber = 0
                self!.stakeholderPageNumber = 0
                self!.postPageNumber = 0
                self!.results = nil
                self!.search()
            }
            
        }, onCancel: { [weak self] in
            self?.searchBar.text = String()
            self?.search()
        })
        
        self.localize()
        
        //Bar Button Item
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        navigationItem.rightBarButtonItems = [barBtnCompose]
        
        searchBar.text = searchKeyWord ?? ""
        
        projectPageNumber = 0
        stakeholderPageNumber = 0
        postPageNumber = 0
        search()
        
        tableResults.tableFooterView = UIView(frame: CGRect.zero)
        tableResults.backgroundColor = UIColor.groupTableViewBackground
    }
    
    // MARK: ACTIONS
    func createNewPost()
    {
        let vcNewPost = NewPostViewController()
        
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = (DeviceType.IS_ANY_IPAD) ?  UIModalPresentationStyle.formSheet :  UIModalPresentationStyle.overFullScreen
        
        present(navController, animated: true, completion: nil)
    }
    
    func search()
    {
        guard searchBar.text != nil && searchBar.text!.trim() != "" else {
            results = nil
            postTotalPageNumber = 0
            projectTotalPageNumber = 0
            stakeholderTotalPageNumber = 0
            tableResults.reloadData()
            
            tableResults.displayBackgroundMessage("Type in the text field and press Search".localized(),
                subMessage: nil)
            
            return
        }
        
        projectPageNumber = projectPageNumber != nil ? projectPageNumber! + 1 : nil
        stakeholderPageNumber = stakeholderPageNumber != nil ? stakeholderPageNumber! + 1 : nil
        postPageNumber = postPageNumber != nil ? postPageNumber! + 1 : nil
        
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        LibraryAPI.shared.searchBO.searchText(searchBar.text!,
            projectPageNumber: self.projectPageNumber,
            stakeholderPageNumber: self.stakeholderPageNumber,
            postPageNumber: self.postPageNumber,
            onSuccess: { (results) -> () in
                
                MessageManager.shared.hideHUD()
                
                if self.results == nil
                {
                    self.results = results
                    self.postTotalPageNumber = Int(ceil(CGFloat(results.postsTotal) / CGFloat(20)))
                    self.projectTotalPageNumber = Int(ceil(CGFloat(results.projectsTotal) / CGFloat(20)))
                    self.stakeholderTotalPageNumber = Int(ceil(CGFloat(results.stakeholdersTotal) / CGFloat(20)))
                    self.tableResults.reloadData()
                }
                else
                {
                    if self.projectPageNumber != nil
                    {
                        self.tableVCProjects?.projects += results.projects
                        self.tableVCProjects?.reload()
                    }
                    else if self.stakeholderPageNumber != nil
                    {
                        self.tableVCStakeholders?.stakeholders += results.stakeholders
                        self.tableVCStakeholders?.reload()
                    }
                    else if self.postPageNumber != nil
                    {
                        self.tableVCPosts?.posts += results.posts
                        self.tableVCPosts?.reload()
                    }
                    else
                    {
                        print("SEVERE ERROR")
                    }
                }
                
                //Spruce framework Animation
                DispatchQueue.main.async {
                    
                    self.tableResults.spruce.animate([.slide(.left, .severely), .fadeIn], animationType: SpringAnimation(duration: 0.7))
                }

                
                self.tableResults.dismissBackgroundMessage()
                
            }) { error in
                
                MessageManager.shared.hideHUD()
                
                self.tableResults.displayBackgroundMessage("An error ocurred".localized(),
                    subMessage: "Try again".localized())
                
                MessageManager.shared.showBar(title: "Error".localized(),
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
        }
    }
    
    // MARK: - Private Methods
    func displayStakeholders()
    {
        projectPageNumber = nil
        stakeholderPageNumber = 1
        postPageNumber = nil
        
        tableVCStakeholders = Storyboard.getInstanceOf(StakeholdersTableViewController.self)
        tableVCStakeholders!.title = "STAKEHOLDERS".localized()
        tableVCStakeholders!.stakeholders = results!.stakeholders
        tableVCStakeholders!.willDisplayLastRow = { [weak self] row in
            
            if let page = self?.stakeholderPageNumber, let totalPages = self?.stakeholderTotalPageNumber, page < totalPages {
                
                self?.search()
            }
        }
        
        navigationController?.pushViewController(tableVCStakeholders!, animated: true)
    }
    
    func displayProjects()
    {
        projectPageNumber = 1
        stakeholderPageNumber = nil
        postPageNumber = nil
        
        tableVCProjects = Storyboard.getInstanceOf(ProjectsTableViewController.self)
        tableVCProjects!.title = "PROJECTS".localized()
        tableVCProjects!.projects = results!.projects
        tableVCProjects!.willDisplayLastRow = { [weak self] row in
            
            if let page = self?.projectPageNumber, let totalPages = self?.projectTotalPageNumber, page < totalPages {
    
                self?.search()
            }
        }
        
        navigationController?.pushViewController(tableVCProjects!, animated: true)
    }
    
    func displayPosts()
    {
        projectPageNumber = nil
        stakeholderPageNumber = nil
        postPageNumber = 1
        
        tableVCPosts = Storyboard.getInstanceOf(PostsTableViewController.self)
        tableVCPosts!.title = "POSTS".localized()
        tableVCPosts!.posts = results!.posts
        tableVCPosts!.willDisplayLastRow = { [weak self] row in
            
            if let page = self?.postPageNumber, let totalPages = self?.postTotalPageNumber, page < totalPages {
                
                self?.search()
            }
        }
        
        navigationController?.pushViewController(tableVCPosts!, animated: true)
    }
}

// MARK: - UITableViewDatasource

extension GlobalSearchViewController: UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
       
        guard results != nil else {
            
            return 0
        }
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style: .default, reuseIdentifier:"ReusableCell")
        
        //Optional chaining
        guard let result = results else {
            
            cell.textLabel?.text = "No results".localized()
            cell.textLabel?.attributedText = nil
            cell.accessoryType = .none
            
            return cell
        }
        
        switch indexPath.section
        {
        case GlobalSearchSections.stakeholders.rawValue:
            
            let title = "\(result.stakeholdersTotal) ∣ " +  "Stakeholders".localized()
            
            let attributedString = NSMutableAttributedString(string: title)
            attributedString.setBoldAndColor("\(result.stakeholdersTotal)", color: .blueTag(), size: 20)
            
            cell.textLabel?.attributedText = attributedString
            cell.accessoryType = .disclosureIndicator
            
        case GlobalSearchSections.projects.rawValue:
            
            let title = "\(result.projectsTotal) ∣ " + "Projects".localized()
            
            let attributedString = NSMutableAttributedString(string: title)
            attributedString.setBoldAndColor("\(result.projectsTotal)", color: .blueTag(), size: 20)
            
            cell.textLabel?.attributedText = attributedString
            cell.accessoryType = .disclosureIndicator
            
        case GlobalSearchSections.posts.rawValue:
            
            let title = "\(result.postsTotal) ∣ " + "Posts".localized()
            
            let attributedString = NSMutableAttributedString(string: title)
            attributedString.setBoldAndColor("\(result.postsTotal)", color: .blueTag(), size: 20)
            
            cell.textLabel?.attributedText = attributedString
            cell.accessoryType = .disclosureIndicator
            
        default:
            
            cell.textLabel?.text = "No results".localized()
            cell.textLabel?.attributedText = nil
            cell.accessoryType = .none
        }
        
        return cell
    }
}

extension GlobalSearchViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.section
        {
        case 0:
            if results!.stakeholdersTotal > 0
            {
                displayStakeholders()
            }
        case 1:
            if results!.projectsTotal > 0
            {
                displayProjects()
            }
        case 2:
            if results!.postsTotal > 0
            {
                displayPosts()
            }
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 88
    }
}
