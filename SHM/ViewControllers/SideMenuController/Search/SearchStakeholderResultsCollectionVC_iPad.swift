//
//  SearchStakeholderResultsCollectionVC_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 1/8/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class SearchStakeholderResultsCollectionVC_iPad: UICollectionViewController
{
    // MARK: Properties
    var resultsType = 0
    var arrayStakeholders = [Stakeholder]()
    var stakeholderSelected:Stakeholder?
    var layout: UICollectionViewFlowLayout!
    var offsetLimit:CGFloat = 0

    
    // MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        self.loadLayoutForCollectionView()
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    //MARK: LOAD CONFIG
    fileprivate func loadConfig()
    {
        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        //Collection View layout and registration
        self.layout = UICollectionViewFlowLayout()
        self.loadLayoutForCollectionView()
        if let collectionViewStakeholders = self.collectionView {
            
            collectionViewStakeholders.collectionViewLayout = self.layout
            collectionViewStakeholders.allowsMultipleSelection = false
            collectionViewStakeholders.register(UINib(nibName: "CollectionStakeholderCell", bundle: nil), forCellWithReuseIdentifier: "CollectionStakeholderCell")
            collectionViewStakeholders.backgroundColor = UIColor.white
        }
    }

    //MARK: Private Functions
    func pushNewPost(_ stakeholderTag: Stakeholder!)
    {
        let vcNewPost = NewPostViewController()
        vcNewPost.stakeholderTagged = stakeholderTag
        
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = (DeviceType.IS_ANY_IPAD) ?  UIModalPresentationStyle.formSheet :  UIModalPresentationStyle.overFullScreen
        present(navController, animated: true, completion: nil)
    }
    
    // MARK: LOAD DATA
    func reloadStakeholders()
    {
//        let parent = parent as! GlobalSearchViewController_iPad
        let parent = GlobalSearchViewController_iPad()
        parent.results = nil
        parent.projectPageNumber = 1
        parent.stakeholderPageNumber = 1
        parent.postPageNumber = 1
        parent.search()

    }
    
    
    //MARK: LAYOUT FOR PORTRAIT AND LANDSCAPE
    func loadLayoutForCollectionView()
    {
        if(UIDeviceOrientationIsPortrait(UIDevice.current.orientation))
        {
            self.layout.scrollDirection =  UICollectionViewScrollDirection.vertical
            self.layout.minimumInteritemSpacing = 15
            self.layout.minimumLineSpacing = 15
            self.layout.sectionInset = UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 20)
            self.layout.headerReferenceSize = CGSize(width: 320, height: 24)
            self.layout.itemSize = CGSize(width: 355, height: 85)
        }
        else
        {
            self.layout.scrollDirection =  UICollectionViewScrollDirection.vertical
            self.layout.minimumInteritemSpacing = 1
            self.layout.minimumLineSpacing = 15
            self.layout.sectionInset = UIEdgeInsets(top: 5, left: 1, bottom: 5, right: 1)
            self.layout.headerReferenceSize = CGSize(width: 320, height: 24)
            self.layout.itemSize = CGSize(width: 340, height: 85)
        }
    }
    
    func pushActorDetail()
    {
        let stakeholderDetailVC = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
        stakeholderDetailVC.stakeholder = stakeholderSelected
        navigationController!.pushViewController(stakeholderDetailVC, animated: true)
    }

    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        // #warning Incomplete implementation, return the number of items
        if self.arrayStakeholders.count != 0
        {
            return self.arrayStakeholders.count
        }

        return 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionStakeholderCell", for: indexPath) as! CollectionStakeholderCell

        // Configure the cell
        let stakeholder = self.arrayStakeholders[indexPath.row]
        
        if self.arrayStakeholders.count != 0
        {
            cell.lblName.text = self.arrayStakeholders[indexPath.item].fullName
            let strJobPosition = self.arrayStakeholders[indexPath.item].jobPosition?.value ?? ""
            let strCompanyName = self.arrayStakeholders[indexPath.item].companyName
            cell.lblJobPosition.attributedText = NSMutableAttributedString().setJobPositionCompany(strJobPosition, company: strCompanyName)
                cell.imgProfile.setImageWith(URL(string: self.arrayStakeholders[indexPath.item].thumbnailUrl), placeholderImage: UIImage(named: "defaultperson"))
            
//            cell.setSite(self.arrayStakeholders[indexPath.item].site)
            cell.setNationalities(self.arrayStakeholders[indexPath.item])
        }
        
        cell.onButtonTap = { button in
            
            //Selected Style
            cell.btnMore.backgroundColor = UIColor.blueTag()
            cell.btnMore.tintColor = UIColor.white
            

            let vcTableOptions = StringTableViewController()
            vcTableOptions.modalPresentationStyle = .popover
            vcTableOptions.options = (stakeholder.isFollow == FollowStatus.following) ? ["Unfollow".localized(), "Post".localized()] : ["Follow".localized(), "Post".localized()]
            
            vcTableOptions.onSelectedOption = { numberOfRow in
                
                if numberOfRow == 0
                {
                    //Follow or Unfollow
                    if stakeholder.isFollow == FollowStatus.following
                    {
                        LibraryAPI.shared.stakeholderBO.unfollowStakeholder(stakeholder,
                            onSuccess: { () -> () in
                                
                                //Reload List
                                self.reloadStakeholders()
                                
                                let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                                    
                                    let info = "Now, you're not following".localized() + " " + stakeholder.fullName
                                    
                                    MessageManager.shared.showBar(title: "Info".localized(),
                                                                subtitle: info,
                                                                type: .info,
                                                                fromBottom: false)
                                }
                                
                            }, onError: { error in
                                
                                MessageManager.shared.showBar(title: "Error".localized(),
                                    subtitle: "Error.TryAgain".localized(),
                                    type: .error,
                                    fromBottom: false)
                        })
                    }
                    else
                    {
                        LibraryAPI.shared.stakeholderBO.followStakeholder(stakeholder,
                            onSuccess: { () -> () in
                                
                                //Reload List
                                self.reloadStakeholders()
                                
                                let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                                    
                                    let info = "Now, you're following".localized() + " " + stakeholder.fullName
                                    
                                    MessageManager.shared.showBar(title: "Success".localized(),
                                                                  subtitle: info,
                                                                  type: .success,
                                                                  fromBottom: false)
                                }
                                
                            }, onError: { error in
                                
                                MessageManager.shared.showBar(title: "Error".localized(),
                                                              subtitle: "Error.TryAgain".localized(),
                                                              type: .error,
                                                              fromBottom: false)
                        })
                    }
                }
                else
                {
                    //Post it
                    self.pushNewPost(self.stakeholderSelected)
                }
            }
            
            vcTableOptions.onClosed = { Void in
                
                //Unselected Style
                cell.btnMore.backgroundColor = UIColor.white
                cell.btnMore.tintColor = UIColor.blueTag()
            }
            
            //Show Popover
            let popoverController = vcTableOptions.popoverPresentationController
            popoverController?.sourceView = cell
            popoverController?.sourceRect = button.frame
            popoverController?.permittedArrowDirections = .any
            
            self.present(vcTableOptions, animated: true, completion: nil)
        }
        
        return cell
    }

    // MARK: UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        guard self.arrayStakeholders.count > 0 else
        {
            return
        }
        self.stakeholderSelected = self.arrayStakeholders[indexPath.item]
        self.pushActorDetail()
    }

    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if bottomEdge >= scrollView.contentSize.height
        {
            guard let parent = parent as? GlobalSearchViewController_iPad else{
                return
            }
            if let pageNumberInt = parent.stakeholderPageNumber, let totalInt = parent.results?.stakeholdersTotal{
                let pageNumberFloat = Float(pageNumberInt)
                let totalFloat = Float(totalInt)
                if pageNumberFloat <= (totalFloat/20)
                {
                    print("page=\(pageNumberFloat) total=\((totalFloat/20))")
                    parent.stakeholderPageNumber = pageNumberInt+1
                    parent.updateType = GlobalSearchViewController_iPad.updateResultsOfType.stakeholders
                    parent.search()
                }
            }
        }
        
    }

}
