//
//  InboxTableViewCell.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 9/21/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class InboxTableViewCell: UITableViewCell
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var imgReadMark: UIImageView!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgDisclosure: UIImageView!
    
    static let imgUnreadMessage = UIImage.getImageWithColor(UIColor.blueUnreadBullet(), size: CGSize(width: 8, height: 8))
    
    //MARK: LIFE CYCLE
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        //Images
        imgReadMark.layer.cornerRadius = 4
        
        //Fields
        self.lblDate.adjustsFontSizeToFitWidth = true
    }
    
    //MARK: ACTIONS
    func setupForMessage(_ message: InboxMessage)
    {
        setupForSubMessage(message)
        
        lblSubject.text = message.subject
        
        if message.hasPrevious
        {
            self.imgDisclosure.image = UIImage(named: "rightArrowDouble")
        }
        else
        {
            self.imgDisclosure.image = UIImage(named: "rightArrow")
        }
    }
    
    func setupForSubMessage(_ message: InboxMessage)
    {
        lblDate.text = message.date.getDateTimeStyle()
        lblFrom.text = message.from.fullName
        lblMessage.text = message.shortBody
        
        if message.viewed == true
        {
            imgReadMark.image = nil
        }
        else
        {
            imgReadMark.image = InboxTableViewCell.imgUnreadMessage
            imgReadMark.layer.cornerRadius = 4
            imgReadMark.clipsToBounds = true
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
}
