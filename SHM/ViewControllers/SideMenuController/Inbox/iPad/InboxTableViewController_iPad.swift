//
//  InboxTableViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas on 1/26/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class InboxTableViewController_iPad: UITableViewController
{
    //MARK: PROPERTIES & OUTLET
    var messages: [InboxMessage] = []
    var messageSelected: InboxMessage?
    var willDisplayLastRow: ((_ row:Int) -> ())?
    var onSelectedMessage:((_ msg: InboxMessage) -> ())?
    var onUnreadOrDeleteMessage:(() -> ())?
    var onRelatedMessage:((_ parentMsj: InboxMessage) -> ())?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig_iPad()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.backButtonArrow()
    }
    
    //MARK: LOAD CONFIG
    fileprivate func loadConfig_iPad()
    {
        //UI
        self.title = "Messages".localized()
        self.setBorder()
        
        //Navigation bar items
        let barBtnNewInbox = UIBarButtonItem(image: UIImage(named: "iconInbox"), style: .plain, target: self, action: #selector(self.newMessage))
        navigationItem.rightBarButtonItem = barBtnNewInbox
        
        //Table config
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 87
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 0)
        self.tableView.hideEmtpyCells()
        self.tableView.displayBackgroundMessage("Loading...".localized(),
            subMessage: nil)
    }
    
    //MARK: ACTIONS
    func newMessage()
    {
        let vcNewMessage = Storyboard.getInstanceOf(NewMessageTableViewController.self)
        let navController = NavyController(rootViewController: vcNewMessage)
        navController.modalPresentationStyle = .formSheet
        
        self.present(navController, animated: true, completion: nil)
    }
    
    func reload()
    {
        if self.messages.count == 0
        {
            self.displayBackgroundMessage("No messages".localized(),
                subMessage: "Pull to refresh".localized())
            return
        }
        
        self.dismissBackgroundMessage()
        //self.onUnreadOrDeleteMessage?()
        self.tableView.reloadData()
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.messages.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let message = self.messages[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "InboxMessageCell", for: indexPath) as! InboxTableViewCell
        
        //Color for selected cell
        let selectedColor = UIView()
        selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
        cell.selectedBackgroundView = selectedColor
        cell.accessoryView = UIImageView(image: UIImage(named: "iconSwipeDots"))
        
        cell.setupForMessage(message)
        
        return cell
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let selectedMessage = self.messages[indexPath.row]
        
        //If has a conversation
        if selectedMessage.hasPrevious == true
        {
            LibraryAPI.shared.inboxBO.getInboxDetail(selectedMessage.id,
                onSuccess: { (message) -> () in
                    
                    //Set conversation id & Pass the mensage
                    self.messages[tableView.indexPathForSelectedRow!.row].conversationId = message.conversationId
                    self.onRelatedMessage?(selectedMessage)
                    
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            })
            
        }
        else
        {
            //Read Message
            let cell = tableView.cellForRow(at: indexPath) as! InboxTableViewCell
            cell.imgReadMark.image = nil
            
            //Show detail
            self.onSelectedMessage?(selectedMessage)
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        let message = self.messages[indexPath.row]
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete".localized() , handler: {
            (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
            
            self.tableView.setEditing(false, animated: true)
            
            LibraryAPI.shared.inboxBO.deleteInboxMessage(message.id,
                onSuccess: { (message) -> () in
                    
                    self.onUnreadOrDeleteMessage?()
                    
                    self.messages.remove(at: indexPath.row)
                    self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                    
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            })
        })
        
        guard message.viewed == true else {
            
            return [deleteAction]
        }
        
        let markAsUnreadAction = UITableViewRowAction(style: .default, title: "Unread".localized(), handler: {
            (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
            
            self.tableView.setEditing(false, animated: true)
            
            LibraryAPI.shared.inboxBO.markasUnRead(parameters: message.getWSObject(),
                onSuccess: { (message) -> () in
                    
                    self.onUnreadOrDeleteMessage?()
                    
                    self.messages[indexPath.row].viewed = false
                    
                    let cell = tableView.cellForRow(at: indexPath) as! InboxTableViewCell
                    cell.imgReadMark.image = InboxTableViewCell.imgUnreadMessage
                    cell.imgReadMark.layer.cornerRadius = 4
                    cell.imgReadMark.clipsToBounds = true
                    
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            })
        })
        
        markAsUnreadAction.backgroundColor = UIColor.lightGray
        
        return [deleteAction, markAsUnreadAction]
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if indexPath.row == self.messages.count - 1
        {
            self.willDisplayLastRow?(indexPath.row)
        }
    }
}
