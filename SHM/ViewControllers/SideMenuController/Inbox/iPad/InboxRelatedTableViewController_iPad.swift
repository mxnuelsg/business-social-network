//
//  InboxRelatedTableViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas on 1/29/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class InboxRelatedTableViewController_iPad: UITableViewController
{
    //MARK: PROPERTIES & OUTLET
    var parentMessage: InboxMessage!
    var subMessages: [InboxMessage] = []
    var currentPageIndex = 1
    var pageSize = 20
    var totalPages = 0
    var onSelectedMessage:((_ msg: InboxMessage) -> ())?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.loadMessages()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "newInboxSent"), object: nil)
    }
    
    //MARK: LOAD CONFIG
    fileprivate func loadConfig()
    {
        //Title
        self.title = "Thread".localized()
        
        //Navigation bar items
        let barBtnNewInbox = UIBarButtonItem(image: UIImage(named: "iconInbox"), style: .plain, target: self, action: #selector(self.newMessage))
        navigationItem.rightBarButtonItem = barBtnNewInbox
        
        //Table config
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 87
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 0)
        self.tableView.hideEmtpyCells()
        self.tableView.displayBackgroundMessage("Loading...".localized(),
            subMessage: nil)
        
        //Notification
        NotificationCenter.default.addObserver(self, selector:#selector(self.newInboxSent(_:)), name: NSNotification.Name(rawValue: "newInboxSent"), object: nil)
    }
    
    // MARK: RELOAD INBOX
    func newInboxSent(_ notification: Notification)
    {
        self.currentPageIndex = 1
        self.loadMessages()
    }
    
    //MARK: LOAD MESSAGES
    func loadMessages()
    {
        LibraryAPI.shared.inboxBO.getInboxConversation(parameters: [
            "PagingInfo" : [
                "PageIndex" : self.currentPageIndex,
                "PageSize" : self.pageSize
            ],
            "Message" : self.parentMessage.getWSObject()
            ],
            onSuccess: { (messages) -> () in
                
                self.subMessages = messages
                self.tableView.reloadData()
                self.tableView.dismissBackgroundMessage()
                
            }, onError: { error in
                
                _ = self.navigationController?.popViewController(animated: true)
                
                MessageManager.shared.showBar(title: "Error",
                                              subtitle: "Error.TryAgain".localized(),
                                              type: .error,
                                              fromBottom: false)
        })
    }
    
    //MARK: ACTIONS
    func newMessage()
    {
        let vcNewMessage = Storyboard.getInstanceOf(NewMessageTableViewController.self)
        let navController = NavyController(rootViewController: vcNewMessage)
        navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
        
        self.present(navController, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.subMessages.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InboxMessageCell", for: indexPath) as! InboxTableViewCell
        
        let message = self.subMessages[indexPath.row]
        
        //Color for selected cell
        let selectedColor = UIView()
        selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
        cell.selectedBackgroundView = selectedColor
        cell.accessoryView = UIImageView(image: UIImage(named: "iconSwipeDots"))
        
        cell.setupForMessage(message)
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //Read Message
        let cell = tableView.cellForRow(at: indexPath) as! InboxTableViewCell
        cell.imgReadMark.image = nil
        
        //Show Detail
        if let selectedRow = tableView.indexPathForSelectedRow?.row {
            
            let selectedMessage = subMessages[selectedRow]
            self.onSelectedMessage?(selectedMessage)
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if indexPath.row == self.subMessages.count - 1
        {
            guard self.currentPageIndex < self.totalPages else {
                
                return
            }
            
            self.currentPageIndex += 1
            self.loadMessages()
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        let message = self.subMessages[indexPath.row]
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete".localized() , handler: { (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
            
            tableView.setEditing(false, animated: true)
            self.tableView.setEditing(false, animated: true)
            
            LibraryAPI.shared.inboxBO.deleteInboxMessage(message.id,
                onSuccess: { (message) -> () in
                    
                    self.subMessages.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                    
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error",
                                                  subtitle: "Error.TryAgain".localized(),
                                                  type: .error,
                                                  fromBottom: false)
            })
        })
        
        guard message.viewed == true else {
            
            return [deleteAction]
        }
        
        let markAsUnreadAction = UITableViewRowAction(style: .default, title: "Unread".localized(), handler: { (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
            
            tableView.setEditing(false, animated: true)
            self.tableView.setEditing(false, animated: true)
            
            LibraryAPI.shared.inboxBO.markasUnRead(parameters: message.getWSObject(),
                onSuccess: { (message) -> () in
                    
                    self.subMessages[indexPath.row].viewed = false
                    let cell = tableView.cellForRow(at: indexPath) as! InboxTableViewCell
                    cell.imgReadMark.image = InboxTableViewCell.imgUnreadMessage
                    cell.imgReadMark.layer.cornerRadius = 4
                    cell.imgReadMark.clipsToBounds = true

                    
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error",
                                                  subtitle: "Error.TryAgain".localized(),
                                                  type: .error,
                                                  fromBottom: false)
            })
        })
        
        markAsUnreadAction.backgroundColor = UIColor.lightGray
        
        return [deleteAction, markAsUnreadAction]
    }
}
