//
//  InboxViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas on 1/26/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class InboxViewController_iPad: UIViewController
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var containerDetail: UIView!
    @IBOutlet weak var lblMsgEmptyState: UILabel!
    
    var vcInboxMasterTable: InboxTableViewController_iPad!
    var vcDetail: InboxDetailTableViewController_iPad!
    var currentPageIndex = 1
    var pageSize = 20
    var totalPages = 0
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig_iPad()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.backButtonArrow()
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
    }
    
    //MARK: LOAD CONFIG
    fileprivate func loadConfig_iPad()
    {
        self.localize()
        
        //title
        self.view.backgroundColor = UIColor.grayTableBackground()
        
        
        //Navigation bar items
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        navigationItem.rightBarButtonItem = barBtnCompose
        
        //Notification
        NotificationCenter.default.addObserver(self, selector:#selector(self.reloadInbox), name: NSNotification.Name(rawValue: "newInboxSent"), object: nil)
        
        //Master Table
        self.vcInboxMasterTable.enableRefreshAction({
            
            self.currentPageIndex = 1
            self.loadInbox()
        })
        
        self.vcInboxMasterTable.tableView.hideEmtpyCells()
        
        self.vcInboxMasterTable.willDisplayLastRow = { [weak self] row in
            
            guard self?.currentPageIndex < self?.totalPages  else {
                
                return
            }
            
            self?.currentPageIndex += 1
            self?.loadInbox()
        }
        
        //Load Inbox
        self.loadInbox()
    }
    
    fileprivate func localize()
    {
        self.title = "INBOX".localized()
        self.lblMsgEmptyState.text = "No Message Selected".localized()
        self.vcInboxMasterTable.displayBackgroundMessage("Loading...".localized(),
                                                         subMessage: "")
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "newInboxSent"), object: nil)
    }
    
    //MARK: ACTIONS
    func createNewPost()
    {
        let vcNewPost = NewPostModal()
        
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
        
        self.present(navController, animated: true, completion: nil)
    }
    
    //MARK: LOAD INBOX
    func loadInbox()
    {
        LibraryAPI.shared.inboxBO.getInbox(parameters: [
            "PageIndex":self.currentPageIndex,
            "PageSize":self.pageSize
            ], onSuccess: { (messages, paging) -> () in
                
                if self.currentPageIndex == 1
                {
                    self.vcInboxMasterTable.refreshed()
                    self.vcInboxMasterTable.messages = messages
                    self.totalPages = Int(ceil(CGFloat(paging!.totalItems) / CGFloat(paging!.pageSize)))
                }
                else
                {
                    self.vcInboxMasterTable.messages += messages
                }
                
                self.vcInboxMasterTable.reload()
                
                //If a message is already selected, maintain that selection in the inboxMasterTable
                if let selectedMessage = self.vcDetail.message {
                    for (index, msg) in self.vcInboxMasterTable.messages.enumerated() where msg.id == selectedMessage.id
                    {
                        self.vcInboxMasterTable.tableView.selectRow(at: IndexPath(row: index, section: 0), animated: false, scrollPosition: .none)
                    }
                }
            }) { error in
                
                self.vcInboxMasterTable.dismissBackgroundMessage()
                self.vcInboxMasterTable.refreshed()
                
                MessageManager.shared.showBar(title: "Error",
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
        }
    }
    
    //MARK: - RELOAD INBOX
    func reloadInbox()
    {
        self.removeDetail()
        self.currentPageIndex = 1
        self.loadInbox()
    }
    
    //MARK: DETAIL
    func removeDetail()
    {
        self.containerDetail.isHidden = true
        
        self.vcDetail.message = nil
        self.vcDetail.loadMessage()
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "InboxTableViewController_iPad"
        {
            let navController = segue.destination as! UINavigationController
            navController.navigationBar.tintColor = UIColor.blackAsfalto()
            navController.setBorder()
            
            self.vcInboxMasterTable = navController.topViewController as! InboxTableViewController_iPad
            self.vcInboxMasterTable.onSelectedMessage = { [weak self] msg in
                
                self?.containerDetail.isHidden = false
                
                self?.vcDetail.message = msg
                self?.vcDetail.loadMessage()
            }
            
            self.vcInboxMasterTable.onRelatedMessage = { [weak self] parentMsj in
                
                let vcRelatedMessage = Storyboard.getInstanceOf(InboxRelatedTableViewController_iPad.self)
                vcRelatedMessage.parentMessage = parentMsj
                vcRelatedMessage.onSelectedMessage = {[weak self] msg in
                    
                    self?.containerDetail.isHidden = false
                    
                    self?.vcDetail.message = msg
                    self?.vcDetail.loadMessage()
                }
                
                self?.vcInboxMasterTable.navigationController?.pushViewController(vcRelatedMessage, animated: true)
            }
            
            self.vcInboxMasterTable.onUnreadOrDeleteMessage = { [weak self] Void in
                
                self?.removeDetail()
            }
        }
        
        if segue.identifier == "InboxDetailTableViewController_iPad"
        {
            self.vcDetail = segue.destination as! InboxDetailTableViewController_iPad
        }
    }
}
