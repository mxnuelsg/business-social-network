//
//  InboxDetailTableViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas on 2/1/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class InboxDetailTableViewController_iPad: UITableViewController
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblExpiration: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnReply: UIButton!
    
    var message: InboxMessage?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.loadMessage()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.backButtonArrow()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: LOAD CONFIG
    fileprivate func loadConfig()
    {
        self.localize()
        //Set title
        if let msj = self.message {
            
            title = msj.subject
        }
        else
        {
            title = "Detail".localized()
        }
        
        //Reply Button
        self.btnReply.layer.cornerRadius = 3
        self.btnReply.clipsToBounds = true
        self.btnReply.addTarget(self, action: #selector(self.reply), for: .touchUpInside)
        
        //TableView config
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.estimatedRowHeight = 37
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
        
        //If the message wasn't sent by myself
        if let msj = self.message, msj.hasReply == true {
            
            self.btnReply.isHidden = false
            self.btnReply.isEnabled = true
        }
        else
        {
            self.btnReply.isHidden = true
            self.btnReply.isEnabled = false
        }
    }
    
    fileprivate func localize()
    {
        self.btnReply.setTitle("REPLY".localized(), for: UIControlState())
        self.lblFrom.text = "From:".localized()
        self.lblTo.text = "To:".localized()
        self.lblSubject.text = "Subject".localized()
        self.lblExpiration.text = "Expiration days:".localized()
    }
    //MARK: LOAD MESSAGE
    func loadMessage()
    {
        guard self.message != nil else {
            
            self.lblFrom.text = String()
            self.lblTo.text =  String()
            self.lblDate.text =  String()
            self.lblSubject.text = String()
            self.lblMessage.text = String()
            self.lblExpiration.text = String()
            
            return
        }
        
        LibraryAPI.shared.inboxBO.getInboxDetail(self.message!.id,
            onSuccess: { (message) -> () in
                
                if message.expiration == 0
                {
                    //Update inbox list (message will dissapear from inbox table)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "newInboxSent"), object: nil)
                    //Update msg detail container visibility
                    (self.parent as! InboxViewController_iPad).containerDetail.isHidden = false
                }
                
                
                self.message = message
                self.loadInfo()
                
            }, onError: { error in
                
                _ = self.navigationController?.popViewController(animated: true)
                
                MessageManager.shared.showBar(title: "Error",
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
        })
    }
    
    //MARK: ACTIONS
    func loadInfo()
    {
        guard let msg = self.message else {
            
            return
        }
        
        //Fill information
        self.lblFrom.text = "From".localized() + ": \(msg.from.fullName)"
        self.lblTo.text = "To".localized() + ": \(msg.recipients.collapseNames())"
        self.lblDate.text = msg.date.getDateTimeStyle()
        self.lblSubject.text = "Subject".localized() + ": \(msg.subject)"
        self.lblMessage.text = msg.body
        
        //Show or not Reply button
        if msg.hasReply == true
        {
            self.btnReply.isHidden = false
            self.btnReply.isEnabled = true
        }
        else
        {
            self.btnReply.isHidden = true
            self.btnReply.isEnabled = false
        }
        
        //Expiration days
        if msg.expiration == -1
        {
            //Hide the field
            self.tableView.reloadData()
            return
        }
        else if msg.expiration == 0
        {
            //Once Read
            let onceRead =  "Once Read".localized()
            let expiration = "Expiration days:".localized() + " " + onceRead
            
            let attributedString = NSMutableAttributedString(string: expiration)
            attributedString.setBoldAndColor(onceRead, color: UIColor.black, size: 14)
            self.lblExpiration.attributedText = attributedString
        }
        else
        {
            if msg.expiration > 0 && msg.daysLeft == 0
            {
                //Today
                let today = "Today".localized()
                let expiration = "Expiration days:".localized() + " " + today
                
                let attributedString = NSMutableAttributedString(string: expiration)
                attributedString.setBoldAndColor(today, color: UIColor.black, size: 14)
                self.lblExpiration.attributedText = attributedString
                
            }
            else
            {
                //Number of days
                let days = (msg.daysLeft == 1) ? "Day".localized() : "Days".localized()
                let finalText = "\(msg.daysLeft) \(days)"
                let expiration = "Expiration days:".localized() + " " + finalText
                
                let attributedString = NSMutableAttributedString(string: expiration)
                attributedString.setBoldAndColor(finalText, color: UIColor.black, size: 14)
                self.lblExpiration.attributedText = attributedString
            }
        }
        
        self.tableView.reloadData()
    }
    
    func reply()
    {
        let vcNewMessage = Storyboard.getInstanceOf(NewMessageTableViewController.self)
        vcNewMessage.replyingMessage = message
        
        let navController = NavyController(rootViewController: vcNewMessage)
        navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
        
        self.present(navController, animated: true, completion: nil)
    }
    
    //MARK: TABLE VIEW DELEGATES
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        guard let msg = self.message else {
            
            return CGFloat.leastNormalMagnitude
        }
        
        //Hide specific rows or not
        if indexPath.row == 1 && msg.recipients.count == 0
        {
            return CGFloat.leastNormalMagnitude
        }
        else if indexPath.row == 3 && msg.expiration == -1
        {
            return CGFloat.leastNormalMagnitude
        }
        else if indexPath.row == 5 && msg.hasReply == false
        {
            return CGFloat.leastNormalMagnitude
        }
        else
        {
            return UITableViewAutomaticDimension
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 37
    }
}
