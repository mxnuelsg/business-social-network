//
//  InboxTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 9/21/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class InboxTableViewController: UITableViewController
{
    //MARK: PROPERTIES & OUTLET
    var messages: [InboxMessage] = []
    var messageSelected: InboxMessage?
    var willDisplayLastRow: ((_ row:Int) -> ())?

    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    //MARK: LOAD CONFIG
    func loadConfig()
    {
        title = "INBOX".localized()
        
        //Table config
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 87
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 0)
        tableView.hideEmtpyCells()
        tableView.displayBackgroundMessage("Loading...".localized(),
            subMessage: nil)
    }
    
    // MARK: - ACTIONS
    func newMessage()
    {
        let vcNewMessage = Storyboard.getInstanceOf(NewMessageTableViewController.self)
        let navyNewMessage = NavyController(rootViewController: vcNewMessage)
        
        present(navyNewMessage, animated: true, completion: nil)
    }
    
    func reload()
    {
        if messages.count == 0
        {
            displayBackgroundMessage("No messages".localized(),
                subMessage: "Pull to refresh".localized())
            return
        }
        
        dismissBackgroundMessage()
        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return messages.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let message = messages[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "InboxMessageCell", for: indexPath) as! InboxTableViewCell
        
        //Color for selected cell
        let selectedColor = UIView()
        selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
        cell.selectedBackgroundView = selectedColor
        cell.accessoryView = UIImageView(image: UIImage(named: "iconSwipeDots"))
        
        cell.setupForMessage(message)
        
        return cell
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let message = messages[indexPath.row]
        
        // SI TIENE UN THREAD/CONVERSACION
        if message.hasPrevious == true
        {
            LibraryAPI.shared.inboxBO.getInboxDetail(message.id,
                onSuccess: { (message) -> () in
                    
                    self.messages[tableView.indexPathForSelectedRow!.row].conversationId = message.conversationId
                    self.performSegue(withIdentifier: "segueID_RelatedMessages", sender: nil)
                    
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            })
        }
        else
        {
            let cell = tableView.cellForRow(at: indexPath) as! InboxTableViewCell
            cell.imgReadMark.image = nil
        }
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        let message = messages[indexPath.row]
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete".localized() , handler: { (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
            
            tableView.setEditing(false, animated: true)
            self.tableView.setEditing(false, animated: true)
            
            LibraryAPI.shared.inboxBO.deleteInboxMessage(message.id,
                onSuccess: { (message) -> () in
                    
                    self.messages.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                    
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            })
        })
        
        guard message.viewed == true else {
            
            return [deleteAction]
        }
        
        let markAsUnreadAction = UITableViewRowAction(style: .default, title: "Unread".localized(), handler: { (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
            
            tableView.setEditing(false, animated: true)
            self.tableView.setEditing(false, animated: true)
            
            LibraryAPI.shared.inboxBO.markasUnRead(parameters: message.getWSObject(),
                onSuccess: { (message) -> () in
                    
                    self.messages[indexPath.row].viewed = false
                    let cell = tableView.cellForRow(at: indexPath) as! InboxTableViewCell
                    cell.imgReadMark.image = InboxTableViewCell.imgUnreadMessage
                    cell.imgReadMark.layer.cornerRadius = 4
                    cell.imgReadMark.clipsToBounds = true
                    
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            })
        })
        
        markAsUnreadAction.backgroundColor = UIColor.lightGray
        
        return [deleteAction, markAsUnreadAction]
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if indexPath.row == messages.count - 1
        {
            willDisplayLastRow?(indexPath.row)
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segueID_ShowMessageDetail"
        {
            let msg = messages[tableView.indexPathForSelectedRow!.row]

            let tabBar = segue.destination as! UITabBarController
            tabBar.title = msg.subject.isEmpty == false ? msg.subject : "Detail".localized()
            
            let vcDetail = tabBar.viewControllers?.first as! InboxDetailTableViewController
            vcDetail.message = msg
        }
        
        if segue.identifier == "segueID_RelatedMessages"
        {
            let msg = messages[tableView.indexPathForSelectedRow!.row]
            
            let tabBar = segue.destination as! UITabBarController
            tabBar.title = "THREAD".localized()
            
            let vcRelated = tabBar.viewControllers?.first as! InboxRelatedTableViewController
            vcRelated.parentMessage = msg
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool
    {
        let message = messages[tableView.indexPathForSelectedRow!.row]
        
        if identifier == "segueID_ShowMessageDetail"
        {
            if message.hasPrevious == true
            {
                return false
            }
        }
        
        return true
    }
}
