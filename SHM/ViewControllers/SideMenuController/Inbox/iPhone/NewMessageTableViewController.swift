//
//  NewMessageTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 9/30/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class NewMessageTableViewController: UITableViewController, UITextViewDelegate
{
    //MARK: PROPERTIES & OUTLET
    @IBOutlet weak var txtTo: UITextView!
    @IBOutlet weak var lblTo: UILabel!
    
    @IBOutlet weak var imgAddTo: UIImageView!
    
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var txtSubject: UITextView!
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var lblExpiration: UILabel!
    @IBOutlet weak var txtExpiration: TextfieldBlockedMenu!
    
    var activityIndicator = UIActivityIndicatorView()
    var barBtnActivity: UIBarButtonItem!
    
    var heightVipFilter : CGFloat = 37
    var heightTo: CGFloat = 37
    var heightSubject: CGFloat = 37
    var heightMessage: CGFloat = 37
    var tableRecipients: InboxRecipientsTableViewController?
    var recipientsSelected = [Member]()
    var keyboardHeight = CGFloat(0)
    var replyingMessage: InboxMessage?
    var barBtnSend: UIBarButtonItem!
    var placeHolderText = "Inbox.Placeholder".localized()
    
    var deleteAfter = -1
    var arrayExpirationDates: [KeyValueObject] = []
    
    var isVipUser = false    
    @IBOutlet weak var lblVipFilter: UILabel!
    @IBOutlet weak var switchVIP: UISwitch!
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.loadExpirationDays()
        
        //Reply Message
        if let replyingMessage = replyingMessage {
            
            if replyingMessage.from.id == LibraryAPI.shared.currentUser!.id
            {
                recipientsSelected = replyingMessage.recipients
                txtTo.text = recipientsSelected.collapseNames()
            }
            else
            {
                print("Recipientes: \(replyingMessage.recipients.collapseNames())")
                print("From: \(replyingMessage.from.fullName)")
                
                recipientsSelected = [replyingMessage.from]
                recipientsSelected += replyingMessage.recipients
//                txtTo.text = replyingMessage.from.fullName
                txtTo.text = recipientsSelected.collapseNames()
            }
            
            _ = adjustSizeForTextView(txtTo)
            txtSubject.text = replyingMessage.subject
            txtTo.isEditable = false
            var reBody = "\n\n------------------------------------------"
            reBody += "\nFrom: \(replyingMessage.from.username)"
            reBody += "\nSent on: \(replyingMessage.date.getStringStyleMedium())"
            reBody += "\n\(replyingMessage.body)"
            txtMessage.text = reBody
            _ = adjustSizeForTextView(txtMessage)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        backButtonArrow()
        
        if recipientsSelected.count > 0
        {
            txtTo.text = recipientsSelected.collapseNames()
            _ = adjustSizeForTextView(txtTo)
        }
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    //MARK: LOAD CONFIG
    func loadConfig()
    {
        //Title
        title = "NEW MESSAGE".localized()
        
        //Nav bar items
        //Cancel
        let btnCancel = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.touchBtnCancel))
        
        self.navigationItem.leftBarButtonItem = btnCancel
        
        //Activity
        activityIndicator.activityIndicatorViewStyle = .white
        barBtnActivity = UIBarButtonItem(customView: activityIndicator)
        
        //Send
        barBtnSend = UIBarButtonItem(title: "Send".localized(), style: .done, target: self, action: #selector(self.touchBtnDone))
        self.navigationItem.rightBarButtonItem = barBtnSend
        
        //Tableview
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.estimatedRowHeight = 37
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
        
        //Labels & Texfield
        self.lblExpiration.text = "Delete message after...".localized()
        self.lblExpiration.adjustsFontSizeToFitWidth = true
        
        self.txtMessage.text = placeHolderText
        self.txtMessage.textColor = UIColor.lightGray
        
        //Set Toolbar
        txtTo.inputAccessoryView = loadToolBar()
        txtSubject.inputAccessoryView = loadToolBar()
        txtMessage.inputAccessoryView = loadToolBar()
        txtExpiration.inputAccessoryView = loadToolBar()
        
        
        self.lblSubject.text = "Subject:".localized()
        self.lblTo.text = "To:".localized()
        self.txtExpiration.text = "No Borrar".localized()
        self.lblVipFilter.text = "Filter by Vip".localized()
        //is VIP
        self.isVipUser = LibraryAPI.shared.currentUser?.role == .vipUser ? true : false
    }
    
    
    @IBAction func didChangeFilter(_ sender: Any)
    {
        self.tableRecipients?.onlyVipFilter = self.switchVIP.isOn
        self.tableRecipients?.tableView.reloadData()
    }
    
    //MARK: TOOLBAR
    func loadToolBar() -> UIToolbar
    {
        let toolBarKeyboard = UIToolbar()
        toolBarKeyboard.barStyle = UIBarStyle.blackOpaque;
        toolBarKeyboard.barTintColor = UIColor.colorForNavigationController()
        
        let barBtnDone = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(self.closeKeyboard))
        barBtnDone.tintColor = UIColor.white
        
        toolBarKeyboard.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            barBtnDone]
        
        toolBarKeyboard.sizeToFit()
        
        return toolBarKeyboard
    }
    
    func closeKeyboard()
    {
        view.endEditing(true)
    }
    
    //MARK: PICKER
    func loadPicker() -> UIPickerView
    {
        let pickerExpiration = UIPickerView()
        pickerExpiration.delegate = self
        pickerExpiration.dataSource = self
        pickerExpiration.backgroundColor = UIColor.groupTableViewBackground
        pickerExpiration.showsSelectionIndicator = true
        pickerExpiration.isMultipleTouchEnabled = false
        pickerExpiration.isExclusiveTouch = true
        
        return pickerExpiration
    }

    //MARK: LOAD EXPIRATION DAYS
    func loadExpirationDays()
    {
        LibraryAPI.shared.inboxBO.getExpirationDays({
            (expirations) -> () in
            
            self.arrayExpirationDates.removeAll()
            self.arrayExpirationDates = expirations
            
            self.txtExpiration.text = self.arrayExpirationDates[0].value
            self.deleteAfter = self.arrayExpirationDates[0].key
            
            }) { error in
                
                self.txtExpiration.isEnabled = false
                self.txtExpiration.text = "-------"
        }
    }
    //MARK: ACTIONS
    func touchBtnCancel()
    {
        self.view.endEditing(true)
        dismiss(animated: true, completion: nil)
    }
    
    func touchBtnDone()
    {
        guard self.recipientsSelected.count > 0 else {
            
             MessageManager.shared.showBar(title: "Warning".localized(),
                subtitle: "Add Recipients".localized(),
                type: .warning,
                fromBottom: false)
            
            return
        }
        
        guard self.txtSubject.text.isEmpty == false else {
            
             MessageManager.shared.showBar(title: "Warning".localized(),
                subtitle: "What's the subject?".localized(),
                type: .warning,
                fromBottom: false)
            
            return
        }
        
        guard self.txtMessage.text.trim().isEmpty == false && self.txtMessage.text != self.placeHolderText else {
            
             MessageManager.shared.showBar(title: "Warning".localized(),
                subtitle: "There's not text to send".localized(),
                type: .warning,
                fromBottom: false)
            
            return
        }
        
        //Indicator
        self.navigationItem.rightBarButtonItem = barBtnActivity
        activityIndicator.startAnimating()
        
        if let replyingMessage = replyingMessage {
            
            replyingMessage.body = txtMessage.text
            replyingMessage.recipients = self.recipientsSelected
            replyingMessage.expiration = deleteAfter
            
            LibraryAPI.shared.inboxBO.replyMessage(replyingMessage, onSuccess: {
                (message) -> () in
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "newInboxSent"), object: nil)
                
                //Indicator
                self.activityIndicator.stopAnimating()
                self.navigationItem.rightBarButtonItem = self.barBtnSend
                
                self.dismiss(animated: true, completion: {
                    
                    MessageManager.shared.showBar(title: "Success".localized(),
                        subtitle: "Message sent".localized(),
                        type: .success,
                        fromBottom: false)
                })
                }, onError: { error in
                    
                    //Indicator
                    self.activityIndicator.stopAnimating()
                    self.navigationItem.rightBarButtonItem = self.barBtnSend
                    
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            })
        }
        else
        {
            LibraryAPI.shared.inboxBO.postNewInboxMessage(txtMessage.text, subject: txtSubject.text, recipients: recipientsSelected, expiration: deleteAfter , onSuccess: {
                (message) -> () in
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "newInboxSent"), object: nil)
                
                //Indicator
                self.activityIndicator.stopAnimating()
                self.navigationItem.rightBarButtonItem = self.barBtnSend
                
                self.dismiss(animated: true, completion: {
                    
                    MessageManager.shared.showBar(title: "Success".localized(),
                        subtitle:"Message sent".localized(),
                        type: .success,
                        fromBottom: false)
                })
                }) { error in
                    
                    //Indicator
                    self.activityIndicator.stopAnimating()
                    self.navigationItem.rightBarButtonItem = self.barBtnSend
                    
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            }
        }
    }
    
    // MARK: - Private methods
    func adjustSizeForTextView(_ textView: UITextView) -> CGFloat
    {
        var textFrame = textView.frame
        let oldHeight = textFrame.size.height
        textFrame.size.height = textView.contentSize.height
        textView.frame = textFrame
        
        let newHeight = max(textFrame.size.height, 31)
        
        if textView == txtTo {
            heightTo = newHeight
        }
        else if textView == txtSubject {
            heightSubject = newHeight
        }
        else if textView == txtMessage {
            heightMessage = newHeight
        }
        
        if oldHeight != newHeight {
            tableView.beginUpdates()
            tableView.endUpdates()
        }

        return newHeight - oldHeight
    }
    
    func dismissRecipients() {
        self.tableRecipients?.recipients = []
        self.tableRecipients?.tableView.reloadData()
        self.tableRecipients?.view.isHidden = true
    }
    
    // MARK: KEYBOARD NOTIFICATION
    
    func keyboardWillShow(_ notification: Notification)
    {
        var info = notification.userInfo!
        let keyboardFrame = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        keyboardHeight = keyboardFrame.size.height
        //tableRecipients?.view.frame.size.height = self.view.bounds.size.height - (tableRecipients?.view.frame.origin.y ?? 0) - keyboardHeight
    }
    
    // MARK: - UITextViewDelegate

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        _ = adjustSizeForTextView(textView)
        return true
    }
    
    var lastLength = 0
    
    func textViewDidChange(_ textView: UITextView) {
        guard textView == txtTo else {
            return
        }
        
        var searchTerm = ""
        
        if let lastCommaRange = textView.text.range(of: ",", options: NSString.CompareOptions.backwards) {
            
            searchTerm = textView.text.substring(from: textView.text.index(lastCommaRange.lowerBound, offsetBy: 1)).trim()
        }
        else
        {
            searchTerm = textView.text.trim()
        }
        
        searchTerm = searchTerm.replacingOccurrences(of: " ,", with: "").trim()
        searchTerm = searchTerm.replacingOccurrences(of: ", ", with: " ").trim()
        
        if textView.text.characters.count < lastLength {
            let oldCount = recipientsSelected.count
            recipientsSelected = recipientsSelected.filter({ (self.txtTo.text as NSString).localizedCaseInsensitiveContains($0.fullName) })
            if oldCount != recipientsSelected.count {
                setRecipientsField()
            }
        }
        
        guard searchTerm != "" else {
            dismissRecipients()
            return
        }
        
        let firstCellHeight = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0))?.frame.height ?? 0
        let txtToHeight = self.txtTo.frame.height
        let yPosition = firstCellHeight + txtToHeight
        //let firstRowHeight = self.isVipUser == true ? 80 : 43
        if self.tableRecipients == nil {
            self.tableRecipients = InboxRecipientsTableViewController(style: .plain)
            self.tableRecipients?.onlyVipFilter = self.switchVIP.isOn
            self.addChildViewController(self.tableRecipients!)
            self.tableRecipients!.didMove(toParentViewController: self)
            self.tableRecipients!.view.frame = CGRect(x: 0, y: Int(yPosition), width: Int(self.view.bounds.size.width), height: Int(self.view.frame.size.height))
            self.view.addSubview(self.tableRecipients!.view)
            
            self.tableRecipients!.onSelectMember = { [weak self] member in
                
                // ADD TO RECIPIENTS IF IT WAS NOT THERE ALREADY
                if self?.recipientsSelected.filter({ $0.id == member.id }).count == 0
                {
                    self?.recipientsSelected.append(member)
                }
                
                self?.setRecipientsField()
                self?.dismissRecipients()
            }
            
            self.tableRecipients!.onSearchComplete = { [weak self] Void in
                
                if self?.tableRecipients!.recipients.count == 0 && self?.tableRecipients!.recipientsVIP.count == 0
                {
                    self?.dismissRecipients()
                }
                else
                {
                    self?.tableRecipients!.view.isHidden = false
                }
            }
        }
        else
        {
            self.tableRecipients!.view.frame = CGRect(x: 0, y: Int(yPosition), width: Int(self.view.bounds.size.width), height: Int(self.view.frame.size.height))
        }
        self.tableRecipients?.searchByText(searchTerm)
        
        lastLength = textView.text.characters.count
    }
    
    func setRecipientsField() {
        self.txtTo.text = self.recipientsSelected.count > 0 ? "\(self.recipientsSelected.collapseNames()), " : ""
        
        let diff = self.adjustSizeForTextView(self.txtTo)
        
        let firstCell = self.tableView.cellForRow(at: IndexPath(row: 1, section: 0))!
        
        self.tableRecipients?.view.frame.size.height = self.view.bounds.size.height - (self.tableRecipients?.view.frame.origin.y ?? 0) - self.keyboardHeight - diff
        self.tableRecipients?.view.frame.origin.y = firstCell.frame.height
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool
    {
        guard textView === txtMessage else {
            return true
        }
        
        self.txtMessage.textColor = UIColor.black
        
        if(self.txtMessage.text == placeHolderText) {
            self.txtMessage.text = ""
        }
  
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView === self.txtTo && self.recipientsSelected.count == 0
        {
            self.dismissRecipients()
        }
        guard textView === txtMessage else {
            return
        }
        
        if(textView.text == "") {
            self.txtMessage.text = placeHolderText
            self.txtMessage.textColor = UIColor.lightGray
        }
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return self.isVipUser == true ? heightVipFilter : 0
        }
        else if indexPath.row == 1
        {
            return heightTo
        }
        else if indexPath.row == 2
        {
            return heightSubject
        }
        else if indexPath.row == 3
        {
            return heightMessage
        }
        else
        {
            return 37
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 37
    }
}

//MARK: TEXTFIELD DELEGATE
extension NewMessageTableViewController: UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        textField.inputView  = loadPicker()
    }
}

//MARK: PICKER DELEGATE & DATASOURCE
extension NewMessageTableViewController: UIPickerViewDataSource, UIPickerViewDelegate
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return  self.arrayExpirationDates.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return self.arrayExpirationDates[row].value
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        self.txtExpiration.text = self.arrayExpirationDates[row].value
        self.deleteAfter = self.arrayExpirationDates[row].key
    }
}

class InboxRecipientsTableViewController: UITableViewController
{
    var recipients = [Member]()
    var recipientsVIP = [Member]()
    var onSelectMember: ((Member) -> ())?
    var onSearchComplete: (() -> ())?
    var isVipUser = false
    var onlyVipFilter = false
    override func viewDidLoad()
    {
        //is VIP user
        self.isVipUser = LibraryAPI.shared.currentUser?.role == .vipUser ? true : false
    }
    
    func searchByText(_ text: String)
    {
        LibraryAPI.shared.inboxBO.getInboxRecipientsByText(text, onSuccess: { (members) -> () in
            
            if self.isVipUser == true
            {
                self.recipients = members.filter({$0.isVip == false})
                self.recipientsVIP = members.filter({$0.isVip == true})
            }
            else
            {
                self.recipients = members
            }
            self.onSearchComplete?()
            self.tableView.reloadData()
            
            }) { error in
                
                MessageManager.shared.showBar(title: "Error",
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        //Two sections, one for VIPs other for Non-VIPs
        if self.isVipUser == true
        {
            return self.onlyVipFilter == true ? 1 : 2
        }
        else
        {
            return 1
        }
        
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        if self.isVipUser == true
        {
            if self.onlyVipFilter == true
            {
                return "VIP Users".localized()
            }
            else
            {
                return section == 0 ? "VIP Users".localized() : "Other Users".localized()
            }
        }
        else
        {
            return String()
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.isVipUser == true
        {
            if self.onlyVipFilter == true
            {
                return recipientsVIP.count
            }
            else
            {
                return section == 0 ? recipientsVIP.count : recipients.count
            }
        }
        else
        {
            return recipients.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard self.isVipUser == true else {
            
            let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
            let member = recipients[indexPath.row]
            cell.textLabel?.text = member.fullName
            
            return cell
        }
        
        guard self.onlyVipFilter == false else {
            
            let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
            let member = recipientsVIP[indexPath.row]
            cell.textLabel?.text = member.fullName
            
            return cell

        }
        
        if indexPath.section == 0
        {
            let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
            let member = recipientsVIP[indexPath.row]
            cell.textLabel?.text = member.fullName
            
            return cell
        }
        else
        {
            let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
            let member = recipients[indexPath.row]
            cell.textLabel?.text = member.fullName
            
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        guard self.isVipUser == true else {
        
            let member = recipients[indexPath.row]
            onSelectMember?(member)
            
            return
        }
        
        if indexPath.section == 0
        {
            let member = recipientsVIP[indexPath.row]
            onSelectMember?(member)
        }
        else
        {
            let member = recipients[indexPath.row]
            onSelectMember?(member)
        }
    }
}

