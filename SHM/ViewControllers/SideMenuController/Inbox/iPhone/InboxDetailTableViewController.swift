//
//  InboxDetailViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 9/21/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class InboxDetailTableViewController: UITableViewController, UITabBarControllerDelegate
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
     @IBOutlet weak var lblExpiration: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    var message: InboxMessage?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.localize()
        
        //Set title
        if let msj = message
        {
            title = msj.subject
        }
        else
        {
            title = "Detail".localized()
        }
        
        //TabBar config
        self.tabBarController?.tabBar.barTintColor = UIColor(red: 0.996078, green: 0.996078, blue: 0.996078, alpha: 1)
        self.tabBarController?.tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        self.tabBarController?.tabBar.items?.first?.title = "New Message".localized() 
        self.tabBarController?.backButtonArrow()
        self.tabBarController?.delegate = self

        //TableView config
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.estimatedRowHeight = 37
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
        
        //If the message wasn't sent by myself
        if let msj = message, msj.hasReply == true
        {
            let barBtnReply = UIBarButtonItem(barButtonSystemItem: .reply, target: self, action: #selector(self.reply))
            self.tabBarController?.navigationItem.rightBarButtonItem = barBtnReply
        }
        
        self.loadMessage()
    }

    fileprivate func localize()
    {
        if DeviceType.IS_ANY_IPAD == false
        {
            self.lblExpiration.text = "Expiration days:".localized()
            self.lblSubject.text = "Subject".localized()
            self.lblTo.text = "To:".localized()
            self.lblFrom.text = "From:".localized()
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: LOAD MESSAGE
    func loadMessage()
    {
        guard message != nil else {
            
            self.lblFrom.text = String()
            self.lblTo.text =  String()
            self.lblDate.text =  String()
            self.lblSubject.text = String()
            self.lblMessage.text = String()
            self.lblExpiration.text = String()
            
            return
        }
        
        LibraryAPI.shared.inboxBO.getInboxDetail(message!.id,
            onSuccess: { (message) -> () in
                
                //Update inbox list
                NotificationCenter.default.post(name: Notification.Name(rawValue: "newInboxSent"), object: nil)
                
                self.message = message
                self.loadInfo()
                
            }, onError: { error in
                
                _ = self.navigationController?.popViewController(animated: true)
                
                 MessageManager.shared.showBar(title: "Error",
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
        })
    }
    
    //MARK: ACTIONS
    func newMessage()
    {
        let vcNewMessage = Storyboard.getInstanceOf(NewMessageTableViewController.self)
        let navyNewMessage = NavyController(rootViewController: vcNewMessage)
        
        present(navyNewMessage, animated: true, completion: nil)
    }
    
    func loadInfo()
    {
        guard let msg = message else {
            
            return
        }
        
        //Fill information
        self.lblFrom.text = "From".localized() + ": \(msg.from.fullName)"
        self.lblTo.text = "To".localized() + ": \(msg.recipients.collapseNames())"
        self.lblDate.text = msg.date.getDateTimeStyle()
        self.lblSubject.text = "Subject".localized() + ": \(msg.subject)"
        self.lblMessage.text = msg.body
        
        //Expiration days
        if msg.expiration == -1
        {
            //Hide the field
            self.tableView.reloadData()
            return
        }
        else if msg.expiration == 0
        {
            //Once Read
            let onceRead =  "Once Read".localized()
            let expiration = "Expiration days:".localized() + " " + onceRead
            
            let attributedString = NSMutableAttributedString(string: expiration)
            attributedString.setBoldAndColor(onceRead, color: UIColor.black, size: 14)
            self.lblExpiration.attributedText = attributedString
        }
        else
        {
            if msg.expiration > 0 && msg.daysLeft == 0
            {
                //Today
                let today = "Today".localized()
                let expiration = "Expiration days:".localized() + " " + today
                
                let attributedString = NSMutableAttributedString(string: expiration)
                attributedString.setBoldAndColor(today, color: UIColor.black, size: 14)
                self.lblExpiration.attributedText = attributedString

            }
            else
            {
                //Number of days
                let days = (msg.daysLeft == 1) ? "Day".localized() : "Days".localized()
                let finalText = "\(msg.daysLeft) \(days)"
                let expiration = "Expiration days:".localized() + " " + finalText
                
                let attributedString = NSMutableAttributedString(string: expiration)
                attributedString.setBoldAndColor(finalText, color: UIColor.black, size: 14)
                self.lblExpiration.attributedText = attributedString
            }
        }
        
        self.tableView.reloadData()
    }
    
    func reply()
    {
        let vcNewMessage = Storyboard.getInstanceOf(NewMessageTableViewController.self)
        vcNewMessage.replyingMessage = self.message
        
        let navyNewMessage = NavyController(rootViewController: vcNewMessage)
        present(navyNewMessage, animated: true, completion: nil)
    }
    
    //MARK: TABLE VIEW DELEGATES
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        guard let msg = self.message else {
            
            return CGFloat.leastNormalMagnitude
        }
        
        //Hide specific rows or not
        if indexPath.row == 1 && message!.recipients.count == 0
        {
            return CGFloat.leastNormalMagnitude
        }
        else if indexPath.row == 3 && msg.expiration == -1
        {
            return CGFloat.leastNormalMagnitude
        }
        else
        {
            return UITableViewAutomaticDimension
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 37
    }
    
    //MARK: TAB BAR CONTROLLER
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController)
    {
        tabBarController.tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        self.newMessage()
    }
}
