//
//  InboxRelatedTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 10/5/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class InboxRelatedTableViewController: UITableViewController, UITabBarControllerDelegate
{
    //MARK: PROPERTIES & OUTLET
    var parentMessage: InboxMessage!
    var subMessages: [InboxMessage] = []
    var currentPageIndex = 1
    var pageSize = 20
    var totalPages = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "THREAD".localized()
        
        
        //TabBar config
        self.tabBarController?.tabBar.barTintColor = UIColor(red: 0.996078, green: 0.996078, blue: 0.996078, alpha: 1)
        self.tabBarController?.tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        self.tabBarController?.tabBar.items?.first?.title = "New Message".localized()
        self.tabBarController?.backButtonArrow()
        self.tabBarController?.delegate = self

        
        //Table config
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 87
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 0)
        tableView.hideEmtpyCells()
        tableView.displayBackgroundMessage("Loading...".localized(),
            subMessage: nil)
        
        loadMessages()
        
        NotificationCenter.default.addObserver(self, selector:#selector(InboxRelatedTableViewController.newInboxSent(_:)), name: NSNotification.Name(rawValue: "newInboxSent"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "newInboxSent"), object: nil)
    }
    
    // MARK: RELOAD INBOX
    func newInboxSent(_ notification: Notification)
    {
        self.currentPageIndex = 1
        loadMessages()
    }
    
    //MARK: LOAD MESSAGES
    func loadMessages()
    {
        LibraryAPI.shared.inboxBO.getInboxConversation(parameters: [
            "PagingInfo" : [
                "PageIndex" : currentPageIndex,
                "PageSize" : pageSize
            ],
            "Message" : parentMessage.getWSObject()
            ],
            onSuccess: { (messages) -> () in
                
                self.subMessages = messages
                self.tableView.reloadData()
                self.tableView.dismissBackgroundMessage()
                
            }, onError: { error in
                
                _ = self.navigationController?.popViewController(animated: true)
                
                 MessageManager.shared.showBar(title: "Error",
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
        })
    }
    
    //MARK: ACTIONS
    func newMessage()
    {
        let vcNewMessage = Storyboard.getInstanceOf(NewMessageTableViewController.self)
        let navyNewMessage = NavyController(rootViewController: vcNewMessage)
        
        present(navyNewMessage, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return subMessages.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InboxMessageCell", for: indexPath) as! InboxTableViewCell
        
        let message = self.subMessages[indexPath.row]
        
        //Color for selected cell
        let selectedColor = UIView()
        selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
        cell.selectedBackgroundView = selectedColor
        cell.accessoryView = UIImageView(image: UIImage(named: "iconSwipeDots"))
        
        cell.setupForMessage(message)
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell = tableView.cellForRow(at: indexPath) as! InboxTableViewCell
        cell.imgReadMark.image = nil
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if indexPath.row == subMessages.count - 1
        {
            guard self.currentPageIndex < self.totalPages  else {
                
                return
            }
            
            self.currentPageIndex += 1
            self.loadMessages()
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        let message = self.subMessages[indexPath.row]
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete".localized() , handler: { (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
            
            tableView.setEditing(false, animated: true)
            self.tableView.setEditing(false, animated: true)
            
            LibraryAPI.shared.inboxBO.deleteInboxMessage(message.id,
                onSuccess: { (message) -> () in
                    
                    self.subMessages.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                    
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            })
        })
        
        guard message.viewed == true else {
            
            return [deleteAction]
        }
        
        let markAsUnreadAction = UITableViewRowAction(style: .default, title: "Unread".localized(), handler: { (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
            
            tableView.setEditing(false, animated: true)
            self.tableView.setEditing(false, animated: true)
            
            LibraryAPI.shared.inboxBO.markasUnRead(parameters: message.getWSObject(),
                onSuccess: { (message) -> () in
                    
                    self.subMessages[indexPath.row].viewed = false
                    let cell = tableView.cellForRow(at: indexPath) as! InboxTableViewCell
                    cell.imgReadMark.image = InboxTableViewCell.imgUnreadMessage
                    cell.imgReadMark.layer.cornerRadius = 4
                    cell.imgReadMark.clipsToBounds = true

                    
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            })
        })
        
        markAsUnreadAction.backgroundColor = UIColor.lightGray
        
        return [deleteAction, markAsUnreadAction]
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier! == "segueID_ShowMessageDetail"
        {
            let msg = subMessages[tableView.indexPathForSelectedRow!.row]
            
            let tabBar = segue.destination as! UITabBarController
            tabBar.title = msg.subject.isEmpty == false ? msg.subject : "Detail".localized()
            
            let vcDetail = tabBar.viewControllers?.first as! InboxDetailTableViewController
            vcDetail.message = msg
        }
    }
    
    //MARK: TAB BAR CONTROLLER
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController)
    {
        tabBarController.tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        self.newMessage()
    }
}
