//
//  InboxViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 9/21/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class InboxViewController: UIViewController
{
    //MARK: PROPERTIES & OUTLETS
    var vcInboxTable: InboxTableViewController!
    
    var currentPageIndex = 1
    var pageSize = 20
    var totalPages = 0

    @IBOutlet weak var tabBarItemNewMessage: UITabBarItem!
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //localizations
        self.localize()
        
        self.vcInboxTable.tableView.enableRefreshAction({
            self.currentPageIndex = 1
            self.loadInbox()
        })
        
        self.vcInboxTable.tableView.hideEmtpyCells()
        self.vcInboxTable.tableView.displayBackgroundMessage("Loading...".localized(), subMessage: "")
        self.vcInboxTable.willDisplayLastRow = { [weak self] row in
            
            guard self?.currentPageIndex < self?.totalPages  else {
                return
            }
            
            self?.currentPageIndex += 1
            self?.loadInbox()
        }
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.newInboxSent(_:)), name: NSNotification.Name(rawValue: "newInboxSent"), object: nil)
        
        self.loadInbox()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        self.vcInboxTable.tableView.setContentOffset(CGPoint.zero, animated: false)
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "newInboxSent"), object: nil)
    }
    
    //MARK: - LOCALIZATIONS
    fileprivate func localize()
    {
        if DeviceType.IS_ANY_IPAD == false
        {
            self.tabBarItemNewMessage.title = "New Message".localized()
            self.title = "INBOX".localized()
        }
    }
    
    //MARK: - RELOAD INBOX
    func newInboxSent(_ notification: Notification)
    {
        self.currentPageIndex = 1
        self.loadInbox()
    }
    
    //MARK: LOAD INBOX
    func loadInbox()
    {
        LibraryAPI.shared.inboxBO.getInbox(parameters: [
                "PageIndex":self.currentPageIndex,
                "PageSize":self.pageSize
            ], onSuccess: {
                (messages, paging) -> () in
                
            if self.currentPageIndex == 1
            {
                self.vcInboxTable.tableView.refreshed()
                self.vcInboxTable.messages = messages
                self.totalPages = Int(ceil(CGFloat(paging!.totalItems) / CGFloat(paging!.pageSize)))
            }
            else
            {
                self.vcInboxTable.messages += messages
            }
                
            self.vcInboxTable.reload()
            
            }) { error in
                
                self.vcInboxTable.tableView.dismissBackgroundMessage()
                self.vcInboxTable.tableView.refreshed()
                
                MessageManager.shared.showBar(title: "Error",
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "InboxTableViewController"
        {
            self.vcInboxTable = segue.destination as! InboxTableViewController
        }
    }
}

// MARK: - UITabBarDelegate
extension InboxViewController: UITabBarDelegate
{
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        //Selected item color equal not selected color
        tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        
        if item.tag == 0
        {
            let vcNewMessage = Storyboard.getInstanceOf(NewMessageTableViewController.self)
            let navyNewMessage = NavyController(rootViewController: vcNewMessage)
            
            present(navyNewMessage, animated: true, completion: nil)
        }
    }
}
