//
//  ActorsTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/13/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class StakeholdersTableViewController: UITableViewController
{
    var stakeholders: [Stakeholder] {
        didSet {
            stakeholders = stakeholders.sorted(by: { $0.fullName < $1.fullName })
        }
    }
    var sectionedStakeholders: [[Stakeholder]]?
    var sectionTitles: [String]?
    var hidePosts = false
    var stakeholderSelected: Stakeholder?
    var onStakeholderSelected: ((_ selectedStakeholder: Stakeholder) -> ())?
    
    var onActorTap: ((_ stakeholder: Stakeholder) -> Void)?
    var willDisplayLastRow: ((_ row:Int) -> ())?
    var stakeholdersType = FollowStatus.following
    
    class func getNewInstance() -> StakeholdersTableViewController
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "StakeholdersTableViewController") as! StakeholdersTableViewController
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        stakeholders = [Stakeholder]()
        super.init(coder: aDecoder)
    }
    
    // MARK: - LIFE CLYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
            
        tableView.hideEmtpyCells()
        tableView.register(UINib(nibName: "ContactListCell", bundle: nil), forCellReuseIdentifier: "ContactListCell")
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        backButtonArrow()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        print("adios ActorList")
    }
    
    // MARK: PRIVATE METHODS
    func indexStakeholders()
    {
        sectionedStakeholders = [[Stakeholder]]()
        
        // load an array of the first letters
        let firstLetters = stakeholders.map({ String($0.fullName.uppercased()[$0.fullName.uppercased().startIndex]) })
        
        // get the unique letters
        sectionTitles = Array(Set(firstLetters))
        
        // sort
        sectionTitles = sectionTitles?.sorted(by: { $0 < $1 })
        
        // fill array of arrays
        for firstLetter in sectionTitles! {
            sectionedStakeholders!.append(stakeholders.filter({ String($0.fullName.uppercased()[$0.fullName.uppercased().startIndex]) == firstLetter }))
        }
    }
    
    func clear() {
        stakeholders = []
        sectionedStakeholders = [[Stakeholder]]()
        sectionTitles = []
        tableView.reloadData()
    }
    
    func reload()
    {
        if stakeholders.count == 0
        {
            tableView.displayBackgroundMessage("No results".localized(),
                subMessage: "Pull to refresh".localized())
        }
        else
        {
            tableView.dismissBackgroundMessage()
        }
        
        tableView.reloadData()
    }
    
    func pushActorDetail()
    {
        guard let parentViewController = self.presentingViewController else {
            
            let vcSHDetail: SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
            vcSHDetail.stakeholder = stakeholderSelected ?? Stakeholder(isDefault:true)
            self.navigationController?.pushViewController(vcSHDetail, animated: true)
            return
        }
        
        if DeviceType.IS_ANY_IPAD == true
        {
            self.presentDetail_iPad()
        }
        else
        {
            self.presentDetail()
        }
    }
    
    fileprivate func presentDetail()
    {
        let stakeholderDetailVC : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder")
        stakeholderDetailVC.stakeholder = stakeholderSelected ?? Stakeholder(isDefault:true)
        
        self.presentingViewController?.dismiss(animated: true, completion: {
            if let sideMenuVC = self.presentingViewController as? SWRevealViewController {
                (sideMenuVC.frontViewController as! UINavigationController).pushViewController(stakeholderDetailVC, animated: true)
            }
            else {
                self.presentingViewController?.navigationController!.pushViewController(stakeholderDetailVC, animated: true)
            }
        })
    }
    
    fileprivate func presentDetail_iPad()
    {
        let stakeholderDetailVC = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
        stakeholderDetailVC.stakeholder = stakeholderSelected
        
        self.presentingViewController?.dismiss(animated: true, completion: {
            if let sideMenuVC = self.presentingViewController as? SWRevealViewController {
                (sideMenuVC.frontViewController as! UINavigationController).pushViewController(stakeholderDetailVC, animated: true)
            }
            else {
                self.presentingViewController?.navigationController!.pushViewController(stakeholderDetailVC, animated: true)
            }
        })
    }
    func pushNewPost(_ stakeholderTag: Stakeholder!)
    {
        let vcNewPost = NewPostViewController()
        vcNewPost.stakeholderTagged = stakeholderTag
        
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = (DeviceType.IS_ANY_IPAD) ?  UIModalPresentationStyle.formSheet :  UIModalPresentationStyle.overFullScreen
        
        present(navController, animated: true, completion: nil)
    }
    
    fileprivate func pushRatingControlFor(_ stakeholder: Stakeholder)
    {
        let vcRating : RatingViewController = Storyboard.getInstanceFromStoryboard("Rating")
        vcRating.didEndRating = { [weak self] in
            
            self?.setEditing(false, animated: true)
            //Update Stakeholder List
            NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadStakeholdersList"), object: nil)
        }
        vcRating.stakeholder = stakeholder
        self.present(vcRating, animated: true, completion: nil)
    }
    
    func deleteIndexPath(_ indexPath: IndexPath)
    {
        if self.sectionedStakeholders != nil
        {
            if self.sectionedStakeholders?[indexPath.section].count == 1
            {
                self.sectionedStakeholders?.remove(at: indexPath.section)
                self.sectionTitles?.remove(at: indexPath.section)
                self.tableView.deleteSections(IndexSet(integer: indexPath.section), with: UITableViewRowAnimation.top)
            }
            else
            {
                self.sectionedStakeholders?[indexPath.section].remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.top)
            }
        }
        else
        {
            self.stakeholders.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.top)
        }
    }
    
    // MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segueID_ViewStakeholderDetail"
        {
            let viewDetailVC = segue.destination as! StakeholderDetailViewController
            viewDetailVC.stakeholder = stakeholderSelected!
        }
    }
}

// MARK: TABLE VIEW DATASOURCE
extension StakeholdersTableViewController
{
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return sectionedStakeholders?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let lblSectionTitle = SHMSectionTitleLabel()
        lblSectionTitle.text = sectionTitles?[section] ?? ""
        lblSectionTitle.backgroundColor = .groupTableViewBackground
        
        return lblSectionTitle
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return sectionedStakeholders?[section].count ?? stakeholders.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return sectionTitles?[section] ?? nil
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        let stakeholder = sectionedStakeholders?[indexPath.section][indexPath.row] ?? stakeholders[indexPath.row]
        
        //POST ACTION
        let actionPost = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Post".localized() , handler: { (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
            
            tableView.setEditing(false, animated: true)
            self.pushNewPost(stakeholder)
            self.tableView.setEditing(false, animated: true)
        })
        actionPost.backgroundColor = UIColor.blueColorNewPost()
        
        //RATING
        //POST ACTION
        let actionRating = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Rating".localized() , handler: { (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
            
            self.pushRatingControlFor(stakeholder)
        })
        actionRating.backgroundColor = UIColor.yellowFriends()
        //FOLLOW/UNFOLLOW
        //Verify Status
        if stakeholder.isFollow == FollowStatus.following
        {
            let unfollowAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Unfollow".localized() , handler: { (action: UITableViewRowAction!, indexPath: IndexPath!) -> Void in
                
                LibraryAPI.shared.stakeholderBO.unfollowStakeholder(stakeholder,
                                                                    onSuccess: { Void in
                                                                        
                                                                        self.tableView.setEditing(false, animated: true)
                                                                        
                                                                        //Update Stakeholder List
                                                                        NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadStakeholdersList"), object: nil)
                                                                        
                                                                        let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                                                        DispatchQueue.main.asyncAfter(deadline: delayTime) {
                                                                            
                                                                            let info = "Now, you're not following".localized() + " " + stakeholder.fullName
                                                                            
                                                                            MessageManager.shared.showBar(title: "Info".localized(), subtitle: info, type: .info, fromBottom: false)
                                                                        }
                                                                        
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error", subtitle: "Error.TryAgain".localized(), type: .error, fromBottom: false)
                })
            })
            
            unfollowAction.backgroundColor = UIColor.redUnfollowAction()
            
            return stakeholder.isEnable == true ? [unfollowAction,actionPost,actionRating] : [unfollowAction, actionRating]
        }
        else
        {
            let followAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Follow".localized() , handler: { (action: UITableViewRowAction!, indexPath: IndexPath!) -> Void in
                
                LibraryAPI.shared.stakeholderBO.followStakeholder(stakeholder,
                                                                  onSuccess: { Void in
                                                                    
                                                                    self.tableView.setEditing(false, animated: true)
                                                                    
                                                                    //Update Stakeholder List
                                                                    NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadStakeholdersList"), object: nil)
                                                                    
                                                                    let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                                                    DispatchQueue.main.asyncAfter(deadline: delayTime) {
                                                                        
                                                                        let info = "Now, you're following".localized() + " " + stakeholder.fullName
                                                                        
                                                                        MessageManager.shared.showBar(title: "Success".localized(), subtitle: info, type: .success, fromBottom: false)
                                                                    }
                                                                    
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error", subtitle: "Error.TryAgain".localized(), type: .error, fromBottom: false)
                })
            })
            
            followAction.backgroundColor = UIColor.greenFollowAction()
            
            return stakeholder.isEnable == true ? [followAction, actionPost,actionRating] : [followAction, actionRating]
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListCell") as! ContactListCell
        let stakeholder = sectionedStakeholders?[indexPath.section][indexPath.row] ?? stakeholders[indexPath.row]
        
        //Color for selected cell
        let selectedColor = UIView()
        selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
        cell.selectedBackgroundView = selectedColor
        cell.accessoryView = UIImageView(image: UIImage(named: "iconSwipeDots"))
        
        cell.cellForStakeholder(stakeholder)
        cell.didTapOnRating = {[weak self] void in
            
            self?.pushRatingControlFor(stakeholder)
        }
        if self.stakeholdersType == .all
        {
            cell.imgFollowed.isHidden = stakeholder.isFollow == FollowStatus.following ? false : true
        }
        else
        {
            cell.imgFollowed.isHidden = true
        }
        
        return cell
    }
}

// MARK: TABLE VIEW DELEGATE
extension StakeholdersTableViewController
{
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        self.stakeholderSelected = self.sectionedStakeholders?[indexPath.section][indexPath.row] ?? self.stakeholders[indexPath.row]
        
        if let actionOnTap = onStakeholderSelected
        {
            actionOnTap(stakeholderSelected!)
        }
        else
        {
            self.pushActorDetail()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return self.sectionedStakeholders != nil ? 24 : 0
    }

    override func sectionIndexTitles(for tableView: UITableView) -> [String]?
    {
        return self.sectionTitles
    }
    
    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int
    {
        return index
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return nil
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let lastStakeholder = sectionedStakeholders?.last?.last
        let currentStakeholder = sectionedStakeholders?[indexPath.section][indexPath.row]
        
        if lastStakeholder?.id == currentStakeholder?.id
        {
            self.willDisplayLastRow?(indexPath.row)
        }
        
        // Remove seperator inset
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset))
        {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))
        {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.responds(to: #selector(setter: UIView.layoutMargins))
        {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        view.endEditing(true)
    }
}

extension Array where Element: Equatable
{
    func arrayRemovingObject(_ object: Element) -> [Element]
    {
        return filter { $0 != object }
    }
}
