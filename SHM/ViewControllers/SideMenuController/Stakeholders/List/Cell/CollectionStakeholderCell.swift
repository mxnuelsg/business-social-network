//
//  CollectionStakeholderCell.swift
//  SHM
//
//  Created by Manuel Salinas on 1/4/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class CollectionStakeholderCell: UICollectionViewCell
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgFollowed: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblJobPosition: UILabel!
    @IBOutlet weak var lblSite: UILabel!
    @IBOutlet weak var btnMore: UIButton!    
    @IBOutlet weak var lblUserRating: UILabel!
    @IBOutlet weak var lblAdminRating: UILabel!
    
    @IBOutlet weak var viewTapRating: UIView!
    var onButtonTap:((_ button: UIButton) -> ())?
    var didTapOnRating:(() -> ())?
    
    //MARK: LIFE CYCLE
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        self.lblJobPosition.adjustsFontSizeToFitWidth = true
        self.lblSite.adjustsFontSizeToFitWidth = true
        self.btnMore.setBorder()
        self.setBorder()
        self.lblSite.isHidden = true
        //Tap on rating control
        self.viewTapRating.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapOnRating)))
    }
        
    @objc fileprivate func tapOnRating()
    {
        self.didTapOnRating?()
    }
    //MARK: ACTIONS
    @IBAction func openOptions(_ sender: UIButton)
    {
        self.onButtonTap?(sender)
    }
    
    func setSite(_ site: Site?)
    {
        if let stakeholderSite = site, stakeholderSite.title.isEmpty == false {
            
            self.lblSite.text = "⚑ \(stakeholderSite.title)"
        }
        else
        {
            self.lblSite.text = String()
        }
    }
    
    func setNationalities(_ stakeholder: Stakeholder)
    {
        //Nationalities or  Sites
        if stakeholder.nationalities.count > 0
        {
            let nationNames = stakeholder.nationalities.map({$0.value})
            let nationalities = "⚑ \(nationNames.collapseWithCommas())"
            let siteTitleAtt = NSMutableAttributedString(string: nationalities)
            siteTitleAtt.setColor(nationalities, color: UIColor.grayConcreto())
            siteTitleAtt.setBold(nationalities, size: 11)
            self.lblSite.attributedText = siteTitleAtt
        }
        else if let site = stakeholder.site, site.title.isEmpty == false {
            
            //Sites
            let siteTitle = "⚑ \(site.title)"
            let siteTitleAtt = NSMutableAttributedString(string: siteTitle)
            siteTitleAtt.setColor(siteTitle, color: UIColor.grayConcreto())
            siteTitleAtt.setBold(siteTitle, size: 11)
            self.lblSite.attributedText = siteTitleAtt
        }
        else
        {
            self.lblSite.text = String()
        }
        
    }
}
