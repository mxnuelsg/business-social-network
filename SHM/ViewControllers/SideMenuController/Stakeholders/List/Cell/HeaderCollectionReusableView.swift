//
//  HeaderCollectionReusableView.swift
//  SHM
//
//  Created by Manuel Salinas on 1/5/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class HeaderCollectionReusableView: UICollectionReusableView
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK: LIFE CYCLE
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
}
