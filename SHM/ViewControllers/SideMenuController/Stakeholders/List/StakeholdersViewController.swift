//
//  ActorsViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 7/24/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import Spruce

enum SearchGetListType
{
    case none
    case followingStatus_NoPagination_NoSearchText
    case followingStatus_NoPagination_SearchText
    case followingStatus_Pagination_SearchText
    case followingStatus_Pagination_NoSearchText
}

enum TabBarOption: Int
{
    case add = 0
    case filter = 1
    case search = 2
}

class StakeholdersViewController: UIViewController
{
    @IBOutlet weak var segmentedStakeholders: UISegmentedControl!
    @IBOutlet weak var constraintContainerTop: NSLayoutConstraint!
    @IBOutlet weak var tabBarItemAddStakeholder: UITabBarItem!
    //@IBOutlet weak var tabBarItemFilterStakeholder: UITabBarItem!
    @IBOutlet weak var tabBarItemSearchStakeholder: UITabBarItem!
    @IBOutlet weak var navigationItemStakeholder: UINavigationItem!
    @IBOutlet weak var tabBar: UITabBar!
    
    var vcStakeholderTable: StakeholdersTableViewController!
    
    var stakeholders = [Stakeholder]()
    var filterSites = [Site]()
    var selectedFilter = 0
    var isSearchByText = false
    var isPagination = false
    var filtersExist = false
    
    var searchPaging = Paging()
    var isSearching = false
    fileprivate lazy var searchBar: AutoSearchBar = AutoSearchBar()
    
    var dataDidLoad: (() -> ())?
    
    // MARK: - LIFE CLYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.loadConfig()
        
        self.getReadySearch(.followingStatus_NoPagination_NoSearchText)
        self.getStakeholderList()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        self.searchBar.isActive = false
        //Cancel previous request
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.GetStakeholderList)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "reloadStakeholdersList"), object: nil)
    }
    
    //MARK: LOAD CONFIG
    fileprivate func loadConfig()
    {
        //Localizations
        self.localize()
        
        self.setupNavigation(animated:false)
        self.loadConfigStakeholdersTable()
        self.loadConfigSearchControl()
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.getStakeholderList), name: NSNotification.Name(rawValue: "reloadStakeholdersList"), object: nil)
        
        //Segemented Control
        if showSplitList() == true
        {
            self.segmentedStakeholders.isHidden = false
            self.constraintContainerTop.constant = 44
        }
        
        self.segmentedStakeholders.selectedSegmentIndex = 0
        self.segmentedStakeholders.addTarget(self, action: #selector(self.segmentedControlHandler(_:)), for: .valueChanged)
        
    }
    
    fileprivate func localize()
    {
        if DeviceType.IS_ANY_IPAD == false
        {
            self.tabBarItemSearchStakeholder.title = "Search Stakeholders".localized()
            //self.tabBarItemFilterStakeholder.title = "Filter".localized()
            self.tabBarItemAddStakeholder.title = "New SH".localized()
            
            self.navigationItemStakeholder.title = "STAKEHOLDERS".localized()
            self.segmentedStakeholders.setTitle("Following".localized(), forSegmentAt: 0)
            self.segmentedStakeholders.setTitle("All".localized(), forSegmentAt: 1)
        }
    }
    func loadConfigStakeholdersTable()
    {
        self.vcStakeholderTable!.hidePosts = true
        self.vcStakeholderTable.enableRefreshAction ({
            
            self.searchPaging.pageNumber = 1
            
            if let text = self.searchBar.text, self.isSearching == true && text.trim().isEmpty == false
            {
                self.getReadySearch(.followingStatus_NoPagination_SearchText)
                self.vcStakeholderTable.refreshed()
            }
            else
            {
                //Reset Filter
                self.selectedFilter = 0
                //self.tabBarItemFilterStakeholder.title = "Filter".localized()
                self.tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
                
                self.vcStakeholderTable.stakeholders.removeAll()
                self.vcStakeholderTable.reload()
                self.vcStakeholderTable.refreshed()
                
                self.getReadySearch(.followingStatus_NoPagination_NoSearchText)
                self.getStakeholderList(showsLoadMessage: false)
            }
        })
    }
    
    func loadConfigSearchControl()
    {
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        searchBar.initialize(shouldSearchAfterDelay: true,
        onSearchText: { [weak self] (text) in
            if (self != nil) {
                self!.searchPaging.pageNumber = 1
                self!.vcStakeholderTable.stakeholders.removeAll()
                self!.vcStakeholderTable.reload()
                
                self!.vcStakeholderTable.displayBackgroundMessage("Loading...".localized(),
                    subMessage: "")
                
                self!.getStakeholderList()
            }
            
        }, onCancel:{ [weak self] in
            
            if (self != nil) {
                self!.searchBar.text = String()
                self!.setupNavigation(animated: true)
                
                self!.isSearching = false
                
                self!.searchPaging.pageNumber = 1
                self!.vcStakeholderTable.stakeholders.removeAll()
                self!.vcStakeholderTable.reload()

                self!.getReadySearch(.followingStatus_NoPagination_NoSearchText)
                self!.getStakeholderList()
            }
        })
    }
    
    func setupNavigation(animated: Bool)
    {
        if showSplitList() == true
        {
            title = "STAKEHOLDERS".localized()
        }
        else
        {
            title = "STAKEHOLDERS (Following)".localized()
        }
        
        self.searchBar.isActive = false
        self.isSearching = false
        self.isSearchByText = false
        
        // Bar Button Items
        let barBtnCompose = UIBarButtonItem(image: #imageLiteral(resourceName: "iconNewPost"), style: .plain,  target: self, action: #selector(self.createNewPost))
        navigationItem.setRightBarButtonItems([barBtnCompose], animated: animated)
        navigationItem.setHidesBackButton(false, animated: animated)
        
        if let titleView = navigationItem.titleView {
            titleView.alpha = 1
            
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                titleView.alpha = 0
                }, completion: { (animated) -> Void in
                    self.navigationItem.titleView = nil
            })
        }
    }
    
    func showSplitList() -> Bool
    {
        if  LibraryAPI.shared.currentUser?.role == .globalManager
            || LibraryAPI.shared.currentUser?.role == .cardManager
            || LibraryAPI.shared.currentUser?.role == .vipUser
            || LibraryAPI.shared.currentUser?.role == .cardCreator
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    // MARK: ACTIONS
    func createNewPost()
    {
        let vcNewPost = NewPostViewController()
        
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = DeviceType.IS_ANY_IPAD == true ?  .formSheet :  .overFullScreen
        
        present(navController, animated: true, completion: nil)
    }
    
    func cleanTable()
    {
        self.vcStakeholderTable.stakeholders.removeAll()
        self.vcStakeholderTable.indexStakeholders()
        self.vcStakeholderTable.reload()
        self.vcStakeholderTable.refreshed()
    }
    
    func openFilters()
    {
        var options = [DropdownItem]()
        
        for filter in self.filterSites
        {
            let item = DropdownItem(title: filter.title)
            options.append(item)
        }
        
        let menuFilters = DropUpMenu(items: options)
        menuFilters.delegate = self
        menuFilters.showMenu()
    }
    
    func segmentedControlHandler(_ segmented: UISegmentedControl)
    {
        self.searchBar.text = String()
        self.setupNavigation(animated: true)
        
        self.vcStakeholderTable.stakeholdersType = segmented.selectedSegmentIndex == 0 ? FollowStatus.following : FollowStatus.all
        
        self.selectedFilter = 0
        self.searchPaging.pageNumber = 1
        
        //self.tabBarItemFilterStakeholder.title = "Filter".localized()
        self.tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
       
        if self.searchBar.isActive == true
        {
            self.searchBar.text = String()
            self.setupNavigation(animated: true)
        }

        self.getReadySearch(.followingStatus_NoPagination_NoSearchText)
        self.getStakeholderList()
    }
    
    
    // MARK: WEB SERVICES
    func getStakeholderList(showsLoadMessage: Bool = true)
    {
        //Cancel previous request
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.GetStakeholderList)
        //Get text from search control
        guard let text = self.searchBar.text else {
            
            return
        }
        
        //Clean Table
        self.vcStakeholderTable.stakeholders.removeAll()
        self.vcStakeholderTable.indexStakeholders()
        self.vcStakeholderTable.reload()
        
        self.vcStakeholderTable.dismissBackgroundMessage()
        if showsLoadMessage == true
        {
            self.vcStakeholderTable.displayBackgroundMessage("Loading...".localized(),
                                                             subMessage: "")
        }
        
        
        //Get current segment
        let stakeholdersType  = (segmentedStakeholders.selectedSegmentIndex == 0) ? FollowStatus.following : FollowStatus.all
        
        LibraryAPI.shared.stakeholderBO.getStakeholders(parameters: [
            "FollowingStatus" : stakeholdersType.rawValue,
            "SearchText" : text,
            "SearchByText" : self.isSearchByText == true ? "true" : "false",
            "PageSize" : self.searchPaging.pageSize,
            "PageNumber" : self.searchPaging.pageNumber,
            "Pagination" : self.isPagination == true ? "true" : "false",
            "SiteMappingId" : self.filterSites.count > 0 ? self.filterSites[self.selectedFilter].siteId : 0 ],
                                                        onSuccess:{
                                                            (stakeholders, sites, totalRecords) -> () in
                                                            
                                                            //Fill Filters
                                                            if self.filtersExist == false
                                                            {
                                                                for (index, site) in sites.enumerated()
                                                                {
                                                                    site.title = index == 0 ? "⚐ \(site.title)" : "⚑ \(site.title)"
                                                                    self.filterSites.append(site)
                                                                }
                                                                
                                                                //Custom option
                                                                if self.filterSites.count == 0
                                                                {
                                                                    let noFilter = Site()
                                                                    noFilter.title = "⚐ " + "No Filter".localized()
                                                                    self.filterSites.insert(noFilter, at: 0)
                                                                }
                                                                
                                                                if self.filterSites.count > 1
                                                                {
                                                                    self.filtersExist = true
                                                                }
                                                            }
                                                            
                                                            //Total Records
                                                            if self.isPagination == true
                                                            {
                                                                self.searchPaging.totalItems = totalRecords
                                                            }
                                                            
                                                            self.vcStakeholderTable.stakeholders = stakeholders
                                                            self.vcStakeholderTable.indexStakeholders()
                                                            self.vcStakeholderTable.reload()
                                                            self.vcStakeholderTable.refreshed()
                                                            self.dataDidLoad?()
                                                            
                                                            //Spruce framework Animation
                                                            DispatchQueue.main.async {
                                                                
                                                                self.vcStakeholderTable.tableView.spruce.animate([.fadeIn, .expand(.slightly)], sortFunction: LinearSortFunction(direction: .topToBottom, interObjectDelay: 0.1))
                                                            }



                                                            
        }) { error in
            guard error.code != -999 else {return}
            MessageManager.shared.showBar(title: "Error".localized(),
                                          subtitle: "Error.TryAgain".localized(),
                                          type: .error,
                                          fromBottom: false)
            
            self.vcStakeholderTable.refreshed()
            self.vcStakeholderTable.tableView.displayBackgroundMessage("Error".localized(),
                                                                       subMessage: "Pull to refresh".localized())
            self.dataDidLoad?()
        }
    }
    
    // MARK: SEARCH TYPES
    func getReadySearch(_ type: SearchGetListType)
    {
        switch type
        {
        case .followingStatus_NoPagination_NoSearchText:
            
            self.isSearchByText = false
            self.isPagination = false
            
        case .followingStatus_NoPagination_SearchText:
            
            self.isSearchByText = true
            self.isPagination = false
            
        case .followingStatus_Pagination_SearchText:
            
            self.isSearchByText = true
            self.isPagination = true
            
        default:
            self.isSearchByText = false
            self.isPagination = false
            break
        }
    }
    
    // MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "StakeholdersTableContainer"
        {
            self.vcStakeholderTable = segue.destination as! StakeholdersTableViewController
            self.vcStakeholderTable.willDisplayLastRow = { [weak self] row in
            
                if let isPag = self?.isPagination, let page = self?.searchPaging.pageNumber, let totalPages = self?.searchPaging.totalPages, isPag == true && page <= totalPages
                {
                    self?.getReadySearch(.followingStatus_Pagination_SearchText)
                    self?.getStakeholderList()
                    self?.searchPaging.pageNumber += 1
                }
            }            
        }
    }
}

// MARK: UITabBarDelegate
extension StakeholdersViewController: UITabBarDelegate
{
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        
        switch item.tag
        {
        case TabBarOption.add.rawValue:
            
            let vcAddStakeholder = Storyboard.getInstanceOf(ContactSearchViewController.self)
            vcAddStakeholder.isSearchingSH = true
            vcAddStakeholder.onFollowOrUnfollow = { [weak self] Void in
                
                self?.getStakeholderList()
            }
            
            let navController = NavyController(rootViewController: vcAddStakeholder)
            navController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            
            present(navController, animated: true, completion: nil)
            
        case TabBarOption.filter.rawValue:
            self.openFilters()
            
        case TabBarOption.search.rawValue:
            
            self.isSearching = true
            self.getReadySearch(.followingStatus_NoPagination_SearchText)
            
            vcStakeholderTable.tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            searchBar.showInNavigationItem(self.navigationItem, animated: true)
            
        default:
            break
        }
    }
}

// MARK: DropDown/Up Menu
extension StakeholdersViewController: DropUpMenuDelegate
{
    func dropUpMenu(_ dropUpMenu: DropUpMenu, didSelectRowAtIndexPath indexPath: IndexPath)
    {
        if self.selectedFilter != indexPath.row
        {
            self.selectedFilter = indexPath.row
            
            //Filter Option UI
            if self.selectedFilter == 0
            {
                self.tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
                //self.tabBarItemFilterStakeholder.title = "Filter".localized()
            }
            else
            {
                self.tabBar.tintColor = UIColor.blueTag()
                //self.tabBarItemFilterStakeholder.title = self.filterSites[self.selectedFilter].title
            }
            
            
            //Searching
            if self.isSearching == true
            {
                self.vcStakeholderTable.stakeholders.removeAll()
                self.vcStakeholderTable.reload()

                self.getReadySearch(.followingStatus_NoPagination_SearchText)
                self.getStakeholderList()
            }
            else
            {
                self.vcStakeholderTable.stakeholders.removeAll()
                self.vcStakeholderTable.reload()

                self.getReadySearch(.followingStatus_NoPagination_NoSearchText)
                self.getStakeholderList()
            }
        }
    }
}
