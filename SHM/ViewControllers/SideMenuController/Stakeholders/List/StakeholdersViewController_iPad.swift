//
//  StakeholdersViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 11/12/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import Spruce

class StakeholdersViewController_iPad: StakeholdersViewController
{
    //MARK: OUTLETS & VARIABLES
    @IBOutlet weak var constraintCollectionTop: NSLayoutConstraint!
    @IBOutlet weak var collectionStakeholders: UICollectionView!
    @IBOutlet weak var searchStakeholderBar: AutoSearchBar!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var navItemStakeholder_iPad: UINavigationItem!
    
    var layout: UICollectionViewFlowLayout!
    var arrayStakeholders = [[Stakeholder]]()
    var arraySectionTitles = [String]()
    var btnFilter: UIButton!
    fileprivate var webServiceActive = false
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        self.loadConfig_iPad()
        
        self.getReadySearch(.followingStatus_NoPagination_NoSearchText)
        self.getStakeholderList()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        self.loadLayoutForCollectionView()
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "reloadStakeholdersList"), object: nil)
    }
    
    // MARK: - CONFIG
    func loadConfig_iPad()
    {
        //initialization of the AutoSearchBar
        searchStakeholderBar.initialize(shouldSearchAfterDelay: true,
        onSearchText: { [weak self] (text) in
            self?.searchPaging.pageNumber = 1
//            self.arraySectionTitles.removeAll()
//            self.arrayStakeholders.removeAll()
            self?.getReadySearch(.followingStatus_NoPagination_SearchText)
            self?.getStakeholderList()
            
        }, onBeginEditing: { [weak self] (searchBar) in
            self?.isSearching = true
            
        }, onEndEditing: { [weak self] (searchBar) in
            if let term = searchBar.text, term.trim() == "" {
                self?.isSearching = false
                self?.searchPaging.pageNumber = 1
            }
            
        }, onCancel: { [weak self] in
            if (self != nil)
            {
                self!.searchStakeholderBar.text = String()
                self!.isSearching = false
                self!.searchPaging.pageNumber = 1

                self!.getReadySearch(.followingStatus_NoPagination_NoSearchText)
                self!.getStakeholderList()
            }
        })
        
        self.localize()
        
        //Update
        NotificationCenter.default.addObserver(self, selector:#selector(self.getStakeholderList), name: NSNotification.Name(rawValue: "reloadStakeholdersList"), object: nil)
        
        searchPaging.pageNumber = 1
        
        
        //Segemented Control
        if showSplitList() == true
        {
            self.segmentedControl.isHidden = false
            self.constraintCollectionTop.constant = 44
        }
        else
        {
            self.segmentedControl.isHidden = true
        }
        
        self.segmentedControl.selectedSegmentIndex = 0
        self.segmentedControl.addTarget(self, action: #selector(StakeholdersViewController.segmentedControlHandler(_:)), for: .valueChanged)
        
        //Items Bar
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(StakeholdersViewController.createNewPost))
        let barBtnAdd = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addStakeholder))
        
        self.btnFilter = UIButton(frame: CGRect(x: 0, y: 0, width: 70, height: 25))
        self.btnFilter.cornerRadius()
        self.btnFilter.setTitle("Filter".localized(), for: UIControlState())
        self.btnFilter.addTarget(self, action: #selector(self.openFilters), for: .touchUpInside)
        let barBtnFilter = UIBarButtonItem(customView: self.btnFilter)
        
        navigationItem.rightBarButtonItems = [barBtnCompose, barBtnAdd]
        
        //Collection Viewz
        self.layout = UICollectionViewFlowLayout()
        self.loadLayoutForCollectionView()
        
        self.collectionStakeholders.collectionViewLayout = self.layout
        self.collectionStakeholders.dataSource = self
        self.collectionStakeholders.delegate = self
        self.collectionStakeholders.allowsMultipleSelection = false
        self.collectionStakeholders.register(UINib(nibName: "CollectionStakeholderCell", bundle: nil), forCellWithReuseIdentifier: "CollectionStakeholderCell")
        
        self.collectionStakeholders.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind:UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
        
        self.collectionStakeholders.backgroundColor = UIColor.clear
        
        //RefreshAction
        self.collectionStakeholders.enableRefreshAction {
            
            self.searchPaging.pageNumber = 1
            if let text = self.searchStakeholderBar.text, self.isSearching == true && text.trim().isEmpty == false
            {
                self.getReadySearch(.followingStatus_NoPagination_SearchText)
                self.collectionStakeholders.refreshed()
            }
            else
            {
                //Reset Filter
                self.selectedFilter = 0
                
                
                self.arraySectionTitles.removeAll()
                self.arrayStakeholders.removeAll()
                self.collectionStakeholders.reloadData()
                self.collectionStakeholders.refreshed()
                
                self.getReadySearch(.followingStatus_NoPagination_NoSearchText)
                self.getStakeholderList(showsLoadMessage: false)
            }

        }
    }
    
    fileprivate func localize()
    {
        self.navItemStakeholder_iPad.title = "STAKEHOLDERS".localized()
        self.segmentedControl.setTitle("Following".localized(), forSegmentAt: 0)
        self.segmentedControl.setTitle("All".localized(), forSegmentAt: 1)
        
        self.searchStakeholderBar.placeholder = "Search Stakeholders".localized()
        self.searchStakeholderBar.setValue("Cancel".localized(), forKey:"_cancelButtonText")
    }
    
    //MARK: LAYOUT FOR COLLECTION VIEW
    func loadLayoutForCollectionView()
    {
        if (UIDeviceOrientationIsPortrait(UIDevice.current.orientation))
        {
            self.layout.scrollDirection =  .vertical
            self.layout.minimumInteritemSpacing = 10
            self.layout.minimumLineSpacing = 10
            self.layout.sectionInset = UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 20)
            self.layout.headerReferenceSize = CGSize(width: 320, height: 24)
            self.layout.itemSize = CGSize(width: 355, height: 85)
        }
        else
        {
            self.layout.scrollDirection =  UICollectionViewScrollDirection.vertical
            self.layout.minimumInteritemSpacing = 1
            self.layout.minimumLineSpacing = 11
            self.layout.sectionInset = UIEdgeInsets(top: 5, left: 1, bottom: 5, right: 1)
            self.layout.headerReferenceSize = CGSize(width: 320, height: 24)
            self.layout.itemSize = CGSize(width: 340, height: 85)
        }
    }
    
    // MARK: WEB SERVICES
    override func getStakeholderList(showsLoadMessage: Bool = true)
    {
        //Cancel previous request
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.GetStakeholderList)
        
        //Get text from search control
        guard var text = self.searchStakeholderBar.text else {
            
            return
        }
        
        text = text.trim()
        
        //Avoiding search if is empty the text
        if self.isSearchByText == true
        {
            if text.isEmpty == true
            {
                return
            }
        }
        
        //Flag to show or not Empty Message
        self.webServiceActive = true
        
        //Clean
        self.arraySectionTitles.removeAll()
        self.arrayStakeholders.removeAll()
        self.collectionStakeholders.reloadData()
        
        self.collectionStakeholders.dismissBackgroundMessage()
        if showsLoadMessage == true
        {
            self.collectionStakeholders.displayBackgroundMessage("Loading...".localized(),
                                                                 subMessage: "")
        }        
        
        let stakeholdersType  = (self.segmentedControl.selectedSegmentIndex == 0) ? FollowStatus.following : FollowStatus.all
        
        LibraryAPI.shared.stakeholderBO.getStakeholders(parameters: [
            "FollowingStatus" : stakeholdersType.rawValue,
            "SearchText" : text,
            "SearchByText" : self.isSearchByText == true ? "true" : "false",
            "PageSize" : searchPaging.pageSize,
            "PageNumber" : searchPaging.pageNumber,
            "Pagination" : self.isPagination == true ? "true" : "false",
            "SiteMappingId" :  self.filterSites.count > 0 ? self.filterSites[self.selectedFilter].siteId : 0 ],
                                                                onSuccess:{
                                                                    (stakeholders, sites, totalRecords) -> () in
                                                                    
                                                                    //Flag to show or not Empty Message
                                                                    self.webServiceActive = false
                                                                    
                                                                    //Fill Filters
                                                                    if self.filtersExist == false
                                                                    {
                                                                        for (index, site) in sites.enumerated()
                                                                        {
                                                                            site.title = index == 0 ? "⚐ \(site.title)" : "⚑ \(site.title)"
                                                                            self.filterSites.append(site)
                                                                        }
                                                                        
                                                                        //Custom option
                                                                        if self.filterSites.count == 0
                                                                        {
                                                                            let noFilter = Site()
                                                                            noFilter.title = "⚐ " + "No Filter".localized()
                                                                            self.filterSites.insert(noFilter, at: 0)
                                                                        }
                                                                        
                                                                        if self.filterSites.count > 1
                                                                        {
                                                                            self.filtersExist = true
                                                                        }
                                                                    }
                                                                    
                                                                    //Total Records
                                                                    if self.isPagination == true
                                                                    {
                                                                        self.searchPaging.totalItems = totalRecords
                                                                    }
                                                                    
                                                                    // load an array of the first letters
                                                                    let firstLetters = stakeholders.map({ String($0.fullName.uppercased()[$0.fullName.uppercased().startIndex]) })
                                                                    
                                                                    //Delete Characters duplicated & Ordering alphabetically
                                                                    self.arraySectionTitles = Array(Set(firstLetters)).sorted(by: {$0 < $1})
                                                                    
                                                                    //Sort Stakeholder by Section Character
                                                                    for character in self.arraySectionTitles
                                                                    {
                                                                        self.arrayStakeholders.append(stakeholders.filter({ String($0.fullName.uppercased()[$0.fullName.uppercased().startIndex]) == character }))
                                                                    }
                                                                    
                                                                    self.collectionStakeholders.reloadData()
                                                                    
                                                                    //Spruce framework Animation
                                                                    DispatchQueue.main.async {
                                                                        
                                                                        self.collectionStakeholders.spruce.animate([.fadeIn, .expand(.slightly)], sortFunction: LinearSortFunction(direction: .topToBottom, interObjectDelay: 0.15))
                                                                    }
                                                                    
                                                                    
        }) { error in
            guard error.code != -999 else {return}
            //Flag to show or not Empty Message
            self.webServiceActive = false
            
            MessageManager.shared.showBar(title: "Error".localized(),
                                                            subtitle: "Error.TryAgain".localized(),
                                                            type: .error,
                                                            fromBottom: false)
            
            self.collectionStakeholders.reloadData()
            self.collectionStakeholders.displayBackgroundMessage("Error".localized(),
                                                                 subMessage: "Try again".localized())
        }
    }
    
    //MARK: ACTIONS
    override func openFilters()
    {
        //Cancel Search (Hide Keyboard)
        if self.isSearching == true
        {
            self.searchStakeholderBar.resignFirstResponder()
        }
        
        //Filter Options
        var filterStrings = [String]()
        
        for filter in filterSites
        {
            filterStrings.append(filter.title)
        }
        
        let vcFilterMenu = StringTableViewController()
        vcFilterMenu.checkedOption = self.selectedFilter
        vcFilterMenu.options = filterStrings
        vcFilterMenu.modalPresentationStyle = .popover
        vcFilterMenu.popoverPresentationController?.sourceView = self.view
        vcFilterMenu.popoverPresentationController?.sourceRect = self.btnFilter.frame
        vcFilterMenu.onSelectedOption = { numberOfRow in
            
            if self.selectedFilter != numberOfRow
            {
                self.selectedFilter = numberOfRow
                
                //Filter Option UI
                self.btnFilter.backgroundColor = self.selectedFilter == 0 ? UIColor.clear : UIColor.blueBelize()
                
                //Searching
                if self.isSearching == true
                {
                    self.getReadySearch(.followingStatus_NoPagination_SearchText)
                    self.getStakeholderList()
                }
                else
                {
                    self.getReadySearch(.followingStatus_NoPagination_NoSearchText)
                    self.getStakeholderList()
                }
            }
        }
        
        self.present(vcFilterMenu, animated: true, completion: nil)
    }
    
    override func segmentedControlHandler(_ segmented: UISegmentedControl)
    {
        self.resetSearch()
    }
    
    func resetSearch()
    {
        //reset
        self.selectedFilter = 0
        self.searchPaging.pageNumber = 1
        
        //Filter Option UI
        self.btnFilter.backgroundColor = self.selectedFilter == 0 ? UIColor.clear : UIColor.blueBelize()
        
        //Close Search
        self.searchStakeholderBar.text = String()
        self.searchStakeholderBar.resignFirstResponder()
        self.searchStakeholderBar.setShowsCancelButton(false, animated: true)
        
        //Load
        self.getReadySearch(.followingStatus_NoPagination_NoSearchText)
        self.getStakeholderList()
    }
    
    func addStakeholder()
    {
        //Cancel Search (Hide Keyboard)
        if self.isSearching == true
        {
            self.searchStakeholderBar.resignFirstResponder()
        }
        
        let vcAddStakeholder = Storyboard.getInstanceOf(ContactSearchViewController.self)
        vcAddStakeholder.isSearchingSH = true
        
        let navController = NavyController(rootViewController:vcAddStakeholder)
        navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
        
        self.present(navController, animated: true, completion: nil)
    }
    
    override func createNewPost()
    {
        if isSearching == true
        {
            self.resetSearch()
        }
        
        let vcNewPost = NewPostModal()
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
        
        self.present(navController, animated: true, completion: nil)
    }
    
    func pushNewPost(_ stakeholderTag: Stakeholder!)
    {
        let vcNewPost = NewPostModal()
        vcNewPost.stakeholderTagged = stakeholderTag
        
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = (DeviceType.IS_ANY_IPAD) ?  UIModalPresentationStyle.formSheet :  UIModalPresentationStyle.overFullScreen
        
        self.present(navController, animated: true, completion: nil)
    }
    
    func openAddNewStakeholdersList()
    {
        let popoverSize = CGSize(width: self.view.bounds.width - 100, height: self.view.bounds.height - 100)
        
        let vcAddStakeholder = Storyboard.getInstanceOf(ContactSearchViewController.self)
        vcAddStakeholder.isSearchingSH = true
        
        let navController = NavyController(rootViewController: vcAddStakeholder)
        navController.modalPresentationStyle = UIModalPresentationStyle.popover
        navController.preferredContentSize = popoverSize
        
        if let popoverPresentationController = navController.popoverPresentationController {
            
            popoverPresentationController.sourceView = view
            popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
            popoverPresentationController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY,width: 0,height: 0)
            DispatchQueue.main.async(execute: {
                
                self.present(navController, animated: true, completion: nil)
            })
        }
    }
    
    fileprivate func pushRatingControlFor(_ stakeholder: Stakeholder, view: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 50)))
    {
        let vcRating = RatingViewController_iPad()
        vcRating.modalPresentationStyle = .popover
        self.present(vcRating, animated: true, completion: nil)
        vcRating.preferredContentSize  = CGSize(width: 280,height: 60)
        vcRating.stakeholder = stakeholder
        let datePopover = vcRating.popoverPresentationController
        datePopover?.permittedArrowDirections = .any
        datePopover?.sourceView = view
        datePopover?.sourceRect = view.frame
        datePopover?.backgroundColor = .black
        vcRating.didEndRating = {[weak self] void in
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadStakeholdersList"), object: nil)
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        super.prepare(for: segue, sender: sender)
    }
}

//MARK: COLLECTION VIEW
extension StakeholdersViewController_iPad: UICollectionViewDelegate, UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        //Background message
        if self.webServiceActive == false
        {
            if self.arrayStakeholders.count > 0
            {
                self.collectionStakeholders.dismissBackgroundMessage()
            }
            else
            {
                self.collectionStakeholders.displayBackgroundMessage("No stakeholders found".localized(),
                                                                     subMessage: "")
            }
        }
        
        return self.arrayStakeholders.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.arrayStakeholders[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        //Just Allow Header type
        guard kind == UICollectionElementKindSectionHeader else {
            
            return UICollectionReusableView()
        }
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
        
        if indexPath.section < self.arraySectionTitles.count
        {
            header.lblTitle.text = self.arraySectionTitles[indexPath.section]
        }
                
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionStakeholderCell", for: indexPath) as! CollectionStakeholderCell
        
        let stakeholder = self.arrayStakeholders[indexPath.section][indexPath.row]
        cell.lblName.text = stakeholder.fullName
        
        //Following indicator
        let stakeholdersType  = (self.segmentedControl.selectedSegmentIndex == 0) ? FollowStatus.following : FollowStatus.all
        
        if stakeholdersType == .all
        {
            cell.imgFollowed.isHidden = stakeholder.isFollow == FollowStatus.following ? false : true
        }
        else
        {
            cell.imgFollowed.isHidden = true
        }
        
        //Position & Company
        let strJobPosition = stakeholder.jobPosition?.value ?? ""
        let strCompanyName = stakeholder.companyName
        cell.lblJobPosition.attributedText = NSMutableAttributedString().setJobPositionCompany(strJobPosition, company: strCompanyName)
        cell.imgProfile.setImageWith(URL(string: stakeholder.thumbnailUrl), placeholderImage: UIImage(named: "defaultperson"))
        cell.setNationalities(stakeholder)
        cell.onButtonTap = { (button) in
            
            //Selected Style
            cell.btnMore.backgroundColor = UIColor.blueTag()
            cell.btnMore.tintColor = UIColor.white
            
            //Content View
            let vcTableOptions = StringTableViewController()
            vcTableOptions.modalPresentationStyle = .popover
            
            var options = [String]()
            if stakeholder.isEnable == true
            {
                options = stakeholder.isFollow == .following ? ["Unfollow".localized(), "Post".localized()] : ["Follow".localized(), "Post".localized()]
            }
            else
            {
                options = stakeholder.isFollow == .following ? ["Unfollow".localized()] : ["Follow".localized()]
            }
            
            
            vcTableOptions.options = options
            
            vcTableOptions.onSelectedOption = { numberOfRow in
                
                if numberOfRow == 0
                {
                    //Follow or Unfollow
                    if stakeholder.isFollow == FollowStatus.following
                    {
                        LibraryAPI.shared.stakeholderBO.unfollowStakeholder(stakeholder,
                                                                                    onSuccess: { () -> () in
                                                                                        
                                                                                        //Reload List
                                                                                        self.getStakeholderList()
                                                                                        
                                                                                        let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                                                                        DispatchQueue.main.asyncAfter(deadline: delayTime) {
                                                                                            
                                                                                            let info = "Now, you're not following".localized() + " " + stakeholder.fullName
                                                                                            
                                                                                            MessageManager.shared.showBar(title: "Info".localized(),
                                                                                                subtitle: info,
                                                                                                type: .info,
                                                                                                fromBottom: false)
                                                                                        }
                                                                                        
                            }, onError: { error in
                                
                                MessageManager.shared.showBar(title: "Error",
                                    subtitle: "Error.TryAgain".localized(),
                                    type: .error,
                                    fromBottom: false)
                        })
                    }
                    else
                    {
                        LibraryAPI.shared.stakeholderBO.followStakeholder(stakeholder,
                                                                                  onSuccess: { () -> () in
                                                                                    
                                                                                    //Reload List
                                                                                    self.getStakeholderList()
                                                                                    
                                                                                    let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                                                                    DispatchQueue.main.asyncAfter(deadline: delayTime) {
                                                                                        
                                                                                        let info = "Now, you're following".localized() + " " + stakeholder.fullName
                                                                                        
                                                                                        MessageManager.shared.showBar(title: "Success".localized(),
                                                                                            subtitle: info,
                                                                                            type: .success,
                                                                                            fromBottom: false)
                                                                                    }
                                                                                    
                            }, onError: { error in
                                
                                MessageManager.shared.showBar(title: "Error",
                                    subtitle: "Error.TryAgain".localized(),
                                    type: .error,
                                    fromBottom: false)
                        })
                    }
                }
                else
                {
                    //Post it
                    self.pushNewPost(stakeholder)
                }
            }
            
            vcTableOptions.onClosed = {
                
                //Unselected Style
                cell.btnMore.backgroundColor = UIColor.white
                cell.btnMore.tintColor = UIColor.blueTag()
            }
            
            //Show Popover
            let popoverController = vcTableOptions.popoverPresentationController
            popoverController?.sourceView = cell
            popoverController?.sourceRect = button.frame
            popoverController?.permittedArrowDirections = .any
            
            self.present(vcTableOptions, animated: true, completion: nil)
        }
        
        //Rating
        cell.lblAdminRating.text = (stakeholder.adminRating ?? 0.0).getString()
        cell.lblUserRating.text = (stakeholder.userRating ?? 0.0).getString()
        cell.didTapOnRating = { [weak self] Void in
        
            self?.pushRatingControlFor(stakeholder, view: cell.viewTapRating)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let stakeholderSelected = self.arrayStakeholders[indexPath.section][indexPath.row]
        
        let vcStakeholderDetail = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
        vcStakeholderDetail.stakeholder = stakeholderSelected
        
        navigationController?.pushViewController(vcStakeholderDetail, animated: true)
    }
}

