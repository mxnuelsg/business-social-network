//
//  EditStakeholderViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas on 5/12/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import TOCropViewController

enum Language
{
    case english
    case spanish
}

private enum CurrentTab: Int
{
    case basicInfo = 0
    case profile = 1
    case projects = 2
    case relations = 3
    case members = 4
    case geographies = 5
}

private enum PickerType
{
    case none
    case updateProfile
    case attachments
}

class EditStakeholderViewController_iPad: UIViewController
{
    //MARK: OUTLETS & PROPERTIES
    //Header
    @IBOutlet weak fileprivate var ivProfileBanner: UIImageView!
    @IBOutlet weak fileprivate var ivProfile: UIImageView!
    @IBOutlet weak fileprivate var btnUpdatePicture: UIButton!
    @IBOutlet weak fileprivate var btnEnglishRoundButton: UIButton!
    @IBOutlet weak fileprivate var btnSpanishRoundButton: UIButton!
    @IBOutlet weak fileprivate var segmentedTabs: UISegmentedControl!
    @IBOutlet weak fileprivate var lblLastUpdate: UILabel!
    @IBOutlet weak var labelAdminRating: UILabel!
    
    //Footer
    @IBOutlet weak var lblAttachments: UILabel!
    @IBOutlet weak fileprivate var viewAttachments: UIView!
    @IBOutlet weak fileprivate var collectionAttachments: UICollectionView!
    
    //Lateral
    @IBOutlet weak fileprivate var viewLateralHeader: UIView!
    @IBOutlet weak fileprivate var containerLateralFeed: UIView!
    @IBOutlet weak fileprivate var lblLateralCurrentPage: UIView!
    @IBOutlet weak fileprivate var pageControlLateral: UIPageControl!
    @IBOutlet weak fileprivate var btnLateralNewPost: UIButton!
    @IBOutlet weak fileprivate var lblLateralTitle: UILabel!
    
    //Body
    @IBOutlet weak fileprivate var containerBasicInfo: UIView!
    @IBOutlet weak fileprivate var containerProfile: UIView!
    @IBOutlet weak fileprivate var containerProjects: UIView!
    @IBOutlet weak fileprivate var containerRelations: UIView!
    @IBOutlet weak fileprivate var containerMembers: UIView!
    @IBOutlet weak fileprivate var containerGeographies: UIView!
    
    //Global properties
    var stakeholder: Stakeholder!
    fileprivate var pickerType = PickerType.none
    fileprivate var languageSelected = Language.english
    fileprivate var arrayAttachments = [AttachmentWithToken]()
    fileprivate var profilePicture = AttachmentWithToken()
    
    //Attachment counter
    fileprivate var uploadCount = 1
    
    //Controllers
    var vcLateralContainer: LateralStakeholderFeeds_iPad!
    var vcBasicInfo: AddStakeholderViewController!
    var vcProfile: ProfileStakeholderEditTable!
    var vcProjects: PRMTable!
    var vcRelations: PRMTable!
    var vcMembers: PRMTable!
    fileprivate var vcGeographies: StakeholderGeographiesTableViewController?
    
    //Callback
    var onCompletedEdition:(() -> ())?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.getStakeholderEdit()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: LOAD CONFIG
    fileprivate func loadConfig()
    {
        self.localize()
        
        //Banner Blur
        self.ivProfileBanner.blur()
        self.lblLastUpdate.text = "Last update".localized() + ": "
        
        //Borders
        self.containerLateralFeed.setBorder()
        self.viewLateralHeader.setBorder()
        self.viewAttachments.setBorder()
        
        //Button Items
        let barBtnClose = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.close))
        let barBtnDone = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(self.done))
        navigationItem.leftBarButtonItem = barBtnClose
        navigationItem.rightBarButtonItem = barBtnDone
        
        //Language Buttons
        self.btnEnglishRoundButton.addTarget(self, action: #selector(self.selectLanguage(_:)), for: .touchUpInside)
        
        self.btnSpanishRoundButton.addTarget(self, action: #selector(self.selectLanguage(_:)), for: .touchUpInside)
        self.selectLanguage(self.btnEnglishRoundButton)
        
        //Update Picture
        self.btnUpdatePicture.addTarget(self, action: #selector(self.updateProfilePicture), for: .touchUpInside)
        self.ivProfile.setBorder()
        
        //Section Tabs
        self.segmentedTabs.selectedSegmentIndex = 0
        self.segmentedTabs.addTarget(self, action: #selector(self.segmentedControlHandler(_:)), for: .valueChanged)
        self.segmentedControlHandler(self.segmentedTabs)
        
        //Lateral Header
        self.btnLateralNewPost.addTarget(self, action: #selector(self.createNewPost), for: .touchUpInside)
        self.pageControlLateral.currentPage = 0
        
        //Collection View (Attachments)
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection =  .horizontal
        layout.minimumInteritemSpacing = 4
        layout.minimumLineSpacing = 4
        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        layout.itemSize = CGSize(width: 50, height: 50)
        
        self.collectionAttachments.collectionViewLayout = layout
        self.collectionAttachments.dataSource = self
        self.collectionAttachments.delegate = self
        self.collectionAttachments.allowsMultipleSelection = false
        self.collectionAttachments.register(UINib(nibName: "AttachmentCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
    }
    
    fileprivate func localize()
    {
        //Title
        self.title = "Edit Stakeholder".localized()
        
        //Segmented
        self.segmentedTabs.setTitle("INFO".localized(), forSegmentAt: 0)
        self.segmentedTabs.setTitle("PROFILE".localized(), forSegmentAt: 1)
        self.segmentedTabs.setTitle("PROJECTS".localized(), forSegmentAt: 2)
        self.segmentedTabs.setTitle("RELATIONS".localized(), forSegmentAt: 3)
        self.segmentedTabs.setTitle("MEMBERS".localized(), forSegmentAt: 4)
        self.segmentedTabs.setTitle("GEOGRAPHIES".localized(), forSegmentAt: 5)
        
        //Pictures & Language
        self.btnUpdatePicture.setTitle("Update Profile Picture".localized(), for: UIControlState())
        self.btnEnglishRoundButton.setTitle(" English".localized(), for: UIControlState())
        self.btnSpanishRoundButton.setTitle(" Spanish".localized(), for: UIControlState())
        
        let feed = "FEED".localized() + " • "
        let recent = "RECENT".localized()
        let title = feed + recent
        
        self.lblLateralTitle.text = title
        self.lblAttachments.text = "ATTACHMENTS".localized()
    }
    
    //MARK: REQUEST OBJECT
    fileprivate func extractStakeholderData()
    {
        //Getting Data
        //** Header
        self.stakeholder.thumbnailUrl = self.profilePicture.file
        
        //** Body
        self.getBasicInfo()
        self.getProfileData()
        self.stakeholder.projects = self.vcProjects.getProjects()
        self.stakeholder.relations = self.vcRelations.getRelations()
        self.stakeholder.members = self.vcMembers.getMembers()
        
        //** Lateral
        self.stakeholder.posts = self.vcLateralContainer.vcFeedRecent.getPosts()
        
        //** Footer
        self.stakeholder.attachments = self.getAttachments()
    }
    
    //MARK: TOP BAR ACTIONS
    func close()
    {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func done()
    {
        let alert = UIAlertController(title:"Stakeholder edition".localized(),
                                      message:"What would you like to do with this card?".localized(),
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler:nil))
        alert.addAction(UIAlertAction(title: "Publish".localized(), style: .default, handler:{
            (action) in
            
            self.publish()
        }))
        alert.addAction(UIAlertAction(title: "Save".localized(), style: .default, handler:{
            (action) in
            
            self.save()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    fileprivate func save()
    {
        self.view.endEditing(true)
        
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        self.extractStakeholderData()
        
        LibraryAPI.shared.stakeholderBO.saveEditStakeholder(self.stakeholder, onSuccess: {
            (stakeholder) in
            
            //Remove Indicator
            MessageManager.shared.hideHUD()
            self.onCompletedEdition?()
            self.dismiss(animated: true, completion: nil)
            
        }) { (error) in
            
            //Remove Indicator
            MessageManager.shared.hideHUD()
            MessageManager.shared.showBar(title: "Error",
                                          subtitle: "Error.TryAgain".localized(),
                                          type: .error,
                                          fromBottom: false)
        }
    }
    
    fileprivate func publish()
    {
        self.view.endEditing(true)
        
        self.extractStakeholderData()
        
        let vcPublishRequest = PublishRequestScreen()
        vcPublishRequest.stakeholderEdited = self.stakeholder
        vcPublishRequest.onPublishSuccess = { [weak self] Void in
            
            //Refresh Stakeholder List
            NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadStakeholdersList"), object: nil)
            
            var message = String()
            
            if let msg = self?.stakeholder.fullName {
                
                message = msg  + " " + "has been edited".localized()
                
            }
            else
            {
                message =  "Stakeholder".localized()  + " " + "has been edited".localized()
            }
            
            //Show message
            MessageManager.shared.showBar(title: "Info".localized(),
                                          subtitle: message,
                                          type: .info,
                                          fromBottom: false)
            
            let delayTime = DispatchTime.now() + Double(Int64(1.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                self?.onCompletedEdition?()
                self?.close()
            }
        }
        
        let navController = NavyController(rootViewController: vcPublishRequest)
        navController.modalPresentationStyle  = .formSheet
        
        self.present(navController, animated: true, completion: nil)
    }
    
    //MARK: WEB SERVICE
    func getStakeholderEdit()
    {
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        LibraryAPI.shared.stakeholderBO.createStakeholderRequest(stakeholderId: self.stakeholder.id, onSuccess: { stakeholder in
            
            let reqId = stakeholder.requestId
            
            LibraryAPI.shared.stakeholderBO.editStakeholder(stakeholderId: stakeholder.id, onSuccess: { stakeholder in
                
                MessageManager.shared.hideHUD()
                
                self.stakeholder = stakeholder
                self.stakeholder.requestId = reqId
                self.stakeholder.site = self.stakeholder.sites.filter({$0.siteId == self.stakeholder.whosSiteId }).first ?? Site()
                //Header
                self.ivProfile.setImageWith(URL(string: self.stakeholder.thumbnailUrl), placeholderImage: UIImage(named: "defaultperson"))
                self.ivProfileBanner.setImageWith(URL(string: self.stakeholder.thumbnailUrl), placeholderImage: UIImage(named: "defaultperson"))
                
                if let lastUpdate = self.stakeholder.modifiedDate {
                    
                    self.lblLastUpdate.text = "Last update".localized() + ": " + lastUpdate.getStringStyleMedium()
                }
                else
                {
                    self.lblLastUpdate.text = "Last update".localized() + ": " + "Unknown".localized()
                }
                
                //Body
                self.vcBasicInfo.stakeholder = self.stakeholder
                self.vcProfile.stakeholder = self.stakeholder
                self.vcProjects.content = self.stakeholder.projects
                self.vcRelations.content = self.stakeholder.relations
                self.vcRelations.relationsTypes = self.stakeholder.relationsLevel1
                self.vcMembers.content = self.stakeholder.members
                self.vcMembers.stakeholderId = self.stakeholder.id
                self.vcGeographies?.initialize(stakeholder: self.stakeholder)
                
                //Lateral
                self.vcLateralContainer.stakeholder = self.stakeholder
                
                //Footer
                for remoteImage in self.stakeholder.attachments
                {
                    let attach = AttachmentWithToken()
                    attach.token = remoteImage.token
                    attach.file = remoteImage.file
                    attach.isRemote = true
                    
                    self.arrayAttachments.append(attach)
                }
                
                if self.arrayAttachments.count > 0
                {
                    self.collectionAttachments.reloadData()
                }
                
                //Sort Sites                
                self.stakeholder.sites = Site.sortSites(self.stakeholder.sites)
                
                self.labelAdminRating.text = stakeholder.adminRating?.getString()
                
                
            }) { (error) in
                
                MessageManager.shared.hideHUD()
                MessageManager.shared.showBar(title: "Error",
                                              subtitle: "Error.TryAgain".localized(),
                                              type: .error,
                                              fromBottom: false)
            }
            
        }) { (error) in
            
            MessageManager.shared.hideHUD()
            
            MessageManager.shared.showBar(title: "Error",
                                          subtitle: "Error.TryAgain".localized(),
                                          type: .error,
                                          fromBottom: false)
        }
    }
    
    //MARK: ACITONS
    func createNewPost()
    {
        let vcNewPost = NewPostModal()
        vcNewPost.stakeholderTagged = self.stakeholder
        
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = .formSheet
        
        present(navController, animated: true, completion:nil)
    }
    
    func selectLanguage(_ buttonSelected: UIButton)
    {
        if buttonSelected.tag == 1
        {
            self.btnEnglishRoundButton.setImage(UIImage(named: "iconRoundSelected"), for: UIControlState())
            self.btnSpanishRoundButton.setImage(UIImage(named: "iconRoundUnselected"), for: UIControlState())
            
            self.languageSelected = Language.english
        }
        else
        {
            self.btnEnglishRoundButton.setImage(UIImage(named: "iconRoundUnselected"), for: UIControlState())
            self.btnSpanishRoundButton.setImage(UIImage(named: "iconRoundSelected"), for: UIControlState())
            
            self.languageSelected = Language.spanish
        }
        
        //Change Controllers
        self.vcBasicInfo.language = self.languageSelected
        self.vcProfile.language = self.languageSelected
    }
    
    func segmentedControlHandler(_ segmented: UISegmentedControl)
    {
        let tab = CurrentTab(rawValue: segmented.selectedSegmentIndex)!
        self.containerBasicInfo.isHidden = tab != .basicInfo
        self.containerProfile.isHidden = tab != .profile
        self.containerProjects.isHidden = tab != .projects
        self.containerRelations.isHidden = tab != .relations
        self.containerMembers.isHidden = tab != .members
        self.containerGeographies.isHidden = tab != .geographies
    }
    
    func updateProfilePicture()
    {
        let alertSheet = UIAlertController(title:nil, message:nil, preferredStyle: .alert)
        alertSheet.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler:{
            (action) in
            
            self.pickerType = .none
        }))
        
        alertSheet.addAction(UIAlertAction(title: "Take Photo".localized(), style: .default, handler: {
            (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera
                imagePicker.allowsEditing = false
                imagePicker.modalPresentationStyle = .overCurrentContext
                
                self.pickerType = .updateProfile
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        alertSheet.addAction(UIAlertAction(title: "Choose Existing".localized(), style: .default, handler: {
            (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
                
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary
                imagePicker.allowsEditing = false
                imagePicker.modalPresentationStyle = .popover
                imagePicker.popoverPresentationController?.sourceView = self.btnUpdatePicture
                
                self.pickerType = .updateProfile
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        self.present(alertSheet, animated: true, completion: nil)
        
    }
    
    func addAttachment()
    {
        let alertSheet = UIAlertController(title:nil, message:nil, preferredStyle: .alert)
        alertSheet.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: {
            (action) in
            
            self.pickerType = .none
        }))
        
        alertSheet.addAction(UIAlertAction(title: "Take Photo".localized(), style: .default, handler: {
            (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera
                imagePicker.allowsEditing = false
                imagePicker.modalPresentationStyle = .overCurrentContext
                
                self.pickerType = .attachments
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        alertSheet.addAction(UIAlertAction(title: "Choose Existing".localized(), style: .default, handler: {
            (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
                
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary
                imagePicker.allowsEditing = false
                imagePicker.modalPresentationStyle = .popover
                imagePicker.popoverPresentationController?.sourceView = self.collectionAttachments;
                
                self.pickerType = .attachments
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        self.present(alertSheet, animated: true, completion: nil)
    }
    
    @IBAction func rate(button: UIButton) {
        RatingViewController_iPad.present(inController: self,
                                          sourceView: button,
                                          shouldSendRating: false,
                                          onSelectRating: { [weak self] (rating) in
                                              self?.stakeholder.adminRating = rating
                                              self?.labelAdminRating.text = rating.getString()
                                          })
    }
    
    //MARK: CLIPBOARD (UIMenuController)
    @IBAction func pasteImageFromClipboard(_ sender: UILongPressGestureRecognizer)
    {
        if sender.state == UIGestureRecognizerState.ended
        {
            becomeFirstResponder()
            
            let menuItem = UIMenuItem(title: "Paste".localized(), action: #selector(self.pasteFromClipboard))
            let menu = UIMenuController.shared
            menu.menuItems = [menuItem]
            menu.arrowDirection = .left
            menu.setTargetRect(CGRect(x: 0, y: 0, width: 50, height: 50), in: self.ivProfile)
            
            menu.setMenuVisible(true, animated: true)
        }
    }
    
    func pasteFromClipboard()
    {
        let pasteboard = UIPasteboard.general
        if let img = pasteboard.image {
            
            UIApplication.shared.isStatusBarHidden = true
            let cropViewController = TOCropViewController(image: img)
            cropViewController?.delegate = self
            self.present(cropViewController!, animated: true, completion: nil)
        }
        else
        {
            //Show Message
            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                MessageManager.shared.showBar(title: "Info",
                                              subtitle: "The clipboard content is not an image".localized(),
                                              type: .info,
                                              fromBottom: false)
            }
        }
    }
    
    override var canBecomeFirstResponder : Bool
    {
        return true
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool
    {
        if action == #selector(self.pasteFromClipboard)
        {
            return true
        }
        
        return false
    }
    
    
    //MARK: EXTRACTION DATA
    fileprivate func getAttachments() -> [Attachment]
    {
        var totalAttachments = [Attachment]()
        
        for att in self.arrayAttachments
        {
            let attachment = Attachment()
            attachment.file = att.file
            attachment.token = att.token
            
            totalAttachments.append(attachment)
        }
        
        return totalAttachments
    }
    
    fileprivate func getProfileData()
    {
        self.stakeholder.summaryPublic = self.vcProfile.stakeholder.summaryPublic
        self.stakeholder.summaryPublicEsp = self.vcProfile.stakeholder.summaryPublicEsp
        self.stakeholder.summaryPrivate = self.vcProfile.stakeholder.summaryPrivate
        self.stakeholder.summaryPrivateEsp = self.vcProfile.stakeholder.summaryPrivateEsp
        
        self.stakeholder.personalInformationPublic = self.vcProfile.stakeholder.personalInformationPublic
        self.stakeholder.personalInformationPublicEsp = self.vcProfile.stakeholder.personalInformationPublicEsp
        self.stakeholder.personalInformationPrivate = self.vcProfile.stakeholder.personalInformationPrivate
        self.stakeholder.personalInformationPrivateEsp = self.vcProfile.stakeholder.personalInformationPrivateEsp
        
        self.stakeholder.professionalPublic = self.vcProfile.stakeholder.professionalPublic
        self.stakeholder.professionalPublicEsp = self.vcProfile.stakeholder.professionalPublicEsp
        self.stakeholder.professionalPrivate = self.vcProfile.stakeholder.professionalPrivate
        self.stakeholder.professionalPrivateEsp = self.vcProfile.stakeholder.professionalPrivateEsp
        
        self.stakeholder.generalInformationPublic = self.vcProfile.stakeholder.generalInformationPublic
        self.stakeholder.generalInformationPublicEsp = self.vcProfile.stakeholder.generalInformationPublicEsp
        self.stakeholder.generalInformationPrivate = self.vcProfile.stakeholder.generalInformationPrivate
        self.stakeholder.generalInformationPrivateEsp = self.vcProfile.stakeholder.generalInformationPrivateEsp
        
        self.stakeholder.relevantDates = self.vcProfile.stakeholder.relevantDates
        self.stakeholder.professionalTimeline = self.vcProfile.stakeholder.professionalTimeline
        self.stakeholder.additionalFields = self.vcProfile.stakeholder.additionalFields
        
        self.stakeholder.references = self.vcProfile.stakeholder.references
        self.stakeholder.references += self.vcProfile.stakeholder.referencesEsp
    }
    
    fileprivate func getBasicInfo()
    {
        self.stakeholder.isCompany = self.vcBasicInfo.stakeholder.isCompany
        self.stakeholder.name = self.vcBasicInfo.stakeholder.name
        self.stakeholder.companyName = self.vcBasicInfo.stakeholder.companyName
        self.stakeholder.lastName = self.vcBasicInfo.stakeholder.lastName
        self.stakeholder.alias = self.vcBasicInfo.stakeholder.alias
        self.stakeholder.jobPosition = self.vcBasicInfo.stakeholder.jobPosition
        self.stakeholder.companyName = self.vcBasicInfo.stakeholder.companyName
        self.stakeholder.placeOfBirth = self.vcBasicInfo.stakeholder.placeOfBirth
        self.stakeholder.occupation = self.vcBasicInfo.stakeholder.occupation
        self.stakeholder.industrySector = self.vcBasicInfo.stakeholder.industrySector
        self.stakeholder.nationalities = self.vcBasicInfo.stakeholder.nationalities
        self.stakeholder.country = self.vcBasicInfo.stakeholder.country
        self.stakeholder.countries = self.vcBasicInfo.stakeholder.countries
        self.stakeholder.birthday = self.vcBasicInfo.stakeholder.birthday
        self.stakeholder.dueDate = self.vcBasicInfo.stakeholder.dueDate
        self.stakeholder.maritalStatus = self.vcBasicInfo.stakeholder.maritalStatus
        self.stakeholder.createdDate = self.vcBasicInfo.stakeholder.createdDate
        self.stakeholder.site = self.vcBasicInfo.stakeholder.site
        self.stakeholder.whosId = self.vcBasicInfo.stakeholder.whosId
        
        self.stakeholder.coordinator = self.vcBasicInfo.stakeholder.coordinator
        self.stakeholder.representative = self.vcBasicInfo.stakeholder.representative
        self.stakeholder.birthDateString = self.vcBasicInfo.stakeholder.birthDateString
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let id = segue.identifier, id == "StakeholderGeographies" {
            self.vcGeographies = segue.destination as? StakeholderGeographiesTableViewController
            return
        }
        
        if let id = segue.identifier, id == "LateralStakeholderFeeds_iPad" {
            
            self.vcLateralContainer = segue.destination as! LateralStakeholderFeeds_iPad
            self.vcLateralContainer.onPageDidChange = { [weak self] page in
                
                self?.pageControlLateral.currentPage = page
                
                //Static value for attribute
                let feed = "FEED".localized() + " • "
                
                switch page
                {
                case 0:
                    let recent = "RECENT".localized()
                    let title = feed + recent
                    
                    self?.lblLateralTitle.text = title
                case 1:
                    let backup = "BACKUP".localized()
                    let title = feed + backup
                    
                    self?.lblLateralTitle.text = title
                case 2:
                    let attachments = "ATTACHMENTS".localized()
                    let title = feed + attachments.uppercased()
                    
                    self?.lblLateralTitle.text = title
                default:
                    break
                }
            }
        }
        
        if let id = segue.identifier, id == "AddStakeholderViewController" {
            
            self.vcBasicInfo = segue.destination as! AddStakeholderViewController
            self.vcBasicInfo.isModal = false
            self.vcBasicInfo.type = .editStakeholder
        }
        
        if let id = segue.identifier, id == "ProfileStakeholderEditTable" {
            
            self.vcProfile = segue.destination as! ProfileStakeholderEditTable
        }
        
        
        if let id = segue.identifier, id == "Projects" {
            
            self.vcProjects = segue.destination as! PRMTable
            self.vcProjects.type = .projects
        }
        
        if let id = segue.identifier, id == "Relations" {
            
            self.vcRelations = segue.destination as! PRMTable
            self.vcRelations.type = .relations
        }
        
        if let id = segue.identifier, id == "Members" {
            
            self.vcMembers = segue.destination as! PRMTable
            self.vcMembers.type = .members
        }
    }
}

// MARK: CollectionView Methods
extension EditStakeholderViewController_iPad: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.arrayAttachments.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if self.arrayAttachments.count == 0
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! AttachmentCell
            cell.backgroundColor = UIColor.clear
            cell.imageAttachment.image = UIImage(named: "iconAdd")
            cell.btnDelete.isHidden = true
            
            return cell
        }
        else
        {
            //Verify If it's the last row
            let lastRowIndex = collectionView.numberOfItems(inSection: collectionView.numberOfSections - 1)
            
            if indexPath.row == lastRowIndex - 1
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! AttachmentCell
                cell.backgroundColor = UIColor.clear
                cell.imageAttachment.image = UIImage(named: "iconAdd")
                cell.btnDelete.isHidden = true
                
                return cell
            }
            else
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! AttachmentCell
                cell.backgroundColor = UIColor.clear
                cell.btnDelete.isHidden = false
                cell.btnDelete.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
                    
                    self.arrayAttachments = self.arrayAttachments.filter({ $0 !== self.arrayAttachments[indexPath.row] })
                    self.collectionAttachments.reloadData()
                    
                }))
                
                //Select source of image
                if self.arrayAttachments[indexPath.row].isRemote == true
                {
                    let thumbnailUrl = self.arrayAttachments[indexPath.row].file
                    cell.imageAttachment.setImageWith(URL(string: thumbnailUrl), placeholderImage: nil)
                }
                else
                {
                    cell.imageAttachment.image = self.arrayAttachments[indexPath.row].picture
                }
                
                return cell
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        //Verify If it's the last row
        let lastRowIndex = collectionView.numberOfItems(inSection: collectionView.numberOfSections - 1)
        
        if indexPath.row == lastRowIndex - 1
        {
            self.addAttachment()
        }
    }
}

// MARK: IMAGEPICKER VIEW METHODS
extension EditStakeholderViewController_iPad: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        self.dismiss(animated: true) { () -> Void in
            
            if let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
            {
                switch self.pickerType
                {
                case .updateProfile:
                    //Present CropViewController
                    UIApplication.shared.isStatusBarHidden = true
                    let cropViewController = TOCropViewController(image: selectedImage)
                    cropViewController?.delegate = self
                    self.present(cropViewController!, animated: true, completion: nil)
                    
                case .attachments:
                    
                    //Indicator
                    MessageManager.shared.showLoadingHUD()
                    
                    self.uploadImage(selectedImage.size.width > 1000 ? selectedImage.resizeToWidth(1000):selectedImage, pickerType: .attachments)
                    
                default:
                    //Remove indicator
                    MessageManager.shared.hideHUD()
                    
                    self.pickerType = .none
                    break
                }
                
                
            }
        }
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: NSErrorPointer?, contextInfo:UnsafeRawPointer)
    {
        if error != nil
        {
            let alert = UIAlertController(title: "Save Failed".localized(),
                                          message: "Failed to save image".localized(),
                                          preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "OK".localized(),
                                             style: .cancel,
                                             handler: nil)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion:nil)
        }
    }
}

extension EditStakeholderViewController_iPad : TOCropViewControllerDelegate
{
    func cropViewController(_ cropViewController: TOCropViewController!, didCropTo image: UIImage!, with cropRect: CGRect, angle: Int)
    {
        var editedImage = image
        UIApplication.shared.isStatusBarHidden = false
        self.presentedViewController?.dismiss(animated: false, completion: {
            
            //Indicator
            MessageManager.shared.showLoadingHUD()
            
            if editedImage!.size.width > 1000
            {
                editedImage = image.resizeToWidth(1000)
            }
            
            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                self.uploadImage(editedImage!, pickerType: .updateProfile)
            }
        })
    }
    
    fileprivate func uploadImage(_ image:UIImage, pickerType:PickerType = PickerType.updateProfile)
    {
        switch pickerType
        {
        case .updateProfile:
            //Request Object
            var wsObject: [String : Any]  = [:]
            wsObject["OlfFile"] = self.stakeholder.thumbnailUrl
            wsObject["VersionId"] = self.stakeholder.versionId
            
            //Convert Image to Base64
            if let imageData = UIImagePNGRepresentation(image) as Data! {
                
                let base64String = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                wsObject["NewFile"] = "data:image/png;base64,\(base64String)"
            }
            
            //Play web service
            LibraryAPI.shared.attachmentBO.uploadStakeholderProfilePicture(parameters: wsObject, onSuccess: {
                (thumbnail) in
                
                //Remove indicator
                MessageManager.shared.hideHUD()
                
                let attachPicture = AttachmentWithToken()
                attachPicture.file = thumbnail
                
                self.profilePicture = attachPicture
                self.ivProfile.setImageWith(URL(string: thumbnail), placeholderImage: UIImage(named: "defaultperson"))
                self.ivProfileBanner.setImageWith(URL(string: thumbnail), placeholderImage: UIImage(named: "defaultperson"))
                self.pickerType = .none
                
                
            }, onError: { (error) in
                
                //Remove indicator
                MessageManager.shared.hideHUD()
                
                self.pickerType = .none
                
                //Show Message
                let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    
                    MessageManager.shared.showBar(title: "Error",
                                                  subtitle: "There was an error trying to save an attachment".localized(),
                                                  type: .error,
                                                  fromBottom: false)
                }
            })
            
        case .attachments:
            
            self.uploadCount += 1
            let localUrl = image.saveToTempDirectory("mobileUpload\(self.uploadCount).png")
            let imageData = UIImagePNGRepresentation(image)
            
            LibraryAPI.shared.attachmentBO.uploadFile(fileSize: imageData!.count, fileURL: localUrl, onSuccess: {
                (token) in
                
                //Remove indicator
                MessageManager.shared.hideHUD()
                
                let attachPicture = AttachmentWithToken(token: token, image: image)
                
                self.arrayAttachments.append(attachPicture)
                self.collectionAttachments.reloadData()
                self.pickerType = .none
                
                //Show Message
                let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    
                    MessageManager.shared.showStatusBar(title: "Attachment Uploaded".localized(),
                                                        type: .success,
                                                        containsIcon: false)
                }
                
                
            }, onError: { error in
                
                //Remove indicator
                MessageManager.shared.hideHUD()
                
                self.pickerType = .none
                
                //Show Message
                let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    
                    MessageManager.shared.showBar(title: "Error",
                                                  subtitle: "There was an error trying to save an attachment".localized(),
                                                  type: .error,
                                                  fromBottom: false)
                }
            })
            
            
        default:
            //Remove indicator
            MessageManager.shared.hideHUD()
            
            self.pickerType = .none
            break
        }
        
    }
    
    func cropViewController(_ cropViewController: TOCropViewController!, didFinishCancelled cancelled: Bool)
    {
        UIApplication.shared.isStatusBarHidden = false
        self.presentedViewController?.dismiss(animated: true, completion: nil)
    }
}
