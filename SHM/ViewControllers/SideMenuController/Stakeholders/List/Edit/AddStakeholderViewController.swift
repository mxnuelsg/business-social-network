//
//  ActorAddViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/6/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

enum StakeholderScreen
{
    case new
    case editStakeholder
}

private enum AddStakeholderSection: Int
{
    case fields = 0
    case comments = 1
    case coordinator_Representative = 2
}

private enum PersonFields: Int
{
    case firstName      = 0
    case lastName       = 1
    case alias          = 2
    case jobPosition    = 3
    case company        = 4
    case cityBirth      = 5
    case country        = 6
    case birthdate      = 7
    case maritalStatus  = 8
    case occupation     = 9
    case nationalities  = 10
    case dueDate        = 11
    case site           = 12
    case associatedWhos = 13
}

private enum CompanyFields: Int
{
    case name           = 0
    case alias          = 1
    case knowAddress    = 2
    case dateFoundation = 3
    case country        = 4
    case industrySector = 5
    case nationalities  = 6
    case dueDate        = 7
    case site           = 8
    case associatedWhos = 9
}

class AddStakeholderViewController: UIViewController
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var tableForm: UITableView!
    @IBOutlet weak var segmentedType: UISegmentedControl!
    
    fileprivate var textFieldBirthDate: UITextField?
    
    var barBtnSave: UIBarButtonItem!
    var didChangeSite = false
   
    //Properties
    fileprivate var arrayForPerson = [
        "First Name".localized(),
        "Last Name".localized(),
        "Alias (more than one is separated by commas)".localized(),
        "Job Position".localized(),
        "Company".localized(),
        "Place of birth (Town, state, country)".localized(),
        "Country of residency".localized(),
        "Birthdate".localized(),
        "Marital status".localized(),
        "Occupation".localized(),
        "Nationality".localized(),
        "DueDate".localized(),
        "Site".localized(),
        "Associated Whos".localized()]
    
    fileprivate var arrayForCompany = [
        "Name".localized(),
        "Alias (more than one is separated by commas)".localized(),
        "Know address".localized(),
        "Date of foundation".localized(),
        "Country".localized(),
        "Industry/Sector".localized(),
        "Nationality".localized(),
        "DueDate".localized(),
        "Site".localized(),
        "Associated Whos".localized()]
    
    
    var type = StakeholderScreen.new
    var language = Language.english {
        didSet {
            
            //Order Ascending
            if self.language == .english
            {
                self.arrayCountries = self.arrayCountries.sorted(by: {$0.valueEn < $1.valueEn})
            }
            else
            {
                self.arrayCountries = self.arrayCountries.sorted(by: {$0.valueEs < $1.valueEs})
            }
            
            self.tableForm.reloadData()
        }
    }
    
    var arrayFormulary = [String]()
    var arrayCountries =  [KeyValueObject]()
    var arrayMarital =  [KeyValueObject]()
    var arrayBusiness =  [KeyValueObject]()
    fileprivate var minDueDate: Date?
    fileprivate var rawMinDueDate: String?
    var isModal = true
    var onStakeholderCreated: ((_ new: Stakeholder) -> ())?
    var onStakeholderModalClosed:(() -> ())?
    
    var textFieldIndexPath: IndexPath!
    var stakeholder = Stakeholder(isDefault: true) {
        didSet {
            if self.stakeholder.isCompany == false
            {
                self.segmentedType.selectedSegmentIndex = 0
            }
            else
            {
                self.segmentedType.selectedSegmentIndex = 1
            }
            
            self.segmentedControlValueChanged(self.segmentedType)
            self.tableForm.reloadData()
        }
    }
    
    fileprivate var createSHcard = false
    fileprivate var createCompanyDossier = false
    
    fileprivate var datePickerBirthDate: UIDatePicker?
    
    // MARK: - LIFE CICLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.loadConfig()
        self.loadDefaultData()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
//        self.loadDefaultData()
    }
    
    // MARK: INITIAL CONFIG
    fileprivate func loadConfig()
    {
        self.localize()
        
        //Override value for value if i't's necessay
        let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
        if language == "es"
        {
            self.language = .spanish
        }
        
        //Title
        self.title = "ADD NEW SH".localized()
        
        //Register Custom Cell
        self.tableForm.register(UINib(nibName: "TableTextfieldCell", bundle: nil), forCellReuseIdentifier: "TableTextfieldCell")
        self.tableForm.register(UINib(nibName: "CellStakeholderCordRepre", bundle: nil), forCellReuseIdentifier: "CellStakeholderCordRepre")
        self.tableForm.register(UINib(nibName: "SwitchTableCell", bundle: nil), forCellReuseIdentifier: "SwitchTableCell")
        self.tableForm.register(UINib(nibName: "CommentsTableCell", bundle: nil), forCellReuseIdentifier: "CommentsTableCell")
        
        //Height dynamic
        self.tableForm.estimatedRowHeight = 60
        self.tableForm.rowHeight = UITableViewAutomaticDimension

        
        //Bar Button Items
        if self.isModal == true
        {
            let barBtnClose = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.close))
            navigationItem.leftBarButtonItem = barBtnClose
        }
        
        self.barBtnSave = UIBarButtonItem(title: "Save".localized(), style: .plain, target: self, action:#selector(self.AddNew))
        navigationItem.rightBarButtonItem = barBtnSave
        
        //Segmented control
        self.segmentedType.addTarget(self, action: #selector(AddStakeholderViewController.segmentedControlValueChanged(_:)), for:.valueChanged)
        
        //Fill Datasource
        self.arrayFormulary = (self.segmentedType.selectedSegmentIndex == 0) ? self.arrayForPerson : self.arrayForCompany
    }
    
    fileprivate func localize()
    {
        self.segmentedType.setTitle("SH profile".localized(), forSegmentAt: 0)
        self.segmentedType.setTitle("Company dossier".localized(), forSegmentAt: 1)
    }
    
    func loadToolBar(withFreeTextOption: Bool = false) -> UIToolbar
    {
        let toolBarKeyboard = UIToolbar()
        toolBarKeyboard.barStyle = UIBarStyle.blackOpaque;
        toolBarKeyboard.barTintColor = UIColor.colorForNavigationController()
        
        let barBtnDone = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(self.closeKeyboard))
        barBtnDone.tintColor = .white
        
        toolBarKeyboard.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
                                 barBtnDone]
        
        if (withFreeTextOption) {
            let freeTextView = UIView()
            let freeTextSwitch = UISwitch()
            freeTextSwitch.addTarget(self, action: #selector(toggleFreeText(aSwitch:)), for: .valueChanged)
            let freeTextLabel = UILabel(frame: CGRect(x: freeTextSwitch.frame.width, y: 0, width: 0, height: 0))
            freeTextLabel.textColor = UIColor.white
            freeTextLabel.text = " Free Text".localized()
            freeTextLabel.sizeToFit()
            freeTextLabel.frame.size.height = freeTextSwitch.frame.height
            freeTextView.addSubview(freeTextSwitch)
            freeTextView.addSubview(freeTextLabel)
            freeTextView.frame = CGRect(x: 0, y: 0, width: freeTextLabel.frame.maxX, height: freeTextSwitch.frame.height)
            let freeTextBtn = UIBarButtonItem(customView: freeTextView)
            
            toolBarKeyboard.items?.insert(freeTextBtn, at: 0)
        }
        
        toolBarKeyboard.sizeToFit()
        
        return toolBarKeyboard
    }
    
    func closeKeyboard()
    {
        view.endEditing(true)
    }
    
    func toggleFreeText(aSwitch: UISwitch) {
        textFieldBirthDate?.text = aSwitch.isOn ? stakeholder.birthDateString : stakeholder.birthday?.showStringFormat("MM/dd/yyyy")
        textFieldBirthDate?.inputView = aSwitch.isOn ? nil : datePickerBirthDate
        textFieldBirthDate?.reloadInputViews()
    }
    
    //MARK: METHODS
    func setMinDate(_ hours: Int, hasRoleValidation: Bool) -> Date
    {
        var startDate = Date()
        var endDate = Date()
        var count = 0
        
        if hasRoleValidation == true
        {
            // Avoid weekdays
            while endDate.weekday == 1 || endDate.weekday == 7
            {
                startDate = startDate.dateByAddingDays(1)
                endDate = startDate
            }
        }
        else
        {
            while count < hours
            {
                startDate = startDate.dateByAddingHours(1)
                endDate = startDate
                
                if endDate.weekday != 1 && endDate.weekday != 7
                {
                    count = count + 1
                }
            }
        }
        
        return endDate
    }
    
    func getRawDate(_ fullDate: Date) -> String
    {
        let month = fullDate.month + 1
        let rawMonth = month < 10 ? ("0\(month)") : "\(month)"
        
        return "\(rawMonth)/\(fullDate.day)/\(fullDate.year)"
    }
    
    // MARK: WEB SERVICES
    func loadDefaultData()
    {
        MessageManager.shared.showLoadingHUD()
        LibraryAPI.shared.stakeholderBO.getStakeholderDefaults(onSuccess: { defaultData in
            
            self.arrayCountries = defaultData.countries
            self.arrayMarital =  defaultData.maritalStatus
            self.arrayBusiness =  defaultData.businessTypes
            
            self.stakeholder.sites = Site.sortSites(defaultData.sites)
            self.stakeholder.site = defaultData.sites.count == 1 ? defaultData.sites[0] : nil //Auto assign
            
            //Defaults Choice
            let defaultChoice = KeyValueObject()
            defaultChoice.key = -1
            defaultChoice.valueEn = "Select one".localized()
            defaultChoice.valueEs = "Seleccionar.."
            
            self.arrayMarital.insert(defaultChoice, at: 0)
            self.arrayBusiness.insert(defaultChoice, at: 0)
            self.minDueDate = self.setMinDate(defaultData.maxHoursToDueDateInRequests, hasRoleValidation: defaultData.hasRoleDueDateValidation)
            
            if let minDate = self.minDueDate {
                
                self.rawMinDueDate = self.getRawDate(minDate)
            }
            self.tableForm.reloadData()
            MessageManager.shared.hideHUD()
        }) { (error) -> () in
            MessageManager.shared.hideHUD()
            MessageManager.shared.showBar(title:"Error",
                                          subtitle: "Error.TryAgain".localized(),
                                          type: .error,
                                          fromBottom: false)
        }
    }
    
    // MARK: ACTION BUTTONS
    func segmentedControlValueChanged(_ segment: UISegmentedControl)
    {
        self.view.endEditing(true)
        
        if segment.selectedSegmentIndex == 0
        {
            self.stakeholder.isCompany = false
            
            self.arrayFormulary.removeAll()
            self.arrayFormulary = self.arrayForPerson
        }
        else
        {
            self.stakeholder.isCompany = true
            
            self.arrayFormulary.removeAll()
            self.arrayFormulary = self.arrayForCompany
        }
        
        self.tableForm.reloadData()
    }
    
    func dateValue(_ sender: UIDatePicker)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US")
        let stringDateHuman = dateFormatter.string(from: sender.date)
        
        let cell = tableForm.cellForRow(at: textFieldIndexPath) as! TableTextfieldCell
        
        if (sender.tag == 11 && self.stakeholder.isCompany == false) || (sender.tag == 7  && self.stakeholder.isCompany == true)
        {
            if sender.date.weekday != 1 && sender.date.weekday != 7
            {
                cell.txtText.text = stringDateHuman
                self.stakeholder.dueDate = sender.date
            }
            else
            {
                let newDate = sender.date.weekday == 1 ? sender.date.dateByAddingDays(2) : sender.date.dateByAddingDays(1)
                cell.txtText.text = dateFormatter.string(from: newDate)
                self.stakeholder.dueDate = newDate
                
                MessageManager.shared.showBar(title:"Warning".localized(),
                                              subtitle: "You choose a weekday(saturday, sunday), the date move a valid date(monday)".localized(),
                                              type: .warning,
                                              fromBottom: false)
            }
        }
    }
    
    func close()
    {
        self.view.endEditing(true)
        self.onStakeholderModalClosed?()
        self.dismiss(animated: true, completion: nil)
    }
    
    func AddNew()
    {
        self.view.endEditing(true)
        
        //Verify Index: 0 = Person | 1 = Company
        if self.stakeholder.isCompany == false
        {
            guard self.stakeholder.name.isEmpty == false else
            {
                MessageManager.shared.showBar(title: "Warning".localized(),
                                              subtitle: "What's the name?".localized(),
                                              type: .warning,
                                              fromBottom: false)
                
                return
            }
            
            //Save data
            self.createRequest()
        }
        else
        {
            guard self.stakeholder.name.isEmpty == false else {
                
                MessageManager.shared.showBar(title:"Warning".localized(),
                                              subtitle: "What's the company name?".localized(),
                                              type: .warning,
                                              fromBottom: false)
                
                return
            }
            
            //Save data
            self.createRequest()
        }
    }
    
    // MARK: REQUEST
    func createRequest()
    {
        self.barBtnSave.isEnabled = false
        
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        LibraryAPI.shared.stakeholderBO.saveStakeholder(self.stakeholder, onSuccess: { stakeholder in
            
            MessageManager.shared.hideHUD()
            self.barBtnSave.isEnabled = true
            
            self.dismiss(animated: true) { () -> Void in
                
                //Update Stakeholder List
                NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadStakeholdersList"), object: nil)
                
                //Show message
                let message = self.stakeholder.fullName + " " + "has been created".localized()
                
                MessageManager.shared.showBar(title:"Success".localized(),
                                              subtitle:message,
                                              type: .success,
                                              fromBottom: false)
                
                
                if let newStakeholder = self.onStakeholderCreated {
                    
                    newStakeholder(stakeholder)
                }
            }
        }) { (error) -> () in
            
            MessageManager.shared.hideHUD()
            self.barBtnSave.isEnabled = true
           
            //Display error message
            if let duplicatedSH = (error.userInfo[NSLocalizedDescriptionKey] as? JSON)?.string {
                
                MessageManager.shared.showBar(title: duplicatedSH,
                                              subtitle: String(),
                                              type: .error,
                                              fromBottom: false)
                return
            }
            MessageManager.shared.showBar(title:"Error",
                                          subtitle: "Error.TryAgain".localized(),
                                          type: .error,
                                          fromBottom: false)
        }
    }
    
    func showSiteMenu()
    {
        var options = [DropdownItem]()
        for site in self.stakeholder.sites
        {
            let item = DropdownItem(title: site.title)
            item.isSelected = self.stakeholder.site?.siteId == site.siteId ? true : false
            item.level = site.level
            options.append(item)
        }
        let menuFilters = DropUpMenu(items: options)
        menuFilters.delegate = self
        menuFilters.showMenu()
    }
    func showSiteMenu_iPad()
    {
        //Create title array for table
        var siteTitles = [String]()
        for site in self.stakeholder.sites
        {
            siteTitles.append(site.getTitleByLevel())
        }
        
        //Create option table controller
        let vcFilterMenu = StringTableViewController()
        vcFilterMenu.options = siteTitles
        vcFilterMenu.isDisplayingSites = true
        if let currentSelectedSite = self.stakeholder.site {
            
            vcFilterMenu.checkedOption = self.stakeholder.sites.index(of: currentSelectedSite) ?? -1
        }
        vcFilterMenu.modalPresentationStyle = .popover
        
        let cell = self.stakeholder.isCompany == false ? tableForm.cellForRow(at: IndexPath(row: PersonFields.site.rawValue, section: AddStakeholderSection.fields.rawValue)) ?? UITableViewCell() : tableForm.cellForRow(at: IndexPath(row: CompanyFields.site.rawValue, section: AddStakeholderSection.fields.rawValue)) ?? UITableViewCell()
        vcFilterMenu.popoverPresentationController?.sourceView = cell.contentView
        vcFilterMenu.popoverPresentationController?.sourceRect = cell.contentView.frame
        vcFilterMenu.onSelectedOption = { [weak self]  row in
        
            self?.stakeholder.site = self?.stakeholder.sites[row]
            if self?.stakeholder.isCompany == false
            {
                self?.tableForm.reloadRows(at: [IndexPath(row: PersonFields.site.rawValue, section: AddStakeholderSection.fields.rawValue)], with: .none)
            }
            else
            {
                self?.tableForm.reloadRows(at: [IndexPath(row: CompanyFields.site.rawValue, section: AddStakeholderSection.fields.rawValue)], with: .none)
            }
            
        }
        self.present(vcFilterMenu, animated: true, completion: nil)
    }
}


//MARK: TABLE VIEW DATASOURCE
extension AddStakeholderViewController: UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        switch self.type
        {
        case .new:
            return 2
        case .editStakeholder:
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        switch section
        {
        case AddStakeholderSection.fields.rawValue:
            
            return self.stakeholder.isCompany == false ? "Stakeholder Info".localized() : "Company Info".localized()
          
        case AddStakeholderSection.comments.rawValue:
            
            return "".localized()
            
        case AddStakeholderSection.coordinator_Representative.rawValue:
            
            return "Coordinator and Representative".localized()
            
        default:
            return "Unknown".localized()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if self.type == .new
        {
            switch section
            {
            case AddStakeholderSection.comments.rawValue:
                
                //Create Cell
                let headerCell = tableView.dequeueReusableCell(withIdentifier: "SwitchTableCell") as! SwitchTableCell
                headerCell.selectionStyle = .none
                headerCell.swSwitch.isOn = self.stakeholder.isCompany == false ? self.createSHcard : self.createCompanyDossier
                headerCell.lblTitle.text = self.stakeholder.isCompany == false ? "Create SH card".localized() : "Create Company/Institution dossier".localized()
                headerCell.onSwitch = { [weak self]  on in
                    
                    self?.closeKeyboard()
                    self?.stakeholder.sendRequest = on
                    
                    //Local flag
                    if self?.stakeholder.isCompany == false
                    {
                        self?.createSHcard = on
                    }
                    else
                    {
                        self?.createCompanyDossier = on
                    }
                    
                    tableView.reloadSections(IndexSet(integer: section), with: .fade)
                }
                
                let headerView = UIView(frame: headerCell.frame)
                headerView.backgroundColor = .white
                headerCell.autoresizingMask = [.flexibleHeight, .flexibleWidth]
                headerView.addSubview(headerCell)
                
                return headerView
                
            default:
                return nil
            }
        }
        else
        {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        switch section
        {
        case AddStakeholderSection.fields.rawValue:
            return self.arrayFormulary.count
        case AddStakeholderSection.comments.rawValue:
            return self.type == .new ? 1 : 0
        case AddStakeholderSection.coordinator_Representative.rawValue:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch indexPath.section
        {
        case AddStakeholderSection.fields.rawValue:
            
            if self.stakeholder.isCompany == false
            {
                switch indexPath.row
                {
                case PersonFields.firstName.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    
                    cell.txtText.text = self.stakeholder.name
                    cell.comboStyle(false)
                    
                    return cell
                    
                case PersonFields.lastName.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    
                    cell.txtText.text = self.stakeholder.lastName
                    cell.comboStyle(false)
                    
                    return cell
                    
                case PersonFields.alias.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    cell.txtText.text = self.stakeholder.alias.collapseWithCommas()
                    cell.comboStyle(false)
                    
                    return cell
                    
                case PersonFields.jobPosition.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    
                    if self.language == .english
                    {
                        let strJobPosition = self.stakeholder.jobPosition?.valueEn ?? ""
                        cell.txtText.text = strJobPosition
                    }
                    else
                    {
                        let strJobPosition = self.stakeholder.jobPosition?.valueEs ?? ""
                        cell.txtText.text = strJobPosition
                    }
                    
                    cell.comboStyle(false)
                    
                    return cell
                    
                case PersonFields.company.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    
                    cell.txtText.text = self.stakeholder.companyName
                    cell.comboStyle(false)
                    
                    return cell
                    
                case PersonFields.cityBirth.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    
                    cell.txtText.text = self.stakeholder.placeOfBirth
                    cell.comboStyle(false)
                    
                    return cell
                    
                case PersonFields.country.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    
                    if self.language == .english
                    {
                        cell.txtText.text = self.stakeholder.country?.valueEn
                    }
                    else
                    {
                        cell.txtText.text = self.stakeholder.country?.valueEs
                    }
                    
                    cell.comboStyle(true)
                    
                    return cell
                    
                case PersonFields.birthdate.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    
                    cell.txtText.text = self.stakeholder.birthday?.getFormatDateString() ?? self.stakeholder.birthDateString
                    cell.comboStyle(true)
                    
                    return cell
                    
                case PersonFields.maritalStatus.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    
                    if self.language == .english
                    {
                        cell.txtText.text = self.stakeholder.maritalStatus?.valueEn
                    }
                    else
                    {
                        cell.txtText.text = self.stakeholder.maritalStatus?.valueEs
                    }
                    
                    cell.comboStyle(true)
                    
                    return cell
                    
                case PersonFields.occupation.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    
                    if self.language == .english
                    {
                        let strJobPosition = self.stakeholder.occupation?.valueEn ?? ""
                        cell.txtText.text = strJobPosition
                    }
                    else
                    {
                        let strJobPosition = self.stakeholder.occupation?.valueEs ?? ""
                        cell.txtText.text = strJobPosition
                    }
                    
                    cell.comboStyle(false)
                    
                    return cell
                    
                case PersonFields.nationalities.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    
                    if self.language == .english
                    {
                        cell.txtText.text = self.stakeholder.nationalities.collapseValues()
                    }
                    else
                    {
                        cell.txtText.text = self.stakeholder.nationalities.collapseValuesEs()
                    }
                    
                    cell.comboStyle(true)
                    
                    return cell
                    
                case PersonFields.dueDate.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    
//                    //Site
//                    if type == .editStakeholder
//                    {
//                        cell.txtText.text = self.stakeholder.site?.title ?? self.stakeholder.sites?[0].title
//                        cell.comboStyle(true)
//                    }
//                    else
//                    {
                        cell.txtText.text = self.stakeholder.dueDate?.getFormatDateString()
                        cell.comboStyle(true)
//                    }
                    
                    return cell
                    
                case PersonFields.site.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    
                    if let sitemap = self.stakeholder.site, sitemap.title.trim().isEmpty == false && sitemap.siteId != 0 {
                        
                        cell.txtText.text = sitemap.title
                    } else {
                        cell.txtText.text = ""
                    }
                    
                    cell.comboStyle(self.stakeholder.sites.count <= 2 ? false : true)
                    
                    return cell
                    
                case PersonFields.associatedWhos.rawValue:
                    
                    //Whos
                    let cell = UITableViewCell(style: .value1, reuseIdentifier:"Cell")
                    cell.accessoryType = .disclosureIndicator
                    cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
                    cell.textLabel?.textColor = UIColor.blackAsfalto()
                    
                    //Selection color
                    let selectedColor = UIView()
                    selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
                    cell.selectedBackgroundView = selectedColor
                    
                    
                    //Info
                    cell.textLabel?.text = self.arrayFormulary[indexPath.row]
                    cell.detailTextLabel?.text = "\(self.stakeholder.whosId.count)"
                    
                    
                    if let sitemap = self.stakeholder.site, sitemap.title.trim().isEmpty == false && sitemap.siteId != 0 {
                        
                        cell.isUserInteractionEnabled = true
                        cell.backgroundColor =  UIColor.white
                    }
                    else
                    {
                        cell.isUserInteractionEnabled = false
                        cell.backgroundColor = DeviceType.IS_ANY_IPAD == false ? UIColor.whiteColor(0.5) : UIColor.groupTableViewBackground
                    }
                    
                    return cell
                    
                default:
                    return UITableViewCell()
                }
            }
            else
            {
                
                switch indexPath.row
                {
                case CompanyFields.name.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    
                    cell.txtText.text = self.stakeholder.name
                    cell.comboStyle(false)
                    
                    return cell
                    
                case CompanyFields.alias.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    cell.txtText.text = self.stakeholder.alias.collapseWithCommas()
                    cell.comboStyle(false)
                    
                    return cell
                    
                case CompanyFields.knowAddress.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    
                    cell.txtText.text = self.stakeholder.placeOfBirth
                    cell.comboStyle(false)
                    
                    return cell
                    
                case CompanyFields.dateFoundation.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    
                    cell.txtText.text = self.stakeholder.birthday?.getFormatDateString() ?? self.stakeholder.birthDateString
                    cell.comboStyle(true)
                    
                    return cell
                    
                case CompanyFields.country.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    
                    
                    if self.language == .english
                    {
                        cell.txtText.text = self.stakeholder.country?.valueEn
                    }
                    else
                    {
                        cell.txtText.text = self.stakeholder.country?.valueEs
                    }
                    
                    cell.comboStyle(true)
                    
                    return cell
                    
                case CompanyFields.industrySector.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    
                    if self.language == .english
                    {
                        cell.txtText.text = self.stakeholder.industrySector.valueEn
                    }
                    else
                    {
                        cell.txtText.text = self.stakeholder.industrySector.valueEs
                    }
                    
                    cell.comboStyle(false)
                    
                    return cell
                    
                case CompanyFields.nationalities.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    
                    if self.language == .english
                    {
                        cell.txtText.text = self.stakeholder.nationalities.collapseValues()
                    }
                    else
                    {
                        cell.txtText.text = self.stakeholder.nationalities.collapseValuesEs()
                    }
                    
                    cell.comboStyle(true)
                    
                    return cell
                    
                case CompanyFields.dueDate.rawValue:
                    
                    //Create Cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    cell.txtText.text = self.stakeholder.dueDate?.getFormatDateString()
                    cell.comboStyle(true)
                    
                    return cell
                    
                case CompanyFields.site.rawValue:
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TableTextfieldCell") as! TableTextfieldCell
                    cell.selectionStyle = .none
                    cell.txtText.delegate = self
                    cell.txtText.placeholder = self.arrayFormulary[indexPath.row]
                    cell.txtText.tag = indexPath.row
                    
                    if let sitemap = self.stakeholder.site, sitemap.title.trim().isEmpty == false && sitemap.siteId != 0 {
                        
                        cell.txtText.text = sitemap.title
                    }
                    
                    cell.comboStyle(self.stakeholder.sites.count <= 2 ? false : true)
                    
                    return cell
                    
                case CompanyFields.associatedWhos.rawValue:
                    
                    //Whos
                    let cell = UITableViewCell(style: .value1, reuseIdentifier:"Cell")
                    cell.accessoryType = .disclosureIndicator
                    cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
                    cell.textLabel?.textColor = UIColor.blackAsfalto()
                    
                    //Selection color
                    let selectedColor = UIView()
                    selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
                    cell.selectedBackgroundView = selectedColor
                    
                    
                    //Info
                    cell.textLabel?.text = self.arrayFormulary[indexPath.row]
                    cell.detailTextLabel?.text = "\(self.stakeholder.whosId.count)"
                    
                    
                    if let sitemap = self.stakeholder.site, sitemap.title.trim().isEmpty == false && sitemap.siteId != 0 {
                        
                        cell.isUserInteractionEnabled = true
                        cell.backgroundColor =  UIColor.white
                    }
                    else
                    {
                        cell.isUserInteractionEnabled = false
                        cell.backgroundColor = DeviceType.IS_ANY_IPAD == false ? UIColor.whiteColor(0.5) : UIColor.groupTableViewBackground
                    }
                    
                    return cell
                    
                default:
                    return UITableViewCell()
                }
            }
        case AddStakeholderSection.comments.rawValue:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentsTableCell") as! CommentsTableCell
            cell.selectionStyle = .none
            cell.isCompany = self.stakeholder.isCompany
            cell.onAnonymousSelected = { [weak self] state in
                                
                self?.stakeholder.IsAnonymous = state
            }
            cell.onTexting = { [weak self] text in
                
                if self?.stakeholder.isCompany == true
                {
                    
                    tableView.beginUpdates()
                    tableView.endUpdates()
                }
                else
                {
                    //TableView animations
                    let currentOffset = tableView.contentOffset
                    UIView.setAnimationsEnabled(false)
                    tableView.beginUpdates()
                    tableView.endUpdates()
                    UIView.setAnimationsEnabled(true)
                    tableView.setContentOffset(currentOffset, animated: false)
                }

                print(text)
                self?.stakeholder.comments = text
            }
            
            return cell
            
        case AddStakeholderSection.coordinator_Representative.rawValue:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellStakeholderCordRepre") as! CellStakeholderCordRepre
            cell.selectionStyle = .none
            
            cell.loadInfoEditStakeholder(self.stakeholder)
            cell.onCoordinatorEmpty = { [weak self] Void in
                
                let vcSearchMember = Storyboard.getInstanceOf(ContactSearchViewController.self)
                vcSearchMember.isModalView = false
                
                if self?.didChangeSite == true
                {
                    vcSearchMember.siteIdSearchParameter = self?.stakeholder.site?.siteId
                }
                else
                {
                    vcSearchMember.stakeholderIdSearchParameter = self?.stakeholder.id
                }
                
                vcSearchMember.addContactToInstance = { contact in
                    
                    let selectedMember = contact as! Member
                    self?.stakeholder.coordinator = selectedMember
                    
                    //Refresh Cell
                    self?.tableForm.reloadRows(at: [indexPath], with: .none)
                }
                
                self?.navigationController?.pushViewController(vcSearchMember, animated: true)
            }
            
            cell.onRepresentativeEmpty = { [weak self] Void in
                
                let vcSearchMember = Storyboard.getInstanceOf(ContactSearchViewController.self)
                vcSearchMember.isModalView = false
               
                if self?.didChangeSite == true
                {
                    vcSearchMember.siteIdSearchParameter = self?.stakeholder.site?.siteId
                }
                else
                {
                    vcSearchMember.stakeholderIdSearchParameter = self?.stakeholder.id
                }
                
                vcSearchMember.addContactToInstance = { contact in
                    
                    let selectedMember = contact as! Member
                    self?.stakeholder.representative = selectedMember
                    
                    //Refresh Cell
                    self?.tableForm.reloadRows(at: [indexPath], with: .none)
                }
                
                self?.navigationController?.pushViewController(vcSearchMember, animated: true)
            }
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    //MARK: TABLE VIEW DELEGATE
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.section
        {
        case AddStakeholderSection.fields.rawValue:
            
            //WHO SELECTION ON STAKEHOLDER TYPE
            if self.stakeholder.isCompany == false && (indexPath.row == PersonFields.associatedWhos.rawValue || indexPath.row == PersonFields.site.rawValue)
            {
                self.view.endEditing(true)
                guard self.stakeholder.site != nil else {
                    
                    MessageManager.shared.showBar(title: nil, subtitle: "Select a Site".localized(), type: .warning, fromBottom: false)
                    
                    return
                }
                
                let vcWhosLevelOne: Whos1TableViewController =  Storyboard.getInstanceFromStoryboard("Integration")
                vcWhosLevelOne.stakeholder = self.stakeholder
                vcWhosLevelOne.onAutomaticSave = { [weak self] totalWhos in
                    
                    self?.stakeholder.whosId = totalWhos
                    
                    //Update
                    if self?.stakeholder.isCompany == false
                    {
                        
                        if self?.type == .new
                        {
                            self?.tableForm.reloadRows(at: [indexPath], with: .fade)
                        }
                        else
                        {
                            self?.tableForm.reloadRows(at: [indexPath], with: .fade)
                        }
                    }
                }
                
                self.navigationController?.pushViewController(vcWhosLevelOne, animated: true)
            }
            else if self.stakeholder.isCompany == true && indexPath.row == CompanyFields.associatedWhos.rawValue
            {
                //WHO SELECTION ON COMPANY TYPE
                self.view.endEditing(true)
                guard self.stakeholder.site != nil else {
                    
                    MessageManager.shared.showBar(title: nil, subtitle: "Select a Site".localized(), type: .warning, fromBottom: false)
                    
                    return
                }
                
                let vcWhosLevelOne: Whos1TableViewController =  Storyboard.getInstanceFromStoryboard("Integration")
                vcWhosLevelOne.stakeholder = self.stakeholder
                vcWhosLevelOne.onAutomaticSave = { [weak self] totalWhos in
                    
                    self?.stakeholder.whosId = totalWhos
                    
                    //Update
                    if self?.stakeholder.isCompany == true
                    {
                        if self?.type == .new
                        {
                            self?.tableForm.reloadRows(at: [indexPath], with: .fade)
                        }
                        else
                        {
                            self?.tableForm.reloadRows(at: [indexPath], with: .fade)
                        }
                        
                    }
                }
                
                self.navigationController?.pushViewController(vcWhosLevelOne, animated: true)
            }
            
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        
        // Remove seperator inset
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset))
        {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))
        {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.responds(to: #selector(setter: UIView.layoutMargins))
        {
            cell.layoutMargins = UIEdgeInsets.zero
        }
        
        //Has only one site (autoselected)
        if self.stakeholder.isCompany == false
        {
            if indexPath.row == PersonFields.site.rawValue
            {
                cell.backgroundColor = self.stakeholder.sites.count <= 2 ? UIColor.whiteColor(0.5) : .white
                cell.isUserInteractionEnabled = self.stakeholder.sites.count <= 2 ? false : true
            }
            else
            {
                cell.backgroundColor = UIColor.white
                cell.isUserInteractionEnabled = true
            }
        }
        else
        {
            if indexPath.row == CompanyFields.site.rawValue
            {
                cell.backgroundColor = self.stakeholder.sites.count <= 2 ? UIColor.whiteColor(0.5) : .white
                cell.isUserInteractionEnabled = self.stakeholder.sites.count <= 2 ? false : true
            }
            else
            {
                cell.backgroundColor = UIColor.white
                cell.isUserInteractionEnabled = true
            }
        }
        
        
        switch self.type
        {
        case .new:
            
            //Show or hide associated whos depending role
            if indexPath.section == AddStakeholderSection.fields.rawValue
            {
                if LibraryAPI.shared.currentUser?.role == .globalManager || LibraryAPI.shared.currentUser?.role == .localManager
                {
                    if self.stakeholder.isCompany == true
                    {
                        switch indexPath.row
                        {
                        case CompanyFields.alias.rawValue: //Client changed: Creacion de Company = Quitar Alias, type of business, Nationalities
                            cell.isHidden = true
                        case CompanyFields.nationalities.rawValue: //Client changed: Creacion de Company = Quitar Alias, type of business, Nationalities
                            cell.isHidden = true
                            
                        case CompanyFields.site.rawValue:
                            cell.isHidden = self.stakeholder.sites.count <= 2 ? true : false
                            
                        default:
                            cell.isHidden = false
                        }
                    }
                    else
                    {
                        switch indexPath.row
                        {
                        case PersonFields.site.rawValue:
                            cell.isHidden = self.stakeholder.sites.count <= 2 ? true : false
                        default:
                            cell.isHidden = false
                        }
                    }
                }
                else
                {
                    if self.stakeholder.isCompany == true
                    {
                        switch indexPath.row
                        {
                        case CompanyFields.alias.rawValue: //Client changed: Creacion de Company = Quitar Alias, type of business, Nationalities
                            cell.isHidden = true
                        case CompanyFields.nationalities.rawValue: //Client changed: Creacion de Company = Quitar Alias, type of business, Nationalities
                            cell.isHidden = true
                        case CompanyFields.site.rawValue:
                            cell.isHidden = self.stakeholder.sites.count <= 2 ? true : false
                        case CompanyFields.associatedWhos.rawValue:
                            cell.isHidden = true
                        default:
                            cell.isHidden = false
                        }
                    }
                    else
                    {
                        switch indexPath.row
                        {
                        case PersonFields.site.rawValue:
                            cell.isHidden = self.stakeholder.sites.count <= 2 ? true : false
                        case PersonFields.associatedWhos.rawValue:
                            cell.isHidden = true
                        default:
                            cell.isHidden = false
                        }
                    }
                }
            }
            else if indexPath.section == AddStakeholderSection.comments.rawValue
            {
                if self.stakeholder.isCompany == false
                {
                    cell.isHidden = self.createSHcard == true ? false : true
                }
                else
                {
                    cell.isHidden = self.createCompanyDossier == true ? false : true
                }
            }
            else
            {
                cell.isHidden = false
            }
            
        case .editStakeholder:
            
            //Show or hide associated whos depending role
            if indexPath.section == AddStakeholderSection.fields.rawValue
            {
                if self.stakeholder.isCompany == true
                {
                    cell.isHidden = indexPath.row == CompanyFields.site.rawValue && self.stakeholder.sites.count <= 2 ? true : false
                }
                else
                {
                    cell.isHidden = indexPath.row == PersonFields.site.rawValue && self.stakeholder.sites.count <= 2 ? true : false
                }
            }
            else
            {
                cell.isHidden = false
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return self.type == .editStakeholder  && section == AddStakeholderSection.comments.rawValue ? CGFloat.leastNormalMagnitude : 44
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        //Show or hide associated whos depending role
        if indexPath.section == AddStakeholderSection.fields.rawValue
        {
            if self.type == .editStakeholder
            {
                if self.stakeholder.isCompany == true
                {
                    //Client changed: Creacion de Company = Quitar Alias, type of business, Nationalities
                    switch indexPath.row
                    {
                    case CompanyFields.alias.rawValue:
                        return CGFloat.leastNormalMagnitude
                    case CompanyFields.nationalities.rawValue:
                        return CGFloat.leastNormalMagnitude
                    case CompanyFields.site.rawValue:
                        return self.stakeholder.sites.count <= 2 ? CGFloat.leastNormalMagnitude : 44.0
                    default:
                        return 44.0
                    }
                }
                else
                {
                    return indexPath.row == PersonFields.site.rawValue && self.stakeholder.sites.count <= 2 ? CGFloat.leastNormalMagnitude : 44.0
                }
            }
            else
            {
                if LibraryAPI.shared.currentUser?.role == .globalManager || LibraryAPI.shared.currentUser?.role == .localManager
                {
                    if self.stakeholder.isCompany == true
                    {
                        //Client changed: Creacion de Company = Quitar Alias, type of business, Nationalities
                        switch indexPath.row
                        {
                        case CompanyFields.alias.rawValue:
                            return CGFloat.leastNormalMagnitude
                        case CompanyFields.nationalities.rawValue:
                            return CGFloat.leastNormalMagnitude
                        case CompanyFields.site.rawValue:
                            return self.stakeholder.sites.count <= 2 ? CGFloat.leastNormalMagnitude : 44.0
                        default:
                            return 44.0
                        }
                    }
                    else
                    {
                        if indexPath.row == PersonFields.site.rawValue
                        {
                            return self.stakeholder.sites.count <= 2 ? CGFloat.leastNormalMagnitude : 44.0
                        }
                        else
                        {
                            return 44.0
                        }
                    }
                }
                else
                {
                    if self.stakeholder.isCompany == true
                    {
                        //Client changed: Creacion de Company = Quitar Alias, type of business, Nationalities
                        switch indexPath.row
                        {
                        case CompanyFields.alias.rawValue:
                            return CGFloat.leastNormalMagnitude
                        case CompanyFields.nationalities.rawValue:
                            return CGFloat.leastNormalMagnitude
                        case CompanyFields.associatedWhos.rawValue:
                            return CGFloat.leastNormalMagnitude
                        case CompanyFields.site.rawValue:
                            return self.stakeholder.sites.count <= 2 ? CGFloat.leastNormalMagnitude : 44.0
                        default:
                            return 44.0
                        }
                    }
                    else
                    {
                        if indexPath.row == PersonFields.associatedWhos.rawValue
                        {
                            return CGFloat.leastNormalMagnitude
                        }
                        else if indexPath.row == PersonFields.site.rawValue
                        {
                            return self.stakeholder.sites.count <= 2 ? CGFloat.leastNormalMagnitude : 44.0
                        }
                        else
                        {
                            return 44.0
                        }
                    }
                }
            }
        }
        else if indexPath.section == AddStakeholderSection.comments.rawValue
        {
            if self.stakeholder.isCompany == false
            {
                return self.createSHcard == true ? tableView.rowHeight : CGFloat.leastNormalMagnitude
            }
            else
            {
                return self.createCompanyDossier == true ? tableView.rowHeight : CGFloat.leastNormalMagnitude
            }
        }
        else
        {
            return 60.0
        }
    }
}

//MARK: TEXTFIELD DELEGATE
extension AddStakeholderViewController: UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        let pointInTable = textField.convert(textField.bounds.origin, to: self.tableForm)
        self.textFieldIndexPath = self.tableForm.indexPathForRow(at: pointInTable)
        
        let withFreeTextOption = (stakeholder.isCompany && textField.tag == CompanyFields.dateFoundation.rawValue)
            || (!stakeholder.isCompany && textField.tag == PersonFields.birthdate.rawValue)
        
        textField.inputAccessoryView = loadToolBar(withFreeTextOption: withFreeTextOption)
        
        //Verify Index: 0 = Person | 1 = Company
        if self.stakeholder.isCompany == false
        {
            switch textField.tag
            {
            case PersonFields.country.rawValue:
                
                let vcCountries = ListCountries(list: self.arrayCountries)
                vcCountries.language = self.language
                vcCountries.isMultiselect = false
                vcCountries.onSelectedCountry = { [weak self] country in
                    
                    textField.text = self?.language == .english ? country.valueEn : country.valueEs
                    self?.stakeholder.country = country
                }
                
                textField.inputView = vcCountries.view
                
            case PersonFields.birthdate.rawValue:
                
                datePickerBirthDate = UIDatePicker()
                datePickerBirthDate?.tag = textField.tag
                datePickerBirthDate?.datePickerMode = .date
                datePickerBirthDate?.backgroundColor = UIColor.groupTableViewBackground
                datePickerBirthDate?.addTarget(self, action: #selector(self.dateValue(_:)), for: .valueChanged)
                datePickerBirthDate?.date = Date()
                datePickerBirthDate?.isMultipleTouchEnabled = false
                datePickerBirthDate?.isExclusiveTouch = true
                textField.inputView = datePickerBirthDate
                textFieldBirthDate = textField
                
            case PersonFields.maritalStatus.rawValue:
                
                let pickerStatus = UIPickerView()
                pickerStatus.delegate = self
                pickerStatus.dataSource = self
                pickerStatus.backgroundColor = UIColor.groupTableViewBackground
                pickerStatus.showsSelectionIndicator = true
                pickerStatus.isMultipleTouchEnabled = false
                pickerStatus.isExclusiveTouch = true
                pickerStatus.tag = 1
                textField.inputView  = pickerStatus
                
            case PersonFields.nationalities.rawValue:
                
                let vcCountries = ListCountries(list: self.arrayCountries)
                vcCountries.language = self.language
                vcCountries.isMultiselect = true
                vcCountries.onSelectedCountries = { [weak self] countries in
                    
                    var finalText = ""
                    
                    for (index,txt) in countries.enumerated()
                    {
                        if countries.count > 1
                        {
                            if index == 0
                            {
                                finalText.append("\(self?.language == .english ? txt.valueEn : txt.valueEs)")
                            }
                            else
                            {
                                finalText.append(", \(self?.language == .english ? txt.valueEn : txt.valueEs)")
                            }
                        }
                        else
                        {
                            finalText.append("\(self?.language == .english ? txt.valueEn : txt.valueEs)")
                        }
                    }
                    
                    textField.text = finalText
                    self?.stakeholder.nationalities = countries
                }
                
                textField.inputView = vcCountries.view
                
            case PersonFields.dueDate.rawValue:
                
                if self.type == .editStakeholder
                {
                    let pickerDateTime = UIDatePicker()
                    pickerDateTime.tag = textField.tag
                    pickerDateTime.datePickerMode = .date
                    pickerDateTime.backgroundColor = UIColor.groupTableViewBackground
                    pickerDateTime.addTarget(self, action: #selector(self.dateValue(_:)), for: .valueChanged)
                    pickerDateTime.minimumDate = self.minDueDate
                    pickerDateTime.isMultipleTouchEnabled = false
                    pickerDateTime.isExclusiveTouch = true
                    textField.inputView = pickerDateTime
                }
                else if self.type == .new
                {
                    let pickerDateTime = UIDatePicker()
                    pickerDateTime.tag = textField.tag
                    pickerDateTime.datePickerMode = .date
                    pickerDateTime.backgroundColor = UIColor.groupTableViewBackground
                    pickerDateTime.addTarget(self, action: #selector(self.dateValue(_:)), for: .valueChanged)
                    pickerDateTime.minimumDate = self.minDueDate
                    pickerDateTime.isMultipleTouchEnabled = false
                    pickerDateTime.isExclusiveTouch = true
                    textField.inputView = pickerDateTime
                }
                
            case PersonFields.site.rawValue:
                textField.endEditing(true)
                if DeviceType.IS_ANY_IPAD == true
                {
                    self.showSiteMenu_iPad()
                }
                else
                {
                    self.showSiteMenu()
                }
                
            default:
                break
            }
        }
        else
        {
            switch textField.tag
            {
            case CompanyFields.dateFoundation.rawValue:
                
                datePickerBirthDate = UIDatePicker()
                datePickerBirthDate?.tag = textField.tag
                datePickerBirthDate?.datePickerMode = .date
                datePickerBirthDate?.backgroundColor = UIColor.groupTableViewBackground
                datePickerBirthDate?.addTarget(self, action: #selector(self.dateValue(_:)), for: .valueChanged)
                datePickerBirthDate?.date = Date()
                datePickerBirthDate?.isMultipleTouchEnabled = false
                datePickerBirthDate?.isExclusiveTouch = true
                textField.inputView = datePickerBirthDate
                textFieldBirthDate = textField
                
            case CompanyFields.country.rawValue:
                
                let vcCountries = ListCountries(list: self.arrayCountries)
                vcCountries.language = self.language
                vcCountries.isMultiselect = false
                vcCountries.onSelectedCountry = { [weak self] country in
                    
                    textField.text = self?.language == .english ? country.valueEn : country.valueEs
                    self?.stakeholder.country = country
                }
                textField.inputView = vcCountries.view
                
                
            case CompanyFields.nationalities.rawValue:
                
                let vcCountries = ListCountries(list: self.arrayCountries)
                vcCountries.language = self.language
                vcCountries.isMultiselect = true
                vcCountries.onSelectedCountries = { [weak self] countries in
                    
                    var finalText = ""
                    
                    for (index,txt) in countries.enumerated()
                    {
                        if countries.count > 1
                        {
                            if index == 0
                            {
                                finalText.append("\(self?.language == .english ? txt.valueEn : txt.valueEs)")
                            }
                            else
                            {
                                finalText.append(", \(self?.language == .english ? txt.valueEn : txt.valueEs)")
                            }
                        }
                        else
                        {
                            finalText.append("\(self?.language == .english ? txt.valueEn : txt.valueEs)")
                        }
                    }
                    
                    textField.text = finalText
                    self?.stakeholder.nationalities = countries
                }
                
                textField.inputView = vcCountries.view
                
            case CompanyFields.dueDate.rawValue:
                
                if self.type == .editStakeholder
                {
                    let pickerDateTime = UIDatePicker()
                    pickerDateTime.tag = textField.tag
                    pickerDateTime.datePickerMode = .date
                    pickerDateTime.backgroundColor = UIColor.groupTableViewBackground
                    pickerDateTime.addTarget(self, action: #selector(self.dateValue(_:)), for: .valueChanged)
                    pickerDateTime.minimumDate = self.minDueDate
                    pickerDateTime.isMultipleTouchEnabled = false
                    pickerDateTime.isExclusiveTouch = true
                    textField.inputView = pickerDateTime
                }
                else if self.type == .new
                {
                    let pickerDateTime = UIDatePicker()
                    pickerDateTime.tag = textField.tag
                    pickerDateTime.datePickerMode = .date
                    pickerDateTime.backgroundColor = UIColor.groupTableViewBackground
                    pickerDateTime.addTarget(self, action: #selector(self.dateValue(_:)), for: .valueChanged)
                    pickerDateTime.minimumDate = self.minDueDate
                    pickerDateTime.isMultipleTouchEnabled = false
                    pickerDateTime.isExclusiveTouch = true
                    textField.inputView = pickerDateTime
                }
                
            case CompanyFields.site.rawValue:
                self.view.endEditing(true)
                if DeviceType.IS_ANY_IPAD == true
                {
                    self.showSiteMenu_iPad()
                }
                else
                {
                    self.showSiteMenu()
                }                
            default:
                break
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if (stakeholder.isCompany && textField.tag == CompanyFields.dateFoundation.rawValue)
            || (!stakeholder.isCompany && textField.tag == PersonFields.birthdate.rawValue) {
            let cell = tableForm.cellForRow(at: textFieldIndexPath) as! TableTextfieldCell
            if textField.inputView == datePickerBirthDate {
                cell.txtText.text = datePickerBirthDate?.date.showStringFormat("MM/dd/yyyy")
                stakeholder.birthday = datePickerBirthDate?.date
                stakeholder.birthDateString = nil
            } else {
                cell.txtText.text = textField.text
                stakeholder.birthDateString = textField.text
                stakeholder.birthday = nil
            }
        }
        
        //Verify Index: 0 = Person | 1 = Company
        if self.stakeholder.isCompany == false
        {
            if textField.tag == 0
            {
                self.stakeholder.name = textField.text!.trimExcessiveSpaces()
            }
            else if textField.tag == 1
            {
                self.stakeholder.lastName = textField.text!.trimExcessiveSpaces()
            }
            else if textField.tag == 2
            {
                let text = textField.text!.trimExcessiveSpaces()
                
                if text.containsString(",") == true
                {
                    let arrayAlias = text.components(separatedBy: ",")
                    self.stakeholder.alias = arrayAlias.map({$0.trim()})
                }
                else
                {
                    let arrayAlias = text.components(separatedBy: " ")
                    self.stakeholder.alias = arrayAlias.map({$0.trim()})
                }
            }
            else if textField.tag == 3
            {
                if self.language == .english
                {
                    self.stakeholder.jobPosition?.valueEn = textField.text!.trimExcessiveSpaces()
                }
                else
                {
                    self.stakeholder.jobPosition?.valueEs = textField.text!.trimExcessiveSpaces()
                }
            }
            else if textField.tag == 4
            {
                self.stakeholder.companyName = textField.text!.trimExcessiveSpaces()
            }
            else if textField.tag == 5
            {
                self.stakeholder.placeOfBirth = textField.text!.trimExcessiveSpaces()
            }
            else if textField.tag == 9
            {
                if self.language == .english
                {
                    self.stakeholder.occupation?.valueEn = textField.text!.trimExcessiveSpaces()
                }
                else
                {
                    self.stakeholder.occupation?.valueEs = textField.text!.trimExcessiveSpaces()
                }
            }
        }
        else
        {
            if textField.tag == 0
            {
                self.stakeholder.name = textField.text!.trimExcessiveSpaces()
            }
            else if textField.tag == 1
            {
                let text = textField.text!.trimExcessiveSpaces()
                
                if text.containsString(",") == true
                {
                    let arrayAlias = text.components(separatedBy:  ",")
                    self.stakeholder.alias = arrayAlias.map({$0.trim()})
                }
                else
                {
                    let arrayAlias = text.components(separatedBy:  " ")
                    self.stakeholder.alias = arrayAlias.map({$0.trim()})
                }
            }
            else if textField.tag == 2
            {
                self.stakeholder.placeOfBirth = textField.text!.trimExcessiveSpaces()
            }
            else if textField.tag == 5 // industry/sector
            {
                if self.language == .english
                {
                    self.stakeholder.industrySector.valueEn = textField.text!.trimExcessiveSpaces()
                }
                else
                {
                    self.stakeholder.industrySector.valueEs = textField.text!.trimExcessiveSpaces()
                }
            }
        }
    }
}

extension AddStakeholderViewController: UIPickerViewDataSource, UIPickerViewDelegate
{
    // MARK: DATASOURCE UIPICKERVIEW
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if pickerView.tag == 1
        {
            return arrayMarital.count
        }
        else if /*(pickerView.tag == 11 && self.type == .EditStakeholder) || (pickerView.tag == 7 && self.type == .EditStakeholder) || */(pickerView.tag == 12 && self.type == .new) || (pickerView.tag == 8 && self.type == .new)
        {
            return self.stakeholder.sites.count
        }
        else
        {
            return arrayBusiness.count
        }
    }
    
    // MARK: DELEGATE UIPICKERVIEW
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if pickerView.tag == 1
        {
            return (self.language == .english) ? arrayMarital[row].valueEn : arrayMarital[row].valueEs
        }
//            //When selecting Site Cell at Editing Mode
//        else if (pickerView.tag == 11 || pickerView.tag == 7) && self.type == .EditStakeholder
//        {
//            return self.stakeholder.sites?[row].title ?? String()
//        }
            //When selecting Site Cell at New Stakeholder Mode
        else if pickerView.tag == 12 && self.type == .new
        {
            return self.stakeholder.sites[row].title
        }
        else if pickerView.tag == 8 && self.type == .new && self.stakeholder.isCompany == true
        {
            return self.stakeholder.sites[row].title
        }
        else
        {
            return (self.language == .english) ? arrayBusiness[row].valueEn : arrayBusiness[row].valueEs
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        var text = String()
        var site = Site()
        
        if pickerView.tag == 1
        {
            text = (self.language == .english) ? arrayMarital[row].valueEn : arrayMarital[row].valueEs
        }
//        else if (pickerView.tag == 11 || pickerView.tag == 7) && self.type == .EditStakeholder
//        {
//            site = self.stakeholder.sites?[row] ?? Site()
//            text = site.title
//        }
        else if pickerView.tag == 12 && self.type == .new
        {
            site = self.stakeholder.sites[row]
            text = site.title
        }
        else if pickerView.tag == 8 && self.type == .new && self.stakeholder.isCompany == true
        {
            site = self.stakeholder.sites[row] 
            text = site.title
        }
        else
        {
            text = (self.language == .english) ? arrayBusiness[row].valueEn : arrayBusiness[row].valueEs
        }
        
        //Instance of cell
        let cell = self.tableForm.cellForRow(at: self.textFieldIndexPath) as! TableTextfieldCell
        
        guard row != 0 else {
            
            text = String()
            cell.txtText.text = String()
            self.stakeholder.maritalStatus = nil
            
            
            //Clean Site
            if  (pickerView.tag == 12 && self.type == .new) || (pickerView.tag == 11 && self.type == .editStakeholder)
            {
                if self.type == .new
                {
                    self.stakeholder.site = nil
                    self.stakeholder.whosId.removeAll()
                    
                    if self.stakeholder.isCompany == false
                    {
                        let indexPath = IndexPath(row: 13, section: 0 )
                        self.tableForm.reloadRows(at: [indexPath], with: .fade)
                    }
                }
                else
                {
                    //Edit Stakeholder Mode
                    self.stakeholder.site = nil
                    self.stakeholder.whosId.removeAll()
                    
                    if self.stakeholder.isCompany == false
                    {
                        let indexPathSite = IndexPath(row: 11, section: 0 )
                        let indexPathWho = IndexPath(row: 12, section: 0 )
                        self.tableForm.reloadRows(at: [indexPathWho, indexPathSite], with: .fade)
                    }
                }
            }
            
            return
        }
        
        cell.txtText.text = text
        
        if pickerView.tag == 1
        {
            self.stakeholder.maritalStatus = arrayMarital[row]
        }
        else if pickerView.tag == 11 && self.type == .editStakeholder
        {
            self.didChangeSite = true
            self.stakeholder.site = site
            self.stakeholder.whosId.removeAll()
            
            if self.stakeholder.isCompany == false
            {
                let indexPathWho = IndexPath(row: 12, section: 0 )
                self.tableForm.reloadRows(at: [indexPathWho], with: .fade)
            }
            
            self.stakeholder.coordinator = nil
            self.stakeholder.representative = nil
            self.tableForm.reloadSections(IndexSet(integer: AddStakeholderSection.coordinator_Representative.rawValue), with: .none)
        }
        else if (pickerView.tag == 12 || pickerView.tag == 8) && self.type == .new
        {
            self.stakeholder.site = site
            self.stakeholder.whosId.removeAll()
            
            if self.stakeholder.isCompany == false
            {
                let indexPath = IndexPath(row: 13, section: 0 )
                self.tableForm.reloadRows(at: [indexPath], with: .fade)
            }
        }
    }
}

extension AddStakeholderViewController: DropUpMenuDelegate
{
    func dropUpMenu(_ dropUpMenu: DropUpMenu, didSelectRowAtIndexPath indexPath: IndexPath)
    {
        self.stakeholder.site = self.stakeholder.sites[indexPath.row]
        self.stakeholder.whosSiteId = self.stakeholder.site?.siteId ?? -1
        if self.stakeholder.isCompany == false
        {
            self.tableForm.reloadRows(at: [IndexPath(row: PersonFields.site.rawValue, section: AddStakeholderSection.fields.rawValue)], with: .none)
        }
        else
        {
            self.tableForm.reloadRows(at: [IndexPath(row: CompanyFields.site.rawValue, section: AddStakeholderSection.fields.rawValue)], with: .none)
        }
    }
}

