//
//  FeedsTable.swift
//  SHM
//
//  Created by Manuel Salinas on 5/19/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

enum PostType
{
    case recents
    case backups
    case attachments
}

private enum HeightCell: CGFloat
{
    case recent = 143.0
    case backupOrAttachment = 115.0
}

class FeedsTable: UITableViewController
{
    //MARK: OUTLETS & PROPERTIES
    var willDisplayLastRow: ((_ row:Int) -> ())?
    var type = PostType.recents
    var posts = [Post]() {
        didSet {
            
            self.tableView.reloadData()
        }
    }
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        //Table
        self.tableView.hideEmtpyCells()
        self.tableView.backgroundColor = UIColor.blueSky()
        
        //Register Custom Cell
        self.tableView.register(UINib(nibName: "EditStakeholderFeedCell", bundle: nil), forCellReuseIdentifier: "EditStakeholderFeedCell")
    }
    
    //MARK: ACTIONS
    func getPosts() -> [Post]
    {
        return self.posts
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //Background Message
        if self.posts.count > 0
        {
            self.tableView.dismissBackgroundMessage()
        }
        else
        {
            self.tableView.displayBackgroundMessage("No posts".localized(),
                                                    subMessage: "")
        }

        return self.posts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditStakeholderFeedCell") as! EditStakeholderFeedCell
        cell.isRecentFeed = self.type == .recents ? true : false
       
        //Extract Post
        let post = self.posts[indexPath.row]
        
        //Color for selected cell
        let selectedColor = UIView()
        selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
        cell.selectedBackgroundView = selectedColor
        
        cell.tvText.delegate = self
        cell.cellForPost(cell, post: post)
        
        //Action
        cell.onStakeholderTap = { [weak self] stakeholder in
            
            //Enabled or Disabled
            guard stakeholder.isEnable == true else {
                
                MessageManager.shared.showBar(title: stakeholder.fullName, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                
                return
            }
            
            let actordetailVC = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
            actordetailVC.stakeholder = stakeholder
            
            self?.navigationController?.pushViewController(actordetailVC, animated: true)
        }
        
        cell.onTextViewTap = { [weak self] Void in
            
            let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController_iPad.self)
            vcDetailPost.post = post
            
            self?.navigationController?.pushViewController(vcDetailPost, animated: true)
        }
        
        //Available just for Recents
        cell.onSaveClipboard = { Void in
            
            let title =   post.title.getEditedTitleForPost(post, truncate: false)
            UIPasteboard.general.string = title
            
            //Sucess Indicator
            MessageManager.shared.showSuccessHUD()
            
            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                MessageManager.shared.hideHUD()
            }

        }
        
        cell.onBackupTap = { Void in
            
            LibraryAPI.shared.feedBO.filedPost(postId: post.id, onSuccess: { post in
                
                self.posts.remove(at: indexPath.row)
                tableView.reloadData()
                
                }, onError: { (error) in
                    
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            })
        }
        
        return cell
    }
    
     // MARK: Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)

        let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController_iPad.self)
        vcDetailPost.post = self.posts[indexPath.row]

        self.navigationController?.pushViewController(vcDetailPost, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        //Lateral UI
        cell.backgroundColor = UIColor.blueSky()
        
        //Load more
        let cellToDisplay = cell as! EditStakeholderFeedCell
        cellToDisplay.layoutConfForCell(cellToDisplay)
        
        if indexPath.row == self.posts.count - 1
        {
            self.willDisplayLastRow?(indexPath.row)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return self.type == .recents ? HeightCell.recent.rawValue : HeightCell.backupOrAttachment.rawValue
    }
}

 // MARK: TEXTVIEW DELEGATE
extension FeedsTable: UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
    {
        let arrayUrl = URL.description.components(separatedBy: "_")
        
        if arrayUrl.count > 1
        {
            let type = arrayUrl[0]
            
            switch type
            {
            case "s":
                //Go to Stakeholder detail
                let stakeholderLinked: Stakeholder! = Stakeholder(isDefault: true)
                stakeholderLinked.id = Int(arrayUrl[1])!
                stakeholderLinked.isEnable = Int(arrayUrl[2])! == 1 ? true : false
                
                let firstname = arrayUrl[3]
                
                //Enabled or Disabled
                guard stakeholderLinked.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: firstname, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                    
                    return true
                }
                
                //Workflow iPad / iPhone
                if DeviceType.IS_ANY_IPAD == true
                {
                    let vcDetailStakeholders = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
                    vcDetailStakeholders.stakeholder = stakeholderLinked
                    navigationController?.pushViewController(vcDetailStakeholders, animated: true)
                }
                else
                {
                    let vcDetailStakeholders : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                    vcDetailStakeholders.stakeholder = stakeholderLinked
                    navigationController?.pushViewController(vcDetailStakeholders, animated: true)
                }
            case "p":
                //Go to Project detail
                let projectLinked: Project! = Project(isDefault: true)
                projectLinked.id = Int(arrayUrl[1])!
                projectLinked.isEnable = Int(arrayUrl[2])! == 1 ? true : false
                
                //Enabled or Disabled
                guard projectLinked.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: nil, subtitle: "This project is disabled".localized(), type: .info, fromBottom: false)
                    
                    return true
                }
                
                //Go to detail
                if DeviceType.IS_ANY_IPAD == true
                {
                    let vcDetailProject = Storyboard.getInstanceOf(ProjectDetailViewController_iPad.self)
                    vcDetailProject.project = projectLinked
                    navigationController?.pushViewController(vcDetailProject, animated: true)
                    
                }
                else
                {
                    let vcDetailProject : DetailProjectViewController = Storyboard.getInstanceFromStoryboard("Projects")
                    vcDetailProject.project = projectLinked
                    navigationController?.pushViewController(vcDetailProject, animated: true)
                }
                
            case "d":
                //Add Event to Calendar
                let alert = UIAlertController(title:"Add Event".localized(), message: "Would you like to save this date in your device calendar".localized(), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Save".localized(), style: .default, handler: {
                    (action) in
                    
                    for thePost in self.posts where thePost.id == Int(arrayUrl[2])!
                    {
                        for theEvent in thePost.events where theEvent.id == Int(arrayUrl[1])!
                        {
                            CalendarManager.sharedInstance.createEvent(thePost.title.getEditedTitleForPost(thePost, truncate: false), event: theEvent)
                        }
                    }
                    
                }))
                self.present(alert, animated: true, completion: nil)
            case "g":
                let geographyId = Int(arrayUrl[1])!
                if DeviceType.IS_ANY_IPAD == false
                {
                    let vcGeoDetail:GeographyDetailViewController = UIStoryboard(name: "Geography", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetailViewController") as! GeographyDetailViewController
                    vcGeoDetail.geographyId = geographyId
                    self.navigationController?.pushViewController(vcGeoDetail, animated: true)
                }
                else
                {
                    let vcGeoDetail:GeographyDetail_iPad = UIStoryboard(name: "GeographyPad", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetail_iPad") as! GeographyDetail_iPad
                    vcGeoDetail.geographyId = geographyId
                    self.navigationController?.pushViewController(vcGeoDetail, animated: true)
                }
            default:
                break
            }
        }
        return true
    }
}
