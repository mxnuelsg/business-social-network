//
//  SwitchTableCell.swift
//  SHM
//
//  Created by Manuel Salinas on 3/3/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class SwitchTableCell: UITableViewCell
{
    //MARK: OUTLETS & PROPERTIES
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var swSwitch: UISwitch!
    
    var onSwitch:((_ on: Bool ) -> ())?

    //MARK: LIFE CYCLE
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.lblTitle.adjustsFontSizeToFitWidth = true
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: ACTIONS
    @IBAction fileprivate func switchChanged(_ sender: UISwitch)
    {
        self.onSwitch?(sender.isOn)
    }
}
