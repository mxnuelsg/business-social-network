//
//  AdittionalFieldCell.swift
//  SHM
//
//  Created by Manuel Salinas on 5/27/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class AdittionalFieldCell: UITableViewCell
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var tvContent: UITextView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.loadConfig()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    fileprivate func loadConfig()
    {
        self.tvContent.cornerRadius()
        self.tvContent.setBorder()
    }
    
}
