//
//  RichTextCell.swift
//  SHM
//
//  Created by Manuel Salinas on 5/25/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class RichTextCell: UITableViewCell
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnReference: UIButton!
    @IBOutlet weak var btnSpecialCharacters: UIButton!
    @IBOutlet weak var webViewTextArea: UIWebView!
    @IBOutlet weak var tvContent: UITextView!
    
    var content = String() {
        didSet {
            self.loadContent()
        }
    }
    
    var onReferenceTap:(() -> ())?
    var onSpecialCharacters:(() -> ())?
    var onTextViewTap : (() -> ())?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.loadConfig()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        //Actions
        self.tvContent.removeGestureRecognizersOfType(ActionsTapGestureRecognizer.self)
        self.tvContent.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
            
            self.onTextViewTap?()
        }))
        
        self.tvContent.setBorder()
        self.tvContent.cornerRadius()
    }
    
    //MARK: ACTIONS
    func loadContent()
    {
        //Content
        var jScript = "document.getElementById('mce_0_ifr').contentWindow.document.body.innerHTML = \"Tap here to start typing\";"
        
        if self.content.trim().isEmpty == false
        {
            jScript = "document.getElementById('mce_0_ifr').contentWindow.document.body.innerHTML = \"\(self.content)\";"
        }
        
        self.webViewTextArea.stringByEvaluatingJavaScript(from: jScript)
    }
    
    func openReference()
    {
        self.onReferenceTap?()
    }
    
    func openSpecialcharacters()
    {
        self.onSpecialCharacters?()
    }
    
    func done()
    {
        if let htmlResult = self.webViewTextArea.stringByEvaluatingJavaScript(from: "tinymce.activeEditor.getContent();") {
            
            print(htmlResult)
        }
        
        if let plainText = self.webViewTextArea.stringByEvaluatingJavaScript(from: "tinymce.activeEditor.getContent({format : 'text'});") {
            
            print(plainText)
        }
    }
}
