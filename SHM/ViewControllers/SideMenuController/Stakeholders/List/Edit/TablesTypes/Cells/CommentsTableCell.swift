//
//  CommentsTableCell.swift
//  SHM
//
//  Created by Manuel Salinas on 3/3/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class CommentsTableCell: UITableViewCell
{
    //MARK: OUTLETS & PROPERTIES
    @IBOutlet weak var textView: UITextView!
    var onTexting:((_ text: String) -> ())?
    var onAnonymousSelected:((_ state: Bool) -> ())?
    @IBOutlet weak var lblAnonymous: UILabel!
    @IBOutlet weak var anonymousSwitch: UISwitch!
    var isCompany = false {
        didSet {
            if self.isCompany == true
            {
                self.lblAnonymous.isHidden = true
                self.anonymousSwitch.isHidden = true
                self.constraintTopText.constant = 2
            }
            else
            {
                self.lblAnonymous.isHidden = false
                self.anonymousSwitch.isHidden = false
                self.constraintTopText.constant = 31
            }
        }
    }
    
    @IBOutlet weak var constraintTopText: NSLayoutConstraint!
    
    //MARK: LIFE CYCLE
    override func awakeFromNib()
    {
        super.awakeFromNib()
        textView.textColor = UIColor.grayCloudy()
        textView.text = "Comments...".localized()
        self.lblAnonymous.adjustsFontSizeToFitWidth = true
        self.lblAnonymous.text = "is anonymous request?".localized()
        self.anonymousSwitch.setOn(false, animated: false)
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    
    @IBAction func didChangeSwitch(_ sender: Any)
    {
        self.onAnonymousSelected?(self.anonymousSwitch.isOn)        
    }
}

extension CommentsTableCell: UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.text == "Comments...".localized()
        {
            textView.textColor = UIColor.black
            textView.text = String()
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.text.isEmpty == true
        {
            textView.textColor = UIColor.grayCloudy()
            textView.text = "Comments...".localized()
        }
    }
    
    func textViewDidChange(_ textView: UITextView)
    {
        self.onTexting?(textView.text)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        guard text != "\n"  else {
        
            return false
        }
        
        return true
    }
}
