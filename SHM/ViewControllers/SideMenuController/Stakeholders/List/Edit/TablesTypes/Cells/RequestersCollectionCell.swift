//
//  RequestersCollectionCell.swift
//  SHM
//
//  Created by Manuel Salinas on 6/2/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class RequestersCollectionCell: UICollectionViewCell
{
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var lblUsername: UILabel!

    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        self.setBorder(UIColor.blueTag())
        self.cornerRadius()
    }

}
