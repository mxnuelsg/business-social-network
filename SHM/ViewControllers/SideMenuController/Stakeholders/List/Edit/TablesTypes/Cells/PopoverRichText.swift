//
//  PopoverRichText.swift
//  SHM
//
//  Created by Manuel Salinas on 5/31/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import RichEditorView

class PopoverRichText: UIViewController
{
    //MARK: PROPERTIES AND OUTLETS
    @IBOutlet fileprivate var editorView: RichEditorView!
    @IBOutlet weak fileprivate var btnReference: UIButton!
    @IBOutlet weak fileprivate var btnSpecialCharacters: UIButton!
    
    var content = String()
    var onSave:((_ html: String, _ references: [StakeholderReference] ) -> ())?
    var specialCharacters = [StringKeyValueObject]()
   
    fileprivate var references = [StakeholderReference]()
    fileprivate var contentHTML = String()
    
    lazy var toolbar: RichEditorToolbar = {
        let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44))
        toolbar.options = [
            RichEditorDefaultOption.bold,
            RichEditorDefaultOption.italic,
            RichEditorDefaultOption.underline
        ]
        return toolbar
    }()

    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        //styles
        self.btnReference.layer.cornerRadius = 3
        self.btnReference.clipsToBounds = true
        self.btnReference.setBorder()
        self.btnReference.isEnabled = false
        self.btnReference.alpha = 0.85
        
        self.btnSpecialCharacters.layer.cornerRadius = 3
        self.btnSpecialCharacters.clipsToBounds = true
        self.btnSpecialCharacters.isEnabled = false
        self.btnSpecialCharacters.alpha = 0.85
    
        //Editor
        self.editorView.delegate = self
        self.editorView.inputAccessoryView = self.toolbar
        
        //Toolbar
        self.toolbar.delegate = self
        self.toolbar.editor = self.editorView
    
        //Load Content
        self.loadContent(self.content)
        
        //Localizations
        self.localize()
    }
    
    fileprivate func localize()
    {
        //Buttons
        self.btnReference.setTitle("Reference".localized(), for: UIControlState())
        self.btnSpecialCharacters.setTitle("SPECIAL CHARACTERS".localized(), for: UIControlState())
       
        //Editor
        self.editorView.placeholder = "Enter text here...".localized()
        // We will create a custom action that clears all the input text when it is pressed
        let clearOption = RichEditorOptionItem(image: nil, title: "Clear".localized()) { toolbar in
            
            toolbar.editor?.html = String()
        }
        
        let endOption = RichEditorOptionItem(image: nil, title: "Done".localized()) { toolbar in
            
            self.view.endEditing(true)
        }
        
        var options = toolbar.options
        options.append(clearOption)
        options.append(endOption)
        toolbar.options = options
    }
    
    //MARK: CONTENT GET / SET
    fileprivate func loadContent(_ text: String)
    {
        self.editorView.html = text
    }
    
    func getContentHTML() -> String
    {
        return self.contentHTML
    }
    
    //MARK: ACTIONS
    @IBAction func openReference()
    {
        self.view.endEditing(true)
        
        let vcAddReference = EditReferenceController()
        vcAddReference.preferredContentSize = CGSize(width: 700, height: 65)
        vcAddReference.modalPresentationStyle = .popover
        vcAddReference.popoverPresentationController?.sourceView = self.btnSpecialCharacters
        vcAddReference.onReferenceSaved = { (reference, isUrl) in
            
            let ref = isUrl == true ? "<a class='tooltip-reference' target='_blank' href='\(reference.value)'></a>" : ""
            
            self.content = self.contentHTML + "<sup>\(ref)\(reference.ordinalIndicator)</sup>.</p>"  //"(ʀᴇғ \(reference.ordinalIndicator))"
            self.loadContent(self.content)
            self.editorView.focus()
            
            self.references.append(reference)
        }
        
        self.present(vcAddReference, animated: true, completion: nil)
    }
    
    @IBAction func openSpecialcharacters()
    {
        let vcSpecials = KeyValueTableController()
        vcSpecials.concatenateKeyAndValue = true
        vcSpecials.options = self.specialCharacters
        vcSpecials.preferredContentSize = CGSize(width: 280, height: 200)
        vcSpecials.modalPresentationStyle = .popover
        vcSpecials.popoverPresentationController?.sourceView = self.btnSpecialCharacters
        vcSpecials.onSelectedOption = { (selected) in
            
            self.content = self.contentHTML + selected.value + "&nbsp;"
            self.loadContent(self.content)
            self.editorView.focus()
        }
        
        self.present(vcSpecials, animated: true, completion: nil)
    }
    
    @IBAction func done()
    {
        self.view.endEditing(true)
        self.onSave?(self.getContentHTML(), self.references)
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension PopoverRichText: RichEditorDelegate
{
    func richEditor(_ editor: RichEditorView, heightDidChange height: Int)
    {
        
    }
    
    func richEditor(_ editor: RichEditorView, contentDidChange content: String)
    {
        if content.isEmpty
        {
            print("HTML Preview")
        }
        else
        {
            self.contentHTML = content
            print(content)
        }
    }
    
    func richEditorTookFocus(_ editor: RichEditorView)
    {
        self.btnReference.isEnabled = true
        self.btnReference.alpha = 1
        
        self.btnSpecialCharacters.isEnabled = true
        self.btnSpecialCharacters.alpha = 1
    }
    
    func richEditorLostFocus(_ editor: RichEditorView)
    {
        self.btnReference.isEnabled = false
        self.btnReference.alpha = 0.85
        
        self.btnSpecialCharacters.isEnabled = false
        self.btnSpecialCharacters.alpha = 0.85
    }
    
    func richEditorDidLoad(_ editor: RichEditorView)
    {
    }
    
    @nonobjc func richEditor(_ editor: RichEditorView, shouldInteractWithURL url: URL) -> Bool
    {
        return true
    }
    
    func richEditor(_ editor: RichEditorView, handleCustomAction content: String)
    {
        
    }
}

extension PopoverRichText: RichEditorToolbarDelegate
{
    
    fileprivate func randomColor() -> UIColor {
        let colors = [
            UIColor.red,
            UIColor.orange,
            UIColor.yellow,
            UIColor.green,
            UIColor.blue,
            UIColor.purple
        ]
        
        let color = colors[Int(arc4random_uniform(UInt32(colors.count)))]
        return color
    }
    
    func richEditorToolbarChangeTextColor(_ toolbar: RichEditorToolbar)
    {
        let color = randomColor()
        toolbar.editor?.setTextColor(color)
    }
    
    func richEditorToolbarChangeBackgroundColor(_ toolbar: RichEditorToolbar)
    {
        let color = randomColor()
        toolbar.editor?.setTextBackgroundColor(color)
    }
}
 
