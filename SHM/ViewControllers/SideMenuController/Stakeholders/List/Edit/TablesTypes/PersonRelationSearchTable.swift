//
//  PersonRelationSearch.swift
//  SHM
//
//  Created by Manuel Salinas on 5/19/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class PersonRelationSearchTable: UIViewController
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var searchBar: AutoSearchBar!
    @IBOutlet weak var tvResults: UITableView!
    
    var contacts = [Contact]()
    var addContactToInstance: ((_ contactAdded: Contact) -> ())?
    var isModal = true
    var onModalViewControllerClosed : (() -> ())?
    
    //Properties for New Post
    var willBeShowedInNewPost = false
    @IBOutlet weak var constraintTableTop: NSLayoutConstraint!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
        
        if let indexPaths = self.tvResults.indexPathsForSelectedRows {
            
            for indexPath in indexPaths
            {
                self.tvResults.deselectRow(at: indexPath, animated: true)
            }
        }
        
        self.tvResults.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        //initialize the AutoSearchBar
        searchBar.initialize(shouldSearchAfterDelay: true,
        onSearchText: { [weak self] (text) in
            if (self != nil) {
                self!.contacts.removeAll()
        
                let term = text.trimmingCharacters(in: CharacterSet.whitespaces)
                if text.characters.count > 0
                {
                    self!.searchContact(term)
                }
            }
            
        }, onEndEditing: { [weak self] (searchBar) in
            self?.clearState()
            self?.tvResults.reloadData()
            
        }, onClearText: { [weak self] in
            self?.clearState()
            self?.tvResults.reloadData()
            
        }, onCancel: { [weak self] in
            self?.clearState()
            self?.tvResults.reloadData()
        })
        
        //Localizations
        self.localize()
        
        
        self.tvResults.register(UINib(nibName: "ContactListCell", bundle: nil), forCellReuseIdentifier: "ContactListCell")
        self.tvResults.hideEmtpyCells()
        
        if willBeShowedInNewPost == false
        {
            self.searchBar.becomeFirstResponder()
        }
        
        self.searchBar.showsCancelButton = false
        self.searchBar.tintColor = UIColor.colorForNavigationController()
        
        
        //Just in case: New Post
        if self.willBeShowedInNewPost == true
        {
            self.searchBar.isHidden = self.willBeShowedInNewPost
            self.constraintTableTop.constant = -44
        }
        
        self.clearState()
        
        if self.presentingViewController != nil && self.isModal == true
        {
            let barBtnClose = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.close))
            navigationItem.leftBarButtonItem = barBtnClose
        }
    }
    
    func localize()
    {
        self.title = "SEARCH".localized()
        self.searchBar.placeholder = "Enter a Name".localized()
    }
    //MARK: ACTIONS
    func clearState()
    {
        self.contacts = []
    }
    
    func close()
    {
        self.searchBar.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadContacts(_ contacts:[Contact])
    {
        self.contacts = contacts.sorted(by: { $0.name < $1.name })
        
        if self.contacts.count == 0
        {
            self.tvResults.displayBackgroundMessage("No results found".localized(),
                                                    subMessage: "")
        }
        else
        {
            self.tvResults.dismissBackgroundMessage()
        }
    }
    
    //MARK: WEB SERVICE
    func searchContact(_ term: String)
    {
        self.tvResults.displayBackgroundMessage("Searching...".localized(),
                                                subMessage: "")
        
        
        LibraryAPI.shared.memberBO.getUsers(term, onSuccess: {
            (contacts) in
            
            self.tvResults.dismissBackgroundMessage()
            self.contacts = contacts
            self.tvResults.reloadData()
            
            }, onError: { (error) in
                
                self.tvResults.dismissBackgroundMessage()
                self.tvResults.reloadData()
        })
    }
    
    // MARK: SCROLL
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if self.willBeShowedInNewPost == false
        {
            self.searchBar.resignFirstResponder()
        }
    }
}

// MARK: Table View Datasourse & Delegate
extension PersonRelationSearchTable:  UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // FOR FOLLOWING STAKEHOLDERS
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListCell") as! ContactListCell
        let contact = self.contacts[indexPath.row]
        
        //Selected Color
        let selectedColor = UIView()
        selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
        cell.selectedBackgroundView = selectedColor
        
        if contact is Stakeholder
        {
            cell.cellForStakeholder(contact as! Stakeholder)
        }
        else
        {
            cell.cellForMember(contact as! Member)
        }
        
        return cell
    }
    
    
    // MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return (self.willBeShowedInNewPost == true) ? 94 : 76
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        self.addContactToInstance?(self.contacts[indexPath.row])
        
        
        //Pop or Dissmiss View Controller
        if presentingViewController != nil && navigationController?.viewControllers.count == 1
        {
            dismiss(animated: true, completion: nil)
        }
        else
        {
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        // Remove seperator inset
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset))
        {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))
        {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.responds(to: #selector(setter: UIView.layoutMargins))
        {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
}


