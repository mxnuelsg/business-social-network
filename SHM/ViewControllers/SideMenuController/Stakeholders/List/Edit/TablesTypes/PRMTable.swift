//
//  PRMTable.swift
//  SHM
//
//  Created by Manuel Salinas on 5/17/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

//MARK: ENUMERATIONS
enum ContentType
{
    case none
    case projects
    case relations
    case members
}

private enum SectionType: Int
{
    case add = 0
    case content = 1
}

private enum HeightCell: CGFloat
{
    case `default` = 44.0
    case add = 45.0
//    case Project = 70.0 //Dynamic Size is applied
    case relation = 82.0
    case member = 76.0
}

class PRMTable: UITableViewController
{
    //MARK:PROPERTIES & OUTLETS
    
    //Public properties
    var type = ContentType.none
    var content = [AnyObject]() {
        didSet {
            switch type
            {
            case .projects:
                self.projects = self.content.map({$0 as! Project})
            case .relations:
                self.relations = self.content.map({$0 as! ContactRelation})
            case .members:
                self.members = self.content.map({$0 as! Member})
            default:
                break
            }
            
            self.tableView.reloadData()
        }
    }
    
    //Private properties
    fileprivate var projects = [Project]()
    fileprivate var relations = [ContactRelation]()
    fileprivate var members = [Member]()

    var stakeholderId:Int?

    
    //Only for Relations content
    var relationsTypes: [KeyValueObjectWithKeyValueArray]?

    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        //Table
        self.tableView.hideEmtpyCells()
        self.tableView.backgroundColor = UIColor.white
        
        //Register Custom Cell
        self.tableView.register(UINib(nibName: "RelationTableViewCell", bundle: nil), forCellReuseIdentifier: "RelationTableViewCell")
        self.tableView.register(UINib(nibName: "ContactListCell", bundle: nil), forCellReuseIdentifier: "ContactListCell")
        
        //Cell Dynamic Size
        if self.type == .projects
        {
            self.tableView.estimatedRowHeight = 45
            self.tableView.rowHeight = UITableViewAutomaticDimension
        }
    }
    
    //MARK: EXTRACTION ACTIONS
    func getProjects() -> [Project]
    {
        return self.projects
    }
    
    func getRelations() -> [ContactRelation]
    {
        return self.relations
    }
    
    func getMembers() -> [Member]
    {
        return self.members
    }
    

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == SectionType.add.rawValue
        {
            return 1
        }
        else
        {
            switch self.type
            {
            case .projects:
                //Background Message
                if self.projects.count > 0
                {
                    self.tableView.dismissBackgroundMessage()
                }
                else
                {
                    self.tableView.displayBackgroundMessage("No Projects".localized(),
                                                       subMessage: "")
                }
                
                return self.projects.count
            case .relations:
                //Background Message
                if self.relations.count > 0
                {
                    self.tableView.dismissBackgroundMessage()
                }
                else
                {
                    self.tableView.displayBackgroundMessage("No Relations".localized(),
                                                       subMessage: "")
                }
                
                return self.relations.count
            case .members:
                //Background Message
                if self.members.count > 0
                {
                    self.tableView.dismissBackgroundMessage()
                }
                else
                {
                    self.tableView.displayBackgroundMessage("No Members".localized(),
                                                            subMessage: "")
                }
                
                return self.members.count
            default:
                return 0
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return indexPath.section == SectionType.add.rawValue ? false : true
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        //REMOVE
        let actionDelete = UITableViewRowAction(style: .destructive, title: "Delete".localized() , handler: {
            (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
            
            switch self.type
            {
            case .projects:
                self.projects.remove(at: indexPath.row)
            case .relations:
                self.relations.remove(at: indexPath.row)
            case .members:
                self.members.remove(at: indexPath.row)
            default:
                break
            }
            
            //Close Swipe
            tableView.setEditing(false, animated: true)
            tableView.reloadData()
        })
        
        return [actionDelete]
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section ==  SectionType.add.rawValue
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            
            //Selected Color
            let selectedColor = UIView()
            selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
            cell.selectedBackgroundView = selectedColor
            
            //Config
            cell.imageView?.image = UIImage(named: "Add")
            cell.textLabel?.textColor = UIColor.darkText
            cell.textLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 13)
            cell.textLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
            cell.textLabel?.numberOfLines = 1
            
            switch self.type
            {
            case .projects:
                cell.textLabel?.text = "Add Project".localized()
            case .relations:
                cell.textLabel?.text = "Add Relation".localized()
            case .members:
                cell.textLabel?.text = "Add Member".localized()
            default:
                cell.textLabel?.text = "Add".localized()
            }
            
            return cell
        }
        else
        {
            switch self.type
            {
            case .projects:
                let cell:UITableViewCell = UITableViewCell(style: .default, reuseIdentifier:"Cell")
                cell.selectionStyle = .none
                cell.accessoryView = UIImageView(image: UIImage(named: "iconSwipeDots"))
                
                //config
                cell.imageView?.image = nil
                cell.textLabel?.textColor = UIColor.blackAsfalto()
                cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
                cell.textLabel?.lineBreakMode = .byTruncatingTail
                cell.textLabel?.numberOfLines = 4

                
                let project = self.projects[indexPath.row]
                cell.textLabel?.text = project.title
                
                return cell

            case .relations:
                let cell = tableView.dequeueReusableCell(withIdentifier: "RelationTableViewCell") as! RelationTableViewCell
                cell.selectionStyle = .none
                cell.accessoryView = UIImageView(image: UIImage(named: "iconSwipeDots"))
                
                let relation = self.relations[indexPath.row]
                cell.setupContactRelation(relation)
                
                return cell
                
            case .members:
                let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListCell") as! ContactListCell
                cell.selectionStyle = .none
                cell.accessoryView = UIImageView(image: UIImage(named: "iconSwipeDots"))
                
                let contact = self.members[indexPath.row] as Contact
                cell.cellForContact(contact)
                
                return cell

            default:
                return UITableViewCell()
            }
        }
     }

    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == SectionType.add.rawValue
        {
            switch self.type
            {
            case .projects:
                //Goto Search Project
                let vcSearchProject = Storyboard.getInstanceOf(ProjectSearchViewController.self)
                vcSearchProject.isModalView = false
                vcSearchProject.popOnProjectSelected = true
                vcSearchProject.onProjectSelected = { [weak self] selectedProject in
                    
                    //Already Selected?
                    if let projcts = self?.projects {
                        
                        for project in projcts where project.id == selectedProject.id
                        {
                            OperationQueue.main.addOperation({
                                
                                MessageManager.shared.showBar(title: "Info".localized(),
                                                              subtitle: "The selected project already is on the list".localized(),
                                                              type: .info,
                                                              fromBottom: false)
                            })
                            
                            return
                        }
                    }
                    
                    self?.projects.append(selectedProject)
                    self?.tableView.reloadData()
                }
                
                self.navigationController?.pushViewController(vcSearchProject, animated: true)
                
            case .relations:
                
                let vcSearchRelation: PersonRelationSearchTable = Storyboard.getInstanceFromStoryboard("EditStakeholderPad")
                vcSearchRelation.isModal = false
                vcSearchRelation.addContactToInstance = { [weak self] contact in
                    
                    //Already Selected?
                    if let relations = self?.relations {
                        
                        for person in relations where person.fullName == contact.fullName
                        {
                            OperationQueue.main.addOperation({
                                
                                MessageManager.shared.showBar(title: "Info".localized(),
                                                              subtitle: "You already have a relationship with this person".localized(),
                                                              type: .info,
                                                              fromBottom: false)
                            })
                            
                            return
                        }
                        
                        //Set Relations
                        let vcContactRelation = EditContactRelacionController()
                        vcContactRelation.types = self?.relationsTypes
                        vcContactRelation.contact = contact
                        vcContactRelation.isNewRelation = true
                        vcContactRelation.onSaveRelation = { contactRelationed in
                            
                            let relation = ContactRelation()
                            relation.id = contactRelationed.id
                            relation.username = contactRelationed.username
                            relation.isStakeholder = contactRelationed.isStakeholder
                            relation.name = contactRelationed.name
                            relation.lastName = contactRelationed.lastName
                            relation.companyName = contactRelationed.companyName
                            relation.thumbnailUrl = contactRelationed.thumbnailUrl
                            relation.jobPosition = contactRelationed.jobPosition
                            relation.relationshipLevel1 = contactRelationed.relationType1
                            relation.relationshipLevel2 = contactRelationed.relationType2
                            relation.relationType1 = contactRelationed.relationType1
                            relation.relationType2 = contactRelationed.relationType2
                            
                            self?.relations.append(relation)
                            
                            _ = self?.navigationController?.popViewController(animated: true)
                            tableView.reloadData()
                        }
                        
                        //Open new / edit relation
                        let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                        DispatchQueue.main.asyncAfter(deadline: delayTime){
                            
                            self?.navigationController?.pushViewController(vcContactRelation, animated: true)
                        }
                    }
                }
                
                self.navigationController?.pushViewController(vcSearchRelation, animated: true)
                
            case .members:
                let vcSearchMember = Storyboard.getInstanceOf(ContactSearchViewController.self)
                vcSearchMember.isModalView = false
                //vcSearchMember.stakeholderIdSearchParameter = self.stakeholderId
                vcSearchMember.addContactToInstance = { [weak self] contact in
                    
                    let selectedMember = contact as! Member
                    
                    //Already Selected?
                    if let members = self?.members {
                        for member in members where member.id == selectedMember.id
                        {
                            OperationQueue.main.addOperation({
                                
                                MessageManager.shared.showBar(title: "Info".localized(),
                                                              subtitle: "The selected member already is on the list".localized(),
                                                              type: .info,
                                                              fromBottom: false)
                            })
                            
                            return
                        }
                        
                        self?.members.append(selectedMember)
                        tableView.reloadData()
                    }
                }
                
                self.navigationController?.pushViewController(vcSearchMember, animated: true)
            default:
                break
            }
        }
        else
        {
            //Content
            switch self.type
            {
            case .relations:

                //Set Relations
                let vcContactRelation = EditContactRelacionController()
                vcContactRelation.types = self.relationsTypes
                vcContactRelation.contact = self.relations[indexPath.row]
                vcContactRelation.isNewRelation = true
                vcContactRelation.onSaveRelation = { [weak self] contactRelationed in
                    
                    let relation = ContactRelation()
                    relation.id = contactRelationed.id
                    relation.isStakeholder = contactRelationed.isStakeholder
                    relation.name = contactRelationed.name
                    relation.lastName = contactRelationed.lastName
                    relation.thumbnailUrl = contactRelationed.thumbnailUrl
                    relation.companyName = contactRelationed.companyName
                    relation.jobPosition = contactRelationed.jobPosition
                    relation.relationshipLevel1 = contactRelationed.relationType1
                    relation.relationshipLevel2 = contactRelationed.relationType2
                    
                    self?.relations[indexPath.row] = relation
                    
                    _ = self?.navigationController?.popViewController(animated: true)
                    tableView.reloadData()
                }
                
                    self.navigationController?.pushViewController(vcContactRelation, animated: true)
            default:
                break
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section ==  SectionType.add.rawValue
        {
            return HeightCell.add.rawValue
        }
        else
        {
            switch self.type
            {
            case .projects:
                return tableView.rowHeight
            case .relations:
                return HeightCell.relation.rawValue
            case .members:
                return HeightCell.member.rawValue
            default:
                return HeightCell.default.rawValue
            }
        }
    }
}
