//
//  ProfileStakeholderEditTable.swift
//  SHM
//
//  Created by Manuel Salinas on 5/25/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

private enum StakeholderEditProfile: Int
{
    case information = 0
    case references = 1
    case importantDates = 2
    case professionalTimeline = 3
    case additionalFields = 4
}

private enum HeightCell: CGFloat
{
    case `default` = 44.0
    case information = 234.0
    case importantDates = 60.0
    case additionalFields = 140.0
}

private enum Info: Int
{
    case resume = 0
    case resumePrivate
    case personalInfo
    case personalInfoPrivate
    case professional
    case professionalPrivate
    case generalInfo
    case generalInfoPrivate
}

class ProfileStakeholderEditTable: UITableViewController
{
    //MARK: PROPERTIES / OUTLETS
    var language = Language.english {
        didSet {
            
            self.tableView.reloadData()
        }
    }
    
    var stakeholder = Stakeholder(isDefault: true) {
        didSet {
            
            self.tableView.reloadData()
        }
    }
    
    //Re-ording cells with drag % drop
    var snapshot: UIView!
    var sourceIndexPath: IndexPath!
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        self.tableView.register(UINib(nibName: "RichTextCell", bundle: nil), forCellReuseIdentifier: "RichTextCell")
        self.tableView.register(UINib(nibName: "CellStakeholderDates", bundle: nil), forCellReuseIdentifier: "CellStakeholderDates")
        self.tableView.register(UINib(nibName: "AdittionalFieldCell", bundle: nil), forCellReuseIdentifier: "AdittionalFieldCell")
    }
    
    //MARK: Actions
    func enterToEditMode(_ sender: UILongPressGestureRecognizer)
    {
        let state = sender.state
        let location = sender.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: location)
        
        switch state
        {
        case .began:
            
            if indexPath != nil
            {
                self.sourceIndexPath = indexPath
                let cell = self.tableView.cellForRow(at: indexPath!)
                self.snapshot = self.customSnapshotFromView(cell!)
                
                var center = (cell?.center)!
                snapshot.center = center
                snapshot.alpha = 0.0
                self.tableView.addSubview(snapshot)
                UIView.animate(withDuration: 0.25, animations: {
                    center.y = location.y
                    self.snapshot.center = center
                    self.snapshot.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                    self.snapshot.alpha = 0.98
                    cell?.alpha = 0.0
                    cell?.isHidden = true
                })
            }
            
        case .changed:
            var center = self.snapshot.center
            center.y = location.y
            snapshot.center = center
            
            if let index = indexPath {
                
                let total = self.tableView.numberOfRows(inSection: index.section) - 1
                
                if (index == self.sourceIndexPath) == false && index.section == self.sourceIndexPath.section && total != index.row
                {
                    switch self.sourceIndexPath.section
                    {
                    case StakeholderEditProfile.references.rawValue:
                        
                        if self.language == .english
                        {
                            swap(&self.stakeholder.references[self.sourceIndexPath.row], &self.stakeholder.references[index.row])
                        }
                        else
                        {
                            swap(&self.stakeholder.referencesEsp[self.sourceIndexPath.row], &self.stakeholder.referencesEsp[index.row])
                        }
                        
                    case StakeholderEditProfile.importantDates.rawValue:
                        
                        swap(&self.stakeholder.relevantDates[self.sourceIndexPath.row], &self.stakeholder.relevantDates[index.row])
                        
                    case StakeholderEditProfile.professionalTimeline.rawValue:
                        
                        swap(&self.stakeholder.professionalTimeline[self.sourceIndexPath.row], &self.stakeholder.professionalTimeline[index.row])
                        
                    case StakeholderEditProfile.additionalFields.rawValue:
                        
                        swap(&self.stakeholder.additionalFields[self.sourceIndexPath.row], &self.stakeholder.additionalFields[index.row])
                        
                    default:
                        break
                    }
                    
                    self.tableView.moveRow(at: self.sourceIndexPath, to: index)
                    self.sourceIndexPath = index
                }
            }
            
        default:
            let cell = self.tableView.cellForRow(at: self.sourceIndexPath)
            cell?.isHidden = false
            cell?.alpha = 0.0
            UIView.animate(withDuration: 0.25, animations: {
                self.snapshot.center = (cell?.center)!
                self.snapshot.transform = CGAffineTransform.identity
                self.snapshot.alpha = 0.0
                
                cell?.alpha = 1.0
                
                }, completion: {(finished) in
                    
                    self.sourceIndexPath = nil
                    self.snapshot.removeFromSuperview()
                    self.snapshot = nil
                    
            })
        }
    }
    
    fileprivate func customSnapshotFromView(_ inputView: UIView) -> UIView
    {
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0)
        inputView.layer.render(in: UIGraphicsGetCurrentContext()!)
       
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let snapshot = UIImageView(image: image)
        snapshot.layer.masksToBounds = false
        snapshot.layer.cornerRadius = 0.0
        snapshot.layer.shadowOffset = CGSize(width: -5.0, height: 0.0)
        snapshot.layer.shadowRadius = 5.0
        snapshot.layer.shadowOpacity = 0.4
        
        
        return snapshot
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        switch section
        {
        case StakeholderEditProfile.information.rawValue:
            return  String()
        case StakeholderEditProfile.references.rawValue:
            return "References".localized()
        case StakeholderEditProfile.importantDates.rawValue:
            return "Important Dates".localized()
        case StakeholderEditProfile.professionalTimeline.rawValue:
            return "Professional Timeline".localized()
        case StakeholderEditProfile.additionalFields.rawValue:
            return "Additional Fields".localized()
        default:
            return "Unknown".localized()
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        switch section
        {
        case StakeholderEditProfile.information.rawValue:
            
            return 8
            
        case StakeholderEditProfile.references.rawValue:
            
            return self.language == .english ? self.stakeholder.references.count + 1 : self.stakeholder.referencesEsp.count + 1
            
        case StakeholderEditProfile.importantDates.rawValue:

            return self.stakeholder.relevantDates.count + 1
            
        case StakeholderEditProfile.professionalTimeline.rawValue:

            return self.stakeholder.professionalTimeline.count + 1
            
        case StakeholderEditProfile.additionalFields.rawValue:

            return self.stakeholder.additionalFields.count + 1
            
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        if indexPath.section == StakeholderEditProfile.references.rawValue
            || indexPath.section == StakeholderEditProfile.importantDates.rawValue
            || indexPath.section == StakeholderEditProfile.professionalTimeline.rawValue
            || indexPath.section == StakeholderEditProfile.additionalFields.rawValue
        {
            let totalRow =  tableView.numberOfRows(inSection: indexPath.section) - 1
            
            //Disable Edition in Add Content Cell
            if indexPath.row == totalRow
            {
                return false
            }
            else
            {
                return true
            }
        }
        else
        {
            return false
        }
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        //REMOVE
        let actionDelete = UITableViewRowAction(style: .destructive, title: "Delete".localized() , handler: {
            (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
            
            switch indexPath.section
            {
            case StakeholderEditProfile.references.rawValue:
                
                if self.language == .english
                {
                    self.stakeholder.references.remove(at: indexPath.row)
                    tableView.reloadSections(IndexSet(integer: StakeholderEditProfile.references.rawValue ), with: .none)
                }
                else
                {
                    self.stakeholder.referencesEsp.remove(at: indexPath.row)
                    tableView.reloadSections(IndexSet(integer: StakeholderEditProfile.references.rawValue ), with: .none)
                }
                
            case StakeholderEditProfile.importantDates.rawValue:
                
                self.stakeholder.relevantDates.remove(at: indexPath.row)
                tableView.reloadSections(IndexSet(integer: StakeholderEditProfile.importantDates.rawValue ), with: .none)
                
            case StakeholderEditProfile.professionalTimeline.rawValue:
                
                self.stakeholder.professionalTimeline.remove(at: indexPath.row)
                tableView.reloadSections(IndexSet(integer: StakeholderEditProfile.professionalTimeline.rawValue ), with: .none)
                
            case StakeholderEditProfile.additionalFields.rawValue:
                
                self.stakeholder.additionalFields.remove(at: indexPath.row)
                tableView.reloadSections(IndexSet(integer: StakeholderEditProfile.additionalFields.rawValue ), with: .none)
                
            default:
                break
            }
        })
        
        tableView.setEditing(false, animated: true)
        
        return [actionDelete]
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch indexPath.section
        {
        case StakeholderEditProfile.information.rawValue:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "RichTextCell") as! RichTextCell
            cell.selectionStyle = .none
            
            var title = String()
            
            switch indexPath.row
            {
            case Info.resume.rawValue:
                
                cell.lblTitle.text = "Resume".localized()
                title = self.language == .english ? self.stakeholder.summaryPublic : self.stakeholder.summaryPublicEsp
                
            case Info.resumePrivate.rawValue:
                
                cell.lblTitle.text = "Private Resume".localized()
                title = self.language == .english ? self.stakeholder.summaryPrivate : self.stakeholder.summaryPrivateEsp
                
            case Info.personalInfo.rawValue:
                
                cell.lblTitle.text = "Personal Information".localized()
                title = self.language == .english ? self.stakeholder.personalInformationPublic : self.stakeholder.personalInformationPublicEsp
                
            case Info.personalInfoPrivate.rawValue:
                
                cell.lblTitle.text = "Personal Information Private".localized()
                    title = self.language == .english ? self.stakeholder.personalInformationPrivate : self.stakeholder.personalInformationPrivateEsp
                
            case Info.professional.rawValue:
                
                cell.lblTitle.text = "Professional".localized()
                title = self.language == .english ? self.stakeholder.professionalPublic : self.stakeholder.professionalPublicEsp
                
            case Info.professionalPrivate.rawValue:
                
                cell.lblTitle.text = "Professional Private".localized()
                title = self.language == .english ? self.stakeholder.professionalPrivate : self.stakeholder.professionalPrivateEsp
                
            case Info.generalInfo.rawValue:
                
                cell.lblTitle.text = "General Information".localized()
                title = self.language == .english ? self.stakeholder.generalInformationPublic : self.stakeholder.generalInformationPublicEsp
                
            case Info.generalInfoPrivate.rawValue:
                
                cell.lblTitle.text = "General Information Private".localized()
                    title = self.language == .english ? self.stakeholder.generalInformationPrivate : self.stakeholder.generalInformationPrivateEsp
                
            default:
                cell.lblTitle.text = ""
                title = String()
            }
            
            cell.tvContent.attributedText = title.htmlMutableAttributedString
            cell.onTextViewTap = { [weak self] Void in
                
                let vcPopoverText = PopoverRichText()
                vcPopoverText.specialCharacters = self?.stakeholder.specialCharacters ?? []
                vcPopoverText.content = title
                vcPopoverText.preferredContentSize = CGSize(width: 500, height: 269)
                vcPopoverText.modalPresentationStyle = .popover
                vcPopoverText.popoverPresentationController?.sourceView = cell.tvContent
                vcPopoverText.onSave = { html, references in
                    
                    //Fill data
                    switch indexPath.row
                    {
                    case Info.resume.rawValue:
                        if self?.language == .english
                        {
                            self?.stakeholder.summaryPublic = html
                        }
                        else
                        {
                            self?.stakeholder.summaryPublicEsp = html
                        }
                    case Info.resumePrivate.rawValue:
                        if self?.language == .english
                        {
                            self?.stakeholder.summaryPrivate = html
                        }
                        else
                        {
                            self?.stakeholder.summaryPrivateEsp = html
                        }
                    case Info.personalInfo.rawValue:
                        if self?.language == .english
                        {
                            self?.stakeholder.personalInformationPublic = html
                        }
                        else
                        {
                            self?.stakeholder.personalInformationPublicEsp = html
                        }
                    case Info.personalInfoPrivate.rawValue:
                        if self?.language == .english
                        {
                            self?.stakeholder.personalInformationPrivate = html
                        }
                        else
                        {
                            self?.stakeholder.personalInformationPrivateEsp = html
                        }
                    case Info.professional.rawValue:
                        if self?.language == .english
                        {
                            self?.stakeholder.professionalPublic = html
                        }
                        else
                        {
                            self?.stakeholder.professionalPublicEsp = html
                        }
                    case Info.professionalPrivate.rawValue:
                        if self?.language == .english
                        {
                            self?.stakeholder.professionalPrivate = html
                        }
                        else
                        {
                            self?.stakeholder.professionalPrivateEsp = html
                        }
                    case Info.generalInfo.rawValue:
                        if self?.language == .english
                        {
                            self?.stakeholder.generalInformationPublic = html
                        }
                        else
                        {
                            self?.stakeholder.generalInformationPublicEsp = html
                        }
                    case Info.generalInfoPrivate.rawValue:
                        if self?.language == .english
                        {
                            self?.stakeholder.generalInformationPrivate = html
                        }
                        else
                        {
                            self?.stakeholder.generalInformationPrivateEsp = html
                        }
                    default:
                        break
                    }
                    
                    //Set format Cell
                    cell.tvContent.attributedText = html.htmlMutableAttributedString
                    tableView.reloadSections(IndexSet(integer: StakeholderEditProfile.information.rawValue), with: .none)
                    
                    //Save references
                    for ref in references
                    {
                        self?.stakeholder.references.append(ref)
                    }
                    
                    //Update reference
                    if references.count > 0
                    {
                         tableView.reloadSections(IndexSet(integer: StakeholderEditProfile.references.rawValue), with: .none)
                    }
                }
                
                self?.present(vcPopoverText, animated: true, completion: nil)
            }
            
            return cell
            
        case StakeholderEditProfile.references.rawValue:
            
            let totalRow =  tableView.numberOfRows(inSection: indexPath.section) - 1
            
            if indexPath.row == totalRow
            {
                //Add
                let cell = UITableViewCell(style: .default, reuseIdentifier:"Cell")
                
                //Selected Color
                let selectedColor = UIView()
                selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
                cell.selectedBackgroundView = selectedColor
                
                //Config
                cell.imageView?.image = UIImage(named: "Add")
                cell.textLabel?.textColor = UIColor.darkText
                cell.textLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 13)
                cell.textLabel?.lineBreakMode = .byTruncatingTail
                cell.textLabel?.numberOfLines = 1
                
                cell.textLabel?.text = "Add Reference".localized()
                
                //Action
                cell.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: { [weak self] () -> () in
                    
                    let vcAddReference = EditReferenceController()
                    if let lang = self?.language { vcAddReference.language = lang }
                    vcAddReference.preferredContentSize = CGSize(width: 650, height: 60)
                    vcAddReference.modalPresentationStyle = .popover
                    vcAddReference.popoverPresentationController?.sourceView = cell.textLabel
                    vcAddReference.onReferenceSaved = { reference, isUrl in
                        
                        if self?.language == .english
                        {
                            self?.stakeholder.references.append(reference)
                        }
                        else
                        {
                            self?.stakeholder.referencesEsp.append(reference)
                        }
                        
                        tableView.reloadSections(IndexSet(integer: StakeholderEditProfile.references.rawValue), with: .none)
                    }
                    
                    self?.present(vcAddReference, animated: true, completion: nil)
                    }))
                
                return cell
            }
            else
            {
                //References
                let cell:UITableViewCell = UITableViewCell(style: .value2, reuseIdentifier:"Cell")
                cell.selectionStyle = .none
                cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
                cell.textLabel?.textColor = UIColor.colorForNavigationController()
                cell.textLabel?.textAlignment = .left
                cell.detailTextLabel?.textAlignment = .left
                cell.accessoryView = UIImageView(image: UIImage(named: "iconSwipeDots"))
                
                let reference = self.language == .english ? self.stakeholder.references[indexPath.row] : self.stakeholder.referencesEsp[indexPath.row]
                let order = reference.ordinalIndicator
                let value = reference.value
                
                //Check if is url
                if let url = URL(string: value) {
                    
                    if url.scheme?.lowercased() == "http" || url.scheme?.lowercased() == "https"
                    {
                        cell.detailTextLabel?.textColor = UIColor.blueTag()
                    }
                }
                
                cell.textLabel?.text = "\(order)"
                cell.detailTextLabel?.text = value
                
                //Action
                cell.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: { [weak self] () -> () in
                    
                    let vcAddReference = EditReferenceController()
                    if let lang = self?.language { vcAddReference.language = lang }
                    vcAddReference.reference = reference
                    vcAddReference.preferredContentSize = CGSize(width: 650, height: 60)
                    vcAddReference.modalPresentationStyle = .popover
                    vcAddReference.popoverPresentationController?.sourceView = cell.textLabel
                    vcAddReference.onReferenceSaved = { reference, isUrl in
                        
                        if self?.language == .english
                        {
                            self?.stakeholder.references[indexPath.row] = reference
                        }
                        else
                        {
                            self?.stakeholder.referencesEsp[indexPath.row] = reference
                        }
                        
                        tableView.reloadSections(IndexSet(integer: StakeholderEditProfile.references.rawValue), with: .none)
                    }
                    
                    self?.present(vcAddReference, animated: true, completion: nil)
                    
                    }))
                
                //Cell Mobile Action: Re-ordering in tableview
                let longGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.enterToEditMode(_:)))
                cell.addGestureRecognizer(longGestureRecognizer)
                
                return cell
            }
            
        case StakeholderEditProfile.importantDates.rawValue:
            
            let totalRow =  tableView.numberOfRows(inSection: indexPath.section) - 1
            
            if indexPath.row == totalRow
            {
                //Add
                let cell = UITableViewCell(style: .default, reuseIdentifier:"Cell")
                
                //Selected Color
                let selectedColor = UIView()
                selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
                cell.selectedBackgroundView = selectedColor
                
                //Config
                cell.imageView?.image = UIImage(named: "Add")
                cell.textLabel?.textColor = UIColor.darkText
                cell.textLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 13)
                cell.textLabel?.lineBreakMode = .byTruncatingTail
                cell.textLabel?.numberOfLines = 1
                
                cell.textLabel?.text = "Add Important Date".localized()
                
                //Action
                cell.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {  () -> () in
                    
                    let vcAddRelevantDate = EditRelevantDateController()
                    vcAddRelevantDate.language = self.language
                    vcAddRelevantDate.preferredContentSize = CGSize(width: 650, height: 60)
                    vcAddRelevantDate.modalPresentationStyle = .popover
                    vcAddRelevantDate.popoverPresentationController?.sourceView = cell.textLabel
                    vcAddRelevantDate.onRelevantDateSaved = { [weak self] relevantDate in
                        
                        self?.stakeholder.relevantDates.append(relevantDate)
                        tableView.reloadSections(IndexSet(integer: StakeholderEditProfile.importantDates.rawValue), with: .none)
                    }
                    
                    self.present(vcAddRelevantDate, animated: true, completion: nil)
                }))
                
                return cell
            }
            else
            {
                //Relevant Dates
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "CellStakeholderDates") as! CellStakeholderDates
                cell.selectionStyle = .none
                cell.accessoryView = UIImageView(image: UIImage(named: "iconSwipeDots"))
                
                
                let importantDate = self.stakeholder.relevantDates[indexPath.row]
                cell.loadInfo(importantDate)
                
                //Action
                cell.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: { [weak self] () -> () in
                    
                    let vcAddRelevantDate = EditRelevantDateController()
                    vcAddRelevantDate.relevantDate = importantDate
                    vcAddRelevantDate.preferredContentSize = CGSize(width: 650, height: 60)
                    vcAddRelevantDate.modalPresentationStyle = .popover
                    vcAddRelevantDate.popoverPresentationController?.sourceView = cell.lblDescription
                    vcAddRelevantDate.onRelevantDateSaved = { relevantDate in
                        
                        self?.stakeholder.relevantDates[indexPath.row] = relevantDate
                        tableView.reloadSections(IndexSet(integer: StakeholderEditProfile.importantDates.rawValue), with: .none)
                    }
                    
                    self?.present(vcAddRelevantDate, animated: true, completion: nil)
                    
                    }))
                
                //Cell Mobile Action: Re-ordering in tableview
                let longGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.enterToEditMode(_:)))
                cell.addGestureRecognizer(longGestureRecognizer)
                    
                return cell
            }

        case StakeholderEditProfile.professionalTimeline.rawValue:
            
            let totalRow =  tableView.numberOfRows(inSection: indexPath.section) - 1
            
            if indexPath.row == totalRow
            {
                //Add
                let cell = UITableViewCell(style: .default, reuseIdentifier:"Cell")
                
                //Selected Color
                let selectedColor = UIView()
                selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
                cell.selectedBackgroundView = selectedColor
                
                //Config
                cell.imageView?.image = UIImage(named: "Add")
                cell.textLabel?.textColor = UIColor.darkText
                cell.textLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 13)
                cell.textLabel?.lineBreakMode = .byTruncatingTail
                cell.textLabel?.numberOfLines = 1
                
                cell.textLabel?.text = "Add Professional Timeline".localized()
                
                //Action
                cell.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {  () -> () in
                    
                    let vcAddProfessionalTimeline = EditRelevantDateController()
                    vcAddProfessionalTimeline.language = self.language
                    vcAddProfessionalTimeline.preferredContentSize = CGSize(width: 650, height: 60)
                    vcAddProfessionalTimeline.modalPresentationStyle = .popover
                    vcAddProfessionalTimeline.popoverPresentationController?.sourceView = cell.textLabel
                    vcAddProfessionalTimeline.onRelevantDateSaved = { [weak self] relevantDate in
                        
                        self?.stakeholder.professionalTimeline.append(relevantDate)
                        tableView.reloadSections(IndexSet(integer: StakeholderEditProfile.professionalTimeline.rawValue), with: .none)
                    }
                    
                    self.present(vcAddProfessionalTimeline, animated: true, completion: nil)
                }))
                
                return cell
            }
            else
            {
                //Relevant Dates
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "CellStakeholderDates") as! CellStakeholderDates
                cell.selectionStyle = .none
                cell.accessoryView = UIImageView(image: UIImage(named: "iconSwipeDots"))
                
                
                let professionalTimeline = self.stakeholder.professionalTimeline[indexPath.row]
                cell.loadInfo(professionalTimeline)
                
                //Action
                cell.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: { [weak self] () -> () in
                    
                    let vcAddProfessionalTimeline = EditRelevantDateController()
                    vcAddProfessionalTimeline.relevantDate = professionalTimeline
                    vcAddProfessionalTimeline.preferredContentSize = CGSize(width: 650, height: 60)
                    vcAddProfessionalTimeline.modalPresentationStyle = .popover
                    vcAddProfessionalTimeline.popoverPresentationController?.sourceView = cell.lblDescription
                    vcAddProfessionalTimeline.onRelevantDateSaved = { relevantDate in
                        
                        self?.stakeholder.professionalTimeline[indexPath.row] = relevantDate
                        tableView.reloadSections(IndexSet(integer: StakeholderEditProfile.professionalTimeline.rawValue), with: .none)
                    }
                    
                    self?.present(vcAddProfessionalTimeline, animated: true, completion: nil)
                    
                    }))
                
                //Cell Mobile Action: Re-ordering in tableview
                let longGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.enterToEditMode(_:)))
                cell.addGestureRecognizer(longGestureRecognizer)
                
                return cell
            }
            
        case StakeholderEditProfile.additionalFields.rawValue:
            
            let totalRow =  tableView.numberOfRows(inSection: indexPath.section) - 1
            
            if indexPath.row == totalRow
            {
                //Add
                let cell = UITableViewCell(style: .default, reuseIdentifier:"Cell")
                
                //Selected Color
                let selectedColor = UIView()
                selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
                cell.selectedBackgroundView = selectedColor
                
                //Config
                cell.imageView?.image = UIImage(named: "Add")
                cell.textLabel?.textColor = UIColor.darkText
                cell.textLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 13)
                cell.textLabel?.lineBreakMode = .byTruncatingTail
                cell.textLabel?.numberOfLines = 1
                
                cell.textLabel?.text = "Add Additional Field".localized()
                
                //Action
                cell.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: { () -> () in
                    
                    let vcAddField = EditAdittionalFieldController()
                    vcAddField.language = self.language
                    vcAddField.preferredContentSize = CGSize(width: 650, height: 200)
                    vcAddField.modalPresentationStyle = .popover
                    vcAddField.popoverPresentationController?.sourceView = cell.textLabel
                    vcAddField.onAdittionalFieldSaved = { [weak self] field in
                        
                        self?.stakeholder.additionalFields.append(field)
                        tableView.reloadSections(IndexSet(integer: StakeholderEditProfile.additionalFields.rawValue), with: .none)
                    }
                    
                    self.present(vcAddField, animated: true, completion: nil)
                }))
                
                return cell
            }
            else
            {
                //Adittional Field
                let cell = tableView.dequeueReusableCell(withIdentifier: "AdittionalFieldCell") as! AdittionalFieldCell
                let field = self.stakeholder.additionalFields[indexPath.row]
                
                cell.accessoryView = UIImageView(image: UIImage(named: "iconSwipeDots"))
                cell.lblDescription.text = field.key
                cell.tvContent.text = field.value
                cell.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {  () -> () in
                    
                    let vcAddField = EditAdittionalFieldController()
                    vcAddField.additionalField = field
                    vcAddField.preferredContentSize = CGSize(width: 650, height: 200)
                    vcAddField.modalPresentationStyle = .popover
                    vcAddField.popoverPresentationController?.sourceView = cell.tvContent
                    vcAddField.onAdittionalFieldSaved = { [weak self] field in
                        
                        self?.stakeholder.additionalFields[indexPath.row] = field
                        tableView.reloadSections(IndexSet(integer: StakeholderEditProfile.additionalFields.rawValue), with: .none)
                    }
                    
                    self.present(vcAddField, animated: true, completion: nil)
                }))
                
                return cell
            }
            
        default:
            return UITableViewCell()
        }
    }
    
    //MARK: Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        // Remove seperator inset
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset))
        {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))
        {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.responds(to: #selector(setter: UIView.layoutMargins))
        {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch indexPath.section
        {
        case StakeholderEditProfile.information.rawValue:
            return HeightCell.information.rawValue
        case StakeholderEditProfile.references.rawValue:
            return HeightCell.default.rawValue
        case StakeholderEditProfile.importantDates.rawValue, StakeholderEditProfile.professionalTimeline.rawValue:
            return HeightCell.importantDates.rawValue
        case StakeholderEditProfile.additionalFields.rawValue:
            let totalRow = self.stakeholder.additionalFields.count + 1
            
            //Disable Edition in Add Content Cell
            if indexPath.row + 1 == totalRow
            {
                return HeightCell.default.rawValue
            }
            else
            {
                return HeightCell.additionalFields.rawValue
            }
        default:
            return HeightCell.default.rawValue
        }
    }
    
    //MARK: UITableView Delegate methods involved in moving cells
    // This methods grants permissions for cells to move
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool
    {
        //Mobile cells will exist only in: references, important dates and profesional time line
        switch indexPath.section
        {
        case StakeholderEditProfile.references.rawValue:
            
            return true
        case StakeholderEditProfile.importantDates.rawValue:
            
            return true
        case StakeholderEditProfile.professionalTimeline.rawValue:
            
            return true
        default:
            
            break
        }
        
        return false
    }
    
    
//    //This methods its called during cell's moving operation.
    override func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath
    {
        //Here we limit the reorganization of the cells only within its section. Cell with + Add button action remains steady
        if proposedDestinationIndexPath.section != sourceIndexPath.section
        {
            return sourceIndexPath
        }
        else if proposedDestinationIndexPath.section == StakeholderEditProfile.references.rawValue
        {
            if self.language == .english
            {
                return proposedDestinationIndexPath.row == self.stakeholder.references.count ? sourceIndexPath : proposedDestinationIndexPath
            }
            else
            {
                return proposedDestinationIndexPath.row == self.stakeholder.referencesEsp.count ? sourceIndexPath : proposedDestinationIndexPath
            }
        }
        else if proposedDestinationIndexPath.section ==  StakeholderEditProfile.importantDates.rawValue
        {
            return proposedDestinationIndexPath.row == self.stakeholder.relevantDates.count ? sourceIndexPath : proposedDestinationIndexPath
        }
        else if proposedDestinationIndexPath.section == StakeholderEditProfile.professionalTimeline.rawValue
        {
            return proposedDestinationIndexPath.row == self.stakeholder.professionalTimeline.count ? sourceIndexPath : proposedDestinationIndexPath
        }
        else
        {
            return proposedDestinationIndexPath
        }
    }
 
    //Called when cell has been moved
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to toIndexPath: IndexPath)
    {
        //Table moves back to normal mode
        self.setEditing(false, animated: true)
        
        //Arrays needs to be re-ordered, matching the new cell distribution in corresponding section
        switch fromIndexPath.section
        {
        case StakeholderEditProfile.references.rawValue:
            
            if self.language == .english
            {
                self.stakeholder.references.remove(at: fromIndexPath.row)
                self.stakeholder.references.insert(self.stakeholder.references[fromIndexPath.row], at: toIndexPath.row)
            }
            else
            {
                self.stakeholder.referencesEsp.remove(at: fromIndexPath.row)
                self.stakeholder.referencesEsp.insert(self.stakeholder.referencesEsp[fromIndexPath.row], at: toIndexPath.row)
            }
            
        case StakeholderEditProfile.importantDates.rawValue:
            
            self.stakeholder.relevantDates.remove(at: fromIndexPath.row)
            self.stakeholder.relevantDates.insert(self.stakeholder.relevantDates[fromIndexPath.row], at: toIndexPath.row)
            
        case StakeholderEditProfile.professionalTimeline.rawValue:
            
            self.stakeholder.professionalTimeline.remove(at: fromIndexPath.row)
            self.stakeholder.professionalTimeline.insert(self.stakeholder.professionalTimeline[fromIndexPath.row], at: toIndexPath.row)
            
        default:
            break
        }
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle
    {
        //This removes the deletion control when table is in editing mode
        if tableView.isEditing == true
        {
            return UITableViewCellEditingStyle.none
        }
        else
        {
            return UITableViewCellEditingStyle.delete
        }
        
    }

    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool
    {
        //Avoids indentation once table enters in editing mode
        return false
    }

}
