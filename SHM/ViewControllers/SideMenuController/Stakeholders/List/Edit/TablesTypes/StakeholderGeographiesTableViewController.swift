//
//  StakeholderGeographiesTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 02/08/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class StakeholderGeographiesTableViewController: UITableViewController {

    fileprivate var stakeholder: Stakeholder?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "GeographyTableViewCell", bundle: nil), forCellReuseIdentifier: "GeographyTableViewCell")
    }
    
    func initialize(stakeholder: Stakeholder) {
        self.stakeholder = stakeholder
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (stakeholder?.geographies.count ?? 0) + 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            cell.textLabel?.text = "Add Geography".localized()
            cell.textLabel?.textColor = UIColor.darkText
            cell.textLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 13)
            cell.imageView?.image = UIImage(named: "Add")
            cell.backgroundColor = UIColor.white
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GeographyTableViewCell") as! GeographyTableViewCell
            let geography = stakeholder?.geographies[indexPath.row - 1]
            
            cell.lblTitle.text = geography!.title
            cell.lblSubtitle.text = geography!.parentTitle
            
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            //Add Geography
            let geographyPicker: GeoListViewController = Storyboard.getInstanceFromStoryboard("Geography")
            geographyPicker.title = "Geographies".localized()
            geographyPicker.onTableAssigned = { (table) in
                table.refreshTable()
                table.ownerModule = .projectAndStakeholderEdition                
                table.selectedGeographies = self.stakeholder?.geographies ?? [ClusterItem]()
                table.onDidSelectGeography = { [weak self] (geography) in
                    self?.stakeholder?.geographies.append(geography)
                    self?.tableView.reloadData()
                    self?.navigationController?.popViewController(animated: true)
//                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {
//                        self?.tableView.scrollToBottom()
//                    })
                }
            }
            navigationController?.pushViewController(geographyPicker, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        if indexPath.row != 0
        {
            let delete = UITableViewRowAction(style: .destructive, title: "Remove".localized(), handler: {(action, indexPath) -> () in
                
                self.stakeholder?.geographies.remove(at: indexPath.row - 1)
                tableView.deleteRows(at: [indexPath], with: .fade)
            })
            
            return [delete]
        }
        
        return nil
    }
}
