//
//  PublishRequestScreen.swift
//  SHM
//
//  Created by Manuel Salinas on 6/2/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class PublishRequestScreen: UIViewController
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak fileprivate var btnSendLink: UIButton!
    @IBOutlet weak fileprivate var btnSendAsFile: UIButton!
    @IBOutlet weak fileprivate var btnCancel: UIButton!
    @IBOutlet weak fileprivate var btnPublish: UIButton!
    @IBOutlet weak fileprivate var lblRequestedBy: UILabel!
    @IBOutlet weak fileprivate var lblComments: UILabel!
    @IBOutlet weak fileprivate var tvComments: UITextView!
    @IBOutlet weak fileprivate var viewCollection: UIView!
    @IBOutlet weak fileprivate var collectionRequesters: UICollectionView!
    
    var onPublishSuccess:(() -> ())?
    var stakeholderEdited = Stakeholder(isDefault: true)
    
    fileprivate var request = Request()
    fileprivate var isSendLink_activated = false
    fileprivate var isSendCard_activated = false
    fileprivate var arrayRequesters = [Member]()
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.getRequest()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        //Style
        self.tvComments.setBorder()
        self.viewCollection.setBorder()
        
        //Localizations
        self.localize()
        
        //Optionals
        self.btnSendLink.addTarget(self, action: #selector(self.selectChecks(_:)), for: .touchUpInside)
        self.btnSendAsFile.addTarget(self, action: #selector(self.selectChecks(_:)), for: .touchUpInside)
        self.btnCancel.addTarget(self, action: #selector(self.cancel), for: .touchUpInside)
        self.btnPublish.addTarget(self, action: #selector(self.publish), for: .touchUpInside)
        self.btnPublish.cornerRadius()
        
        //Collection View (Attachments)
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection =  .horizontal
        layout.minimumInteritemSpacing = 4
        layout.minimumLineSpacing = 4
        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        layout.itemSize = CGSize(width: 200, height: 50)
        
        self.collectionRequesters.collectionViewLayout = layout
        self.collectionRequesters.dataSource = self
        self.collectionRequesters.delegate = self
        self.collectionRequesters.allowsMultipleSelection = false
        self.collectionRequesters.register(UINib(nibName: "RequestersCollectionCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
    }
    
    fileprivate func localize()
    {
        //Title
        self.title = "Publish Request".localized()
        self.btnSendLink.setTitle(" Send link of updated card".localized(), for: UIControlState())
        self.btnSendAsFile.setTitle(" Send as attached file".localized(), for: UIControlState())
        //Labels
        self.lblRequestedBy.text = "Requested By:".localized()
        self.lblComments.text =  "Comments".localized()
        //Buttons
        self.btnCancel.setTitle("Cancel".localized(), for: UIControlState())
        self.btnPublish.setTitle("Publish".localized(), for: UIControlState())
    }
    
    //MARK: WEB SERVICE
    fileprivate func getRequest()
    {
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        LibraryAPI.shared.requestBO.getRequestToPublish(requestId: self.stakeholderEdited.requestId, onSuccess: {
            (request) in
            
            //Remove indicator
            MessageManager.shared.hideHUD()
            
            self.request = request
            self.arrayRequesters = self.request.requesters
            self.collectionRequesters.reloadData()
            
            }) { (error) in
                
                //Remove indicator
                MessageManager.shared.hideHUD()
                
                MessageManager.shared.showBar(title: "Error",
                                              subtitle: "Error.TryAgain".localized(),
                                              type: .error,
                                              fromBottom: false)
                
                self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK: ACTIONS
    func cancel()
    {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func publish()
    {
        self.view.endEditing(true)
        
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        self.request.sendCardAsAttachedFile = self.isSendCard_activated
        self.request.sendLinkUpdatedCard = self.isSendLink_activated
        self.request.requesters = self.arrayRequesters
        self.request.stakeholderDetail = self.stakeholderEdited
        
        LibraryAPI.shared.requestBO.postRequestToPublish(request: self.request, onSuccess: {
            
            //Remove indicator
            MessageManager.shared.hideHUD()
            
            self.onPublishSuccess?()
            self.dismiss(animated: true, completion: nil)
            
            }) { (error) in
                
                //Remove indicator
                MessageManager.shared.hideHUD()
                
                MessageManager.shared.showBar(title: "Error",
                                              subtitle: "Error.TryAgain".localized(),
                                              type: .error,
                                              fromBottom: false)

        }
        
        
    }
    
    func selectChecks(_ buttonSelected: UIButton)
    {
        self.view.endEditing(true)
        
        if buttonSelected.tag == 1
        {
            if self.isSendLink_activated == false
            {
                self.btnSendLink.setImage(UIImage(named: "iconRoundSelected"), for: UIControlState())
                self.isSendLink_activated = true
            }
            else
            {
                self.btnSendLink.setImage(UIImage(named: "iconRoundUnselected"), for: UIControlState())
                self.isSendLink_activated = false
            }
        }
        else
        {
            if self.isSendCard_activated == false
            {
                self.btnSendAsFile.setImage(UIImage(named: "iconRoundSelected"), for: UIControlState())
                self.isSendCard_activated = true
            }
            else
            {
                self.btnSendAsFile.setImage(UIImage(named: "iconRoundUnselected"), for: UIControlState())
                self.isSendCard_activated = false
            }
        }
    }
    
    fileprivate func openSearch()
    {
        self.view.endEditing(true)
        
        let vcSearchRequesters = SearchRequesterController()
        vcSearchRequesters.requesters = self.request.members
        vcSearchRequesters.onSelected = { [weak self] requester in
            
            if let requesters = self?.arrayRequesters {
                
                for req in requesters where req.id == requester.id
                {
                    OperationQueue.main.addOperation({
                        
                        MessageManager.shared.showBar(title: "Info".localized(),
                                                      subtitle: "The selected member already is on the list".localized(),
                                                      type: .info,
                                                      fromBottom: false)
                    })
                    
                    return
                }
            }
            
            self?.arrayRequesters.append(requester)
            self?.collectionRequesters.reloadData()
        }
        
        self.navigationController?.pushViewController(vcSearchRequesters, animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        self.view.endEditing(true)
    }
}

//MARK: Textview Delegate
extension PublishRequestScreen: UITextViewDelegate
{
    func textViewDidEndEditing(_ textView: UITextView)
    {
        self.request.publishComments = textView.text
    }
}

// MARK: CollectionView Methods
extension PublishRequestScreen: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.arrayRequesters.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if self.arrayRequesters.count == 0
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! RequestersCollectionCell
            cell.backgroundColor = UIColor.clear
            cell.btnRemove.setTitle("＋", for: UIControlState())
            cell.btnRemove.backgroundColor = UIColor.blueBelize()
            cell.btnRemove.isUserInteractionEnabled = false
            
            cell.lblUsername.text = "Add Requester".localized()
            
            return cell
        }
        else
        {
            //Verify If it's the last row
            let lastRowIndex = collectionView.numberOfItems(inSection: collectionView.numberOfSections - 1)
            
            if indexPath.row == lastRowIndex - 1
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! RequestersCollectionCell
                cell.backgroundColor = UIColor.clear
                cell.btnRemove.setTitle("＋", for: UIControlState())
                cell.btnRemove.backgroundColor = UIColor.blueBelize()
                cell.btnRemove.isUserInteractionEnabled = false
                
                cell.lblUsername.text = "Add Requester".localized()
                
                return cell
            }
            else
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! RequestersCollectionCell
                cell.backgroundColor = UIColor.clear
                cell.btnRemove.setTitle("×", for: UIControlState())
                cell.btnRemove.backgroundColor = UIColor.blackAsfalto()
                cell.btnRemove.isUserInteractionEnabled = true
                cell.btnRemove.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
                    
                    self.arrayRequesters = self.arrayRequesters.filter({ $0 !== self.arrayRequesters[indexPath.row] })
                    self.collectionRequesters.reloadData()
                    
                }))
                
                cell.lblUsername.text = self.arrayRequesters[indexPath.row].accountName

                
                return cell
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        //Verify If it's the last row
        let lastRowIndex = collectionView.numberOfItems(inSection: collectionView.numberOfSections - 1)
        
        if indexPath.row == lastRowIndex - 1
        {
            self.openSearch()
        }
    }
}



