//
//  SearchRequesterController.swift
//  SHM
//
//  Created by Manuel Salinas on 6/2/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class SearchRequesterController: UIViewController
{
    // MARK: - OUTLETS AND PROPERTIES
    @IBOutlet weak var searchBar: AutoSearchBar!
    @IBOutlet weak var tableContent: UITableView!
    
    var onSelected:((_ requester: Member) -> ())?
    
    var requesters = [Member]()
    fileprivate var requestersBackup = [Member]() {
        didSet{
            self.requestersBackup = self.requestersBackup.sorted(by: { $0.accountName.localizedCompare($1.accountName) == .orderedAscending})
        }
    }
    
    //MARK: LIFE CYCLE
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - LOAD CONFIGS
    fileprivate func loadConfig()
    {
        //initialize AutoSearchBar
        searchBar.initialize(onSearchText: { [weak self] (text) in
            if (self != nil) {
                let arrayFiltered = self!.requesters.filter({ ($0.accountName as NSString).localizedCaseInsensitiveContains(text) })
                self!.requestersBackup = arrayFiltered
                self!.tableContent.reloadData()
            }
            
        }, onEndEditing: { [weak self] (searchBar) in
            if (self != nil) {
                self!.requestersBackup = self!.requesters
                self!.tableContent.reloadData()
            }
            
        }, onClearText: { [weak self] in
            if (self != nil) {
                self!.requestersBackup = self!.requesters
                self!.tableContent.reloadData()
            }
                
        }, onCancel: { [weak self] in
            if (self != nil) {
                self!.searchBar.text = String()
                self!.requestersBackup = self!.requesters
                self!.tableContent.reloadData()
            }
        })
        
        self.localize()
        
        self.tableContent.hideEmtpyCells()
        
        //Content
        self.loadRequesters(self.requesters)
        
    }
    
    fileprivate func localize()
    {
        //Title
        self.title = "Requesters".localized()
        
        //Searchbar
        self.searchBar.placeholder =  "Search".localized()
    }
    
    //MARK: ACTIONS
    fileprivate func loadRequesters(_ requesters:[Member])
    {
        self.requestersBackup = requesters
        self.tableContent.reloadData()
    }
}

//MARK: TABLE VIEW DELEGATE & DATASOURCE
extension SearchRequesterController: UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.requestersBackup.count == 0
        {
            self.tableContent.displayBackgroundMessage("No results found".localized(),
                                                       subMessage: "")
        }
        else
        {
            self.tableContent.dismissBackgroundMessage()
        }

        return self.requestersBackup.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style:.default, reuseIdentifier:"Cell")
        
        //Selected Color
        let selectedColor = UIView()
        selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
        cell.selectedBackgroundView = selectedColor
        
        //Config
        cell.textLabel?.numberOfLines = 2
        cell.textLabel?.lineBreakMode = .byCharWrapping
        cell.textLabel?.textColor = UIColor.blackAsfalto()
        
        cell.textLabel?.text = self.requestersBackup[indexPath.row].accountName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let requester = self.requestersBackup[indexPath.row]
        self.onSelected?(requester)
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {
        self.searchBar.resignFirstResponder()
    }
}

