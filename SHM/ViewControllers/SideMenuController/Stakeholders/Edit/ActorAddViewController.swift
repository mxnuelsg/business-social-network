//
//  ActorAddViewController.swift
//  SHM
//
//  Created by Diego Navarro on 8/6/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ActorAddViewController: UIViewController
{
    // MARK: - LIFE CICLE
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        /**
        Navigation Bar
        */
        
        let navbarFont = UIFont(name: "HelveticaNeue-Bold", size: 15) ?? UIFont.systemFontOfSize(15)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: navbarFont, NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - BAR BUTTON ACTIONS

    @IBAction func barBtnCancelPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func barBtnSavePressed(sender: AnyObject) {
    }
}
