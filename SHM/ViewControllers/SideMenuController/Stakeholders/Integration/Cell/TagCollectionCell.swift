//
//  TagCollectionCell.swift
//  SHM
//
//  Created by Manuel Salinas on 10/5/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class TagCollectionCell: UICollectionViewCell
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak fileprivate var btnDelete: UIButton!
    
    var onDeleteTap:(() -> ())?

    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.loadConfig()
    }
    
    fileprivate func loadConfig()
    {
        self.layer.cornerRadius = 11
        self.clipsToBounds = true
    }

    @IBAction func removeTag()
    {
        self.onDeleteTap?()
    }
}
