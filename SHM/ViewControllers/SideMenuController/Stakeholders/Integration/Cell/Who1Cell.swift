//
//  Who1Cell.swift
//  SHM
//
//  Created by Manuel Salinas on 10/5/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class Who1Cell: UITableViewCell
{
    //MARK: VARIABLES & OUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSeeAll: UIButton!
    @IBOutlet weak fileprivate var viewTags: UIView!
    @IBOutlet weak fileprivate var collectionTags: UICollectionView!
    
    var whoTags = [WhoLevels]() {
        didSet{
            self.collectionTags.reloadData()
        }
    }
    
    var onSelectedCell:(() -> ())?
    var onDelete:((_ who: WhoLevels) -> ())?

    //MARK: LIFE CYCLE
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.loadConfig()
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        //UI
        self.viewTags.cornerRadius()
        self.viewTags.layer.borderColor = UIColor.grayCloudy().cgColor
        self.viewTags.layer.borderWidth = 0.5
        
        //Collection View (Declaration)
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection =  .horizontal
        layout.minimumInteritemSpacing = 2
        layout.minimumLineSpacing = 2
        layout.sectionInset = UIEdgeInsets(top: 4, left: 1, bottom: 4, right: 1)
        layout.itemSize = CGSize(width: 140, height: 29)
        
        self.collectionTags.collectionViewLayout = layout
        self.collectionTags.dataSource = self
        self.collectionTags.delegate = self
        self.collectionTags.allowsMultipleSelection = false
        self.collectionTags.register(UINib(nibName: "TagCollectionCell", bundle: nil), forCellWithReuseIdentifier: "TagCollectionCell")
        self.collectionTags.backgroundColor = UIColor.clear
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: ACITONS
    @IBAction func seeAll()
    {
    }
}

extension Who1Cell: UICollectionViewDelegate, UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if self.whoTags.count > 0
        {
            self.collectionTags.removeGestureRecognizersOfType(ActionsTapGestureRecognizer.self)
        }
        else
        {
            self.collectionTags.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: { [weak self] () -> () in
                
                self?.onSelectedCell?()
                }))
        }
        
        return self.whoTags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCollectionCell", for: indexPath) as! TagCollectionCell
        let tag =  self.whoTags[indexPath.row]
        
        cell.onDeleteTap = { Void in
        
            self.whoTags.remove(at: indexPath.row)
            self.onDelete?(tag)
        }

        cell.lblTitle.text = tag.name
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        print("Tap in cell \(indexPath.row) on section \(indexPath.section)")
    }
    
}
