//
//  Whos2TableViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 10/6/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class Whos2TableViewController: UITableViewController
{
    //MARK: VARIABLES & OUTLETS
    var whos2 = [WhoLevels]()
    var whos2_selected = [WhoLevels]()
    var isCentral = false
    var onDeinit:((_ whos2Selected: [WhoLevels]) -> ())?
    var onDeselect:((_ who2Deselected: WhoLevels) -> ())?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        if self.isCentral == true
        {
           self.onDeinit?(self.whos2_selected as! [WhoLevel_II])
        }
        else
        {
            self.onDeinit?(self.whos2_selected as! [WhoLevel_III])
        }
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        self.title = "WHOS II"
        self.tableView.hideEmtpyCells()
        
        //Indicator
        self.tableView.displayBackgroundMessage("Loading...".localized(),
                                                subMessage: "")
        
        if self.isCentral == true
        {
            let barBtnAll = UIBarButtonItem(title: "All".localized(), style: .done, target: self, action:#selector(self.selectAllWhos))
            navigationItem.rightBarButtonItem = barBtnAll
        }
    }
    
    //MARK: ACTIONS
    func selectAllWhos()
    {
        if self.whos2_selected.count == self.whos2.count
        {
            //Remove all
            self.whos2_selected.removeAll()
        }
        else
        {
            //Select All
            self.whos2_selected.removeAll()
            self.whos2_selected = self.whos2
        }
        
        self.tableView.reloadData()
    }
    

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //Indicator
        if self.whos2.count > 0
        {
            self.tableView.dismissBackgroundMessage()
        }
        else
        {
            self.tableView.displayBackgroundMessage("No results".localized(),
                                                    subMessage: "")
        }
        
        return self.whos2.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style:.default, reuseIdentifier:"Cell")
        let who =  self.whos2[indexPath.row]
        
        //Color for selected cell
        let selectedColor = UIView()
        selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
        cell.selectedBackgroundView = selectedColor
        
        //Config
        cell.textLabel?.numberOfLines = 2
        cell.textLabel?.textColor = UIColor.blackAsfalto()
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
        cell.accessoryType = self.isCentral == false ? .disclosureIndicator : .none
        
        cell.textLabel?.text = who.name
        
        //Checkmark
        if self.isCentral == true
        {
            if self.whos2_selected.contains(who) == true
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
        }
        
        return cell
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let who =  self.whos2[indexPath.row] as! WhoLevel_II
        
        if self.isCentral == true
        {
            if let cell = tableView.cellForRow(at: indexPath) as UITableViewCell! {
                
                if cell.accessoryType == .none
                {
                    cell.accessoryType = .checkmark
                    self.whos2_selected.append(who)
                    
                }
                else
                {
                    cell.accessoryType = .none
                    self.whos2_selected = self.whos2_selected.filter {$0 != who }
                    self.onDeselect?(who)
                }
            }
        }
        else
        {
            let vcWhos3: Whos3TableViewController = Storyboard.getInstanceFromStoryboard("Integration")
            vcWhos3.whos3 = who.whoLocal
            vcWhos3.whos3_selected = self.whos2_selected as!  [WhoLevel_III]
            vcWhos3.onDeinit = { [weak self] whos3Selected in
                
                self?.whos2_selected += whos3Selected
                self?.whos2_selected = Array(Set(self?.whos2_selected ?? []))
                self?.tableView.reloadData()
            }
            
            vcWhos3.onDeselect = { [weak self] who3Deselected in
                
                if let whos2_selected = self?.whos2_selected {
                    
                    for (index, tag) in whos2_selected.enumerated() where tag.id == who3Deselected.id
                    {
                        self?.whos2_selected.remove(at: index)
                    }
                    
                    self?.onDeselect?(who3Deselected)
                }
            }

            self.navigationController?.pushViewController(vcWhos3, animated: true)
        }
    }
}
