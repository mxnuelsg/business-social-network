//
//  Whos1TableViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 10/5/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class Whos1TableViewController: UITableViewController
{
    //MARK: VARIABLES & OUTLETS
    var stakeholder = Stakeholder(isDefault: true)
    var onAutomaticSave:((_ totalWhos: [Int]) -> ())?
    
    fileprivate var stakeholderAssociatedWhos = [WhoLevels]()
    
    var stakeholderPreselectedWhos = [Int]()
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.getWhos()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func viewWillDisappear(_ animated : Bool)
    {
        super.viewWillDisappear(animated)
        
        //If It's back trasition
        if self.isMovingFromParentViewController == true
        {
            self.onAutomaticSave?(self.stakeholderAssociatedWhos.map({$0.id}))
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        self.title = "WHOS"
        self.tableView.backgroundColor =  UIColor.groupTableViewBackground
        self.tableView.hideEmtpyCells()
        
        //Register Custom Cell
        tableView.register(UINib(nibName: "Who1Cell", bundle: nil), forCellReuseIdentifier: "Who1Cell")
    }
    
    //MARK: WEB SERVICE
    func getWhos()
    {
        //Request Object
        var wsObject: Dictionary <String, Any>  = [:]
        wsObject["IsDetail"] = "false"
        wsObject["SiteMappingId"] = self.stakeholder.site?.siteId ?? 0
        wsObject["StakeholderId"] = self.stakeholder.id
        
        //Indicator
        self.tableView.displayBackgroundMessage("Loading...".localized(),
                                             subMessage: "")
        
        LibraryAPI.shared.integrationBO.getWhos(wsObject, onSuccess: { (whos_L1, whos_L2) in
            
            //Remove indicator
            self.tableView.dismissBackgroundMessage()
            
            self.stakeholder.whos = whos_L1
            self.stakeholder.whosLevel2 =  whos_L2
            
            //Verify if It's prefilled
            if self.stakeholder.whosId.count > 0
            {
                self.preFillInfo(self.stakeholder.whosId)
            }
            
            self.tableView.reloadData()
            
            }) { (error) in
                
                self.tableView.displayBackgroundMessage("Error.TryAgain".localized(),
                                                                          subMessage: "")
        }
    }
    
    //MARK: ACTIONS
    fileprivate func getWhos2ForSelected(_ who: WhoLevel_I) -> [WhoLevel_II]
    {
        var whos2 = [WhoLevel_II]()
        
        for whoII in self.stakeholder.whosLevel2 where whoII.parentId == who.id
        {
            whos2.append(whoII)
        }
        
        //Sort Ascending
        whos2 = whos2.sorted(by: {$0.name < $1.name})
        
        return whos2
    }
    
    fileprivate func getWhosPreSelected(_ who: WhoLevel_I) -> [WhoLevels]
    {
        var whos_generic = [WhoLevels]()
        let central = self.stakeholder.site?.isCentral ?? false
        
        if central == true
        {
            for whoII in self.stakeholderAssociatedWhos as! [WhoLevel_II]where whoII.parentId == who.id
            {
                whos_generic.append(whoII)
            }
        }
        else
        {
            for whoII in self.stakeholderAssociatedWhos as! [WhoLevel_III]where whoII.grandParentId == who.id
            {
                whos_generic.append(whoII)
            }
        }
        
        return whos_generic
    }
    
    fileprivate func getTagsBy(_ who: WhoLevel_I) -> [WhoLevels]
    {
        var whoTags = [WhoLevels]()
        let central = self.stakeholder.site?.isCentral ?? false
        
        if central == true
        {
            for tag in self.stakeholderAssociatedWhos as! [WhoLevel_II] where tag.parentId == who.id
            {
                whoTags.append(tag)
            }
        }
        else
        {
            for tag in self.stakeholderAssociatedWhos as! [WhoLevel_III] where tag.grandParentId == who.id
            {
                whoTags.append(tag)
            }
        }
        
        //Sort Ascending
        whoTags = whoTags.sorted(by: {$0.name < $1.name})
        
        return whoTags
    }
    
    fileprivate func preFillInfo(_ whos: [Int])
    {
        let central = self.stakeholder.site?.isCentral ?? false
        
        if central == true
        {
            for id in whos
            {
                for whoL2 in self.stakeholder.whosLevel2 where whoL2.id == id
                {
                    self.stakeholderAssociatedWhos.append(whoL2)
                }
            }
        }
        else
        {
            for id in whos
            {
                for whoL2 in self.stakeholder.whosLevel2
                {
                    for whoL3 in whoL2.whoLocal where whoL3.id == id
                    {
                        self.stakeholderAssociatedWhos.append(whoL3)
                    }
                }
            }
            
            self.tableView.reloadData()
        }
    }
    
    //MARK: SHOW VIEW CONTROLLER
    fileprivate func pushWhos2ViewController(_ whos2: [WhoLevel_II], whos2PreSelected: [WhoLevels])
    {
        let vcWho2: Whos2TableViewController = Storyboard.getInstanceFromStoryboard("Integration")
        vcWho2.isCentral =  self.stakeholder.site?.isCentral ?? false
        vcWho2.whos2 = whos2
        vcWho2.whos2_selected = whos2PreSelected
        vcWho2.onDeinit = { [weak self] whos2Selected in
            
            self?.stakeholderAssociatedWhos += whos2Selected
            self?.stakeholderAssociatedWhos = Array(Set(self?.stakeholderAssociatedWhos ?? []))
            self?.tableView.reloadData()
        }
        
        vcWho2.onDeselect = { [weak self] who2Deselected in
            
            if let associatedWhos = self?.stakeholderAssociatedWhos {
                
                for (index, tag) in associatedWhos.enumerated() where tag.id == who2Deselected.id
                {
                    self?.stakeholderAssociatedWhos.remove(at: index)
                    self?.tableView.reloadData()
                }
            }
        }
        
        self.navigationController?.pushViewController(vcWho2, animated: true)
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.stakeholder.whos.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Who1Cell") as! Who1Cell
        cell.selectionStyle = .none
        
        let who = self.stakeholder.whos[indexPath.row]
        
        cell.lblTitle.text = who.name
        cell.whoTags = self.getTagsBy(who)
        cell.onSelectedCell = { [weak self] Void in
            
            if let whos2 = self?.getWhos2ForSelected(who), let whos2Selected = self?.getWhosPreSelected(who) {
                
                self?.pushWhos2ViewController(whos2, whos2PreSelected: whos2Selected)
            }
        }
        
        cell.onDelete = { [weak self] who in
            
            if let associatedWhos = self?.stakeholderAssociatedWhos {
                
                for (index, tag) in associatedWhos.enumerated() where tag.id == who.id
                {
                    self?.stakeholderAssociatedWhos.remove(at: index)
                    self?.tableView.reloadData()
                }
            }
        }

        return cell
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let who = self.stakeholder.whos[indexPath.row]
        
        let whos2 = self.getWhos2ForSelected(who)
        let whos2Selected = self.getWhosPreSelected(who)
        self.pushWhos2ViewController(whos2, whos2PreSelected: whos2Selected)

    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 110
    }
}
