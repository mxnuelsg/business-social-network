//
//  Whos3TableViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 10/6/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class Whos3TableViewController: UITableViewController
{
    //MARK: VARIABLES & OUTLETS
    var whos3 = [WhoLevels]()
    var whos3_selected = [WhoLevels]()
    var onDeinit:((_ whos3Selected: [WhoLevels]) -> ())?
    var onDeselect:((_ who3Deselected: WhoLevels) -> ())?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        self.onDeinit?(self.whos3_selected)
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        self.title = "WHOS III"
        self.tableView.hideEmtpyCells()
        
        //Indicator
        self.tableView.displayBackgroundMessage("Loading...".localized(),
                                                subMessage: "")
        
        let barBtnAll = UIBarButtonItem(title: "All".localized(), style: .done, target: self, action:#selector(self.selectAllWhos))
        navigationItem.rightBarButtonItem = barBtnAll
    }
    
    //MARK: ACTIONS
    func selectAllWhos()
    {
        if self.whos3_selected.count == self.whos3.count
        {
            //Remove all
            self.whos3_selected.removeAll()
        }
        else
        {
            //ASelect All
            self.whos3_selected.removeAll()
            self.whos3_selected = self.whos3
        }
        
        self.tableView.reloadData()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //Indicator
        if self.whos3.count > 0
        {
            self.tableView.dismissBackgroundMessage()
        }
        else
        {
            self.tableView.displayBackgroundMessage("No results".localized(),
                                                    subMessage: "")
        }
        
        return self.whos3.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style:.default, reuseIdentifier:"Cell")
        let who =  self.whos3[indexPath.row]
        
        //Color for selected cell
        let selectedColor = UIView()
        selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
        cell.selectedBackgroundView = selectedColor
        
        //Config
        cell.textLabel?.numberOfLines = 2
        cell.textLabel?.textColor = UIColor.blackAsfalto()
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
        
        cell.textLabel?.text = who.name
        
        if self.whos3_selected.contains(who)
        {
            cell.accessoryType = .checkmark
        }
        else
        {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let who =  self.whos3[indexPath.row] as! WhoLevel_III
        
        if let cell = tableView.cellForRow(at: indexPath) as UITableViewCell! {
            
            if cell.accessoryType == .none
            {
                cell.accessoryType = .checkmark
                self.whos3_selected.append(who)
                
            }
            else
            {
                cell.accessoryType = .none
                self.whos3_selected = self.whos3_selected.filter {$0 != who }
                self.onDeselect?(who)
            }
        }
    }
}
