//
//  StakeholderDetailViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 11/17/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class StakeholderDetailViewController_iPad: UIViewController
{
    //MARK: PROPERTIES & OUTLET
    @IBOutlet weak var imgPersonBanner: UIImageView!
    @IBOutlet weak var imgPersonThumbnail: UIImageView!
    @IBOutlet weak var lblFullname: UILabel!
    @IBOutlet weak var lblLateralTitle: UILabel!
    @IBOutlet weak var lblSite: UILabel!
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var viewLateralTitle: UIView!
    @IBOutlet weak var pageControlLateral: UIPageControl!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var btnDisable: UIButton!
    @IBOutlet weak var segmentedPages: UISegmentedControl!
    @IBOutlet weak var labelUserRating: UILabel!
    @IBOutlet weak var labelAdminRating: UILabel!
    fileprivate lazy var searchBar: AutoSearchBar = AutoSearchBar()
    
    //Containers
    @IBOutlet weak var containerProfile: UIView!
    @IBOutlet weak var containerProjects: UIView!
    @IBOutlet weak var containerRelations: UIView!
    @IBOutlet weak var containerGeographies: UIView!
    
    //Properties
    var stakeholder: Stakeholder!
    var isEnglish = true
    var isModal = false
    var arrayBarButtonItems = [UIBarButtonItem]()
    var barBtnActions: UIBarButtonItem!
    var barBtnCompose: UIBarButtonItem!
    
    //Controllers
    var lateralContainer:LateralCalendarFeed_iPad!
    var vcProfileSection: StakeholderProfileSectionTable_iPad!
    var vcProjectsSection: ProjectsTableViewController!
    var vcRelationsSection: RelationsTableViewController!
    
    
    //MARK: LIFE CYCLE
    required init?(coder aDecoder: NSCoder)
    {
        self.stakeholder = Stakeholder(json: nil)
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig_iPad()
        self.loadStakeholderDetail()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
        
        //Reset Badge icon
        self.removeBadgeIcon()
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig_iPad()
    {
        self.localize()
        self.viewLateralTitle.setBorder()
        self.setupNavigation(animated: false)

        //Blur
        self.imgPersonBanner.blur()
        
        //Btn Follow
        self.btnFollow.layer.cornerRadius = 2
        self.btnFollow.clipsToBounds = true
        self.btnFollow.setTitle(String(), for: UIControlState())
        self.btnFollow.addTarget(self, action: #selector(self.followOrNotStakeholder), for: UIControlEvents.touchUpInside)
        
        //Btn Disable
        if LibraryAPI.shared.currentUser?.role == UserRole.globalManager
        {
            self.btnDisable.layer.cornerRadius = 2
            self.btnDisable.clipsToBounds = true
            self.btnDisable.setTitle(String(), for: UIControlState())
            self.btnDisable.addTarget(self, action: #selector(self.enableOrDisableStakeholder), for: UIControlEvents.touchUpInside)
            self.btnDisable.isHidden = false
        }
        
        //Segmented
        self.segmentedPages.selectedSegmentIndex = 0
        self.segmentedPages.addTarget(self, action: #selector(self.segmentedControlHandler(_:)), for: UIControlEvents.valueChanged)
        
        //Backgrounds
        self.vcProjectsSection.view.backgroundColor = UIColor.white
        self.vcRelationsSection.view.backgroundColor = UIColor.white
        
        //Items Bar
        self.barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        let barBtnSearch = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(self.searchInDetail))
        let barBtnRefresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(self.loadStakeholderDetail))
        self.barBtnActions = self.getActionsButton(target: self, action: #selector(self.showMenuActions))
        
        self.arrayBarButtonItems = [self.barBtnCompose, barBtnSearch, barBtnRefresh, self.barBtnActions]
        navigationItem.rightBarButtonItems = self.arrayBarButtonItems
        
        //Modal Configuration
        if self.isModal == true
        {
            let barBtnCancel = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.stop, target: self, action: #selector(self.closeModal))
            navigationItem.leftBarButtonItem = barBtnCancel
        }
        
        //Search Controller
        searchBar.initialize(onSearchText: { [weak self] (text) in
            self?.updateSearch(text: text)
            
            }, onCancel: { [weak self] in
                self?.searchBar.text = ""
                self?.updateSearch(text: "")
                self?.setupNavigation(animated: true)
        })
    }
    
    fileprivate func localize()
    {
        self.segmentedPages.setTitle("PROFILE".localized(), forSegmentAt: 0)
        self.segmentedPages.setTitle("PROJECTS".localized(), forSegmentAt: 1)
        self.segmentedPages.setTitle("RELATIONS".localized(), forSegmentAt: 2)
        self.segmentedPages.setTitle("GEOGRAPHIES".localized(), forSegmentAt: 3)
    }
    //MARK: CRATE ACTIONS BUTTON
    func getActionsButton(target: AnyObject, action: Selector) -> UIBarButtonItem
    {
        let btnActions = UIButton(frame: CGRect(x: 0, y: 0, width: 80, height: 24))
        btnActions.setTitle("Actions  ▾".localized(), for: UIControlState())
        btnActions.titleLabel!.font = UIFont.systemFont(ofSize: 14)
        btnActions.layer.borderColor = UIColor.whiteColor(0.4).cgColor
        btnActions.layer.borderWidth = 1
        btnActions.layer.cornerRadius = 3
        btnActions.addTarget(target, action: action, for: UIControlEvents.touchUpInside)
        
        return UIBarButtonItem(customView: btnActions)
    }
    
    // MARK: WEB SERVICE
    func loadStakeholderDetail()
    {
        self.loadStakeholderDetailLight()
        self.loadStakeholderProjects()
        self.loadStakeholderRelations()
        self.loadStakeholderGeographies()
        
        self.displayFeed()
        self.displayCalendar()
        
//        //Indicator
//        MessageManager.shared.showLoadingHUD()
//        LibraryAPI.shared.stakeholderBO.getStakeholderDetail(stakeholder.id, onSuccess: {
//            (actor) -> () in
//
//            MessageManager.shared.hideHUD()
//
//            //Verify if exists/ is valid
//            guard let existingActor = actor else {
//                return
//            }
//
//            self.stakeholder = existingActor
//            self.loadStakeholderInfo()
//
//            }) { error in
//
//                MessageManager.shared.hideHUD()
//
//                _ = self.navigationController?.popViewController(animated: true)
//
//                MessageManager.shared.showBar(title: "Error",
//                    subtitle: "Error.TryAgain".localized(),
//                    type: .error,
//                    fromBottom: false)
//        }
    }
    
    func loadStakeholderDetailLight()
    {
        MessageManager.shared.showLoadingHUD()
        LibraryAPI.shared.stakeholderBO.getStakeholderDetailLight(stakeholder.id, onSuccess: { (stakeholder) in
            
            MessageManager.shared.hideHUD()
            self.stakeholder = stakeholder
            self.loadStakeholderInfo()
        }) { (error) in
            
            MessageManager.shared.hideHUD()
            MessageManager.shared.showBar(title: "There was an error while loading the stakeholder information".localized(),
                                          subtitle: String(),
                                          type: .error,
                                          fromBottom: false)
        }
    }
    
    func loadStakeholderProjects()
    {
        self.vcProjectsSection.displayBackgroundMessage("Loading...".localized(), subMessage: String())
        LibraryAPI.shared.stakeholderBO.getStakeholdersProjects(stakeholder.id , onSuccess: { (projects) in
        
            self.vcProjectsSection.dismissBackgroundMessage()
            self.stakeholder.projects = projects
            self.displayStakeholderProjects()
            //UI update
//            self.vcProjects?.tableView.dismissBackgroundMessage()
//            self.vcProjects?.refreshControl?.setLastUpdate()
//            self.vcProjects?.refreshControl?.endRefreshing()
//            if projects.count == 0
//            {
//                self.vcProjects?.tableView.displayBackgroundMessage("No data available".localized(), subMessage: "Pull to refresh".localized())
//            }
//
//            //Update table
//            self.vcProjects?.projects = projects
//            self.vcProjects?.reload()
        }) { (error) in
            self.vcProjectsSection.displayBackgroundMessage("There was an error while loading the projects".localized(),
                                                            subMessage: String())
//            self.vcProjects?.projects.removeAll()
//            self.vcProjects?.reload()
//            self.vcProjects?.refreshControl?.endRefreshing()
//            self.vcProjects?.tableView.displayBackgroundMessage("An error ocurred".localized(), subMessage: "Pull to refresh".localized())
        }
    }
    
    func loadStakeholderRelations()
    {
        self.vcRelationsSection.displayBackgroundMessage("Loading...".localized(), subMessage: String())
        LibraryAPI.shared.stakeholderBO.getStakeholdersRelations(stakeholder.id, onSuccess: { (relations) in
        
            self.vcRelationsSection.dismissBackgroundMessage()
            self.stakeholder.relations = relations
            self.displayStakeholderRelations()
//            //UI update
//            self.vcRelations?.tableView.dismissBackgroundMessage()
//            self.vcRelations?.refreshControl?.setLastUpdate()
//            self.vcRelations?.refreshControl?.endRefreshing()
//            if relations.count == 0
//            {
//                self.vcRelations?.tableView.displayBackgroundMessage("No data available".localized(), subMessage: "Pull to refresh".localized())
//            }
//
//            //Update table
//            self.vcRelations?.relations = relations
//            self.vcRelations?.tableView.reloadData()
        }) { (error) in
            
            self.vcRelationsSection.displayBackgroundMessage("There was an error while loading the relations".localized(),
                                                             subMessage: String())
//            self.vcRelations?.relations.removeAll()
//            self.vcRelations?.tableView.reloadData()
//            self.vcRelations?.refreshControl?.endRefreshing()
//            self.vcRelations?.tableView.displayBackgroundMessage("An error ocurred".localized(), subMessage: "Pull to refresh".localized())
        }
    }
    
    func loadStakeholderGeographies()
    {
        LibraryAPI.shared.stakeholderBO.getStakeholdersGeographies( stakeholder.id, onSuccess: { (geographies) in
            
            self.stakeholder.geographies = geographies
            self.displayGeographies()
//            //UI update
//            self.vcGeographies?.tableView.dismissBackgroundMessage()
//            self.vcGeographies?.refreshControl?.setLastUpdate()
//            self.vcGeographies?.refreshControl?.endRefreshing()
//            if geographies.count == 0
//            {
//                self.vcGeographies?.tableView.displayBackgroundMessage("No data available".localized(), subMessage: "Pull to refresh".localized())
//            }
//
//            //Update table
//            self.vcGeographies?.clusterItems = geographies
//            self.vcGeographies?.backupClusterItems = geographies
        }) { (error) in
            MessageManager.shared.showBar(title: "There was an error while loading the geographies".localized(),
                                          subtitle: String(),
                                          type: .error,
                                          fromBottom: false)
//            self.vcGeographies?.clusterItems.removeAll()
//            self.vcGeographies?.tableView.reloadData()
//            self.vcGeographies?.refreshControl?.endRefreshing()
//            self.vcGeographies?.tableView.displayBackgroundMessage("An error ocurred".localized(), subMessage: "Pull to refresh".localized())
        }
    }
    //MARK: LOAD STAKEHOLDER INFORMATION
    func loadStakeholderInfo()
    {
        //Post
        self.barBtnCompose.isEnabled = self.stakeholder.isEnable
        
        //Picture
        self.imgPersonBanner.setImageWith(URL(string: self.stakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        self.imgPersonThumbnail.setImageWith(URL(string: self.stakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        
        //Title
        let strJobPosition = self.isEnglish == true ? stakeholder.jobPosition!.valueEn : stakeholder.jobPosition!.valueEs
        
        self.lblFullname.text = self.stakeholder.fullName
        self.lblFullname.adjustsFontSizeToFitWidth = true
        
        //Position / Company
        self.lblPosition.text = strJobPosition.trim().isEmpty && stakeholder.companyName.trim().isEmpty ? "" : "\(strJobPosition) / \(stakeholder.companyName)"
        self.lblPosition.adjustsFontSizeToFitWidth = true
        
        //Site
        self.lblSite.isHidden = true        
        
        //Following status
        if self.stakeholder.isFollow == FollowStatus.following
        {
            self.btnFollow.setTitle("︎︎︎︎︎✔︎ " + "Following".localized(), for: UIControlState())
        }
        else
        {
            self.btnFollow.setTitle("Follow".localized(), for: UIControlState())
        }
        
        //Stakeholder status
        if self.stakeholder.isEnable == true
        {
            self.btnDisable.setTitle("✔︎ " + " " + "Enabled".localized(), for: UIControlState())
        }
        else
        {
            self.btnDisable.setTitle("Enable".localized(), for: UIControlState())
        }
        
        self.labelUserRating.text = stakeholder.userRating?.getString()
        self.labelAdminRating.text = stakeholder.adminRating?.getString()
        
        //Load Sub-Modules
        self.displayStakeholderProfile()
    }
    
    //MARK: SUB-MODULES
    func displayStakeholderProfile()
    {
        self.vcProfileSection.stakeholder = self.stakeholder
        self.vcProfileSection.tableView.reloadData()
    }
    
    func displayStakeholderProjects()
    {
        self.vcProjectsSection.projects = self.stakeholder.projects
        
        //Background Message
        if self.vcProjectsSection.projects.count > 0
        {
            self.vcProjectsSection.tableView.dismissBackgroundMessage()
        }
        else
        {
            self.vcProjectsSection.tableView.displayBackgroundMessage("No Projects".localized(),
                subMessage: "")
        }
        
        self.vcProjectsSection.tableView.reloadData()
    }
    
    func displayStakeholderRelations()
    {
        self.vcRelationsSection.relations = self.stakeholder.relations
        self.vcRelationsSection.tableView.reloadData()
    }
    
    func displayFeed()
    {
        self.lateralContainer.loadFeedForStakeholder(self.stakeholder)
    }
    
    func displayCalendar()
    {
        self.lateralContainer.loadCalendarForStakeholder(self.stakeholder)
    }
    
    func displayGeographies() {
        let geographies: GeoTableViewController = Storyboard.getInstanceFromStoryboard("Main")
        geographies.ownerModule = .stakeholderDetail
        geographies.clusterItems = stakeholder.geographies
        if (geographies.clusterItems.count == 0) {
            geographies.tableView.displayBackgroundMessage("No Geographies".localized(),
                                                                      subMessage: "")
        }
        containerGeographies.addSubViewController(geographies, parentVC: self)
    }

    //MARK: ACTIONS (Local Search)
    func updateSearch(text: String)
    {
        self.vcProfileSection.searchText(text)
        
        self.lateralContainer.vcFeedList.filterByText(text)
        self.lateralContainer.vcFile.filterByText(text)
        self.lateralContainer.vcCalendar.filterByText(text)
        self.vcProjectsSection.filterByText(text)
        self.vcRelationsSection.filterByText(text)
    }
    
    func searchInDetail()
    {
        //Set Search bar on Navigation Controller
        self.searchBar.showInNavigationItem(navigationItem, animated: true)
        let barBtnCancel = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action:#selector(self.cancelSearch))
        navigationItem.rightBarButtonItem = barBtnCancel
    }
    
    func cancelSearch()
    {
        //clean search bar
        self.searchBar.cancelSearch()
        
        if self.isModal == true
        {
            let barBtnCancel = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.stop, target: self, action: #selector(self.closeModal))
            navigationItem.leftBarButtonItem = barBtnCancel
        }
        
        navigationItem.rightBarButtonItems = self.arrayBarButtonItems
    }
    
    func setupNavigation(animated: Bool)
    {
        self.title = "STAKEHOLDER".localized()
        
        navigationItem.setHidesBackButton(false, animated: animated)
        navigationItem.fadeOutTitleView()
    }
    
    //MARK:ACTIONS
    func closeModal()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func followOrNotStakeholder()
    {
        if self.stakeholder.isFollow == FollowStatus.following
        {
            let vcConfirmUnfollow = UIAlertController(title: "Would you like to disable?".localized(), message: self.stakeholder.fullName, preferredStyle: .alert)
            let actionUnfollow = UIAlertAction(title: "Unfollow".localized(), style: UIAlertActionStyle.destructive, handler: {
                (action) in
                
                //Unfollow
                LibraryAPI.shared.stakeholderBO.unfollowStakeholder(self.stakeholder,
                    onSuccess: { () -> () in
                        
                        //Refresh Stakeholder List
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadStakeholdersList"), object: nil)
                        
                        MessageManager.shared.showBar(title: "Info".localized(),
                            subtitle: "Now, you're not following to".localized() + " " + "\(self.stakeholder.fullName)",
                            type: .info,
                            fromBottom: false)
                        
                        self.stakeholder.isFollow = FollowStatus.notFollowing
                        self.btnFollow.setTitle("Follow".localized(), for: UIControlState())
                        
                    }, onError: {error in
                        
                        MessageManager.shared.showBar(title: "Error", subtitle: "Error.TryAgain".localized(),
                            type: .error,
                            fromBottom: false)
                })
            })
            
            let actionCancel = UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler: nil)
            
            vcConfirmUnfollow.addAction(actionUnfollow)
            vcConfirmUnfollow.addAction(actionCancel)
            
            self.present(vcConfirmUnfollow, animated: true, completion: nil)
        }
        else
        {
            //Follow
            LibraryAPI.shared.stakeholderBO.followStakeholder(self.stakeholder,
                onSuccess: { () -> () in
                    
                    //Refresh Stakeholder List
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadStakeholdersList"), object: nil)
                    
                    MessageManager.shared.showBar(title: "Info".localized(),
                        subtitle: "Now, you're  following to".localized() + " " + "\(self.stakeholder.fullName)",
                        type: .info,
                        fromBottom: false)
                    
                    self.stakeholder.isFollow = FollowStatus.following
                    self.btnFollow.setTitle("✔︎ " + "Following".localized(), for: UIControlState())
                    
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error", subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            })
        }
    }
    
    func enableOrDisableStakeholder()
    {
        if self.stakeholder.isEnable == true
        {
            let vcConfirmUnfollow = UIAlertController(title: nil, message: self.stakeholder.fullName, preferredStyle: UIAlertControllerStyle.alert)
            let actionUnfollow = UIAlertAction(title: "Disable".localized(), style: UIAlertActionStyle.destructive, handler: {
                (action) in
                
                //Disable
                LibraryAPI.shared.stakeholderBO.disableStakeholder(self.stakeholder,
                    onSuccess: { () -> () in
                        
                        //Refresh Stakeholder List
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadStakeholdersList"), object: nil)
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "ReloadNewPostHome"), object: nil)
                        
                        MessageManager.shared.showBar(title: "Info".localized(),
                            subtitle: "\(self.stakeholder.fullName)" + " " + "has been disabled".localized(),
                            type: .info,
                            fromBottom: false)
                        
                        self.stakeholder.isEnable = false
                        self.btnDisable.setTitle("Enable".localized(), for: UIControlState())
                        self.barBtnCompose.isEnabled = self.stakeholder.isEnable
                        
                    }, onError: {error in
                        
                        MessageManager.shared.showBar(title: "Error", subtitle: "Error.TryAgain".localized(),
                            type: .error,
                            fromBottom: false)
                })
            })
            
            let actionCancel = UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler: nil)
            
            vcConfirmUnfollow.addAction(actionUnfollow)
            vcConfirmUnfollow.addAction(actionCancel)
            
            self.present(vcConfirmUnfollow, animated: true, completion: nil)
        }
        else
        {
            //Enable
            LibraryAPI.shared.stakeholderBO.enableStakeholder(self.stakeholder,
                onSuccess: { () -> () in
                    
                    //Refresh Stakeholder List
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadStakeholdersList"), object: nil)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "ReloadNewPostHome"), object: nil)
                    
                    MessageManager.shared.showBar(title: "Info".localized(),
                        subtitle: "\(self.stakeholder.fullName)" + " " + "has been enabled".localized(),
                        type: .info,
                        fromBottom: false)
                    
                    self.stakeholder.isEnable = true
                    self.btnDisable.setTitle("✔︎ " + " " + "Enabled".localized(), for: UIControlState())
                    self.barBtnCompose.isEnabled = self.stakeholder.isEnable
                    
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            })
        }
    }

    func segmentedControlHandler(_ segmented: UISegmentedControl)
    {
        let index = segmented.selectedSegmentIndex
        
        self.containerProfile.isHidden = index != 0
        self.containerProjects.isHidden = index != 1
        self.containerRelations.isHidden = index != 2
        self.containerGeographies.isHidden = index != 3
    }
    
    func createNewPost()
    {
        let vcNewPost = NewPostModal()
        vcNewPost.stakeholderTagged = self.stakeholder
        vcNewPost.onViewControllerClosed = { [weak self] Void in
            
            //Refresh all
            self?.loadStakeholderDetail()
        }
        
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
        
        present(navController, animated: true, completion:nil)
    }
    
    func changeLanguage()
    {
        if self.isEnglish == true
        {
            self.isEnglish = false
            
            MessageManager.shared.showStatusBar(title:"Spanish".localized(),
                                                                 type: .info,
                                                                 containsIcon: false)
        }
        else
        {
            self.isEnglish = true
            
            MessageManager.shared.showStatusBar(title:"English".localized(),
                                                                 type: .info,
                                                                 containsIcon: false)
        }
        
        self.vcProfileSection.isEnglish = self.isEnglish
    }
    
    func sendUpdateRequest()
    {
        let vcUpdateRequest: RequestUpdateViewController = Storyboard.getInstanceFromStoryboard("popups")
        vcUpdateRequest.stakeholderId = self.stakeholder.id
        vcUpdateRequest.modalPresentationStyle = .formSheet
        self.present(vcUpdateRequest, animated: true, completion: nil)
    }
    
    func editStakeholder()
    {
        let vcEditStakeholder: EditStakeholderViewController_iPad = Storyboard.getInstanceFromStoryboard("EditStakeholderPad")
        vcEditStakeholder.stakeholder = self.stakeholder
        vcEditStakeholder.onCompletedEdition = { [weak self] Void in
            
            //Reload
            self?.loadStakeholderDetail()
        }
        
        
        let navController = NavyController(rootViewController: vcEditStakeholder)
        navController.modalPresentationStyle = .pageSheet
        
        present(navController, animated: true, completion: nil)
    }
    
    @IBAction func rate(button: UIButton) {
        RatingViewController_iPad.present(inController: self,
                                          stakeholder: stakeholder,
                                          sourceView: button,
                                          onEndRating: { [weak self] in
                                              self?.loadStakeholderDetail()
                                          })
    }
    
    //MARK: ACTION MENU
    func showMenuActions()
    {
        //Selected Style
        self.barBtnActions.customView?.backgroundColor = UIColor.blueTag()
        
        //Content View
        let vcTableOptions = StringTableViewController()
        
        if (LibraryAPI.shared.currentUser?.role == .globalManager || LibraryAPI.shared.currentUser?.role == .cardManager)
            && self.stakeholder.canEdit == true
        {
            vcTableOptions.options = [
                "Language".localized(),
                "Request Update".localized(),
                "Edit Stakeholder".localized()
            ]
            vcTableOptions.icons = [
                "LanguageEsp",
                "Request",
                "EditUser"
            ]
        }
        else
        {
            vcTableOptions.options = [
                "Language".localized(),
                "Request Update".localized()
            ]
            vcTableOptions.icons = [
                "LanguageEsp",
                "Request"
            ]
        }
        
        vcTableOptions.onSelectedOption = { (numberOfRow) in
            
            switch numberOfRow
            {
            case 0:
                self.changeLanguage()
            case 1:
                self.sendUpdateRequest()
            case 2:
                self.editStakeholder()
            default:
                break
            }
        }
        
        vcTableOptions.onClosed = {
        
            //Unselected Style
            self.barBtnActions.customView?.backgroundColor = UIColor.clear
        }
        
        //Show as popover
        vcTableOptions.modalPresentationStyle = .popover
        vcTableOptions.popoverPresentationController?.permittedArrowDirections = .any
        vcTableOptions.popoverPresentationController?.barButtonItem = barBtnActions
        
        present(vcTableOptions, animated: true, completion: nil)
    }
    
    //MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let id = segue.identifier, id == "segueID_LateralCalendarFeed" {
            
            self.lateralContainer  = segue.destination as! LateralCalendarFeed_iPad
            self.lateralContainer.onPageDidChange = {
                (page) in
                
                self.pageControlLateral.currentPage = page
                self.lblLateralTitle.text = (page == 0) ? "FEED".localized() : "CALENDAR".localized()
            }
            
            self.lateralContainer.onGoCalendar = {
                
                let vcCalendar = Storyboard.getInstanceOf(CalendarViewController_iPad.self)
                self.navigationController?.pushViewController(vcCalendar, animated: true)
            }
        }
        
        if let id = segue.identifier, id == "StakeholderProfileSectionTable_iPad" {
            
            self.vcProfileSection  = segue.destination as! StakeholderProfileSectionTable_iPad
        }
        
        if let id = segue.identifier, id == "ProjectsTableViewController" {
            
            self.vcProjectsSection  = segue.destination as! ProjectsTableViewController
            self.vcProjectsSection.isSwipeable = false
        }
        
        if let id = segue.identifier, id == "RelationsTableViewController" {
            
            self.vcRelationsSection  = segue.destination as! RelationsTableViewController
        }
    }
}
