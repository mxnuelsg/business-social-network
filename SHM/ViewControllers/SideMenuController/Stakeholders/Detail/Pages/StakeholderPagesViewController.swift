//
//  StakeholderPagesViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/14/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class StakeholderPagesViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var constraintContentWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintContentHeight: NSLayoutConstraint!
    
    @IBOutlet weak var containerViewCalendar: UIView!
    @IBOutlet weak var containerViewCalendarList: UIView!
    
    var onHeightChanged: ((_ newHeight: CGFloat) -> (CGFloat))!
    
    var pageCount = 3
    var currentPage = 0
    
    var vcDetail: StakeholderDetailPageViewController!
    var vcFeed: StakeholderFeedPageViewController!
    var vcCalendar: CalendarViewController!
    var vcCalendarList: CalendarListViewController!
    
    var onPageChange: ((_ page:Int) -> ())?
    var pageDidChange: (() -> ())?
    var stakeholderId: Int = 0
    
    // MARK:  LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()

        let allWidth = UIScreen.main.bounds.width * CGFloat(pageCount)
        
        constraintContentWidth.constant = allWidth
        
        self.view.isUserInteractionEnabled = true
    }
    
    override func viewDidLayoutSubviews() {
        
        let allWidth = UIScreen.main.bounds.width * CGFloat(pageCount)
        
        constraintContentWidth.constant = allWidth
        
        self.scrollView.contentSize = self.viewContent.frame.size
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        self.scrollView.contentSize = self.viewContent.frame.size
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segueID_StakeholderPageDetail"
        {
            vcDetail = segue.destination as! StakeholderDetailPageViewController
            vcDetail.focus()
            vcDetail.onHeightChanged = { (newHeight) in
                
                print("Detail Page (NEW HEIGHT): \(newHeight) in \(type(of: self)))")
                
                if self.vcDetail.hasFocus == true
                {
                    let recommendedHeight = self.onHeightChanged(newHeight)
                    
                    if recommendedHeight > newHeight
                    {
                        self.constraintContentHeight.constant = recommendedHeight
                    }
                    else
                    {
                        self.constraintContentHeight.constant = newHeight
                    }
                }
                self.viewContent.layoutIfNeeded()
                
                return self.constraintContentHeight.constant
            }
            
        }
        else if segue.identifier == "segueID_StakeholderPageFeed"
        {
            vcFeed = segue.destination as! StakeholderFeedPageViewController
            vcFeed.onHeightChanged = { (newHeight) in
                print("Feed (NEW HEIGHT): \(newHeight) in \(type(of: self)))")
                
                if self.vcFeed.hasFocus == true
                {
                    let recommendedHeight = self.onHeightChanged(newHeight)
                
                    if recommendedHeight > newHeight
                    {
                        self.constraintContentHeight.constant = recommendedHeight
                    }
                    else
                    {
                        self.constraintContentHeight.constant = newHeight
                    }
                }
                self.viewContent.layoutIfNeeded()
                
                return self.constraintContentHeight.constant
            }
        }
        else if segue.identifier == "segueID_CalendarViewController"
        {
            vcCalendar = segue.destination as! CalendarViewController
            vcCalendar.loadByStakeholderId = stakeholderId
            vcCalendar.onHeightChanged = { (newHeight) in
                print("Calendar View (NEW HEIGHT): \(newHeight) in \(type(of: self)))")
                
                if self.vcCalendar.hasFocus == true
                {
                    let recommendedHeight = self.onHeightChanged(newHeight)
                
                    if recommendedHeight > newHeight
                    {
                        self.constraintContentHeight.constant = recommendedHeight
                    }
                    else
                    {
                        self.constraintContentHeight.constant = newHeight
                    }
                }
                self.viewContent.layoutIfNeeded()
                
                return self.constraintContentHeight.constant
            }
            containerViewCalendar.isHidden = false
        }
        else if segue.identifier == "segueID_CalendarListViewController"
        {
            vcCalendarList = segue.destination as! CalendarListViewController
            vcCalendarList.loadByStakeholderId = stakeholderId
            vcCalendarList.onHeightChanged = { (newHeight) in
                
                print("Calendar List (NEW HEIGHT): \(newHeight) in \(type(of: self)))")
                
                if self.vcCalendarList.hasFocus == true
                {
                    let recommendedHeight = self.onHeightChanged(newHeight)
                
                    if recommendedHeight > newHeight
                    {
                        self.constraintContentHeight.constant = recommendedHeight
                    }
                    else
                    {
                        self.constraintContentHeight.constant = newHeight
                    }
                }
                
                self.viewContent.layoutIfNeeded()
                
                return self.constraintContentHeight.constant
            }
            containerViewCalendarList.isHidden = true
        }
    }
}

extension StakeholderPagesViewController: UIScrollViewDelegate
{
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let width = scrollView.frame.size.width;
        let page = Int((scrollView.contentOffset.x + (0.5 * width)) / width);
        
        if currentPage != page
        {
            currentPage = page
            onPageChange?(page)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        pageDidChange?()
    }
}
