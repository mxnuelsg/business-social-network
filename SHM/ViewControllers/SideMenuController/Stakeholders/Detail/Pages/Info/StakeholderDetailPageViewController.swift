//
//  StakeholderDetailPageViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/17/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit


class StakeholderDetailPageViewController: UIViewController
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var segmentedSections: UISegmentedControl!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var viewContainerProfile: UIView!
    @IBOutlet weak var viewContainerProjects: UIView!
    @IBOutlet weak var viewContainerRelations: UIView!
    
    @IBOutlet weak var viewContainerGeographies: UIView!
    
    @IBOutlet weak var constraintViewContainerHeight: NSLayoutConstraint!
    
    var vcSectionProfile: StakeholderProfileSectionTable_iPad!
    var vcSectionProjects: ProjectsTableViewController!
    var vcSectionRelations: RelationsTableViewController!
    var vcSectionGeographies: GeoTableViewController!
    
    var onHeightChanged: ((_ newHeight: CGFloat) -> (CGFloat))!
    
    var totalHeightProfile: CGFloat = 1000
    var totalHeightProjects: CGFloat = 1000
    var totalHeightRelations: CGFloat = 1000
    var totalHeightGeographies: CGFloat = 1000 {
        didSet{
            self.setGeographiesTableHeight()
        }
    }

    fileprivate(set) internal var hasFocus = false

    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.localize()
        segmentedSections.tintColor = UIColor.blueBelize()
        displayProfile()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func localize()
    {
        self.segmentedSections.setTitle("Profile".localized(), forSegmentAt: 0)
        self.segmentedSections.setTitle("Projects".localized(), forSegmentAt: 1)
        self.segmentedSections.setTitle("Relations".localized(), forSegmentAt: 2)
        self.segmentedSections.setTitle("Geographies".localized(), forSegmentAt: 3)
        let attr = NSDictionary(object: UIFont(name: "HelveticaNeue", size: 12.0)!, forKey: NSFontAttributeName as NSCopying)
        self.segmentedSections.setTitleTextAttributes(attr as  [NSObject : AnyObject], for: .normal)
    }
    // MARK: SEGMENTED CONTROL
    @IBAction func segmentedSectionsChanged(_ sender: AnyObject)
    {
        if let segmented = sender as? UISegmentedControl
        {
            switch(segmented.selectedSegmentIndex)
            {
            case 0:
                displayProfile()
            case 1:
                displayProjects()
            case 2:
                displayRelations()
            case 3:
                displayGeographies()
            default:
                return
            }
        }
    }
    
    // MARK: - DISPLAY SEGMENTS
    func displayProfile()
    {
        viewContainerProfile.isHidden = false
        viewContainerProjects.isHidden = true
        viewContainerRelations.isHidden = true
        viewContainerGeographies.isHidden = true
        
        vcSectionProfile.focus()
    }
    
    func displayProjects()
    {
        viewContainerProfile.isHidden = true
        viewContainerProjects.isHidden = false
        viewContainerRelations.isHidden = true
        viewContainerGeographies.isHidden = true
        
        vcSectionProfile.unfocus()
        
        setProjectsTableHeight()
    }
    
    func displayRelations()
    {
        viewContainerProfile.isHidden = true
        viewContainerProjects.isHidden = true
        viewContainerRelations.isHidden = false
        viewContainerGeographies.isHidden = true
        
        vcSectionProfile.unfocus()
        
        setRelationsTableHeight()
    }
    
    func displayGeographies()
    {
        viewContainerProfile.isHidden = true
        viewContainerProjects.isHidden = true
        viewContainerRelations.isHidden = true
        viewContainerGeographies.isHidden = false
        
        vcSectionProfile.unfocus()
        
        setGeographiesTableHeight()
    }
    
    //MARK: FOCUS & UNFOCUS
    func focus()
    {
        hasFocus = true
        
        guard onHeightChanged != nil else { return }
        
        var currentTotalHeight = CGFloat(self.viewContainer.frame.origin.y)
        
        if viewContainerProfile.isHidden == false {
            currentTotalHeight = totalHeightProfile
        }
        else if viewContainerProjects.isHidden == false {
            currentTotalHeight = totalHeightProjects
        }
        else if viewContainerRelations.isHidden == false {
            currentTotalHeight = totalHeightRelations
        }
        else if viewContainerGeographies.isHidden == false {
            currentTotalHeight = totalHeightGeographies
        }
        
        _ = onHeightChanged(currentTotalHeight)
    }
    
    func unfocus() {
        hasFocus = false
    }
    
    //MARK:RELOAD PROYECTS
    func reloadProjectsTable()
    {
        vcSectionProjects.tableView.reloadData()
        
        guard vcSectionProjects.tableView.numberOfRows(inSection: 0) > 0 else {
            self.totalHeightProjects = self.viewContainer.frame.origin.y
            return
        }
        
        let lastIndexPath = IndexPath(row: vcSectionProjects.tableView.numberOfRows(inSection: 0) - 1, section: 0)
        
        self.vcSectionProjects.tableView.scrollToRow(at: lastIndexPath, at: UITableViewScrollPosition.bottom, animated: false)
        
        DispatchQueue.main.async(execute: {
            let tableHeight = self.vcSectionProjects.tableView.contentSize.height
            
            self.totalHeightProjects = tableHeight + self.viewContainer.frame.origin.y
        });
    }
    
    //MARK: RELOAD RELATIONS
    func reloadRelationsTable()
    {
        vcSectionRelations.tableView.reloadData()
        
        guard vcSectionRelations.tableView.numberOfRows(inSection: 0) > 0 else {
            self.totalHeightRelations = self.viewContainer.frame.origin.y
            return
        }
        
        let lastIndexPath = IndexPath(row: vcSectionRelations.tableView.numberOfRows(inSection: 0) - 1, section: 0)
        
        self.vcSectionRelations.tableView.scrollToRow(at: lastIndexPath, at: UITableViewScrollPosition.bottom, animated: false)
        
        DispatchQueue.main.async(execute: {
            let tableHeight = self.vcSectionRelations.tableView.contentSize.height
            
            self.totalHeightRelations = tableHeight + self.viewContainer.frame.origin.y
        });
    }
    
    //MARK: SEGMENTS HEIGHT
    func setProjectsTableHeight()
    {
        constraintViewContainerHeight.constant = self.totalHeightProjects
        viewContainer.layoutIfNeeded()
        _ = onHeightChanged(self.totalHeightProjects)
    }
    
    func setRelationsTableHeight()
    {
        constraintViewContainerHeight.constant = self.totalHeightRelations
        viewContainer.layoutIfNeeded()
        _ = onHeightChanged(self.totalHeightRelations)
    }
    
    func setGeographiesTableHeight()
    {
        constraintViewContainerHeight.constant = self.totalHeightGeographies
        viewContainer.layoutIfNeeded()
        _ = onHeightChanged(self.totalHeightGeographies)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier! == "segueID_StakeholderProfileSection"
        {
            vcSectionProfile = segue.destination as! StakeholderProfileSectionTable_iPad
            vcSectionProfile.focus()
            vcSectionProfile.onHeightChanged = { (newHeight) in
                
                print("NEW HEIGHT: \(newHeight) in \(type(of: self)))")
                
                self.totalHeightProfile = self.viewContainer.frame.origin.y + newHeight
                let recommendedHeight = self.onHeightChanged(self.totalHeightProfile)
                
                if recommendedHeight > self.totalHeightProfile
                {
                    self.totalHeightProfile = recommendedHeight
                    self.constraintViewContainerHeight.constant = recommendedHeight
                }
                else
                {
                    self.constraintViewContainerHeight.constant = self.totalHeightProfile
                }
                
                self.viewContainer.layoutIfNeeded()
            }
        }
        else if segue.identifier! == "segueID_ProjectsTableView"
        {
            vcSectionProjects = segue.destination as! ProjectsTableViewController
            vcSectionProjects.isSwipeable = false
            vcSectionProjects.tableView.isScrollEnabled = false
            vcSectionProjects.tableView.bounces = false
        }
        else if segue.identifier! == "segueID_RelationsTableView"
        {
            vcSectionRelations = segue.destination as! RelationsTableViewController
            vcSectionRelations.tableView.isScrollEnabled = false
            vcSectionRelations.tableView.bounces = false
        }
        else if segue.identifier! == "segueID_GeoTableViewController"
        {
            self.vcSectionGeographies = segue.destination as! GeoTableViewController
            self.vcSectionGeographies.tableView.isScrollEnabled = false
            self.vcSectionGeographies.tableView.bounces = false
            self.vcSectionGeographies.ownerModule = .stakeholderDetail
            self.vcSectionGeographies.onDidLoadGeographies = {[weak self] items in
                
                guard items > 0 else {
                    
                    //Just enough space to show the "No data" message
                    self?.totalHeightGeographies = 44
                    return
                }
                self?.totalHeightGeographies = (self?.vcSectionGeographies.tableView.contentSize.height) ?? items * CGFloat(200)
            }
        }
    }
}
