//
//  CellStakeholderCordRepre.swift
//  SHM
//
//  Created by Manuel Salinas on 1/18/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class CellStakeholderCordRepre: UITableViewCell
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var viewCoordinator: UIView!
    @IBOutlet weak var viewRepresentative: UIView!
    @IBOutlet weak var imgCoordinator: UIImageView!
    @IBOutlet weak var lblCoordinator: UILabel!
    @IBOutlet weak var imgRepresentative: UIImageView!
    @IBOutlet weak var lblRepresentative: UILabel!
    
    
    
    @IBOutlet weak var lblCoordinatorIndicator: UILabel!
    
    @IBOutlet weak var lblRepresentativeIndicator: UILabel!
    
    var onCoordinator: ((_ coordinatorSelected: Member?) -> ())?
    var onCoordinatorEmpty:(() -> ())?
    
    var onRepresentative: ((_ representativeSelected: Member?) -> ())?
    var onRepresentativeEmpty:(() -> ())?
    

    //MARK: LIFE CYCLE
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.viewCoordinator.setBorder()
        self.viewRepresentative.setBorder()
        self.localize()
    }

    fileprivate func localize()
    {
        self.lblCoordinatorIndicator.text = "Coordinator".localized()
        self.lblRepresentativeIndicator.text = "Representative".localized()
    }
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: FILL INFORMATION
    func loadInfo(_ stakeholder: Stakeholder?, searchText: String?)
    {
        //Name
        if let search = searchText {
            
            //Coordinator
            let fullnameCoord =  stakeholder?.coordinator?.getAttributedName()
            fullnameCoord?.highlightText(search)
            self.lblCoordinator.attributedText = fullnameCoord
            
            //Representative
            let fullnameRepre =  stakeholder?.representative?.getAttributedName()
            fullnameRepre?.highlightText(search)
            self.lblRepresentative.attributedText = fullnameRepre
        }
        else
        {
            self.lblCoordinator.attributedText = stakeholder?.coordinator?.getAttributedName()
            self.lblRepresentative.attributedText = stakeholder?.representative?.getAttributedName()
//            self.lblCoordinator.text = stakeholder?.coordinator?.fullName
//            self.lblRepresentative.text = stakeholder?.representative?.fullName
        }

        //Picture
        if let thumbnailUrl = stakeholder?.coordinator?.thumbnailUrl {
            
            self.imgCoordinator.setImageWith(URL(string: thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        }
        
        if let thumbnailUrl = stakeholder?.representative?.thumbnailUrl {
            
            self.imgRepresentative.setImageWith(URL(string: thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        }
        
        
        //Gestures Actions
        self.viewCoordinator.removeGestureRecognizersOfType(ActionsTapGestureRecognizer.self)
        self.viewCoordinator.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: { [weak self] () -> () in
            
            if let stake = stakeholder, let coordina = stake.coordinator {
                
                self?.onCoordinator?(coordina)
            }
        }))
        
        self.viewRepresentative.removeGestureRecognizersOfType(ActionsTapGestureRecognizer.self)
        self.viewRepresentative.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: { [weak self] () -> () in
            
            if let stake = stakeholder, let repre = stake.representative {
                
                self?.onRepresentative?(repre)
            }
        }))
    }

    func loadInfoEditStakeholder(_ stakeholder: Stakeholder?)
    {
        //Borders
        self.viewCoordinator.setBorder()
        self.viewRepresentative.setBorder()
        
        //Name
        self.lblCoordinator.attributedText = stakeholder?.coordinator?.getAttributedName()
        self.lblRepresentative.attributedText = stakeholder?.representative?.getAttributedName()
        
        //Picture
        if let thumbnailUrl = stakeholder?.coordinator?.thumbnailUrl {
            
            self.imgCoordinator.setImageWith(URL(string: thumbnailUrl), placeholderImage: UIImage(named: "defaultperson"))
        }
        else
        {
            self.imgCoordinator.image = UIImage(named: "defaultperson")
        }
        
        if let thumbnailUrl = stakeholder?.representative?.thumbnailUrl {
            
            self.imgRepresentative.setImageWith(URL(string: thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        }
        else
        {
            self.imgRepresentative.image = UIImage(named: "defaultperson")
        }
        
        
        //Gestures Actions
        self.viewCoordinator.removeGestureRecognizersOfType(ActionsTapGestureRecognizer.self)
        self.viewCoordinator.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: { [weak self] () -> () in
            
            self?.onCoordinatorEmpty?()
            
            }))
        
        self.viewRepresentative.removeGestureRecognizersOfType(ActionsTapGestureRecognizer.self)
        self.viewRepresentative.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: { [weak self] () -> () in
            
            self?.onRepresentativeEmpty?()
            
            }))
    }
}

