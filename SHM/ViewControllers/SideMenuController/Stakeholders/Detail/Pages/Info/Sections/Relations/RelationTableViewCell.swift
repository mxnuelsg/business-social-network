//
//  RelationTableViewCell.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 9/3/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class RelationTableViewCell: UITableViewCell
{
    //MARK: PROPERTIES & OUTLET
    @IBOutlet weak var imgRelationBadge: UIImageView!
    @IBOutlet weak var imgThumbnail: UIImageView!
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var lblJobTitle: UILabel!
    @IBOutlet weak var lblRelation: UILabel!
    @IBOutlet weak var lblUserRating: UILabel!
    @IBOutlet weak var lblAdminRating: UILabel!
    @IBOutlet weak var ratingViewContainer: UIView!
    var didTapOnRating:(()->())?
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.lblFullName.text = String()
        self.lblJobTitle.text = String()
        self.lblRelation.text = String()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

    func setupContactRelation(_ relation: ContactRelation)
    {
        //Thumbnail
        self.imgThumbnail.setImageWith(URL(string: relation.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        
        let companySuffix = relation.companyName.isEmpty ? "" : " / \(relation.companyName)"
        let strJobPosition = relation.jobPosition?.value ?? ""
        let attributedPosition = NSMutableAttributedString().setJobPositionCompany(strJobPosition, company: companySuffix)

        
        //Image Card
        if relation.isStakeholder == false
        {
            self.imgRelationBadge.image = UIImage(named: "IconIDBlue")
        }
        else
        {
            self.imgRelationBadge.image = nil
        }
        
        //Information
        self.lblJobTitle.attributedText = attributedPosition
        self.lblFullName.text = relation.fullName
        self.lblRelation.text = "\(relation.relationshipLevel1.value) - \(relation.relationshipLevel2.value)"
    }
    
    func setupStakeholdertRelation(_ relation: Stakeholder, isEnglish: Bool)
    {
        //Thumbnail
        self.imgThumbnail.setImageWith(URL(string: relation.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        
        let companySuffix = relation.companyName.isEmpty ? "" : " / \(relation.companyName)"
        let strJobPosition = isEnglish == true ? relation.jobPosition!.valueEn : relation.jobPosition!.valueEs
        let attributedPosition = NSMutableAttributedString().setJobPositionCompany(strJobPosition, company: companySuffix)
        
        
        //Image Card
        if relation.isStakeholder == false
        {
            self.imgRelationBadge.image = UIImage(named: "IconIDBlue")
        }
        else
        {
            self.imgRelationBadge.image = nil
        }
        
        //Information
        self.lblJobTitle.attributedText = attributedPosition
        self.lblFullName.text = relation.fullName
        self.lblRelation.text = "\(isEnglish == true ? relation.relationType1!.valueEn : relation.relationType1!.valueEs) - \(isEnglish == true ? relation.relationType2!.valueEn : relation.relationType2!.valueEs)"
    }
    
    func setupRating(_ stakeholder: Stakeholder)
    {
        self.lblUserRating.text = (stakeholder.userRating ?? 0).getString()
        self.lblAdminRating.text = (stakeholder.adminRating ?? 0).getString()
        self.ratingViewContainer.isUserInteractionEnabled = true
        self.ratingViewContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapOnRating)))
    }
    
    func tapOnRating()
    {
        self.didTapOnRating?()
    }
}
