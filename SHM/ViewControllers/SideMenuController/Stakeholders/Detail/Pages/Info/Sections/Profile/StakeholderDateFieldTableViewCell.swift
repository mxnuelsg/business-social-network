//
//  StakeholderDateFieldTableViewCell.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/27/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class StakeholderDateFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
