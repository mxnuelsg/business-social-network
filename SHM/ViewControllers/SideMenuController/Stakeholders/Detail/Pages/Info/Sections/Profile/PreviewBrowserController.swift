//
//  PreviewBrowserController.swift
//  SHM
//
//  Created by Manuel Salinas on 6/7/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class PreviewBrowserController: UIViewController
{
    //MARK: PROPERTIES & OUTLET
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak fileprivate var browser: UIWebView!
    @IBOutlet weak fileprivate var btnClose: UIButton!
    @IBOutlet weak fileprivate var btnOpen: UIButton!
    
    var url = URL(string: "http://shm.definityfirst.com")
    var onClosed : (() -> ())?
    var onOpen : ((_ url: URL) -> ())?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        
        //Open Web
        if let web = self.url {
            
            self.openWeb(web)
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        print("Deinit: PreviewBrowserController")
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        self.lblTitle.text = "Preview".localized()
        self.btnClose.setTitle("Close".localized(), for: .normal)
        self.btnOpen.setTitle("Open".localized(), for: .normal)
        
        self.btnClose.setBorder()
        self.btnOpen.setBorder()
        self.browser.setBorder()
    }

    //MARK: ACTIONS
    fileprivate func openWeb(_ url: URL)
    {
        let request = URLRequest(url: url)
        self.browser.loadRequest(request)
    }
    
    @IBAction fileprivate func close()
    {
        self.dismiss(animated: true, completion: nil)
        self.onClosed?()
    }
    
    @IBAction fileprivate func open()
    {
        self.dismiss(animated: true) {
            
            //Open Web
            if let web = self.url {
                
                self.onOpen?(web)
            }
        }
        
    }
    
}
