//
//  CellStakeholderInfo.swift
//  SHM
//
//  Created by Manuel Salinas on 1/18/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class CellStakeholderInfo: UITableViewCell
{
    //MARK:PROPERTIES & OUTLETS
    @IBOutlet weak var lblAlias: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblPlaceOfBirth: UILabel!
    @IBOutlet weak var lblBirthday: UILabel!
    @IBOutlet weak var lblMaritalStatus: UILabel!
    @IBOutlet weak var lblOccupation: UILabel!
    @IBOutlet weak var lblNationality: UILabel!
    @IBOutlet weak var lblLastUpdate: UILabel!
    @IBOutlet weak var lblCharacters: UILabel!
    
    //For Company
    @IBOutlet weak var lblBirthOrCreationDate: UILabel!
    @IBOutlet weak var lblMaritalOrBusiness: UILabel!
    @IBOutlet weak var lblPlaceOfBirthIndicator: UILabel!
    @IBOutlet weak var lblOccupationIndicator: UILabel!
    @IBOutlet weak var lblCharactersIndicator: UILabel!
    @IBOutlet weak var lblLastUpdateIndicator: UILabel!
    @IBOutlet weak var lblNationalityIndicator: UILabel!
    @IBOutlet weak var lblStatusTitle: UILabel!
    
    //MARK: LYFE CYCLE
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.setBorder()
        self.loadConfig()
    }

    private func loadConfig()
    {
        self.lblOccupationIndicator.text = "Occupation".localized()
        self.lblCharactersIndicator.text = "Characters".localized()
        self.lblLastUpdateIndicator.text = "Last Update".localized()
        self.lblNationalityIndicator.text = "Nationality".localized()
        self.lblStatusTitle.text = "Status".localized()
        
        self.lblBirthOrCreationDate.adjustsFontSizeToFitWidth = true
    }
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: FILL INFORMATION
    func loadInfo(_ stakeholder: Stakeholder?, _ isEnglish: Bool, _ searchText: String?)
    {
        if let search = searchText {
            
            if stakeholder?.isCompany == true
            {
                //Labels
                self.lblPlaceOfBirthIndicator.text = "Know address".localized()
                self.lblBirthOrCreationDate.text = "Date of foundation".localized()
                self.lblMaritalOrBusiness.text = "Industry/Sector".localized()

                //Business Type
                let businessType =  stakeholder?.industrySector.value.mutableAttributed
                businessType?.highlightText(search)
                self.lblMaritalStatus.attributedText = businessType
                
                //Occupation
                let occupation =  "N/A".mutableAttributed
                occupation.highlightText(search)
                self.lblOccupation.attributedText = occupation
                
                //Nationality
                let nation =  "N/A".mutableAttributed
                nation.highlightText(search)
                self.lblNationality.attributedText = occupation
                
                //Alias
                let aliases =  "N/A".mutableAttributed
                aliases.highlightText(search)
                self.lblAlias.attributedText = occupation
            }
            else
            {
                //Labels
                self.lblPlaceOfBirthIndicator.text = "Place of Birth".localized()
                self.lblBirthOrCreationDate.text = "Birthday (Age)".localized()
                self.lblMaritalOrBusiness.text = "Marital Status".localized()
                
                
                //Marital Status
                let marital =  stakeholder?.maritalStatus?.value.mutableAttributed
                marital?.highlightText(search)
                self.lblMaritalStatus.attributedText = marital
                
                //Occupation
                let occupation =  stakeholder?.occupation?.value.mutableAttributed
                occupation?.highlightText(search)
                self.lblOccupation.attributedText = occupation
                
                //Nationality
                let nationality =  stakeholder?.nationalities.collapseValues().mutableAttributed
                nationality?.highlightText(search)
                self.lblNationality.attributedText = nationality
                
                //Alias
                let alias = stakeholder?.aliasString.mutableAttributed
                alias?.highlightText(search)
                self.lblAlias.attributedText = alias
            }
            
            //Birthday
            let birthday = (stakeholder?.birthday?.getStringStyleMedium() ?? stakeholder?.birthDateString)?.mutableAttributed
            birthday?.highlightText(search)
            self.lblBirthday.attributedText = birthday

            //Place of Birth
            let placeBirth =  stakeholder?.placeOfBirth.mutableAttributed
            placeBirth?.highlightText(search)
            self.lblPlaceOfBirth.attributedText = placeBirth
            
            //Last Update
            let lastUpdate =  stakeholder?.modifiedDate?.getStringStyleMedium().mutableAttributed
            lastUpdate?.highlightText(search)
            self.lblLastUpdate.attributedText = lastUpdate
            
            //Status
            let status = (stakeholder?.isEnable == true) ? "Enabled".localized().mutableAttributed : "Disabled".localized().mutableAttributed
            status.highlightText(search)
            self.lblStatus.attributedText = status
        }
        else
        {
            if stakeholder?.isCompany == true
            {
                //Labels
                self.lblPlaceOfBirthIndicator.text = "Know address".localized()
                self.lblBirthOrCreationDate.text = "Date of foundation".localized()
                self.lblMaritalOrBusiness.text = "Industry/Sector".localized()

                //Values
                self.lblMaritalStatus.text = stakeholder?.industrySector.value ?? "No Registered".localized()
                self.lblOccupation.text = "N/A".localized()
                self.lblNationality.text = "N/A".localized()
                self.lblAlias.text = "N/A".localized()
            }
            else
            {
                //Labels
                self.lblPlaceOfBirthIndicator.text = "Place of Birth".localized()
                self.lblBirthOrCreationDate.text = "Birthday (Age)".localized()
                self.lblMaritalOrBusiness.text = "Marital Status".localized()
                
                //Values
                self.lblMaritalStatus.text = stakeholder?.maritalStatus?.value ?? "No Registered".localized()
                let strOccupation = stakeholder?.occupation?.value ?? ""
                self.lblOccupation.text = strOccupation
                self.lblNationality.text = stakeholder?.nationalities.collapseValues() ?? "No Registered".localized()
                self.lblAlias.text = stakeholder?.aliasString ?? "No Registered".localized()
            }
            
            self.lblStatus.text = (stakeholder?.isEnable == true) ? "Enabled".localized() : "Disabled".localized()
            self.lblBirthday.text = stakeholder?.birthday?.getStringStyleMedium() ?? stakeholder?.birthDateString ?? "No Registered".localized()
            self.lblPlaceOfBirth.text = stakeholder?.placeOfBirth ?? "No Registered".localized()
            self.lblLastUpdate.text = stakeholder?.modifiedDate?.getStringStyleMedium() ?? "No Registered".localized()
        }
        
        //Language characters
        let languageChars = NSMutableAttributedString(string: "")
        
        guard let stake = stakeholder else {
            
            self.lblCharacters.text = "Eng (∙∙∙) / Esp (∙∙∙)"
            return
        }
        
        let engChars = "Eng \(stake.charactersEng)"
        let espChars = "Esp \(stake.charactersEsp)"
        
        languageChars.append(isEnglish == true ? engChars.bold(15) : NSAttributedString(string: engChars))
        languageChars.append(NSAttributedString(string: " / "))
        languageChars.append(isEnglish == false ? espChars.bold(15) : NSAttributedString(string: espChars))

        self.lblCharacters.attributedText = languageChars
    }
}
