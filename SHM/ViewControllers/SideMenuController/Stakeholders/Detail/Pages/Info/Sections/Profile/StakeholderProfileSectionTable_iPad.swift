//
//  StakeholderProfileSectionTable_iPad.swift
//  SHM
//
//  Created by Manuel Salinas on 1/18/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import SafariServices

//Sections
enum ProfileSection: Int
{
    case coordinatorRepresentative = 0
    case info = 1
    case summary = 2
    case personalInfo = 3
    case professionalInfo = 4
    case generalInfo = 5
    case additionalFields = 6
    case relevantDates = 7
    case professionalTimeline = 8
    case references = 9
    case associatedWhos = 10
}

class StakeholderProfileSectionTable_iPad: UITableViewController
{
    //MARK: PROPERTIES & OUTLETS
    var arraySections = [ "", "", "Summary".localized(), "Personal Information".localized(), "Professional".localized(), "General Information".localized(), "Additional Fields".localized(), "Relevant Dates".localized(), "Professional Timeline".localized(),  "References".localized(), "Associated Whos".localized()]
    
    var stakeholder = Stakeholder(isDefault: true) {
        didSet {
            
            self.tableView.reloadData()
        }
    }
    
    var isEnglish: Bool = true {
        didSet {
            
            self.tableView.reloadData()
        }
    }
    
    var searchText: String?
    
    var highlightCount = 0 {
        didSet {
            
            print(highlightCount)
        }
    }
    
    var onHeightChanged: ((_ newHeight: CGFloat) -> ())?
    fileprivate(set) internal var hasFocus = false
    var totalHeight: CGFloat = 1000
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //Show or hide Associated Whos depending role
        if LibraryAPI.shared.currentUser?.role != .globalManager && LibraryAPI.shared.currentUser?.role != .localManager
        {
            self.arraySections.removeLast()
        }
        
        //No visible Scroll
        self.tableView.showsVerticalScrollIndicator = false
        
        //Dynamic size
        self.tableView.estimatedRowHeight = 60.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        //Register Custom Cells
        self.tableView.register(UINib(nibName: "CellStakeholderCordRepre", bundle: nil), forCellReuseIdentifier: "CellStakeholderCordRepre")
        self.tableView.register(UINib(nibName: "CellStakeholderInfo", bundle: nil), forCellReuseIdentifier: "CellStakeholderInfo")
        self.tableView.register(UINib(nibName: "TextViewTableCell", bundle: nil), forCellReuseIdentifier: "TextViewTableCell")
        self.tableView.register(UINib(nibName: "CellStakeholderDates", bundle: nil), forCellReuseIdentifier: "CellStakeholderDates")
    }
    
    override func viewDidLayoutSubviews()
    {
//        //viewDidLayoutSubviews must be override only in iPhone
//        if DeviceType.IS_ANY_IPAD == false
//        {
//            super.viewDidLayoutSubviews()
//
//            DispatchQueue.main.async(execute: {
//
//                self.totalHeight = self.tableView.contentSize.height
//
//                self.tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
//
//                if self.hasFocus == true
//                {
//                    self.onHeightChanged?(self.totalHeight)
//                }
//            })
//        }
    }
    
    //MARK: SEARCH TEXT
    func searchText(_ text: String)
    {
        self.searchText = (text.isEmpty == true) ? "" : text
        self.tableView.reloadData()
    }
    
    // MARK: ACTIONS
    func createNewPost()
    {
        let vcNewPost = NewPostModal()
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = .formSheet
        
        self.present(navController, animated: true, completion: nil)
    }
    
    fileprivate func openSafari(_ url: URL)
    {
        //Open Safari
        let vcSafari = SFSafariViewController(url: url, entersReaderIfAvailable: true)
        vcSafari.delegate = self
        vcSafari.title = "References".localized()
        
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        vcSafari.navigationItem.setRightBarButtonItems([barBtnCompose], animated: true)
        
        self.navigationController?.pushViewController(vcSafari, animated: true)
    }
    
    //MARK: FUNCTIONS
    func tappedUrl(_ sender: UITapGestureRecognizer)
    {
        let touch = sender.location(in: self.tableView)
        
        if let indexPath = tableView.indexPathForRow(at: touch){
            
            if self.isEnglish == true
            {
                guard let url = URL(string: self.stakeholder.references[indexPath.row].value)  else {
                    
                    return
                }
                
                
                let vcPreview: PreviewBrowserController =  Storyboard.getInstanceFromStoryboard("popups")
                vcPreview.url = url
                vcPreview.onOpen = { [weak self] url in
                    
                    //Open Browser
                    self?.openSafari(url)
                }
                
                let vcModal = ModalViewController(subViewController: vcPreview)
                self.present(vcModal, animated:true, completion: nil)
            }
            else
            {
                guard let url = URL(string: self.stakeholder.referencesEsp[indexPath.row].value)  else {
                    
                    return
                }
                
                let vcPreview: PreviewBrowserController =  Storyboard.getInstanceFromStoryboard("popups")
                vcPreview.url = url
                vcPreview.onOpen = { [weak self] url in
                    
                    //Open Browser
                    self?.openSafari(url)
                }
                
                let vcModal = ModalViewController(subViewController: vcPreview)
                self.present(vcModal, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: METHODS FOR STAKEHOLDER DETAIL IPHONE
    @discardableResult func focus() -> CGFloat
    {
        hasFocus = true
        onHeightChanged?(totalHeight)
        
        return totalHeight
    }
    
    func unfocus()
    {
        hasFocus = false
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.arraySections.count
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let lblSectionTitle = SHMSectionTitleLabel()
        lblSectionTitle.text = self.arraySections[section]
        lblSectionTitle.backgroundColor = UIColor.whiteColor(0.95)
        lblSectionTitle.textColor = UIColor.grayCloudy()
        lblSectionTitle.font = UIFont.boldSystemFont(ofSize: 12)
        
        return lblSectionTitle
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        switch section
        {
        case ProfileSection.coordinatorRepresentative.rawValue, ProfileSection.info.rawValue:
            
            return 1
            
        case ProfileSection.additionalFields.rawValue:

            let additionals = self.isEnglish == true ? self.stakeholder.additionalFields.filter({$0.language == "en"}) : self.stakeholder.additionalFields.filter({$0.language == "es"})
            return additionals.count
            
        case ProfileSection.relevantDates.rawValue:
            
            let importantDates = self.isEnglish == true ? self.stakeholder.relevantDates.filter({$0.language == "en"}) : self.stakeholder.relevantDates.filter({$0.language == "es"})
            return importantDates.count

        case ProfileSection.professionalTimeline.rawValue:
            
            let timelines = self.isEnglish == true ? self.stakeholder.professionalTimeline.filter({$0.language == "en"}) : self.stakeholder.professionalTimeline.filter({$0.language == "es"})
            return timelines.count

        case ProfileSection.references.rawValue:
            
            return self.isEnglish == true ? self.stakeholder.references.count : self.stakeholder.referencesEsp.count
            
        case ProfileSection.associatedWhos.rawValue:
            
            return self.stakeholder.whos.count
            
        default:
            return self.stakeholder.isVisiblePrivateFields == true ? 2 : 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch indexPath.section
        {
        case ProfileSection.coordinatorRepresentative.rawValue:
            
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CellStakeholderCordRepre") as! CellStakeholderCordRepre
            cell.selectionStyle = .none
            
            cell.loadInfo(self.stakeholder, searchText: self.searchText)
            cell.onCoordinator = { [weak self] coordinatorSelected in
                
                
                let vcProfile = DeviceType.IS_ANY_IPAD == true ? Storyboard.getInstanceOf(ProfileViewController_iPad.self) : Storyboard.getInstanceOf(ProfileViewController.self)
                vcProfile.member = coordinatorSelected
                
                self?.navigationController?.pushViewController(vcProfile, animated: true)
            }
            
            cell.onRepresentative = { [weak self] representativeSelected in
                
                let vcProfile = DeviceType.IS_ANY_IPAD == true ? Storyboard.getInstanceOf(ProfileViewController_iPad.self) : Storyboard.getInstanceOf(ProfileViewController.self)
                vcProfile.member = representativeSelected
                
                self?.navigationController?.pushViewController(vcProfile, animated: true)
            }
            
            return cell
            
        case ProfileSection.info.rawValue:
            
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CellStakeholderInfo") as! CellStakeholderInfo
            cell.selectionStyle = .none
            
            cell.loadInfo(self.stakeholder, self.isEnglish, self.searchText)
            
            return cell
            
        case ProfileSection.summary.rawValue, ProfileSection.personalInfo.rawValue, ProfileSection.professionalInfo.rawValue, ProfileSection.generalInfo.rawValue:
            
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "TextViewTableCell") as! TextViewTableCell
            cell.selectionStyle = .none
            
            switch indexPath.section
            {
            case ProfileSection.summary.rawValue:
                //Summary
                if self.isEnglish == true
                {
                    if let search = searchText {
                        
                        let summary =  indexPath.row == 0 ? self.stakeholder.summaryPublic.htmlMutableAttributedString : self.stakeholder.summaryPrivate.htmlMutableAttributedString
                        summary.highlightText(search)
                        
                        cell.tvText.attributedText = summary
                    }
                    else
                    {
                        cell.tvText.attributedText = indexPath.row == 0 ? self.stakeholder.summaryPublic.htmlMutableAttributedString : self.stakeholder.summaryPrivate.htmlMutableAttributedString
                    }
                }
                else
                {
                    if let search = searchText {
                        
                        let summaryES =  indexPath.row == 0 ? self.stakeholder.summaryPublicEsp.htmlMutableAttributedString : self.stakeholder.summaryPrivateEsp.htmlMutableAttributedString
                        summaryES.highlightText(search)
                        
                        cell.tvText.attributedText = summaryES
                    }
                    else
                    {
                        cell.tvText.attributedText = indexPath.row == 0 ? self.stakeholder.summaryPublicEsp.htmlMutableAttributedString : self.stakeholder.summaryPrivateEsp.htmlMutableAttributedString
                    }
                }
                
                cell.tvText.textColor = indexPath.row == 1 ? UIColor.red : UIColor.black
                
            case ProfileSection.personalInfo.rawValue:
                //Personal Information
                if self.isEnglish == true
                {
                    if let search = searchText {
                        
                        let info = indexPath.row == 0 ? self.stakeholder.personalInformationPublic.htmlMutableAttributedString : self.stakeholder.personalInformationPrivate.htmlMutableAttributedString
                        info.highlightText(search)
                        
                        cell.tvText.attributedText = info
                    }
                    else
                    {
                        cell.tvText.attributedText = indexPath.row == 0 ? self.stakeholder.personalInformationPublic.htmlMutableAttributedString : self.stakeholder.personalInformationPrivate.htmlMutableAttributedString
                    }
                }
                else
                {
                    if let search = searchText {
                        
                        let infoES = indexPath.row == 0 ? self.stakeholder.personalInformationPublicEsp.htmlMutableAttributedString : self.stakeholder.personalInformationPrivateEsp.htmlMutableAttributedString
                        infoES.highlightText(search)
                        
                        cell.tvText.attributedText = infoES
                    }
                    else
                    {
                        cell.tvText.attributedText = indexPath.row == 0 ? self.stakeholder.personalInformationPublicEsp.htmlMutableAttributedString : self.stakeholder.personalInformationPrivateEsp.htmlMutableAttributedString
                    }
                }
                
                cell.tvText.textColor = indexPath.row == 1 ? UIColor.red : UIColor.black
                
            case ProfileSection.professionalInfo.rawValue:
               
                //Professional
                if self.isEnglish == true
                {
                    if let search = searchText {
                        
                        let profesional = indexPath.row == 0 ? self.stakeholder.professionalPublic.htmlMutableAttributedString : self.stakeholder.professionalPrivate.htmlMutableAttributedString
                        profesional.highlightText(search)
                        
                        cell.tvText.attributedText = profesional
                    }
                    else
                    {
                        cell.tvText.attributedText = indexPath.row == 0 ? self.stakeholder.professionalPublic.htmlMutableAttributedString : self.stakeholder.professionalPrivate.htmlMutableAttributedString
                    }
                }
                else
                {
                    if let search = searchText {
                        
                        let profesionalES = indexPath.row == 0 ? self.stakeholder.professionalPublicEsp.htmlMutableAttributedString : self.stakeholder.professionalPrivateEsp.htmlMutableAttributedString
                        profesionalES.highlightText(search)
                        
                        cell.tvText.attributedText = profesionalES
                    }
                    else
                    {
                        cell.tvText.attributedText = indexPath.row == 0 ? self.stakeholder.professionalPublicEsp.htmlMutableAttributedString : self.stakeholder.professionalPrivateEsp.htmlMutableAttributedString
                    }
                }
                
                cell.tvText.textColor = indexPath.row == 1 ? UIColor.red : UIColor.black
                
            case ProfileSection.generalInfo.rawValue:
                
                //General Information
                if self.isEnglish == true
                {
                    if let search = searchText {
                        
                        let general = indexPath.row == 0 ? self.stakeholder.generalInformationPublic.htmlMutableAttributedString : self.stakeholder.generalInformationPrivate.htmlMutableAttributedString
                        general.highlightText(search)
                        
                        cell.tvText.attributedText = general
                    }
                    else
                    {
                        cell.tvText.attributedText = indexPath.row == 0 ? self.stakeholder.generalInformationPublic.htmlMutableAttributedString : self.stakeholder.generalInformationPrivate.htmlMutableAttributedString
                    }
                }
                else
                {
                    if let search = searchText {
                        
                        let generalES = indexPath.row == 0 ? self.stakeholder.generalInformationPublicEsp.htmlMutableAttributedString : self.stakeholder.generalInformationPrivateEsp.htmlMutableAttributedString
                        generalES.highlightText(search)
                        
                        cell.tvText.attributedText = generalES
                    }
                    else
                    {
                        cell.tvText.attributedText = indexPath.row == 0 ? self.stakeholder.generalInformationPublicEsp.htmlMutableAttributedString : self.stakeholder.generalInformationPrivateEsp.htmlMutableAttributedString
                    }
                }
                
                cell.tvText.textColor = indexPath.row == 1 ? UIColor.red : UIColor.black
                
            default:
                break
            }
            
            return cell
         
        case ProfileSection.additionalFields.rawValue:
            
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "TextViewTableCell") as! TextViewTableCell
            cell.selectionStyle = .none
            
            let additionals = self.isEnglish == true ? self.stakeholder.additionalFields.filter({$0.language == "en"}) : self.stakeholder.additionalFields.filter({$0.language == "es"})
            
            //Fill data
            let field = additionals[indexPath.row]
            let text = "<b>\(field.key)</b> <br><br> \(field.value)"
            
            //Searching active
            if let search = searchText {
                
                let info = text.htmlMutableAttributedString
                info.highlightText(search)
                
                cell.tvText.attributedText = info
            }
            else
            {
                cell.tvText.attributedText = text.htmlMutableAttributedString
            }
            
            return cell
            
        case ProfileSection.relevantDates.rawValue:
            
            //Relevant Dates
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CellStakeholderDates") as! CellStakeholderDates
            cell.selectionStyle = .none
            
            let importantDates = self.isEnglish == true ? self.stakeholder.relevantDates.filter({$0.language == "en"}) : self.stakeholder.relevantDates.filter({$0.language == "es"})
            
            //Fill data & Search
            cell.loadInfo(importantDates[indexPath.row], searchText: self.searchText)
            
            return cell
            
        case ProfileSection.professionalTimeline.rawValue:
            
            //Timeline
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CellStakeholderDates") as! CellStakeholderDates
            cell.selectionStyle = .none
            
            let timelines = self.isEnglish == true ? self.stakeholder.professionalTimeline.filter({$0.language == "en"}) : self.stakeholder.professionalTimeline.filter({$0.language == "es"})
            cell.loadInfo(timelines[indexPath.row], searchText: self.searchText)
            
            return cell
            
        case ProfileSection.references.rawValue:
            
            //References
            let cell:UITableViewCell = UITableViewCell(style: .value2, reuseIdentifier:"Cell")
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            cell.textLabel?.textColor = UIColor.colorForNavigationController()
            cell.textLabel?.textAlignment = .left
            cell.detailTextLabel?.textAlignment = .left
            
            //Selected Color
            let selectedColor = UIView()
            selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
            cell.selectedBackgroundView = selectedColor
            
            
            var order = 0
            var value = String()
            
            if self.isEnglish == true
            {
                order = self.stakeholder.references[indexPath.row].ordinalIndicator
                value = self.stakeholder.references[indexPath.row].value
            }
            else
            {
                order = self.stakeholder.referencesEsp[indexPath.row].ordinalIndicator
                value = self.stakeholder.referencesEsp[indexPath.row].value
                
            }
            
            //Check if is url
            if let url = URL(string: value), let scheme = url.scheme {
                
                if scheme.lowercased() == "http" || scheme.lowercased() == "https"
                {
                    cell.detailTextLabel?.textColor = UIColor.blueTag()
                    
                    //Add gesture
                    cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tappedUrl(_:))))
                }
            }
            
            if let search = searchText {
                
                let reference = value.mutableAttributed
                reference.highlightText(search)
                
                cell.textLabel?.text = "\(order)"
                cell.detailTextLabel?.attributedText = reference
            }
            else
            {
                cell.textLabel?.text = "\(order)"
                cell.detailTextLabel?.text = value
            }
            
            return cell
            
            case ProfileSection.associatedWhos.rawValue:
                
                let cell = UITableViewCell(style:.default, reuseIdentifier:"Cell")
                cell.textLabel?.numberOfLines = 0
                cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
                cell.selectionStyle = .none
                
                //Info
                let who1 = self.stakeholder.whos[indexPath.row]

                let fisrtTitle = "● \(who1.name)\n"
                var secondTitle = String()
                var thirTitle = String()

                for who2 in who1.whoLevel_II
                {
                    secondTitle = secondTitle + "   ▪︎ \(who2.name)\n"
                    
                    for who3 in who2.whoLocal
                    {
                        thirTitle = thirTitle + "        ・ \(who3.name)\n"
                    }
                }
                
                let finalText =  fisrtTitle + secondTitle + thirTitle
                
                /** Style Text */
                let attributedString = NSMutableAttributedString(string: finalText)
                attributedString.setBold(fisrtTitle, size: 14)
                
                
                //Search Format or Normal Format
                if let search = searchText {
                    
                    let reference = finalText.mutableAttributed
                    reference.highlightText(search)
                    
                    cell.textLabel?.attributedText = reference
                }
                else
                {
                    cell.textLabel?.attributedText = attributedString
                }
                
                return cell
            
        default:
            return UITableViewCell()
        }
    }
    
     //MARK: Table View Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return (section == ProfileSection.coordinatorRepresentative.rawValue || section == ProfileSection.info.rawValue) ? CGFloat.leastNormalMagnitude : 15
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return section == 0 ? CGFloat.leastNormalMagnitude : 15

    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        //THIS VALIDATIONS ARE TO HANDLE THE HEIGHT OF THE CELL IN CASE THERE'S NO INFORMATION
        if indexPath.section == ProfileSection.summary.rawValue
            || indexPath.section == ProfileSection.personalInfo.rawValue
            || indexPath.section == ProfileSection.professionalInfo.rawValue
            || indexPath.section == ProfileSection.generalInfo.rawValue
        {
            switch indexPath.section
            {
            case ProfileSection.summary.rawValue:
                
                //Summary
                if self.isEnglish == true
                {
                    let text = indexPath.row == 0 ? self.stakeholder.summaryPublic : self.stakeholder.summaryPrivate
                    return self.getRowHeightByText(text)
                }
                else
                {
                    let text = indexPath.row == 0 ? self.stakeholder.summaryPublicEsp : self.stakeholder.summaryPrivateEsp
                    return self.getRowHeightByText(text)
                }
                
            case ProfileSection.personalInfo.rawValue:
                
                //Personal Information
                if self.isEnglish == true
                {
                    let text = indexPath.row == 0 ? self.stakeholder.personalInformationPublic : self.stakeholder.personalInformationPrivate
                    return self.getRowHeightByText(text)
                }
                else
                {
                    let text = indexPath.row == 0 ? self.stakeholder.personalInformationPublicEsp : self.stakeholder.personalInformationPrivateEsp
                    return self.getRowHeightByText(text)
                }
                
            case ProfileSection.professionalInfo.rawValue:
                
                //Professional
                if self.isEnglish == true
                {
                    let text = indexPath.row == 0 ? self.stakeholder.professionalPublic : self.stakeholder.professionalPrivate
                    return self.getRowHeightByText(text)
                }
                else
                {
                    let text = indexPath.row == 0 ? self.stakeholder.professionalPublicEsp : self.stakeholder.professionalPrivateEsp
                    return self.getRowHeightByText(text)
                }
                
            case ProfileSection.generalInfo.rawValue:
                
                //General Information
                if self.isEnglish == true
                {
                    let text = indexPath.row == 0 ? self.stakeholder.generalInformationPublic : self.stakeholder.generalInformationPrivate
                    return self.getRowHeightByText(text)
                }
                else
                {
                    let text = indexPath.row == 0 ? self.stakeholder.generalInformationPublicEsp : self.stakeholder.generalInformationPrivateEsp
                    return self.getRowHeightByText(text)
                }
                
            default:
                return CGFloat.leastNormalMagnitude
            }
        }
        else
        {
            return self.tableView.rowHeight
        }
    }
    
    //MARK: ROW HEIGHT DEPENDING CONTENT
    func getRowHeightByText(_ text: String) -> CGFloat
    {
        if text.trim().isEmpty == true
        {
            return CGFloat.leastNormalMagnitude
        }
        else
        {
            return self.tableView.rowHeight
        }
    }
}

//MARK: EXTENSION WEB BROWSER
extension StakeholderProfileSectionTable_iPad: SFSafariViewControllerDelegate
{
    //MARK DELEGATE SAFARI
    func safariViewControllerDidFinish(_ controller: SFSafariViewController)
    {
        MessageManager.shared.hideHUD()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func safariViewController(_ controller: SFSafariViewController, didCompleteInitialLoad didLoadSuccessfully: Bool)
    {
        MessageManager.shared.hideHUD()
        
        if didLoadSuccessfully == false
        {
            MessageManager.shared.showBar(title: "Error",
                                          subtitle: "Error.TryAgain".localized(),
                                          type: .error,
                                          fromBottom: false)
            
            controller.dismiss(animated: true, completion: nil)
            return
        }
    }
}
