//
//  RelationsTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 9/2/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class RelationsTableViewController: UITableViewController {
    var relations: [ContactRelation] = [] {
        didSet {
            if let searchText = self.searchText {
                relationsFiltered = LibraryAPI.shared.stakeholderBO.filterRelations(relations, byText: searchText)
            }
            else {
                relationsFiltered = relations
            }
        }
    }
    var relationsFiltered: [ContactRelation] = []
    var searchText: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.hideEmtpyCells()
    }
    
    func filterByText(_ text: String) {
        guard text.isEmpty == false else {
            if searchText != nil {
                relationsFiltered = relations
                searchText = nil
                tableView.reloadData()
            }
            return
        }
        relationsFiltered = LibraryAPI.shared.stakeholderBO.filterRelations(relations, byText: text)
        searchText = text.isEmpty ? nil : text
        tableView.reloadData()
    }
}

// MARK: UITableViewDataSource

extension RelationsTableViewController
{
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //Background Message
        if relationsFiltered.count > 0
        {
            tableView.dismissBackgroundMessage()
        }
        else
        {
            //tableView.displayBackgroundMessage("No Relations".localized(), subMessage: "")
        }
        
        return relationsFiltered.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RelationCell") as! RelationTableViewCell
        cell.selectionStyle = .none
        
        cell.setupContactRelation(relationsFiltered[indexPath.row])
        
        return cell
    }
}

// MARK: UITableViewDelegate

extension RelationsTableViewController {
    
}
