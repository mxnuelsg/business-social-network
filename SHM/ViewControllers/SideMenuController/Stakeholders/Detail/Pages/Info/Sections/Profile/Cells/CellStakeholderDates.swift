//
//  CellStakeholderDates.swift
//  SHM
//
//  Created by Manuel Salinas on 1/18/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class CellStakeholderDates: UITableViewCell
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!

    //MARK: LIFE CYCLE
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: FILL INFORMATION
    func loadInfo(_ event: RelevantDate?, searchText: String?)
    {
        if let search = searchText {
            
            //Date
            let date =  (event?.isFreeDateString == true) ? event?.stringValue?.mutableAttributed : event?.date?.getStringStyleMedium().mutableAttributed
            date?.highlightText(search)
            self.lblDate.attributedText = date
            
            //Description
            let description =  event?.title.mutableAttributed
            description?.highlightText(search)
            self.lblDescription.attributedText = description
        }
        else
        {
            self.lblDate.text = (event?.isFreeDateString == true) ? event?.stringValue : event?.date?.getStringStyleMedium()
            self.lblDescription.text = event?.title
        }
    }
    
    //MARK: FILL INFORMATION
    func loadInfo(_ event: RelevantDate?)
    {
            self.lblDate.text = (event?.isFreeDateString == true) ? event?.stringValue : event?.date?.getStringStyleMedium()
            self.lblDescription.text = event?.title
    }
}
