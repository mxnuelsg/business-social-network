//
//  ActorDetailViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/10/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

private enum TabBarOptions: Int
{
    case newPost = 0
    case updateRequest = 1
    case changeLanguage = 2
    case search = 3
    case calendars = 4
}

class StakeholderDetailViewController: UIViewController
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet var scrollMain: UIScrollView!
    @IBOutlet weak var imgPersonBanner: UIImageView!
    @IBOutlet weak var imgPersonThumbnail: UIImageView!
    @IBOutlet weak var lblActorTitle: UILabel!
    @IBOutlet weak var lblCurrentPageTitle: UILabel!
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var lblSite: UILabel!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var viewContainerPages: UIView!
    @IBOutlet weak var constraintContentHeight: NSLayoutConstraint!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var btnDisable: UIButton!
    
    //Tab bar items
    @IBOutlet weak var tabBarItemPostFeed: UITabBarItem!
    @IBOutlet weak var tabBarItemRequestUpdate: UITabBarItem!
    @IBOutlet weak var tabBarItemLanguage: UITabBarItem!
    @IBOutlet weak var tabBarItemSearchStakeholder: UITabBarItem!
    
    var stakeholder: Stakeholder!
    var vcPages: StakeholderPagesViewController!
    var isEnglish = true
    var containerViewHeight: CGFloat = 0
    fileprivate lazy var searchBar: AutoSearchBar = AutoSearchBar()
    
    @IBOutlet weak var imgUserRating: UIImageView!
    @IBOutlet weak var lblUserRating: UILabel!
    @IBOutlet weak var lblUserRatingTitle: UILabel!
    @IBOutlet weak var containerUserRating: UIView!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var containerRate: UIView!
    @IBOutlet weak var lblAdminRating: UILabel!
    @IBOutlet weak var lblAdminRatingTitle: UILabel!
    @IBOutlet weak var containerAdminRate: UIView!
    
    @IBOutlet weak var viewRateAction: UIView!
    //MARK: LIFE CYCLE
    required init?(coder aDecoder: NSCoder)
    {
        stakeholder = Stakeholder(json: nil)
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.loadConfig()
        self.loadStakeholderDetail()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
        
        //Reset Badge icon
        self.removeBadgeIcon()
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        containerViewHeight = self.tabBar.frame.origin.y - self.viewContainerPages.frame.origin.y
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        //Localizations
        self.localize()
        
        //Nav Bar
        setupNavigation(animated: false)
        
        //Blur
        imgPersonBanner.blur()
        
        //Btn Follow
        btnFollow.layer.cornerRadius = 2
        btnFollow.clipsToBounds = true
        btnFollow.setTitle("● ● ● ●", for: UIControlState())
        
        //Btn Disable
        if LibraryAPI.shared.currentUser?.role == UserRole.globalManager
        {
            self.btnDisable.layer.cornerRadius = 2
            self.btnDisable.clipsToBounds = true
            self.btnDisable.setTitle("● ● ● ●", for: UIControlState())
            self.btnDisable.addTarget(self, action: #selector(self.enableOrDisableStakeholder), for: .touchUpInside)
            self.btnDisable.isHidden = false
        }
        
        //Page control
        pageController.currentPageIndicatorTintColor = UIColor.colorForNavigationController()
        
        //Scroll//Refresh Control
//        self.scrollMain.refreshControl = UIRefreshControl()
//        self.scrollMain.refreshControl?.addTarget(self, action: #selector(self.loadStakeholderDetail), for: .valueChanged)
//        self.scrollMain.refreshControl?.setLastUpdate()
        
        scrollMain.enableRefreshAction({
            
            self.loadStakeholderDetail()
        })
        
        
        
        //Search
        searchBar.initialize(onSearchText: { [weak self] (text) in
            if (self != nil) {
                self!.updateSearch(text: text, page: self!.vcPages.currentPage)
            }
            
            }, onCancel: { [weak self] in
                if (self != nil) {
                    self!.searchBar.text = ""
                    self!.updateSearch(text: "", page: self!.vcPages.currentPage)
                    self!.setupNavigation(animated: true)
                }
        })
    }
    
    fileprivate func localize()
    {
        if DeviceType.IS_ANY_IPAD == false
        {
            self.lblCurrentPageTitle.text = "DETAIL".localized()
            self.btnFollow.setTitle("✔︎ Following".localized(), for: UIControlState())
            self.tabBarItemPostFeed.title = "Stakeholder Post".localized()
            self.tabBarItemRequestUpdate.title = "Request Update".localized()
            self.tabBarItemLanguage.title = "Language".localized()
            self.tabBarItemSearchStakeholder.title = "Search in Stakeholder".localized()
        }
        
    }
    // MARK: - UPDATE SEARCH (Private Methods)
    func updateSearch(text: String, page: Int)
    {
        if page == 0
        {
            self.vcPages.vcDetail.vcSectionProfile.searchText(text)
            self.vcPages.vcDetail.vcSectionProjects.filterByText(text)
            self.vcPages.vcDetail.vcSectionRelations.filterByText(text)
        }
        else if page == 1
        {
            self.vcPages.vcFeed.vcPostsTable.filterByText(text)
            self.vcPages.vcFeed.vcAttachmentsTable?.filterByText(text)
        }
        else if page == 2
        {
            self.vcPages.vcCalendar.filterByText(text)
        }
    }
    
    func setupNavigation(animated: Bool)
    {
        title = "STAKEHOLDER".localized()
        
        navigationItem.setHidesBackButton(false, animated: animated)
        navigationItem.fadeOutTitleView()
    }
    
    //MARK: ACTION FOLLOW
    @IBAction func tapBtnFollow(_ sender: AnyObject)
    {
        if stakeholder.isFollow == FollowStatus.following
        {
            let vcConfirmUnfollow = UIAlertController(title: nil, message: stakeholder.fullName, preferredStyle: UIAlertControllerStyle.actionSheet)
            let actionUnfollow = UIAlertAction(title: "Unfollow".localized(), style: UIAlertActionStyle.destructive, handler: {
                (action) in
                
                //Unfollow
                LibraryAPI.shared.stakeholderBO.unfollowStakeholder(self.stakeholder,
                    onSuccess: { () -> () in
                        
                        //Refresh Stakeholder List
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadStakeholdersList"), object: nil)
                        
                        MessageManager.shared.showBar(title: "Info".localized(),
                            subtitle: "Now, you're not following to".localized() + " " + "\(self.stakeholder.fullName)",
                            type: .info,
                            fromBottom: false)
                        
                        self.stakeholder.isFollow = FollowStatus.notFollowing
                        self.btnFollow.setTitle("Follow".localized(), for: UIControlState())
                        
                    }, onError: {error in
                        
                        MessageManager.shared.showBar(title: "Error", subtitle: "Error.TryAgain".localized(),
                            type: .error,
                            fromBottom: false)
                })
            })
            
            let actionCancel = UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler: nil)
            
            vcConfirmUnfollow.addAction(actionUnfollow)
            vcConfirmUnfollow.addAction(actionCancel)
            
            self.present(vcConfirmUnfollow, animated: true, completion: nil)
        }
        else
        {
            //Follow
            LibraryAPI.shared.stakeholderBO.followStakeholder(stakeholder,
                onSuccess: { () -> () in
                    
                    //Refresh Stakeholder List
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadStakeholdersList"), object: nil)
                    
                    MessageManager.shared.showBar(title: "Info".localized(),
                        subtitle: "Now, you're  following to".localized() + " " + "\(self.stakeholder.fullName)",
                        type: .info,
                        fromBottom: false)
                    
                    self.stakeholder.isFollow = FollowStatus.following
                    self.btnFollow.setTitle("✔︎ " + "Following".localized(), for: UIControlState())
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error", subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            })
        }
    }
    
    func enableOrDisableStakeholder()
    {
        if self.stakeholder.isEnable == true
        {
            let vcConfirmUnfollow = UIAlertController(title: "Would you like to disable?".localized(), message: self.stakeholder.fullName, preferredStyle: .alert)
            let actionUnfollow = UIAlertAction(title: "Disable".localized(), style: UIAlertActionStyle.destructive, handler: {
                (action) in
                
                //Disable
                LibraryAPI.shared.stakeholderBO.disableStakeholder(self.stakeholder,
                    onSuccess: { () -> () in
                        
                        //Refresh Stakeholder List
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadStakeholdersList"), object: nil)
                        
                        MessageManager.shared.showBar(title: "Info".localized(),
                            subtitle: "\(self.stakeholder.fullName)" + " " + "has been disabled".localized(),
                            type: .info,
                            fromBottom: false)
                        
                        self.stakeholder.isEnable = false
                        self.btnDisable.setTitle("Enable".localized(), for: UIControlState())
                        self.tabBarItemPostFeed.isEnabled = self.stakeholder.isEnable
                        
                    }, onError: {error in
                        
                        MessageManager.shared.showBar(title: "Error", subtitle: "Error.TryAgain".localized(),
                            type: .error,
                            fromBottom: false)
                })
            })
            
            let actionCancel = UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler: nil)
            
            vcConfirmUnfollow.addAction(actionUnfollow)
            vcConfirmUnfollow.addAction(actionCancel)
            
            self.present(vcConfirmUnfollow, animated: true, completion: nil)
            
        }
        else
        {
            //Enable
            LibraryAPI.shared.stakeholderBO.enableStakeholder(self.stakeholder,
                onSuccess: { () -> () in
                    
                    //Refresh Stakeholder List
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadStakeholdersList"), object: nil)
                    
                    MessageManager.shared.showBar(title: "Info".localized(),
                        subtitle: "\(self.stakeholder.fullName)" + " " + "has been enabled".localized(),
                        type: .info,
                        fromBottom: false)
                    
                    self.stakeholder.isEnable = true
                    self.btnDisable.setTitle("✔︎ " + " " + "Enabled".localized(), for: UIControlState())
                    self.tabBarItemPostFeed.isEnabled = self.stakeholder.isEnable
                    
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error", subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            })
        }
    }
    
    // MARK: WEB SERVICE
    func loadStakeholderDetail()
    {
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        LibraryAPI.shared.stakeholderBO.getStakeholderDetail(stakeholder.id, onSuccess: { actor in
            
            MessageManager.shared.hideHUD()
            
            //Verify if exists/ is valid
            guard let existingActor = actor else {
                return
            }
            
            self.stakeholder = existingActor
            
            self.loadStakeholderInfo()
            self.scrollMain.refreshed()
            //Add geographies
            self.vcPages.vcDetail.vcSectionGeographies.clusterItems = existingActor.geographies
            }) { error in
                
                MessageManager.shared.hideHUD()
                self.scrollMain.refreshed()
                
                _ = self.navigationController?.popViewController(animated: true)
                
                MessageManager.shared.showBar(title: "Error",
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
        }
    }
    
    //MARK: LOAD STAKEHOLDER INFORMATION
    func loadStakeholderInfo()
    {
        //Post
        self.tabBarItemPostFeed.isEnabled = self.stakeholder.isEnable
        
        //Picture
        self.imgPersonBanner.setImageWith(URL(string: stakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        self.imgPersonThumbnail.setImageWith(URL(string: stakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        
        //Title
        let strJobPosition = self.isEnglish == true ? stakeholder.jobPosition!.valueEn : stakeholder.jobPosition!.valueEs
        
        self.lblActorTitle.text = self.stakeholder.fullName
        self.lblActorTitle.adjustsFontSizeToFitWidth = true
        
        //Position / Company
        self.lblPosition.text = strJobPosition.trim().isEmpty && stakeholder.companyName.trim().isEmpty ? "" : "\(strJobPosition) / \(stakeholder.companyName)"
        self.lblPosition.adjustsFontSizeToFitWidth = true
        
        //Site
        self.lblSite.isHidden = true
        
        //Following status
        if stakeholder.isFollow == FollowStatus.following
        {
            btnFollow.setTitle("︎︎︎︎︎✔︎ " + "Following".localized(), for: UIControlState())
        }
        else
        {
            btnFollow.setTitle("Follow".localized(), for: UIControlState())
        }
        
        //Stakeholder status
        if self.stakeholder.isEnable == true
        {
            self.btnDisable.setTitle("✔︎ " + " " + "Enabled".localized(), for: UIControlState())
        }
        else
        {
            self.btnDisable.setTitle("Enable".localized(), for: UIControlState())
        }
        
        //Rating
        self.containerUserRating.setBorder()
        self.containerAdminRate.setBorder()
        self.containerRate.setBorder()
        
        self.lblUserRatingTitle.text = "User Rating".localized()
        self.lblUserRatingTitle.adjustsFontSizeToFitWidth = true
        self.lblUserRating.adjustsFontSizeToFitWidth = true
        self.lblUserRating.text = (self.stakeholder.userRating ?? 0).getString()
        
        self.lblAdminRatingTitle.text = "Admin Rating".localized()
        self.lblAdminRatingTitle.adjustsFontSizeToFitWidth = true
        self.lblAdminRating.adjustsFontSizeToFitWidth = true
        self.lblAdminRating.text = (self.stakeholder.adminRating ?? 0).getString()
        
        self.lblRate.text = "Rate".localized()
        self.lblRate.adjustsFontSizeToFitWidth = true
 
        self.viewRateAction.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapOnRating)))
        
        //Load Sub-Modules
        self.displayStakeholderProfile()
        self.displayStakeholderProjects()
        self.displayStakeholderRelations()
        self.displayFeed()
        self.displayCalendar()
    }
    
    //MARK: RATING
    func tapOnRating()
    {
        self.pushRatingControlFor(self.stakeholder)
    }
    
    fileprivate func pushRatingControlFor(_ stakeholder: Stakeholder)
    {
        let vcRating : RatingViewController = Storyboard.getInstanceFromStoryboard("Rating")
        vcRating.didEndRating = { [weak self] in
            
            self?.loadStakeholderDetail()
        }
        vcRating.stakeholder = stakeholder
        self.present(vcRating, animated: true, completion: nil)
    }
    
    //MARK: SUB-MODULES
    func displayStakeholderProfile()
    {
        vcPages.vcDetail.vcSectionProfile.stakeholder = self.stakeholder
        vcPages.vcDetail.vcSectionProfile.tableView.isScrollEnabled = false
    }
    
    func displayStakeholderProjects()
    {
        vcPages.vcDetail.vcSectionProjects.projects = stakeholder.projects
        vcPages.vcDetail.reloadProjectsTable()
    }
    
    func displayStakeholderRelations()
    {
        vcPages.vcDetail.vcSectionRelations.relations = stakeholder.relations
        vcPages.vcDetail.reloadRelationsTable()
    }
    
    func displayFeed()
    {
        vcPages.vcFeed.loadFeedForStakeholder(stakeholder)
    }
    
    func displayCalendar()
    {
        vcPages.vcCalendar.configureForEmbed()
        vcPages.vcCalendarList.configureForEmbed()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segueID_StakeholderPageContainer"
        {
            vcPages = segue.destination as! StakeholderPagesViewController
            vcPages.stakeholderId = stakeholder.id
            vcPages.onHeightChanged = { newHeight in
                
                print("NEW HEIGHT: \(newHeight) in \(type(of: self)))")
                
                //Height
                var recommendedHeight: CGFloat = 0
                
                if self.containerViewHeight > newHeight
                {
                    recommendedHeight = self.containerViewHeight
                }
                else
                {
                    recommendedHeight = newHeight
                }
                
                self.constraintContentHeight.constant = self.viewContainerPages.frame.origin.y + recommendedHeight
                self.viewContainerPages.layoutIfNeeded()
                
                //Scroll to bottom if there are events
                self.vcPages?.vcCalendar?.scrollToBottom = { [weak self] in
                    
                    let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: delayTime) {
                        
                        //Scroll to bottom, JUST in calendar view
                        if self?.vcPages?.vcCalendar.hasFocus == true
                        {
                            self?.scrollMain.scrollToBottom()
                        }
                    }
                }
                
                return recommendedHeight
            }
            
            vcPages.onPageChange = { page in
                
                self.searchBar.cancelSearch()
                self.pageController.currentPage = page
                
                switch(page)
                {
                case 0:
                    self.vcPages.vcFeed.unfocus()
                    self.vcPages.vcCalendar.unfocus()
                    self.vcPages.vcCalendarList.unfocus()
                    
                    self.lblCurrentPageTitle.text = "DETAIL".localized()
                    self.vcPages.vcDetail.focus()
                    
                    //Add language item
                    if self.vcPages.vcDetail.vcSectionProfile.isEnglish == true
                    {
                        let itemCalendarList = UITabBarItem(title: "Language", image: UIImage(named: "LanguageEng"), selectedImage: UIImage(named: "LanguageEng"))
                        itemCalendarList.tag = 2
                        self.tabBar.items?.insert(itemCalendarList, at: 2)
                    }
                    else
                    {
                        let itemCalendarList = UITabBarItem(title:"Lenguaje", image: UIImage(named: "LanguageEsp"), selectedImage: UIImage(named: "LanguageEsp"))
                        itemCalendarList.tag = 2
                        self.tabBar.items?.insert(itemCalendarList, at: 2)
                    }
                    
                    //Enable Search
                    for item in self.tabBar.items! where item.tag == 3
                    {
                        item.isEnabled = true
                    }
                case 1:
                    self.vcPages.vcDetail.unfocus()
                    self.vcPages.vcCalendar.unfocus()
                    self.vcPages.vcCalendarList.unfocus()
                    
                    self.lblCurrentPageTitle.text = "FEED".localized().uppercased()
                    self.vcPages.vcFeed.focus()
                    
                    //Remove language item
                    for (index, item) in self.tabBar.items!.enumerated() where item.tag == 2
                    {
                        self.tabBar.items?.remove(at: index)
                    }
                    
                    //Remove calendar item
                    for (index, item) in self.tabBar.items!.enumerated() where item.tag == 4
                    {
                        self.tabBar.items?.remove(at: index)
                    }
                    
                    //Enable Search
                    for item in self.tabBar.items! where item.tag == 3
                    {
                        item.isEnabled = true
                    }
                case 2:
                    self.vcPages.vcFeed.unfocus()
                    self.vcPages.vcDetail.unfocus()
                    
                    if self.vcPages.containerViewCalendar.isHidden == false
                    {
                        self.vcPages.vcCalendar.focus()
                    }
                    else
                    {
                        self.vcPages.vcCalendarList.focus()
                    }
                    
                    self.lblCurrentPageTitle.text = "CALENDAR".localized()
                    
                    //Add calendar item
                    let itemCalendarList = UITabBarItem(title: "Calendar List".localized(), image: UIImage(named: "DayView"), selectedImage: UIImage(named: "DayView"))
                    itemCalendarList.tag = 4
                    
                    self.tabBar.items?.insert(itemCalendarList, at: self.tabBar.items!.count - 1)
                    
                    //Enable Search
                    for item in self.tabBar.items! where item.tag == 3
                    {
                        if self.vcPages.vcCalendar.hasFocus == true
                        {
                            item.isEnabled = true
                        }
                        else
                        {
                            item.isEnabled = false
                        }
                    }
                default:
                    return
                }
            }
            
            vcPages.pageDidChange = {
                print("DID CHANGE!!")
                
                if self.pageController.currentPage == 2
                {
                    self.delay(0.1, closure: { () -> () in
                        
                        if self.vcPages.vcCalendar.calendar.selectedDate == nil
                        {
                            self.vcPages.vcCalendar.calendar.select(Date())
                        }
                        else
                        {
                            self.vcPages.vcCalendar.calendar.select(self.vcPages.vcCalendar.calendar.selectedDate)
                        }
                    })
                }
            }
        }
    }
    
    //Delay function
    func delay(_ delay:Double, closure:@escaping () -> ()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
}

//MARK: TAB BAR
extension StakeholderDetailViewController: UITabBarDelegate
{
    private func RequestUpdate()
    {
        let vcUpdateRequest: RequestUpdateViewController = Storyboard.getInstanceFromStoryboard("popups")
        vcUpdateRequest.stakeholderId = self.stakeholder.id 
        self.present(vcUpdateRequest, animated: true, completion: nil)

    }
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        
        switch item.tag
        {
        case TabBarOptions.newPost.rawValue:
            //New Post
            let vcNewPost = NewPostViewController()
            vcNewPost.stakeholderTagged = stakeholder
            vcNewPost.onViewControllerClosed = {
                
                self.vcPages.vcFeed.loadFeedForStakeholder(self.stakeholder)
                
                if vcNewPost.arrayDateAdded.count > 0
                {
                    self.vcPages.vcCalendar.loadFullCalendarById()
                }
            }
            
            let navController = NavyController(rootViewController: vcNewPost)
            navController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            
            present(navController, animated: true, completion:nil)
        case TabBarOptions.updateRequest.rawValue:
            
            self.RequestUpdate()
            
        case TabBarOptions.changeLanguage.rawValue:
            //Change Language
            vcPages.vcDetail.vcSectionProfile.isEnglish = !vcPages.vcDetail.vcSectionProfile.isEnglish
            displayStakeholderProfile()
            
            if self.vcPages.vcDetail.vcSectionProfile.isEnglish == true
            {
                MessageManager.shared.showStatusBar(title:"English".localized(),
                                                    type: .info,
                                                    containsIcon: false)

                for item in self.tabBar.items! where item.tag == 2
                {
                    item.title = "Language"
                    item.image = UIImage(named: "LanguageEsp")
                    item.selectedImage =  UIImage(named: "LanguageEsp")
                }
            }
            else
            {
                MessageManager.shared.showStatusBar(title:"Spanish".localized(),
                                                    type: .info,
                                                    containsIcon: false)
                
                for item in self.tabBar.items! where item.tag == 2
                {
                    item.title = "Lenguaje"
                    item.image = UIImage(named: "LanguageEng")
                    item.selectedImage =  UIImage(named: "LanguageEng")
                }
            }
            
        case TabBarOptions.search.rawValue:
            //Search in Detail
            searchBar.showInNavigationItem(self.navigationItem, animated: true)
        case TabBarOptions.calendars.rawValue:
            //Calendar View or Calendar List
            if self.vcPages.containerViewCalendar.isHidden == false
            {
                self.vcPages.containerViewCalendar.isHidden = true
                self.vcPages.containerViewCalendarList.isHidden = false
                self.vcPages.vcCalendar.unfocus()
                self.vcPages.vcCalendarList.focus()
                
                for item in self.tabBar.items! where item.tag == 4
                {
                    item.title = "Calendar View".localized()
                    item.image = UIImage(named: "Calendar")
                    item.selectedImage =  UIImage(named: "Calendar")
                }
                
                //Disable Search
                for item in self.tabBar.items! where item.tag == 3
                {
                    item.isEnabled = false
                }
            }
            else
            {
                self.vcPages.containerViewCalendar.isHidden = false
                self.vcPages.containerViewCalendarList.isHidden = true
                self.vcPages.vcCalendar.focus()
                self.vcPages.vcCalendarList.unfocus()
                
                for item in self.tabBar.items! where item.tag == 4
                {
                    item.title = "Calendar List".localized()
                    item.image = UIImage(named: "DayView")
                    item.selectedImage =  UIImage(named: "DayView")
                }
                
                //Enable Search
                for item in self.tabBar.items! where item.tag == 3
                {
                    item.isEnabled = true
                }
            }
            
        default:
            break
        }
    }
}
