//
//  GeographyViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 7/31/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit
enum GeoViewMode_iPad: Int
{
    case mapList
    case collection
}
class GeographyViewController_iPad: UIViewController
{
    //Constraints and UI elements
    @IBOutlet weak var containerCollection: UIView!
    @IBOutlet weak var containerMap: UIView!
    @IBOutlet weak var containerList: UIView!
    @IBOutlet weak var constraintMapRight: NSLayoutConstraint!
    @IBOutlet weak var viewBottomBar: UIView!
    @IBOutlet weak var btnChangeView: UIButton!
    var constraintMapRight_init: CGFloat = 0.0
    var viewMode: GeoViewMode_iPad = .mapList
    
    //Embeded Controllers
    var vcMap: ClusterMapViewController?
    var vcList: GeoListViewController?
    var vcCollection: GeoListViewController_iPad?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.getFollowedGeographiesLight()
    }
    
    fileprivate func loadConfig()
    {
        self.title = "GEOGRAPHIES".localized()
        self.viewBottomBar.blur()
        self.viewBottomBar.alpha = 0.9
        self.btnChangeView.round()
        self.btnChangeView.addShadow()
        self.constraintMapRight_init = self.constraintMapRight.constant
        self.constraintMapRight.constant = 0.0
        self.containerMap.setBorder()
        self.containerList.setBorder()
        self.updateView()
        
        //Items Bar
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        navigationItem.rightBarButtonItems = [barBtnCompose]
    }

    @IBAction func changeViewMode(_ sender: Any)
    {
        self.viewMode = self.viewMode == .mapList ? .collection : .mapList
        self.updateView()
    }
    
    //MARK: NEW POST
    func createNewPost()
    {
        let vcNewPost = NewPostModal()
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
        
        present(navController, animated: true, completion:nil)
    }
    
    //MARK: WEB SERVICE
    fileprivate func getFollowedGeographiesLight(/*GeographyId:Int, SearchText:String*/)
    {
        let params:[String : Any] = [:]
        MessageManager.shared.showLoadingHUD()
    
        self.vcList?.vcGeoTable.tableView.displayBackgroundMessage("Loading...".localized(), subMessage: String())
        LibraryAPI.shared.geographyBO.getFollowedGeographiesLight(params, onSuccess: { (geographies, totalItems) in
            
            MessageManager.shared.hideHUD()
            self.vcList?.vcGeoTable.tableView.dismissBackgroundMessage()
            self.vcMap?.locations = geographies
            self.vcList?.vcGeoTable.clusterItems = geographies
            self.vcList?.vcGeoTable.backupClusterItems = geographies
            self.vcCollection?.vcGeoCollection?.clusterItems = geographies
            self.vcCollection?.vcGeoCollection?.backupClusterItems = geographies
        }) { error in
            
            //Error Indicator
            MessageManager.shared.showErrorHUD()
            self.vcList?.vcGeoTable.tableView.displayBackgroundMessage("No data".localized(), subMessage: String())
            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                MessageManager.shared.hideHUD()
            }
        }
    }
    
    //MARK: UTILITIES
    fileprivate func updateView()
    {
        if self.viewMode == .mapList
        {
            self.containerMap.isHidden = false
            self.containerList.isHidden = false
            self.containerCollection.isHidden = true
            self.btnChangeView.setImage(UIImage(named: "iconList")!, for: .normal)
            if UIDevice.current.orientation.isLandscape == true
            {
                self.showList()
            }
            else
            {
                self.hideList()
            }
        }
        else if self.viewMode == .collection
        {
            self.containerMap.isHidden = true
            self.containerList.isHidden = true
            self.containerCollection.isHidden = false
            self.btnChangeView.setImage(UIImage(named: "iconMapView")!, for: .normal)
            self.hideList()
        }
    }
    
    fileprivate func hideList()
    {
        self.constraintMapRight.constant = 0.0
        self.containerList.isHidden = true
    }
    
    fileprivate func showList()
    {
        self.constraintMapRight.constant = self.constraintMapRight_init
        self.containerList.isHidden = false
    }
    
    //MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "ClusterMapViewController"
        {
            if let map = segue.destination as? ClusterMapViewController {
                self.vcMap = map
                self.vcMap?.onDidUpdateGeography = {[weak self] void in
                    
                    self?.getFollowedGeographiesLight()
                }
            }
        }
        else if segue.identifier == "GeoListViewController"
        {
            if let list = segue.destination as? GeoListViewController {
                
                self.vcList = list
                self.vcList?.onDidUpdateGeography = {[weak self] void in
                    
                    self?.getFollowedGeographiesLight()
                }
            }
        }
        else if segue.identifier == "GeoListViewController_iPad"
        {
            if let collection = segue.destination as? GeoListViewController_iPad {
                
                self.vcCollection = collection
                self.vcCollection?.onDidUpdateCollection = { [weak self] Void in
                
                    self?.getFollowedGeographiesLight()
                }
            }
        }
    }
    
    //MARK: ROTATION
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        if UIDevice.current.orientation.isLandscape == true
        {
            guard self.viewMode != .collection else {
                
                return
            }
            self.showList()
        }
        else
        {
            guard self.viewMode != .collection else {
                
                return
            }
            self.hideList()
        }
    }
}
