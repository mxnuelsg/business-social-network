//
//  GeoStakeholderProjects_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 8/2/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit
import Spruce

class GeoStakeholderProjects_iPad: UIViewController
{
    @IBOutlet weak var collectionStakeholders: UICollectionView!
    @IBOutlet weak var tableProjects: UITableView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    var layout: UICollectionViewFlowLayout!
    var geographyId: Int = -1
    var stakeholders = [Stakeholder]() {
        didSet{
            //clean up
            self.arraySectionTitles.removeAll()
            self.arrayStakeholders.removeAll()
            //Prepare arrays for collection
            // load an array of the first letters
            let firstLetters = self.stakeholders.map({ String($0.fullName.uppercased()[$0.fullName.uppercased().startIndex]) })
            //Delete Characters duplicated & Ordering alphabetically
            self.arraySectionTitles = Array(Set(firstLetters)).sorted(by: {$0 < $1})
            //Sort Stakeholder by Section Character
            for character in self.arraySectionTitles
            {
                self.arrayStakeholders.append(stakeholders.filter({ String($0.fullName.uppercased()[$0.fullName.uppercased().startIndex]) == character }))
            }
            
            //Reload data
            self.collectionStakeholders.reloadData()
            //Spruce framework Animation
            DispatchQueue.main.async {
                
                self.collectionStakeholders.spruce.animate([.fadeIn, .expand(.slightly)], sortFunction: LinearSortFunction(direction: .topToBottom, interObjectDelay: 0.1))
            }
        }
    }
    var arrayStakeholders = [[Stakeholder]]()
    var arraySectionTitles = [String]()
    
    var projects = [Project]() {
        didSet{
            
            self.tableProjects.reloadData()
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func loadConfig()
    {
        self.segmentControl.setTitle("Projects".localized(), forSegmentAt: 0)
        self.updateContent()
        self.segmentControl.setTitle("Stakeholders".localized(), forSegmentAt: 1)        
        self.tableProjects.register(UINib(nibName: "ProjectTableCell", bundle: nil), forCellReuseIdentifier: "ProjectTableCell")
        self.tableProjects.hideEmtpyCells()
        
        //Register cells
        self.collectionStakeholders.register(UINib(nibName: "CollectionStakeholderCell", bundle: nil), forCellWithReuseIdentifier: "CollectionStakeholderCell")
        self.collectionStakeholders.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind:UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
        
        //Collection layout
        self.layout = UICollectionViewFlowLayout()
        self.loadLayoutForCollectionView()
        self.collectionStakeholders.collectionViewLayout = self.layout
        self.collectionStakeholders.dataSource = self
        self.collectionStakeholders.delegate = self
        self.collectionStakeholders.allowsMultipleSelection = false
        self.collectionStakeholders.register(UINib(nibName: "CollectionStakeholderCell", bundle: nil), forCellWithReuseIdentifier: "CollectionStakeholderCell")
    }
    
    func updateContent()
    {
        if self.segmentControl.selectedSegmentIndex == 0
        {
            self.collectionStakeholders.isHidden = true
            self.tableProjects.isHidden = false
        }
        else if self.segmentControl.selectedSegmentIndex == 1
        {
            self.collectionStakeholders.isHidden = false
            self.tableProjects.isHidden = true
        }
    }

    @IBAction func segmentedChanged(_ sender: Any)
    {
        //Update Table and Collection visibility
        self.updateContent()
        //Update Contents
        self.loadStakeholderProjects()
    }
    
    //MARK: WEB SERVICES
    func loadStakeholderProjects()
    {
        if segmentControl.selectedSegmentIndex == 0
        {
            self.loadProjects()
        }
        else
        {
            self.loadStakeholders()
        }
    }
    
    fileprivate func loadProjects()
    {
        //Cancel previous request
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.getStakeholdersByGeoId)
        
        //Load Projects
        let params = ["geographyId":self.geographyId]
        self.tableProjects.displayBackgroundMessage("Loading...".localized(), subMessage: String())
        LibraryAPI.shared.geographyBO.getProjectsByGeoId(params, onSuccess: { (projects) in
            
            self.tableProjects.dismissBackgroundMessage()
            self.projects = projects
            if self.projects.count == 0
            {
                self.tableProjects.displayBackgroundMessage("No data".localized(), subMessage: String())
            }
        }) { (error) in
            
            self.tableProjects.dismissBackgroundMessage()
            guard error.code != -999 else {return}
            MessageManager.shared.showBar(title: "Error", subtitle: "It wasn't possible to obtain this geography detail. Try again.".localized(), type: .error, fromBottom: false)
        }
    }
    
    fileprivate func loadStakeholders()
    {
        //Cancel Previous requests
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.getProjectsByGeoId)
        //Load Stakeholders
        let params = ["geographyId":self.geographyId]
        self.collectionStakeholders.displayBackgroundMessage("Loading...".localized(), subMessage: String())
        
        LibraryAPI.shared.geographyBO.getStakeholdersByGeoId(params, onSuccess: { (stakeholders) in
            
            self.collectionStakeholders.dismissBackgroundMessage()
            self.stakeholders =  stakeholders
            if self.stakeholders.count == 0
            {
                self.collectionStakeholders.displayBackgroundMessage("No data".localized(), subMessage: String())
            }
        }) { (error) in
            self.collectionStakeholders.dismissBackgroundMessage()
            guard error.code != -999 else {return}
            MessageManager.shared.showBar(title: "Error", subtitle: "It wasn't possible to obtain this geography detail. Try again.".localized(), type: .error, fromBottom: false)
        }
    }

    
    //MARK: LAYOUT FOR COLLECTION VIEW
    func loadLayoutForCollectionView()
    {
        if (UIDeviceOrientationIsPortrait(UIDevice.current.orientation))
        {
            self.layout.scrollDirection =  .vertical
            self.layout.minimumInteritemSpacing = 1
            self.layout.minimumLineSpacing = 3
            self.layout.sectionInset = UIEdgeInsets(top: 5, left: 2, bottom: 5, right: 2)
            self.layout.headerReferenceSize = CGSize(width: 320, height: 24)
            self.layout.itemSize = CGSize(width: 250, height: 85)
        }
        else
        {
            self.layout.scrollDirection =  UICollectionViewScrollDirection.vertical
            self.layout.minimumInteritemSpacing = 1
            self.layout.minimumLineSpacing = 3
            self.layout.sectionInset = UIEdgeInsets(top: 5, left: 2, bottom: 5, right: 2)
            self.layout.headerReferenceSize = CGSize(width: 320, height: 24)
            self.layout.itemSize = CGSize(width: 250, height: 85)
        }
    }
    
    //MARK: RATING CONTROL
    fileprivate func pushRatingControlFor(_ stakeholder: Stakeholder, view: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 50)))
    {
        let vcRating = RatingViewController_iPad()
        vcRating.modalPresentationStyle = .popover
        self.present(vcRating, animated: true, completion: nil)
        vcRating.preferredContentSize  = CGSize(width: 280,height: 60)
        vcRating.stakeholder = stakeholder
        let datePopover = vcRating.popoverPresentationController
        datePopover?.permittedArrowDirections = .any
        datePopover?.sourceView = view
        datePopover?.sourceRect = view.frame
        datePopover?.backgroundColor = .black
        vcRating.didEndRating = { void in
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "getDetail"), object: nil)
        }
    }
}

extension GeoStakeholderProjects_iPad: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.projects.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 155
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let project = self.projects[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProjectTableCell") as! ProjectTableCell
        cell.selectionStyle = .none
        cell.cellForProject(cell, project: project)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let project = self.projects[indexPath.row]
        if DeviceType.IS_ANY_IPAD == true
        {
            let vcProjectDetail = Storyboard.getInstanceOf(ProjectDetailViewController_iPad.self)
            vcProjectDetail.project = project
            navigationController?.pushViewController(vcProjectDetail, animated: true)
        }
        else
        {
            let vcProjectDetail : DetailProjectViewController = Storyboard.getInstanceFromStoryboard("Projects")
            vcProjectDetail.project = project                        
            navigationController?.pushViewController(vcProjectDetail, animated: true)
        }
    }
}

extension GeoStakeholderProjects_iPad: UICollectionViewDelegate, UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        //Background message
        if self.arrayStakeholders.count > 0
        {
            self.collectionStakeholders.dismissBackgroundMessage()
        }
        else
        {
            self.collectionStakeholders.displayBackgroundMessage("No stakeholders found".localized(),
                                                                 subMessage: "")
        }        
        return self.arrayStakeholders.count
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.arrayStakeholders[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        //Just Allow Header type
        guard kind == UICollectionElementKindSectionHeader else {
            
            return UICollectionReusableView()
        }
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
        
        if indexPath.section < self.arraySectionTitles.count
        {
            header.lblTitle.text = self.arraySectionTitles[indexPath.section]
        }
        
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionStakeholderCell", for: indexPath) as! CollectionStakeholderCell
        let stakeholder = self.arrayStakeholders[indexPath.section][indexPath.row]
        
        cell.lblName.text = stakeholder.fullName
        print(stakeholder.fullName)
        //Following indicator
        cell.imgFollowed.isHidden = stakeholder.isFollow == FollowStatus.following ? false : true
        //Position & Company
        let strJobPosition = stakeholder.jobPosition?.value ?? ""
        let strCompanyName = stakeholder.companyName
        cell.lblJobPosition.attributedText = NSMutableAttributedString().setJobPositionCompany(strJobPosition, company: strCompanyName)
        cell.imgProfile.setImageWith(URL(string: stakeholder.thumbnailUrl), placeholderImage: UIImage(named: "defaultperson"))
        cell.setNationalities(stakeholder)
        cell.btnMore.isHidden = true
        
        //Rating
        cell.lblUserRating.text = (stakeholder.userRating ?? 0).getString()
        cell.lblAdminRating.text = (stakeholder.adminRating ?? 0).getString()
        cell.didTapOnRating = {[weak self] void in
            
            self?.pushRatingControlFor(stakeholder, view: cell.lblUserRating)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let stakeholderSelected = self.arrayStakeholders[indexPath.section][indexPath.row]
        
        let vcStakeholderDetail = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
        vcStakeholderDetail.stakeholder = stakeholderSelected
        
        navigationController?.pushViewController(vcStakeholderDetail, animated: true)
    }
}
