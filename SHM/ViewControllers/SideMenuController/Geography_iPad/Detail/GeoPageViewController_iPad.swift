//
//  GeoPageViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 8/2/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class GeoPageViewController_iPad: UIPageViewController
{
    //MARK: OUTLETS AND VARIABLES
    var vcPosts: GeoFeedViewController?
    var vcCalendar: GeoCalendarViewController?
    var viewControllerList:[UIViewController]?
    var onDidChangePage:((_ page: Int)->())?
    
    var geoItem = ClusterItem(json:nil)     
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    //MARK: LOAD CONFIG
    fileprivate func loadConfig()
    {
        self.delegate = self
        self.dataSource = self
        self.setVCList()
        if let firstViewController = self.viewControllerList?.first {
            
            self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
        self.view.backgroundColor = UIColor.whiteCloud()
    }

    //MARK: UTILITIES
    fileprivate func setVCList()
    {
        let sb = UIStoryboard(name: "GeographyPad", bundle: nil)
        self.vcPosts = sb.instantiateViewController(withIdentifier: "GeoFeedViewController") as! GeoFeedViewController
        self.vcCalendar = sb.instantiateViewController(withIdentifier: "GeoCalendarViewController") as! GeoCalendarViewController
        
        self.viewControllerList = [self.vcPosts!, self.vcCalendar!]
    }
}

extension GeoPageViewController_iPad: UIPageViewControllerDataSource,UIPageViewControllerDelegate
{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        guard let vcIndex = viewControllerList?.index(of: viewController) else {return nil}
        
        let previousIndex = vcIndex - 1
        guard previousIndex >= 0 else {return nil}
        guard (viewControllerList?.count)! > previousIndex else {return nil}
        
        
        return viewControllerList?[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        guard let vcIndex = viewControllerList?.index(of: viewController) else {return nil}
        
        let nextIndex = vcIndex + 1
        guard viewControllerList?.count != nextIndex else {return nil}
        guard (viewControllerList?.count)! > nextIndex else {return nil}
        
        return viewControllerList?[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        guard completed == true else {return}
        if let page = pageViewController.viewControllers?.first?.view.tag {
            
            self.onDidChangePage?(page)
            print(page)
        }
    }
    
}
