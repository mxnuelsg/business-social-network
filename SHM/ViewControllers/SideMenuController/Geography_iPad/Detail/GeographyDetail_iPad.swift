//
//  GeographyDetail_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 8/1/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class GeographyDetail_iPad: UIViewController
{
    @IBOutlet weak var lblPageTitle: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var containerStakholderProjects: UIView!
    @IBOutlet weak var containerFeedCalendar: UIView!
    
    var geoItem = ClusterItem(json:nil)
    var geographyId = -1
    var vcPageController: GeoPageViewController_iPad?
    var vcStakeholderProjecst: GeoStakeholderProjects_iPad?
    var onDidUpdateGeography:(()->())?
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        self.backButtonArrow()
    }
    
    deinit
    {
        print("Deinit: GeographyDetailViewController_iPad")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "getDetail"), object: nil)
    }
    
    fileprivate func loadConfig()
    {
        self.title = "GEOGRAPHY".localized()
        self.getDetail()
        self.containerFeedCalendar.setBorder()
        self.containerStakholderProjects.setBorder()
        self.view.backgroundColor = UIColor.whiteCloud()
        self.btnFollow.cornerRadius()
        self.btnFollow.addShadow()
        //Items Bar
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        navigationItem.rightBarButtonItems = [barBtnCompose]
        //Notifications
        NotificationCenter.default.addObserver(self, selector:#selector(self.getDetail), name: NSNotification.Name(rawValue: "getDetail"), object: nil)
    }
    
    fileprivate func loadHeader()
    {
        self.lblTitle.attributedText = self.geoItem.getTitleForDetail()
        let btnTitle = self.geoItem.isFollowed == true ? "Unfollow".localized() : "Follow".localized()
        self.btnFollow.setTitle(btnTitle, for: .normal)        
    }
    
    //MARK: WEB SERVICES
    @objc fileprivate func getDetail()
    {
        let params = ["geographyId":self.geographyId]
        
        //LOAD TITLE AND FOLLOW STATUS
        MessageManager.shared.showLoadingHUD()
        LibraryAPI.shared.geographyBO.getDetailById(params, onSuccess: { (geography) in
            
            MessageManager.shared.hideHUD()
            self.geoItem = geography
            self.vcPageController?.geoItem = geography
            self.vcStakeholderProjecst?.geographyId = geography.id
            self.loadHeader()
            
        }) { (error) in
            MessageManager.shared.hideHUD()
            MessageManager.shared.showBar(title: "Error", subtitle: "It wasn't possible to obtain this geography detail. Try again.".localized(), type: .error, fromBottom: false)
        }
        
        //LOAD PROJECTS
        self.vcStakeholderProjecst?.geographyId = self.geographyId
        self.vcStakeholderProjecst?.loadStakeholderProjects()
        
        //LOAD FEEDS
        self.vcPageController?.vcPosts?.geographyId = self.geographyId
        self.vcPageController?.vcPosts?.updateFeed()
        
        //PREPARE CALENDAR
        self.vcPageController?.vcCalendar?.geographyId = self.geographyId
    }
    
    fileprivate func updateLateralPage(currentPage:Int)
    {
        if currentPage == 0 //FEEDS
        {
            //Cancel previous request for calendar events
            LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.getEventsByGeoId)
            
            self.vcPageController?.vcPosts?.geographyId = self.geographyId
            self.vcPageController?.vcPosts?.updateFeed()
        }
        else        //CALENDARS
        {
            //Cancel previous request for feeds
            LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.GetFeedEditingStakeholder)
            self.vcPageController?.vcCalendar?.geographyId = self.geographyId
            self.vcPageController?.vcCalendar?.loadCalendar()
        }
    }
    //MARK: NEW POST
    func createNewPost()
    {
        let vcNewPost = NewPostModal()
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
        vcNewPost.geographyTagged = self.geoItem
        vcNewPost.didPostGeography = { [weak self] Void in
            
            //update posts
            self?.vcPageController?.vcPosts?.lastRegisteredPostId = 0
            self?.vcPageController?.vcPosts?.posts.removeAll()
            self?.vcPageController?.vcPosts?.updateFeed()
            
            //update calendar            
            self?.vcPageController?.vcCalendar?.loadCalendar_iPad()
            
        }
        present(navController, animated: true, completion:nil)
    }
    
    // MARK: FOLLOW/UNFOLLOW ACTION
    
    @IBAction func followAction(_ sender: Any)
    {
        MessageManager.shared.showLoadingHUD()
        if self.geoItem.isFollowed == false
        {
            
            LibraryAPI.shared.geographyBO.followGeography(self.geoItem.id, onSuccess: { (clusterItem) in
                
                self.geoItem.isFollowed = clusterItem.isFollowed
                
                self.btnFollow.setTitle("Unfollow".localized(), for: .normal)
                MessageManager.shared.hideHUD()
                let info = "Now, you're following".localized() + " " + self.geoItem.title
                MessageManager.shared.showBar(title: "Success".localized(), subtitle: info, type: .success, fromBottom: false)
                
                self.onDidUpdateGeography?()
            }, onError: { (error) in
                
                MessageManager.shared.hideHUD()
                MessageManager.shared.showBar(title: "Error", subtitle: "It wasn't possible follow this geography. Try again.".localized(), type: .error, fromBottom: false)
            })
        }
        else
        {
            LibraryAPI.shared.geographyBO.unfollowGeography(self.geoItem.id, onSuccess: { (clusterItem) in
                
                self.geoItem.isFollowed = clusterItem.isFollowed
                self.btnFollow.setTitle("Follow".localized(), for: .normal)
                MessageManager.shared.hideHUD()
                let info = "Now, you're not following".localized() + " " + self.geoItem.title
                MessageManager.shared.showBar(title: "Success".localized(), subtitle: info, type: .success, fromBottom: false)
                self.onDidUpdateGeography?()
            }, onError: { (error) in
                
                MessageManager.shared.hideHUD()
                MessageManager.shared.showBar(title: "Error", subtitle: "It wasn't possible unfollow this geography. Try again.".localized(), type: .error, fromBottom: false)
            })
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "GeoPageViewController_iPad"
        {
            if let pageController = segue.destination as? GeoPageViewController_iPad {
                self.vcPageController = pageController
                self.vcPageController?.onDidChangePage = {[weak self] page in
                
                    self?.pageControl.currentPage = page
                    self?.updateLateralPage(currentPage: page)
                }
            }
        }
        else if segue.identifier == "GeoStakeholderProjects_iPad"
        {
            if let vcSHPro = segue.destination as? GeoStakeholderProjects_iPad {
                self.vcStakeholderProjecst = vcSHPro
            }
            
        }
    }
}
