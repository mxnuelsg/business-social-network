//
//  GeoListViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 8/1/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class GeoListViewController_iPad: UIViewController
{

    //MARK: VARIABLES AND OUTLETS
    @IBOutlet weak var containerCollection: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var searchBar: UISearchBar!
    var vcGeoCollection: GeoCollectionViewController?
    var onDidUpdateCollection:(()->())?
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        //Search
        self.searchBar.placeholder = "Search".localized()                
        self.segmentControl.setTitle("Following".localized(), forSegmentAt: 0)
        self.segmentControl.setTitle("All".localized(), forSegmentAt: 1)
    }

    //MARK: ACTIONS
    func hideKeyboard()
    {
        self.searchBar.resignFirstResponder()
        self.view.gestureRecognizers?.removeAll()
    }
    
    
    @IBAction func segmentChanged(_ sender: Any)
    {
        //Restore searchbar
        self.searchBar.text = String()
        self.searchBar.showsCancelButton = false
        self.searchBar.resignFirstResponder()

        //Segment has changed, initialize the geoQuery to search from beginning
        self.vcGeoCollection?.geoQuery.getAllWithPagination(pageSize: 60)
        self.vcGeoCollection?.geoQuery.isOrderedByTitle = true
        //Update list mode
        self.vcGeoCollection?.collectionMode = GeoCollectionMode(rawValue: segmentControl.selectedSegmentIndex) ?? .following

//        //Refresh list for current List Mode
        self.vcGeoCollection?.refreshCollection()
    }
    
    
    //MARK: SEARCH FILTERS
    fileprivate func localSearchFilter(_ searchText: String)
    {
        let term = searchText.trim()
        guard term.isEmpty == false else {
            
            self.vcGeoCollection?.clusterItems = self.vcGeoCollection?.backupClusterItems ?? [ClusterItem]()
            return
        }
        let filteredItems = self.vcGeoCollection?.backupClusterItems.filter({$0.title.localizedCaseInsensitiveContains(term)}) ?? [ClusterItem]()
        self.vcGeoCollection?.clusterItems = filteredItems
    }
    
    fileprivate func remoteSearchFilter(_ searchText: String)
    {
        let term = searchText.trim()
        guard term.isEmpty == false else {
            
            self.vcGeoCollection?.geoQuery.getAllWithPagination()
            self.vcGeoCollection?.refreshCollection()
            return
        }
        
        self.vcGeoCollection?.geoQuery.setSearchText(term)
        self.vcGeoCollection?.refreshCollection()
    }
    
    //MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let collection = segue.destination as? GeoCollectionViewController {
            
            self.vcGeoCollection = collection
            self.vcGeoCollection?.onDidUpdateCollection = { [weak self] in
                
                    self?.onDidUpdateCollection?()
            }
            //self.onTableAssigned?(geoTable)
        }
    }
}

//MARK: SEARCH BAR DELEGATE
extension GeoListViewController_iPad: UISearchBarDelegate
{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        //Show Cancel
        searchBar.setShowsCancelButton(true, animated: true)
        searchBar.tintColor = .white
        
        //Add gesture to remove keyboard
        let tapGR = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        self.view.addGestureRecognizer(tapGR)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        //Filter by segment
        if self.vcGeoCollection?.collectionMode == .following
        {
            self.localSearchFilter(searchText)
        }
        else if self.vcGeoCollection?.collectionMode == .all
        {
            guard searchText.trim().isEmpty == false else {
                
                self.remoteSearchFilter(String())
                return
            }
            guard searchText.trim().characters.count >= 3 else {
                
                return
            }
            let delay = DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delay) {
                
                self.remoteSearchFilter(searchText)
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        //Hide Cancel
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        
        guard let term = searchBar.text , term.trim().isEmpty == false else {
            
            MessageManager.shared.showBar(title: nil,
                                          subtitle: "White spaces are not permitted".localized(),
                                          type: .warning,
                                          containsIcon: true,
                                          fromBottom: false)
            return
        }
        //Filter by segment
        if self.vcGeoCollection?.collectionMode == .following
        {
            self.localSearchFilter(term)
        }
        else if self.vcGeoCollection?.collectionMode == .all
        {
            self.remoteSearchFilter(term)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        //Hide Cancel
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.text = String()
        searchBar.resignFirstResponder()
        //Filter by segment
        if self.vcGeoCollection?.collectionMode == .following
        {
            self.localSearchFilter(String())
        }
        else if self.vcGeoCollection?.collectionMode == .all
        {
            self.remoteSearchFilter(String())
        }
    }
}
