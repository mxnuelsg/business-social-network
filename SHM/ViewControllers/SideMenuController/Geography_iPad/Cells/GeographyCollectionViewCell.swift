//
//  GeographyCollectionViewCell.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 8/1/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class GeographyCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var btnOptions: UIButton!
    var onButtonTap:((_ button: UIButton) -> ())?
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.btnOptions.setBorder()
        // Initialization code
    }

    @IBAction func showActions(_ sender: Any)
    {
        self.onButtonTap?(sender as! UIButton)
    }
}
