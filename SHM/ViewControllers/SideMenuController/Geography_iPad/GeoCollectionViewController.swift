//
//  GeoCollectionViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 8/1/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit
import Spruce

enum GeoCollectionMode: Int
{
    case following = 0
    case all
}
private let reuseIdentifier = "GeographyCollectionViewCell"

class GeoCollectionViewController: UICollectionViewController
{
    //UI
    var layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    
    //Query
    var geoQuery = GeographyQuery()
    var collectionMode: GeoCollectionMode = .following
    var ownerModule: OwnerModule?
    
    //Geographies
    fileprivate var sectionTitles = [String]()
    fileprivate var sectionedGeographies = [[ClusterItem]]()
    
    //Closures
    var onDidLoadGeographies:((_ numberOfItems: CGFloat)->())?
    var onDidSelectGeography:((_ geography: ClusterItem)->())?
    var onDidUpdateCollection:(()->())?
    
    //Cluster items
    var backupClusterItems = [ClusterItem]()
    var geographiesAdded = [ClusterItem]()
    var clusterItems = [ClusterItem]() {
        didSet {
            
            //Rerefresh Table's right indexes
            self.indexGeographies()
            //Reload table
            self.collectionView?.reloadData()
            
            //Spruce framework Animation
            DispatchQueue.main.async {
                
                self.collectionView?.spruce.animate([.fadeIn, .expand(.slightly)], sortFunction: LinearSortFunction(direction: .topToBottom, interObjectDelay: 0.1))
            }
            guard self.clusterItems.count > 0 else {
                
                //Broadcast 0 number of items
                //self.onDidLoadGeographies?(0)
                self.collectionView?.displayBackgroundMessage("No data".localized(), subMessage: String())
                
                return
            }
            self.collectionView?.dismissBackgroundMessage()
            
            //Broadcast the number of items updated
            self.onDidLoadGeographies?(CGFloat(self.clusterItems.count))
        }
    }

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.register(UINib(nibName: "GeographyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GeographyCollectionViewCell")
        self.collectionView!.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind:UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
    }

    fileprivate func loadConfig()
    {
        self.loadLayoutForCollectionView()
        self.collectionView?.collectionViewLayout = self.layout
    }
    
    //MARK: REFRESH COLLECTION VIEW
    //This function can be used from the ParentController
    //as this class have the ability to fetch its own data.
    func refreshCollection()
    {
        //Update UI
        self.view.endEditing(true)
        MessageManager.shared.showLoadingHUD()
        if self.collectionMode == .following || self.geoQuery.pageNumber == 1
        {
            self.showLoadingMode()
        }
        
        //Cancel Previous request
        self.cancelSearchRequests()
        
        //Request Geographies
        if self.collectionMode == .all
        {
            LibraryAPI.shared.geographyBO.getGeographiesLight(self.geoQuery.getWSObject(), onSuccess: { (geographies, totalItems) in
                
                //Update and broadcast the number of items
                self.geoQuery.totalItems = totalItems
                self.clusterItems.append(contentsOf: geographies)
                self.onDidLoadGeographies?(CGFloat(geographies.count))
                MessageManager.shared.hideHUD()
                
            }, onError: { (error) in
                
                MessageManager.shared.hideHUD()
                guard error.code != -999 else {
                    
                    return
                }
                self.collectionView?.displayBackgroundMessage("No data".localized(), subMessage: String())
            })
        }
        else if self.collectionMode == .following
        {
            LibraryAPI.shared.geographyBO.getFollowedGeographiesLight(GeographyQuery().getWSObject(), onSuccess: { (geographies, totalItems) in
                
                self.clusterItems = geographies
                self.onDidLoadGeographies?(CGFloat(geographies.count))
                MessageManager.shared.hideHUD()
            }, onError: { (error) in
                
                MessageManager.shared.hideHUD()
                guard error.code != -999 else {
                    
                    return
                }
                self.collectionView?.displayBackgroundMessage("No data".localized(), subMessage: String())
            })
        }
    }
    
    //MARK: WEB SERVICES
    fileprivate func updateFollowStatus(_ clusterItem: ClusterItem)
    {
        if clusterItem.isFollowed == true
        {
            LibraryAPI.shared.geographyBO.unfollowGeography(clusterItem.id, onSuccess: { clusterItem in
                
                //Update Geo List
                if self.collectionMode == .following
                {
                    self.refreshCollection()
                }
                
                var title = String()
                if let idx = self.clusterItems.index(where: {$0.id == clusterItem.id}) {
                    
                    self.clusterItems[idx].isFollowed = false
                    title = self.clusterItems[idx].title
                }
                
                //self.onDidUpdateCollection?()
                
                let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    
                    let info = "Now, you're not following".localized() + " " + title
                    MessageManager.shared.showBar(title: "Success".localized(), subtitle: info, type: .success, fromBottom: false)
                }
                
            }, onError: { error in
                
                MessageManager.shared.showBar(title: "Error", subtitle: "It wasn't possible unfollow this geography. Try again.".localized(), type: .error, fromBottom: false)
            })
        }
        else
        {
            //Follow action
            LibraryAPI.shared.geographyBO.followGeography(clusterItem.id, onSuccess: { clusterItem in
                
                //Update Geo List
                if self.collectionMode == .following
                {
                    self.refreshCollection()
                }
                var title = String()
                if let idx = self.clusterItems.index(where: {$0.id == clusterItem.id}) {
                    
                    self.clusterItems[idx].isFollowed = true
                    title = self.clusterItems[idx].title
                }
                //self.refreshCollection()
                //self.onDidUpdateCollection?()
                
                let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    
                    let info = "Now, you're following".localized() + " " + title
                    MessageManager.shared.showBar(title: "Success".localized(), subtitle: info, type: .success, fromBottom: false)
                }
                
            }, onError: { error in
                
                MessageManager.shared.showBar(title: "Error", subtitle: "It wasn't possible follow this geography. Try again.".localized(), type: .error, fromBottom: false)
            })
        }
    }
    //MARK: LAYOUT FOR COLLECTION VIEW
    func loadLayoutForCollectionView()
    {
        if (UIDeviceOrientationIsPortrait(UIDevice.current.orientation))
        {
            self.layout.scrollDirection =  .vertical
            self.layout.minimumInteritemSpacing = 5
            self.layout.minimumLineSpacing = 5
            self.layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
            self.layout.headerReferenceSize = CGSize(width: 10, height: 24)
            self.layout.itemSize = CGSize(width: 235, height: 85)
        }
        else
        {
            self.layout.scrollDirection =  UICollectionViewScrollDirection.vertical
            self.layout.minimumInteritemSpacing = 1
            self.layout.minimumLineSpacing = 5
            self.layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
            self.layout.headerReferenceSize = CGSize(width: 10, height: 24)
            self.layout.itemSize = CGSize(width: 235, height: 85)
        }
    }
    //MARK: NEW POST
    func createNewPost(_ geography: ClusterItem)
    {
        let vcNewPost = NewPostModal()
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
        vcNewPost.geographyTagged = geography
        
        present(navController, animated: true, completion:nil)
    }
    //MARK: UTILITIES
    func showLoadingMode()
    {
        self.clusterItems.removeAll()
        self.sectionedGeographies.removeAll()
        self.collectionView?.reloadData()
        self.collectionView?.displayBackgroundMessage("Loading...".localized(), subMessage: String())
    }
    
    func cancelSearchRequests()
    {
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.getGeographiesLight)
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.getFollowedGeographiesLight)
    }
    fileprivate func indexGeographies()
    {
        //Cleanup
        self.sectionedGeographies.removeAll()
        self.sectionTitles.removeAll()
        
        //Obtain the first letters of the array elements
        let firstLetters = self.clusterItems.map({String($0.title.uppercased()[$0.title.uppercased().startIndex])})
        self.sectionTitles = Array(Set(firstLetters))
        
        //Order the characters from A-Z
        var alphabetChars = self.sectionTitles.filter({$0.asciiValue >= 65 && $0.asciiValue <= 91})
        alphabetChars = alphabetChars.sorted(by: {$0 < $1})
        
        //Order all characters different from A-Z
        var nonAlphabetChars = self.sectionTitles.filter({$0.asciiValue < 65 || $0.asciiValue > 91})
        nonAlphabetChars = nonAlphabetChars.sorted(by: {$0 < $1})
        
        //Update the titles used by the section table and the lateral index
        self.sectionTitles.removeAll()
        self.sectionTitles = nonAlphabetChars + alphabetChars
        
        //Use the titles to order the geographies
        for idxLetter in self.sectionTitles
        {
            self.sectionedGeographies.append(self.clusterItems.filter({ String($0.title.uppercased()[$0.title.uppercased().startIndex]) == idxLetter }))
        }
    }
    
    fileprivate func filterUpdateGeographies(geographies: [ClusterItem]) -> [ClusterItem]
    {
        var items = geographies
        
        for geo in self.geographiesAdded
        {
            items = items.filter({$0.id != geo.id})
        }
        
        return items
    }


    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        // #warning Incomplete implementation, return the number of sections
        return self.sectionedGeographies.count
    }
    
    


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        // #warning Incomplete implementation, return the number of items
        return self.sectionedGeographies[section].count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GeographyCollectionViewCell", for: indexPath) as? GeographyCollectionViewCell
        let currentItem = self.sectionedGeographies[indexPath.section][indexPath.row]
        cell?.cornerRadius()
        cell?.setBorder()
        cell?.lblTitle.text = currentItem.title
        cell?.lblSubtitle.text = currentItem.parentTitle
        cell?.onButtonTap = { [weak self] (button) in
        
            //Selected Style
            cell?.btnOptions.backgroundColor = UIColor.blueTag()
            cell?.btnOptions.tintColor = UIColor.white
            
            //Content View
            let vcTableOptions = StringTableViewController()
            vcTableOptions.modalPresentationStyle = .popover
            
            var options = currentItem.isFollowed == true ? ["Unfollow".localized()] : ["Follow".localized()]
            options.append("Post".localized())
            vcTableOptions.options = options
            vcTableOptions.onClosed = {
                
                //Unselected Style
                cell?.btnOptions.backgroundColor = UIColor.white
                cell?.btnOptions.tintColor = UIColor.blueTag()
            }
            vcTableOptions.onSelectedOption = {[weak self] row in
                if row == 0
                {
                    self?.updateFollowStatus(currentItem)
                }
                else if row == 1
                {
                    self?.createNewPost(currentItem)
                }
            }
            //Show Popover
            let popoverController = vcTableOptions.popoverPresentationController
            popoverController?.sourceView = cell
            popoverController?.sourceRect = button.frame
            popoverController?.permittedArrowDirections = .any
            
            self?.present(vcTableOptions, animated: true, completion: nil)
            
        }
        return cell ?? UICollectionViewCell()
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        //Just Allow Header type
        guard kind == UICollectionElementKindSectionHeader else {
            
            return UICollectionReusableView()
        }
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
        
        if indexPath.section < self.sectionTitles.count
        {
            header.lblTitle.text = self.sectionTitles[indexPath.section]
        }
        
        return header
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        let lastGeography = self.sectionedGeographies.last?.last
        let currentGeography = self.sectionedGeographies[indexPath.section][indexPath.row]
        if lastGeography?.id == currentGeography.id && (self.collectionMode == .all)
        {
            guard self.geoQuery.pageNumber < self.geoQuery.totalPages else {
                
                return
            }
            self.geoQuery.pageNumber += 1
            self.refreshCollection()
        }

        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))
        {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.responds(to: #selector(setter: UIView.layoutMargins))
        {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let currentItem = self.sectionedGeographies[indexPath.section][indexPath.row]
        let vcGeoDetail:GeographyDetail_iPad = UIStoryboard(name: "GeographyPad", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetail_iPad") as! GeographyDetail_iPad
        vcGeoDetail.geographyId = currentItem.id
        self.navigationController?.pushViewController(vcGeoDetail, animated: true)
    }

}
