//
//  ActionsMenu_iPad.swift
//  SHM
//
//  Created by Manuel Salinas on 11/13/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ActionsMenu_iPad: UITableViewController
{
    //MARK: VARIABLES
    fileprivate var actions = [MenuAction]()
    static let CELL_HEIGHT = CGFloat(44.0)
    
    //MARK:LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.modalPresentationStyle = .popover
        self.tableView.isScrollEnabled = false
        
        self.tableView.separatorColor = UIColor.blueSky()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Class functions
    class func getActionsButton(target: AnyObject, action: Selector) -> UIBarButtonItem
    {
        let btnActions = UIButton(frame: CGRect(x: 0, y: 0, width: 80, height: 24))
        btnActions.setTitle("Actions  ▾".localized(), for: UIControlState())
        btnActions.titleLabel!.font = UIFont.systemFont(ofSize: 14)
        btnActions.layer.borderColor = UIColor.whiteColor(0.4).cgColor
        btnActions.layer.borderWidth = 1
        btnActions.layer.cornerRadius = 3
        btnActions.addTarget(target, action: action, for: UIControlEvents.touchUpInside)
        
        return UIBarButtonItem(customView: btnActions)
    }
    
    // MARK: - Private Methods
    func addAction(_ action: MenuAction)
    {
        actions.append(action)
        preferredContentSize = CGSize(width: 320, height: ActionsMenu_iPad.CELL_HEIGHT * CGFloat(actions.count))
    }
    
    func openFromBarButtonItem(_ barBtnActions: UIBarButtonItem, inController controller: UIViewController)
    {
        self.modalPresentationStyle = .popover
        popoverPresentationController?.permittedArrowDirections = .any
        popoverPresentationController?.barButtonItem = barBtnActions
        
        controller.present(self, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.actions.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier:"Cell")

        cell.textLabel?.textColor = UIColor.blueTag()
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        cell.textLabel?.numberOfLines = 2
        cell.textLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        cell.tag = indexPath.row
        
        cell.textLabel?.text = self.actions[indexPath.row].title
        
        if let imageName = self.actions[indexPath.row].imageName
        {
            cell.imageView?.image = UIImage(named: imageName)
        }

        return cell
    }
    
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        actions[indexPath.row].action()
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        // Remove seperator inset
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset))
        {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))
        {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.responds(to: #selector(setter: UIView.layoutMargins))
        {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return ActionsMenu_iPad.CELL_HEIGHT
    }
    
    //MARK: Menu Action (Init)
    class MenuAction
    {
        var title: String
        var action: () -> ()
        var imageName: String?
        
        init(title: String, imageName: String, action: @escaping () -> ()) {
            self.title = title
            self.action = action
            self.imageName = imageName
        }
    }
}
