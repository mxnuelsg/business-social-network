//
//  MenuTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 9/10/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

fileprivate enum LateralMenu: Int
{
    case globalSearch = 0
    case inbox = 1
    case stakeholders = 2
    case projects = 3
    case geography = 4
    case calendar = 5
    case settings = 6
    case profile = 7
    case report = 8
    case reportRelations = 9
    case reportMyPosts = 10
    case reportConnections = 11
    case reportCompanyConnections = 12
    case logout = 13
}

class MenuTableViewController: UITableViewController
{
    //MARK: OUTLETS AND PROPERTIES
    @IBOutlet weak var searchBar: AutoSearchBar!
    @IBOutlet weak var lblInboxBadge: UILabel!
    @IBOutlet weak var btnFeedback: UIButton!
    
    @IBOutlet weak var lblVersion: UILabel!
    var isReportOpen = false
    @IBOutlet weak var imgReportArrow: UIImageView!
    
    //Cell content labels
    @IBOutlet weak var lblMenuInbox: UILabel!
    @IBOutlet weak var lblMenuStakeholders: UILabel!
    @IBOutlet weak var lblMenuProject: UILabel!
    @IBOutlet weak var lblMenuCalendar: UILabel!
    @IBOutlet weak var lblMenuSettings: UILabel!
    @IBOutlet weak var lblMenuProfile: UILabel!
    @IBOutlet weak var lblMenuReport: UILabel!
    @IBOutlet weak var lblMenuRelations: UILabel!
    @IBOutlet weak var lblMenuMyPosts: UILabel!
    @IBOutlet weak var lblMenuRelationMap: UILabel!
    @IBOutlet weak var lblMenuHowToConnect: UILabel!
    @IBOutlet weak var lblMenuLogout: UILabel!
    
    @IBOutlet weak var lblGeography: UILabel!
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        backButtonArrow()
        
        if let unreadCount = LibraryAPI.shared.currentUser?.unReadInbox {
            if unreadCount > 0 {
                lblInboxBadge.text = "\(unreadCount)"
                lblInboxBadge.isHidden = false
            }
            else {
                lblInboxBadge.text = "0"
                lblInboxBadge.isHidden = true
            }
        }
    }
    
    func inboxReceived()
    {
        if let user = LibraryAPI.shared.currentUser, user.isLoggedIn == true  {
            
                lblInboxBadge.text = "\(LibraryAPI.shared.currentUser!.unReadInbox)"
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "userDidLogin"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "inboxReceived"), object: nil)
    }
    
    //MARK: CONFIG
    func loadConfig()
    {
        //initialize AutoSearchBar
        searchBar.initialize(shouldSearchOnlyWithSearchButton: true,
        onSearchText: { [weak self] (text) in
            self?.search()
        })
        
        //Localizations
        self.localize()
        
        //Remove separator below
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        //Config SearchBar
        searchBar.placeholder = "Search in SHM".localized()
        
        //Config Badge
        lblInboxBadge.isHidden = true
        lblInboxBadge.clipsToBounds = true
        lblInboxBadge.layer.cornerRadius = 10
        lblInboxBadge.isHidden = true
        
        //Version Info
        self.setVersionInformation()
        
        //Feedback button
        self.btnFeedback.addTarget(self, action: #selector(self.openFeedback), for: .touchUpInside)
        self.btnFeedback.setTitle("Feedback".localized(), for: UIControlState())
        self.btnFeedback.layer.cornerRadius = 3
        self.btnFeedback.clipsToBounds = true
        loadCounterInbox()
        
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.userDidLogin), name: NSNotification.Name(rawValue: "userDidLogin"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.inboxReceived), name: NSNotification.Name(rawValue: "inboxReceived"), object: nil)
    }
    
    //MARK: LOCALIZE
    fileprivate func localize()
    {
        if DeviceType.IS_ANY_IPAD == false
        {
            self.lblMenuInbox.text = "INBOX".localized()
            self.lblMenuStakeholders.text = "STAKEHOLDERS".localized()
            self.lblMenuProject.text = "PROJECTS".localized()
            self.lblMenuCalendar.text = "CALENDAR".localized()
            self.lblMenuSettings.text = "SETTINGS".localized()
            self.lblMenuProfile.text = "MY PROFILE".localized()
            self.lblMenuReport.text = "REPORT".localized()
            self.lblMenuRelations.text = "  \("RELATIONSHIP MAP".localized())"
            self.lblMenuMyPosts.text = "  MY POSTS".localized()
            self.lblMenuRelationMap.text = "  \("CONNECTIONS".localized())"
            self.lblMenuHowToConnect.text = "COMPANY CONNECTIONS".localized()
            self.lblGeography.text = "GEOGRAPHIES".localized()
            self.lblMenuLogout.text = "LOG OUT".localized()
        }
    }
    
    func setVersionInformation()
    {
        if let versionLabel = self.lblVersion {
        
            //Version Information
            let strAppVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
            versionLabel.text = "Version \(strAppVersion)"
            self.lblVersion.text = versionLabel.text
        }
    }
    
    
    //MARK: - Private Methods
    func openFeedback()
    {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "goToFeedback"), object: nil)
        
        //Close Menu
        perform(#selector(self.closeMenu), with:nil, afterDelay: 0)
    }
    
    func userDidLogin()
    {
        tableView.reloadData()
    }

    //MARK: COUNTER INBOX
    func loadCounterInbox()
    {
        LibraryAPI.shared.inboxBO.getCountForInbox({
            (count) -> () in
            
            self.lblInboxBadge.text = "\(count)"
            self.lblInboxBadge.isHidden = (count > 0) ? false : true
            
            }) { error in
                print(error)
                self.lblInboxBadge.text = "!"
        }
    }

    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        //Selected Option
        switch indexPath.row
        {
        case LateralMenu.inbox.rawValue:
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToInbox"), object: nil)
            
        case LateralMenu.stakeholders.rawValue:
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToStakeHolders"), object: nil)
            
        case LateralMenu.projects.rawValue:
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToProyects"), object: nil)
            
        case LateralMenu.calendar.rawValue:
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToCalendar"), object: nil)
            
        case LateralMenu.geography.rawValue:
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToGeography"), object: nil)
            
        case LateralMenu.settings.rawValue:
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToSettings"), object: nil)
            
        case LateralMenu.profile.rawValue:
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToProfile"), object: nil)
            
        case LateralMenu.report.rawValue:

            self.isReportOpen = self.isReportOpen == true ? false : true
            self.imgReportArrow.image = self.isReportOpen ? UIImage(named:"iconArrowUp"):UIImage(named:"iconArrowDown")
            self.tableView.reloadData()
            
            //Light menu options
            tableView.dequeueReusableCell(withIdentifier: "Reports_Relations")?.contentView.backgroundColor = isReportOpen ? UIColor.blackAsfalto() : UIColor.blueSearchBar()
            
            tableView.dequeueReusableCell(withIdentifier: "Reports_MyPosts")?.contentView.backgroundColor = isReportOpen ? UIColor.blackAsfalto() : UIColor.blueSearchBar()
            
            tableView.dequeueReusableCell(withIdentifier: "Reports_Connections")?.contentView.backgroundColor = isReportOpen ? UIColor.blackAsfalto() : UIColor.blueSearchBar()
            
            tableView.dequeueReusableCell(withIdentifier: "Reports_StakeholderConnect")?.contentView.backgroundColor = isReportOpen ? UIColor.blackAsfalto() : UIColor.blueSearchBar()
            
            return
            
        case LateralMenu.reportRelations.rawValue:
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToRelations"), object: nil)
            
        case LateralMenu.reportMyPosts.rawValue:
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToMyPosts"), object: nil)
            
        case LateralMenu.reportConnections.rawValue:
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToConnections"), object: nil)
            
        case LateralMenu.reportCompanyConnections.rawValue:
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToStakeholderConnect"), object: nil)
            
        case LateralMenu.logout.rawValue:
            
            self.imgReportArrow.image = UIImage(named:"iconArrowDown")            
            self.isReportOpen = false
            self.tableView.reloadData()
            NotificationCenter.default.post(name: Notification.Name(rawValue: "logout"), object: nil)
            
        default:
            return
        }
        
        //Close Menu
        perform(#selector(self.closeMenu), with:nil, afterDelay: 0)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        cell.backgroundColor = UIColor.clear
        
        // Remove seperator inset
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset))
        {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))
        {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.responds(to: #selector(setter: UIView.layoutMargins))
        {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    //MARK: Search
    func search()
    {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "goToSearch"), object: nil, userInfo: ["SearchKeyWord" : searchBar.text!])
        perform(#selector(self.closeMenu), with:nil, afterDelay: 0)
        
        searchBar.text = String()
        searchBar.resignFirstResponder()
    }
    
      // MARK: Close Menu
    func closeMenu()
    {
        if self.revealViewController() != nil
        {
            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if ((indexPath.row == LateralMenu.reportRelations.rawValue
            || indexPath.row == LateralMenu.reportMyPosts.rawValue
            || indexPath.row == LateralMenu.reportConnections.rawValue
            || indexPath.row == LateralMenu.reportCompanyConnections.rawValue)
            && self.isReportOpen == false)
        {
            return CGFloat.leastNormalMagnitude
        }

        else
        {
            return 44
        }
    }
}
