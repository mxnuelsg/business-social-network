//
//  MenuTableViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 1/6/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class MenuTableViewController_iPad: MenuTableViewController
{

    //MARK : Properties
    var isProfileOpen = false

    @IBOutlet weak var imgReportArrow_ipad: UIImageView!
    
    
    //Lateral menu
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var imgProfileOpen: UIImageView!
    @IBOutlet weak var lblVersion_iPad: UILabel!
    
    @IBOutlet weak var lblMyProfile_iPad: UILabel!
    @IBOutlet weak var lblSettings_iPad: UILabel!
    
    @IBOutlet weak var lblLogout_iPad: UILabel!
    @IBOutlet weak var lblHome_iPad: UILabel!
    @IBOutlet weak var lblInbox_iPad: UILabel!
    @IBOutlet weak var lblStakeholders_iPad: UILabel!
    @IBOutlet weak var lblProjects_iPad: UILabel!
    @IBOutlet weak var lblCalendar_iPad: UILabel!
    @IBOutlet weak var lblReports_iPad: UILabel!
    @IBOutlet weak var lblRelations_iPad: UILabel!
    @IBOutlet weak var lblMyPost_iPad: UILabel!
    @IBOutlet weak var lblSHRelationMap: UILabel!
    
    @IBOutlet weak var lblHowToConnect_iPad: UILabel!
    
    @IBOutlet weak var lblGeography_iPad: UILabel!
    
    //MARK : LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //Version Information
        self.setVersionInformation_iPad()
        
        //localizations
        self.localize()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if let username = LibraryAPI.shared.currentUser?.userName
        {
            self.lblUsername.text = username
        }
        if let unreadCount = LibraryAPI.shared.currentUser?.unReadInbox {
            if unreadCount > 0 {
                lblInboxBadge.text = "\(unreadCount)"
                lblInboxBadge.isHidden = false
            }
            else {
                lblInboxBadge.text = "0"
                lblInboxBadge.isHidden = true
            }
        }
    }
    
    //MARK: LOCALIZE
    fileprivate func localize()
    {
        self.lblMyProfile_iPad.text = "My Profile".localized()
        self.lblLogout_iPad.text = "Log Out".localized()
        self.lblSettings_iPad.text = "SETTINGS".localized()
        self.lblHome_iPad.text = "HOME".localized()
        self.lblInbox_iPad.text = "INBOX".localized()
        self.lblStakeholders_iPad.text = "STAKEHOLDERS".localized()
        self.lblProjects_iPad.text = "PROJECTS".localized()        
        self.lblCalendar_iPad.text = "CALENDAR".localized()
        
        self.lblReports_iPad.text = "REPORTS".localized()
        self.lblRelations_iPad.text = "RELATIONSHIP MAP".localized()
        self.lblMyPost_iPad.text = "MY POSTS".localized()
        lblSHRelationMap.text = "CONNECTIONS".localized()
        self.lblHowToConnect_iPad.text = "COMPANY CONNECTIONS".localized()
        self.lblGeography_iPad.text = "GEOGRAPHIES".localized()
        
    }
    
    func setVersionInformation_iPad()
    {
        if let versionLabel = self.lblVersion_iPad {
            
            let strAppVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
            versionLabel.text = "Version \(strAppVersion)"
            self.lblVersion_iPad.text = versionLabel.text
        }
    }
    
    // MARK: Close Menu
    override func closeMenu()
    {
        if self.revealViewController() != nil
        {
            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }
    }
    
    //MARK: Search
    override func search()
    {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "goToSearch_iPad"), object: nil, userInfo: ["SearchKeyWord" : searchBar.text!])
        perform(#selector(MenuTableViewController.closeMenu), with:nil, afterDelay: 0)

        searchBar.text = String()
        searchBar.resignFirstResponder()
    }
    
    //MARK : TABLE VIEW DELEGATE
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row
        {
        case 1:
            
            perform(#selector(self.closeMenu), with:nil, afterDelay: 0)
        case 2:
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToInbox"), object: nil)
            perform(#selector(self.closeMenu), with:nil, afterDelay: 0)
        case 3:
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToStakeHolders"), object: nil)
            perform(#selector(self.closeMenu), with:nil, afterDelay: 0)
        case 4:
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToProyects"), object: nil)
            perform(#selector(self.closeMenu), with:nil, afterDelay: 0)
        case 5:
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToGeography"), object: nil)
            perform(#selector(self.closeMenu), with:nil, afterDelay: 0)
        case 6:
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToCalendar"), object: nil)
            perform(#selector(self.closeMenu), with:nil, afterDelay: 0)
        case 7:
            print("REPORT")
            self.isReportOpen = self.isReportOpen == true ? false : true
            self.imgReportArrow_ipad.image = self.isReportOpen ? UIImage(named:"iconArrowUp"):UIImage(named:"iconArrowDown")
            self.tableView.reloadData()
            //Light menu options
            
            
            tableView.dequeueReusableCell(withIdentifier: "Reports_Relations", for: indexPath).contentView.backgroundColor = isReportOpen ? UIColor.blueSearchBar() : UIColor.blackAsfalto()
            
            tableView.dequeueReusableCell(withIdentifier: "Reports_MyPosts", for: indexPath).contentView.backgroundColor = isReportOpen ? UIColor.blueSearchBar() : UIColor.blackAsfalto()
            
            tableView.dequeueReusableCell(withIdentifier: "Reports_Connections", for: indexPath).contentView.backgroundColor = isReportOpen ? UIColor.blueSearchBar() : UIColor.blackAsfalto()
            
            tableView.dequeueReusableCell(withIdentifier: "Reports_StakeholderConnect", for: indexPath).contentView.backgroundColor = isReportOpen ? UIColor.blueSearchBar() : UIColor.blackAsfalto()
        case 8:
            
            print("RELATIONS")
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToRelations"), object: nil)
            perform(#selector(self.closeMenu), with:nil, afterDelay: 0)
            
        case 9:
            print("MY POSTS")
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToMyPosts"), object: nil)
            perform(#selector(self.closeMenu), with:nil, afterDelay: 0)
            
 
        case 10:
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToConnections"), object: nil)
            perform(#selector(self.closeMenu), with:nil, afterDelay: 0)
            
        case 11:
            
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToStakeholderConnect"), object: nil)
            perform(#selector(self.closeMenu), with:nil, afterDelay: 0)
            
        case 12:
            self.isProfileOpen = self.isProfileOpen ? false : true
            self.imgProfileOpen.image = self.isProfileOpen ? UIImage(named:"iconArrowUp"):UIImage(named:"iconArrowDown")
            self.tableView.reloadData()
            
        case 13:
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToProfile"), object: nil)
            perform(#selector(self.closeMenu), with:nil, afterDelay: 0)
        case 14:
            
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "goToSettings"), object: nil)
            perform(#selector(self.closeMenu), with:nil, afterDelay: 0)
        case 15:
            
            //Close Embedded menus before leaving
            self.isProfileOpen = false
            self.imgProfileOpen.image = UIImage(named:"iconArrowDown")
            self.isReportOpen = false
            self.imgReportArrow_ipad.image = UIImage(named:"iconArrowDown")
            NotificationCenter.default.post(name: Notification.Name(rawValue: "logout"), object: nil)
            perform(#selector(self.closeMenu), with:nil, afterDelay: 0)
            
        default:
                        
            return
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        //Profile embedded options
        if ((indexPath.row == 13 || indexPath.row == 14 || indexPath.row == 15) && (self.isProfileOpen==false))
        {
            return 0.0
        }
        
        //Report embedded options
        if ((indexPath.row == 8 || indexPath.row == 9 || indexPath.row == 10 || indexPath.row == 11) && !isReportOpen)
        {
            return 0.0
        }
        
        return 44.0
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
}
