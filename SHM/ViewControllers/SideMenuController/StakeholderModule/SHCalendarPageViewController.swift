//
//  SHCalendarPageViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 10/15/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class SHCalendarPageViewController: UIViewController
{
    var stakeholderId = 0 {
        didSet{
            
        }
    }
    var vcCalendar: EmbededCalendarViewController?
    var vcCalendarList: EmbededCalendarTableViewController?
    var calendarItems = [CalendarItem]() {
        didSet {
            self.loadCalendars()
        }
    }
    
    @IBOutlet weak var listContainer: UIView!
    @IBOutlet weak var calendarContainer: UIView!
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.getStakeholderCalendarEvents(Date())
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        print("deinit SHCalendarPageViewController")
    }
    //MARK: CONFIGURATION
    fileprivate func loadConfig()
    {
        self.calendarContainer.isHidden = false
        self.listContainer.isHidden = true
    }
    
    //MARK: WEB SERVICES
    fileprivate func getStakeholderCalendarEvents(_ date: Date)
    {
        //Cancel previous request
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.GetCalendarEvents)
        
        //Object to request
        var wsObject: [String : Any]  = [:]
        wsObject["StakeholderId"] = self.stakeholderId
        wsObject["ProjectId"] = 0
        wsObject["Month"] = date.month
        wsObject["Year"] = date.year
        
        LibraryAPI.shared.calendarBO.getEvents(wsObject, onSuccess: { (CalendarItems) in
            
            self.calendarItems = CalendarItems
        }) { (error) in
            
        }
    }
    
    //MARK: ACTIONS
    fileprivate func loadCalendars()
    {
        //LOAD CALENDAR VIEW
        self.vcCalendar?.arrayCalendarItems = self.calendarItems
        self.vcCalendar?.calendar.reloadData()
    }
    
    func updateCalendarViews(date: Date)
    {
        //Update Calendar View
        self.getStakeholderCalendarEvents(date)
        
        //Update Calendar List View
        self.vcCalendarList?.ultimateDate = date.getStartOfDay().convertToRequestString()
        self.vcCalendarList?.arrayCalendarList.removeAll()
        self.vcCalendarList?.loadCalendarListById()
    }
    
    func reloadCalendar()
    {
        self.vcCalendar?.ID = self.stakeholderId
        self.vcCalendarList?.ID = self.stakeholderId
    }
    func flipCalendarView()
    {
        if self.listContainer.isHidden == false
        {
            self.listContainer.isHidden = true
            self.calendarContainer.isHidden = false
        }
        else
        {
            self.listContainer.isHidden = false
            self.calendarContainer.isHidden = true
        }
    }
    //MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "calendar"
        {
            if let calendar = segue.destination as? EmbededCalendarViewController {
                
                self.vcCalendar = calendar
                self.vcCalendar?.calendarType = .StakeholderDetail
                self.vcCalendar?.ID = self.stakeholderId
                self.vcCalendar?.onMonthChanged = {[weak self] date in
                    
                    self?.updateCalendarViews(date: date)
                }
            }
        }
        else if segue.identifier == "List"
        {
            if let list = segue.destination as? EmbededCalendarTableViewController {
             
                self.vcCalendarList = list
                self.vcCalendarList?.ultimateDate = Date().firstDayOfMonth().getStartOfDay().convertToRequestString()
                self.vcCalendarList?.contentType = .StakeholderDetail
                self.vcCalendarList?.ID = self.stakeholderId
            }
        }
    }
}
