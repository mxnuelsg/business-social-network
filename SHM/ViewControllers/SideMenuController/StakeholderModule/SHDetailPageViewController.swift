//
//  SHDetailPageViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 10/15/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class SHDetailPageViewController: UIViewController
{
    //MARK: Outles and variables
    var stakeholderId: Int = 0 {
        didSet {
            
            self.loadStakeholderDetailLight()
            self.loadStakeholderProjects()
            self.loadStakeholderRelations()
            self.loadStakeholderGeographies()
        }
    }
    
    //STAKEHOLDER DETAIL TABS
    var vcProfile: StakeholderProfileSectionTable_iPad?
    @IBOutlet weak var profileContainer: UIView!
    
    var vcProjects: ProjectsTableViewController?
    @IBOutlet weak var projectContainer: UIView!
    
    var vcRelations:RelationsTableViewController?
    @IBOutlet weak var relationsContainer: UIView!
    
    var vcGeographies:GeoTableViewController?
    @IBOutlet weak var geographiesContainer: UIView!
       
    //Segmented control
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    //Closures
    var onDidLoadStakeholderLight:((_ stakeholder: Stakeholder)->())?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        print("deinit SHDetailPageViewController")
    }
    
    //MARK: CONFIGURATIONS
    fileprivate func loadConfig()
    {
        self.segmentedControl.setTitle("Profile".localized(), forSegmentAt: 0)
        self.segmentedControl.setTitle("Projects".localized(), forSegmentAt: 1)
        self.segmentedControl.setTitle("Relations".localized(), forSegmentAt: 2)
        self.segmentedControl.setTitle("Geographies".localized(), forSegmentAt: 3)
        
        self.setTabsVisibility()
    }
    
    fileprivate func setTabsVisibility()
    {
        let tab =  self.segmentedControl.selectedSegmentIndex
        switch tab
        {
        case 0:
            self.profileContainer.isHidden = false
            self.projectContainer.isHidden = true
            self.relationsContainer.isHidden = true
            self.geographiesContainer.isHidden = true
        case 1:
            self.profileContainer.isHidden = true
            self.projectContainer.isHidden = false
            self.relationsContainer.isHidden = true
            self.geographiesContainer.isHidden = true
        case 2:
            self.profileContainer.isHidden = true
            self.projectContainer.isHidden = true
            self.relationsContainer.isHidden = false
            self.geographiesContainer.isHidden = true
        case 3:
            self.profileContainer.isHidden = true
            self.projectContainer.isHidden = true
            self.relationsContainer.isHidden = true
            self.geographiesContainer.isHidden = false
        default:
            break
        }
        
    }
    
    @IBAction func onTabIndexChanged(_ sender: Any)
    {
        self.setTabsVisibility()
    }
    
    //MARK: WEB SERVICES
    func loadStakeholderDetailLight()
    {
        MessageManager.shared.showLoadingHUD()
        LibraryAPI.shared.stakeholderBO.getStakeholderDetailLight(self.stakeholderId, onSuccess: { (stakeholder) in
            
            MessageManager.shared.hideHUD()
            self.vcProfile?.stakeholder = stakeholder
            self.onDidLoadStakeholderLight?(stakeholder)
        }) { (error) in
            
            MessageManager.shared.hideHUD()
            MessageManager.shared.showBar(title: "There was an error while loading the stakeholder information".localized(),
                                          subtitle: String(),
                                          type: .error,
                                          fromBottom: false)
        }
    }
    
    @objc fileprivate func loadStakeholderProjects()
    {
        self.vcProjects?.tableView.displayBackgroundMessage("Loading...".localized(), subMessage: String())
        LibraryAPI.shared.stakeholderBO.getStakeholdersProjects(self.stakeholderId , onSuccess: { (projects) in
            
            //UI update
            self.vcProjects?.tableView.dismissBackgroundMessage()
            self.vcProjects?.refreshControl?.setLastUpdate()
            self.vcProjects?.refreshControl?.endRefreshing()
            if projects.count == 0
            {
                self.vcProjects?.tableView.displayBackgroundMessage("No data available".localized(), subMessage: "Pull to refresh".localized())
            }
            
            //Update table
            self.vcProjects?.projects = projects
            self.vcProjects?.reload()
        }) { (error) in
            
            self.vcProjects?.projects.removeAll()
            self.vcProjects?.reload()
            self.vcProjects?.refreshControl?.endRefreshing()
            self.vcProjects?.tableView.displayBackgroundMessage("An error ocurred".localized(), subMessage: "Pull to refresh".localized())
        }
    }
    
    @objc fileprivate func loadStakeholderRelations()
    {
        self.vcRelations?.tableView.displayBackgroundMessage("Loading...".localized(), subMessage: String())
        LibraryAPI.shared.stakeholderBO.getStakeholdersRelations(self.stakeholderId, onSuccess: { (relations) in
            
            //UI update
            self.vcRelations?.tableView.dismissBackgroundMessage()
            self.vcRelations?.refreshControl?.setLastUpdate()
            self.vcRelations?.refreshControl?.endRefreshing()
            if relations.count == 0
            {
                self.vcRelations?.tableView.displayBackgroundMessage("No data available".localized(), subMessage: "Pull to refresh".localized())
            }
            
            //Update table
            self.vcRelations?.relations = relations
            self.vcRelations?.tableView.reloadData()
        }) { (error) in
            self.vcRelations?.relations.removeAll()
            self.vcRelations?.tableView.reloadData()
            self.vcRelations?.refreshControl?.endRefreshing()
            self.vcRelations?.tableView.displayBackgroundMessage("An error ocurred".localized(), subMessage: "Pull to refresh".localized())
        }
    }
    
    @objc fileprivate func loadStakeholderGeographies()
    {
        self.vcGeographies?.tableView.displayBackgroundMessage("Loading...".localized(), subMessage: String())
        LibraryAPI.shared.stakeholderBO.getStakeholdersGeographies(self.stakeholderId, onSuccess: { (geographies) in
        
            //UI update
            self.vcGeographies?.tableView.dismissBackgroundMessage()
            self.vcGeographies?.refreshControl?.setLastUpdate()
            self.vcGeographies?.refreshControl?.endRefreshing()
            if geographies.count == 0
            {
                self.vcGeographies?.tableView.displayBackgroundMessage("No data available".localized(), subMessage: "Pull to refresh".localized())
            }
            
            //Update table
            self.vcGeographies?.clusterItems = geographies
            self.vcGeographies?.backupClusterItems = geographies
        }) { (error) in
            self.vcGeographies?.clusterItems.removeAll()
            self.vcGeographies?.tableView.reloadData()
            self.vcGeographies?.refreshControl?.endRefreshing()
            self.vcGeographies?.tableView.displayBackgroundMessage("An error ocurred".localized(), subMessage: "Pull to refresh".localized())
        }
    }
    //MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "Profile"
        {
            if let profile = segue.destination as? StakeholderProfileSectionTable_iPad {
            
                self.vcProfile = profile
            }
        }
        if segue.identifier == "projects"
        {
            if let projects = segue.destination as? ProjectsTableViewController {
                
                self.vcProjects = projects
                self.vcProjects?.refreshControl = UIRefreshControl()
                self.vcProjects?.refreshControl?.setLastUpdate()
                self.vcProjects?.refreshControl?.addTarget(self, action: #selector(self.loadStakeholderProjects), for: UIControlEvents.valueChanged)
            }
        }
        if segue.identifier == "relations"
        {
            if let relations = segue.destination as? RelationsTableViewController {
                
                self.vcRelations = relations
                self.vcRelations?.refreshControl = UIRefreshControl()
                self.vcRelations?.refreshControl?.setLastUpdate()
                self.vcRelations?.refreshControl?.addTarget(self, action: #selector(self.loadStakeholderRelations), for: UIControlEvents.valueChanged)
            }
        }
        if segue.identifier == "geographies"
        {
            if let geographies = segue.destination as? GeoTableViewController {
                
                self.vcGeographies = geographies
                self.vcGeographies?.ownerModule = .stakeholderDetail
                self.vcGeographies?.refreshControl = UIRefreshControl()
                self.vcGeographies?.refreshControl?.setLastUpdate()
                self.vcGeographies?.refreshControl?.addTarget(self, action: #selector(self.loadStakeholderGeographies), for: UIControlEvents.valueChanged)
            }
        }
    }
}
