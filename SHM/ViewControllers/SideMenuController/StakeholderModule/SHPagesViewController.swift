//
//  SHPagesViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 10/15/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class SHPagesViewController: UIPageViewController
{
    var stakeholderId: Int = 0
    
    var vcSHDetailPage: SHDetailPageViewController?
    var vcSHFeedPage: SHFeedPageViewController?
    var vcSHCalendarPage: SHCalendarPageViewController?
    var viewControllerList:[UIViewController]?
    
    //CLOSURES
    var onDidChangePage:((_ page: Int)->())?
    var onDidLoadStakeholderLight:((_ stakeholder: Stakeholder)->())?
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.setStakeholderId()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit
    {
        print("deinit SHPagesViewController")
    }
    fileprivate func loadConfig()
    {
        self.dataSource = self
        self.delegate = self        
        self.setVCList()
        if let firstViewController = self.viewControllerList?.first {
            
            self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    fileprivate func setVCList()
    {
        let sb = UIStoryboard(name: "Stakeholder", bundle: nil)
        self.vcSHDetailPage = sb.instantiateViewController(withIdentifier: "SHDetailPageViewController") as? SHDetailPageViewController
        self.vcSHFeedPage = sb.instantiateViewController(withIdentifier: "SHFeedPageViewController") as? SHFeedPageViewController
        vcSHDetailPage?.onDidLoadStakeholderLight = { [weak self] stakeholder in
            
            self?.onDidLoadStakeholderLight?(stakeholder)
        }
        self.vcSHCalendarPage = sb.instantiateViewController(withIdentifier: "SHCalendarPageViewController") as? SHCalendarPageViewController
        
        self.viewControllerList = [self.vcSHDetailPage!, self.vcSHFeedPage!, self.vcSHCalendarPage!]
    }
    
    fileprivate func setStakeholderId()
    {
        self.vcSHDetailPage?.stakeholderId = self.stakeholderId
        self.vcSHFeedPage?.stakeholderId = self.stakeholderId
        self.vcSHCalendarPage?.stakeholderId = self.stakeholderId
    }
}

extension SHPagesViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate
{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        guard let vcIndex = viewControllerList?.index(of: viewController) else {return nil}
        
        let previousIndex = vcIndex - 1
        guard previousIndex >= 0 else {return nil}
        guard (viewControllerList?.count)! > previousIndex else {return nil}
        
        
        return viewControllerList?[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        guard let vcIndex = viewControllerList?.index(of: viewController) else {return nil}
        
        let nextIndex = vcIndex + 1
        guard viewControllerList?.count != nextIndex else {return nil}
        guard (viewControllerList?.count)! > nextIndex else {return nil}
        
        return viewControllerList?[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        guard completed == true else {return}
        if let page = pageViewController.viewControllers?.first?.view.tag {
            
            self.onDidChangePage?(page)
            print(page)
        }
    }        
}
