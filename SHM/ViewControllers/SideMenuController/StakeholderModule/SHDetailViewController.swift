//
//  SHDetailViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 10/15/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class SHDetailViewController: UIViewController
{
    //MARK: Variables and outlets
    var stakeholder: Stakeholder = Stakeholder(isDefault:true){
        didSet{
            if self.isViewLoaded == true {
            
                self.loadStakeholderInfo()
            }
        }
    }
    var vcPagesController: SHPagesViewController?
    
    //HEADER OUTLETS
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var lblPublicRating: UILabel!
    @IBOutlet weak var lblRateAction: UILabel!
    @IBOutlet weak var lblAdminRating: UILabel!
    @IBOutlet weak var viewRateAction: UIView!
    @IBOutlet weak var lblPublicRatingTitle: UILabel!
    @IBOutlet weak var lblAdminRatingTitle: UILabel!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var btnEnable: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lblPageTitle: UILabel!
    
    //FLAGS
    var isEnglish = true
    
    //SEARCH BAR
    fileprivate lazy var searchBar: AutoSearchBar = AutoSearchBar()
    
    //TAB BAR
    @IBOutlet weak var tabBar: UITabBar!
    lazy var tabBarItems_Detail: [UITabBarItem] = {
        
        let icon1 = UITabBarItem(title:"Stakeholder Post".localized(), image: UIImage(named: "NewPost"), tag: 0)
        icon1.tag = 1
        let icon2 = UITabBarItem(title: "Request Update".localized(), image: UIImage(named: "Request"), tag: 0)
        let icon3 = UITabBarItem(title: "Language".localized(), image: UIImage(named: "LanguageEsp"), tag: 2)
        let icon4 = UITabBarItem(title: "Search in Stakeholder".localized(), image: UIImage(named: "Search"), tag: 0)
        return [icon1, icon2, icon3, icon4]
    }()
    
    lazy var tabBarItems_Calendar: [UITabBarItem] = {
        
        let icon1 = UITabBarItem(title:"Stakeholder Post".localized(), image: UIImage(named: "NewPost"), tag: 0)
        icon1.tag = 1
        let icon2 = UITabBarItem(title: "Request Update".localized(), image: UIImage(named: "Request"), tag: 0)
        let icon3 = UITabBarItem(title: "List".localized(), image: UIImage(named: "DayView"), tag: 2)
        let icon4 = UITabBarItem(title: "Search in Stakeholder".localized(), image: UIImage(named: "Search"), tag: 0)
        
        return [icon1, icon2, icon3, icon4]
    }()
    lazy var tabBarItems_CalendarList: [UITabBarItem] = {
        
        let icon1 = UITabBarItem(title:"Stakeholder Post".localized(), image: UIImage(named: "NewPost"), tag: 0)
        let icon2 = UITabBarItem(title: "Request Update".localized(), image: UIImage(named: "Request"), tag: 0)
        let icon3 = UITabBarItem(title: "Calendar".localized(), image: UIImage(named: "Calendar"), tag: 1)
        let icon4 = UITabBarItem(title: "Search in Stakeholder".localized(), image: UIImage(named: "Search"), tag: 0)
        
        return [icon1, icon2, icon3, icon4]
    }()
    
    lazy var tabBarItems_feed: [UITabBarItem] = {
        
        let icon1 = UITabBarItem(title:"Stakeholder Post".localized(), image: UIImage(named: "NewPost"), tag: 0)
        let icon2 = UITabBarItem(title: "Request Update".localized(), image: UIImage(named: "Request"), tag: 0)
        let icon3 = UITabBarItem(title: "Search in Stakeholder".localized(), image: UIImage(named: "Search"), tag: 0)
        
        return [icon1, icon2, icon3]
    }()
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.loadStakeholderInfo()
    }
    
    deinit
    {
        print("deinit stakeholder detail")
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: LOAD CONFIGS
    fileprivate func loadConfig()
    {
        //Nav Bar
        self.setupNavigation(animated: false)
        //Rating
        self.lblPublicRatingTitle.text = "User Rating".localized()
        self.lblPublicRatingTitle.adjustsFontSizeToFitWidth = true
        self.lblPublicRating.adjustsFontSizeToFitWidth = true
        self.lblAdminRatingTitle.text = "Admin Rating".localized()
        self.lblAdminRatingTitle.adjustsFontSizeToFitWidth = true
        self.lblAdminRating.adjustsFontSizeToFitWidth = true
        self.viewRateAction.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapOnRating)))
        self.lblRateAction.text = "Rate".localized()
        
        //set tab bar
        self.tabBar.delegate = self
        self.tabBar.setItems(tabBarItems_Detail, animated: false)
        self.updateEnableStatusForPostItem()
        
        //Btn Follow
        self.btnFollow.layer.cornerRadius = 2
        self.btnFollow.clipsToBounds = true
        self.btnFollow.setTitle("● ● ● ●", for: UIControlState())
        
        //Btn Disable
        self.btnEnable.isHidden = true
        if LibraryAPI.shared.currentUser?.role == UserRole.globalManager
        {
            self.btnEnable.layer.cornerRadius = 2
            self.btnEnable.clipsToBounds = true
            self.btnEnable.setTitle("● ● ● ●", for: UIControlState())
            self.btnEnable.addTarget(self, action: #selector(self.enableOrDisableStakeholder), for: .touchUpInside)
            self.btnEnable.isHidden = false
        }
        
        //Search
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        searchBar.initialize(onSearchText: { [weak self] (text) in
            
            let currentPage = self?.pageControl.currentPage ?? 0            
            if (self != nil) {
                self!.updateSearch(text: text, page: currentPage)
            }
            }, onCancel: { [weak self] in
                
                let currentPage = self?.pageControl.currentPage ?? 0
                self?.searchBar.text = ""
                self?.updateSearch(text: "", page: currentPage)
                self?.setupNavigation(animated: true)
        })
    }
    
    
    func setupNavigation(animated: Bool)
    {
        title = "STAKEHOLDER".localized()
        navigationItem.setHidesBackButton(false, animated: animated)
        navigationItem.fadeOutTitleView()
    }
    
    //MARK: ACTIONS
    fileprivate func loadStakeholderInfo()
    {
        //Follow
        self.imgProfile.setImageWith(URL(string: stakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        
        //Title
        self.lblName.text = self.stakeholder.fullName
        self.lblName.adjustsFontSizeToFitWidth = true
        
        //Position / Company
        let strJobPosition = self.isEnglish == true ? stakeholder.jobPosition!.valueEn : stakeholder.jobPosition!.valueEs
        self.lblInfo.text = strJobPosition.trim().isEmpty && stakeholder.companyName.trim().isEmpty ? "" : "\(strJobPosition) / \(stakeholder.companyName)"
        self.lblInfo.adjustsFontSizeToFitWidth = true
        
        //Following status
        if stakeholder.isFollow == FollowStatus.following
        {
            self.btnFollow.setTitle("︎︎︎︎︎✔︎ " + "Following".localized(), for: UIControlState())
        }
        else
        {
            self.btnFollow.setTitle("Follow".localized(), for: UIControlState())
        }
        
        //Stakeholder status
        if self.stakeholder.isEnable == true
        {
            self.btnEnable.setTitle("✔︎ " + " " + "Enabled".localized(), for: UIControlState())
        }
        else
        {
            self.btnEnable.setTitle("Enable".localized(), for: UIControlState())
        }
        
        //Rating
        self.lblPublicRating.text = (self.stakeholder.userRating ?? 0).getString()
        self.lblAdminRating.text = (self.stakeholder.adminRating ?? 0).getString()
    }
    
    fileprivate func presentNewPost()
    {
        //New Post
        let vcNewPost = NewPostViewController()
        vcNewPost.stakeholderTagged = stakeholder
        vcNewPost.onViewControllerClosed = {
            
            self.vcPagesController?.vcSHFeedPage?.refreshFeedTable()
            self.vcPagesController?.vcSHCalendarPage?.reloadCalendar()
        }
        
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        
        present(navController, animated: true, completion:nil)
    }
    fileprivate func requestUpdate()
    {
        let vcUpdateRequest: RequestUpdateViewController = Storyboard.getInstanceFromStoryboard("popups")
        vcUpdateRequest.stakeholderId = self.stakeholder.id
        self.present(vcUpdateRequest, animated: true, completion: nil)
        
    }
    
    @IBAction func changeFollowStatus(_ sender: Any)
    {
        if stakeholder.isFollow == FollowStatus.following
        {
            let vcConfirmUnfollow = UIAlertController(title: nil, message: stakeholder.fullName, preferredStyle: UIAlertControllerStyle.actionSheet)
            let actionUnfollow = UIAlertAction(title: "Unfollow".localized(), style: UIAlertActionStyle.destructive, handler: {
                (action) in
                
                //Unfollow
                LibraryAPI.shared.stakeholderBO.unfollowStakeholder(self.stakeholder,
                                                                    onSuccess: { () -> () in
                                                                        
                                                                        //Refresh Stakeholder List
                                                                        NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadStakeholdersList"), object: nil)
                                                                        
                                                                        MessageManager.shared.showBar(title: "Info".localized(),
                                                                                                      subtitle: "Now, you're not following to".localized() + " " + "\(self.stakeholder.fullName)",
                                                                            type: .info,
                                                                            fromBottom: false)
                                                                        
                                                                        self.stakeholder.isFollow = FollowStatus.notFollowing
                                                                        self.btnFollow.setTitle("Follow".localized(), for: UIControlState())
                                                                        
                }, onError: {error in
                    
                    MessageManager.shared.showBar(title: "Error", subtitle: "Error.TryAgain".localized(),
                                                  type: .error,
                                                  fromBottom: false)
                })
            })
            
            let actionCancel = UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler: nil)
            
            vcConfirmUnfollow.addAction(actionUnfollow)
            vcConfirmUnfollow.addAction(actionCancel)
            
            self.present(vcConfirmUnfollow, animated: true, completion: nil)
        }
        else
        {
            //Follow
            LibraryAPI.shared.stakeholderBO.followStakeholder(stakeholder,
                                                              onSuccess: { () -> () in
                                                                
                                                                //Refresh Stakeholder List
                                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadStakeholdersList"), object: nil)
                                                                
                                                                MessageManager.shared.showBar(title: "Info".localized(),
                                                                                              subtitle: "Now, you're  following to".localized() + " " + "\(self.stakeholder.fullName)",
                                                                    type: .info,
                                                                    fromBottom: false)
                                                                
                                                                self.stakeholder.isFollow = FollowStatus.following
                                                                self.btnFollow.setTitle("✔︎ " + "Following".localized(), for: UIControlState())
            }, onError: { error in
                
                MessageManager.shared.showBar(title: "Error", subtitle: "Error.TryAgain".localized(),
                                              type: .error,
                                              fromBottom: false)
            })
        }
    }
    
    func tapOnRating()
    {
        self.pushRatingControlFor(self.stakeholder)
    }
    
    fileprivate func pushRatingControlFor(_ stakeholder: Stakeholder)
    {
        let vcRating : RatingViewController = Storyboard.getInstanceFromStoryboard("Rating")
        vcRating.didEndRating = { [weak self] in
            
            self?.vcPagesController?.vcSHDetailPage?.loadStakeholderDetailLight()
        }
        vcRating.stakeholder = stakeholder
        self.present(vcRating, animated: true, completion: nil)
    }
    
    fileprivate func updateTabBarItems(page: Int)
    {
        self.searchBar.cancelSearch()
        self.pageControl.currentPage = page
        if page == 0
        {
            self.lblPageTitle.text = "DETAIL".localized()
            self.tabBar.setItems(tabBarItems_Detail, animated: false)
        }
        else if page == 1
        {
            self.lblPageTitle.text = "FEED".localized()
            self.tabBar.setItems(tabBarItems_feed, animated: false)
        }
        else
        {
            self.lblPageTitle.text = "CALENDAR".localized()
            self.tabBar.setItems(tabBarItems_Calendar, animated: false)
        }
    }
    
    fileprivate func updateEnableStatusForPostItem()
    {
        self.tabBar.items?.forEach({ (item) in
            if item.title == "Stakeholder Post".localized()
            {
                item.isEnabled = self.stakeholder.isEnable
            }
            
        })
    }
    //MARK: SEARCH BAR    
    func updateSearch(text: String, page: Int)
    {
        if page == 0
        {
            self.vcPagesController?.vcSHDetailPage?.vcProfile?.searchText(text)
            self.vcPagesController?.vcSHDetailPage?.vcProjects?.filterByText(text)
            self.vcPagesController?.vcSHDetailPage?.vcRelations?.filterByText(text)
            self.vcPagesController?.vcSHDetailPage?.vcGeographies?.filterByText(text)
        }
        else if page == 1
        {
            self.vcPagesController?.vcSHFeedPage?.vcPosts?.filterByText(text)
            self.vcPagesController?.vcSHFeedPage?.vcAttachments?.filterByText(text)
        }
        else if page == 2
        {
            self.vcPagesController?.vcSHCalendarPage?.vcCalendar?.filterByText(text)
            self.vcPagesController?.vcSHCalendarPage?.vcCalendarList?.filterByText(text)
        }
    }
    
    //MARK: WEB SERVICE
    @objc fileprivate func enableOrDisableStakeholder()
    {
        if self.stakeholder.isEnable == true
        {
            let vcConfirmUnfollow = UIAlertController(title: "Would you like to disable?".localized(), message: self.stakeholder.fullName, preferredStyle: .alert)
            let actionUnfollow = UIAlertAction(title: "Disable".localized(), style: UIAlertActionStyle.destructive, handler: {
                (action) in
                
                //Disable
                LibraryAPI.shared.stakeholderBO.disableStakeholder(self.stakeholder,
                                                                   onSuccess: { () -> () in
                                                                    
                                                                    //Refresh Stakeholder List
                                                                    NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadStakeholdersList"), object: nil)
                                                                    
                                                                    MessageManager.shared.showBar(title: "Info".localized(),
                                                                                                  subtitle: "\(self.stakeholder.fullName)" + " " + "has been disabled".localized(),
                                                                                                  type: .info,
                                                                                                  fromBottom: false)
                                                                    
                                                                    self.stakeholder.isEnable = false
                                                                    self.updateEnableStatusForPostItem()
                                                                    self.btnEnable.setTitle("Enable".localized(), for: UIControlState())
                                                                    //self.tabBarItemPostFeed.isEnabled = self.stakeholder.isEnable
                                                                    
                }, onError: {error in
                    
                    MessageManager.shared.showBar(title: "Error", subtitle: "Error.TryAgain".localized(),
                                                  type: .error,
                                                  fromBottom: false)
                })
            })
            
            let actionCancel = UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler: nil)
            
            vcConfirmUnfollow.addAction(actionUnfollow)
            vcConfirmUnfollow.addAction(actionCancel)
            
            self.present(vcConfirmUnfollow, animated: true, completion: nil)
            
        }
        else
        {
            //Enable
            LibraryAPI.shared.stakeholderBO.enableStakeholder(self.stakeholder,
                                                              onSuccess: { () -> () in
                                                                
                                                                //Refresh Stakeholder List
                                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadStakeholdersList"), object: nil)
                                                                
                                                                MessageManager.shared.showBar(title: "Info".localized(),
                                                                                              subtitle: "\(self.stakeholder.fullName)" + " " + "has been enabled".localized(),
                                                                                              type: .info,
                                                                                              fromBottom: false)
                                                                
                                                                self.stakeholder.isEnable = true
                                                                self.updateEnableStatusForPostItem()
                                                                self.btnEnable.setTitle("✔︎ " + " " + "Enabled".localized(), for: UIControlState())
                                                                //self.tabBarItemPostFeed.isEnabled = self.stakeholder.isEnable
                                                                
            }, onError: { error in
                
                MessageManager.shared.showBar(title: "Error", subtitle: "Error.TryAgain".localized(),
                                              type: .error,
                                              fromBottom: false)
            })
        }
    }
    
    //MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "Pages"
        {
            if let vcPages = segue.destination as? SHPagesViewController {
                
                self.vcPagesController = vcPages
                self.vcPagesController?.stakeholderId = self.stakeholder.id
                self.vcPagesController?.onDidChangePage = { [weak self] page in
                    
                    self?.updateTabBarItems(page:page)
                }
                self.vcPagesController?.onDidLoadStakeholderLight = { [weak self] stakeholder in
                    
                    self?.stakeholder = stakeholder
                }
            }
        }
    }
}

extension SHDetailViewController: UITabBarDelegate
{
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        if item.title == "List".localized()
        {
            self.vcPagesController?.vcSHCalendarPage?.flipCalendarView()
        }
        else if item.title == "Calendar".localized()
        {
            self.vcPagesController?.vcSHCalendarPage?.flipCalendarView()
        }
        else if item.title == "Search in Stakeholder".localized()
        {
            searchBar.showInNavigationItem(self.navigationItem, animated: true)
        }
        else if item.title == "Stakeholder Post".localized()
        {
           self.presentNewPost()
        }
        else if item.title == "Request Update".localized()
        {
            self.requestUpdate()
        }
        else if item.tag == 2
        {
            //Change Language
            self.vcPagesController?.vcSHDetailPage?.vcProfile?.isEnglish = self.vcPagesController?.vcSHDetailPage?.vcProfile?.isEnglish == true ? false:true
            self.vcPagesController?.vcSHDetailPage?.vcProfile?.stakeholder = self.stakeholder
            
            if self.vcPagesController?.vcSHDetailPage?.vcProfile?.isEnglish == true
            {
                MessageManager.shared.showStatusBar(title:"English".localized(),
                                                    type: .info,
                                                    containsIcon: false)
                
                for item in self.tabBar.items! where item.tag == 2
                {
                    item.title = "Language"
                    item.image = UIImage(named: "LanguageEsp")
                    item.selectedImage =  UIImage(named: "LanguageEsp")
                }
            }
            else
            {
                MessageManager.shared.showStatusBar(title:"Spanish".localized(),
                                                    type: .info,
                                                    containsIcon: false)
                
                for item in self.tabBar.items! where item.tag == 2
                {
                    item.title = "Lenguaje"
                    item.image = UIImage(named: "LanguageEng")
                    item.selectedImage =  UIImage(named: "LanguageEng")
                }
            }
        }
    }
}
