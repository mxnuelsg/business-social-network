//
//  MitigationSearchViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 10/3/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class MitigationSearchViewController: UIViewController
{
    //MARK: VARIABLES & OUTLETS
    @IBOutlet weak var searchBarMitigation: AutoSearchBar!
    
    fileprivate var vcMitigationTable: MitigationSearchTableViewController!
    fileprivate var searchPaging = Paging()
    
    var currentSite = 0
    var onMitigationSelected:((_ selection: MitigationMeasure) -> ())?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: LOAD CONFIG
    fileprivate func loadConfig()
    {
        self.title = "Mitigation Search".localized()
        self.searchBarMitigation.placeholder = "Search".localized()
        self.searchBarMitigation.tintColor = UIColor.blackAsfalto()
        self.searchBarMitigation.returnKeyType = .done
        
        self.searchPaging.pageNumber = 1
        self.searchPaging.pagination = true
        self.searchPaging.totalPages = 1
        
        //initialize AutoSearchBar
        searchBarMitigation.initialize(shouldSearchAfterDelay: true,
        onSearchText: { [weak self] (text) in
            if (self != nil) {
                self!.searchPaging.pageNumber = 1
                self!.vcMitigationTable.mitigationsMeasures.removeAll()
                self!.vcMitigationTable.tableView.reloadData()

                self!.vcMitigationTable.displayBackgroundMessage("Loading...".localized(),                                                                         subMessage: "")
                
                self!.getMitigations()
            }
            
        }, onBeginEditing: { [weak self] (searchBar) in
            searchBar.tintColor = DeviceType.IS_ANY_IPAD == true ? UIColor.black : UIColor.white
            self?.vcMitigationTable.tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            
        }, onClearText: { [weak self] in
            self?.vcMitigationTable.mitigationsMeasures.removeAll()
            self?.vcMitigationTable.tableView.reloadData()
            
        }, onCancel: { [weak self] in
            if (self != nil) {
                self!.searchBarMitigation.text = String()

                self!.searchPaging.pageNumber = 1
                self!.vcMitigationTable.mitigationsMeasures.removeAll()
                self!.vcMitigationTable.tableView.reloadData()

                self!.vcMitigationTable.dismissBackgroundMessage()
            }
        })
    }
    
    //MARK: WEB SERVICE
    func getMitigations()
    {
        //Request Object
        var wsObject: Dictionary <String, Any>  = [:]
        wsObject["SearchText"] = self.searchBarMitigation.text ?? String()
        wsObject["FilterId"] = 3
        wsObject["SiteMappingId"] = self.currentSite
        wsObject["Paging"] = self.searchPaging.getWSObject()
        
        LibraryAPI.shared.integrationBO.getMitigationMeasures(wsObject, onSuccess: { (list, totalRecords) in
            
            self.vcMitigationTable.dismissBackgroundMessage()
            
            //Total Records
            self.searchPaging.totalItems = totalRecords
            self.searchPaging.pageNumber += 1

            //Adding rows
            var newIndexPaths = [IndexPath]()
            let nextRow = self.vcMitigationTable.mitigationsMeasures.count
            
            for newRow in nextRow..<(nextRow + list.count)
            {
                newIndexPaths.append(IndexPath(row: newRow, section: 0))
            }
            
            self.vcMitigationTable.mitigationsMeasures += list
            self.vcMitigationTable.tableView.insertRows(at: newIndexPaths, with: .bottom)
            
            //indicator
            if self.vcMitigationTable.mitigationsMeasures.count == 0
            {
                self.vcMitigationTable.displayBackgroundMessage("No results".localized(),
                    subMessage: "")
            }
            else
            {
                self.vcMitigationTable.dismissBackgroundMessage()
            }

            
            }) { (error) in
                
                self.vcMitigationTable.tableView.displayBackgroundMessage("Error.TryAgain".localized(),
                                                                        subMessage: "")
        }


    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let id = segue.identifier, id == "MitigationSearchTableViewController" {
            
            self.vcMitigationTable = segue.destination as! MitigationSearchTableViewController
            self.vcMitigationTable.willDisplayLastRow = { [weak self] row in
                
                if let isPagination = self?.searchPaging.pagination, let page = self?.searchPaging.pageNumber, let totalPages = self?.searchPaging.totalPages, isPagination  == true && page <= totalPages {
                    
                    self?.getMitigations()
                }
            }
            
            
            self.vcMitigationTable.onMitigationSelected = { [weak self] selection in
            
                self?.searchBarMitigation.resignFirstResponder()
                self?.onMitigationSelected?(selection)
                _ = self?.navigationController?.popViewController(animated: true)
            }
        }
    }
}

