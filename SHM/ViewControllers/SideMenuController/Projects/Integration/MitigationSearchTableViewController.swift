//
//  MitigationSearchTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 10/3/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class MitigationSearchTableViewController: UITableViewController
{
    //MARK: VARIABLES & OUTLETS
    var mitigationsMeasures = [MitigationMeasure]()
    var onMitigationSelected:((_ selection: MitigationMeasure) -> ())?
    var willDisplayLastRow: ((_ row: Int) -> ())?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - LOAD CONFIG
    fileprivate func loadConfig()
    {
        self.tableView.hideEmtpyCells()
//        self.tableView.backgroundColor = UIColor.backgroundLight()
    }
    
    //MARK: - DATASOURCE
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.mitigationsMeasures.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style: .default, reuseIdentifier:"Cell")
        
        //Selection color
        let selectedColor = UIView()
        selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
        cell.selectedBackgroundView = selectedColor
        
        //Config
        cell.textLabel?.numberOfLines = 2
        cell.textLabel?.textColor = UIColor.blackAsfalto()
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
        
        //Info
        cell.textLabel?.text = self.mitigationsMeasures[indexPath.row].title
        
        return cell
    }
    
    // MARK: - DELEGATE
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let mitigationSelected = self.mitigationsMeasures[indexPath.row]
        self.onMitigationSelected?(mitigationSelected)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if indexPath.row == self.mitigationsMeasures.count - 1
        {
            self.willDisplayLastRow?(indexPath.row)
        }
    }
}
