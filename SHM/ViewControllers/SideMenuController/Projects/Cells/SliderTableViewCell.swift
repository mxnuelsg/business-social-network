//
//  SliderTableViewCell.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 26/08/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class SliderTableViewCell: UITableViewCell {

    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var lblSliderValue: UILabel!
    
    var updateProgress: ((_ progress: Float) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func progressChanged(_ sender: AnyObject) {
        lblSliderValue.text = "\(Int(slider.value))%"
        updateProgress?(slider.value)
    }
}
