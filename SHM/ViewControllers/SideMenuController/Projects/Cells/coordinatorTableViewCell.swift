//
//  coordinatorTableViewCell.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 1/19/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class coordinatorTableViewCell: UITableViewCell
{
    //MARK: PROPERTIES
    @IBOutlet weak var imgMail: UIImageView!
    @IBOutlet weak var imgInbox: UIImageView!
    @IBOutlet weak var imgCall: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var constraintInboxTrailing: NSLayoutConstraint!
    @IBOutlet weak var constraintEmailTrailing: NSLayoutConstraint!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: FUNCTIONS
    func hideInbox()
    {
        self.constraintEmailTrailing.constant = self.constraintInboxTrailing.constant
    }
}
