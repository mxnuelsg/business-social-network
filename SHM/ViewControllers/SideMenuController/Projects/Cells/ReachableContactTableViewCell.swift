//
//  ReachableContactTableViewCell.swift
//  Pods
//
//  Created by Manuel Salinas Gonzalez on 11/2/15.
//
//

import UIKit

class ReachableContactTableViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgInbox: UIImageView!
    @IBOutlet weak var imgCall: UIImageView!
    @IBOutlet weak var imgMail: UIImageView!
    @IBOutlet weak var constraintNameTrailing: NSLayoutConstraint!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupCellForMember(_ member: Member)
    {
    
    }
}
