//
//  coordinatorContactCell.swift
//  SHM
//
//  Created by Nestor Javier Hernandez Bautista on 1/19/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class coordinatorContactCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgMail: UIImageView!
    
    @IBOutlet weak var imgCall: UIImageView!
    @IBOutlet weak var imgInbox: UIImageView!
    
  
    //MARK: LIFE CYCLE
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
