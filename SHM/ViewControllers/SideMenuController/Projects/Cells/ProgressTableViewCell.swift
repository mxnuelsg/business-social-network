//
//  ProgressTableViewCell.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 20/08/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ProgressTableViewCell: UITableViewCell
{

    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var lblProgress: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

}
