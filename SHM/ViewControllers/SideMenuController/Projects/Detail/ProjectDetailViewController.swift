//
//  TopicDetailViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 10/08/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ProjectDetailViewController: UIViewController, UIScrollViewDelegate, UITabBarDelegate {

    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var vTitleContainer: UIView!
    @IBOutlet weak var scvMain: UIScrollView!
    @IBOutlet weak var viewContainerPages: UIView!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDateBy: UILabel!
    @IBOutlet weak var btnFiles: UIButton!
    @IBOutlet weak var lblPageTitle: UILabel!
    @IBOutlet weak var pcDetail: UIPageControl!
    @IBOutlet weak var constraintContentHeight: NSLayoutConstraint!
    @IBOutlet weak var btnDisable: UIButton!

    var onlyEditMembers = false
    var isModal = false
    var vcProjectPages: ProjectPagesViewController!
    var onProjectWasEdited : (() -> ())?
    var currentPage = 0
    var project  = Project(isDefault: true)
    var pageControlBeingUsed = false
    var containerViewHeight: CGFloat = 0
    fileprivate lazy var searchBar: AutoSearchBar = AutoSearchBar()
    
    //Tab bar
    @IBOutlet weak var tabBarPostProject: UITabBarItem!
    @IBOutlet weak var tabBarEdit: UITabBarItem!
    @IBOutlet weak var tabBarSearch: UITabBarItem!
    @IBOutlet weak var lblProjectSiteTitle: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //Localizations
        self.localize()
                
        if self.isModal == true
        {
            //Close Button
            let barBtnCancel = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.stop, target: self, action:#selector(self.close))
            navigationItem.leftBarButtonItem = barBtnCancel
        }
        
        if self.validateEditionPermissions() == false
        {
            self.tabBarEdit.isEnabled = false
            self.tabBarEdit.image = UIImage()
        }
        
        self.setupNavigation(animated: true)
        
        self.scvMain.enableRefreshAction({
            self.loadProjectDetail()
        })
    
        self.btnFollow.layer.cornerRadius = 2
        self.btnFollow.clipsToBounds = true
        
        self.btnFollow.setTitle(String(), for: UIControlState())
        self.btnFiles.isUserInteractionEnabled = false
        
        self.pcDetail.currentPageIndicatorTintColor = UIColor.colorForNavigationController()
        
        self.searchBar.initialize(onSearchText: { [weak self] (text) in
            if (self != nil) {
                self!.updateSearch(text: text, page: self!.vcProjectPages.currentPage)
            }
            
            }, onCancel: { [weak self] in
                if (self != nil) {
                    self!.searchBar.text = ""
                    self!.updateSearch(text: "", page: self!.vcProjectPages.currentPage)
                    self!.setupNavigation(animated: true)
                }
        })
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.loadProjectDetail), name: NSNotification.Name(rawValue: "loadProjectDetail"), object: nil)
        
        loadProjectDetail()
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "loadProjectDetail"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
        
        //Reset Badge icon
        self.removeBadgeIcon()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews()
    {
        self.containerViewHeight = self.tabBar.frame.origin.y - self.viewContainerPages.frame.origin.y
    }
    
    // MARK: Private Methods
    fileprivate func localize()
    {
        self.tabBarEdit.title = "Edit".localized()
        self.tabBarPostProject.title = "Project Post".localized()
        self.tabBarSearch.title = "Search".localized()
        self.btnFiles.setTitle(" 0 File".localized(), for: UIControlState())
        self.lblDateBy.text = "Updated by:".localized()
        self.lblPageTitle.text = "DETAIL".localized()
    }
    
    func setupNavigation(animated: Bool) {
        self.title = "PROJECT".localized()
        
        navigationItem.setHidesBackButton(false, animated: animated)
        navigationItem.fadeOutTitleView()
    }
    
    //MARK: Site Mapping Information
    func setSiteMappingInfo(_ site:Site?)
    {
        if let siteTitle = site?.title {
            
            self.lblProjectSiteTitle.isHidden = false
            self.lblProjectSiteTitle.text = "⚑  \(siteTitle)"
        }
    }
    
    //MARK: VALIDATE PERMISSIONS FOR EDITION
    func validateEditionPermissions() -> Bool
    {
        if (LibraryAPI.shared.currentUser?.role == .vipUser)
            || (LibraryAPI.shared.currentUser?.role == .globalManager)
            || self.project.coordinator.id == LibraryAPI.shared.currentUser?.id
        {
            return true
        }

        
        return false
    }
    
    //MARK:WEB SERVICE
    func loadProjectDetail()
    {
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        let detail = "true"
        
        LibraryAPI.shared.projectBO.getProjectDetailById(project.id, isDetail: detail, onSuccess: { project in
            
            //Indicator
            MessageManager.shared.hideHUD()
            self.scvMain.refreshed()
            
            //Load Data
            self.project = project
            self.setSiteMappingInfo(project.site)
            self.loadConfig()
           
            //Disable Edit Project
            for (index, item) in self.tabBar.items!.enumerated() where item.tag == 1
            {
                if project.coordinator.id != LibraryAPI.shared.currentUser!.id
                    && LibraryAPI.shared.currentUser!.role != UserRole.globalManager
                {
                    self.tabBar.items?.remove(at: index)
                }
            }
            
            }) { error in
                
                MessageManager.shared.hideHUD()
                
                _ = self.navigationController?.popViewController(animated: true)
                
                MessageManager.shared.showBar(title: "Error",
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
        }
    }

    @IBAction func changeStatusFollow(_ sender: AnyObject)
    {
        if project.followingStatus == false
        {
            self.btnFollow.isEnabled = false
            LibraryAPI.shared.projectBO.followProject(project.id, onSuccess: {
                (success) -> () in
                
                //Refresh Project List
                NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadProjectList"), object: nil)
                
                MessageManager.shared.showBar(title: "Info".localized(),
                    subtitle: "Now, you're following this project".localized(),
                    type: .info,
                    fromBottom: false)
                
                self.btnFollow.isEnabled = true
                self.btnFollow.setTitle("✔︎ Following".localized(), for: UIControlState())
                self.project.followingStatus = true
                
                }) {error in
                    self.btnFollow.isEnabled = true
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            }
        }
        else
        {
            let vcConfirmUnfollow = UIAlertController(title: "Would you like to stop following this project?".localized(), message: project.title, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let actionUnfollow = UIAlertAction(title: "Unfollow".localized(), style: UIAlertActionStyle.destructive, handler: {
                (action) in
                
                self.btnFollow.isEnabled = false
                LibraryAPI.shared.projectBO.unfollowProject(self.project.id, onSuccess: {
                    (success) -> () in
                    
                    //Refresh Project List
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadProjectList"), object: nil)
                    
                    MessageManager.shared.showBar(title: "Info".localized(),
                        subtitle: "Now, you're not following this project".localized(),
                        type: .info,
                        fromBottom: false)
                    
                    self.btnFollow.isEnabled = true
                    self.btnFollow.setTitle("Follow".localized(), for: UIControlState())
                    self.project.followingStatus = false
                    
                    }) {error in

                        MessageManager.shared.showBar(title: "Error",
                            subtitle: "Error.TryAgain".localized(),
                            type: .error,
                            fromBottom: false)
                        
                        self.btnFollow.isEnabled = true
                }
            })
            
            let actionCancel = UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler: nil)
            
            vcConfirmUnfollow.addAction(actionUnfollow)
            vcConfirmUnfollow.addAction(actionCancel)
            
            self.present(vcConfirmUnfollow, animated: true, completion: nil)
        }
    }
    
    func loadConfig()
    {
        self.vTitleContainer.backgroundColor = UIColor(red: 0.21, green: 0.24, blue: 0.26, alpha: 1.0)
        self.lblTitle.text = self.project.title
        self.tabBarPostProject.isEnabled =  self.project.isEnable
        
        let strBy = "by".localized()
        self.lblDateBy.text = ((self.project.modifiedDate?.showStringFormat("MM/dd/yyyy")) ?? "-") + " \(strBy) " + (self.project.author?.fullName ?? "-")
        
        //Following status
        if self.project.followingStatus == true
        {
            self.btnFollow.setTitle("︎︎︎︎︎✔︎ " + "Following".localized(), for: UIControlState())
        }
        else
        {
            self.btnFollow.setTitle("Follow".localized(), for: UIControlState())
        }
        
        self.vcProjectPages.vcDetail.project = self.project
        self.vcProjectPages.vcDetail.loadData(project: self.project)
        self.vcProjectPages.vcFeed.loadFeedForProject(self.project)
        self.vcProjectPages.vcCalendar.configureForEmbed()
        
        self.btnFiles.setTitle(" \(self.project.totalAttachments) " + (self.project.totalAttachments != 1 ? "Files".localized() : "File".localized()), for: .normal)
        
        
        //Btn Disable
        if LibraryAPI.shared.currentUser?.role == .globalManager || self.project.coordinator.id == LibraryAPI.shared.currentUser?.id
        {
            //Project status
            if self.project.isEnable == true
            {
                self.btnDisable.setTitle("✔︎ " + " " + "Enabled".localized(), for: .normal)
            }
            else
            {
                self.btnDisable.setTitle("Enable".localized(), for: .normal)
            }
            
            self.btnDisable.layer.cornerRadius = 2
            self.btnDisable.clipsToBounds = true
            self.btnDisable.addTarget(self, action: #selector(self.enableOrDisableProject), for: .touchUpInside)
            self.btnDisable.isHidden = false
        }

    }
    
    func updateSearch(text: String, page: Int)
    {
        if page == 0
        {
            self.vcProjectPages.vcDetail.searchText(text)
        }
        else if page == 1
        {
            self.vcProjectPages.vcFeed.vcPostsTable.filterByText(text)
            self.vcProjectPages.vcFeed.vcAttachmentsTable?.filterByText(text)
        }
        else if page == 2
        {
            self.vcProjectPages.vcCalendar.configureForEmbed()
            self.vcProjectPages.vcCalendarList.configureForEmbed()
            
            self.vcProjectPages.vcCalendar.filterByText(text)
//            self.vcProjectPages.vcCalendarList.filterByText(text)
        }
    }
    
    //MARK: ACTIONS
    func close()
    {
        dismiss(animated: true) { () -> Void in
        }
    }
    
    func enableOrDisableProject()
    {
        if self.project.isEnable == true
        {
            let vcConfirmUnfollow = UIAlertController(title: "Would you like to disable?".localized(), message: self.project.title, preferredStyle: .alert)
            let actionUnfollow = UIAlertAction(title: "Disable".localized(), style: .destructive, handler: {
                (action) in
                
                //Disable
                self.projectEnabled(false)

            })
            
            let actionCancel = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil)
            
            vcConfirmUnfollow.addAction(actionUnfollow)
            vcConfirmUnfollow.addAction(actionCancel)
            
            self.present(vcConfirmUnfollow, animated: true, completion: nil)
        }
        else
        {
            //Enable
            self.projectEnabled(true)
        }
    }
    
    private func projectEnabled(_ enable: Bool)
    {
        LibraryAPI.shared.projectBO.enable(self.project, enable: enable, onSuccess: { isEnable in
            
            //Refresh project List
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadProjectList"), object: nil)
            
            let status = isEnable == true ? "has been enabled".localized() : "has been disabled".localized()
            MessageManager.shared.showBar(title: "Info".localized(),
                                          subtitle: "This Project" + " " + status,
                                          type: .info,
                                          fromBottom: false)

            
            self.project.isEnable = isEnable
            
            let title = isEnable == true ? "✔︎ " + " " + "Enabled".localized() : "Enable".localized()
            self.btnDisable.setTitle(title, for: .normal)
            self.tabBarPostProject.isEnabled =  self.project.isEnable
            
        }, onError: { error in
            
            MessageManager.shared.showBar(title: "Error",
                                          subtitle: "Error.TryAgain".localized(),
                                          type: .error,
                                          fromBottom: false)
        })
    }

    func showProjectEditionController()
    {
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        LibraryAPI.shared.projectBO.getCurrentUserSiteMappings({ (sites) in
            
            MessageManager.shared.hideHUD()
            var rootVC : UIViewController
            let vcEditProject = Storyboard.getInstanceOf(ProjectNewViewController.self)
            vcEditProject.project = self.project
            vcEditProject.sites = sites
            vcEditProject.currentSite = self.project.site
            vcEditProject.onProjectEdited = { [weak self] Void in
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                    
                    self?.loadProjectDetail()
                }
                
                self?.onProjectWasEdited?()
            }
            
            rootVC = vcEditProject
            self.navigationController?.pushViewController(rootVC, animated: true)
//            let vcEditProject = Storyboard.getInstanceOf(ProjectNewViewController.self)
//            vcEditProject.sites = sites
//            vcEditProject.currentSite = currentProject.site
//            vcEditProject.project = currentProject
//            vcEditProject.onProjectEdited = {
//                
//                self.loadProjectDetail()
//                self.navigationController?.presentedViewController?.dismissViewControllerAnimated(true, completion: nil)
//            }
//            
//            let navController = NavyController(rootViewController:vcEditProject)
//            navController.modalPresentationStyle = .FormSheet
//            
//            let btnCancel = UIBarButtonItem(barButtonSystemItem: .Stop, target: self, action: #selector(self.cancelEditing))
//            vcEditProject.navigationItem.leftBarButtonItem = btnCancel
//            
//            self.navigationController?.presentViewController(navController, animated: true, completion: nil)
            })
        { (error) in
            
            MessageManager.shared.hideHUD()
        }
    }
    
     // MARK: - TAB BAR
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        //Selected item color equal not selected color
        tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        
        switch item.tag
        {
        case 0:
            let vcNewPost = NewPostViewController()
            vcNewPost.projectTagged = self.project
            vcNewPost.onViewControllerClosed = { [weak self] Void in
                
                self?.vcProjectPages.vcFeed.loadFeedForProject(self?.project)
                
                if vcNewPost.arrayDateAdded.count > 0
                {
                    self?.vcProjectPages.vcCalendar.loadFullCalendarById()
                }
            }
            
            let navController = NavyController(rootViewController: vcNewPost)
            navController.modalPresentationStyle = (DeviceType.IS_ANY_IPAD) ?  UIModalPresentationStyle.formSheet :  UIModalPresentationStyle.overFullScreen
            
            present(navController, animated: true, completion: nil)
        case 1:
            
            
            if onlyEditMembers
            {
                var rootVC : UIViewController
                let vcMembersEdit = Storyboard.getInstanceOf(MembersEditViewController.self)
                vcMembersEdit.members = project.members
                rootVC = vcMembersEdit
                navigationController?.pushViewController(rootVC, animated: true)
            }
            else
            {
                self.showProjectEditionController()
            }

        case 2:
            searchBar.showInNavigationItem(navigationItem, animated: true)
        case 3:
            if self.vcProjectPages.containerViewCalendar.isHidden == false
            {
                self.vcProjectPages.containerViewCalendar.isHidden = true
                self.vcProjectPages.containerViewCalendarList.isHidden = false
                self.vcProjectPages.vcCalendarList.focus()
                self.vcProjectPages.vcCalendar.unfocus()
                
                for item in self.tabBar.items! where item.tag == 3
                {
                    item.title = "Calendar View".localized()
                    item.image = UIImage(named: "Calendar")
                    item.selectedImage =  UIImage(named: "Calendar")
                }
                
                //Disable Search
                for item in self.tabBar.items! where item.tag == 2
                {
                    item.isEnabled = false
                }
            }
            else
            {
                self.vcProjectPages.containerViewCalendar.isHidden = false
                self.vcProjectPages.containerViewCalendarList.isHidden = true
                self.vcProjectPages.vcCalendarList.unfocus()
                self.vcProjectPages.vcCalendar.focus()
                
                for item in self.tabBar.items! where item.tag == 3
                {
                    item.title = "Calendar List".localized()
                    item.image = UIImage(named: "DayView")
                    item.selectedImage =  UIImage(named: "DayView")
                }
                
                //Enable Search
                for item in self.tabBar.items! where item.tag == 2
                {
                    item.isEnabled = true
                }
            }
            
        default:
            print("A search bar must appear, yet to be defined")
            break
        }
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segueID_ProjectPagesView" {
            vcProjectPages = segue.destination as! ProjectPagesViewController
            vcProjectPages.project = project
            vcProjectPages.onHeightChanged = { [weak self] (newHeight) in
                self?.constraintContentHeight.constant = self!.viewContainerPages.frame.origin.y + newHeight
                self?.viewContainerPages.layoutIfNeeded()
                return 0
            }
            
            vcProjectPages.onPageChange = {
                (page) in
                
                self.searchBar.cancelSearch()
                self.pcDetail.currentPage = page
               
                switch page
                {
                case 0:
                    self.vcProjectPages.vcFeed.unfocus()
                    self.vcProjectPages.vcCalendar.unfocus()
                    self.vcProjectPages.vcCalendarList.unfocus()
                    
                    self.lblPageTitle.text = "DETAIL".localized()
                    
                    //Remove calendar item
                    for (index, item) in self.tabBar.items!.enumerated() where item.tag == 3
                    {
                        self.tabBar.items?.remove(at: index)
                    }
                    
                    //Enable Search
                    for item in self.tabBar.items! where item.tag == 2
                    {
                        item.isEnabled = true
                    }
                    
                case 1:
                    self.vcProjectPages.vcCalendar.unfocus()
                    self.vcProjectPages.vcCalendarList.unfocus()
                    
                    self.lblPageTitle.text = "FEED".localized().uppercased()
                    self.vcProjectPages.vcFeed.focus()
                    
                    //Remove calendar item
                    for (index, item) in self.tabBar.items!.enumerated() where item.tag == 3
                    {
                        self.tabBar.items?.remove(at: index)
                    }
                    
                    //Enable Search
                    for item in self.tabBar.items! where item.tag == 2
                    {
                        item.isEnabled = true
                    }
                case 2:
                    self.vcProjectPages.vcFeed.unfocus()
                    if self.vcProjectPages.containerViewCalendar.isHidden == false {
                        self.vcProjectPages.vcCalendar.focus()
                    }
                    else {
                        self.vcProjectPages.vcCalendarList.focus()
                    }
                    self.lblPageTitle.text = "CALENDAR".localized()
                    
                    //Add calendar item
                    let itemCalendarList = UITabBarItem(title: "Calendar List".localized(), image: UIImage(named: "DayView"), selectedImage: UIImage(named: "DayView"))
                     itemCalendarList.tag = 3

                    self.tabBar.items?.insert(itemCalendarList, at: self.tabBar.items!.count - 1)
                    
                    //Enable Search
                    for item in self.tabBar.items! where item.tag == 2
                    {
                        if self.vcProjectPages.vcCalendar.hasFocus == true
                        {
                            item.isEnabled = true
                        }
                        else
                        {
                            item.isEnabled = false
                        }
                    }
                default:
                    return
                }
            }
            
            vcProjectPages.pageDidChange = { [weak self] Void in
                
                if self?.pcDetail.currentPage == 2
                {
                    self?.delay(0.1, closure: { () -> () in
                        
                        if self?.vcProjectPages.vcCalendar.calendar.selectedDate == nil
                        {
                            self?.vcProjectPages.vcCalendar.calendar.select(Date())
                        }
                        else
                        {
                            self?.vcProjectPages.vcCalendar.calendar.select(self?.vcProjectPages.vcCalendar.calendar.selectedDate)
                        }
                    })
                }
            }
        }
    }

    func delay(_ delay:Double, closure:@escaping () -> ()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
}
