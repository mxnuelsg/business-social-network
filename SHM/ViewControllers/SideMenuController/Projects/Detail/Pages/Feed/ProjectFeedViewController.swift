//
//  TopicFeedViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 12/08/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ProjectFeedViewController: UIViewController {
    @IBOutlet weak var segmentedFeedAttachment: UISegmentedControl!
    
    @IBOutlet weak var viewContainer: UIView!
    
    @IBOutlet weak var viewContainerTableFeed: UIView!
    @IBOutlet weak var viewContainerTableAttachments: UIView!
    
    var vcPostsTable: PostsTableViewController!
    var vcAttachmentsTable: FileListViewController!
    
    var onHeightChanged: ((_ newHeight: CGFloat) -> (CGFloat))!
    
    var totalFeedHeight: CGFloat = 1000
    var totalAttachmentsHeight: CGFloat = 1000
    
    fileprivate(set) internal var project: Project!
    
    @IBOutlet weak var constraintContainerHeight: NSLayoutConstraint!
    
    fileprivate(set) internal var hasFocus = false
    
    // MARK: LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localize()
        segmentedFeedAttachment.tintColor = UIColor.blueBelize()
        
        turnOnRotation({
            self.reloadFeedTable()
        })
        
        displayFeed()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        turnOffRotation()
    }
    
    fileprivate func localize()
    {
        if DeviceType.IS_ANY_IPAD == false
        {
            self.segmentedFeedAttachment.setTitle("FEED".localized(), forSegmentAt: 0)
            
            self.segmentedFeedAttachment.setTitle("ATTACHMENTS".localized(), forSegmentAt: 1)
        }
    }
    // MARK: UI User Interaction
    
    @IBAction func segmentedControlChanged(_ sender: AnyObject) {
        if let segmented = sender as? UISegmentedControl
        {
            switch(segmented.selectedSegmentIndex)
            {
            case 0:
                displayFeed()
            case 1:
                displayAttachments()
            default:
                return
            }
        }
    }
    
    // MARK: - PRIVATE METHODS
    
    func displayFeed()
    {
        viewContainerTableFeed.isHidden = false
        viewContainerTableAttachments.isHidden = true
        
        setFeedTableHeight()
    }
    
    func displayAttachments()
    {
        viewContainerTableFeed.isHidden = true
        viewContainerTableAttachments.isHidden = false
        
        setAttachmentsTableHeight()
    }
    
    func focus()
    {
        hasFocus = true
        
        guard onHeightChanged != nil else { return }
        
        if viewContainerTableFeed.isHidden == false {
            setFeedTableHeight()
        }
        else if viewContainerTableAttachments.isHidden == false {
            setAttachmentsTableHeight()
        }
    }
    
    func unfocus() {
        hasFocus = false
    }
    
    func loadFeedForProject(_ project: Project!)
    {
        //Indicators
        self.vcPostsTable.displayBackgroundMessage("Loading...".localized(), subMessage: "")
        
        if let vcAttachment = self.vcAttachmentsTable {
            
            vcAttachment.tableFiles.displayBackgroundMessage("Loading...".localized(), subMessage: "")
        }
        
        self.project = project
        
        LibraryAPI.shared.feedBO.getProjectFeed(project, onSuccess: { posts in
            
            self.vcPostsTable.posts = posts
            self.reloadFeedTable()
            
            //Empty State
            self.vcPostsTable.posts.count == 0 ? self.vcPostsTable.displayBackgroundMessage("No posts".localized(), subMessage: "") : self.vcPostsTable.dismissBackgroundMessage()
            
        }) { error in
            
            self.vcPostsTable.dismissBackgroundMessage()
            
            //Empty State
            if self.vcPostsTable.posts.count == 0
            {
                self.vcPostsTable.displayBackgroundMessage("Error".localized(), subMessage: "An Error has ocurred trying to load the Feed".localized())
            }
        }
        
        LibraryAPI.shared.attachmentBO.getFilesByProjectId(project.id, onSuccess: { files in
            
            self.vcAttachmentsTable = FileListViewController(attachments: files)
            self.viewContainerTableAttachments.addSubViewController(self.vcAttachmentsTable, parentVC: self)
            self.reloadAttachmentsTable()
            
            //Empty State
            self.vcAttachmentsTable.attachments.count == 0 ? self.vcAttachmentsTable.tableFiles.displayBackgroundMessage("No Attachments Found".localized(), subMessage: "") : self.vcAttachmentsTable.tableFiles.dismissBackgroundMessage()
            
            
        }) { error in
            
            self.vcAttachmentsTable.tableFiles.dismissBackgroundMessage()
            
            //Empty State
            if self.vcAttachmentsTable.attachments.count == 0
            {
                self.vcAttachmentsTable.tableFiles.displayBackgroundMessage("Error".localized(), subMessage: "An error has ocurred loading the attachments list".localized())
            }
        }
    }
    
    func reloadFeedTable()
    {
        vcPostsTable.tableView.reloadData()
        
        guard vcPostsTable.tableView.numberOfRows(inSection: 0) > 0 else {
            self.totalFeedHeight = self.viewContainer.frame.origin.y
            return
        }
        
        let lastIndexPath = IndexPath(row: self.vcPostsTable.tableView.numberOfRows(inSection: 0) - 1, section: 0)
        
        self.vcPostsTable.tableView.scrollToRow(at: lastIndexPath, at: UITableViewScrollPosition.bottom, animated: false)
        
        DispatchQueue.main.async(execute: {
            let tableHeight = self.vcPostsTable.tableView.contentSize.height
            
            self.totalFeedHeight = tableHeight + self.viewContainer.frame.origin.y
            
            if self.hasFocus == true {
                self.setFeedTableHeight()
            }
        });
    }
    
    func setFeedTableHeight() {
        let recommendedHeight = onHeightChanged(totalFeedHeight)
        
        if recommendedHeight > totalFeedHeight {
            totalFeedHeight = recommendedHeight
        }
        
        constraintContainerHeight.constant = totalFeedHeight
        viewContainer.layoutIfNeeded()
    }
    
    func reloadAttachmentsTable(){
        self.vcAttachmentsTable.tableFiles.reloadData()
        self.vcAttachmentsTable.tableFiles.isScrollEnabled = false
        self.vcAttachmentsTable.tableFiles.bounces = false
        
        guard vcAttachmentsTable.tableFiles.numberOfRows(inSection: 0) > 0 else {
            self.totalAttachmentsHeight = self.viewContainer.frame.origin.y
            return
        }
        
        let lastIndexPath = IndexPath(row: self.vcAttachmentsTable.tableFiles.numberOfRows(inSection: 0) - 1, section: 0)
        
        self.vcAttachmentsTable.tableFiles.scrollToRow(at: lastIndexPath, at: UITableViewScrollPosition.bottom, animated: false)
        
        DispatchQueue.main.async(execute: {
            let tableHeight = self.vcAttachmentsTable.tableFiles.contentSize.height
            
            self.totalAttachmentsHeight = tableHeight + self.viewContainer.frame.origin.y
            
            if self.hasFocus == true {
                self.setAttachmentsTableHeight()
            }
        });
    }
    
    func setAttachmentsTableHeight() {
        let recommendedHeight = onHeightChanged(totalAttachmentsHeight)
        
        if recommendedHeight > totalAttachmentsHeight {
            totalAttachmentsHeight = recommendedHeight
        }
        
        constraintContainerHeight.constant = totalAttachmentsHeight
        viewContainer.layoutIfNeeded()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier! == "segueID_PostsTableViewController" {
            vcPostsTable = segue.destination as! PostsTableViewController
            vcPostsTable.tableView.isScrollEnabled = false
            vcPostsTable.tableView.bounces = false
        }
    }
}
