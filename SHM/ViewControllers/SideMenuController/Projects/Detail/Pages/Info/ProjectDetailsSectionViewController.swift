//
//  ProjectDetailsSectionViewController
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 7/27/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class ProjectDetailsSectionViewController: UIViewController {
    
    @IBOutlet weak var summaryContainer: UIView!
    @IBOutlet weak var stakeholdersContainer: UIView!
    @IBOutlet weak var geographiesContainer: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    fileprivate var summaryVC: ProjectDetailTableViewController?
    fileprivate var stakeholdersVC: ProjectDetailsStakeholdersTableViewController?
    fileprivate var geographiesVC: GeoTableViewController?
    var onHeightSet: ((CGFloat) -> Void)?
    var project: Project!
    var searchText: String?
    var sectionHeight: CGFloat = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        localize()
    }
    
    @IBAction func segmentedControlValueChanged(sc: UISegmentedControl) {
        let index = sc.selectedSegmentIndex
        
        summaryContainer.isHidden = !(index == 0)
        stakeholdersContainer.isHidden = !(index == 1)
        geographiesContainer.isHidden = !(index == 2)
        
        if index == 0 {
            summaryVC?.setActive()
        } else if index == 1{
            stakeholdersVC?.setActive()
        } else if index == 2{
            geographiesVC?.setActive()
        }
    }
    
    fileprivate func localize() {
        segmentedControl.setTitle("SUMMARY".localized(), forSegmentAt: 0)
        segmentedControl.setTitle("STAKEHOLDERS".localized(), forSegmentAt: 1)
        segmentedControl.setTitle("GEOGRAPHIES".localized(), forSegmentAt: 2)
    }
    
    func initialize(onHeightSet: @escaping (CGFloat) -> Void) {
        self.onHeightSet = onHeightSet
    }
    
    func getSectionHeight() -> CGFloat {
        return sectionHeight
    }
    
    func loadData(project: Project) {
        self.project = project
        
        let onHeight = { [weak self] (height: CGFloat) in
            self?.sectionHeight = height + self!.summaryContainer.frame.minY
            self?.onHeightSet?(self!.sectionHeight)
        }
        
        summaryVC?.initialize(project: project, onHeightSet: onHeight)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            self.summaryVC?.setActive()
            self.summaryContainer.isHidden = false
        }
        stakeholdersVC?.initialize(stakeholders: project.stakeholders, onHeightSet: onHeight)
        geographiesVC?.initialize(geographies: project.geographies, onHeightSet: onHeight)
    }
    
    func loadSummary(_ project: Project)
    {
        self.project = project
        let onHeight = { [weak self] (height: CGFloat) in
            self?.sectionHeight = height + self!.summaryContainer.frame.minY
            self?.onHeightSet?(self!.sectionHeight)
        }
        
        summaryVC?.initialize(project: project, onHeightSet: onHeight)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            self.summaryVC?.setActive()
            self.summaryContainer.isHidden = false
        }
    }
    
    func loadStakeholders(_ project: Project)
    {
        self.project = project
        let onHeight = { [weak self] (height: CGFloat) in
            self?.sectionHeight = height + self!.summaryContainer.frame.minY
            self?.onHeightSet?(self!.sectionHeight)
        }
        stakeholdersVC?.initialize(stakeholders: project.stakeholders, onHeightSet: onHeight)
    }
    
    func loadGeographies(_ project: Project)
    {
        self.project = project
        let onHeight = { [weak self] (height: CGFloat) in
            self?.sectionHeight = height + self!.summaryContainer.frame.minY
            self?.onHeightSet?(self!.sectionHeight)
        }
        geographiesVC?.initialize(geographies: project.geographies, onHeightSet: onHeight)
    }
    
    func searchText(_ text: String) {
        summaryVC?.searchText(text)
        stakeholdersVC?.searchText(text)
        geographiesVC?.searchText(text)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let id = segue.identifier else { return }
        
        if id == "projectSummarySegue" {
            summaryVC = segue.destination as? ProjectDetailTableViewController
            
        } else if id == "projectStakeholdersSegue" {
            stakeholdersVC = segue.destination as? ProjectDetailsStakeholdersTableViewController
            
        } else if id == "projectGeographiesSegue" {
            geographiesVC = segue.destination as? GeoTableViewController
        }
    }
}
