//
//  ProjectDetailsStakeholdersTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 7/27/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit
import Spruce

class ProjectDetailsStakeholdersTableViewController: BaseProjectDetailsTableViewController
{

    var stakeholders: [Stakeholder]?
    var stakeholdersBackup = [Stakeholder]()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //tableView.bounces = false
        tableView.register(UINib(nibName: "ContactListCell", bundle: nil), forCellReuseIdentifier: "ContactListCell")
    }
    
    func initialize(stakeholders: [Stakeholder], onHeightSet: @escaping (CGFloat) -> Void)
    {
        self.stakeholders = stakeholders
        self.onHeightSet = onHeightSet
        tableView.reloadData()
        //Spruce framework Animation
        DispatchQueue.main.async {
            
            self.tableView.spruce.animate([.fadeIn, .expand(.slightly)], sortFunction: LinearSortFunction(direction: .topToBottom, interObjectDelay: 0.1))
        }
        if stakeholders.count > 0 {
            tableView.dismissBackgroundMessage()
        } else {
            tableView.displayBackgroundMessage("No stakeholders found".localized(), subMessage: "")
        }
    }

    fileprivate func pushRatingControlFor(_ stakeholder: Stakeholder, view: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 50)))
    {
        if DeviceType.IS_ANY_IPAD == false
        {
            let vcRating : RatingViewController = Storyboard.getInstanceFromStoryboard("Rating")
            vcRating.didEndRating = { [weak self] in
                
                self?.setEditing(false, animated: true)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "loadProjectDetail"), object: nil)
            }
            vcRating.stakeholder = stakeholder
            self.present(vcRating, animated: true, completion: nil)
        }
        else
        {
            let vcRating = RatingViewController_iPad()
            vcRating.modalPresentationStyle = .popover
            self.present(vcRating, animated: true, completion: nil)
            vcRating.preferredContentSize  = CGSize(width: 280,height: 60)
            vcRating.stakeholder = stakeholder
            let datePopover = vcRating.popoverPresentationController
            datePopover?.permittedArrowDirections = .any
            datePopover?.sourceView = view
            datePopover?.sourceRect = view.frame
            datePopover?.backgroundColor = .black
            vcRating.didEndRating = {void in
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "loadProjectDetail"), object: nil)
            }
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return stakeholders?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 96
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListCell") as! ContactListCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none

        let stakeholder = stakeholders![indexPath.row]

        if let searchText = self.searchText {

            let mutableAttString = stakeholder.fullName.mutableAttributed
            self.highlightCount += mutableAttString.highlightText(searchText)
            cell.lblName.attributedText = mutableAttString
            let strCompanyName = stakeholder.companyName
            var strJobPosition = ""
            if let jobPosition = stakeholder.jobPosition?.value {
                
                strJobPosition = jobPosition
            }
            let attributedPosition = NSMutableAttributedString().setJobPositionCompany(strJobPosition, company: strCompanyName)
            
            self.highlightCount += attributedPosition.highlightText(searchText)
            cell.lblPosition.attributedText = (strJobPosition.trim().isEmpty && stakeholder.companyName.trim().isEmpty) ? nil : attributedPosition
        }
        else
        {
            cell.cellForStakeholder(stakeholder)
        }

        cell.imgActor.setImageWith(URL(string: stakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        cell.removeGestureRecognizersOfType(ActorTapGestureRecognizer.self)
        cell.didTapOnRating = {[weak self] void in
            
            self?.pushRatingControlFor(stakeholder, view: cell.lblUserRating)
        }
        cell.addGestureRecognizer(ActorTapGestureRecognizer(actor: stakeholder, onTap: {
            (actor) -> Void in
            
            if DeviceType.IS_ANY_IPAD == false
            {
                let vcStakeholderDetail : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                vcStakeholderDetail.stakeholder = actor
                self.navigationController?.pushViewController(vcStakeholderDetail, animated: true)
            }
            else
            {
                let vcStakeholderDetail = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
                vcStakeholderDetail.stakeholder = actor
                self.navigationController?.pushViewController(vcStakeholderDetail, animated: true)
            }
        }))
        
        
        return cell
    }
    
    func filterByText(_ text: String)
    {
        guard text.isEmpty == false else {
            
            self.stakeholders = self.stakeholdersBackup
            self.tableView?.reloadData()
            return
        }
        self.stakeholders = self.stakeholdersBackup.filter({$0.fullName.containsString(text)})
        self.tableView?.reloadData()
    }
}
