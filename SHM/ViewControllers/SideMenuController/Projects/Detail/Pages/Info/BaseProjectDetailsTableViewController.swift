//
//  BaseProjectDetailsTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 7/28/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class BaseProjectDetailsTableViewController: UITableViewController {
    
    internal var onHeightSet: ((CGFloat) -> Void)?
    internal var searchText: String?
    internal var highlightCount = 0

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func searchText(_ text: String) {
        searchText = text.isEmpty ? nil : text
        highlightCount = 0
        tableView.reloadData()
    }
    
    func setActive() {
        onHeightSet?(max(tableView.contentSize.height, 44))
    }
}
