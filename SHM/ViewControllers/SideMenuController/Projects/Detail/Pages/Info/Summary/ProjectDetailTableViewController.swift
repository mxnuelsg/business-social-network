//
//  ProjectDetailTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 20/08/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import MessageUI

class ProjectDetailTableViewController: BaseProjectDetailsTableViewController {

    var project: Project!
    var arraySections = ["", "", "Mitigation Measure".localized(),  "Coordinator".localized(), "Members".localized(), ""]
    
    let NameTrailingWithInbox: CGFloat = 107
    let NameTrailingWithoutInbox: CGFloat = 74
    var progressBarLoads = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        //Register cells
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 76.0
        
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    func initialize(project: Project, onHeightSet: @escaping (CGFloat) -> Void) {
        self.project = project
        self.onHeightSet = onHeightSet
        tableView.reloadData()
    }
    
    // MARK: - TABLE VIEW DATA SOURCE
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return arraySections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        guard project != nil else {
            return 0
        }
        
        if section == 4
        {
            return project.members.count
        }
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerTitle = SHMSectionTitleLabel()
        headerTitle.backgroundColor = UIColor.white
        headerTitle.font = UIFont.boldSystemFont(ofSize: 16)
        headerTitle.textColor = UIColor.colorDetailTitleLabel()
        headerTitle.text = arraySections[section]
        return headerTitle
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            //Progress Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProgressCell") as! ProgressTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            cell.lblProgress.text = "\(project.progress)%"
            cell.progressView.setProgress(Float(project.progress) / (100), animated: false)
            if self.progressBarLoads < 2
            {
                self.progressBarLoads = self.progressBarLoads + 1
                cell.progressView.transform = cell.progressView.transform.scaledBy(x: 1, y: 3)
            }
            
            return cell
        }
        else
        {
            if indexPath.section == 1
            {
                //Resume
                let cell = tableView.dequeueReusableCell(withIdentifier: "commonCell", for: indexPath)
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.removeGestureRecognizersOfType(ActionsTapGestureRecognizer.self)
                
                if let searchText = self.searchText {
                    
                    let mutableAttString = project.resume.mutableAttributed
                    self.highlightCount += mutableAttString.highlightText(searchText) 
                    cell.textLabel?.attributedText = mutableAttString
                }
                else
                {
                    cell.textLabel?.attributedText = nil
                    cell.textLabel?.text = project.resume
                }
                
                return cell
            }
            else if indexPath.section == 2
            {
                //Mitigation Measure
                let cell = UITableViewCell(style:.default, reuseIdentifier:"Cell")
                cell.selectionStyle = .none
                
                if let searchText = self.searchText {
                    
                    if let mitigation = self.project.mitigationMeasure, mitigation.isNull == false {
                        
                        let mutableAttString = mitigation.title.mutableAttributed
                        mutableAttString.highlightText(searchText)
                        cell.textLabel?.attributedText = mutableAttString
                    }
                    else
                    {
                        let mutableAttString = "No assigned".localized().mutableAttributed
                        mutableAttString.highlightText(searchText)
                        cell.textLabel?.attributedText = mutableAttString
                    }
                }
                else
                {
                    cell.textLabel?.attributedText  = nil
                    
                    if let mitigation = self.project.mitigationMeasure, mitigation.isNull == false {
                        
                        cell.textLabel?.text = mitigation.title
                    }
                    else
                    {
                        cell.textLabel?.text = "No assigned".localized()
                    }
                }
                
                return cell
            }
            else if indexPath.section == 3
            {
                //Coordinator
                let cell = tableView.dequeueReusableCell(withIdentifier: "reachableMemberCell") as! ReachableContactTableViewCell
                cell.selectionStyle = .none
                
                if let searchText = self.searchText {
                    
                    let mutableAttString = project.coordinator.getAttributedName()
                    self.highlightCount += mutableAttString.highlightText(searchText) 
                    cell.lblName?.attributedText = mutableAttString
                }
                else
                {
                    cell.lblName?.attributedText = project.coordinator.getAttributedName()
                    //cell.lblName?.text = project.coordinator.fullName
                }
                
                cell.removeGestureRecognizersOfType(ActionsTapGestureRecognizer.self)
                cell.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
                    let vcProfile = Storyboard.getInstanceOf(ProfileViewController.self)
                    vcProfile.member = self.project.coordinator
                    self.navigationController?.pushViewController(vcProfile, animated: true)
                }))
                
                addReachabilityToCell(cell, member: self.project.coordinator)
                
                return cell
            }
            else if indexPath.section == 4
            {
                //Members
                let member = project.members[indexPath.row]
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "reachableMemberCell") as! ReachableContactTableViewCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none

                if let searchText = self.searchText {
                    let mutableAttString = member.getAttributedName()
                    self.highlightCount += mutableAttString.highlightText(searchText) 
                    cell.lblName?.attributedText = mutableAttString
                }
                else
                {
                    cell.lblName?.attributedText = member.getAttributedName()
                }
                
                cell.removeGestureRecognizersOfType(ActionsTapGestureRecognizer.self)
                cell.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
                    
                    let vcProfile = Storyboard.getInstanceOf(ProfileViewController.self)
                    vcProfile.member = member
                    self.navigationController?.pushViewController(vcProfile, animated: true)
                }))
                
                addReachabilityToCell(cell, member: member)
                
                return cell
            }
            else
            {
                //== 5
                let cell = tableView.dequeueReusableCell(withIdentifier: "commonCell", for: indexPath)
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.textLabel?.text = "SHOULD BE HIDDEN!!"
                return cell
            }
        }
    }
    
    //MARK: TABLE VIEW DELEGATE
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        
        if indexPath.section == 2
        {
            //Show or hide Associated Whos depending role
            if LibraryAPI.shared.currentUser?.role != .globalManager && LibraryAPI.shared.currentUser?.role != .localManager
            {
                cell.isHidden = true
            }
            else
            {
                cell.isHidden = false
            }
        }
        else
        {
            cell.isHidden = false
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 3 || section == 4
        {
            return 30
        }
        
        if section == 2
        {
            //Show or hide Associated Whos depending role
            if LibraryAPI.shared.currentUser?.role != .globalManager && LibraryAPI.shared.currentUser?.role != .localManager
            {
                return CGFloat.leastNormalMagnitude
            }
            
            return 30
        }
        
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section != 5
        {
            if indexPath.section == 2
                && LibraryAPI.shared.currentUser?.role != .globalManager
                && LibraryAPI.shared.currentUser?.role != .localManager
            {
                return CGFloat.leastNormalMagnitude
            }
            
            return 44
        }
        
        return CGFloat.leastNormalMagnitude
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        if section == 2
            && LibraryAPI.shared.currentUser?.role != .globalManager
            && LibraryAPI.shared.currentUser?.role != .localManager
        {
            return CGFloat.leastNormalMagnitude
        }
        
        return 8
    }

    //MARK: CELL INTERACTION
    func addReachabilityToCell(_ cell: ReachableContactTableViewCell, member: Member) {
        if LibraryAPI.shared.currentUser!.role == UserRole.vipUser {
            cell.imgInbox.isUserInteractionEnabled = true
            cell.imgInbox.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: { () -> () in
                ProfileViewController.sendInbox(member: member, navigation: self.navigationController!)
            }))
            cell.constraintNameTrailing.constant = NameTrailingWithInbox
        }
        else
        {
            cell.imgInbox.isHidden = true
            cell.constraintNameTrailing.constant = NameTrailingWithoutInbox
        }
        
        cell.imgCall.isUserInteractionEnabled = true
        cell.imgCall.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: { () -> () in
            ProfileViewController.callMember(member, fromViewController: self)
        }))
        
        cell.imgMail.isUserInteractionEnabled = true
        cell.imgMail.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: { () -> () in
            ProfileViewController.sendMailWithProject(member: member, project: self.project, delegate: self, navigation: self.navigationController!)
        }))
    }
}

// MARK: MFMailComposeViewControllerDelegate Method
extension ProjectDetailTableViewController: MFMailComposeViewControllerDelegate
{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        controller.dismiss(animated: true) { () -> Void in
            
//            switch result.rawValue
//            {
//            case MFMailComposeResultCancelled.rawValue:
//                MessageManager.shared.showBarTitle("Mail Cancelled".localized(),
//                    type: .info,
//                    fromBottom: false)
//            case MFMailComposeResultSaved.rawValue:
//                MessageManager.shared.showBarTitle("Mail Saved".localized(),
//                    type: .info,
//                    fromBottom: false)
//            case MFMailComposeResultSent.rawValue:
//                MessageManager.shared.showBarTitle("Mail Sent".localized(),
//                    type: .success,
//                    fromBottom: false)
//            case MFMailComposeResultFailed.rawValue:
//                MessageManager.shared.showBar(title: "Error",
//                    subtitle:error!.localizedDescription,
//                    type: .error,
//                    fromBottom: false)
//            default:
//                break
//            }
        }
    }
}
