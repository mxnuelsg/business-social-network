//
//  ProjectDetailTableViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 1/13/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit
import MessageUI

class ProjectDetailTableViewController_iPad: UITableViewController
{
    //MARK: Properties and Outlets
    var project: Project!
    var isLeftTable = true
    
    let NameTrailingWithInbox: CGFloat = 107
    let NameTrailingWithoutInbox: CGFloat = 74
    let arraySectionsLeft = ["Coordinator".localized(), "Members".localized()]
    var arraySectionsRight = ["Mitigation Measure".localized(), "Stakeholders".localized()]
    
    //for search
    var searchText: String?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()

        //Show or hide mitigation measure title
        if LibraryAPI.shared.currentUser?.role != .globalManager && LibraryAPI.shared.currentUser?.role != .localManager
        {
            if self.isLeftTable == false
            {
                self.arraySectionsRight[0] = ""
            }
        }
        
        if self.project == nil
        {
            self.project = Project(isDefault: true)
        }
        
        self.tableView.register(UINib(nibName: "ContactListCell", bundle: nil), forCellReuseIdentifier: "ContactListCell")
        self.tableView.register(UINib(nibName: "coordinatorTableViewCell", bundle: nil), forCellReuseIdentifier: "coordinatorTableViewCell")
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 76.0
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Local search
    
    func searchText(_ text: String)
    {
        self.searchText = text.isEmpty ? nil : text
        self.tableView.reloadData()
    }

    //MARK: - TABLE VIEW DATASOURCE
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        if self.isLeftTable == true
        {
            return self.arraySectionsLeft.count
        }
        else
        {
            return self.arraySectionsRight.count
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.isLeftTable == true
        {
            return section == 0 ? 1 : self.project.members.count
        }
        else
        {
            return section == 0 ? 1 : self.project.stakeholders.count
        }
    }
    
    
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        if self.isLeftTable == true
        {
            return self.arraySectionsLeft[section]
        }
        else
        {
            return self.arraySectionsRight[section]
        }
    }

    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if self.isLeftTable == true
        {
            if indexPath.section == 0
            {   //Coordinator
                let member = self.project.coordinator
                let cell = tableView.dequeueReusableCell(withIdentifier: "coordinatorTableViewCell") as! coordinatorTableViewCell
                
                if let searchText = self.searchText {
                    
                    let mutableAttString = member.getAttributedName()
                    mutableAttString.highlightText(searchText)
                    cell.lblName.attributedText = mutableAttString
                }
                else
                {                    
                    cell.lblName.attributedText = member.getAttributedName()
                }

                cell.removeGestureRecognizersOfType(ActionsTapGestureRecognizer.self)
                cell.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
                    
                    let vcProfile = Storyboard.getInstanceOf(ProfileViewController_iPad.self)
                    vcProfile.member = member
                    self.navigationController?.pushViewController(vcProfile, animated: true)
                }))
                
                self.addReachabilityToCell(cell, member: member)
                
                return cell
            }
            else
            {   //Members
                let member = self.project.members[indexPath.row]
                let cell = tableView.dequeueReusableCell(withIdentifier: "coordinatorTableViewCell") as! coordinatorTableViewCell
                
                if let searchText = self.searchText {
                    
                    let mutableAttString = self.project.members[indexPath.row].getAttributedName()
                    mutableAttString.highlightText(searchText)
                    cell.lblName?.attributedText = mutableAttString
                }
                else
                {
                    cell.lblName?.attributedText = member.getAttributedName()
                }
                
                cell.removeGestureRecognizersOfType(ActionsTapGestureRecognizer.self)
                cell.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
                    
                    let vcProfile = Storyboard.getInstanceOf(ProfileViewController_iPad.self)
                    vcProfile.member = member
                    self.navigationController?.pushViewController(vcProfile, animated: true)
                }))
                
                self.addReachabilityToCell(cell, member: member)
                
                return cell
            }
        }
        else
        {
            if indexPath.section == 0
            {
                //Mitigation Measure
                let cell = UITableViewCell(style:.default, reuseIdentifier:"Cell")
                cell.selectionStyle = .none
                
                
                //Stakeholder information
                if let searchText = self.searchText {
                    
                    if let mitigation = self.project.mitigationMeasure, mitigation.isNull == false {
                        
                        let mutableAttString = mitigation.title.mutableAttributed
                        mutableAttString.highlightText(searchText)
                        cell.textLabel?.attributedText = mutableAttString
                    }
                    else
                    {
                        
                        let mutableAttString = "No assigned".localized().mutableAttributed
                        mutableAttString.highlightText(searchText)
                        cell.textLabel?.attributedText = mutableAttString
                    }
                }
                else
                {
                     cell.textLabel?.attributedText  = nil
                    
                    if let mitigation = self.project.mitigationMeasure, mitigation.isNull == false {
                        
                        cell.textLabel?.text = mitigation.title
                    }
                    else
                    {
                        cell.textLabel?.text = "No assigned".localized()
                    }
                }
                
                return cell
            }
            else
            {
                //Stakeholder
                let stakeholder = self.project.stakeholders[indexPath.row]
                let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListCell") as! ContactListCell
                
                //Stakeholder information
                if let searchText = self.searchText {
                    
                    let mutableAttString = stakeholder.fullName.mutableAttributed
                    mutableAttString.highlightText(searchText)
                    cell.lblName.attributedText = mutableAttString
                    
                    let strJobPosition = stakeholder.jobPosition?.value ?? ""
                    let strCompanyName = stakeholder.companyName 
                    let attributedPosition = NSMutableAttributedString().setJobPositionCompany(strJobPosition, company: strCompanyName)
                    attributedPosition.highlightText(searchText)
                    cell.lblPosition.attributedText = (strJobPosition.trim().isEmpty && stakeholder.companyName.trim().isEmpty) ? nil : attributedPosition
                }
                else
                {
                    cell.lblName.attributedText = nil
                    cell.lblName.text = "\(stakeholder.fullName)"
                    let strJobPosition = stakeholder.jobPosition?.value ?? ""
                    let strCompanyName = stakeholder.companyName 
                    let attributedPosition = NSMutableAttributedString().setJobPositionCompany(strJobPosition, company: strCompanyName)
                    cell.lblPosition.attributedText = (strJobPosition.trim().isEmpty && stakeholder.companyName.trim().isEmpty) ? nil : attributedPosition
                }
                
                //Profile Image
                cell.imgActor.setImageWith(URL(string: stakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
                cell.removeGestureRecognizersOfType(ActorTapGestureRecognizer.self)
                cell.addGestureRecognizer(ActorTapGestureRecognizer(actor: stakeholder, onTap: {
                    (actor) -> Void in
                    
                    let vcStakeholderDetail = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
                    vcStakeholderDetail.stakeholder = actor
                    self.navigationController?.pushViewController(vcStakeholderDetail, animated: true)
                }))
                
                return cell
            }
        }
    }
    
    //MARK: TABLE VIEW DELEGATE
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if self.isLeftTable == false
        {
            if indexPath.section == 0
            {
                //Show or hide Associated Whos depending role
                if LibraryAPI.shared.currentUser?.role != .globalManager && LibraryAPI.shared.currentUser?.role != .localManager
                {
                    cell.isHidden = true
                }
                else
                {
                    cell.isHidden = false
                }
            }
        }
        else
        {
            cell.isHidden = false
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if self.isLeftTable == false
        {
            if indexPath.section == 0
            {
                //Show or hide Associated Whos depending role
                if LibraryAPI.shared.currentUser?.role != .globalManager && LibraryAPI.shared.currentUser?.role != .localManager
                {
                    return CGFloat.leastNormalMagnitude
                }
                else
                {
                    return tableView.rowHeight
                }
            }
            else
            {
                return tableView.rowHeight
            }
        }
        else
        {
            return tableView.rowHeight
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if self.isLeftTable == false
        {
            if section == 0
            {
                //Show or hide Associated Whos depending role
                if LibraryAPI.shared.currentUser?.role != .globalManager && LibraryAPI.shared.currentUser?.role != .localManager
                {
                    return CGFloat.leastNormalMagnitude
                }
                else
                {
                    return tableView.sectionHeaderHeight
                }
            }
            else
            {
                return tableView.sectionHeaderHeight
            }
        }
        else
        {
            return tableView.sectionHeaderHeight
        }

    }
    
    //MARK: PRIVATE FUNCTIONS
    func addReachabilityToCell(_ cell: coordinatorTableViewCell, member: Member)
    {
        if LibraryAPI.shared.currentUser!.role == UserRole.vipUser && member.isVip == true
        {
            cell.imgInbox.isUserInteractionEnabled = true
            cell.imgInbox.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: { () -> () in
                
                ProfileViewController.sendInbox(member: member, navigation: self.navigationController!)
            }))
        }
        else
        {
            cell.imgInbox.isHidden = true
            cell.imgInbox.isUserInteractionEnabled = false
            cell.hideInbox()
        }
        
        if cell.imgCall.isHidden == false
        {
            cell.imgCall.isUserInteractionEnabled = true
            cell.imgCall.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: { () -> () in
                
                ProfileViewController.callMember(member, fromViewController: self)
            }))
        }
        
        
        cell.imgMail.isUserInteractionEnabled = true
        cell.imgMail.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: { () -> () in
            
            ProfileViewController.sendMailWithProject(member: member, project: self.project, delegate: self, navigation: self.navigationController!)
        }))
    }
}

// MARK: MFMailComposeViewControllerDelegate Method
extension ProjectDetailTableViewController_iPad: MFMailComposeViewControllerDelegate
{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        controller.dismiss(animated: true) { () -> Void in
            
//            switch result.rawValue
//            {
//            case MFMailComposeResultCancelled.rawValue:
//                MessageManager.shared.showBarTitle("Mail Cancelled".localized(),
//                    type: .info,
//                    fromBottom: false)
//            case MFMailComposeResultSaved.rawValue:
//                MessageManager.shared.showBarTitle("Mail Saved".localized(),
//                    type: .info,
//                    fromBottom: false)
//            case MFMailComposeResultSent.rawValue:
//                MessageManager.shared.showBarTitle("Mail Sent".localized(),
//                    type: .success,
//                    fromBottom: false)
//            case MFMailComposeResultFailed.rawValue:
//                MessageManager.shared.showBar(title: "Error",
//                    subtitle:error!.localizedDescription,
//                    type: .error,
//                    fromBottom: false)
//            default:
//                break
//            }
        }
    }
}

