//
//  ProjectPagesViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/20/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ProjectPagesViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var constraintContentWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintContentHeight: NSLayoutConstraint!
    
    @IBOutlet weak var containerViewCalendar: UIView!
    @IBOutlet weak var containerViewCalendarList: UIView!
    
    var onHeightChanged: ((_ newHeight: CGFloat) -> (CGFloat))!
    
    let pageCount = 3
    var currentPage = 0
    
    var vcDetail: ProjectDetailsSectionViewController!
    var vcFeed: ProjectFeedViewController!
    var vcCalendar: CalendarViewController!
    var vcCalendarList: CalendarListViewController!
    
    var totalDetailHeight: CGFloat = 0
    
    var onPageChange: ((_ page:Int) -> ())?
    var pageDidChange: (() -> ())?
    var project: Project!
    
    var childVC = [UIViewController]()
    
    // MARK: VIEW CONTROLLER LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let allWidth = UIScreen.main.bounds.width * CGFloat(pageCount)
        constraintContentWidth.constant = allWidth
        childVC = [vcDetail, vcFeed, vcCalendar]
    }
    
    override func viewDidLayoutSubviews() {
        let allWidth = UIScreen.main.bounds.width * CGFloat(pageCount)
        
        constraintContentWidth.constant = allWidth
        
        self.scrollView.contentSize = self.viewContent.frame.size
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.scrollView.contentSize = self.viewContent.frame.size
    }
    
    // MARK: PRIVATE METHODS
    
    fileprivate func setHeight(_ height: CGFloat) {
        self.constraintContentHeight.constant = height
        _ = self.onHeightChanged(height)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segueID_ProjectPageDetail"
        {
            vcDetail = segue.destination as! ProjectDetailsSectionViewController
            vcDetail.initialize(onHeightSet: { [weak self] (sectionHeight) in
                self?.setHeight(sectionHeight)
            })
        }
        else if segue.identifier == "segueID_ProjectPageFeed"
        {
            vcFeed = segue.destination as! ProjectFeedViewController
            vcFeed.onHeightChanged = {(newHeight) in
                
                print("NEW HEIGHT: \(newHeight) in \(type(of: self)))")
                
                if self.vcFeed.hasFocus == true
                {
                    let recommendedHeight = self.onHeightChanged(newHeight)
                    if recommendedHeight > newHeight {
                        self.constraintContentHeight.constant = recommendedHeight
                    }
                    else
                    {
                        self.constraintContentHeight.constant = newHeight
                    }
                }
                self.viewContent.layoutIfNeeded()
                
                return self.constraintContentHeight.constant
            }
        }
        else if segue.identifier == "segueID_ProjectPageCalendar"
        {
            vcCalendar = segue.destination as! CalendarViewController
            vcCalendar.loadByProjectId = project.id
            vcCalendar.onHeightChanged = { (newHeight) in
                
                print("NEW HEIGHT: \(newHeight) in \(type(of: self)))")
                
                if self.vcCalendar.hasFocus == true
                {
                    let recommendedHeight = self.onHeightChanged(newHeight)
                    
                    if recommendedHeight > newHeight
                    {
                        self.constraintContentHeight.constant = recommendedHeight
                    }
                    else
                    {
                        self.constraintContentHeight.constant = newHeight
                    }
                }
                self.viewContent.layoutIfNeeded()
                
                return self.constraintContentHeight.constant
            }
            containerViewCalendar.isHidden = false
        }
        else if segue.identifier == "segueID_ProjectPageCalendarList"
        {
            vcCalendarList = segue.destination as! CalendarListViewController
            vcCalendarList.loadByProjectId = project.id
            vcCalendarList.onHeightChanged = { (newHeight) in
                
                print("NEW HEIGHT: \(newHeight) in \(type(of: self)))")
                
                if self.vcCalendarList.hasFocus == true
                {
                    let recommendedHeight = self.onHeightChanged(newHeight)
                    
                    if recommendedHeight > newHeight
                    {
                        self.constraintContentHeight.constant = recommendedHeight
                    }
                    else
                    {
                        self.constraintContentHeight.constant = newHeight
                    }
                }
                self.viewContent.layoutIfNeeded()
                
                return self.constraintContentHeight.constant
            }
            containerViewCalendarList.isHidden = true
        }
    }
}

extension ProjectPagesViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let width = scrollView.frame.size.width;
        let page = Int((scrollView.contentOffset.x + (0.5 * width)) / width);
        if currentPage != page {
            currentPage = page
            onPageChange?(page)
            if page == 0 {
                self.setHeight(vcDetail.getSectionHeight())
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageDidChange?()
    }
}
