//
//  ProjectDetailViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas on 1/11/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class ProjectDetailViewController_iPad: UIViewController
{
    //MARK: Properties and Outlets
    
    //Lateral elements
    @IBOutlet weak var pageController: UIPageControl!
    var lateralContainer:LateralCalendarFeed_iPad?
    @IBOutlet weak var lblLateralContainer: UILabel!
    
    //SearchBar
    fileprivate lazy var searchBar: AutoSearchBar = AutoSearchBar()
    
    //Follow button
    @IBOutlet weak var btnOutletFollowing: UIButton!
    var isProjectFollowed:Bool!
    
    //status label 
    @IBOutlet weak var lblProjectInfo: UILabel!
    
    //Projects
    var project = Project(isDefault: true)

    //TextView
    @IBOutlet weak var textViewHeader: UITextView!
    
    @IBOutlet weak var lblProjectSite: UILabel!
    @IBOutlet weak var btnDisable: UIButton!
    
    //Bar buttons
    var btnBarItems = [UIBarButtonItem]()
    var barBtnActions: UIBarButtonItem!
    var barBtnCompose: UIBarButtonItem!
    
    //For new post
    var vcProjectPages: ProjectPagesViewController!
    
    //For search button
    var isModal = false
    var searchText: String?
    
    @IBOutlet weak var detailsContainer: UIView!
    fileprivate var detailsVC: ProjectDetailsSectionViewController?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig_iPad()
        self.loadProjectDetail()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
        
        //Reset Badge icon
        self.removeBadgeIcon()
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "loadProjectDetail"), object: nil)
    }
    
    //MARK: VALIDATE PERMISSIONS FOR EDITION
    func validateEditionPermissions() -> Bool
    {
        if (LibraryAPI.shared.currentUser?.role == .vipUser)
        || (LibraryAPI.shared.currentUser?.role == .globalManager)
        || self.project.coordinator.id == LibraryAPI.shared.currentUser?.id
        {
            return true
        }
        
        return false
    }
    
    //MARK: CREATE ACTIONS BUTTON
    func getActionsButton(target: AnyObject, action: Selector) -> UIBarButtonItem
    {
        let btnActions = UIButton(frame: CGRect(x: 0, y: 0, width: 80, height: 24))
        btnActions.setTitle("Actions  ▾".localized(), for: UIControlState())
        btnActions.titleLabel!.font = UIFont.systemFont(ofSize: 14)
        btnActions.layer.borderColor = UIColor.whiteColor(0.4).cgColor
        btnActions.layer.borderWidth = 1
        btnActions.layer.cornerRadius = 3
        btnActions.addTarget(target, action: action, for: UIControlEvents.touchUpInside)
        
        return UIBarButtonItem(customView: btnActions)
    }
    
    //MARK: SHOW VIEW CONTROLLER FOR PROJECT EDITION
    func showProjectEditionController()
    {
        //you will need the user SiteMappings
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        LibraryAPI.shared.projectBO.getCurrentUserSiteMappings({ (sites) in
            
            MessageManager.shared.hideHUD()
            let vcEditProject = Storyboard.getInstanceOf(ProjectNewViewController.self)
            vcEditProject.sites = sites
            vcEditProject.currentSite = self.project.site
            vcEditProject.project = self.project
            vcEditProject.onProjectEdited = { [weak self] Void in
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                    
                    self?.loadProjectDetail()
                }
                
                self?.navigationController?.presentedViewController?.dismiss(animated: true, completion: nil)
            }
            
            let navController = NavyController(rootViewController:vcEditProject)
            navController.modalPresentationStyle = .formSheet
            
            let btnCancel = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.cancelEditing))
            vcEditProject.navigationItem.leftBarButtonItem = btnCancel
            
            self.navigationController?.present(navController, animated: true, completion: nil)
            })
        { (error) in
            
            MessageManager.shared.hideHUD()
        }
    }
    
    //MARK: ACTION MENU
    func showMenuActions()
    {
        //Selected Style
        self.barBtnActions.customView?.backgroundColor = UIColor.blueTag()
        
        //Content View
        let vcTableOptions = StringTableViewController()
        vcTableOptions.modalPresentationStyle = .popover
        vcTableOptions.options = ["Project edition".localized()]
        vcTableOptions.icons = ["EditUser"]
        
        vcTableOptions.onSelectedOption = { [weak self] numberOfRow in
            
            if numberOfRow == 0
            {
                self?.showProjectEditionController()
            }
        }
        
        vcTableOptions.onClosed = { [weak self] Void in
            
            //Unselected Style
            self?.barBtnActions.customView?.backgroundColor = .clear
        }
        
        //Show Popover
        let popoverController = vcTableOptions.popoverPresentationController
        popoverController?.sourceView = self.view
        popoverController?.barButtonItem = self.barBtnActions
        popoverController?.permittedArrowDirections = .any
        
        self.present(vcTableOptions, animated: true, completion: nil)
    }
    
    func cancelEditing()
    {
     self.navigationController?.presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
    func comeBack()
    {
        self.navigationController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Load Configs
    func loadConfig_iPad()
    {
        self.textViewHeader.isEditable = false
        
        //Bar buttons for navigation control
        self.barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        let barBtnSearch = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(self.searchInDetail))
        let barBtnUpdate = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(self.updateProjectDetail))

        self.barBtnActions = self.getActionsButton(target: self, action: #selector(self.showMenuActions))
        btnBarItems = self.validateEditionPermissions() == true ? [self.barBtnCompose, barBtnSearch, barBtnUpdate,self.barBtnActions] :[self.barBtnCompose, barBtnSearch, barBtnUpdate]
        navigationItem.rightBarButtonItems = btnBarItems
        if self.isModal == true
        {
            //let barBtnBack = UIBarButtonItem(image: UIImage(named: "iconArrowLeft"), style:.Plain, target: self, action: "comeBack")
            let barBtnBack = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.comeBack))
            navigationItem.leftBarButtonItem = barBtnBack
        }
        
        
        //Text view
        self.setupNavigation(animated: true)
        
        searchBar.initialize(onSearchText: { [weak self] (text) -> () in
            self?.updateSearch(text: text)
            
            }, onCancel: { [weak self] in
                self?.searchBar.text = ""
//                self?.textViewProjectDetail.text = self?.project.resume
                self?.setupNavigation(animated: true)
                self?.detailsVC?.searchText("")
        })
        
        self.btnOutletFollowing.layer.cornerRadius = 2
        
        //Btn Disable
        if LibraryAPI.shared.currentUser?.role == .globalManager
        {
            self.btnDisable.layer.cornerRadius = 2
            self.btnDisable.clipsToBounds = true
            self.btnDisable.setTitle(String(), for: .normal)
            self.btnDisable.addTarget(self, action: #selector(self.enableOrDisableProject), for: .touchUpInside)
            self.btnDisable.isHidden = false
        }
        
        detailsVC = Storyboard.getInstanceFromStoryboard("Main")
        detailsContainer.addSubViewController(detailsVC!, parentVC: self)
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.loadProjectDetail), name: NSNotification.Name(rawValue: "loadProjectDetail"), object: nil)
    }
    
    //MARK: Private functions
    
    func createNewPost()
    {
        let vcNewPost = NewPostModal()
        vcNewPost.projectTagged = self.project
        vcNewPost.onViewControllerClosed = { [weak self] Void in
            
            //Update lateral Calendar-feeds
            self?.lateralContainer?.loadFeedForProject(self?.project)
            
            if vcNewPost.arrayDateAdded.count > 0
            {
                self?.loadProjectDetail()
            }
        }
        
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = (DeviceType.IS_ANY_IPAD) ?  .formSheet :  .overFullScreen
        //navController.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        
        present(navController, animated: true, completion: nil)
    }

    func loadProjectInfo(_ project: Project)
    {
        //Post
        self.barBtnCompose.isEnabled = project.isEnable
        
        //Header
        self.textViewHeader.text = project.title
        let strModifiedDateString = "\("Last Update".localized()) \(project.modifiedDate?.showStringFormat("MM/dd/yyyy") ?? "-") \("by".localized()) \(project.author?.fullName ?? "-")\n"
        let strProjectAttachmentInfo = (" \(project.files.count)" ) + " " + "Files".localized()
        let strHeaderLabel = (strModifiedDateString + strProjectAttachmentInfo).mutableAttributed
        strHeaderLabel.setAttributes([NSFontAttributeName:UIFont.systemFont(ofSize: 12),NSForegroundColorAttributeName:UIColor.grayConcreto()], range: NSRange(location:0, length:strHeaderLabel.length))
        self.lblProjectInfo.textAlignment = .right
        self.lblProjectInfo.attributedText = strHeaderLabel
        
        //Follow Button
        if project.followingStatus == true
        {
            self.btnOutletFollowing.setTitle("✔︎ Following".localized(), for: UIControlState())
        }
        else
        {
            self.btnOutletFollowing.setTitle("Follow".localized(), for: UIControlState())
        }
        
        //Project status
        if project.isEnable == true
        {
            self.btnDisable.setTitle("✔︎ " + " " + "Enabled".localized(), for: .normal)
        }
        else
        {
            self.btnDisable.setTitle("Enable".localized(), for: .normal)
        }
        
        if self.project.coordinator.id == LibraryAPI.shared.currentUser?.id
        {
            self.btnDisable.isHidden = false
        }
        
        detailsVC?.loadData(project: self.project)
    }
    
    func setupNavigation(animated: Bool)
    {
        self.title = "PROJECT".localized()
        navigationItem.setHidesBackButton(false, animated: animated)
        navigationItem.fadeOutTitleView()
    }

    //MARK: local Search 
    
    func updateSearch(text: String)
    {
        //Search in coordinators,members, stakeholders and project detail
        self.searchTextInProjectResume(text)
        self.detailsVC?.searchText(text)
        
        //Search in Lateral Container
        if self.pageController.currentPage == 0
        {
            //Search in Feeds/Attachments
            guard let segment = self.lateralContainer?.segmentedController.selectedSegmentIndex else
            {
                return
            }
            switch segment
            {
            case 0: //perform search in project feeds
                self.lateralContainer?.vcFeedList.filterByText(text)
                self.lateralContainer?.vcFeedList.reload()
            case 1: //perform search in project attachments
                self.lateralContainer?.vcFile.filterByText(text)
                self.lateralContainer?.vcFile.tableFiles.reloadData()
            default:
                break
            }
        }
        else //Search in Calendar
        {
            self.lateralContainer?.vcCalendar.filterByText(text)
        }
        
    }
    
    func searchTextInProjectResume(_ text: String)
    {
        searchText = text.isEmpty ? nil : text
        
        if let searchText = self.searchText{
            
            let resumeMutableAttString = self.project.resume.mutableAttributed
            resumeMutableAttString.addAttribute(NSFontAttributeName,
                value: UIFont.systemFont(ofSize: 14),
                range: NSRange(
                    location: 0,
                    length: (resumeMutableAttString.length)))
            resumeMutableAttString.highlightText(searchText)
        }
    }

    //MARK: Button Actions
    
    @IBAction func followBtnPressed(_ sender: AnyObject)
    {
        //if self.isProjectFollowed == true
        if self.project.followingStatus == true
        {
            LibraryAPI.shared.projectBO.unfollowProject(self.project.id,
                onSuccess: { (success) -> () in
                    
                    //Refresh Project List
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadProjectList"), object: nil)
                    
                    let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: delayTime) {
                        
                        let info = "Now, you're not following".localized() + " " + self.project.title
                        
                        MessageManager.shared.showBar(title: "Info".localized(),
                            subtitle: info,
                            type: .info,
                            fromBottom: false)
                        
                        //self.btnOutletFollowing.setTitle("Follow", forState: .Normal)
                        self.loadProjectDetail()
                        //self.isProjectFollowed = false
                    }
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            })
        }
        else
        {
            LibraryAPI.shared.projectBO.followProject(self.project.id,
                onSuccess: { (success) -> () in
                    
                    //Refresh Project List
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadProjectList"), object: nil)
                    
                    let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: delayTime) {
                        
                        let info = "Now, you're following".localized() + " " + self.project.title
                        
                        MessageManager.shared.showBar(title: "Info".localized(),
                            subtitle: info,
                            type: .info,
                            fromBottom: false)
                        
                        self.loadProjectDetail()                        
                    }
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "Error.TryAgain".localized(),
                        type: .error,
                        fromBottom: false)
            })
        }
        
        
    }
    
    func updateProjectDetail()
    {
        self.loadProjectDetail()
    }
    
    func searchInDetail()
    {
        
        searchBar.showInNavigationItem(navigationItem, animated: true)
        let barBtnCancel = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action:#selector(self.close))
        navigationItem.rightBarButtonItem = barBtnCancel
    }
    
    func close()
    {
        self.searchBar.cancelSearch()
        dismiss(animated: true) { () -> Void in
        }
        self.setupNavigation(animated: true)
        if self.isModal == true
        {
            let barBtnBack = UIBarButtonItem(image: UIImage(named: "iconArrowLeft"), style:.plain, target: self, action: #selector(self.comeBack))
            navigationItem.leftBarButtonItem = barBtnBack
        }
        
        navigationItem.rightBarButtonItems = btnBarItems
    }
    
    func enableOrDisableProject()
    {
        if self.project.isEnable == true
        {
            let vcConfirmUnfollow = UIAlertController(title: "Would you like to disable?".localized(), message: self.project.title, preferredStyle: .alert)
            let actionUnfollow = UIAlertAction(title: "Disable".localized(), style: .destructive, handler: {
                (action) in
                
                //Disable
                self.projectEnabled(false)
            })
            
            let actionCancel = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil)
            
            vcConfirmUnfollow.addAction(actionUnfollow)
            vcConfirmUnfollow.addAction(actionCancel)
            
            self.present(vcConfirmUnfollow, animated: true, completion: nil)
        }
        else
        {
            //Enable
            self.projectEnabled(true)
        }
    }
    
    private func projectEnabled(_ enable: Bool)
    {
        LibraryAPI.shared.projectBO.enable(self.project, enable: enable, onSuccess: { isEnable in
            
            //Refresh project List
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadProjectList"), object: nil)
            
            let status = isEnable == true ? "has been enabled".localized() : "has been disabled".localized()
            MessageManager.shared.showBar(title: "Info".localized(),
                                          subtitle: "Project" + " " + status,
                                          type: .info,
                                          fromBottom: false)
            
            self.project.isEnable = isEnable
            
            let title = isEnable == true ? "✔︎ " + " " + "Enabled".localized() : "Enable".localized()
            self.btnDisable.setTitle(title, for: .normal)
            
            self.barBtnCompose.isEnabled = self.project.isEnable
            
        }, onError: { error in
            
            MessageManager.shared.showBar(title: "Error",
                                          subtitle: "Error.TryAgain".localized(),
                                          type: .error,
                                          fromBottom: false)
        })
    }
    
    //MARK: Site mapping info
    func setSiteMappingInfo(_ site:Site?)
    {
        if let siteTitle = site?.title {
        
            self.lblProjectSite.isHidden = false
            self.lblProjectSite.text = "⚑ \(siteTitle)"
        }
    }
    
    //MARK: Web services
    func loadProjectDetail()
    {
        self.loadProjectDetailLight()
        self.loadStakeholdersByProject()
        self.loadGeographiesByProject()
        self.lateralContainer?.loadFeedForProject(project)
        self.lateralContainer?.loadCalendarForProject(project)
//        //Indicator
//        MessageManager.shared.showLoadingHUD()
//
//        let detail = "true"
//        LibraryAPI.shared.projectBO.getProjectDetailById(project.id, isDetail: detail, onSuccess: {
//            (project) -> () in
//
//            MessageManager.shared.hideHUD()
//
//            self.project = project
//            self.setSiteMappingInfo(self.project.site)
//            self.loadProjectInfo(self.project)
//
//            }) { error in
//
//                //Return to  Project List
//                MessageManager.shared.hideHUD()
//
//                _ = self.navigationController?.popViewController(animated: true)
//
//                MessageManager.shared.showBar(title: "Error",
//                    subtitle: "Error.TryAgain".localized(),
//                    type: .error,
//                    fromBottom: false)
//        }
    }
    
    fileprivate func loadProjectDetailLight()
    {
        MessageManager.shared.showLoadingHUD()
        LibraryAPI.shared.projectBO.getProjectDetailLight(project.id, onSuccess: { (project) in
            
            MessageManager.shared.hideHUD()
            self.project = project
            self.setSiteMappingInfo(self.project.site)
            self.loadProjectInfo(self.project)
            self.detailsVC?.loadSummary(self.project)
//            MessageManager.shared.hideHUD()
//            self.onDidLoadProjectLight?(project)
//            self.vcSummary?.project = project
//            self.vcSummary?.tableView.reloadData()
            
        }) { (error) in
            
            //Return to  Project List
            MessageManager.shared.hideHUD()
            
            _ = self.navigationController?.popViewController(animated: true)
            
            MessageManager.shared.showBar(title: "Error",
                                          subtitle: "Error.TryAgain".localized(),
                                          type: .error,
                                          fromBottom: false)
        }
    }
    
    fileprivate func loadStakeholdersByProject()
    {
        LibraryAPI.shared.stakeholderBO.getStakeholdersByProject( project.id, onSuccess: { stakeholders in
            
            self.project.stakeholders = stakeholders
            self.detailsVC?.loadStakeholders(self.project)
//            self.vcStakeholders?.stakeholders = stakeholders
//            self.vcStakeholders?.stakeholdersBackup = stakeholders
//            self.vcStakeholders?.tableView.reloadData()
        }) { (error) in
            
            MessageManager.shared.showBar(title: "Error",
                                          subtitle: "Error.TryAgain".localized(),
                                          type: .error,
                                          fromBottom: false)
        }
    }
    
    fileprivate func loadGeographiesByProject()
    {
        LibraryAPI.shared.geographyBO.getGeographiesByProject(project.id, onSuccess: { (geographies) in
            
            self.project.geographies = geographies
            self.detailsVC?.loadGeographies(self.project)
//            print(geographies.count)
//            self.vcGeographies?.clusterItems = geographies
//            self.vcGeographies?.backupClusterItems = geographies
        }) { (error) in
            MessageManager.shared.showBar(title: "Error",
                                          subtitle: "Error.TryAgain".localized(),
                                          type: .error,
                                          fromBottom: false)
        }
    }
    
    func loadProjectFeed()
    {
        //Indicators
        self.lateralContainer?.vcFeedList.displayBackgroundMessage("Loading...".localized(), subMessage: "")
        
        LibraryAPI.shared.feedBO.getProjectFeed(self.project, onSuccess: { posts in
            
            //Load the project details
            self.lateralContainer?.vcFeedList.posts = posts
            self.lateralContainer?.vcFeedList.tableView.reloadData()
            /*Here you need to check the pages controll*/
            //self.reloadFeedTable()
            
            //Empty State
            self.lateralContainer?.vcFeedList.posts.count == 0 ? self.lateralContainer?.vcFeedList.displayBackgroundMessage("No posts".localized(), subMessage: "") : self.lateralContainer?.vcFeedList.dismissBackgroundMessage()
            
        }) { error in
            
            self.lateralContainer?.vcFeedList.dismissBackgroundMessage()
            
            //Empty State
            if self.lateralContainer?.vcFeedList.posts.count == 0
            {
                self.lateralContainer?.vcFeedList.displayBackgroundMessage("Error".localized(), subMessage: "An Error has ocurred trying to load the Feed".localized())
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segueID_LateralCalendarFeed"
        {
            self.lateralContainer  = segue.destination as? LateralCalendarFeed_iPad

            self.lateralContainer?.onPageDidChange = { [weak self] page in
                
                self?.pageController.currentPage = page
                self?.lblLateralContainer.text = page == 0 ? "FEED".localized():"CALENDAR".localized()
            }
            
            self.lateralContainer?.onGoCalendar = { [weak self] Void in
                
                let vcCalendar = Storyboard.getInstanceOf(CalendarViewController_iPad.self)
                self?.navigationController?.pushViewController(vcCalendar, animated: true)
            }
        }
        
    }
    
    
}
