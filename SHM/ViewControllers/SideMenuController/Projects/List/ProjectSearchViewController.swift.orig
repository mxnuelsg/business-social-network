//
//  ProjectSearchTableViewController.swift
//  SHM
//
//  Created by Aldo R Bonilla Guerrero on 03/09/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import Spruce

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class ProjectSearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var searchBar: AutoSearchBar!
    @IBOutlet weak var tvResults: UITableView!

    
    var isModalView = true
    
    //Properties for New Post
    var onProjectSelected:((_ selectedProject: Project) -> ())?
    var onModalViewControllerClosed : (() -> ())?
    var willBeShowedInNewPost = false
    var popOnProjectSelected = false
    
    //This object arrange projects by site region and follow status
    var projectsBySite : ProjectsInOrder?
    var projects : [Project]?
    
    @IBOutlet weak var constraintTableTop: NSLayoutConstraint!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if isModalView == true
        {
            //Close Button
            let barBtnCancel = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.stop, target: self, action:#selector(self.close))
            navigationItem.leftBarButtonItem = barBtnCancel
        }
        
        if willBeShowedInNewPost == false
        {
            searchBar.becomeFirstResponder()
        }
        
        //initialize AutoSearchBar
        searchBar.initialize(shouldSearchAfterDelay: true,
            onSearchText: { [weak self] (text) in
                if (self != nil) {
                    self!.projects?.removeAll()
                    self!.projectsBySite?.clear()
                    let term = text.trimmingCharacters(in: CharacterSet.whitespaces)
                    if term.characters.count > 0
                    {
                        self!.searchProject(text)
                    }
                }
            }, onClearText: { [weak self] in
                self?.cancelSearch()
            }, onCancel: { [weak self] in
            self?.cancelSearch()
        })
        
        searchBar.showsCancelButton = false
        searchBar.tintColor = UIColor.colorForNavigationController()
        searchBar.placeholder = "Enter a Project Title".localized()
        
        //Just in case: New Post
        if self.willBeShowedInNewPost == true
        {
            self.searchBar.isHidden = self.willBeShowedInNewPost
            self.constraintTableTop.constant = -44
        }
        
        clearState()
        
        tvResults.rowHeight = UITableViewAutomaticDimension
        tvResults.estimatedRowHeight = 80
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        backButtonArrow()
    }
    
    // MARK: ACTIONS
    fileprivate func cancelSearch() {
        self.searchBar.text = String()
        self.clearState()
        self.tvResults.reloadData()
    }
    
    func clearState()
    {
        title = "PROJECT LIST".localized()
        self.projects?.removeAll()
        self.projectsBySite?.clear()
        tvResults.displayBackgroundMessage("NewProjectMessage".localized(),
            subMessage: "")
    }
    
    func close()
    {
        searchBar.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadProjects(_ projects:[Project])
    {
        //Projects by ordered by site and following status
        self.projects = projects
        self.projectsBySite = ProjectsInOrder(projects:self.projects ?? [Project]())
        
        if self.projectsBySite?.isEmpty() == true
        {
            if self.tvResults != nil
            {
                self.tvResults.displayBackgroundMessage("No results found".localized(),
                                                        subMessage: "")
            }
        }
        else
        {
            if self.tvResults != nil
            {
                self.tvResults.dismissBackgroundMessage()
            }
        }
    }
    
    // MARK: SHOW NEW PROJECT VIEW CONTROLLER
    func createNewProject(_ sites:[Site]?)
    {
        //Create new project in new post
        if self.willBeShowedInNewPost == true
        {
            let vcProjectNew = Storyboard.getInstanceOf(ProjectNewViewController.self)
            vcProjectNew.isNewProject = true
            vcProjectNew.sites = sites
            vcProjectNew.onProjectCreated = {
                (new) in
                self.projects?.append(new)
                self.loadProjects(self.projects ?? [new])
                self.onProjectSelected?(new)
            }
            
            let navController = NavyController(rootViewController: vcProjectNew)
            
            //Type of Device
            if DeviceType.IS_ANY_IPAD == true
            {
                //FormSheet Style
                vcProjectNew.onProjectModalClosed = {
                    
                    self.onModalViewControllerClosed?()
                }
                
                navController.modalPresentationStyle = .formSheet
                self.present(navController, animated:true, completion: nil)
            }
            else
            {
                //Modal Style
                let vcModal = ModalViewController(subViewController: navController)
                vcModal.onModalViewControllerClosed = {
                    
                    self.onModalViewControllerClosed?()
                }
                
                self.present(vcModal, animated:true, completion: nil)
            }
        }
        else
        {
            //Creating New Project
            let vcProjectNew = Storyboard.getInstanceOf(ProjectNewViewController.self)
            vcProjectNew.isNewProject = true
            vcProjectNew.sites = sites
            vcProjectNew.searchTerm = searchBar.text ?? ""
            vcProjectNew.onProjectCreated = {
                (new) in
                
                //Search Project added
                if let textTerm = self.searchBar.text {
                    
                    let text = textTerm.trimmingCharacters(in: CharacterSet.whitespaces)
                    
                    if text.characters.count > 0
                    {
                        self.searchProject(text)
                    }
                }
            }
            vcProjectNew.sites = sites
            //Type of Presentation
            let navController = NavyController(rootViewController: vcProjectNew)
            
            if DeviceType.IS_ANY_IPAD == true
            {
                navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
            }
            
            present(navController, animated: true, completion: nil)
        }
    }
    
    // MARK: EXECUTE SERVICE
    func searchProject(_ term: String)
    {
        tvResults.displayBackgroundMessage("Searching...".localized(),
            subMessage: "")
        
        LibraryAPI.shared.projectBO.searchAllProjects(term, onSuccess: { projects in
            
            self.tvResults.dismissBackgroundMessage()
            
            self.loadProjects(projects)
            self.tvResults.reloadData()
            
            //Spruce framework Animation
            DispatchQueue.main.async {
                
                self.tvResults.spruce.animate([.slide(.left, .severely), .fadeIn], animationType: SpringAnimation(duration: 0.8))
            }

            
            }) { error in
                
                self.tvResults.dismissBackgroundMessage()
                self.tvResults.reloadData()
        }
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int
    {
        //return 3
        return (self.projectsBySite?.arraySectionTitles.count ?? 0) + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section < self.projectsBySite?.arraySectionTitles.count
        {
            return self.projectsBySite?.arrayProjectsBySiteAndFollowStatus[section].count ?? 0
        }

        return 1
    }
    
    // MARK: - TableView Delegate
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section < self.projectsBySite?.arraySectionTitles.count
        {
            return 40
        }
        
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section < self.projectsBySite?.arraySectionTitles.count
        {
            let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier:"Cell")
            
            //Selected Color
            let selectedColor = UIView()
            selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
            cell.selectedBackgroundView = selectedColor
            
            cell.imageView?.image = nil
            cell.textLabel?.textColor = UIColor.blackAsfalto()
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            cell.textLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
            cell.textLabel?.numberOfLines = 0
            
            let project = self.projectsBySite?.arrayProjectsBySiteAndFollowStatus[indexPath.section][indexPath.row] ?? Project(isDefault:true)
            
            
            let siteTitle = "⚑ \(project.site?.title ?? "")"
            let projectInfo = NSMutableAttributedString(string: "\(project.title)\n\n\(siteTitle)")
            projectInfo.setColor(siteTitle, color: UIColor.grayConcreto())
            projectInfo.setBold(siteTitle, size: 11.0)
            
            cell.textLabel?.attributedText = projectInfo
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "commonCell", for: indexPath)
            
            //Selected Color
            let selectedColor = UIView()
            selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
            cell.selectedBackgroundView = selectedColor
            
            cell.imageView?.image = UIImage(named: "Add")
            cell.textLabel?.textColor = UIColor.darkText
            cell.textLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 13)
            cell.textLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
            cell.textLabel?.numberOfLines = 1
            
            cell.textLabel?.text = "Add Project".localized()
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        if indexPath.section < self.projectsBySite?.arraySectionTitles.count
        {
            return true
        }
        
        return false
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        if self.projectsBySite?.arrayProjectsBySiteAndFollowStatus[indexPath.section][indexPath.row].followingStatus == true
        {
            let unfollow = UITableViewRowAction(style: .default, title: "Unfollow".localized(), handler: {
                (action, indexPath) -> () in
                
                let projectToUnfollow = self.projectsBySite?.arrayProjectsBySiteAndFollowStatus[indexPath.section][indexPath.row] ?? Project(isDefault:true)
                tableView.setEditing(false, animated: true)
                
                
                LibraryAPI.shared.projectBO.unfollowProject(projectToUnfollow.id, onSuccess: {
                    (success) -> () in
                    
                    //Reload Project List
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadProjectList"), object: nil)
                    for (index, project) in (self.projects ?? [Project]()).enumerated() where projectToUnfollow.id == project.id
                    {
                        self.projects?[index].followingStatus = false
                    }
                    self.projectsBySite?.updateProjects(self.projects)
                    self.tvResults.reloadData()
                    
                    //Spruce framework Animation
                    DispatchQueue.main.async {
                        
                        self.tvResults.spruce.animate([.slide(.right, .severely), .fadeIn], animationType: SpringAnimation(duration: 0.8))
                    }

                    }) { error in
                        MessageManager.shared.showBar(title: "Error", subtitle: "Error.TryAgain".localized(), type: .error, fromBottom: false)
                }
            })
            unfollow.backgroundColor = UIColor.redUnfollowAction()
            return [unfollow]
        }
        else if self.projectsBySite?.arrayProjectsBySiteAndFollowStatus[indexPath.section][indexPath.row].followingStatus == false
        {
            let follow = UITableViewRowAction(style: .normal, title:"Follow".localized(), handler: {
                (action, indexPath) -> () in
                
                let projectToFollow = self.projectsBySite?.arrayProjectsBySiteAndFollowStatus[indexPath.section][indexPath.row] ?? Project(isDefault:true)
                tableView.setEditing(false, animated: true)
                
                
                LibraryAPI.shared.projectBO.followProject(projectToFollow.id, onSuccess: {
                    (success) -> () in
                    
                    //Reload Project List
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadProjectList"), object: nil)
                    for (index, project) in (self.projects ?? [Project]()).enumerated() where projectToFollow.id == project.id
                    {
                        self.projects?[index].followingStatus = true
                    }
                    self.projectsBySite?.updateProjects(self.projects)
                    self.tvResults.reloadData()
                    
                    //Spruce framework Animation
                    DispatchQueue.main.async {
                        
                        self.tvResults.spruce.animate([.slide(.right, .severely), .fadeIn], animationType: SpringAnimation(duration: 0.8))
                    }

                    }) { error in
                        
                        MessageManager.shared.showBar(title: "Error", subtitle: "Error.TryAgain".localized(), type: .error, fromBottom: false)
                }
            })
            follow.backgroundColor = UIColor.greenFollowAction()
            return [follow]
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        searchBar.resignFirstResponder()
        
        if indexPath.section < self.projectsBySite?.arraySectionTitles.count
        {
            let projectSelected = self.projectsBySite?.arrayProjectsBySiteAndFollowStatus[indexPath.section][indexPath.row] ?? Project(isDefault:true)
            
            if self.popOnProjectSelected == true
            {
                //Return Project to Stakeholder Edit
                self.onProjectSelected?(projectSelected)
                _ = navigationController?.popViewController(animated: true)
            }
            else if self.willBeShowedInNewPost == true
            {
                //Return Project
                self.onProjectSelected?(projectSelected)
            }
            else
            {
                //Show Project Detail
                if DeviceType.IS_ANY_IPAD == true
                {
                    let vcProjectDetail = Storyboard.getInstanceOf(ProjectDetailViewController_iPad.self)
                    vcProjectDetail.project = projectSelected
                    vcProjectDetail.isModal = true
                    
                    let navController = NavyController(rootViewController: vcProjectDetail)
                    present(navController, animated: true, completion: nil)
                }
                else
                {
                    let vcProjectDetail = Storyboard.getInstanceOf(ProjectDetailViewController.self)
                    vcProjectDetail.project = projectSelected
                    self.navigationController?.pushViewController(vcProjectDetail, animated: true)
                }
            }
        }
        else
        {
            //Proceed to create a new project, you will need the SiteMappings
            //Indicator
            MessageManager.shared.showLoadingHUD()
            
            LibraryAPI.shared.projectBO.getCurrentUserSiteMappings({
                (sites) in
                
                    MessageManager.shared.hideHUD()
                    self.createNewProject(sites)
                },
                onError: { (error) in
                    
                    MessageManager.shared.hideHUD()
            })
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let lblTitle = SHMSectionTitleLabel()
        lblTitle.backgroundColor = UIColor.groupTableViewBackground
        lblTitle.font = UIFont.boldSystemFont(ofSize: 12)
        lblTitle.textColor = UIColor.grayConcreto()
        
        //if section == 0 || section == 1
        if section < self.projectsBySite?.arraySectionTitles.count
        {
            
            lblTitle.numberOfLines = 2
            lblTitle.attributedText = self.projectsBySite?.arraySectionTitles[section]
            lblTitle.backgroundColor = UIColor.graySectionSeparator()
        }
        else
        {
            lblTitle.text = ""
        }
        
        return lblTitle
    }
    
    // MARK: SCROLL
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if self.willBeShowedInNewPost == false
        {
            self.searchBar.resignFirstResponder()
        }
    }
}
<<<<<<< HEAD
=======

// MARK: SEARCH BAR
extension ProjectSearchViewController: UISearchBarDelegate
{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        //Show Cancel
        searchBar.setShowsCancelButton(true, animated: true)
        searchBar.tintColor = DeviceType.IS_ANY_IPAD == true ? .black : .white
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        let text = searchText.trim()
        
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        if text.characters.count > 0
        {
            perform(#selector(self.searchProject(_:)), with: text, afterDelay: Constants.TimeInterval.SearchWritingDelay)
        }
        else
        {
            perform(#selector(UISearchBarDelegate.searchBarCancelButtonClicked(_:)), with: searchBar, afterDelay: 0.1)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        //Hide Cancel
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        
        self.projects?.removeAll()
        self.projectsBySite?.clear()
        
        if let textTerm = searchBar.text as String!
        {
            let text = textTerm.trimmingCharacters(in: CharacterSet.whitespaces)
            
            if text.characters.count > 0
            {
                searchProject(text)
            }
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        //Hide Cancel
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.text = String()
        searchBar.resignFirstResponder()
        
        clearState()
        tvResults.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar)
    {
        if searchBar.text?.isEmpty == true
        {
            NSObject.cancelPreviousPerformRequests(withTarget: self)
            perform(#selector(UISearchBarDelegate.searchBarCancelButtonClicked(_:)), with: searchBar, afterDelay: 0.1)
        }
    }
}
>>>>>>> develop
