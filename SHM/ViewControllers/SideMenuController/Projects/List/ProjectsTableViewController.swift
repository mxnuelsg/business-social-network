//
//  ProjectsTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 7/24/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ProjectsTableViewController: UITableViewController
{
    @IBOutlet weak var barBtn: UIBarButtonItem!
    
    var projectsFiltered = [Project]()
    var onProjectListNeedsUpdate : (() -> ())?
    var searchText: String?
    var willDisplayLastRow: ((_ row:Int) -> ())?
    var projectSelected: Project!
    var isSwipeable = true
    var showsFromProfile = false
    var didUpdateProjectList:(()->())?
    var projects = [Project]() {
        didSet {
            //Order by Date or Title
            let orderByTitle = UserDefaultsManager.getProjectOrder()
           
            if orderByTitle == true
            {
                self.projects = self.projects.sorted(by:{ $0.title < $1.title })
            }
            else
            {
                 self.projects = self.projects.sorted(by:{ $0.modifiedDate! < $1.modifiedDate! })
            }
            
            //Searching
            if let searchText = self.searchText {
                
                projectsFiltered = LibraryAPI.shared.projectBO.filterProjects(self.projects, byText: searchText)
            }
            else
            {
                projectsFiltered = self.projects
            }
        }
    }

    
    // MARK: - LIFE CLYCLE 
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.hideEmtpyCells()
        
        loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: INITIAL CONFIG
    func loadConfig()
    {
        //Navigation Bar
        let navbarFont = UIFont(name: "HelveticaNeue-Bold", size: 15) ?? UIFont.systemFont(ofSize: 15)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: navbarFont, NSForegroundColorAttributeName: UIColor.white]
        
        //Register Custom Cell
        tableView.register(UINib(nibName: "ProjectTableCell", bundle: nil), forCellReuseIdentifier: "ProjectTableCell")
        self.tableView.backgroundColor = UIColor.white
    }
    
    // MARK: PRIVATE METHODS
    func filterByText(_ text: String)
    {
        guard text.isEmpty == false else {
            if searchText != nil {
                projectsFiltered = projects
                searchText = nil
                tableView.reloadData()
            }
            
            return
        }
        projectsFiltered = LibraryAPI.shared.projectBO.filterProjects(projects, byText: text)
        searchText = text.isEmpty ? nil : text
        tableView.reloadData()
    }
    
    func pushActor(_ actor: Stakeholder)
    {
        if DeviceType.IS_ANY_IPAD == true
        {
            let actorDetailVC = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
            actorDetailVC.stakeholder = actor
            navigationController?.pushViewController(actorDetailVC, animated: true)
        }
        else
        {
            let actorDetailVC: SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
            actorDetailVC.stakeholder = actor
            navigationController?.pushViewController(actorDetailVC, animated: true)
        }
    }
    
    func reload()
    {
        if projectsFiltered.count == 0
        {
            tableView.displayBackgroundMessage("No results".localized(),
                subMessage: "Pull to refresh".localized())
        }
        else
        {
            tableView.dismissBackgroundMessage()
        }
        
        tableView.reloadData()
    }
    
    
    func pushNewPost(_ projectTag: Project!)
    {
        if DeviceType.IS_ANY_IPAD == true
        {
            let vcNewPost = NewPostModal()
            vcNewPost.projectTagged = projectTag
            
            let navController = NavyController(rootViewController: vcNewPost)
            navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
            
            self.present(navController, animated: true, completion: nil)
        }
        else
        {
            let vcNewPost = NewPostViewController()
            vcNewPost.projectTagged = projectTag
            
            let navController = NavyController(rootViewController: vcNewPost)
            navController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    func clear() {
        projects = []
        projectsFiltered = []
        tableView.reloadData()
    }
    
    // MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if DeviceType.IS_ANY_IPAD == true
        {
            switch(segue.identifier!)
            {
            case "ProjectDetailViewController_iPad":
                
                let vcProjectDetail = segue.destination as! ProjectDetailViewController_iPad
                vcProjectDetail.project = projectSelected
                
            default:
                return
            }
        }
        else
        {
            switch(segue.identifier!)
            {
            case "ViewProjectDetail":
                
                let vcProjectDetail = segue.destination as! ProjectDetailViewController
                vcProjectDetail.project = projectSelected
                vcProjectDetail.onProjectWasEdited = { [weak self] Void in
                    
                    self?.onProjectListNeedsUpdate?()
                }
                
            default:
                return
            }
        }
    }
}

// MARK: TABLE VIEW DATASOURCE
extension ProjectsTableViewController
{
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //Background Message
        if projectsFiltered.count > 0
        {
            tableView.dismissBackgroundMessage()
        }
        else
        {
            //tableView.displayBackgroundMessage("No Projects".localized(), subMessage: "")
        }

        return projectsFiltered.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProjectTableCell") as! ProjectTableCell
        
        //MARK: Check if its correct because tha app Crashes when pull to update several times (projectsFiltered.count == 0)
        guard projectsFiltered.count > 0 else {
            
            return cell
        }
        
        let project = projectsFiltered[indexPath.row]
        
        //Color for selected cell
        let selectedColor = UIView()
        selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
        cell.selectedBackgroundView = selectedColor
        cell.ivSwipeDots.isHidden = self.isSwipeable == true ? false : true
        
        cell.cellForProject(cell, project: project)
        
        cell.onStakeholderTap = { [weak self] stakeholder in
            
            //Enabled or Disabled
            guard stakeholder.isEnable == true else {
                
                MessageManager.shared.showBar(title: stakeholder.fullName, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                
                return
            }
            
            self?.pushActor(stakeholder)
        }
        
        cell.onAllStakeholdersTap = { [weak self] Void in
            
            let stakeholdersTableVC = StakeholdersTableViewController.getNewInstance()
            stakeholdersTableVC.stakeholders = project.stakeholders
            
            let vcModal = ModalViewController(subViewController: stakeholdersTableVC)
            self?.present(vcModal, animated:true, completion: nil)
        }
        cell.onFilesTap = { [weak self] Void in
            
            let vcFileList = FileListViewController(attachments: project.files)
            let vcModal = ModalViewController(subViewController: vcFileList)
            vcModal.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            vcModal.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            
            self?.present(vcModal, animated:true, completion: nil)
        }
        
        cell.onTextViewTap = { [weak self] Void in
            
            self?.projectSelected = project
            guard self?.showsFromProfile == false else {
                let vcProjectDetail = Storyboard.getInstanceOf(ProjectDetailViewController_iPad.self)
                vcProjectDetail.project = (self?.projectSelected) ?? Project(isDefault: true)
                self?.navigationController?.pushViewController(vcProjectDetail, animated: true)
                return
            }
            if DeviceType.IS_ANY_IPAD == true
            {
                self?.performSegue(withIdentifier: "ProjectDetailViewController_iPad", sender: nil)
            }
            else
            {
                let vcProjectDetail: DetailProjectViewController = Storyboard.getInstanceFromStoryboard("Projects")
                vcProjectDetail.project = self?.projectSelected ?? Project(isDefault:true)
                vcProjectDetail.onProjectWasEdited = { [weak self] Void in
                    
                    self?.onProjectListNeedsUpdate?()
                }
                self?.navigationController?.pushViewController(vcProjectDetail, animated: true)
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 155
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let cellToDisplay = cell as! ProjectTableCell
        cellToDisplay.layoutConfForCell(cellToDisplay)
        
        if indexPath.row == projectsFiltered.count - 1
        {
            willDisplayLastRow?(indexPath.row)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        projectSelected = projectsFiltered[indexPath.row]
        guard self.showsFromProfile == false else {
            let vcProjectDetail = Storyboard.getInstanceOf(ProjectDetailViewController_iPad.self)
            vcProjectDetail.project = projectSelected
            self.navigationController?.pushViewController(vcProjectDetail, animated: true)
            return
        }
        if DeviceType.IS_ANY_IPAD == true
        {
            self.performSegue(withIdentifier: "ProjectDetailViewController_iPad", sender: nil)
        }
        else
        {
            let vcProjectDetail: DetailProjectViewController = Storyboard.getInstanceFromStoryboard("Projects")
            vcProjectDetail.project = projectSelected
            vcProjectDetail.onProjectWasEdited = { [weak self] Void in
                
                self?.onProjectListNeedsUpdate?()
            }
            self.navigationController?.pushViewController(vcProjectDetail, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        if indexPath.row < projectsFiltered.count
        {            
            let project = self.projectsFiltered[indexPath.row]
            
             //POST ACTION
            let actionPost = UITableViewRowAction(style: .normal, title: "Post".localized(), handler: {(action, indexPath) -> () in
                tableView.setEditing(false, animated: true)
                self.pushNewPost(project)
                self.tableView.setEditing(false, animated: true)
            })
            actionPost.backgroundColor = UIColor.blueColorNewPost()
            
            //Verify Status
            if project.followingStatus == true
            {
                //Following
                let unfollowAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Unfollow".localized(), handler: { (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
                    
                    let alertUnfollowConfirmed = UIAlertController(title: "Would you like to stop following this project?".localized(),
                        message: project.title,
                        preferredStyle: (DeviceType.IS_ANY_IPAD == true) ? .alert : .actionSheet)
                    
                    let unfollow = UIAlertAction(title: "Unfollow".localized(), style: UIAlertActionStyle.destructive, handler: {
                        (action) in
                        
                        LibraryAPI.shared.projectBO.unfollowProject(project.id, onSuccess: {
                            (success) -> () in
                            
    //                            self.projectsFiltered.removeAtIndex(indexPath.row)
    //                            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                            self.didUpdateProjectList?()
                            let delayTimePrincipal = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                            DispatchQueue.main.asyncAfter(deadline: delayTimePrincipal) {
                                
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadProjectList"), object: nil)
                                
                                let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                                    
                                    let info = "Now, you're not following:".localized() + " " + project.title
                                    
                                    MessageManager.shared.showBar(title: "Info".localized(), subtitle: info, type: .info, fromBottom: false)
                                }
                            }
                            }, onError: { error in
                                
                                MessageManager.shared.showBar(title: "Error", subtitle: "Error.TryAgain".localized(), type: .error, fromBottom: false)
                        })
                        
                        self.tableView.setEditing(false, animated: true)
                    })
                    
                    let actionCancel = UIAlertAction(title:
                        "Cancel".localized(), style: UIAlertActionStyle.cancel, handler: nil)
                    
                    alertUnfollowConfirmed.addAction(unfollow)
                    alertUnfollowConfirmed.addAction(actionCancel)
                    
                    self.present(alertUnfollowConfirmed, animated: true, completion: nil)
                })
                
                unfollowAction.backgroundColor = UIColor.redUnfollowAction()
                
                return project.isEnable == true ? [unfollowAction, actionPost] : [unfollowAction]
            }
            else
            {
                //Not following
                let followAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Follow".localized(), handler: { (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
                    
                    let alertFollowConfirmed = UIAlertController(title: "Would you like to follow this project?".localized(),
                        message: project.title,
                        preferredStyle: (DeviceType.IS_ANY_IPAD == true) ? .alert : .actionSheet)
                    
                    let follow = UIAlertAction(title: "Follow".localized(), style: UIAlertActionStyle.destructive, handler: {
                        (action) in
                        
                        LibraryAPI.shared.projectBO.followProject(project.id, onSuccess: {
                            (success) -> () in
                            
                            //                            self.projectsFiltered.removeAtIndex(indexPath.row)
                            //                            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                            self.didUpdateProjectList?()
                            let delayTimePrincipal = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                            DispatchQueue.main.asyncAfter(deadline: delayTimePrincipal) {
                                
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadProjectList"), object: nil)
                                
                                let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                                    
                                    let info = "Now, you're following:".localized() + " " + project.title
                                    
                                    MessageManager.shared.showBar(title: "Success".localized(), subtitle: info, type: .success, fromBottom: false)
                                }
                            }
                            }, onError: { error in
                                
                                MessageManager.shared.showBar(title: "Error", subtitle: "Error.TryAgain".localized(), type: .error, fromBottom: false)
                                
                        })
                        
                        self.tableView.setEditing(false, animated: true)
                    })
                    
                    let actionCancel = UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler: nil)
                    
                    alertFollowConfirmed.addAction(follow)
                    alertFollowConfirmed.addAction(actionCancel)
                    
                    self.present(alertFollowConfirmed, animated: true, completion: nil)
                })
                
                followAction.backgroundColor = UIColor.greenFollowAction()
                
                return project.isEnable == true ? [followAction, actionPost] : [followAction]
            }
        }
        else
        {
            return nil
        }
    }
}
