//
//  ProjectsViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/13/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ProjectsViewController: UIViewController
{
    @IBOutlet weak var segmentedProjects: UISegmentedControl!
    @IBOutlet weak var constraintContainerTop: NSLayoutConstraint!
    
    fileprivate lazy var searchBar: AutoSearchBar = AutoSearchBar()
    var vcProjectsTable: ProjectsTableViewController!
    var searchPaging = Paging()
    var isSearching = false
    var status: FollowStatus?
    
    var projects = [Project]()
    var filterSites = [Site]()
    var selectedFilter = 0
    var filtersExist = false
    var isSearchByText = false
    var isPagination = false
    
    //Tab Bar
    @IBOutlet weak var navigationItemProjects: UINavigationItem!
    @IBOutlet weak var tabBarItemNewProject: UITabBarItem!
    @IBOutlet weak var tabBarItemSearchProjects: UITabBarItem!
    @IBOutlet weak var tabBarItemFilterProject: UITabBarItem!
    @IBOutlet weak var tabBar: UITabBar!
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.loadConfig()
        
        self.searchPaging.pageNumber = 1
        self.getReadySearch(.followingStatus_Pagination_NoSearchText)
        self.getProjectList()
        
        //Avoiding duplicate data by lastRow
        self.searchPaging.pageNumber += 1
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        backButtonArrow()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        self.searchBar.isActive = false
        //Cancel Previous Requests
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.GetProjectList)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "reloadProjectList"), object: nil)
    }
    
    //MARK: LOAD CONFIG
    func loadConfig()
    {
        //Localizations
        self.localize()
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.reloadProjectList), name: NSNotification.Name(rawValue: "reloadProjectList"), object: nil)

        self.setupNavigation(animated:false)
        
        self.vcProjectsTable.displayBackgroundMessage("Loading...".localized(),
                                                      subMessage: "")
        
        //Segemented Control
        if showSplitList() == true
        {
            self.segmentedProjects.isHidden = false
            self.constraintContainerTop.constant = 44
        }
        
        self.segmentedProjects.selectedSegmentIndex = 0
        self.segmentedProjects.addTarget(self, action: #selector(self.segmentedControlHandler(_:)), for: UIControlEvents.valueChanged)

        
        self.vcProjectsTable.enableRefreshAction({
            
            self.searchPaging.pageNumber = 1
            
            if let text = self.searchBar.text, self.isSearching == true && text.trim().isEmpty == false
            {
                self.getReadySearch(.followingStatus_NoPagination_SearchText)
                self.vcProjectsTable.refreshed()
            }
            else
            {
                //Reset filter
                self.selectedFilter = 0
                self.tabBarItemFilterProject.title = "Filter".localized()
                self.tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
                
                self.vcProjectsTable.projects.removeAll()
                self.vcProjectsTable.reload()
                self.vcProjectsTable.refreshed()
                
                self.getReadySearch(.followingStatus_Pagination_NoSearchText)
                self.getProjectList(showsLoadMessage: false)
                
                self.searchPaging.pageNumber += 1
            }
        })
        
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        searchBar.initialize(shouldSearchAfterDelay: true,
        onSearchText: { [weak self] (text) in
            
            if (self != nil) {
                self!.searchPaging.pageNumber = 1
                self!.vcProjectsTable.projects.removeAll()
                self!.vcProjectsTable.reload()
                
                self!.vcProjectsTable.displayBackgroundMessage("Loading...".localized(),
                    subMessage: "")

                self!.getProjectList()
            }

        }, onCancel: { [weak self] in
            
            if (self != nil) {
                self!.searchBar.text = String()
                self!.setupNavigation(animated: true)
                
                self!.isSearching = false
                
                self!.searchPaging.pageNumber = 1
                self!.vcProjectsTable.projects.removeAll()
                self!.vcProjectsTable.reload()
                
                self!.getReadySearch(.followingStatus_Pagination_NoSearchText)
                self!.getProjectList()
                
                self!.searchPaging.pageNumber += 1
            }
        })
    }
    
    fileprivate func localize()
    {
        if DeviceType.IS_ANY_IPAD == false
        {
            self.tabBarItemSearchProjects.title = "Search Projects".localized()
            self.tabBarItemNewProject.title = "New Project".localized()
            self.tabBarItemFilterProject.title = "Filter".localized()
            
            self.navigationItemProjects.title = "PROJECTS".localized()
            self.segmentedProjects.setTitle("Following".localized(), forSegmentAt: 0)
            self.segmentedProjects.setTitle("All".localized(), forSegmentAt: 1)
        }
    }
    func setupNavigation(animated: Bool)
    {
        if showSplitList() == true
        {
            title = "PROJECTS".localized()
        }
        else
        {
            title = "PROJECTS (Following)".localized()
        }
        
        self.searchBar.isActive = false
        self.isSearching = false
        self.isSearchByText = false
        
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        navigationItem.setRightBarButtonItems([barBtnCompose], animated: animated)
        navigationItem.setHidesBackButton(false, animated: animated)
        
        if let titleView = navigationItem.titleView {
            titleView.alpha = 1
            
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                titleView.alpha = 0
                }, completion: { (animated) -> Void in
                    self.navigationItem.titleView = nil
            })
        }
    }
    
    func showSplitList() -> Bool
    {
        if  LibraryAPI.shared.currentUser?.role == UserRole.globalManager
            || LibraryAPI.shared.currentUser?.role == UserRole.vipUser
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    // MARK: ACTIONS
    func createNewPost()
    {
        let vcNewPost = NewPostViewController()
        
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = .overFullScreen
        
        present(navController, animated: true, completion: nil)
    }
    
    func openFilters()
    {
        var options = [DropdownItem]()
        
        for (idx, filter) in self.filterSites.enumerated()
        {
            let item = DropdownItem(title: filter.title)
            item.isSelected = idx == self.selectedFilter ? true : false
            options.append(item)
        }
        
        let menuFilters = DropUpMenu(items: options)
        menuFilters.delegate = self
        menuFilters.showMenu()
    }
    
    func cleanTable()
    {
        self.vcProjectsTable.projects.removeAll()
        self.vcProjectsTable.reload()
        self.vcProjectsTable.refreshed()
    }
    
    ///These functions reset pagination and reload the project list
    func reloadProjectList()
    {
        self.searchPaging.pageNumber = 1
        
        if self.searchBar.isActive == true
        {
            self.searchBar.text = String()
            self.setupNavigation(animated: true)
        }
        
        self.vcProjectsTable.projects.removeAll()
        self.vcProjectsTable.reload()
        
        self.getReadySearch(.followingStatus_Pagination_NoSearchText)
        self.getProjectList()
        
        self.searchPaging.pageNumber += 1
    }
    
    func segmentedControlHandler(_ segmented: UISegmentedControl)
    {
        self.searchBar.text = String()
        self.setupNavigation(animated: true)
        
        self.searchPaging.pageNumber = 1
        
        if self.searchBar.isActive == true
        {
            self.searchBar.text = String()
            self.setupNavigation(animated: true)
        }
        
        //Reset Filter
        self.selectedFilter = 0
        self.tabBarItemFilterProject.title = "Filter".localized()
        self.tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        
        
        self.vcProjectsTable.projects.removeAll()
        self.vcProjectsTable.reload()

        self.getReadySearch(.followingStatus_Pagination_NoSearchText)
        self.self.getProjectList()
        
        //Avoiding duplicate data by lastRow
        self.searchPaging.pageNumber += 1
    }
    
    // MARK: WEB SERVICES
    func getProjectList(showsLoadMessage: Bool = true)
    {
        //Cancel Previous Requests
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.GetProjectList)
        guard let text = searchBar.text else {
            
            return
        }
        
        self.vcProjectsTable.dismissBackgroundMessage()
        if showsLoadMessage == true
        {
            self.vcProjectsTable.displayBackgroundMessage("Loading...".localized(),
                                                          subMessage: "")
        }
        
        let projectsType  = (segmentedProjects.selectedSegmentIndex == 0) ? FollowStatus.following : FollowStatus.all
    
        LibraryAPI.shared.projectBO.getProjects(parameters: [
            "FollowingStatus" : projectsType.rawValue,
            "SearchText" : text,
            "SearchByText" : self.isSearchByText == true ? "true" : "false",
            "PageSize" : self.searchPaging.pageSize,
            "PageNumber" : self.searchPaging.pageNumber,
            "Pagination" : self.isPagination == true ? "true" : "false",
            "SiteMappingId" : self.filterSites.count > 0 ? self.filterSites[self.selectedFilter].siteId : 0],
            onSuccess:{ (projects, sites, totalRecords) -> () in
                
                //Fill Filters
                if self.filtersExist == false
                {
                    for (index, site) in sites.enumerated()
                    {
                        site.title = index == 0 ? "⚐ \(site.title)" : "⚑ \(site.title)"
                        self.filterSites.append(site)
                    }
                    
                    //Custom option
                    if self.filterSites.count == 0
                    {
                        let noFilter = Site()
                        noFilter.title = "⚐ " + "No Filter".localized()
                        self.filterSites.insert(noFilter, at: 0)
                    }
                    
                    if self.filterSites.count > 1
                    {
                        self.filtersExist = true
                    }
                }
                
                //Total Records
                if self.isPagination == true
                {
                    self.searchPaging.totalItems = totalRecords
                }
                
                var newIndexPaths = [IndexPath]()
                let nextRow = self.vcProjectsTable.projects.count
                
                for newRow in nextRow..<(nextRow + projects.count)
                {
                    newIndexPaths.append(IndexPath(row: newRow, section: 0))
                }
                
                self.vcProjectsTable.projects += projects
                self.vcProjectsTable.tableView.insertRows(at: newIndexPaths, with: .bottom)
                
                if self.vcProjectsTable.projects.count == 0
                {
                    self.vcProjectsTable.tableView.displayBackgroundMessage("No Projects".localized(), subMessage: "")
                }
                
            }) { error in
                guard error.code != -999 else {return}
                MessageManager.shared.showBar(title: "Error".localized(),
                                                                subtitle: "Error.TryAgain".localized(),
                                                                type: .error,
                                                                fromBottom: false)
                
                self.vcProjectsTable.refreshed()
                self.vcProjectsTable.tableView.displayBackgroundMessage("Error".localized(),
                    subMessage: "Pull to refresh".localized())
        }
    }
    
    // MARK: SEARCH TYPES
    func getReadySearch(_ type: SearchGetListType)
    {
        switch type
        {
        case .followingStatus_NoPagination_NoSearchText:
            
            self.isSearchByText = false
            self.isPagination = false
            
        case .followingStatus_NoPagination_SearchText:
            
            self.isSearchByText = true
            self.isPagination = false
            
        case .followingStatus_Pagination_SearchText:
            
            self.isSearchByText = true
            self.isPagination = true
            
        case .followingStatus_Pagination_NoSearchText:
            
            self.isSearchByText = false
            self.isPagination = true
            
        default:
            self.isSearchByText = false
            self.isPagination = false
            break
        }
    }
    
    // MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier! == "ProjectsTableVC"
        {
            self.vcProjectsTable = segue.destination as! ProjectsTableViewController
            vcProjectsTable.onProjectListNeedsUpdate = { [weak self] in

                self?.reloadProjectList()
            }
            
            self.vcProjectsTable.willDisplayLastRow = { [weak self] row in
                
                if let isPagination = self?.isPagination, let page =  self?.searchPaging.pageNumber, let totalPages = self?.searchPaging.totalPages, isPagination == true && page <= totalPages
                {
                    self?.getReadySearch(.followingStatus_Pagination_NoSearchText)
                    self?.getProjectList()
                    self?.searchPaging.pageNumber += 1
                }
            }

        }
    }
}

// MARK: TAB BAR
extension ProjectsViewController: UITabBarDelegate {
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        //Selected item color equal not selected color
        tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        
        switch item.tag
        {
        case TabBarOption.add.rawValue:
            let vcSearchProject = Storyboard.getInstanceOf(ProjectSearchViewController.self)
            let navController = NavyController(rootViewController:vcSearchProject)
            
            present(navController, animated: true, completion: nil)
            
        case TabBarOption.filter.rawValue:
            self.openFilters()
            
        case TabBarOption.search.rawValue:
            
            self.isSearching = true
            self.getReadySearch(.followingStatus_NoPagination_SearchText)
            
            self.vcProjectsTable.tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            self.searchBar.showInNavigationItem(self.navigationItem, animated: true)
            
        default:
            break
        }
    }
}

// MARK: DropDown/Up Menu
extension ProjectsViewController: DropUpMenuDelegate
{
    func dropUpMenu(_ dropUpMenu: DropUpMenu, didSelectRowAtIndexPath indexPath: IndexPath)
    {
        if self.selectedFilter != indexPath.row
        {
            self.selectedFilter = indexPath.row
            
            //Filter Option UI
            if self.selectedFilter == 0
            {
                self.tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
                self.tabBarItemFilterProject.title = "Filter".localized()
            }
            else
            {
                self.tabBar.tintColor = UIColor.blueTag()
                self.tabBarItemFilterProject.title = self.filterSites[self.selectedFilter].title
            }
            
            
            //Searching
            if self.isSearching == true
            {
                self.searchPaging.pageNumber = 1
                self.vcProjectsTable.projects.removeAll()
                self.vcProjectsTable.reload()

                self.getReadySearch(.followingStatus_NoPagination_SearchText)
                self.getProjectList()
                
                self.searchPaging.pageNumber += 1
            }
            else
            {
                self.searchPaging.pageNumber = 1
                self.vcProjectsTable.projects.removeAll()
                self.vcProjectsTable.reload()
                
                self.getReadySearch(.followingStatus_Pagination_NoSearchText)
                self.getProjectList()
                
                self.searchPaging.pageNumber += 1
            }
        }
    }
}

