//
//  ProjectTableCell.swift
//  SHM
//
//  Created by Manuel Salinas on 3/2/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class ProjectTableCell: UITableViewCell
{
    // MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var tvText: UITextView!
    @IBOutlet weak var imgActor1: UIImageView!
    @IBOutlet weak var imgActor2: UIImageView!
    @IBOutlet weak var imgActor3: UIImageView!
    @IBOutlet weak var lblDateBy: UILabel!
    @IBOutlet weak var btnFiles: UIButton!
    @IBOutlet weak var btnMoreActors: UIButton!
    @IBOutlet weak var lblCoordinator: UILabel!
    @IBOutlet weak var lblProjectSite: UILabel!
    @IBOutlet weak var ivSwipeDots: UIImageView!
    
    var onStakeholderTap: ((_ stakeholder: Stakeholder) -> ())?
    var onAllStakeholdersTap : (() -> ())?
    var onFilesTap : (() -> ())?
    var onTextViewTap : (() -> ())?

    // MARK: - LIFE CYCLE
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        /** Images */
        //self.imgActor1.layer.cornerRadius = self.imgActor1.frame.size.width / 2
        self.imgActor1.clipsToBounds = true
        self.imgActor1.setBorderWidth(0.5)
        
        //self.imgActor2.layer.cornerRadius = self.imgActor2.frame.size.width / 2
        self.imgActor2.clipsToBounds = true
        self.imgActor2.setBorderWidth(0.5)
        
        //self.imgActor3.layer.cornerRadius = self.imgActor3.frame.size.width / 2
        self.imgActor3.clipsToBounds = true
        self.imgActor3.setBorderWidth(0.5)
        
        /** Buttons */
        //self.btnMoreActors.layer.cornerRadius = self.btnMoreActors.frame.size.width / 2
        self.btnMoreActors.clipsToBounds = true
        self.btnMoreActors.isUserInteractionEnabled = false
        
        self.tvText.isEditable = false
        self.tvText.isSelectable = true
        
        self.tvText.linkTextAttributes = [ NSFontAttributeName : UIFont.boldSystemFont(ofSize: Constants.FontSize.PostText) ]
        
        self.localize()

    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    func setSiteMappingInfo(_ site:Site?)
    {
        if let siteTitle = site?.title {
            
            self.lblProjectSite.isHidden = false
            self.lblProjectSite.text = "⚑ \(siteTitle)"
        }
    }
    
    //MARK: CELL FOR PROJECT
    func cellForProject(_ cell: ProjectTableCell, project: Project)
    {
        /** Fill Date and Author */
        let dateString = project.modifiedDate?.getStringStyleMedium()
        
        if let date = dateString {
            
            cellLabelsConf(cell, date:date, author: project.coordinator.fullName, title: project.title)
        }
        else
        {
            cellLabelsConf(cell, date:"", author: project.coordinator.fullName, title: project.title)
        }
        
        /** Fill Files */
        self.cellAttachments(cell, files: project.totalAttachments)
        
        /** Coordinator*/
        let coordinator = "Coordinator".localized() + ":" + " " + project.coordinator.fullName
        let attributedString = NSMutableAttributedString(string: coordinator)
        attributedString.setBoldAndColor(project.coordinator.fullName, color: UIColor.blackAsfalto(), size: Constants.FontSize.PostSubtext)
        
        cell.lblCoordinator.attributedText = attributedString
        cell.lblCoordinator.adjustsFontSizeToFitWidth = true
        
        /** Stakeholders */
        self.cellStakeholders(cell, stakeHolders: project.stakeholders)
        
        cell.tvText.textColor = UIColor.blackAsfalto()
        
        self.setSiteMappingInfo(project.site)
    }
    
    //MARK: TITLE & DATES
    func cellLabelsConf(_ cell: ProjectTableCell, date: String, author: String, title: String)
    {
        let dateAndAuthor = ((date.isEmpty) ? "-" : date) + " " + "by".localized() + " " + ((author.isEmpty) ? "-" : author)
        
        let attributedString = NSMutableAttributedString(string: dateAndAuthor)
        attributedString.setBoldAndColor(author, color: UIColor.grayConcreto(), size: Constants.FontSize.PostSubtext)
        
        cell.lblDateBy.attributedText = attributedString
        cell.lblDateBy.adjustsFontSizeToFitWidth = true
        cell.lblDateBy.sizeToFit()
        cell.tvText.text = title
        cell.tvText.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
            
            self.onTextViewTap?()
        }))
    }
    
    //MARK: ATTACHMENTS
    func cellAttachments(_ cell: ProjectTableCell, files: Int)
    {
        let hideFiles = (files > 0) ? false : true
        cell.btnFiles.isHidden = hideFiles
        cell.btnFiles.isUserInteractionEnabled = false //Demo
        
        if hideFiles == false
        {
            cell.btnFiles.setTitle(" \(files) " + (files != 1 ? "Files".localized() : "File".localized()), for: UIControlState())
            cell.btnFiles.removeGestureRecognizersOfType(ActionsTapGestureRecognizer.self)
            cell.btnFiles.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
                
                self.onFilesTap?()
            }))
        }
    }
    
    //MARK: STAKEHOLDERS
    func cellStakeholders(_ cell: ProjectTableCell, stakeHolders: [Stakeholder])
    {
        
        //Actors Hidden or not configuration
        cell.imgActor1.isHidden = stakeHolders.count == 0
        cell.imgActor2.isHidden = stakeHolders.count <= 1
        cell.imgActor3.isHidden = stakeHolders.count != 3
        cell.btnMoreActors.isHidden = stakeHolders.count <= 3
        
        /**Fill Actors */
        //Clean previous images in cell
        cell.imgActor1.image = nil
        cell.imgActor2.image = nil
        cell.imgActor3.image = nil
        
        for (index, theStakeholder) in stakeHolders.enumerated()
        {
            if index == 0
            {
                cell.imgActor1.setImageWith(URL(string: theStakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
                cell.imgActor1.removeGestureRecognizersOfType(ActorTapGestureRecognizer.self)
                cell.imgActor1.addGestureRecognizer(ActorTapGestureRecognizer(actor: theStakeholder, onTap: {
                    (actor) -> Void in
                    self.onStakeholderTap?(actor)
                }))
            }
            else if index == 1
            {
                cell.imgActor2.setImageWith(URL(string: theStakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
                cell.imgActor2.removeGestureRecognizersOfType(ActorTapGestureRecognizer.self)
                cell.imgActor2.addGestureRecognizer(ActorTapGestureRecognizer(actor: theStakeholder, onTap: {
                    (actor) -> Void in
                    self.onStakeholderTap?(actor)
                }))
            }
            else if index == 2
            {
                if stakeHolders.count > 3
                {
                    cell.imgActor3.isHidden = true
                    
                    let totalActors: Int = stakeHolders.count - 2
                    cell.btnMoreActors.setBackgroundImage(UIImage(named: "backBlack"), for: UIControlState())
                    cell.btnMoreActors.setTitle("+\(totalActors)", for: UIControlState())
                    cell.btnMoreActors.isUserInteractionEnabled = true
                }
                else
                {
                    cell.imgActor3.setImageWith(URL(string: theStakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
                    cell.imgActor3.removeGestureRecognizersOfType(ActorTapGestureRecognizer.self)
                    cell.imgActor3.addGestureRecognizer(ActorTapGestureRecognizer(actor: theStakeholder, onTap: {
                        (actor) -> Void in
                        
                        self.onStakeholderTap?(actor)
                    }))
                }
            }
            else
            {
                break
            }
        }
    }
    
    //MARK: SEPARATOR
    func layoutConfForCell(_ cell: ProjectTableCell)
    {
        // Remove seperator inset
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset))
        {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))
        {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.responds(to: #selector(setter: UIView.layoutMargins))
        {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    // MARK: Actions
    
    @IBAction func btnAllStakeholdersAction(_ sender: AnyObject)
    {
        self.onAllStakeholdersTap?()
    }
    
    // MARK: LOCALIZE
    fileprivate func localize()
    {
        self.btnFiles.setTitle("0 File".localized(), for: UIControlState())
        self.lblDateBy.text = "Updated by:".localized()
        self.lblCoordinator.text = "Coordinator:".localized()
    }
}
