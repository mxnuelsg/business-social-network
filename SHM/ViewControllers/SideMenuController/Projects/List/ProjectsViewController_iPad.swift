//
//  ProjectsViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas on 12/31/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ProjectsViewController_iPad: ProjectsViewController
{
    //MARK: PROPERTIES & OUTLETS
//    @IBOutlet weak var searchProjectsBar: UISearchBar!
    @IBOutlet weak var navItemProjects_iPad: UINavigationItem!
    var btnFilter: UIButton!
    @IBOutlet fileprivate weak var searchBar: AutoSearchBar!
    
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        self.loadConfig_iPad()

        self.searchPaging.pageNumber = 1
        self.getReadySearch(.followingStatus_Pagination_NoSearchText)
        self.getProjectList()
        
        //Avoiding duplicate data by lastRow
        self.searchPaging.pageNumber += 1
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "reloadProjectList"), object: nil)
    }
    
    //MARK:LOAD CONFIG
    func loadConfig_iPad()
    {
        //creation of the search controller
        searchBar.initialize(shouldSearchAfterDelay: true,
        onSearchText: { [weak self] (text) in
            if (self != nil) {
                self!.searchPaging.pageNumber = 1

                self!.vcProjectsTable.projects.removeAll()
                self!.vcProjectsTable.reload()

                self!.vcProjectsTable.displayBackgroundMessage("Loading...".localized(),
                                                              subMessage: "")

                self!.getReadySearch(.followingStatus_NoPagination_SearchText)
                self!.getProjectList()
            }
            
        }, onBeginEditing: { [weak self] (searchBar) in
            self?.isSearching = true
            self?.vcProjectsTable.tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            
        }, onCancel: { [weak self] in
            if (self != nil) {
                self!.searchBar.text = String()
                self!.isSearching = false

                self!.searchPaging.pageNumber = 1
                self!.vcProjectsTable.projects.removeAll()
                self!.vcProjectsTable.reload()

                self!.getReadySearch(.followingStatus_Pagination_NoSearchText)
                self!.getProjectList()
                
                self!.searchPaging.pageNumber += 1
            }
        })
        
        self.localize()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadProjectList), name: NSNotification.Name(rawValue: "reloadProjectList"), object: nil)
        
        //SearchBar
//        self.searchProjectsBar.delegate = self
        
        //Items Bar
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(ProjectsViewController.createNewPost))
        let barBtnAdd = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addProject))
       
        self.btnFilter = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 25))
        self.btnFilter.cornerRadius()
        self.btnFilter.setTitle("Filter".localized(), for: UIControlState())
        self.btnFilter.addTarget(self, action: #selector(self.openFilters), for: .touchUpInside)
        let barBtnFilter = UIBarButtonItem(customView: self.btnFilter)

        navigationItem.rightBarButtonItems = [barBtnCompose, barBtnAdd, barBtnFilter]
        
        //Table
        vcProjectsTable.displayBackgroundMessage("Loading...".localized(),
            subMessage: "")
        
        //Segemented Control
        if showSplitList() == true
        {
            self.segmentedProjects.isHidden = false
            self.constraintContainerTop.constant = 44
        }
        
        self.segmentedProjects.selectedSegmentIndex = 0
        self.segmentedProjects.addTarget(self, action: #selector(ProjectsViewController.segmentedControlHandler(_:)), for: UIControlEvents.valueChanged)
        
        //Refresh Action
        vcProjectsTable.enableRefreshAction({
            
            self.searchPaging.pageNumber = 1
            
            if let text = self.searchBar.text, self.isSearching && text.trim().isEmpty == false {
                
                self.getReadySearch(.followingStatus_NoPagination_SearchText)
                self.vcProjectsTable.refreshed()

            }
            else
            {
                //Reset filter
                self.selectedFilter = 0
                self.btnFilter.backgroundColor = UIColor.clear

                self.vcProjectsTable.projects.removeAll()
                self.vcProjectsTable.reload()
                self.vcProjectsTable.refreshed()
                
                self.getReadySearch(.followingStatus_Pagination_NoSearchText)
                self.getProjectList(showsLoadMessage: false)
                
                self.searchPaging.pageNumber += 1
            }
        })
    }
    
    fileprivate func localize()
    {
        self.segmentedProjects.setTitle("Following".localized(), forSegmentAt: 0)        
        self.segmentedProjects.setTitle("All".localized(), forSegmentAt: 1)
        self.searchBar.placeholder = "Search Projects".localized()
        self.searchBar.setValue("Cancel".localized(), forKey:"_cancelButtonText")
        self.navItemProjects_iPad.title = "PROJECTS".localized()
    }
    
    // MARK: WEB SERVICES
    override func getProjectList(showsLoadMessage:Bool = true)
    {
        //Cancel Previous Requests
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.GetProjectList)

        self.vcProjectsTable.dismissBackgroundMessage()
        if showsLoadMessage == true
        {
            self.vcProjectsTable.displayBackgroundMessage("Loading...".localized(),
                                                          subMessage: "")
        }
        
        
        let projectsType  = (segmentedProjects.selectedSegmentIndex == 0) ? FollowStatus.following : FollowStatus.all
        
        LibraryAPI.shared.projectBO.getProjects(parameters: [
            "FollowingStatus" : projectsType.rawValue,
            "SearchText" : self.searchBar.text ?? String(),
            "SearchByText" : self.isSearchByText == true ? "true" : "false",
            "PageSize" : self.searchPaging.pageSize,
            "PageNumber" : self.searchPaging.pageNumber,
            "Pagination" : self.isPagination == true ? "true" : "false",
            "SiteMappingId" : self.filterSites.count > 0 ? self.filterSites[self.selectedFilter].siteId : 0],
                                                        onSuccess:{ (projects, sites, totalRecords) -> () in
                                                            
                                                            //Fill Filters
                                                            if self.filtersExist == false
                                                            {
                                                                for (index, site) in sites.enumerated()
                                                                {
                                                                    site.title = index == 0 ? "⚐ \(site.title)" : "⚑ \(site.title)"
                                                                    self.filterSites.append(site)
                                                                }
                                                                
                                                                //Custom option
                                                                if self.filterSites.count == 0
                                                                {
                                                                    let noFilter = Site()
                                                                    noFilter.title = "⚐ " + "No Filter".localized()
                                                                    self.filterSites.insert(noFilter, at: 0)
                                                                }
                                                                
                                                                if self.filterSites.count > 1
                                                                {
                                                                    self.filtersExist = true
                                                                }
                                                            }
                                                            
                                                            //Total Records
                                                            if self.isPagination == true
                                                            {
                                                                self.searchPaging.totalItems = totalRecords
                                                            }
                                                            
                                                            var newIndexPaths = [IndexPath]()
                                                            let nextRow = self.vcProjectsTable.projects.count
                                                            
                                                            for newRow in nextRow..<(nextRow + projects.count)
                                                            {
                                                                newIndexPaths.append(IndexPath(row: newRow, section: 0))
                                                            }
                                                            
                                                            self.vcProjectsTable.projects += projects
                                                            self.vcProjectsTable.tableView.insertRows(at: newIndexPaths, with: .bottom)
                                                            
        }) { error in
            guard error.code != -999 else {return}
            MessageManager.shared.showBar(title: "Error".localized(),
                                                            subtitle: "Error.TryAgain".localized(),
                                                            type: .error,
                                                            fromBottom: false)
            
            self.vcProjectsTable.refreshed()
            self.vcProjectsTable.tableView.displayBackgroundMessage("Error".localized(),
                                                                    subMessage: "Pull to refresh".localized())
        }
    }
    
    //MARK: ACTIONS
    override func openFilters()
    {
        //Cancel Search (Hide Keyboard)
        if self.isSearching == true
        {
            self.searchBar.resignFirstResponder()
        }
        
        //Filter Options
        var filterStrings = [String]()
        
        for filter in filterSites
        {
            filterStrings.append(filter.title)
        }
        
        let vcFilterMenu = StringTableViewController()
        vcFilterMenu.checkedOption = self.selectedFilter
        vcFilterMenu.options = filterStrings
        vcFilterMenu.modalPresentationStyle = .popover
        vcFilterMenu.popoverPresentationController?.sourceView = self.view
        vcFilterMenu.popoverPresentationController?.sourceRect = self.btnFilter.frame
        vcFilterMenu.onSelectedOption = { [weak self] numberOfRow in
            
            if self?.selectedFilter != numberOfRow
            {
                self?.selectedFilter = numberOfRow
                
                //Filter Option UI
                self?.btnFilter.backgroundColor = self?.selectedFilter == 0 ? UIColor.clear : UIColor.blueBelize()
                
                //Searching
                if self?.isSearching == true
                {
                    self?.searchPaging.pageNumber = 1
                    self?.vcProjectsTable.projects.removeAll()
                    self?.vcProjectsTable.reload()
                    
                    self?.getReadySearch(.followingStatus_NoPagination_SearchText)
                    self?.getProjectList()
                    
                    self?.searchPaging.pageNumber += 1
                }
                else
                {
                    self?.searchPaging.pageNumber = 1
                    self?.vcProjectsTable.projects.removeAll()
                    self?.vcProjectsTable.reload()
                    
                    self?.getReadySearch(.followingStatus_Pagination_NoSearchText)
                    self?.getProjectList()
                    
                    self?.searchPaging.pageNumber += 1
                }
            }
        }
        
        self.present(vcFilterMenu, animated: true, completion: nil)
    }
    
    func addProject()
    {
        if isSearching == true
        {
            self.resetSearch()
        }
        
        let vcSearchProject = Storyboard.getInstanceOf(ProjectSearchViewController.self)
        let navController = NavyController(rootViewController:vcSearchProject)
        navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
        
        self.present(navController, animated: true, completion: nil)
    }
    
    override func createNewPost()
    {
        if isSearching == true
        {
            self.resetSearch()
        }
        
        let vcNewPost = NewPostModal()
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
        
        self.present(navController, animated: true, completion: nil)
    }
    
    override func segmentedControlHandler(_ segmented: UISegmentedControl)
    {
        self.searchPaging.pageNumber = 1
        
        if self.isSearching == true
        {
            self.searchBar.text = String()
            self.searchBar.resignFirstResponder()
            self.searchBar.setShowsCancelButton(false, animated: true)
        }
        
        //Reset Filter
        self.selectedFilter = 0
        self.btnFilter.backgroundColor = UIColor.clear
        
        self.vcProjectsTable.projects.removeAll()
        self.vcProjectsTable.reload()
        
        self.getReadySearch(.followingStatus_Pagination_NoSearchText)
        self.getProjectList()
        
        //Avoiding duplicate data by lastRow
        self.searchPaging.pageNumber += 1
    }
    
    func resetSearch()
    {
        //Close Search
        self.searchBar.text = String()
        self.searchBar.resignFirstResponder()
        self.searchBar.setShowsCancelButton(false, animated: true)
        
        //Load projects depending segment
        searchPaging.pageNumber = 1
        vcProjectsTable.projects.removeAll()
        vcProjectsTable.tableView.reloadData()
        self.getProjectList()
    }
    
    override func reloadProjectList()
    {
        self.searchPaging.pageNumber = 1
        
        if self.isSearching == true
        {
            self.searchBar.resignFirstResponder()
            self.searchBar.setShowsCancelButton(false, animated: true)
        }
        
        self.vcProjectsTable.projects.removeAll()
        self.vcProjectsTable.reload()
        
        self.getReadySearch(.followingStatus_Pagination_NoSearchText)
        self.getProjectList()
        
        self.searchPaging.pageNumber += 1
    }
}

