//
//  ProjectNewTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 25/08/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

enum NewProjectSection: Int
{
    case title = 0
    case progress = 1
    case description = 2
    case site = 3
    case mitigation = 4
    case coordinator = 5
    case members = 6
    case stakeholders = 7
    case geographies = 8
}

class ProjectNewTableViewController: UITableViewController
{
    let titleLimitSize = 100
    
    var project:Project!
    var isEditable = true
    var isNewProject = false
    var didChangeSite = false
    var sites: [Site]? {
        didSet{
            self.sites = Site.sortSites(self.sites)
        }
    }
    var currentSite: Site? {
        didSet {
            self.project.site = self.currentSite
        }
    }
    var didUpdateProjectSite:((_ newSite:Site) -> ())?
    let headerTitles = [
        "Title".localized(),
        "Progress".localized(),
        "Description".localized(),
        "Site".localized(),
        "Associate Mitigation Measure".localized(),
        "Coordinator".localized(),
        "Members".localized(),
        "Stakeholders".localized(),
        "Geographies".localized()]
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "ContactListCell", bundle: nil), forCellReuseIdentifier: "ContactListCell")
        tableView.register(UINib(nibName: "TextViewTableCell", bundle: nil), forCellReuseIdentifier: "TextViewTableCell")
        tableView.register(UINib(nibName: "GeographyTableViewCell", bundle: nil), forCellReuseIdentifier: "GeographyTableViewCell")
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100.0
        
        if self.isNewProject == true
        {
            if self.sites != nil && self.sites!.count > 0
            {
                self.currentSite = self.sites![0]
            }
        }
        else
        {
            self.currentSite = self.project.site
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: SITE MAPPING MENU SELECTION
    func showSiteMenu_iPad()
    {
        var siteTitles = [String]()
        if let sitesArray = self.sites {
            
            for site in sitesArray
            {
                siteTitles.append(site.getTitleByLevel())
            }
        }
        
        let vcFilterMenu = StringTableViewController()
        vcFilterMenu.options = siteTitles
        vcFilterMenu.isDisplayingSites = true
        if let currentSelectedSite = self.currentSite {
            
            vcFilterMenu.checkedOption = self.sites?.index(of: currentSelectedSite) ?? -1
        }
        vcFilterMenu.modalPresentationStyle = .popover
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: NewProjectSection.site.rawValue)) ?? UITableViewCell()
        vcFilterMenu.popoverPresentationController?.sourceView = cell.contentView
        vcFilterMenu.popoverPresentationController?.sourceRect = cell.contentView.frame
        vcFilterMenu.onSelectedOption = { [weak self]  row in
            
            //Did user change site?
            if self?.currentSite?.title != self?.sites?[row].title
            {
                //Update status
                self?.didChangeSite = true
                //Remove coordinator to choose again
                self?.project.coordinator = Member(isDefault:true)
                self?.tableView.reloadRows(at: [IndexPath(row: 0, section: NewProjectSection.coordinator.rawValue)], with: .none)
            }
            
            //Update Site Info at tableview
            let siteSelected = self?.sites?[row] ?? Site()
            self?.currentSite = siteSelected
            self?.tableView.reloadRows(at: [IndexPath(row: 0, section: NewProjectSection.site.rawValue)], with: .none)
            
            //Update Project Site
            self?.project.site = self?.currentSite
            self?.didUpdateProjectSite?(self?.currentSite ?? Site())
        }
        
        self.present(vcFilterMenu, animated: true, completion: nil)
    }
    
    func showSiteMenu()
    {
        var options = [DropdownItem]()
        if let userSites = self.sites {
           
            for site in userSites
            {
                let item = DropdownItem(title: site.title)
                item.isSelected = self.currentSite?.siteId == site.siteId ? true : false
                item.level = site.level
                options.append(item)
            }
        }
        let menuFilters = DropUpMenu(items: options)
        menuFilters.delegate = self
        menuFilters.showMenu()
    }
    //MARK: TOOLBAR
    func loadToolBar() -> UIToolbar
    {
        let toolBarKeyboard = UIToolbar()
        toolBarKeyboard.barStyle = UIBarStyle.blackOpaque;
        toolBarKeyboard.barTintColor = UIColor.colorForNavigationController()
        
        let barBtnDone = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action:#selector(self.closeKeyboard))
        barBtnDone.tintColor = UIColor.white
        
        toolBarKeyboard.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            barBtnDone]
        
        toolBarKeyboard.sizeToFit()
        
        return toolBarKeyboard
    }
    
    func closeKeyboard()
    {
        view.endEditing(true)
    }

    // MARK: - Table view DataSource and Delegate
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        /*
        let canEditGeographies = LibraryAPI.shared.currentUser?.role == .globalManager
                                 || project.coordinator.id == LibraryAPI.shared.currentUser?.id
        return canEditGeographies ? headerTitles.count : headerTitles.count - 1
         */
        return headerTitles.count
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerTitle = SHMSectionTitleLabel()
        headerTitle.backgroundColor = UIColor.white
        headerTitle.font = UIFont.boldSystemFont(ofSize: 16)
        headerTitle.textColor = UIColor.colorDetailTitleLabel()
        headerTitle.text = headerTitles[section]
        return headerTitle
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == NewProjectSection.mitigation.rawValue
        {
            if LibraryAPI.shared.currentUser?.role == .localManager || LibraryAPI.shared.currentUser?.role == .globalManager
            {
                return 30
            }
            else
            {
                return CGFloat.leastNormalMagnitude
            }
        }
        else
        {
            return 30
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == NewProjectSection.stakeholders.rawValue
        {
            return indexPath.row == 0 ? 50 : 90
        }
            
        if indexPath.section == NewProjectSection.geographies.rawValue {
            return 65
        }
            
        if indexPath.section == NewProjectSection.progress.rawValue || indexPath.section == NewProjectSection.coordinator.rawValue || indexPath.section == NewProjectSection.members.rawValue || indexPath.section == NewProjectSection.site.rawValue
        {
            return 44.0
        }
        else if indexPath.section == NewProjectSection.mitigation.rawValue
        {
            return LibraryAPI.shared.currentUser?.role == .localManager || LibraryAPI.shared.currentUser?.role == .globalManager ? 44.0 : 0
        }
        else
        {
            return tableView.estimatedRowHeight
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 6.0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // #warning Incomplete implementation, return the number of rows
        if section == NewProjectSection.members.rawValue
        {
            return project.members.count + 1
        }
        
        if section == NewProjectSection.stakeholders.rawValue
        {
            return project.stakeholders.count + 1
        }
        
        if section == NewProjectSection.geographies.rawValue {
            return project.geographies.count + 1
        }
        
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == NewProjectSection.title.rawValue
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextViewTableCell") as! TextViewTableCell
            //cell.tvText.delegate = self
            cell.tvText.text = project.title
            cell.tvText.isEditable = isEditable
            cell.tvText.inputAccessoryView = loadToolBar()
            cell.tvText.backgroundColor = (isEditable) ? UIColor.clear : UIColor.noEditableColor()
            cell.onTextChange = { [weak self] text in
                
                self?.project.title = text
            }

            return cell
        }
        
        if indexPath.section == NewProjectSection.progress.rawValue
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sliderCell") as! SliderTableViewCell
            cell.lblSliderValue.text = "\(project.progress)%"
            cell.slider.value = Float(project.progress)
            cell.slider.isEnabled = isEditable
            cell.updateProgress = { [weak self] progress in
                
                self?.project.progress = Int(progress)
                self?.view.endEditing(true)
            }
            
            return cell
        }
        
        if indexPath.section == NewProjectSection.description.rawValue
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextViewTableCell") as! TextViewTableCell
            cell.tvText.text = project.resume
            cell.tvText.isEditable = isEditable
            cell.tvText.inputAccessoryView = loadToolBar()
            cell.tvText.backgroundColor = (isEditable) ? UIColor.clear : UIColor.noEditableColor()
            cell.onTextChange = { [weak self] text in
                
                self?.project.resume = text
            }
            
            return cell
        }
        
        if indexPath.section == NewProjectSection.stakeholders.rawValue
        {
            if indexPath.row == 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "commonCell", for: indexPath)
                cell.textLabel?.text = "Add SH".localized()
                cell.textLabel?.textColor = UIColor.darkText
                cell.textLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 13)
                cell.imageView?.image = UIImage(named: "Add")
                cell.isUserInteractionEnabled = isEditable
                cell.backgroundColor = (isEditable) ? UIColor.white : UIColor.noEditableColor()
                
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListCell") as! ContactListCell
                let stakeholder = project.stakeholders[indexPath.row - 1]
                
                cell.cellForStakeholder(stakeholder)
                cell.isUserInteractionEnabled = isEditable
                cell.backgroundColor = (isEditable) ? UIColor.white : UIColor.noEditableColor()
                cell.accessoryView = UIImageView(image: UIImage(named: "iconSwipeDots"))
                
                return cell
            }
            
        }
        
        if indexPath.section == NewProjectSection.site.rawValue
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "commonCell", for: indexPath)
            cell.imageView?.image = nil
            cell.accessoryType = .none
            cell.textLabel?.text = self.currentSite?.title ?? "No site info".localized()
            
            return cell
        }
        
        if indexPath.section == NewProjectSection.mitigation.rawValue
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "commonCell", for: indexPath)
            
            if let mitigation = self.project.mitigationMeasure, mitigation.isNull == false {
                
                cell.imageView?.image = nil
                cell.textLabel?.text =  mitigation.title
                cell.accessoryType = .none
            }
            else
            {
                cell.imageView?.image = UIImage(named: "Search")
                cell.textLabel?.text =  "Search".localized()
                cell.accessoryType = .disclosureIndicator
            }
            
            return cell
        }
        
        if indexPath.section == NewProjectSection.geographies.rawValue {
            if indexPath.row == 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "commonCell", for: indexPath)
                cell.textLabel?.text = "Add Geography".localized()
                cell.textLabel?.textColor = UIColor.darkText
                cell.textLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 13)
                cell.imageView?.image = UIImage(named: "Add")
                cell.isUserInteractionEnabled = isEditable
                cell.backgroundColor = (isEditable) ? UIColor.white : UIColor.noEditableColor()
                
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "GeographyTableViewCell") as! GeographyTableViewCell
                let geography = project.geographies[indexPath.row - 1]
                
                cell.lblTitle.text = geography.title
                cell.lblSubtitle.text = geography.parentTitle
                
                return cell
            }
        }
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "commonCell", for: indexPath)
        
        if indexPath.section == NewProjectSection.coordinator.rawValue
        {
            if project.coordinator.fullName.isEmpty
            {
                cell.textLabel?.text = "Add Coordinator".localized()
                cell.textLabel?.textColor = UIColor.darkText
                cell.textLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 13)
                cell.imageView?.image = UIImage(named: "Add")
                cell.isUserInteractionEnabled = isEditable
                cell.backgroundColor = (isEditable) ? UIColor.white : UIColor.noEditableColor()
            }
            else
            {
                cell.textLabel?.attributedText = project.coordinator.getAttributedName()
                //cell.textLabel?.textColor = UIColor.black
                cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
                cell.imageView?.image = UIImage(named: "Edit")
                cell.isUserInteractionEnabled = isEditable
                cell.backgroundColor = (isEditable) ? UIColor.white : UIColor.noEditableColor()
            }
        }
        else
        {
            if indexPath.row == 0
            {
                cell.textLabel?.text = "Add Member".localized()
                cell.textLabel?.textColor = UIColor.darkText
                cell.textLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 13)
                cell.imageView?.image = UIImage(named: "Add")
                cell.isUserInteractionEnabled = true
                cell.backgroundColor = UIColor.white
            }
            else
            {
                let member = project.members[indexPath.row - 1]
                cell.textLabel?.attributedText = member.getAttributedName()
                cell.accessoryView = UIImageView(image: UIImage(named: "iconSwipeDots"))
                cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
                cell.imageView?.image = nil
                cell.isUserInteractionEnabled = true
                cell.backgroundColor = UIColor.white
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        self.view.endEditing(true)

        let peoplePicker = Storyboard.getInstanceOf(ContactSearchViewController.self)
        peoplePicker.isModalView = false
        
        if indexPath.section == NewProjectSection.coordinator.rawValue && indexPath.row == 0
        {
            peoplePicker.isSearchingSH = false
            if self.isNewProject == true
            {
                peoplePicker.siteIdSearchParameter = self.currentSite?.siteId
            }
            else
            {
                //If user haven't change site, keep looking by projectId
                if self.didChangeSite == false
                {
                    peoplePicker.projectIdSearchParameter = self.project.id
                }
                else
                {
                    //If user changes site, then searchMember will be adequate to the new siteId
                    peoplePicker.siteIdSearchParameter = self.currentSite?.siteId
                }
            }
            peoplePicker.addContactToInstance = { [weak self] contact in
                
                self?.project.coordinator = contact as! Member
                self?.tableView.reloadData()
            }
            
            navigationController?.pushViewController(peoplePicker, animated: true)
        }
        else if indexPath.section == NewProjectSection.members.rawValue && indexPath.row == 0
        {
            //Not looking for stakeholders only
            peoplePicker.isSearchingSH = false
            //SearchMember services needs ProjectId = stakeholderId = siteId = 0 to get (stakeholders + accounts)            
            peoplePicker.addContactToInstance = { [weak self] contact in
                
                //Look for repeated contacts
                let verificationArray = self?.project.members.filter({$0.username == (contact as! Member).username})
                
                if verificationArray != nil && verificationArray!.count > 0
                {
                    //Show alert indicating the duplicity
                    MessageManager.shared.showBar(title: "Warning".localized(),
                                                  subtitle: "This Member is already involved with the project".localized(),
                                                  type: .warning,
                                                  fromBottom: false)
                }
                else
                {
                    self?.project.members.insert((contact as! Member), at: 0)
                    let indexPathInsert = IndexPath(row: 1, section: NewProjectSection.members.rawValue)
                    tableView.insertRows(at: [indexPathInsert], with: .none)
                }
            }
            
            navigationController?.pushViewController(peoplePicker, animated: true)
        }
        else if indexPath.section == NewProjectSection.stakeholders.rawValue && indexPath.row == 0
        {
            ///Add Stakeholders
            peoplePicker.isSearchingSH = true
            peoplePicker.addContactToInstance = { [weak self] contact in
                
                //Look for repeated contacts
                let verificationArray = self?.project.stakeholders.filter({$0.id == (contact as! Stakeholder).id})
                if verificationArray != nil && verificationArray!.count > 0
                {
                    //Show alert indicating the duplicity
                    MessageManager.shared.showBar(title: "Warning".localized(),
                                                  subtitle: "This Stakeholder is already involved with the project".localized(),
                                                  type: .warning,
                                                  fromBottom: false)

                }
                else
                {
                    self?.project.stakeholders.insert((contact as! Stakeholder), at: 0)
                    let indexPathInsert = IndexPath(row: 1, section: NewProjectSection.stakeholders.rawValue)
                    tableView.insertRows(at: [indexPathInsert], with: .none)
                }
            }
            
            navigationController?.pushViewController(peoplePicker, animated: true)
        }
        else if indexPath.section == NewProjectSection.site.rawValue
        {
            if DeviceType.IS_ANY_IPAD == true
            {
                self.showSiteMenu_iPad()
            }
            else
            {
                self.showSiteMenu()
            }
        }
        else if indexPath.section == NewProjectSection.mitigation.rawValue
        {
            let vcSearcMitigation: MitigationSearchViewController = Storyboard.getInstanceFromStoryboard("Integration")
            vcSearcMitigation.currentSite = self.currentSite?.siteId ?? 0
            vcSearcMitigation.onMitigationSelected = { [weak self] selection  in
            
                self?.project.mitigationMeasure = selection
                
                let indexPath = IndexPath(row: 0, section: NewProjectSection.mitigation.rawValue)
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
            
            self.navigationController?.pushViewController(vcSearcMitigation, animated: true)
            
        } else if indexPath.section == NewProjectSection.geographies.rawValue && indexPath.row == 0 {
            //Add Geography
            let geographyPicker: GeoListViewController = Storyboard.getInstanceFromStoryboard("Geography")
            geographyPicker.title = "Geographies".localized()
            geographyPicker.onTableAssigned = { (table) in
                table.ownerModule = .projectAndStakeholderEdition
                table.selectedGeographies = self.project.geographies
                table.refreshTable()
                table.onDidSelectGeography = { [weak self] (geography) in
                    self?.navigationController?.popViewController(animated: true)
                    self?.project.geographies.append(geography)
                    self?.tableView.reloadSections(IndexSet(integer: indexPath.section), with: .none)
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                        UIView.animate(withDuration: 0.3, animations: { 
                            tableView.contentOffset.y = tableView.contentSize.height - tableView.frame.height
                        })
                    })
                }
            }
            navigationController?.pushViewController(geographyPicker, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        if (indexPath.section == NewProjectSection.members.rawValue
            || indexPath.section == NewProjectSection.stakeholders.rawValue
            || indexPath.section == NewProjectSection.geographies.rawValue)
            && indexPath.row != 0
        {
            return true
        }
        else if indexPath.section == NewProjectSection.mitigation.rawValue
            && (self.project.mitigationMeasure != nil && self.project.mitigationMeasure?.isNull == false)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        if (indexPath.section == NewProjectSection.members.rawValue
            || indexPath.section == NewProjectSection.stakeholders.rawValue
            || indexPath.section == NewProjectSection.geographies.rawValue)
            && indexPath.row != 0
        {
            let delete = UITableViewRowAction(style: .destructive, title: "Remove".localized(), handler: {(action, indexPath) -> () in
                
                if indexPath.section == NewProjectSection.members.rawValue
                {
                    self.project.members.remove(at: indexPath.row - 1)
                    tableView.deleteRows(at: [indexPath], with: .fade)
                }
                else if indexPath.section == NewProjectSection.stakeholders.rawValue
                {
                    self.project.stakeholders.remove(at: indexPath.row - 1)
                    tableView.deleteRows(at: [indexPath], with: .fade)
                }
                else if indexPath.section == NewProjectSection.geographies.rawValue
                {
                    self.project.geographies.remove(at: indexPath.row - 1)
                    tableView.deleteRows(at: [indexPath], with: .fade)
                }
            })
            return [delete]
        }
        else if indexPath.section == NewProjectSection.mitigation.rawValue
        {
            let delete = UITableViewRowAction(style: .destructive, title: "Remove".localized(), handler: {(action, indexPath) -> () in
                
                tableView.setEditing(false, animated: true)
                
                self.project.mitigationMeasure = nil
                
                let indexPath = IndexPath(row: 0, section: NewProjectSection.mitigation.rawValue)
                tableView.reloadRows(at: [indexPath], with: .fade)
            })
            return [delete]
        }
        else
        {
            return nil
        }
    }
}


//MARK: TEXTVIEW DELEGATE
extension ProjectNewTableViewController: UITextViewDelegate
{
    func textViewDidEndEditing(_ textView: UITextView)
    {
        project.title = textView.text
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if textView.text.characters.count + (text.characters.count - range.length) <= titleLimitSize
        {
            return true
        }
        
        textView.shakeAnimation()
        
        return false
    }
}


//MARK: DROP MENU
extension ProjectNewTableViewController: DropUpMenuDelegate
{
    func dropUpMenu(_ dropUpMenu: DropUpMenu, didSelectRowAtIndexPath indexPath: IndexPath)
    {
        //Did user change site?
        if self.currentSite?.title != self.sites?[indexPath.row].title
        {
            //Update status
            self.didChangeSite = true
           
            //Remove coordinator or mitigation measure to force added again
            self.project.mitigationMeasure = nil
            self.project.coordinator = Member(isDefault:true)
            
            self.tableView.reloadRows(at: [IndexPath(row: 0, section: NewProjectSection.mitigation.rawValue)], with: .none)
            self.tableView.reloadRows(at: [IndexPath(row: 0, section: NewProjectSection.coordinator.rawValue)], with: .none)
        }
        
        //Update tableview Site info
        self.currentSite = self.sites?[indexPath.row]
        self.tableView.reloadRows(at: [IndexPath(row: 0, section: NewProjectSection.site.rawValue)], with: .none)

        //Update Project Site
        self.project.site = self.currentSite
        self.didUpdateProjectSite?(self.currentSite ?? Site())
    }
}
