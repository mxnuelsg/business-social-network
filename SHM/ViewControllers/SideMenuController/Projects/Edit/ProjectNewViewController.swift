//
//  ProjectNewViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 25/08/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProjectNewViewController: UIViewController
{
    @IBOutlet weak var tabBarOptions: UITabBar!
    @IBOutlet weak var ContainerbottomLayout: NSLayoutConstraint!
    
    var barBtnSave: UIBarButtonItem!
    var vcNewEditProject:ProjectNewTableViewController!
    @IBOutlet weak var tabBarItemSave: UITabBarItem!
    var onProjectCreated : ((_ new: Project) -> ())?
    var onProjectEdited : (() -> ())?
    var onProjectModalClosed:(() -> ())?
    var isNewProject = false
    var searchTerm: String?
    var sites:[Site]?
    var currentSite:Site?
    
    @NSCopying var project:Project!
    
    // MARK: - LYFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    // MARK: - LOAD CONFIG
    func loadConfig()
    {
        self.localize()
        
        if project == nil
        {
            project = Project(isDefault: true)
            
            if let searchText = searchTerm as String!
            {
                project.title = searchText
            }
        }
        
        //Title
        title = (project.id == -1) ? "NEW PROJECT".localized() : "PROJECT EDIT".localized()
        
        //UI
        if project.id == -1
        {
            tabBarOptions.isHidden = true
            
            //Bar Button Items
            let barBtnClose = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.close))
            barBtnSave = UIBarButtonItem(title: "Save".localized(), style: .plain, target: self, action:#selector(self.addNew))
            
            navigationItem.leftBarButtonItem = barBtnClose
            navigationItem.rightBarButtonItem = barBtnSave
            
            //Constraint
            ContainerbottomLayout.constant = 0
        }
        
        //Update site
        if self.isNewProject == true
        {
            vcNewEditProject.currentSite = sites != nil && sites!.count > 0 ? self.sites![0] : Site()
        }
        else
        {
            vcNewEditProject.currentSite = self.currentSite
        }        
        self.vcNewEditProject.tableView.reloadData()
    }
    
    fileprivate func localize()
    {
        if DeviceType.IS_ANY_IPAD == false
        {
            self.tabBarItemSave.title = "Save".localized()
        }
    }
    
    // MARK: - ACTIONS
    func close()
    {
        self.view.endEditing(true)
        self.onProjectModalClosed?()
        self.dismiss(animated: true, completion: nil)
    }
    
    func addNew()
    {
        view.endEditing(true)
        
        guard project.title.isEmpty == false else
        {
            MessageManager.shared.showBar(title: "Warning".localized(), subtitle: "What is the project's title?".localized(), type: .warning, fromBottom: false)
            return
        }
        
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        barBtnSave.isEnabled = false
        
        LibraryAPI.shared.projectBO.projectPost(createRequest(), onSuccess: {
            (project) -> () in
            
            MessageManager.shared.hideHUD()
            self.barBtnSave.isEnabled = true
            self.project = project
            
            self.dismiss(animated: true, completion: { () -> Void in
                
                //Reload Project List
                NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadProjectList"), object: nil)
                
                //Show Message
                MessageManager.shared.showBar(title: "Success".localized(),
                                              subtitle: "The project's been created".localized(),
                                              type: .success,
                                              fromBottom: false)
                
                if let newProject = self.onProjectCreated
                {
                    newProject(self.project)
                }
            })
        }) { error in
            
            MessageManager.shared.hideHUD()
            self.barBtnSave.isEnabled = true
            
            MessageManager.shared.showBar(title: "Error",
                                          subtitle: "There was a problem. The new project hasn't been created".localized(),
                                          type: .error,
                                          fromBottom: false)
        }
    }
    
    // MARK: REQUEST
    func createRequest() -> [String : Any]
    {
        var  wsRequest  = project.getWSObject()
        wsRequest["Status"] = false
        wsRequest["CoordinatorId"] = project.coordinator.id
        wsRequest["CreatedById"] = LibraryAPI.shared.currentUser!.id 
        wsRequest["ModifiedById"] = LibraryAPI.shared.currentUser!.id
        wsRequest["CreatedDate"] = (project.id == -1) ? Date().convertToRequestString() : project.createdDateString
        wsRequest["ModifiedDate"] = Date().convertToRequestString()
        wsRequest["Modified"]  = project.coordinator.getWSObject()
        wsRequest["MitigationMeasure"] = project.mitigationMeasure?.getWSObject()  ?? "null"
        //        wsRequest["Coordinator"]  = (project.coordinator.id == 0)  ? [:] : project.coordinator.getWSObject()
        
        return wsRequest
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "projectNewTVC"
        {
            self.vcNewEditProject = segue.destination as! ProjectNewTableViewController
            
            if project == nil
            {
                project = Project(isDefault: true)
                
                if let searchText = searchTerm as String!
                {
                    project.title = searchText
                }
            }
            vcNewEditProject.isNewProject = self.isNewProject
            vcNewEditProject.project = project
            vcNewEditProject.sites = self.sites
            if self.isNewProject == true
            {
                vcNewEditProject.currentSite = sites != nil && sites!.count > 0 ? self.sites![0] : Site()
            }
            else
            {
                vcNewEditProject.currentSite = self.currentSite
            }
            vcNewEditProject.didUpdateProjectSite = { [weak self] newSite in
                
                self?.project.site = newSite
            }
            //Validate if it's a VIP user
            /*
             if project.coordinator.id != LibraryAPI.shared.currentUser!.id
             || LibraryAPI.shared.currentUser!.role != UserRole.VIPUser
             {
             vcNewEditProject.isEditable = false
             }
             */
        }
    }
    
    func tabBar(_ tabBar: UITabBar, didSelectItem item: UITabBarItem)
    {
        //Selected item color equal not selected color
        tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        
        switch item.tag
        {
        case 1:
            view.endEditing(true)
            
            guard project.title.isEmpty == false else
            {
                MessageManager.shared.showBar(title: "Warning".localized(), subtitle: "What is the project's title?".localized(), type: .warning, fromBottom: false)
                return
            }
            
            guard project.coordinator.id > 0 else {
                MessageManager.shared.showBar(subtitle: "Coordinator is required".localized(), type: .warning, fromBottom: false)
                return
            }
            
            //Indicator
            MessageManager.shared.showLoadingHUD()
            
            LibraryAPI.shared.projectBO.editProject(createRequest(), onSuccess: {
                (project) -> () in
                
                MessageManager.shared.hideHUD()
                
                //Show Message
                MessageManager.shared.showBar(title: "Success".localized(), subtitle: "The project's been edited".localized(), type: .success, fromBottom: false)
                
                if let projectEdited = self.onProjectEdited {
                    projectEdited()
                }
                
                _ = self.navigationController?.popViewController(animated: true)
                
            }) { error in
                
                MessageManager.shared.hideHUD()
                MessageManager.shared.showBar(title: "Error", subtitle: "There was a problem. The project hasn't been edited".localized(), type: .error, fromBottom: false)
            }
        default:
            break
        }
    }
}
