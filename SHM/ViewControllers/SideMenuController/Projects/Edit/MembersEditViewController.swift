//
//  MembersEditViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 03/09/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class MembersEditViewController: UIViewController
{
    var members = [Member]()
    
    @IBOutlet weak var tabBarItemCancel: UITabBarItem!
    @IBOutlet weak var tabBarItemSave: UITabBarItem!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.localize()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        backButtonArrow()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    fileprivate func localize()
    {
        self.tabBarItemSave.title = "Save".localized()
        self.tabBarItemCancel.title = "Cancel".localized()
    }
    func tabBar(_ tabBar: UITabBar, didSelectItem item: UITabBarItem)
    {
        //Selected item color equal not selected color
        tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        
        switch item.tag
        {
        case 1:
            print("Save Project")
        case 2:
            navigationController?.dismiss(animated: false, completion: nil)
        default:
            break
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "projectNewTVC" {
            let membersEditTVC = segue.destination as! MembersEditTableViewController
            membersEditTVC.members = members
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}
