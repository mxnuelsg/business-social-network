//
//  MembersEditTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 02/09/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class MembersEditTableViewController: UITableViewController
{
    var members = [Member]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        backButtonArrow()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return members.count + 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        if indexPath.row == (members.count + 1) {
            cell.textLabel?.text = "Member"
            cell.imageView?.image = UIImage(named: "addIcon")
        } else {
            let member = members[indexPath.row]
            cell.textLabel?.text = member.name
        }
        // Configure the cell...

        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.row == members.count + 1 ? true : false
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            tableView.deleteRows(at: [indexPath], with: .fade)
            members.remove(at: indexPath.row)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
    }
}
