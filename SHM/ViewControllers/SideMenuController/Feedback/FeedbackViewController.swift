//
//  FeedbackViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 5/5/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController
{
    //MARK: OUTLETS & PROPERTIES
    @IBOutlet fileprivate weak var tvFeedback: UITextView!
    fileprivate var keyboardShowing = false
    
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)

        self.tvFeedback.becomeFirstResponder()
    }
    
    deinit
    {
        print("Bye Feedback")
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        //Title
        self.localize()
        
        //Notifications
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //Navugation Items
        let barBtnCancel = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.close))
        let barBtnSend = UIBarButtonItem(title:"Send".localized(), style: .done, target: self,  action: #selector(self.sendFeedback))
        
        //Adding to NavBar
        navigationItem.leftBarButtonItem = barBtnCancel
        navigationItem.rightBarButtonItem = barBtnSend

    }
    
    fileprivate func localize()
    {
        self.title = "Feedback".localized()
    }
    //KEYBOARD NOTIFICATIONS
    func keyboardWillShow(_ notification: Notification)
    {
        //Keyboard height
        let info = notification.userInfo!
        var keyboardFrame = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.tvFeedback.convert(keyboardFrame, from:nil)
        
        self.tvFeedback.contentInset.bottom = keyboardFrame.size.height
        self.tvFeedback.scrollIndicatorInsets.bottom = keyboardFrame.size.height
        
        self.keyboardShowing = true
    }
    
    func keyboardWillHide(_ notification: Notification)
    {
        self.tvFeedback.contentInset = UIEdgeInsets.zero
        self.tvFeedback.scrollIndicatorInsets = UIEdgeInsets.zero
        
        self.keyboardShowing = false
    }
    
    //MARK: ACTIONS
    func close()
    {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func sendFeedback()
    {
        self.view.endEditing(true)
        
        
        guard self.tvFeedback.text.trim().isEmpty == false  else {
        
            MessageManager.shared.showBar(title: "Warning".localized(),
                                          subtitle:"Write a comment for feedback".localized(),
                                          type: .warning,
                                          fromBottom: false)

            return
        }
        
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        //Request Object
        var wsObject: [String : Any]  = [:]
        wsObject["Comments"] = self.tvFeedback.text
        
        LibraryAPI.shared.feedbackBO.postFeedback(wsObject, onSuccess: {
            
            MessageManager.shared.hideHUD()
            
            self.dismiss(animated: true, completion: {
                
                MessageManager.shared.showBar(title: "Success".localized(),
                                              subtitle: "Feedback has been sent".localized(),
                                              type: .success,
                                              fromBottom: false)
            })
            
            
        }) { (error) in
            
            MessageManager.shared.hideHUD()
            
            MessageManager.shared.showBar(title: "Error",
                                          subtitle: "Error.TryAgain".localized(),
                                          type: .error,
                                          fromBottom: false)
            
        }
    }
}
