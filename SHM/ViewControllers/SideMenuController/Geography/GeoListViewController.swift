//
//  GeoListViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 7/18/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class GeoListViewController: UIViewController
{
    //MARK: OUTLETS 7 PROPERTY
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var segmentedState: UISegmentedControl!
    
    @IBOutlet weak var constraintTopContainer: NSLayoutConstraint!
    var vcGeoTable: GeoTableViewController!
    var willBeShowedInNewPost = false
    var onTableAssigned: ((GeoTableViewController) -> ())?
    var onDidUpdateGeography:(()->())?
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    deinit
    {
        print("Deinit: GeoListViewController")
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        //Search
        self.searchBar.placeholder = "Search".localized()
        
        //Segmented control
        self.segmentedState.addTarget(self, action: #selector(GeoListViewController.segmentedControlValueChanged(_:)), for:.valueChanged)
        self.segmentedState.setTitle("Following".localized(), forSegmentAt: 0)
        self.segmentedState.setTitle("All".localized(), forSegmentAt: 1)
        
        //Modify for show in new post
        if self.willBeShowedInNewPost == true
        {
            self.searchBar.isHidden = true
            self.segmentedState.isHidden = true
            self.view.backgroundColor = .clear
            self.constraintTopContainer.constant = -14
            self.vcGeoTable.ownerModule = .newPost
        }
    }
    
    //MARK: WEB SERVICES
    fileprivate func refreshList(listMode: GeoListMode)
    {
        //Update UI
        self.view.endEditing(true)
        if self.vcGeoTable.geoListMode == .following || self.vcGeoTable.geoQuery.pageNumber == 1
        {
            self.vcGeoTable.showLoadingMode()
        }
        
        //Cancel Previous request
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.getGeographiesLight)
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.getFollowedGeographiesLight)
        
        //Request Geographies
        if listMode == .all
        {
            LibraryAPI.shared.geographyBO.getGeographiesLight(self.vcGeoTable.geoQuery.getWSObject(), onSuccess: { (geographies, totalItems) in
                
                //HANDLE THE PAGINATION
                self.vcGeoTable.geoQuery.totalItems = totalItems
                if self.vcGeoTable.geoQuery.pagination == true
                {
                    let geoItems = self.vcGeoTable.clusterItems + geographies
                    self.vcGeoTable.clusterItems = geoItems
                }
                
                
            }, onError: { (error) in
                
                self.vcGeoTable.displayBackgroundMessage("No data".localized(), subMessage: String())
            })
        }
        else if listMode == .following
        {
            LibraryAPI.shared.geographyBO.getFollowedGeographiesLight(GeographyQuery().getWSObject(), onSuccess: { (geographies, totalItems) in
                
                self.vcGeoTable.clusterItems = geographies
                self.vcGeoTable.backupClusterItems = geographies
                
            }, onError: { (error) in
                
                self.vcGeoTable.displayBackgroundMessage("No data".localized(), subMessage: String())
            })
        }
    }
    
    //MARK: ACTIONS
    func hideKeyboard()
    {
        self.searchBar.resignFirstResponder()
        self.view.gestureRecognizers?.removeAll()
    }
    
    //MARK: ACTION BUTTONS
    func segmentedControlValueChanged(_ segment: UISegmentedControl)
    {
        //Restore searchbar
        self.searchBar.text = String()
        self.searchBar.showsCancelButton = false
        self.searchBar.resignFirstResponder()
        
        //Segment has changed, initialize the geoQuery to search from beginning
        self.vcGeoTable.geoQuery.getAllWithPagination()
        self.vcGeoTable.geoQuery.isOrderedByTitle = true
        //Update list mode
        self.vcGeoTable.geoListMode = GeoListMode(rawValue: segment.selectedSegmentIndex) ?? .following
        
        //Refresh list for current List Mode
        //self.refreshList(listMode: self.vcGeoTable.geoListMode)
        self.vcGeoTable.refreshTable()
    }


    //MARK: SEARCH FILTERS
    fileprivate func localSearchFilter(_ searchText: String)
    {
        let term = searchText.trim()
        guard term.isEmpty == false else {
            
            self.vcGeoTable.clusterItems = self.vcGeoTable.backupClusterItems
            return
        }
        let filteredItems = self.vcGeoTable.backupClusterItems.filter({$0.title.localizedCaseInsensitiveContains(term)})
        self.vcGeoTable.clusterItems = filteredItems        
    }
    
    fileprivate func remoteSearchFilter(_ searchText: String)
    {
        let term = searchText.trim()
        guard term.isEmpty == false else {
            
            self.vcGeoTable.geoQuery.getAllWithPagination()
            self.vcGeoTable.refreshTable()
            return
        }
        
        self.vcGeoTable.geoQuery.setSearchText(term)
        self.vcGeoTable.refreshTable()
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let geoTable = segue.destination as? GeoTableViewController {
            
            self.vcGeoTable = geoTable
            self.onTableAssigned?(geoTable)
            self.vcGeoTable.onDidUpdateGeography = {[weak self] void in
                
             self?.onDidUpdateGeography?()
            }
        }
    }
}


//MARK: SEARCH BAR DELEGATE
extension GeoListViewController: UISearchBarDelegate
{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        //Show Cancel
        searchBar.setShowsCancelButton(true, animated: true)
        searchBar.tintColor = .white
        
        //Add gesture to remove keyboard
        let tapGR = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        self.view.addGestureRecognizer(tapGR)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        //Filter by segment
        if self.vcGeoTable.geoListMode == .following
        {
            self.localSearchFilter(searchText)
        }
        else if self.vcGeoTable.geoListMode == .all
        {
            guard searchText.trim().isEmpty == false else {
                
                self.remoteSearchFilter(String())
                return
            }
            guard searchText.trim().characters.count >= 3 else {
                
                return
            }
            let delay = DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delay) {
            
                self.remoteSearchFilter(searchText)
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        //Hide Cancel
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        
        guard let term = searchBar.text , term.trim().isEmpty == false else {
            
            MessageManager.shared.showBar(title: nil,
                                          subtitle: "White spaces are not permitted".localized(),
                                          type: .warning,
                                          containsIcon: true,
                                          fromBottom: false)
            return
        }
        //Filter by segment
        if self.vcGeoTable.geoListMode == .following
        {
            self.localSearchFilter(term)
        }
        else if self.vcGeoTable.geoListMode == .all
        {
            self.remoteSearchFilter(term)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        //Hide Cancel
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.text = String()
        searchBar.resignFirstResponder()
        //Filter by segment
        if self.vcGeoTable.geoListMode == .following
        {
            self.localSearchFilter(String())
        }
        else if self.vcGeoTable.geoListMode == .all
        {
            self.remoteSearchFilter(String())
        }
    }
}
