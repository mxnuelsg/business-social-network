//
//  GeographyDetailViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 7/21/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

enum GeoPageIndex: Int
{
    case detail = 0
    case feed
    case calendar
}
class GeographyDetailViewController: UIViewController
{
    //MARK: PROPERTY & OUTLETS
    var geography = ClusterItem(position: CLLocationCoordinate2DMake(0.0, 0.0))
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnFollow: UIButton!    
    @IBOutlet weak var pageControl: UIPageControl!
    
    var geoPagesViewController: GeoPagesViewController?
    @IBOutlet weak var lblPageTitle: UILabel!
    var geographyId: Int = -1
    var geoItem = ClusterItem(json:nil)
    var onDidUpdateGeography:(()->())?
    //TAB BAR
    @IBOutlet weak var tabBar: UITabBar!
    
    lazy var tabBarItem_Post: [UITabBarItem] = {
        
        let icon1 = UITabBarItem(title: "Post", image: UIImage(named: "NewPost"), tag: 0)
        return [icon1]
    }()
    lazy var tabBarItem_Calendar: [UITabBarItem] = {
        
        let icon1 = UITabBarItem(title: "Post", image: UIImage(named: "NewPost"), tag: 0)
        let icon2 = UITabBarItem(title: "Calendar", image: UIImage(named: "Calendar"), tag: 1)
        return [icon1, icon2]
    }()
    lazy var tabBarItem_CalendarList: [UITabBarItem] = {
        
        let icon1 = UITabBarItem(title: "Post", image: UIImage(named: "NewPost"), tag: 0)
        let icon2 = UITabBarItem(title: "List", image: UIImage(named: "DayView"), tag: 2)
        
        return [icon1, icon2]
    }()

    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.getDetail()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        print("Deinit: GeographyDetailViewController")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "getDetail"), object: nil)
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        self.title = "DETAIL".localized()
        
        self.btnFollow.cornerRadius()
        self.btnFollow.addShadow()
        self.lblPageTitle.text = "DETAIL".localized()
        
        //set tab bar
        self.tabBar.delegate = self
        self.tabBar.setItems(tabBarItem_Post, animated: false)
        
        //Notifications
        NotificationCenter.default.addObserver(self, selector:#selector(self.getDetail), name: NSNotification.Name(rawValue: "getDetail"), object: nil)
    }
    
    fileprivate func loadHeader()
    {
        self.lblTitle.attributedText = self.geoItem.getTitleForDetail()
        let btnTitle = self.geoItem.isFollowed == true ? "Unfollow".localized() : "Follow".localized()
        self.btnFollow.setTitle(btnTitle, for: .normal)
        
    }
    
    //MARK: WEB SERVICE
    @objc fileprivate func getDetail()
    {
        let params = ["geographyId":self.geographyId]
        
        //This loads only the title and follow status
        MessageManager.shared.showLoadingHUD()
        LibraryAPI.shared.geographyBO.getDetailById(params, onSuccess: { (geography) in
            
            MessageManager.shared.hideHUD()
            self.geoItem = geography
            //self.geoPagesViewController?.geoItem = self.geoItem
            self.loadHeader()
    
        }) { (error) in
            MessageManager.shared.hideHUD()
            guard error.code != -999 else {return}
            MessageManager.shared.showBar(title: "Error", subtitle: "It wasn't possible to obtain this geography detail. Try again.".localized(), type: .error, fromBottom: false)
        }
        
        //Load Stakeholders
        self.geoPagesViewController?.vcStakeholderProjects?.tableStakeholders.displayBackgroundMessage("Loading...".localized(), subMessage: String())
        LibraryAPI.shared.geographyBO.getStakeholdersByGeoId(params, onSuccess: { (stakeholders) in
            
            self.geoPagesViewController?.vcStakeholderProjects?.tableStakeholders.dismissBackgroundMessage()
            self.geoItem.stakeholders = stakeholders
            self.geoPagesViewController?.geoItem = self.geoItem
            
            if self.geoItem.stakeholders.count == 0
            {
                self.geoPagesViewController?.vcStakeholderProjects?.tableStakeholders.displayBackgroundMessage("No data".localized(), subMessage: String())
            }
            
        }) { (error) in
            self.geoPagesViewController?.vcStakeholderProjects?.tableStakeholders.dismissBackgroundMessage()
            guard error.code != -999 else {return}
            MessageManager.shared.showBar(title: "Error", subtitle: "It wasn't possible to obtain this geography detail. Try again.".localized(), type: .error, fromBottom: false)
        }
        
        //Add geography Id to child controllers
        self.geoPagesViewController?.vcCalendar?.geographyId = self.geographyId
    }

    // MARK: FOLLOW/UNFOLLOW ACTION
    @IBAction func followAction(_ sender: Any)
    {
        MessageManager.shared.showLoadingHUD()
        if self.geoItem.isFollowed == false
        {
            
            LibraryAPI.shared.geographyBO.followGeography(self.geoItem.id, onSuccess: { (clusterItem) in
                
                self.geoItem.isFollowed = clusterItem.isFollowed
                
                self.btnFollow.setTitle("Unfollow".localized(), for: .normal)
                MessageManager.shared.hideHUD()
                let info = "Now, you're following".localized() + " " + self.geoItem.title
                MessageManager.shared.showBar(title: "Success".localized(), subtitle: info, type: .success, fromBottom: false)
                self.onDidUpdateGeography?()
                
            }, onError: { (error) in
                
                MessageManager.shared.hideHUD()
                MessageManager.shared.showBar(title: "Error", subtitle: "It wasn't possible follow this geography. Try again.".localized(), type: .error, fromBottom: false)
            })
        }
        else
        {
            LibraryAPI.shared.geographyBO.unfollowGeography(self.geoItem.id, onSuccess: { (clusterItem) in
                
                self.geoItem.isFollowed = clusterItem.isFollowed
                self.btnFollow.setTitle("Follow".localized(), for: .normal)
                MessageManager.shared.hideHUD()
                let info = "Now, you're not following".localized() + " " + self.geoItem.title
                MessageManager.shared.showBar(title: "Success".localized(), subtitle: info, type: .success, fromBottom: false)
                self.onDidUpdateGeography?()
                
            }, onError: { (error) in
                
                MessageManager.shared.hideHUD()
                MessageManager.shared.showBar(title: "Error", subtitle: "It wasn't possible unfollow this geography. Try again.".localized(), type: .error, fromBottom: false)
            })
        }
    }
    
    // MARK: - TAB BAR
    fileprivate func updateTabBar(currentPage: Int)
    {
        self.pageControl.currentPage = currentPage
        if currentPage == 2
        {
            if self.geoPagesViewController?.vcCalendar?.containerCalendar.isHidden == false
            {
                self.tabBar.setItems(self.tabBarItem_CalendarList, animated: false)
            }
            else
            {
                self.tabBar.setItems(self.tabBarItem_Calendar, animated: false)
            }
        }
        else
        {
            self.tabBar.setItems(self.tabBarItem_Post, animated: false)
        }
    }
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "GeoPagesViewController"
        {
            if let pageController =  segue.destination as? GeoPagesViewController {
                
                self.geoPagesViewController = pageController
                self.geoPagesViewController?.onDidChangePage = { [weak self] page in
                    
                    //Cancel previous requests
                    LibraryAPI.shared.requestBO.cancelAllNetworkRequests()
                    
                    //Set title
                    self?.lblPageTitle.text = (page == 0 ? "DETAIL" : page == 1 ? "FEED" : "CALENDAR").localized().uppercased()
                    
                    //Set tab bar items for each page
                    self?.updateTabBar(currentPage: page)
                    
                    //Load Page Service
                    self?.loadPageService(currentPage: GeoPageIndex(rawValue: page) ?? .detail)
                    
                }
                    
            }
        }
    }

    //MARK: UPDATE PAGE WITH WEB SERVICE
    fileprivate func loadPageService(currentPage: GeoPageIndex)
    {
        //Cancel previous requests
        LibraryAPI.shared.requestBO.cancelAllNetworkRequests()
        switch currentPage
        {
        case .detail:
                self.geoPagesViewController?.vcStakeholderProjects?.updateStakeholderProjectsTable()
        case .feed:
            print("feed")
            self.geoPagesViewController?.vcPosts?.updateFeed()
        case .calendar:
            print("calendar")
            self.geoPagesViewController?.vcCalendar?.loadCalendar()
        }
    }
}

//MARK: TAB BAR DELEGATE
extension GeographyDetailViewController: UITabBarDelegate
{
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        if item.tag == 0
        {
            let vcNewPost = NewPostViewController()
            let navController = NavyController(rootViewController: vcNewPost)
            vcNewPost.geographyTagged = self.geoItem
            vcNewPost.didPostGeography = {[weak self] Void in
                
                //update feeds
                self?.geoPagesViewController?.vcPosts?.lastRegisteredPostId = 0
                self?.geoPagesViewController?.vcPosts?.posts.removeAll()
                self?.geoPagesViewController?.vcPosts?.updateFeed()
                
                //update calendars
                let calendarDate = self?.geoPagesViewController?.vcCalendar?.vcCalendarEvents?.calendar.currentPage ?? Date()
                
                //For iPad
                self?.geoPagesViewController?.vcCalendar?.loadCalendarWithDate(calendarDate)
                
                //For iPhone
                
                self?.geoPagesViewController?.vcCalendar?.loadCalendarWithDate(calendarDate)
                
            }
            navController.modalPresentationStyle = (DeviceType.IS_ANY_IPAD) ?  UIModalPresentationStyle.formSheet :  UIModalPresentationStyle.overFullScreen
            
            present(navController, animated: true, completion: nil)
        }
        else if item.tag == 1
        {
            self.tabBar.setItems(self.tabBarItem_CalendarList, animated: false)
            self.geoPagesViewController?.vcCalendar?.containerCalendar.isHidden = false
            self.geoPagesViewController?.vcCalendar?.containerCalendarList.isHidden = true
        }
        else if item.tag == 2
        {
            self.tabBar.setItems(self.tabBarItem_Calendar, animated: false)
            self.geoPagesViewController?.vcCalendar?.containerCalendar.isHidden = true
            self.geoPagesViewController?.vcCalendar?.containerCalendarList.isHidden = false
        }
    }
}
