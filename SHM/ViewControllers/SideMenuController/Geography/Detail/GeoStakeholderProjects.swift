//
//  GeoStakeholderProjects.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 7/28/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class GeoStakeholderProjects: UIViewController
{
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var tableProjects: UITableView!
    @IBOutlet weak var tableStakeholders: UITableView!
    var geographyId: Int = -1
    var stakeholders = [Stakeholder]() {
        didSet{
            if stakeholders.count == 0 {
                self.tableStakeholders.displayBackgroundMessage("No stakeholders found".localized(), subMessage: "")
            } else {
                self.tableStakeholders.dismissBackgroundMessage()
            }
            self.tableStakeholders.reloadData()
        }
    }
    
    var projects = [Project]() {
        didSet{
            if (projects.count == 0) {
                self.tableProjects.displayBackgroundMessage("No Projects".localized(), subMessage: "")
            } else {
                self.tableProjects.dismissBackgroundMessage()
            }
            self.tableProjects.reloadData()
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: CONFIGS
    fileprivate func loadConfig()
    {
        self.segmentControl.setTitle("Stakeholders".localized(), forSegmentAt: 0)
        self.segmentControl.setTitle("Projects".localized(), forSegmentAt: 1)
        
        //Table Stakeholders
        self.tableStakeholders.tag = 0
        self.tableStakeholders.isHidden = false
        self.tableStakeholders.hideEmtpyCells()
        self.tableStakeholders.register(UINib(nibName: "ContactListCell", bundle: nil), forCellReuseIdentifier: "ContactListCell")
        
        //Table Projects
        //Register Custom Cell
        self.tableProjects.tag = 1
        self.tableProjects.isHidden = true
        self.tableProjects.hideEmtpyCells()
        self.tableProjects.register(UINib(nibName: "ProjectTableCell", bundle: nil), forCellReuseIdentifier: "ProjectTableCell")
        
    }
    
    fileprivate func pushRatingControlFor(_ stakeholder: Stakeholder)
    {
        let vcRating : RatingViewController = Storyboard.getInstanceFromStoryboard("Rating")
        vcRating.didEndRating = { [weak self] in
            
            self?.setEditing(false, animated: true)
            NotificationCenter.default.post(name: Notification.Name(rawValue: "getDetail"), object: nil)
        }
        vcRating.stakeholder = stakeholder        
        self.present(vcRating, animated: true, completion: nil)
    }
    
    @IBAction func segmentChanged(_ sender: Any)
    {
        guard let segment = sender as? UISegmentedControl else {return}
        
        if segment.selectedSegmentIndex == 0
        {   //Load stakeholders
            self.loadStakeholders()
            self.tableStakeholders.isHidden = false
            self.tableProjects.isHidden = true
            
        }
        else
        {
            //Load Projects
            self.loadProjects()
            self.tableStakeholders.isHidden = true
            self.tableProjects.isHidden = false
        }
    }
    
    //MARK: WEB SERVICES 
    func updateStakeholderProjectsTable()
    {
        if self.segmentControl.selectedSegmentIndex == 0
        {
            self.loadStakeholders()
        }
        else
        {
            self.loadProjects()
        }
    }
    
    fileprivate func loadProjects()
    {
        //Cancel previous request
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.getStakeholdersByGeoId)
        //Load Stakeholders
        let params = ["geographyId":self.geographyId]
        
        self.tableProjects.displayBackgroundMessage("Loading...".localized(), subMessage: String())
        LibraryAPI.shared.geographyBO.getProjectsByGeoId(params, onSuccess: { (projects) in
            
            self.tableProjects.dismissBackgroundMessage()
            self.projects = projects
            if self.projects.count == 0
            {
                self.tableProjects.displayBackgroundMessage("No data".localized(), subMessage: String())
            }
        }) { (error) in
            
            self.tableProjects.dismissBackgroundMessage()
            guard error.code != -999 else {return}
            MessageManager.shared.showBar(title: "Error", subtitle: "It wasn't possible to obtain this geography detail. Try again.".localized(), type: .error, fromBottom: false)
        }
    }
    
    fileprivate func loadStakeholders()
    {
        //Cancel Previous requests
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.getProjectsByGeoId)
        //Load Stakeholders
        let params = ["geographyId":self.geographyId]
        self.tableStakeholders.displayBackgroundMessage("Loading...".localized(), subMessage: String())
        
        LibraryAPI.shared.geographyBO.getStakeholdersByGeoId(params, onSuccess: { (stakeholders) in
            
            self.tableStakeholders.dismissBackgroundMessage()
            self.stakeholders =  stakeholders
            if self.stakeholders.count == 0
            {
                self.tableStakeholders.displayBackgroundMessage("No data".localized(), subMessage: String())
            }
            
        }) { (error) in
            self.tableStakeholders.dismissBackgroundMessage()
            guard error.code != -999 else {return}
            MessageManager.shared.showBar(title: "Error", subtitle: "It wasn't possible to obtain this geography detail. Try again.".localized(), type: .error, fromBottom: false)
        }
    }
}

extension GeoStakeholderProjects: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tableView.tag == 0 ? self.stakeholders.count : self.projects.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return tableView.tag == 0 ? 100 : 155
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {        
        if tableView.tag == 0
        {
            let stakeholder = self.stakeholders[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListCell") as? ContactListCell
            cell?.selectionStyle = .none
            cell?.cellForStakeholder(stakeholder)
            cell?.didTapOnRating = {[weak self] void in
                
                self?.pushRatingControlFor(stakeholder)
            }
            return cell ?? UITableViewCell()
        }
        else
        {            
            let project = self.projects[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProjectTableCell") as! ProjectTableCell
            cell.cellForProject(cell, project: project)
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView.tag == 0
        {
            let stakeholder = self.stakeholders[indexPath.row]
            let vcStakeholderDetail : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
            vcStakeholderDetail.stakeholder = stakeholder
            navigationController?.pushViewController(vcStakeholderDetail, animated: true)
        }
        else
        {
            let project = self.projects[indexPath.row]
            let vcProjectDetail : DetailProjectViewController = Storyboard.getInstanceFromStoryboard("Projects")
            vcProjectDetail.project = project
            navigationController?.pushViewController(vcProjectDetail, animated: true)
        }
    }
}
