//
//  GeoCalendarEventsViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 9/1/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit
import FSCalendar
import Spruce

class GeoCalendarEventsViewController: UIViewController
{
    //MARK: OUTLETS AND VARIABLES
    @IBOutlet weak var tableEvents: UITableView!
    @IBOutlet weak var btnMonthYear: UIButton!
    @IBOutlet weak var calendar: FSCalendar!
    
    var selectedDate = Date()
    var onMonthChanged:((_ currentMont:Date)->())?
    var arrayCalendarItems = [CalendarItem]() {
        didSet {
            guard self.tableEvents != nil else{return}
            
            self.calendar(self.calendar, didSelect: self.selectedDate, at: .current)
            if arrayCalendarItems.count == 0 {
                
                self.tableEvents.displayBackgroundMessage("No Events Found".localized(), subMessage: "")
            }
        }
    }
    var arrayDateSelected = [CalendarItem]()
    
    //Dot Colors
    let dotNormal = UIImage.getImageWithColor(UIColor.yellowPost(), size: CGSize(width: 12, height: 12))
    let dotBirthday = UIImage.getImageWithColor(UIColor.greenBirthday(), size: CGSize(width: 12, height: 12))
    let dotRelevantDate = UIImage.getImageWithColor(UIColor.blueImportantDate(), size: CGSize(width: 12, height: 12))

    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()        
    }
    
    fileprivate func loadConfig()
    {
        self.tableEvents.register(UINib(nibName: "CalendarPostTableCell", bundle: nil), forCellReuseIdentifier: "CalendarPostTableCell")
        self.tableEvents.backgroundColor = UIColor.white
        self.tableEvents.hideEmtpyCells()
        
        //Calendar UI Setup
        self.btnMonthYear.backgroundColor = .white
        self.btnMonthYear.setTitle(calendar.currentPage.getMonthYear(), for: .normal)
        //Calendar Setup
        self.calendar.today = Date()
        self.calendar.scrollDirection = .horizontal
        self.calendar.appearance.headerMinimumDissolvedAlpha = 0.0
        self.calendar.scope = .month
    }
    
    //MARK: CALENDAR NAVIGATION
    @IBAction func nextMonth(_ sender: Any)
    {
        self.calendar.currentPage = calendar.currentPage.dateByAddingMonths(1)
        self.onMonthChanged?(self.calendar.currentPage)
    }
    
    @IBAction func previousMonth(_ sender: Any)
    {
        self.calendar.currentPage = calendar.currentPage.dateByAddingMonths(-1)
        self.onMonthChanged?(self.calendar.currentPage)
    }
    
    @IBAction func currentMonthTapped(_ sender: Any)
    {
        
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int
    {
        return self.dotsFor(date).dots
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]?
    {
        return self.dotsFor(date).colors
    }

    private func dotsFor(_ date: Date) -> (dots: Int, colors: [UIColor]?)
    {
        //Dots
        var isBirthday = false
        var isRelevantDate = false
        var isPost = false
        
        //Verify Date has event
        for item in self.arrayCalendarItems where date.getStringStyleMedium() == item.startDate.getStringStyleMedium()
        {
            let arrayEvents = self.arrayCalendarItems.filter({
                $0.startDate.getStringStyleMedium() == date.getStringStyleMedium()
            })
            
            //Get type of events contained
            for event in arrayEvents
            {
                if event.type.lowercased() == "birthday" { isBirthday = true }
                if event.type.lowercased() == "post" { isPost = true }
                if event.type.lowercased() == "relevantdate" { isRelevantDate = true }
            }
        }
        
        //Draw dots
        if isBirthday == true && isPost == false && isRelevantDate == false
        {
            //Only Birthday
            return (1, [UIColor.greenBirthday()])
        }
        else if isBirthday == false && isPost == true && isRelevantDate == false
        {
            //Only Post
            return (1, [UIColor.yellowPost()])
        }
        else if isBirthday == false && isPost == false && isRelevantDate == true
        {
            //Only relevant date
            return (1,[UIColor.blueImportantDate()])
        }
        else if isBirthday == true && isPost == true && isRelevantDate == false
        {
            //birthday and post
            return (2, [UIColor.greenBirthday(), UIColor.yellowPost()])
        }
        else if isBirthday == true && isPost == false && isRelevantDate == true
        {
            //birthday and relevant date
            return (2, [UIColor.greenBirthday(), UIColor.blueImportantDate()])
        }
        else if isBirthday == false && isPost == true && isRelevantDate == true
        {
            //Post and relevant date
            return (2,[UIColor.yellowPost(), UIColor.blueImportantDate()])
        }
        else if isBirthday == true && isPost == true && isRelevantDate == true
        {
            //All Birthday, Post and relevant date
            return (3, [UIColor.greenBirthday(), UIColor.yellowPost(), UIColor.blueImportantDate()])
        }
        else
        {
            return (0,  nil)
        }
    }
}

//MARK: TABLEVIEW DELEGATE, DATASOURCE
extension GeoCalendarEventsViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayDateSelected.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let item = arrayDateSelected[indexPath.row]
        
        if item.type.lowercased() == "post"
        {
            //create custom post
            let customPost = Post()
            customPost.id = item.id
            customPost.title = item.title
            customPost.stakeholders = item.stakeholders
            customPost.geographies = item.geographies
            customPost.attachments = item.attachments
            customPost.projects = item.projects
            customPost.events = item.events
            customPost.createdDateString = item.createdDate
            customPost.createdDate = item.createdDate.toDate()!
            customPost.author = item.createdBy
            customPost.isPrivate  = item.isPrivate
            
            //Cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarPostTableCell") as! FeedTableCell
            cell.tvText.delegate = self
            
            //Color for selected cell
            if DeviceType.IS_ANY_IPAD == false
            {
                let selectedColor = UIView()
                selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
                cell.selectedBackgroundView = selectedColor
            }
            
            cell.iconEvent.image = dotNormal
            cell.iconEvent.layer.cornerRadius = 6
            cell.iconEvent.clipsToBounds = true
            
            
            cell.tvText.text = item.title
            
            cell.cellForPostEvent(cell, post: customPost)
            cell.onStakeholderTap = { [weak self] stakeholder in
                
                //Enabled or Disabled
                guard stakeholder.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: stakeholder.fullName, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                    
                    return
                }
                
                //Workflow iPad / iPhone
                if DeviceType.IS_ANY_IPAD == true
                {
                    let vcStakeholderDetail = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
                    vcStakeholderDetail.stakeholder = stakeholder
                    self?.navigationController?.pushViewController(vcStakeholderDetail, animated: true)
                }
                else
                {
                    let vcStakeholderDetail : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                    vcStakeholderDetail.stakeholder = stakeholder                                        
                    self?.navigationController?.pushViewController(vcStakeholderDetail, animated: true)
                }
            }
            
            cell.onAllStakeholdersTap = { [weak self] Void in
                
                let stakeholdersTableVC = StakeholdersTableViewController.getNewInstance()
                stakeholdersTableVC.stakeholders = customPost.stakeholders
                
                let vcModal = ModalViewController(subViewController: stakeholdersTableVC)
                self?.present(vcModal, animated:true, completion: nil)
            }
            
            cell.onTextViewTap = { [weak self] Void in
                
                if DeviceType.IS_ANY_IPAD == true
                {
                    let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController_iPad.self)
                    vcDetailPost.post = customPost
                    self?.navigationController?.pushViewController(vcDetailPost, animated: true)
                }
                else
                {
                    let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController.self)
                    vcDetailPost.post = customPost
                    self?.navigationController?.pushViewController(vcDetailPost, animated: true)
                }
            }
            
            return cell
        }
        else if item.type.lowercased() == "birthday"
        {
            let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier:"Cell")
            
            //Color for selected cell
            if (DeviceType.IS_ANY_IPAD == false)
            {
                let selectedColor = UIView()
                selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
                cell.selectedBackgroundView = selectedColor
            }
            
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            cell.textLabel?.textColor = UIColor.blackAsfalto()
            cell.textLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
            cell.textLabel?.numberOfLines = 2
            
            let finalText = RelevantEventType.Birthday.rawValue.localized() + " " +  item.title
            cell.textLabel?.text = finalText
            
            /** Style Text */
            let attributedString = NSMutableAttributedString(string: cell.textLabel!.text!)
            attributedString.setBoldAndColor(RelevantEventType.Birthday.rawValue.localized(), color: UIColor.greenBirthday(), size: 10)
            cell.textLabel?.attributedText = attributedString
            
            //type
            cell.imageView?.image = dotBirthday
            cell.imageView?.layer.cornerRadius = 6
            cell.imageView?.clipsToBounds = true
            
            return cell
        }
        else
        {
            //Relevant Dates
            let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier:"Cell")
            
            //Color for selected cell
            if (DeviceType.IS_ANY_IPAD == false)
            {
                let selectedColor = UIView()
                selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
                cell.selectedBackgroundView = selectedColor
            }
            
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            cell.textLabel?.textColor = UIColor.blackAsfalto()
            cell.textLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
            cell.textLabel?.numberOfLines = 5
            
            cell.textLabel?.text = item.title
            
            //type
            cell.imageView?.image = dotRelevantDate
            cell.imageView?.layer.cornerRadius = 6
            cell.imageView?.clipsToBounds = true
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        guard arrayDateSelected.count != 0 else {return 70}
        if arrayDateSelected[indexPath.row].type.lowercased() == "birthday"
        {
            return 44
        }
        else if arrayDateSelected[indexPath.row].type.lowercased() == "post"
        {
            return 115
        }
        else
        {
            return 70
        }
    }
}

// MARK: FSCALENDAR DELEGATE
extension GeoCalendarEventsViewController: FSCalendarDelegate, FSCalendarDataSource
{
    // MARK: FSCalendar DELEGATE
    func calendarCurrentPageDidChange(_ calendar: FSCalendar)
    {
        print("change page to \(calendar.currentPage.getStringStyleMedium()))")
        self.btnMonthYear.setTitle(calendar.currentPage.getMonthYear(), for: .normal)
        
        var oldPage = String()
        
        if oldPage == calendar.currentPage.getStringStyleMedium() {
            print("not really a page change")
            return
        }
        
        oldPage = calendar.currentPage.getStringStyleMedium()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition)
    {
        //Searching items for date selected
        let arrayEvents = arrayCalendarItems.filter({
            $0.startDate.getStringStyleMedium() == date.getStringStyleMedium()
        })
        
        self.selectedDate = date
        
        arrayDateSelected = arrayEvents
        
        //Verify if it's necessary change the height in calendar details
        self.tableEvents.reloadData()
        
        //Spruce framework Animation
        DispatchQueue.main.async {
            
            self.tableEvents.spruce.animate([.contract(.moderately)], animationType: SpringAnimation(duration: 0.7))
        }
        
        //Empty State
        if arrayDateSelected.count == 0
        {
            tableEvents.displayBackgroundMessage("No events".localized(),
                                                 subMessage: "")
        }
        else
        {
            tableEvents.dismissBackgroundMessage()
        }
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool)
    {
        calendar.frame = CGRect(origin: calendar.frame.origin, size: bounds.size)
        self.view.layoutIfNeeded()
    }
}

// MARK: TEXTVIEW DELEGATE (FeedTableCell)
extension GeoCalendarEventsViewController: UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
    {
        let arrayUrl = URL.description.components(separatedBy: "_")
        
        if arrayUrl.count > 1
        {
            let type = arrayUrl[0]
            
            switch type
            {
            case "s":
                //Go to Stakeholder detail
                let stakeholderLinked: Stakeholder! = Stakeholder(isDefault: true)
                stakeholderLinked.id = Int(arrayUrl[1])!
                stakeholderLinked.isEnable = Int(arrayUrl[2])! == 1 ? true : false
                
                let firstname = arrayUrl[3]
                
                //Enabled or Disabled
                guard stakeholderLinked.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: firstname, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                    
                    return true
                }
                
                //Workflow iPad / iPhone
                if DeviceType.IS_ANY_IPAD == true
                {
                    let vcStakeholderDetail = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
                    vcStakeholderDetail.stakeholder = stakeholderLinked
                    navigationController?.pushViewController(vcStakeholderDetail, animated: true)
                }
                else
                {
                    let vcStakeholderDetail : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                    vcStakeholderDetail.stakeholder = stakeholderLinked
                    navigationController?.pushViewController(vcStakeholderDetail, animated: true)
                }
            case "p":
                //Go to Project detail
                let projectLinked: Project! = Project(isDefault: true)
                projectLinked.id = Int(arrayUrl[1])!
                projectLinked.isEnable = Int(arrayUrl[2])! == 1 ? true : false
                
                //Enabled or Disabled
                guard projectLinked.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: nil, subtitle: "This project is disabled".localized(), type: .info, fromBottom: false)
                    
                    return true
                }
                
                //Go to detail
                if DeviceType.IS_ANY_IPAD == true
                {
                    let vcDetailProject = Storyboard.getInstanceOf(ProjectDetailViewController_iPad.self)
                    vcDetailProject.project = projectLinked
                    navigationController?.pushViewController(vcDetailProject, animated: true)
                }
                else
                {
                    let vcDetailProject : DetailProjectViewController = Storyboard.getInstanceFromStoryboard("Projects")
                    vcDetailProject.project = projectLinked
                    navigationController?.pushViewController(vcDetailProject, animated: true)
                }
                
                
            case "d":
                //Add Event to Calendar
                let alert = UIAlertController(title: "Add Event".localized(), message: "Would you like to save this date in your device calendar".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Save".localized(), style: UIAlertActionStyle.default, handler: {
                    (action) in
                    
                    for item in self.arrayDateSelected where item.type.lowercased() == "post"
                    {
                        for theEvent in item.events where theEvent.id == Int(arrayUrl[1])!
                        {
                            //Create post
                            let customPost = Post()
                            customPost.id = item.id
                            customPost.title = item.title
                            customPost.stakeholders = item.stakeholders
                            customPost.attachments = item.attachments
                            customPost.projects = item.projects
                            customPost.events = item.events
                            customPost.createdDateString = item.createdDate
                            customPost.createdDate = item.createdDate.toDate()!
                            customPost.author = item.createdBy
                            
                            //Save Event to iPhone Calendar
                            CalendarManager.sharedInstance.createEvent(customPost.title.getEditedTitleForPost(customPost, truncate: false), event: theEvent)
                        }
                    }
                    
                }))
                self.present(alert, animated: true, completion: nil)
            case "g":
                let geographyId = Int(arrayUrl[1])!
                if DeviceType.IS_ANY_IPAD == false
                {
                    let vcGeoDetail:GeographyDetailViewController = UIStoryboard(name: "Geography", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetailViewController") as! GeographyDetailViewController
                    vcGeoDetail.geographyId = geographyId
                    self.navigationController?.pushViewController(vcGeoDetail, animated: true)
                }
                else
                {
                    let vcGeoDetail:GeographyDetail_iPad = UIStoryboard(name: "GeographyPad", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetail_iPad") as! GeographyDetail_iPad
                    vcGeoDetail.geographyId = geographyId
                    self.navigationController?.pushViewController(vcGeoDetail, animated: true)
                }
            default:
                break
            }
        }
        
        return true
    }
}

