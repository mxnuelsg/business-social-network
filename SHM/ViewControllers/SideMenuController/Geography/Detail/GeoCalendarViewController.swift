//
//  GeoCalendarViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 7/28/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class GeoCalendarViewController: UIViewController
{
    //MARK: OUTLETS AND VARIABLES
    var vcCalendar: CalendarViewController? //Used in iPad
    var vcCalendarEvents: GeoCalendarEventsViewController?//Used in iPhone
    var vcCalendarList: CalendarListViewController?
    
    var currentDate:Date = Date()
    
    var calendarItems = [CalendarItem]() {
        didSet {
            self.loadCalendars()
        }
    }
    var geoItem = ClusterItem(json: nil)
    var geographyId = -1
    
    @IBOutlet weak var btnGoToCalendar: UIButton!
    @IBOutlet weak var containerCalendarList: UIView!
    @IBOutlet weak var containerCalendar: UIView!
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    deinit
    {
        print("deinit GeoCalendarViewController")
    }
    
    //MARK: CONFIGS
    fileprivate func loadConfig()
    {
        if DeviceType.IS_ANY_IPAD == true
        {
            self.btnGoToCalendar.cornerRadius()
            self.btnGoToCalendar.setTitle("GO TO CALENDAR".localized(), for: UIControlState())
        }
    }
    //MARK: CONFIGS
    fileprivate func updateCurrentSelectedDate()
    {
        if DeviceType.IS_ANY_IPAD == true
        {
            let selectedDate = self.vcCalendarEvents?.calendar.selectedDate ?? Date()
            if let calendar = self.vcCalendarEvents?.calendar {
                self.vcCalendarEvents?.calendar(calendar, didSelect: selectedDate, at: .current)
            }
        }
        else
        {
            let selectedDate = self.vcCalendar?.calendar.selectedDate ?? Date()
            if let calendar = self.vcCalendar?.calendar {
                self.vcCalendar?.calendar(calendar, didSelect: selectedDate, at: .current)
            }
        }
    }
    //MARK: ACTIONS
    @IBAction func goToCalendar(_ sender: Any)
    {
        let vcCalendar = Storyboard.getInstanceOf(CalendarViewController_iPad.self)
        navigationController?.pushViewController(vcCalendar, animated: true)
    }
    
    // MARK: - WEB SERVICES
    func loadCalendar()
    {
        //Cancel previous request
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.getStakeholdersByGeoId)
        //Load Stakeholders
        let params = ["geographyId":self.geographyId]
        
        self.vcCalendar?.tableEvents.displayBackgroundMessage("Loading...", subMessage: String())
        self.vcCalendarEvents?.tableEvents.displayBackgroundMessage("Loading...", subMessage: String())
        self.vcCalendarList?.tableEvents.displayBackgroundMessage("Loading...", subMessage: String())
        LibraryAPI.shared.geographyBO.getEventsByGeoId(params, onSuccess: { (clusterItem) in
            
            self.calendarItems = clusterItem.events
            
            //Update current selected day in calendar
            self.updateCurrentSelectedDate()
            
            //Remove messages
            self.vcCalendar?.tableEvents.dismissBackgroundMessage()
            self.vcCalendarList?.tableEvents.dismissBackgroundMessage()
        }) { (error) in
            
            guard error.code != -999 else {return}
            MessageManager.shared.showBar(title: "Error", subtitle: "It wasn't possible to obtain this geography detail. Try again.".localized(), type: .error, fromBottom: false)
            self.vcCalendar?.tableEvents.displayBackgroundMessage("No data".localized(), subMessage: String())
            self.vcCalendarEvents?.tableEvents.displayBackgroundMessage("No data".localized(), subMessage: String())
            self.vcCalendarList?.tableEvents.displayBackgroundMessage("No data".localized(), subMessage: String())
        }
    }

    // LOAD CALENDAR WITH SPECIFIC DATE
    func loadCalendarWithDate(_ date: Date)
    {
        //Cancel previous request
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.getStakeholdersByGeoId)
        
        //Prepare parameters
        var params:[String: Any] = [:]
        params["Type"] = 3
        params["Month"] = date.month
        params["Year"] = date.year
        params["Id"] = 0
        params["AllDay"] = "false"
        params["ProjectId"] = 0
        params["StakeholderId"] = 0
        params["GeographyId"] = self.geographyId
        
        self.vcCalendar?.tableEvents.displayBackgroundMessage("Loading...", subMessage: String())
        self.vcCalendarEvents?.tableEvents.displayBackgroundMessage("Loading...", subMessage: String())
        self.vcCalendarList?.tableEvents.displayBackgroundMessage("Loading...", subMessage: String())
        LibraryAPI.shared.geographyBO.getCalendarEventsByGeoId(params, onSuccess: { (clusterItem) in
            
            self.calendarItems = clusterItem.events
            
            //Update current selected day in calendar
            self.updateCurrentSelectedDate()
            
            //Remove messages
            self.vcCalendar?.tableEvents.dismissBackgroundMessage()
            self.vcCalendarList?.tableEvents.dismissBackgroundMessage()
        }) { (error) in
            
            guard error.code != -999 else {return}
            MessageManager.shared.showBar(title: "Error", subtitle: "It wasn't possible to obtain this geography detail. Try again.".localized(), type: .error, fromBottom: false)
            self.vcCalendar?.tableEvents.displayBackgroundMessage("No data".localized(), subMessage: String())
            self.vcCalendarEvents?.tableEvents.displayBackgroundMessage("No data".localized(), subMessage: String())
            self.vcCalendarList?.tableEvents.displayBackgroundMessage("No data".localized(), subMessage: String())
        }
    }
    
    func loadCalendar_iPad()
    {
        //Cancel previous request
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.getStakeholdersByGeoId)
        
        //Prepare parameters
        let date = self.vcCalendar?.calendar.currentPage ?? Date()
        var params:[String: Any] = [:]
        params["Type"] = 3
        params["Month"] = date.month
        params["Year"] = date.year
        params["Id"] = 0
        params["AllDay"] = "false"
        params["ProjectId"] = 0
        params["StakeholderId"] = 0
        params["GeographyId"] = self.geographyId
        
        self.vcCalendar?.tableEvents.displayBackgroundMessage("Loading...", subMessage: String())
        self.vcCalendarEvents?.tableEvents.displayBackgroundMessage("Loading...", subMessage: String())
        self.vcCalendarList?.tableEvents.displayBackgroundMessage("Loading...", subMessage: String())
        LibraryAPI.shared.geographyBO.getCalendarEventsByGeoId(params, onSuccess: { (clusterItem) in
            
            self.calendarItems = clusterItem.events
            
            //Update current selected day in calendar
            self.updateCurrentSelectedDate()
            
            //Remove messages
            self.vcCalendar?.tableEvents.dismissBackgroundMessage()
            self.vcCalendarList?.tableEvents.dismissBackgroundMessage()
        }) { (error) in
            
            guard error.code != -999 else {return}
            MessageManager.shared.showBar(title: "Error", subtitle: "It wasn't possible to obtain this geography detail. Try again.".localized(), type: .error, fromBottom: false)
            self.vcCalendar?.tableEvents.displayBackgroundMessage("No data".localized(), subMessage: String())
            self.vcCalendarEvents?.tableEvents.displayBackgroundMessage("No data".localized(), subMessage: String())
            self.vcCalendarList?.tableEvents.displayBackgroundMessage("No data".localized(), subMessage: String())
        }
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "Calendar"
        {
            if DeviceType.IS_ANY_IPAD == true
            {
                if let calendar = segue.destination as? CalendarViewController {
                    
                    self.vcCalendar = calendar
                    self.vcCalendar?.showsInGeography = true
                    self.vcCalendar?.arrayCalendarItems = self.calendarItems
                    self.vcCalendar?.onMonthChanged = {[weak self] date in
                        
                        self?.loadCalendarWithDate(date)
                    }
                }
            }
            else
            {
                if let calendar = segue.destination as?
                    GeoCalendarEventsViewController {
                    
                    self.vcCalendarEvents = calendar
                    self.vcCalendarEvents?.arrayCalendarItems = self.calendarItems
                    self.vcCalendarEvents?.onMonthChanged = {[weak self] date in
                        
                        self?.loadCalendarWithDate(date)
                    }
                }
            }
        }
        else if segue.identifier == "CalendarList"
        {
            if let calendarList = segue.destination as? CalendarListViewController {
                
                self.vcCalendarList = calendarList
                self.vcCalendarList?.showsInGeography = true
                
                //Load with required instance of DateList
                var dateListArray = [DateList]()
                for item in self.calendarItems
                {
                    let dateList = DateList()
                    let post = Post()
                    //Extract posts
                    post.title = item.title
                    post.geographies = item.geographies
                    post.events = item.events
                    post.stakeholders = item.stakeholders
                    post.projects = item.projects
                    post.attachments = item.attachments
                    //Include extracted posts to calendar list
                    dateList.date = item.startDate
                    dateList.posts = [post]
                    dateListArray.append(dateList)
                    dateListArray.last?.arrayEvents.append(post)
                }
                self.vcCalendarList?.arrayCalendarList = dateListArray
                self.vcCalendarList?.didReloadGeoList = {[weak self] void in
                
                    self?.loadCalendar()
                }
            }
        }
    }
    
    fileprivate func loadCalendars()
    {
        //Load Calendar
        if DeviceType.IS_ANY_IPAD == true
        {
            self.vcCalendar?.arrayCalendarItems = self.calendarItems
            self.vcCalendar?.calendar.reloadData()
        }
        else
        {
            self.vcCalendarEvents?.arrayCalendarItems = self.calendarItems
            self.vcCalendarEvents?.calendar.reloadData()
        }
        //Load Calendar list
        self.vcCalendarList?.activityIndicator.stopAnimating()
        self.vcCalendarList?.refreshControl.endRefreshing()
        
        var dateListArray = [DateList]()
        for item in self.calendarItems
        {
            let dateList = DateList()
            let post = Post()
            //Extract posts
            post.title = item.title
            post.geographies = item.geographies
            post.events = item.events
            post.stakeholders = item.stakeholders
            post.projects = item.projects
            post.attachments = item.attachments
            //Include extracted posts to calendar list
            dateList.date = item.startDate
            dateList.posts = [post]
            dateListArray.append(dateList)
            dateListArray.last?.arrayEvents.append(post)
        }
        self.vcCalendarList?.arrayCalendarList = dateListArray
        self.vcCalendarList?.tableEvents.reloadData()
        if self.calendarItems.count == 0
        {
            self.vcCalendarList?.tableEvents.hideEmtpyCells()
            self.vcCalendarList?.tableEvents.displayBackgroundMessage("No Events Found".localized(), subMessage: "")
        }
    }
}
