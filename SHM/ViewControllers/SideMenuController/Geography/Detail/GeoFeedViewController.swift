//
//  GeoFeedViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 7/29/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class GeoFeedViewController: UIViewController
{

    @IBOutlet weak var containerFeed: UIView!
    @IBOutlet weak var containerAttachment: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var lastRegisteredPostId = 0
    var geographyId = -1
    var vcPostTable: PostsTableViewController?
    var vcAttachments: FileListViewController?
    var posts = [Post]() {
        didSet{
            self.vcPostTable?.posts = self.posts
            self.vcPostTable?.reload()
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.loadAttachments()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func loadConfig()
    {
        self.segmentedControl.setTitle("Post".localized(), forSegmentAt: 0)
        self.segmentedControl.setTitle("Attachments".localized(), forSegmentAt: 1)
        self.containerFeed.isHidden = false
        self.containerAttachment.isHidden = true
        if DeviceType.IS_ANY_IPAD == true
        {
            self.view.backgroundColor = UIColor.whiteCloud()
            self.vcPostTable?.tableView.backgroundColor = UIColor.whiteCloud()
            self.vcAttachments?.view.backgroundColor = UIColor.whiteCloud()
        }
    }
    
    func reloadFeedsAttachments()
    {
        self.vcPostTable?.reload()        
        self.loadAttachments()
    }
    
    fileprivate func loadAttachments()
    {
        var attachments = [Attachment]()
        for post in self.posts
        {
            attachments.append(contentsOf: post.attachments)
        }
        self.vcAttachments = FileListViewController(attachments: attachments)
        if let vcFile = self.vcAttachments {
        
            self.containerAttachment.addSubViewController(vcFile, parentVC: self)
            self.vcAttachments?.tableFiles.reloadData()
        }
    }
    
    @IBAction func segmentChanged(_ sender: Any)
    {
        guard let segment = sender as? UISegmentedControl else {return}
        
        if segment.selectedSegmentIndex == 0
        {
            self.containerFeed.isHidden = false
            self.containerAttachment.isHidden = true
        }
        else
        {
            self.containerFeed.isHidden = true
            self.containerAttachment.isHidden = false
        }
    }
    
    // MARK: - WEB SERVICES
    func updateFeed()
    {
        //Prepare request
        var wsObject: [String : Any]  = [:]
        wsObject["postId"] = self.lastRegisteredPostId
        wsObject["type"] = 1
        wsObject["pageSize"] = 20
        wsObject["geographyId"] = self.geographyId
        
        if self.posts.count == 0
        {
            self.vcPostTable?.displayBackgroundMessage("Loading...".localized(), subMessage: String())
        }
        LibraryAPI.shared.stakeholderBO.getFeedEditingStakeholder(parameters: wsObject, onSuccess: { (feed) in
            
            guard feed.posts.count != 0 else {
                
                if self.posts.count == 0
                {
                    self.vcPostTable?.displayBackgroundMessage("No data".localized(), subMessage: String())
                }
                return
            }
            //Update posts and attachments table
            let posts = self.posts + feed.posts
            self.posts = posts
            self.loadAttachments()
            //Update last registered post id
            self.lastRegisteredPostId = feed.posts.last?.id ?? self.lastRegisteredPostId
            //Dismiss table message
            self.vcPostTable?.dismissBackgroundMessage()
            
        }, onError: { error in
            self.vcPostTable?.dismissBackgroundMessage()
            guard error.code != -999 else {return}
            MessageManager.shared.showBar(title: "Error",
                                          subtitle: "Error.TryAgain".localized(),
                                          type: .error,
                                          fromBottom: false)
        
        })
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "PostsTableViewController"
        {
            if let vcPosts =  segue.destination as? PostsTableViewController {
                
                self.vcPostTable = vcPosts
                self.vcPostTable?.canPullToRefresh = false
                self.vcPostTable?.posts = self.posts
                self.vcPostTable?.reload()
                self.vcPostTable?.willDisplayLastRow = { row in
                    
                    print("Last row")
                    self.updateFeed()
                    
                }
            }
        }
    } 
}
