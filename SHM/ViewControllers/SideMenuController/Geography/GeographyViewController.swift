//
//  GeographyViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 7/6/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

fileprivate enum GeoScreenType: Int
{
    case map
    case list
}
class GeographyViewController: UIViewController
{
    //MARK: OUTLETS AND VARIABLES
    @IBOutlet weak var containerMap: UIView!
    @IBOutlet weak var containerList: UIView!
    @IBOutlet weak var viewBottomBar: UIView!
    @IBOutlet weak var btnChangeView: UIButton!
    
    fileprivate var vcGeoMap: ClusterMapViewController!
    fileprivate var vcGeoList: GeoListViewController!
    fileprivate var screen: GeoScreenType = .map
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()        
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit
    {
        print("Deinit: GeographyViewController")
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        self.title = "GEOGRAPHIES".localized()
        
        //UI
        self.viewBottomBar.blur()
        self.btnChangeView.round()
        self.btnChangeView.addShadow()
        self.vcGeoList.vcGeoTable.bottomSpace(self.viewBottomBar.frame.height)
        
        if self.screen == .map
        {
            self.containerList.alpha = 0
            self.containerList.isUserInteractionEnabled = false
            self.btnChangeView.setImage(UIImage(named: "iconList")!, for: .normal)
        }
        else
        {
            self.containerMap.alpha = 0
            self.containerMap.isUserInteractionEnabled = false
            self.btnChangeView.setImage(UIImage(named: "iconMapView")!, for: .normal)
        }
        
        let barBtnCompose = UIBarButtonItem(image: #imageLiteral(resourceName: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        navigationItem.rightBarButtonItem = barBtnCompose
        
        //Request WS
        self.getFollowedGeographiesLight()
    }
    
    //MARK: WEB SERVICE
    fileprivate func getFollowedGeographiesLight(/*GeographyId:Int, SearchText:String*/)
    {
        let params:[String : Any] = [:]
        MessageManager.shared.showLoadingHUD()
        self.vcGeoList.vcGeoTable.tableView.displayBackgroundMessage("Loading...".localized(), subMessage: String())
        LibraryAPI.shared.geographyBO.getFollowedGeographiesLight(params, onSuccess: { (geographies, totalItems) in
            
            MessageManager.shared.hideHUD()
            self.vcGeoList.vcGeoTable.tableView.dismissBackgroundMessage()            
            self.vcGeoMap.locations = geographies
            self.vcGeoList.vcGeoTable.clusterItems = geographies
            self.vcGeoList.vcGeoTable.backupClusterItems = geographies
            
        }) { error in
            
            //Error Indicator
            MessageManager.shared.showErrorHUD()
            self.vcGeoList.vcGeoTable.tableView.displayBackgroundMessage("No data".localized(), subMessage: String())
            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                MessageManager.shared.hideHUD()
            }
        }
    }
    
    //MARK: ACTIONS
    func createNewPost()
    {
        let vcNewPost = NewPostViewController()
        
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = DeviceType.IS_ANY_IPAD == true ?  .formSheet :  .overFullScreen
        
        present(navController, animated: true, completion: nil)
    }
    
    @IBAction func changeView()
    {
        switch self.screen
        {
        case .map:
            
            //Change to list
            self.screen = .list
            self.containerList.isUserInteractionEnabled = true
            self.containerMap.isUserInteractionEnabled = false
            
            UIView.animate(withDuration: 0.5, animations: {
                
                self.containerList.alpha = 1
                self.btnChangeView.setImage(UIImage(named: "iconMapView")!, for: .normal)
            })
            
            UIView.animate(withDuration: 0.5, animations: {
                
                self.containerMap.alpha = 0
            })
        case .list:
            
            //Change to map
            self.screen = .map
            self.containerMap.isUserInteractionEnabled = true
            self.containerList.isUserInteractionEnabled = false
            self.getFollowedGeographiesLight()
            
            UIView.animate(withDuration: 0.5, animations: {
                
                self.containerMap.alpha = 1
                self.btnChangeView.setImage(UIImage(named: "iconList")!, for: .normal)
            })
            
            UIView.animate(withDuration: 0.5, animations: {
                
                self.containerList.alpha = 0
            })
        }
    }
    
    //MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let vcMap = segue.destination as? ClusterMapViewController {
            
            self.vcGeoMap = vcMap
            
        }
        
        if let vcList = segue.destination as? GeoListViewController {
            
            self.vcGeoList = vcList
        }
    }
}
