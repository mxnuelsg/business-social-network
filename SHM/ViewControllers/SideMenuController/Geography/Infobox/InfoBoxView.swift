//
//  InfoBoxView.swift
//  SHM
//
//  Created by Manuel Salinas on 7/20/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class InfoBoxView: UIView
{
    //MARK: OUTLETS & PROPERTIES
    @IBOutlet weak fileprivate var lblTitle: UILabel!
    @IBOutlet weak fileprivate var lblSubtitle: UILabel!
    
    var mapItem: ClusterItem {
        didSet {
        
            self.lblTitle.text = mapItem.title
            self.lblSubtitle.text = mapItem.parentTitle
        }
    }

    //MARK// LIFE CYCLE
    override init(frame: CGRect)
    {
        self.mapItem = ClusterItem(position: CLLocationCoordinate2DMake(0.0, 0.0))
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        self.mapItem = ClusterItem(position: CLLocationCoordinate2DMake(0.0, 0.0))
        super.init(coder: aDecoder)
    }
    
     override func draw(_ rect: CGRect)
     {
     }
    
    class func instantiateFromXib() -> InfoBoxView
    {
        return UINib(nibName: "InfoBoxView", bundle: nil).instantiate(withOwner: nil, options: nil).first as! InfoBoxView
    }
}
