//
//  GeoPagesViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 7/28/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class GeoPagesViewController: UIPageViewController
{
    var vcStakeholderProjects: GeoStakeholderProjects?
    var vcPosts: GeoFeedViewController?
    var vcCalendar: GeoCalendarViewController?
    var viewControllerList:[UIViewController]?
    var onDidChangePage:((_ page: Int)->())?
    
    var geoItem = ClusterItem(json:nil) {
        didSet{
            self.vcStakeholderProjects?.projects = self.geoItem.projects
            self.vcStakeholderProjects?.stakeholders = self.geoItem.stakeholders
            self.vcStakeholderProjects?.geographyId = self.geoItem.id
            
            self.vcPosts?.posts = self.geoItem.posts
            self.vcPosts?.geographyId = self.geoItem.id
            
            self.vcCalendar?.geographyId = self.geoItem.id
            self.vcCalendar?.calendarItems = self.geoItem.events            
            self.vcCalendar?.geoItem = self.geoItem
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func loadConfig()
    {
        self.dataSource = self
        self.delegate = self
        self.setVCList()
        
        if let firstViewController = self.viewControllerList?.first {
            
            self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    fileprivate func setVCList()
    {
        let sb = UIStoryboard(name: "Geography", bundle: nil)
        self.vcStakeholderProjects = sb.instantiateViewController(withIdentifier: "GeoStakeholderProjects") as? GeoStakeholderProjects
        self.vcPosts = sb.instantiateViewController(withIdentifier: "GeoFeedViewController") as? GeoFeedViewController
        self.vcCalendar = sb.instantiateViewController(withIdentifier: "GeoCalendarViewController") as? GeoCalendarViewController
        
        self.viewControllerList = [self.vcStakeholderProjects!, self.vcPosts!, self.vcCalendar!]
    }
}

extension GeoPagesViewController: UIPageViewControllerDataSource,UIPageViewControllerDelegate
{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        guard let vcIndex = viewControllerList?.index(of: viewController) else {return nil}
        
        let previousIndex = vcIndex - 1
        guard previousIndex >= 0 else {return nil}
        guard (viewControllerList?.count)! > previousIndex else {return nil}
        
        
        return viewControllerList?[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        guard let vcIndex = viewControllerList?.index(of: viewController) else {return nil}
        
        let nextIndex = vcIndex + 1
        guard viewControllerList?.count != nextIndex else {return nil}
        guard (viewControllerList?.count)! > nextIndex else {return nil}
        
        return viewControllerList?[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        guard completed == true else {return}
        if let page = pageViewController.viewControllers?.first?.view.tag {
            
            self.onDidChangePage?(page)
            print(page)
        }
    }

}
