//
//  GeographyTableViewCell.swift
//  SHM
//
//  Created by Manuel Salinas on 7/18/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class GeographyTableViewCell: UITableViewCell
{
    //MARK: OUTLETS & PROPERTIES
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    
    //MARK: LIFE CYCLE
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.loadConfig()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    fileprivate func loadConfig()
    {
        self.accessoryView = UIImageView(image: UIImage(named: "iconSwipeDots"))
    }
}
