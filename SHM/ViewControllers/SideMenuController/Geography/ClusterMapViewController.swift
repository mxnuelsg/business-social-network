//
//  ClusterMapViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 7/6/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit
import GoogleMaps


class ClusterMapViewController: UIViewController
{
    //MARK: OUTLETS & PROPERTIES
    var onDidUpdateGeography:(()->())?
    
    //Default Values
    fileprivate let kCameraLatitude = 36.2077343
    fileprivate let kCameraLongitude = -113.7407914
    fileprivate let kZoom: Float = 2.5//min 1 |- - - - - - - -| 10 max
    
    //Map and Manager
    fileprivate var mapView: GMSMapView!
    fileprivate var clusterManager: GMUClusterManager!
    
    var locations = [ClusterItem]() {
        didSet {
            self.clusterManager.clearItems()
            self.clusterManager.add(self.locations)
            self.clusterManager.cluster()
        }
    }

    //MARK: LIFE CYCLE
    override func loadView()
    {
        let camera = GMSCameraPosition.camera(withLatitude: self.kCameraLatitude, longitude: self.kCameraLongitude, zoom: self.kZoom)
        self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)

        //Adding to View
        self.view = self.mapView
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        print("Deinit: ClusterMapViewController")
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        //Group buckets : how many levels do you want? (must be increasing order)
        let numbers: [NSNumber] = [10, 50, 99]
        let images = [UIImage(named: "iconCluster")!, UIImage(named: "iconCluster")!, UIImage(named: "iconCluster")!]
      
        // Set up the cluster manager with default icon generator and renderer.
        let iconGenerator = GMUDefaultClusterIconGenerator(buckets: numbers, backgroundImages: images)
//        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView, clusterIconGenerator: iconGenerator)
        self.clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm, renderer: renderer)
        renderer.delegate = self

        // Register self to listen to both GMUClusterManagerDelegate and GMSMapViewDelegate events.
        self.clusterManager.setDelegate(self, mapDelegate: self)
    }
}


// MARK: - GMUClusterManagerDelegate
extension ClusterMapViewController: GMUClusterManagerDelegate
{
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool
    {
        //Zoom values: min 1 |- - - - - - - -| 10 max
        let newZoom = self.mapView.camera.zoom >= 7 ? self.mapView.camera.zoom + 2.5 : self.mapView.camera.zoom + 2.9
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position, zoom: newZoom)
        let update = GMSCameraUpdate.setCamera(newCamera)
        self.mapView.moveCamera(update)
        
        return false
    }
}

// MARK: - GMUMapViewDelegate
extension ClusterMapViewController: GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool
    {
        if let item = marker.userData as? ClusterItem {
            
            NSLog("Did tap marker for cluster item \(item.title)")
            
        }
        else
        {
            NSLog("Did tap a normal marker")
        }
        
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D)
    {
        print("LAT: \(coordinate.latitude) -  LONG: \(coordinate.longitude)")
        
        //Get Country and State
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { GMSReverseGeocodeResponse, error in
            
            let address = GMSReverseGeocodeResponse?.firstResult()//.results()
            print(address?.country ?? "none")
            print(address?.administrativeArea ?? "none")
        }
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView?
    {
        guard let data = marker.userData, let item = data as? ClusterItem else  {
            
            return nil
        }
        
        //Location Point
        let viewInfoBox = InfoBoxView.instantiateFromXib()
        viewInfoBox.mapItem = item
        viewInfoBox.setBorder(UIColor.blueUnreadBullet())
        
        let popup = UIView(frame: CGRect(x: marker.accessibilityActivationPoint.x, y: marker.accessibilityActivationPoint.y, width: viewInfoBox.frame.width, height: viewInfoBox.frame.height))
        popup.addSubview(viewInfoBox)
        
        return popup
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker)
    {
        if let data = marker.userData, let item = data as? ClusterItem {
            
            if DeviceType.IS_ANY_IPAD == false
            {
                let vcGeoDetail: GeographyDetailViewController =  Storyboard.getInstanceFromStoryboard("Geography")
                vcGeoDetail.geography = item
                vcGeoDetail.geographyId = item.id
                self.navigationController?.pushViewController(vcGeoDetail, animated: true)
            }
            else
            {
                let vcGeoDetail:GeographyDetail_iPad = UIStoryboard(name: "GeographyPad", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetail_iPad") as! GeographyDetail_iPad
                vcGeoDetail.geographyId = item.id
                vcGeoDetail.onDidUpdateGeography = {[weak self] void in
                    
                    self?.onDidUpdateGeography?()
                }
                self.navigationController?.pushViewController(vcGeoDetail, animated: true)
            }
        }
    }
}

//MARK: GMUClusterRendererDelegate
extension ClusterMapViewController: GMUClusterRendererDelegate
{
    func renderer(_ renderer: GMUClusterRenderer, markerFor object: Any) -> GMSMarker
    {
        let pin = GMSMarker()
        pin.icon = UIImage(named: "iconPin")
        
        return pin
    }
    
    /*
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker)
    {
        print("Rendered")
    }
    */
}
