//
//  GeoTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 7/6/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit
import Spruce

enum GeoListMode: Int
{
    case following = 0
    case all
}

enum OwnerModule {
    case geography
    case stakeholderDetail
    case newPost
    case projectDetail
    case projectAndStakeholderEdition
}

enum ContentDisplay: Int
{
    case posts
    case projects
    case stakeholders
}

class GeoTableViewController: BaseProjectDetailsTableViewController
{
    //MARK: VARIABLES AND OUTLETS
    
    //Geographies
    fileprivate var sectionTitles = [String]()
    fileprivate var sectionedGeographies = [[ClusterItem]]()
    var selectedGeographies = [ClusterItem]()
    //Post, Stakeholders, Projects
    fileprivate var posts = [Post]()
    fileprivate var stakeholders: [Stakeholder]?
    fileprivate var projects:[Project]?
    
    //Query
    var geoQuery = GeographyQuery()
    var geoListMode : GeoListMode = .following
    var ownerModule: OwnerModule = .geography
    fileprivate var currentContent: ContentDisplay = .posts
    
    //Closures
    var onDidLoadGeographies:((_ numberOfItems: CGFloat)->())?
    var onDidSelectGeography:((_ geography: ClusterItem)->())?
    var onDidUpdateGeography:(()->())?
    
    //Cluster items
    var backupClusterItems = [ClusterItem]()
    var geographiesAdded = [ClusterItem]()
    
    var clusterItems = [ClusterItem]() {
        didSet {
        
            //Rerefresh Table's right indexes
            if self.ownerModule == .newPost
            {
                self.indexGeographiesForPost()
            }
            else
            {
                self.indexGeographies()
            }

            //Reload table
            self.tableView.reloadData()
            
            //Spruce framework Animation
            DispatchQueue.main.async {
                
                self.tableView.spruce.animate([.fadeIn, .expand(.slightly)], sortFunction: LinearSortFunction(direction: .topToBottom, interObjectDelay: 0.1))
            }
            guard self.clusterItems.count > 0 else {
                
                //Broadcast 0 number of items
                //self.onDidLoadGeographies?(0)
                //self.tableView.displayBackgroundMessage("No data".localized(), subMessage: String())
                self.tableView.displayBackgroundMessage("No Geographies".localized(),
                                                   subMessage: "")
                self.onDidLoadGeographies?(0)
                
                return
            }
            self.tableView.dismissBackgroundMessage()
            
            //Update post
            self.posts.removeAll()
            self.clusterItems.forEach({self.posts.append(contentsOf: $0.posts)})
            
            //Broadcast the number of items updated
            self.onDidLoadGeographies?(CGFloat(self.clusterItems.count))
        }
    }
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    func initialize(geographies: [ClusterItem], onHeightSet: @escaping (CGFloat) -> Void) {
        self.ownerModule = .projectDetail
        self.tableView.bounces = false
        self.onHeightSet = onHeightSet
        self.clusterItems = geographies
    }

    deinit
    {
        print("Deinit: GeoTableViewController")
    }
    
    // MARK: CONFIG
    fileprivate func loadConfig()
    {
        self.tableView.register(UINib(nibName: "GeographyTableViewCell", bundle: nil), forCellReuseIdentifier: "GeographyTableViewCell")        
    }
    
    //MARK: PUBLIC ACTION
    func bottomSpace(_ bottomInset: CGFloat)
    {
        self.tableView.contentInset.bottom = bottomInset
    }
    
    func showLoadingMode()
    {
        self.clusterItems.removeAll()
        self.sectionedGeographies.removeAll()
        self.tableView.reloadData()
        self.tableView.displayBackgroundMessage("Loading...".localized(), subMessage: String())
    }
    
    //This function can be used from the ParentController
    //as this class have the ability to fetch its own data.
    func refreshTable()
    {        
        //Update UI
        self.view.endEditing(true)
        if self.geoListMode == .following || self.geoQuery.pageNumber == 1 
        {
            self.showLoadingMode()
        }
        
        //Cancel Previous request
        self.cancelSearchRequests()
        
        //Request Geographies
        if self.geoListMode == .all
        {
            LibraryAPI.shared.geographyBO.getGeographiesLight(self.geoQuery.getWSObject(), onSuccess: { (geographies, totalItems) in
                
                //Update and broadcast the number of items
                self.geoQuery.totalItems = totalItems
                if self.ownerModule == .newPost
                {
                    let geoItems = self.clusterItems + geographies
                    self.clusterItems = self.filterUpdateGeographies(geographies: geoItems)
                    
                    self.onDidLoadGeographies?(CGFloat(geographies.count))
                }
                else
                {
                    if self.geoQuery.pagination == true
                    {
                        let geoItems = self.clusterItems + geographies
                        self.clusterItems = geoItems
                    }
                    //Update and broadcast the number of items
                    self.onDidLoadGeographies?(CGFloat(geographies.count))
                }
            }, onError: { (error) in
                guard error.code != -999 else {
                    
                    return
                }
                self.displayBackgroundMessage("No data".localized(), subMessage: String())
            })
        }
        else if self.geoListMode == .following
        {
            LibraryAPI.shared.geographyBO.getFollowedGeographiesLight(GeographyQuery().getWSObject(), onSuccess: { (geographies, totalItems) in
                
                self.clusterItems = geographies
                self.backupClusterItems = geographies
                self.onDidLoadGeographies?(CGFloat(geographies.count))
            }, onError: { (error) in
                guard error.code != -999 else {
                    
                    return
                }
                self.displayBackgroundMessage("No data".localized(), subMessage: String())
            })
        }
        else if self.ownerModule == .newPost
        {
            LibraryAPI.shared.geographyBO.getGeographiesLight(self.geoQuery.getWSObject(), onSuccess: { (geographies, totalItems) in
                
                //Update and broadcast the number of items
                self.geoQuery.totalItems = totalItems
                self.onDidLoadGeographies?(CGFloat(geographies.count))
                
                self.clusterItems.append(contentsOf: geographies)
                let geoItems = self.filterUpdateGeographies(geographies: self.clusterItems)
                self.clusterItems = geoItems
                
            }, onError: { (error) in
                guard error.code != -999 else {
                    
                    return
                }
                self.displayBackgroundMessage("No data".localized(), subMessage: String())
            })
        }
    }
    
    //MARK: UTILITIES
    func filterByText(_ text: String)
    {
        guard text.isEmpty == false else {
            
            self.clusterItems = self.backupClusterItems
            return
        }
        self.clusterItems = self.backupClusterItems.filter({$0.titleComplete.containsString(text)})
    }
    func cancelSearchRequests()
    {
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.getGeographiesLight)
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.getFollowedGeographiesLight)
    }
    
    fileprivate func isGeographyPresent(item:ClusterItem, array:[ClusterItem]) -> Bool
    {
        let resultArray = array.filter({item.id == $0.id})
        
        return resultArray.count > 0
    }
    
    fileprivate func indexGeographies()
    {
        //Cleanup
        self.sectionedGeographies.removeAll()
        self.sectionTitles.removeAll()
        
        //Obtain the first letters of the array elements
        let firstLetters = self.clusterItems.map({String($0.title.uppercased()[$0.title.uppercased().startIndex])})
        self.sectionTitles = Array(Set(firstLetters))
        
        //Order the characters from A-Z
        var alphabetChars = self.sectionTitles.filter({$0.asciiValue >= 65 && $0.asciiValue <= 91})
        alphabetChars = alphabetChars.sorted(by: {$0 < $1})
        
        //Order all characters different from A-Z
        var nonAlphabetChars = self.sectionTitles.filter({$0.asciiValue < 65 || $0.asciiValue > 91})
        nonAlphabetChars = nonAlphabetChars.sorted(by: {$0 < $1})
        
        //Update the titles used by the section table and the lateral index
        self.sectionTitles.removeAll()
        self.sectionTitles = nonAlphabetChars + alphabetChars
        
        //Use the titles to order the geographies
        for idxLetter in self.sectionTitles
        {
            self.sectionedGeographies.append(self.clusterItems.filter({ String($0.title.uppercased()[$0.title.uppercased().startIndex]) == idxLetter }))
        }
    }
    
    func indexGeographiesForPost()
    {
        //Cleanup
        self.sectionedGeographies.removeAll()
        self.sectionTitles.removeAll()
        
        
        //Obtain the first letters of the array elements
        self.sectionTitles = ["Following".localized(), "Not following".localized()]
        
        var followGeoItems = self.clusterItems.filter({$0.isFollowed == true})
        followGeoItems = followGeoItems.sorted(by: {$0.title < $1.title})
        var notFollowGeoItems = self.clusterItems.filter({$0.isFollowed == false})
        notFollowGeoItems = notFollowGeoItems.sorted(by: {$0.title < $1.title})
        self.sectionedGeographies.append(followGeoItems)
        self.sectionedGeographies.append(notFollowGeoItems)
    }
    
    fileprivate func filterUpdateGeographies(geographies: [ClusterItem]) -> [ClusterItem]
    {
        var items = geographies
        
        for geo in self.geographiesAdded
        {
            items = items.filter({$0.id != geo.id})
        }
        
        return items
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.sectionedGeographies.count
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let lblTitle = SHMSectionTitleLabel()
        lblTitle.backgroundColor = .groupTableViewBackground
        
        //if section == 0 || section == 1
        if section < self.sectionTitles.count
        {
            
            lblTitle.numberOfLines = 1
            let title = NSMutableAttributedString(string: self.sectionTitles[section])
            lblTitle.attributedText = title
        }
        else
        {
            lblTitle.text = ""
        }
        
        return lblTitle
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.sectionedGeographies[section].count
    }

    override func sectionIndexTitles(for tableView: UITableView) -> [String]?
    {
        return self.ownerModule != .newPost ? sectionTitles : nil
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        let isDetail = self.ownerModule == .projectDetail || self.ownerModule == .stakeholderDetail
        return isDetail == true ? false : true
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        let itemSelected = self.sectionedGeographies[indexPath.section][indexPath.row]
        var actions = [UITableViewRowAction]()
        
        if itemSelected.isFollowed == true
        {
            //Unfollow action
            let actionUnfollow = UITableViewRowAction(style: .destructive, title: "Unfollow".localized(), handler: { action,  indexPath in
                
                LibraryAPI.shared.geographyBO.unfollowGeography(itemSelected.id, onSuccess: { clusterItem in
                    
                    tableView.setEditing(false, animated: true)
                    
                    //Update Geo List
                    self.refreshTable()
                    self.onDidUpdateGeography?()
                    let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: delayTime) {
                        
                        let info = "Now, you're not following".localized() + " " + itemSelected.title
                        MessageManager.shared.showBar(title: "Success".localized(), subtitle: info, type: .success, fromBottom: false)
                    }
                    
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error", subtitle: "It wasn't possible unfollow this geography. Try again.".localized(), type: .error, fromBottom: false)
                })
            })
            actionUnfollow.backgroundColor = UIColor.redUnfollowAction()
            
            actions.append(actionUnfollow)
        }
        else
        {
            //Follow action
            let actionFollow = UITableViewRowAction(style: .destructive, title: "Follow".localized(), handler: { action,  indexPath in
                
                LibraryAPI.shared.geographyBO.followGeography(itemSelected.id, onSuccess: { clusterItem in
                    
                    tableView.setEditing(false, animated: true)
                    
                    //Update Geo List
                    self.refreshTable()
                    self.onDidUpdateGeography?()
                    
                    let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: delayTime) {
                        
                        let info = "Now, you're following".localized() + " " + itemSelected.title
                        MessageManager.shared.showBar(title: "Success".localized(), subtitle: info, type: .success, fromBottom: false)
                    }
                    
                }, onError: { error in
                    
                    MessageManager.shared.showBar(title: "Error", subtitle: "It wasn't possible follow this geography. Try again.".localized(), type: .error, fromBottom: false)
                })
            })
            actionFollow.backgroundColor = UIColor.greenFollowAction()
            actions.append(actionFollow)
        }
        
        //New post action
        let actionPost = UITableViewRowAction(style: .destructive, title: "Post".localized(), handler: { action,  indexPath in
            
            let vcNewPost = NewPostViewController()
            let navController = NavyController(rootViewController: vcNewPost)
            vcNewPost.geographyTagged = itemSelected
            self.present(navController, animated: true, completion: nil)
        })
        actionPost.backgroundColor = UIColor.blueColorNewPost()
        
        actions.append(actionPost)
        
        return actions
    }
 
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "GeographyTableViewCell") as! GeographyTableViewCell
        //let currentItem = self.clusterItems[indexPath.row]
        let currentItem = self.sectionedGeographies[indexPath.section][indexPath.row]
        
        if searchText != nil {
            let attributedTitle = currentItem.title.mutableAttributed
            highlightCount += attributedTitle.highlightText(searchText!)
            cell.lblTitle.attributedText = attributedTitle
            
            let attributedSubTitle = currentItem.parentTitle.mutableAttributed
            highlightCount += attributedSubTitle.highlightText(searchText!)
            cell.lblSubtitle.attributedText = attributedSubTitle
            
        } else {
            cell.lblTitle.text = currentItem.title
            cell.lblSubtitle.text = currentItem.parentTitle
        }
        
        return cell
    }
    
    //MARK: Table view Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        let itemSelected = self.sectionedGeographies[indexPath.section][indexPath.row]
        if self.ownerModule == .projectAndStakeholderEdition
        {

            guard self.isGeographyPresent(item: itemSelected, array: self.selectedGeographies) == false else {
                
                MessageManager.shared.showBar(title: "This geography has been already included".localized(), subtitle: String(), type: .warning, containsIcon: false, fromBottom: false)
                
                return
            }
            self.onDidSelectGeography?(self.sectionedGeographies[indexPath.section][indexPath.row])
        }
        else if self.ownerModule == .newPost
        {
            
            self.onDidSelectGeography?(itemSelected)
        }
        else
        {
            if DeviceType.IS_ANY_IPAD == false
            {
                let vcGeoDetail:GeographyDetailViewController = UIStoryboard(name: "Geography", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetailViewController") as! GeographyDetailViewController
                vcGeoDetail.geographyId = itemSelected.id
                vcGeoDetail.onDidUpdateGeography = { [weak self] void in
                    
                    self?.refreshTable()
                }
                self.navigationController?.pushViewController(vcGeoDetail, animated: true)
            }
            else
            {
                let vcGeoDetail:GeographyDetail_iPad = UIStoryboard(name: "GeographyPad", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetail_iPad") as! GeographyDetail_iPad
                vcGeoDetail.geographyId = itemSelected.id
                vcGeoDetail.onDidUpdateGeography = { [weak self] void in
                    
                    self?.onDidUpdateGeography?()
                }
                self.navigationController?.pushViewController(vcGeoDetail, animated: true)
            }
            
        }
    }
    
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let lastGeography = self.sectionedGeographies.last?.last
        let currentGeography = self.sectionedGeographies[indexPath.section][indexPath.row]
        if lastGeography?.id == currentGeography.id && (self.geoListMode == .all || self.ownerModule == .newPost)
        {
            guard self.geoQuery.pageNumber < self.geoQuery.totalPages else {
                
                return
            }                        
            self.geoQuery.pageNumber += 1
            self.refreshTable()
        }
        
        // Remove seperator inset
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset))
        {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))
        {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.responds(to: #selector(setter: UIView.layoutMargins))
        {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 65
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 24
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return CGFloat.leastNormalMagnitude
    }
}
