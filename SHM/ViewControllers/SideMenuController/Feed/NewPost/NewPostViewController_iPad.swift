//
//  NewPostViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 11/11/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class NewPostViewController_iPad: NewPostViewController,UIPopoverControllerDelegate, UIPopoverPresentationControllerDelegate
{
    @IBOutlet weak var btnAddAttachment: UIButton!
    @IBOutlet weak var btnConfidentiality: UIButton!
    @IBOutlet weak var btnPost: UIButton!
    @IBOutlet weak var activityIndicator_iPad: UIActivityIndicatorView!
    @IBOutlet weak var collectionAttachments: UICollectionView!
    @IBOutlet weak var constraintBottomTextView: NSLayoutConstraint!
    
    var vcGeoTable: GeoTableViewController! = UIStoryboard(name: "Geography", bundle: nil).instantiateViewController(withIdentifier: "GeoTableViewController") as! GeoTableViewController
  
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        self.loadIpadConfig()
        self.enableSpecialTextRecognizer()
        self.placeholder(true)
        loadStakeholders()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews()
    {
        //This just override the parent function without any implementation
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        //This just override the parent function without any implementation
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReloadNewPostHome"), object: nil)
    }

    //MARK: LOAD CONFIG
    func loadIpadConfig()
    {
        self.localize()
        
        tvText.delegate = self
        tvText.layer.cornerRadius = 3
        tvText.clipsToBounds = true
        
        //Notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadListsInBackground), name: NSNotification.Name(rawValue: "ReloadNewPostHome"), object: nil)

        //Round buttons
        self.btnAddAttachment.titleLabel?.adjustsFontSizeToFitWidth = true
        self.btnAddAttachment.layer.cornerRadius = 2
        self.btnAddAttachment.clipsToBounds = true
        self.btnAddAttachment.addTarget(self, action: #selector(NewPostViewController.addAttachment), for: .touchUpInside)
        
        self.btnConfidentiality.titleLabel?.adjustsFontSizeToFitWidth = true
        self.btnConfidentiality.layer.cornerRadius = 2
        self.btnConfidentiality.clipsToBounds = true
        self.btnConfidentiality.addTarget(self, action: #selector(NewPostViewController.visibilityPost), for: .touchUpInside)
        self.btnConfidentiality.setTitle("Public".localized(), for: UIControlState())
        self.btnConfidentiality.tag = 1
        
        self.btnPost.setTitle("POST\nNOW".localized(), for: UIControlState())
        self.btnPost.titleLabel?.textAlignment = .center
        self.btnPost.titleLabel?.numberOfLines = 2
        self.btnPost.titleLabel?.adjustsFontSizeToFitWidth = true
        self.btnPost.layer.cornerRadius = 2
        self.btnPost.clipsToBounds = true
        
        //Toolbar for Keyboard
        let toolbarKey: UIToolbar! = UIToolbar()
        toolbarKey.barTintColor = UIColor.whiteCloud()
        toolbarKey.tintColor = UIColor.blackAsfalto()
        
        //Buttons
        btnAt = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        btnAt.setTitle("＠", for: UIControlState())
        btnAt.setTitleColor(UIColor.blackAsfalto(), for: UIControlState())
        btnAt.titleLabel!.font = UIFont.systemFont(ofSize: 30)
        btnAt.layer.borderColor = UIColor.blackAsfalto().cgColor
        btnAt.layer.borderWidth = 1
        btnAt.layer.cornerRadius = 3
        btnAt.addTarget(self, action: #selector(NewPostViewController.addAt), for: .touchUpInside)
        
        btnCat = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        btnCat.setTitle("#", for: UIControlState())
        btnCat.setTitleColor(UIColor.blackAsfalto(), for: UIControlState())
        btnCat.titleLabel!.font = UIFont.systemFont(ofSize: 30)
        btnCat.layer.borderColor = UIColor.blackAsfalto().cgColor
        btnCat.layer.borderWidth = 1
        btnCat.layer.cornerRadius = 3
        btnCat.addTarget(self, action: #selector(NewPostViewController.addHashtag), for: .touchUpInside)
        
        let btnPercent = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        btnPercent.setTitle("%", for: UIControlState())
        btnPercent.setTitleColor(UIColor.blackAsfalto(), for: UIControlState())
        btnPercent.titleLabel!.font = UIFont.systemFont(ofSize: 30)
        btnPercent.layer.borderColor = UIColor.blackAsfalto().cgColor
        btnPercent.layer.borderWidth = 1
        btnPercent.layer.cornerRadius = 3
        btnPercent.addTarget(self, action: #selector(NewPostViewController.addEvent), for: .touchUpInside)
        
        let btnGeography = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        btnGeography.setTitle("!", for: UIControlState())
        btnGeography.setTitleColor(UIColor.blackAsfalto(), for: UIControlState())
        btnGeography.titleLabel!.font = UIFont.systemFont(ofSize: 30)
        btnGeography.layer.borderColor = UIColor.blackAsfalto().cgColor
        btnGeography.layer.borderWidth = 1
        btnGeography.layer.cornerRadius = 3
        btnGeography.addTarget(self, action: #selector(self.addGeography), for: UIControlEvents.touchUpInside)
        
        btnPrivacy = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        btnPrivacy.setImage(UIImage(named: "iconPublicBlack"), for: UIControlState())
        btnPrivacy.titleLabel!.font = UIFont.systemFont(ofSize: 30)
        btnPrivacy.layer.borderColor = UIColor.blackAsfalto().cgColor
        btnPrivacy.layer.borderWidth = 1
        btnPrivacy.layer.cornerRadius = 3
        btnPrivacy.addTarget(self, action: #selector(self.visibilityPost), for: .touchUpInside)
        btnPrivacy.tag = 1
        
        btnAttach = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        btnAttach.setImage(UIImage(named: "iconAttach"), for: UIControlState())
        btnAttach.titleLabel!.font = UIFont.systemFont(ofSize: 30)
        btnAttach.layer.borderColor = UIColor.blackAsfalto().cgColor
        btnAttach.layer.borderWidth = 1
        btnAttach.layer.cornerRadius = 3
        btnAttach.addTarget(self, action: #selector(self.addAttachment), for: .touchUpInside)
        
        //Toolbar items
        let barBtnMention = UIBarButtonItem(customView: btnAt)
        let barBtnHashtag = UIBarButtonItem(customView: btnCat)
        let barBtnEvent = UIBarButtonItem(customView: btnPercent)
        let barBtnGeography = UIBarButtonItem(customView: btnGeography)
        let barBtnAttachment =  UIBarButtonItem(customView: btnAttach)
        let barBtnSecurity =  UIBarButtonItem(customView: btnPrivacy)
        
        toolbarKey.items =
            [
                barBtnMention,
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
                barBtnHashtag,
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
                barBtnEvent,
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
                barBtnGeography,
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
                barBtnSecurity,
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
                barBtnAttachment
        ]
        
        toolbarKey.sizeToFit()
        tvText.inputAccessoryView = toolbarKey
        
        //Collection View (Attachments)
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection =  .horizontal
        layout.minimumInteritemSpacing = 4
        layout.minimumLineSpacing = 4
        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        layout.itemSize = CGSize(width: 50, height: 50)
        
        if let collectionViewAttachments = collectionAttachments {
            collectionViewAttachments.collectionViewLayout = layout
            collectionViewAttachments.dataSource = self
            collectionViewAttachments.delegate = self
            collectionViewAttachments.allowsMultipleSelection = false
            collectionViewAttachments.register(UINib(nibName: "AttachmentCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
            collectionViewAttachments.backgroundColor = UIColor.clear
        }
        
        //Popovers & content
        vcStakeholdersTable.isSearchingSH = true
        vcStakeholdersTable.willBeShowedInNewPost = true
        vcStakeholdersTable.modalPresentationStyle = .popover
        vcStakeholdersTable.preferredContentSize = CGSize(width: 280,height: 280)
        vcStakeholdersTable.onModalViewControllerClosed = {
                
                self.tvText.resignFirstResponder()
                self.tvText.becomeFirstResponder()
        }
        
        vcProjectTable.willBeShowedInNewPost = true
        vcProjectTable.modalPresentationStyle = .popover
        vcProjectTable.preferredContentSize = CGSize(width: 280,height: 280)
        vcProjectTable.onModalViewControllerClosed = {
            
            self.tvText.resignFirstResponder()
            self.tvText.becomeFirstResponder()
        }
        
        self.vcCalendar = CalendarPickerViewController()
        self.vcCalendar.modalPresentationStyle = .popover
        self.vcCalendar.preferredContentSize = CGSize(width: 280,height: 280)
        //Geography Table
        self.vcGeoTable.modalPresentationStyle = .popover
        self.vcGeoTable.preferredContentSize = CGSize(width: 280,height: 280)
        
        
        
        //Calendar PIcker
        view.addSubview(vcCalendar.view)
        vcCalendar.view.isHidden = true
        
        //Load functionality
        enableSpecialTextRecognizer()
    }
   
    fileprivate func localize()
    {
        self.btnAddAttachment.setTitle("Attach File".localized(), for: UIControlState())
        
        self.btnConfidentiality.setTitle("Confidential".localized(), for: UIControlState())
        
        self.btnPost.setTitle("POST \\nNOW".localized(), for: UIControlState())
        
    }
    //MARK: RELOAD DATA IN BACKGROUND
    func reloadListsInBackground()
    {
        loadStakeholders()
    }
    
    //MARK: ACTIONS
    @IBAction func touchBtnPost(_ sender: AnyObject)
    {
        tvText.resignFirstResponder()
        savePost()
    }
    
    override func savePost()
    {
        guard arrayStakeholdersAdded.count > 0 || arrayProjectsAdded.count > 0 || arrayDateAdded.count > 0 || arrayGeographiesAdded.count > 0  else {
            
            MessageManager.shared.showBar(title: "Info".localized(),
                subtitle: "It's necessary include a mention (stakeholder / project / geography) or a date".localized(),
                type: .info,
                fromBottom: false)
            
            return
        }
        
        //Indicator
        self.showActivityIndicator(true)
        
        LibraryAPI.shared.feedBO.publicNewPost(parameters: createRequest(), onSuccess: {
            () -> () in
            
            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                self.showActivityIndicator(false)
                
                MessageManager.shared.showBar(title: "Success".localized(),
                    subtitle:"The post's been created".localized(),
                    type: .success,
                    fromBottom: false)
                
                self.reset()
            }
            
            }) { error in
                
                let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    
                    self.showActivityIndicator(false)
                    
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "There was a problem. The new post hasn't been posted".localized(),
                        type: .error,
                        fromBottom: false)
                }
        }
    }
    
    func reset()
    {
        //Initial State
        tvText.text = String()
        self.placeholder(true)
        
        if isPrivate == true
        {
            btnConfidentiality.tag = 1
            isPrivate = false
            btnConfidentiality.setTitle("Public".localized(), for: UIControlState())
        }
        
        arrayStakeholdersAdded = []
        arrayProjectsAdded = []
        arrayProjectsAdded = []
        arrayDateAdded = []
        arrayAttachments = []
        collectionAttachments.reloadData()
        
        if let vcClosed = onViewControllerClosed
        {
            vcClosed()
        }
    }
    
    override func visibilityPost()
    {
        btnConfidentiality.tag = (btnConfidentiality.tag == 1) ? 0 : 1
        isPrivate = (btnConfidentiality.tag == 1) ? false : true
        
        let iconName = (btnConfidentiality.tag == 0) ? "iconPrivate" : "iconPublic"
//        let imageIcon: UIImage! =  UIImage(named: iconName)
        
        if btnConfidentiality.tag == 0
        {
            btnConfidentiality.setTitle("Private".localized(), for: UIControlState())
            
//            MessageManager.shared.showBar(title: "Status".localized(),
//                subtitle: "Private Post".localized(),
//                backgroundColor: UIColor.blackAsfalto(),
//                textColor: UIColor.white,
//                icon: imageIcon)
        }
        else
        {
            btnConfidentiality.setTitle("Public".localized(), for: UIControlState())
            
//            MessageManager.shared.showBar(title: "Status".localized(),
//                subtitle: "Public Post".localized(),
//                backgroundColor: UIColor.blackAsfalto(),
//                textColor: UIColor.white,
//                icon: imageIcon)
        }
    }
    
    override func addAttachment()
    {
        let alertSheet = UIAlertController(title:nil, message:nil, preferredStyle: UIAlertControllerStyle.alert)
        alertSheet.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler:nil))
        
        alertSheet.addAction(UIAlertAction(title: "Take Photo".localized(), style: .default, handler: {
            (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera
                imagePicker.allowsEditing = false
                imagePicker.modalPresentationStyle = .overCurrentContext
                
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        alertSheet.addAction(UIAlertAction(title: "Choose Existing".localized(), style: .default, handler: {
            (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
                
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary
                imagePicker.allowsEditing = false
                imagePicker.modalPresentationStyle = .popover
                imagePicker.preferredContentSize = CGSize(width: 320, height: 215)
                
                let popoverPhotoLibrary = UIPopoverController(contentViewController: imagePicker)
                popoverPhotoLibrary.delegate = self
                popoverPhotoLibrary.present(from: self.btnAddAttachment.frame, in: self.view, permittedArrowDirections: .any, animated: true)
            }
        }))
        self.present(alertSheet, animated: true, completion: nil)
    }
    //MARK: UTILITIES
    override func suddenDeletionCheck(_ text: String)
    {
        if text.isEmpty == true
        {
            print("sudden deletion")
            
            //Remove stakeholders selected and restore arrayStakeholders
            while arrayStakeholdersAdded.count > 0
            {
                if let stakeholderAdded = arrayStakeholdersAdded.last {
                    
                    filterStakeholders_Restore(stakeholderAdded.0)
                    arrayStakeholdersAdded.removeLast()
                }
            }
            
            //Remove Projects selected and restore arrayStakeholders
            while arrayProjectsAdded.count > 0
            {
                if let projectAdded = arrayProjectsAdded.last {
                    
                    filterProjects_Restore(projectAdded.0)
                    arrayProjectsAdded.removeLast()
                }
            }
            
            //Remove Events if any
            while arrayDateAdded.count > 0
            {
                arrayDateAdded.removeLast()
            }
            
            //Remove Geographies
            self.vcGeoTable.geographiesAdded.removeAll()
        }
    }
    
    //MARK: STYLE FUNCTIONS
    func showActivityIndicator(_ willShow: Bool)
    {
        if willShow == true
        {
            self.btnPost.setTitle("", for: UIControlState())
            self.activityIndicator_iPad.startAnimating()
            self.btnPost.isEnabled = false
        }
        else
        {
            self.activityIndicator_iPad.stopAnimating()
            self.btnPost.isEnabled = true
            self.btnPost.setTitle("POST\nNOW".localized(), for: UIControlState())
        }
    }
    
    func placeholder(_ yesNo:Bool)
    {
        if yesNo == true
        {
            let attributedString = NSMutableAttributedString(string:"Post something about @stakeholders #projects !geographies and %dates".localized())
            attributedString.setPlaceholderHomePost(["@stakeholders".localized(), "#projects".localized(), "!geographies".localized(), "%dates".localized()])
            tvText.attributedText = attributedString
            tvText.hasPlaceholder = yesNo
        }
        else
        {
            tvText.text = String()
            tvText.attributedText = nil
            tvText.hasPlaceholder = yesNo
        }
    }
    
    // MARK: LOAD FUNCTIONALITY
    override func enableSpecialTextRecognizer()
    {
        typeMentionsAndHashtags = WordManager(text: "") {
            (text, magicWord, charRange, searchWordRange) in
            
            if text.characters.count == 0
            {
                self.currentMentionChar = magicWord.rawValue
                self.currentMentionCharRange = charRange
            }
            
            if magicWord == SpecialWord.Mention
            {
                let arrayStakeHoldersFilter: [Stakeholder]!
                
                if text == ""
                {
                    arrayStakeHoldersFilter = self.arrayStakeholders
                }
                else
                {
                    arrayStakeHoldersFilter = self.arrayStakeholders.filter({
                        
                        ($0.fullName as NSString).localizedCaseInsensitiveContains(text) || ($0.aliasString as NSString).localizedCaseInsensitiveContains(text)
                    })
                }
                

                //Present or not Popover
                if arrayStakeHoldersFilter.count > 0
                {
                    self.presentStakeholderPopover()
                }
                else
                {
                    self.dismissStakeholderPopover()
                }
                
                
                //Filter Table
                self.vcStakeholdersTable.loadStakeholders(arrayStakeHoldersFilter)
                if let stakeholdersResultTable = self.vcStakeholdersTable.tvResults {
                
                    stakeholdersResultTable.reloadData()
                }
                self.vcStakeholdersTable.addContactToInstance = {
                    (contact) in
                    
                    //Setting Text
                    //rangeOfText.lowerBound = <#T##Collection corresponding to your index##Collection#>.index(rangeOfText.lowerBound, offsetBy: -1)
                    print(contact.fullName)
                    let replacementString = contact.fullName + ", "
                    let finalText: String = self.tvText.text.replacingCharacters(in: searchWordRange, with: replacementString)
                    
                    //Verify Number of characters
                    if finalText.characters.count > self.limitOfChracters
                    {
                        MessageManager.shared.showBar(title: "Warning".localized(),
                            subtitle: "You have exceeded the limit of characters".localized(),
                            type: .warning,
                            fromBottom: false)
                    }
                    else
                    {
                        let rangeOfFullName = (finalText.range(of: contact.fullName, options:NSString.CompareOptions.backwards)?.lowerBound)!..<finalText.index((finalText.endIndex), offsetBy: -3)
                        
                        self.tvText.text = finalText
                        
                        let tupleStakeholder = (stakeholder: contact as! Stakeholder, range: rangeOfFullName)
                        self.arrayStakeholdersAdded.append(tupleStakeholder)
                        self.filterStakeholders_Remove()
                        
                        self.textViewDidChange(self.tvText)

                        self.dismissStakeholderPopover()
                        self.perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self.tvText, afterDelay: 0.1)
                    }
                }
            }
            else if magicWord == SpecialWord.Hashtag
            {
                let arrayProjects: [Project]!
                
                if text == ""
                {
                    arrayProjects = self.arrayProjects
                }
                else
                {
                    arrayProjects =  self.arrayProjects.filter({
                        ($0.title as NSString).localizedCaseInsensitiveContains(text)
                    })
                }
                
                //Present or not Popover
                if arrayProjects.count > 0
                {
                    self.presentProjectsPopover()
                }
                else
                {
                    self.dismissProjectsPopover()
                }
                
                
                //Filter Table
                self.vcProjectTable.loadProjects(arrayProjects)
                if let projectsResultTable = self.vcProjectTable.tvResults {
                    
                    projectsResultTable.reloadData()
                }
                self.vcProjectTable.onProjectSelected = {
                    (selectedProject) in
                    
                    //Setting Text
                    //rangeOfText.lowerBound = <#T##Collection corresponding to your index##Collection#>.index(rangeOfText.lowerBound, offsetBy: -1)
                    
                    let finalText: String! = self.tvText.text.replacingCharacters(in: searchWordRange, with:"\(selectedProject.title), ")
                    
                    //Verify Number of characters
                    if finalText.characters.count > self.limitOfChracters
                    {
                        MessageManager.shared.showBar(title: "Warning".localized(),
                            subtitle: "You have exceeded the limit of characters".localized(),
                            type: .warning,
                            fromBottom: false)
                    }
                    else
                    {
                        let rangeOfTitle = (finalText.range(of: selectedProject.title, options:NSString.CompareOptions.backwards)?.lowerBound)!..<finalText.index((finalText.endIndex), offsetBy: -3)
                        
                        self.tvText.text = finalText
                        
                        let tupleProject = (project: selectedProject, range: rangeOfTitle)
                        self.arrayProjectsAdded.append(tupleProject)
                        self.filterProjects_Remove()
                        
                        self.textViewDidChange(self.tvText)
                        
                        self.dismissProjectsPopover()
                        self.perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self.tvText, afterDelay: 0.1)
                    }
                }
            }
            else if magicWord == SpecialWord.CalendarDate
            {
                //Present or not Popover
                if text.characters.count == 0
                {
                    self.presentDatePopover()
                }
                else
                {
                    self.dismissDatePopover()
                }
                

                self.vcCalendar.onDateSelected = {
                    (date) in

                    //Setting Text
                    let dateString: String! = (date.allDay) ? date.date.getDateStringForPost() : "\(date.date.getDateTimeStringForPost())"
                    let replacementString = dateString + ", "
                    let finalText: String! = self.tvText.text.replacingCharacters(in: searchWordRange, with: replacementString)
                    
                    //Verify Number of characters
                    if finalText.characters.count > self.limitOfChracters
                    {
                        MessageManager.shared.showBar(title: "Warning".localized(),
                            subtitle: "You have exceeded the limit of characters".localized(),
                            type: .warning,
                            fromBottom: false)
                    }
                    else
                    {
                        //Avoiding duplicated dates
                        for event in self.arrayDateAdded
                        {
                            let eventString: String! = event.0.allDay ? event.0.date.getDateStringForPost() : "\(event.0.date.getDateTimeStringForPost())"
                            
                            if eventString == dateString
                            {
                                MessageManager.shared.showBar(title: "Info".localized(),
                                    subtitle:"This date already has been selected".localized(),
                                    type: .info,
                                    fromBottom: false)
                                
                                self.textViewDidChange(self.tvText)
                                self.perform(#selector(UITextViewDelegate.textViewDidChange(_:)), with: self.tvText, afterDelay: 0.1)
                                
                                return
                            }
                        }
                        
                        let rangeOfDate = (finalText.range(of: dateString, options:NSString.CompareOptions.backwards)?.lowerBound)!..<finalText.index((finalText.endIndex), offsetBy: -3)
                        
                        self.tvText.text = finalText
                        
                        let tupleEvent = (date: date, range: rangeOfDate)
                        self.arrayDateAdded.append(tupleEvent)
                        
                        self.textViewDidChange(self.tvText)
                        self.perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self.tvText, afterDelay: 0.1)
                        self.dismissDatePopover()
                    }
                }
            }
            else if magicWord == SpecialWord.Geography
            {
                guard text.characters.first != " " else {
                    
                    self.dismissGeographyPopover()
                    return
                }
                self.vcGeoTable.geoQuery.getAllWithPagination()
                if text == ""
                {
                    self.vcGeoTable.geoQuery.searchText = "A"
                }
                else
                {
                    self.vcGeoTable.clusterItems.removeAll()
                    self.vcGeoTable.geoQuery.searchText = text
                }
                self.activityIndicator.startAnimating()
                self.navigationItem.rightBarButtonItem = self.barBtnActivity
                self.vcGeoTable.geoListMode = .all
                self.vcGeoTable.ownerModule = .newPost
                self.presentGeographyPopover()
                self.vcGeoTable.refreshTable()
                
                self.vcGeoTable.onDidLoadGeographies = {[weak self] numberOfitems in
                    
                    //UI-> Update visibility of table and focus textView
                    self?.activityIndicator.stopAnimating()
                    self?.navigationItem.rightBarButtonItem = self?.barBtnDone
                }
                self.vcGeoTable.onDidSelectGeography = {[weak self] geography in
                    
                    let finalText: String! = self?.tvText.text.replacingCharacters(in: searchWordRange, with:"\(geography.titleComplete), ")
                    
                    //Verify Number of characters
                    if finalText.characters.count > (self?.limitOfChracters) ?? 0
                    {
                        MessageManager.shared.showBar(title: "Warning".localized(),
                                                      subtitle: "You have exceeded the limit of characters".localized(),
                                                      type: .warning,
                                                      fromBottom: false)
                    }
                    else
                    {
                        let rangeOfTitle = (finalText.range(of: geography.titleComplete, options:NSString.CompareOptions.backwards)?.lowerBound)!..<finalText.index((finalText.endIndex), offsetBy: -3)
                        
                        self?.tvText.text = finalText
                        
                        let tupleGeography = (geography: geography, range: rangeOfTitle)
                        self?.arrayGeographiesAdded.append(tupleGeography)
                        self?.vcGeoTable.geographiesAdded.append(geography)
                        self?.textViewDidChange((self?.tvText)!)
                        
                        self?.dismissGeographyPopover()
                        self?.perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self?.tvText, afterDelay: 0.1)
                    }
                }
            }
        }
    }
    
    // MARK: IMAGE PICKER DELEGATE
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        dismiss(animated: true) { () -> Void in
            
            if let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
            {
                self.btnAddAttachment.isEnabled = false
                self.btnAddAttachment.alpha = 0.5
                
                //Create Request Object
                var wsObject: [String : Any]  = [:]
                wsObject["OldFile"] = String()
                wsObject["VersionId"] = 1
                
                var imageData: Data?
                var validImage: UIImage?
                
                if selectedImage.size.width > 500 {
                    validImage = selectedImage.resizeToWidth(500.0)
                }
                else {
                    validImage = selectedImage
                }
                
                imageData = UIImagePNGRepresentation(validImage!)
                
                self.uploadCount += 1
                let localUrl = validImage!.saveToTempDirectory("mobileUpload\(self.uploadCount).png")
                
                LibraryAPI.shared.attachmentBO.uploadFile(fileSize: imageData!.count, fileURL: localUrl, onSuccess: {
                    (token) in
                    
                    self.btnAddAttachment.isEnabled = true
                    self.btnAddAttachment.alpha = 1
                    
                    let attachPicture = AttachmentWithToken(token: token, image: validImage!)
                    self.arrayAttachments.append(attachPicture)
                    self.collectionAttachments.reloadData()
                    
                    MessageManager.shared.showStatusBar(title: "Attachment Uploaded".localized(),
                        type: .success,
                        containsIcon: false)
                    
                    }, onError: { error in
                        
                        self.btnAddAttachment.isEnabled = true
                        self.btnAddAttachment.alpha = 1
                        
                        MessageManager.shared.showBar(title: "Error",
                            subtitle: "There was an error trying to save an attachment".localized(),
                            type: .error,
                            fromBottom: false)
                })
            }
        }
    }
    
    override func image(_ image: UIImage, didFinishSavingWithError error: NSErrorPointer?, contextInfo:UnsafeRawPointer)
    {
        if error != nil
        {
            let alert = UIAlertController(title: "Save Failed".localized(),
                                          message: "Failed to save image".localized(),
                                          preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "OK".localized(),  style: .cancel, handler: nil)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: { () -> Void in
                self.positionForAtacchments()
            })
        }
    }
    
    // MARK: CollectionView Methods
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        constraintBottomTextView.constant = (arrayAttachments.count > 0) ? 50 : 8
        return arrayAttachments.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! AttachmentCell
        
        //Frame
        let cell_origin_X = cell.frame.origin.x
        cell.frame = CGRect(x: cell_origin_X, y: 1, width: 48, height: 48)
        
        cell.imageAttachment.image = arrayAttachments[indexPath.row].picture
        cell.btnDelete.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
            
            self.arrayAttachments = self.arrayAttachments.filter({ $0 !== self.arrayAttachments[indexPath.row] })
            self.collectionAttachments.reloadData()
            
        }))
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let vcViewer = ImageViewer(image: arrayAttachments[indexPath.row].picture)
        vcViewer.onRemoveThis = {
            (image) in
            self.arrayAttachments = self.arrayAttachments.filter({ $0.picture !== image })
            self.collectionAttachments.reloadData()
        }
        
        let navController = UINavigationController(rootViewController: vcViewer)
        navController.modalPresentationStyle = UIModalPresentationStyle.formSheet
        vcViewer.view.backgroundColor = UIColor.black
        
        present(navController, animated: true, completion: nil)
    }
    
    //MARK: POPOVER'S PRESENTATION AND DISMISSAL FUNCTIONS
    func presentStakeholderPopover()
    {
        if let currentPopover = self.presentedViewController {
            
            currentPopover.dismiss(animated: true, completion: {
                
                self.presentStakeholderTVC()
            })
        }
        else
        {
            self.presentStakeholderTVC()
        }
    }
    private func presentStakeholderTVC()
    {
        self.present(self.vcStakeholdersTable, animated: true, completion: nil)
        let stakeholderPopover = self.vcStakeholdersTable.popoverPresentationController
        stakeholderPopover?.permittedArrowDirections = .any
        stakeholderPopover?.sourceView = self.view
        stakeholderPopover?.sourceRect = self.tvText.frame
        stakeholderPopover?.backgroundColor = UIColor.grayCloudy()
    }
    
    func dismissStakeholderPopover()
    {
        self.vcStakeholdersTable.dismiss(animated: true, completion: nil)
    }
    
    func presentProjectsPopover()
    {
        if let currentPopover = self.presentedViewController {
            
            currentPopover.dismiss(animated: true, completion: {
                
                self.presentProjectsTVC()
            })
        }
        else
        {
            self.presentProjectsTVC()
        }
    }
    
    private func presentProjectsTVC()
    {
        self.present(self.vcProjectTable, animated: true, completion: nil)
        let projectsPopover = self.vcProjectTable.popoverPresentationController
        projectsPopover?.permittedArrowDirections = .any
        projectsPopover?.sourceView = self.view
        projectsPopover?.sourceRect = self.tvText.frame
        projectsPopover?.backgroundColor = UIColor.grayCloudy()

    }
    
    func dismissProjectsPopover()
    {
        self.vcProjectTable.dismiss(animated: true, completion: nil)
    }
    
    
    func presentGeographyPopover()
    {
        if let currentPopover = self.presentedViewController {
            
            currentPopover.dismiss(animated: true, completion: {
                
                self.presentGeographyTVC()
            })
        }
        else
        {
            self.presentGeographyTVC()
        }
    }
    
    private func presentGeographyTVC()
    {
        self.present(self.vcGeoTable, animated: true, completion: nil)
        let projectsPopover = self.vcGeoTable.popoverPresentationController
        projectsPopover?.permittedArrowDirections = .any
        projectsPopover?.sourceView = self.view
        projectsPopover?.sourceRect = self.tvText.frame
        projectsPopover?.backgroundColor = UIColor.grayCloudy()
    }
    
    func dismissGeographyPopover()
    {
        self.vcGeoTable.dismiss(animated: true, completion: nil)
    }
    
    func presentDatePopover()
    {
        if let currentPopover = self.presentedViewController {
            
            currentPopover.dismiss(animated: true, completion: {
                
                self.presentDateTVC()
            })
        }
        else
        {
            self.presentDateTVC()
        }
    }
    
    private func presentDateTVC()
    {
        self.vcCalendar.view.isHidden = false
        self.present(self.vcCalendar, animated: true, completion: nil)
        let datePopover = self.vcCalendar.popoverPresentationController
        datePopover?.permittedArrowDirections = .any
        datePopover?.sourceView = self.view
        datePopover?.sourceRect = self.tvText.frame
        datePopover?.backgroundColor = UIColor.grayCloudy()
    }
    
    func dismissDatePopover()
    {
        self.vcCalendar.dismiss(animated: true, completion: nil)
    }

    
    // MARK: TEXTVIEW DELEGATE
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if tvText.hasPlaceholder == true
        {
            self.placeholder(false)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if tvText.text.trim().isEmpty
        {
            self.placeholder(true)
        }
    }
    
    override func textViewDidChange(_ textView: UITextView)
    {
        //Set Style
        let attributedString = NSMutableAttributedString(string:textView.text)
        attributedString.setColorAndBold(arrayStakeholdersAdded, projects: arrayProjectsAdded, geographies: arrayGeographiesAdded, events: arrayDateAdded)
        textView.attributedText = attributedString
        
        //Check for sudden deletion
        self.suddenDeletionCheck(textView.text.trim())
        
        //Verify Mentions added
        var arrayStakeholderUpdated = arrayStakeholdersAdded
        for tuple in arrayStakeholdersAdded
        {
            //Remove it!
            if textView.text.containsString(tuple.0.fullName) == false
            {
                var rangeWithoutTheLastCharacter = tuple.1
                //rangeWithoutTheLastCharacter.upperBound = <#T##Collection corresponding to your index##Collection#>.index(rangeWithoutTheLastCharacter.upperBound, offsetBy: -1)
                
                if  rangeWithoutTheLastCharacter.upperBound <= textView.text.endIndex 
                {
                    let finalText: String! = textView.text.replacingCharacters(in: rangeWithoutTheLastCharacter, with:"")
                    //Refresh Style
                    let attributedString = NSMutableAttributedString(string:finalText)
                    attributedString.setColorAndBold(arrayStakeholdersAdded, projects: arrayProjectsAdded, geographies: arrayGeographiesAdded, events: arrayDateAdded)
                    textView.attributedText = attributedString
                }
                
                arrayStakeholderUpdated = arrayStakeholderUpdated.filter({$0.0.id != tuple.0.id})
                filterStakeholders_Restore(tuple.0)
            }
        }
        arrayStakeholdersAdded = arrayStakeholderUpdated
        
        //Verify Projects added
        var arrayProjectsUpdated = arrayProjectsAdded
        for tuple in arrayProjectsAdded
        {
            if textView.text.containsString(tuple.0.title) == false
            {
                //Remove it!
                var rangeWithoutTheLastCharacter = tuple.1
                //rangeWithoutTheLastCharacter.upperBound = <#T##Collection corresponding to your index##Collection#>.index(rangeWithoutTheLastCharacter.upperBound, offsetBy: -1)
                
                if rangeWithoutTheLastCharacter.upperBound <= textView.text.endIndex
                {
                    let finalText: String! = textView.text.replacingCharacters(in: rangeWithoutTheLastCharacter, with:"")
                    //Refresh Style
                    let attributedString = NSMutableAttributedString(string:finalText)
                    attributedString.setColorAndBold(arrayStakeholdersAdded, projects: arrayProjectsAdded, geographies: arrayGeographiesAdded, events: arrayDateAdded)
                    textView.attributedText = attributedString
                }
                arrayProjectsUpdated = arrayProjectsUpdated.filter({$0.0.id != tuple.0.id})
                filterProjects_Restore(tuple.0)
            }
        }
        arrayProjectsAdded = arrayProjectsUpdated
        
        //Verify Geographies added
        var arrayGeographiesUpdated = arrayGeographiesAdded
        let cursorPosition = textView.position(from: textView.beginningOfDocument, offset: (textView.selectedRange.toRange()?.lowerBound)!)
        let offsetPosition = textView.offset(from: textView.beginningOfDocument, to: cursorPosition!)
        print("cursor: \(offsetPosition)")
        for tuple in self.arrayGeographiesAdded
        {
            //Remove it!
            var shouldRemove = false
            if cursorPosition == textView.endOfDocument
            {
                
                shouldRemove = textView.text.containsString(tuple.0.titleComplete) == false || ((textView.text.range(of: textView.text)?.upperBound)! <= tuple.1.upperBound)
            }
            else
            {
                shouldRemove = textView.text.containsString(tuple.0.titleComplete) == false
            }
            if shouldRemove == true
            {
                let rangeWithoutTheLastCharacter = tuple.1
                if  rangeWithoutTheLastCharacter.upperBound <= textView.text.endIndex
                {
                    let finalText: String! = textView.text.replacingCharacters(in: rangeWithoutTheLastCharacter, with:"")
                    //Refresh Style
                    let attributedString = NSMutableAttributedString(string:finalText)
                    attributedString.setColorAndBold(arrayStakeholdersAdded, projects: self.arrayProjectsAdded, geographies: arrayGeographiesAdded, events: self.arrayDateAdded)
                    textView.attributedText = attributedString
                }
                arrayGeographiesUpdated = arrayGeographiesUpdated.filter({$0.0.id != tuple.0.id})
                self.vcGeoTable.geographiesAdded = self.vcGeoTable.geographiesAdded.filter({$0.id != tuple.0.id})
                filterGeographies_Restore(tuple.0)
            }
        }
        arrayGeographiesAdded = arrayGeographiesUpdated
        
        //Verify Events added
        var arrayDateUpdated = arrayDateAdded
        for tuple in arrayDateAdded
        {
            let dateString: String! = (tuple.0.allDay) ? tuple.0.date.getDateStringForPost() : "\(tuple.0.date.getDateTimeStringForPost())"
            
            if textView.text.containsString(dateString) == false
            {
                //Remove it!
                var rangeWithoutTheLastCharacter = tuple.1
                //rangeWithoutTheLastCharacter.upperBound = <#T##Collection corresponding to your index##Collection#>.index(rangeWithoutTheLastCharacter.upperBound, offsetBy: -1)
                
                if rangeWithoutTheLastCharacter.upperBound <= textView.text.endIndex
                {
                    let finalText: String! = textView.text.replacingCharacters(in: rangeWithoutTheLastCharacter,     with:"")
                    //Refresh Style
                    let attributedString = NSMutableAttributedString(string:finalText)
                    attributedString.setColorAndBold(arrayStakeholdersAdded, projects: arrayProjectsAdded, geographies: arrayGeographiesAdded, events: arrayDateAdded)
                    textView.attributedText = attributedString
                }                
                arrayDateUpdated = arrayDateUpdated.filter({$0.0.date != tuple.0.date})
            }
        }
        arrayDateAdded = arrayDateUpdated
        
        //Elements Showed
        if let currentMentionCharRange = currentMentionCharRange {
            
            if textView.attributedText.string.characters.endIndex < currentMentionCharRange.upperBound
                || textView.attributedText.string.substring(with: currentMentionCharRange) != currentMentionChar!
            {
                self.dismissStakeholderPopover()
                self.dismissProjectsPopover()                
                self.dismissDatePopover()
                self.dismissGeographyPopover()
            }
        }
    }
    
    override func textViewDidChangeSelection(_ textView: UITextView)
    {
//        self.processMagicWord(textView.text)
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        self.perform(#selector(self.processMagicWord(_:)), with: textView.text, afterDelay: 1.5)
    }
    
    func processMagicWord(_ text: String)
    {
        typeMentionsAndHashtags.text = text
    }
}

//MARK: Placeholder
extension NSMutableAttributedString
{
    ///Set Bold and color Style for array of Strings
    func setPlaceholderHomePost(_ words: [String]) -> Bool
    {
        let foundRangeGeneral = self.mutableString.range(of: self.string)
        self.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: foundRangeGeneral)
        self.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 13), range: foundRangeGeneral)
        
        for word in words
        {
            let foundRange = self.mutableString.range(of: word)
            
            if foundRange.location != NSNotFound {
                self.addAttribute(NSForegroundColorAttributeName, value: UIColor.blueTag(), range: foundRange)
                self.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 14), range: foundRange)
            }
        }
        return true
    }
 
}
