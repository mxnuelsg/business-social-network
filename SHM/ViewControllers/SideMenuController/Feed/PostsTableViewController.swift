//
//  FeedViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 7/24/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class PostsTableViewController: UITableViewController, UITextViewDelegate, UITabBarDelegate
{
     // MARK: - CONTROLS, OUTLETS & VARIABLES
    @IBOutlet weak var tabOptions: UITabBar!
    @IBOutlet weak var barBtn:UIBarButtonItem!
    
    var activeBlueForIpad = false
    var postSelected: Post!
    var posts = [Post](){
        didSet {
            if let searchText = self.searchText {
                postsFiltered = LibraryAPI.shared.feedBO.filterPosts(posts, byText: searchText)
            }
            else {
                postsFiltered = posts
            }
        }
    }
    var postsFiltered = [Post]()
    var willDisplayLastRow: ((_ row:Int) -> ())?
    var searchText: String?
    
    var canPullToRefresh = true
    
    // MARK: - LIFE CLYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.hideEmtpyCells()
        
        loadConfig()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        backButtonArrow()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
        
    deinit
    {
        
    }
    
     // MARK: INITIAL CONFIG
    func loadConfig()
    {
        /**
        Register Custom Cell
        */
        tableView.register(UINib(nibName: "FeedTableCell", bundle: nil), forCellReuseIdentifier: "FeedTableCell")
    }
    
    // MARK: - PRIVATE METHODS
    func filterByText(_ text: String) {
        guard text.isEmpty == false else {
            if searchText != nil {
                postsFiltered = posts
                searchText = nil
                tableView.reloadData()
            }
            return
        }
        postsFiltered = LibraryAPI.shared.feedBO.filterPosts(posts, byText: text)
        searchText = text.isEmpty ? nil : text
        tableView.reloadData()
    }
    
    func pushStakeholder(_ stakeholder: Stakeholder)
    {
        //Workflow iPad / iPhone
        if DeviceType.IS_ANY_IPAD == true
        {
            let actordetailVC = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
            actordetailVC.stakeholder = stakeholder
            navigationController?.pushViewController(actordetailVC, animated: true)
        }
        else
        {
            let actordetailVC : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
            actordetailVC.stakeholder = stakeholder
            navigationController?.pushViewController(actordetailVC, animated: true)
        }
    }
    
    func reload() {
        tableView.reloadData()
        
        if postsFiltered.count == 0
        {
            displayBackgroundMessage("No posts".localized(),
                                     subMessage: canPullToRefresh ? "Pull to refresh".localized() : "")
            
            return
        }
        dismissBackgroundMessage()
    }
    
    // MARK: TABLE VIEW DATASOURCE
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return postsFiltered.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let post = postsFiltered[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedTableCell") as! FeedTableCell
        
        //Color for selected cell
        let selectedColor = UIView()
        selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
        cell.selectedBackgroundView = selectedColor
        
        cell.tvText.delegate = self
        cell.cellForPost(cell, post: post)
        cell.onStakeholderTap = { [weak self] stakeholder in
            
            //Enabled or Disabled
            guard stakeholder.isEnable == true else {
                
                MessageManager.shared.showBar(title: stakeholder.fullName, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                
                return
            }
            
            self?.pushStakeholder(stakeholder)
        }
       
        cell.onAllStakeholdersTap = { [weak self] Void in
            
            let stakeholdersTableVC = StakeholdersTableViewController.getNewInstance()
            stakeholdersTableVC.stakeholders = post.stakeholders
            
            let vcModal = ModalViewController(subViewController: stakeholdersTableVC)
            self?.present(vcModal, animated:true, completion: nil)
        }
        
        cell.onFilesTap = { [weak self] Void in
            
            let vcFileList = FileListViewController(attachments: post.attachments)
            let vcModal = ModalViewController(subViewController: vcFileList)
            vcModal.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            vcModal.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            
            self?.present(vcModal, animated:true, completion: nil)
        }
        
        cell.onTextViewTap = { [weak self] Void in
            
            if DeviceType.IS_ANY_IPAD == true
            {
                let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController_iPad.self)
                vcDetailPost.post = post
                self?.navigationController?.pushViewController(vcDetailPost, animated: true)
            }
            else
            {
                let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController.self)
                vcDetailPost.post = post
                self?.navigationController?.pushViewController(vcDetailPost, animated: true)
            }
        }
        
        return cell
    }
    
    // MARK: TABLE VIEW DELEGATE
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        
        if DeviceType.IS_ANY_IPAD == true
        {
            let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController_iPad.self)
            vcDetailPost.post = postsFiltered[indexPath.row]
            self.navigationController?.pushViewController(vcDetailPost, animated: true)
        }
        else
        {
            let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController.self)
            vcDetailPost.post = postsFiltered[indexPath.row]
            self.navigationController?.pushViewController(vcDetailPost, animated: true)
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 115
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        //Lateral UI
        cell.backgroundColor = (self.activeBlueForIpad == true) ? UIColor.blueSky() : .white
        
        //fade Animation
        cell.alpha = 0
        UIView.animate(withDuration: 0.5, animations: {
            
            cell.alpha = 1
        })
        
        //Load more
        let cellToDisplay = cell as! FeedTableCell
        cellToDisplay.layoutConfForCell(cellToDisplay)
        
        if indexPath.row == postsFiltered.count - 1
        {
            self.willDisplayLastRow?(indexPath.row)
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return CGFloat.leastNormalMagnitude
    }
    
    // MARK: TEXTVIEW DELEGATE (FeedTableCell)
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
    {
        let arrayUrl = URL.description.components(separatedBy: "_")

        if arrayUrl.count > 1
        {
            let type = arrayUrl[0]
            
            switch type
            {
            case "s":
                //Go to Stakeholder detail
                let stakeholderLinked: Stakeholder! = Stakeholder(isDefault: true)
                stakeholderLinked.id = Int(arrayUrl[1])!
                stakeholderLinked.isEnable = Int(arrayUrl[2])! == 1 ? true : false
                
                let firstname = arrayUrl[3]
                
                //Enabled or Disabled
                guard stakeholderLinked.isEnable == true else {
                
                MessageManager.shared.showBar(title: firstname, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                    
                    return true
                }

                //Workflow iPad / iPhone
                if DeviceType.IS_ANY_IPAD == true
                {
                    let vcDetailStakeholders = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
                    vcDetailStakeholders.stakeholder = stakeholderLinked
                    navigationController?.pushViewController(vcDetailStakeholders, animated: true)
                }
                else
                {
                    let vcDetailStakeholders : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                    vcDetailStakeholders.stakeholder = stakeholderLinked
                    navigationController?.pushViewController(vcDetailStakeholders, animated: true)
                }
            case "p":
                //Go to Project detail
                let projectLinked: Project! = Project(isDefault: true)
                projectLinked.id = Int(arrayUrl[1])!
                projectLinked.isEnable = Int(arrayUrl[2])! == 1 ? true : false
                
                //Enabled or Disabled
                guard projectLinked.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: nil, subtitle: "This project is disabled".localized(), type: .info, fromBottom: false)
                    
                    return true
                }
                
                //Go to detail
                if DeviceType.IS_ANY_IPAD == true
                {
                    let vcDetailProject = Storyboard.getInstanceOf(ProjectDetailViewController_iPad.self)
                    vcDetailProject.project = projectLinked
                    navigationController?.pushViewController(vcDetailProject, animated: true)

                }
                else
                {
                    let vcDetailProject : DetailProjectViewController = Storyboard.getInstanceFromStoryboard("Projects")
                    vcDetailProject.project = projectLinked
                    navigationController?.pushViewController(vcDetailProject, animated: true)
                }

            case "d":
                //Add Event to Calendar
                let alert = UIAlertController(title: "Add Event".localized(), message: "Would you like to save this date in your device calendar".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Save".localized(), style: UIAlertActionStyle.default, handler: {
                    (action) in
                    
                    for thePost in self.postsFiltered where thePost.id == Int(arrayUrl[2])!
                    {
                        for theEvent in thePost.events where theEvent.id == Int(arrayUrl[1])!
                        {
                            CalendarManager.sharedInstance.createEvent(thePost.title.getEditedTitleForPost(thePost, truncate: false), event: theEvent)
                        }
                    }
                    
                }))
                self.present(alert, animated: true, completion: nil)
            case "g":
                let geographyId = Int(arrayUrl[1])!
                if DeviceType.IS_ANY_IPAD == false
                {
                    let vcGeoDetail:GeographyDetailViewController = UIStoryboard(name: "Geography", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetailViewController") as! GeographyDetailViewController
                    vcGeoDetail.geographyId = geographyId
                    self.navigationController?.pushViewController(vcGeoDetail, animated: true)
                }
                else
                {
                    let vcGeoDetail:GeographyDetail_iPad = UIStoryboard(name: "GeographyPad", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetail_iPad") as! GeographyDetail_iPad
                    vcGeoDetail.geographyId = geographyId
                    self.navigationController?.pushViewController(vcGeoDetail, animated: true)
                }
            default:
                break
            }
        }
        return true
    }
    
    // MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        switch(segue.identifier!)
        {
        case "ViewPostDetail":
            let postDetailVC = segue.destination as! PostDetailViewController
            postDetailVC.post = postSelected
            break
        default:
            return
        }
    }
}
