//
//  PostDetailViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/7/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import QuickLook

class PostDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, QLPreviewControllerDataSource
{
    var arraySectionTitles = ["",  "", "In This Post".localized(), "Attachments".localized()]
    var arrayStakeholders = [Stakeholder]()
    var arrayAttachments = [Attachment]()
    var arrayProjects = [Project]()
    var arrayDates = [Event]()
    var author: Member!
    var attachmentSelected: Attachment?
    
    
    var datasource = [SHMSectionDataSource]()
    var post: Post!
    
    @IBOutlet weak var tablePostDetails: UITableView!
    @IBOutlet weak var lblPrivacy: UILabel!
    @IBOutlet weak var constraintDetailTop: NSLayoutConstraint!

    //Tab bar items
    @IBOutlet weak var tabBarItemNewPost: UITabBarItem!
    @IBOutlet weak var tabBarItemStakeholder: UITabBarItem!
    @IBOutlet weak var tabBarItemProjects: UITabBarItem!
    @IBOutlet weak var tabBarItemCalendar: UITabBarItem!
    @IBOutlet weak var tabBarItemSearchFeed: UITabBarItem!
    
     // MARK: - LIFE CLYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        /** Loading message */
        tablePostDetails.hideEmtpyCells()
        tablePostDetails.displayBackgroundMessage("Loading...".localized(),
            subMessage: "")

        loadConfig()
        loadPostDetail()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
        
        //Reset Badge icon
        self.removeBadgeIcon()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        
    }
    
    //MARK: LOAD CONFIG
    func loadConfig()
    {
        //Localizations
        self.localize()
        
        /**
        Navigation Bar
        */
        //Buttons
        let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(self.createNewPost))
        
        //Adding to NavBar
        navigationItem.rightBarButtonItem  = barBtnCompose
        
        /**
        TableView
        */
        tablePostDetails.register(UINib(nibName: "ContactListCell", bundle: nil), forCellReuseIdentifier: "ContactListCell")
        tablePostDetails.register(UINib(nibName: "TextViewTableCell", bundle: nil), forCellReuseIdentifier: "TextViewTableCell")
        
//        tablePostDetails.estimatedRowHeight = 150;
//        tablePostDetails.rowHeight = UITableViewAutomaticDimension;
        
        tablePostDetails.rowHeight = UITableViewAutomaticDimension
        tablePostDetails.estimatedRowHeight = 160

    }
    
    fileprivate func localize()
    {
        title = "POST DETAIL".localized()
        if DeviceType.IS_ANY_IPAD == false
        {
            self.tabBarItemNewPost.title = "New Post".localized()
            self.tabBarItemStakeholder.title = "Stakeholders".localized()
            self.tabBarItemProjects.title = "Projects".localized()
            self.tabBarItemCalendar.title = "Calendar".localized()
            self.tabBarItemSearchFeed.title = "Search Feed".localized()
        }
    }
    
    //MARK: LOAD POST DETAIL
    func loadPostDetail()
    {
        let wsRequest = KeyValueObject()
        wsRequest.key = post.id
        
        LibraryAPI.shared.feedBO.getPostDetail(parameters: wsRequest.getWSObject(), onSuccess: {
            (postDetail) -> () in
            
            self.tablePostDetails.dismissBackgroundMessage()
            
            if let detail = postDetail as Post!
            {
                if detail.isPrivate == true
                {
                    self.showPrivacy()
                }
                
                self.post = detail
                self.arrayStakeholders = self.post.stakeholders
                self.arrayProjects = self.post.projects
                self.arrayDates = self.post.events
                self.arrayAttachments = self.post.attachments
                self.author = self.post.author
                
                let datePost = self.post.createdDate?.getStringStyleMedium()
                
                if let date = datePost as String!
                {
                    self.arraySectionTitles[0] = "Date: ".localized() + date
                }
                else
                {
                    self.arraySectionTitles[0] = "Date: ".localized()
                }
                
                self.buildDataSource()
                self.tablePostDetails.reloadData()
            }
            else
            {
                self.tablePostDetails.displayBackgroundMessage("No results found".localized(),
                    subMessage: "")
                
                _ = self.navigationController?.popViewController(animated: true)
            }
            
            }) { error in
                
                _ = self.navigationController?.popViewController(animated: true)
                
                MessageManager.shared.showBar(title: "Error",
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
        }
    }
    
    //MARK: QUICKLOOK VIEWER
    func showQuickLoock()
    {
        let previewQL = QLPreviewController()
        previewQL.dataSource = self
        previewQL.currentPreviewItemIndex = 0
        
        self.navigationController?.pushViewController(previewQL, animated: true)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
     // MARK: TABLE VIEW DATASOURCE
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let lblSectionTitle = SHMSectionTitleLabel()
        lblSectionTitle.text = datasource[section].sectionTitle
        lblSectionTitle.backgroundColor = UIColor.whiteColor(0.95)
        lblSectionTitle.textColor = UIColor.grayCloudy()
        lblSectionTitle.font = UIFont.boldSystemFont(ofSize: 12)
        
        return lblSectionTitle
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return (section == 1 || section == 0) ? CGFloat.leastNormalMagnitude : 22.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       return datasource[section].rowsInSection!()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        return datasource[indexPath.section].cellForRow(indexPath.row)
    }
    
     // MARK: TABLE VIEW DELEGATE
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
         tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return CGFloat.leastNormalMagnitude
    }
    
    
    // MARK: TEXTVIEW DELEGATE (FeedTableCell)
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool
    {
        let arrayUrl = URL.description.components(separatedBy: "_")
        
        if arrayUrl.count > 1
        {
            let type = arrayUrl[0]
            
            switch type
            {
            case "s":
                //Go to Stakeholder detail
                let stakeholderLinked: Stakeholder! = Stakeholder(isDefault: true)
                stakeholderLinked.id = Int(arrayUrl[1])!
                stakeholderLinked.isEnable = Int(arrayUrl[2])! == 1 ? true : false
                
                let firstname = arrayUrl[3]
                
                //Enabled or Disabled
                guard stakeholderLinked.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: firstname, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                    
                    return true
                }
                
                let vcDetailStakeholders : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                vcDetailStakeholders.stakeholder = stakeholderLinked
                navigationController?.pushViewController(vcDetailStakeholders, animated: true)
            case "p":
                //Go to Project detail
                let projectLinked: Project! = Project(isDefault: true)
                projectLinked.id = Int(arrayUrl[1])!
                projectLinked.isEnable = Int(arrayUrl[2])! == 1 ? true : false
                
                //Enabled or Disabled
                guard projectLinked.isEnable == true else {
                    
                    MessageManager.shared.showBar(title: nil, subtitle: "This project is disabled".localized(), type: .info, fromBottom: false)
                    
                    return true
                }
                
                //Go to detail
                let vcDetailProject : DetailProjectViewController = Storyboard.getInstanceFromStoryboard("Projects")
                vcDetailProject.project = projectLinked
                navigationController?.pushViewController(vcDetailProject, animated: true)
                
            case "d":
                //Add Event to Calendar
                let alert = UIAlertController(title: "Add Event".localized(), message: "Would you like to save this date in your device calendar".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "Save".localized(), style: UIAlertActionStyle.default, handler: {
                    (action) in
                    
                    for theEvent in self.post.events where theEvent.id == Int(arrayUrl[1])!
                    {
                            CalendarManager.sharedInstance.createEvent(self.post.title.getEditedTitleForPost(self.post, truncate: false), event: theEvent)
                    }
                    
                }))
                self.present(alert, animated: true, completion: nil)
            case "g":
                let geographyId = Int(arrayUrl[1])!
                if DeviceType.IS_ANY_IPAD == false
                {
                    let vcGeoDetail:GeographyDetailViewController = UIStoryboard(name: "Geography", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetailViewController") as! GeographyDetailViewController
                    vcGeoDetail.geographyId = geographyId
                    self.navigationController?.pushViewController(vcGeoDetail, animated: true)
                }
                else
                {
                    let vcGeoDetail:GeographyDetail_iPad = UIStoryboard(name: "GeographyPad", bundle: nil).instantiateViewController(withIdentifier: "GeographyDetail_iPad") as! GeographyDetail_iPad
                    vcGeoDetail.geographyId = geographyId
                    self.navigationController?.pushViewController(vcGeoDetail, animated: true)
                }

            default:
                break
            }
        }
        
        return true
    }
    
    // MARK: TABBAR DELEGATE (FeedTableCell)
    func tabBar(_ tabBar: UITabBar, didSelectItem item: UITabBarItem)
    {
        //Selected item color equal not selected color
        tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        
        switch item.tag
        {
        case 1:
            createNewPost()
        case 2:
            goToStakeHolders()
        case 3:
            goToProyects()
        case 4:
            goToCalendar()
        case 5:
            goToSearch()
        default:
            break
        }
    }
    
    //MARK: ACTIONS
    func showPrivacy()
    {
        self.lblPrivacy.isHidden = false
        self.lblPrivacy.alpha = 0
        
        UIView.animate(withDuration: 0.7, animations: {
            
            self.lblPrivacy.alpha = 1
            
            }, completion: { finished in
                
                self.lblPrivacy.text = "Private Post".localized()
        })
        
        self.constraintDetailTop.constant = self.lblPrivacy.frame.height
    }
    
    // MARK: ACTIONS TAB BAR MENU
    func createNewPost()
    {
        let vcNewPost = NewPostViewController()
        
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = (DeviceType.IS_ANY_IPAD) ?  UIModalPresentationStyle.formSheet :  UIModalPresentationStyle.overFullScreen
        
        present(navController, animated: true, completion: nil)
    }
    
    func goToStakeHolders()
    {
        let vcStakeholders = Storyboard.getInstanceOf(StakeholdersViewController.self)
        navigationController?.pushViewController(vcStakeholders, animated: true)
    }
    
    func goToProyects()
    {
        let vcProjects = Storyboard.getInstanceOf(ProjectsViewController.self)
        navigationController?.pushViewController(vcProjects, animated: true)
    }
    
    func goToCalendar()
    {
        let vcCalendar = Storyboard.getInstanceOf(CalendarViewController.self)
        navigationController?.pushViewController(vcCalendar, animated: true)
    }
    
    func goToSearch()
    {
        let vcSearch = Storyboard.getInstanceOf(GlobalSearchViewController.self)
        navigationController?.pushViewController(vcSearch, animated: true)
    }
    
    // MARK: MAKE DATASOURCE
    func buildDataSource()
    {
        datasource.removeAll()
        
        /** Author */
        datasource.append(SHMSectionDataSource(sectionTitle: String(arraySectionTitles[0]),
            rowsInSection: {
                return 1
            },
            cellForRow: { row in
                
                let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.subtitle, reuseIdentifier:"Cell")
                
                //Selected Color
                let selectedColor = UIView()
                selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
                cell.selectedBackgroundView = selectedColor

                cell.textLabel?.adjustsFontSizeToFitWidth = true
                cell.detailTextLabel?.adjustsFontSizeToFitWidth = true
                
                cell.textLabel?.textColor = UIColor.grayCloudy()
                cell.detailTextLabel?.textColor = UIColor.grayCloudy()
                
                cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 11)
                cell.detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 11)
                
                
                //Title
                if let postDate = self.post.createdDate?.getStringStyleMedium() as String!
                {
                    let title = self.arraySectionTitles[0]
                    let attribTitle = NSMutableAttributedString(string: title)
                    attribTitle.setColor(postDate, color: UIColor.blueBelize(), font: UIFont.systemFont(ofSize: 13))
                    cell.textLabel?.attributedText = attribTitle
                }
                else
                {
                    cell.textLabel?.text = self.arraySectionTitles[0]
                }
                
                //Detail
                let subtitle = "Posted by".localized() + ": " + self.author.fullName
                let attribSubtitle = NSMutableAttributedString(string: subtitle)
                attribSubtitle.setColor(self.author.fullName, color: UIColor.blueBelize(), font: UIFont.systemFont(ofSize: 13))
                cell.detailTextLabel?.attributedText = attribSubtitle
                
                //Action
                cell.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
                    let vcProfile = Storyboard.getInstanceOf(ProfileViewController.self)
                    vcProfile.member = self.author
                    self.navigationController?.pushViewController(vcProfile, animated: true)
                }))
                
                return cell
        }))
        
        /** Text */
        datasource.append(SHMSectionDataSource(sectionTitle: String(arraySectionTitles[1]),
            rowsInSection: {
                return 1
            },
            cellForRow: { row in
                
                let cell = self.tablePostDetails.dequeueReusableCell(withIdentifier: "TextViewTableCell") as! TextViewTableCell
                cell.tvText.delegate = self
                cell.selectionStyle = .none
                
                var title = self.post.title.getEditedTitleForPost(self.post, truncate: false)
                title = String(htmlEncodedString: title)!
                
                let attributedString = NSMutableAttributedString(string:title)
                attributedString.setLinks(self.post)
                cell.tvText.attributedText = attributedString
                
                return cell
        }))
        
        /** Stakeholders */
        if self.arrayStakeholders.count > 0
        {
            datasource.append(SHMSectionDataSource(sectionTitle: String(arraySectionTitles[2]),
                rowsInSection: {
                    return self.arrayStakeholders.count
                },
                cellForRow: { (row) in
                    
                    let cell = self.tablePostDetails.dequeueReusableCell(withIdentifier: "ContactListCell") as! ContactListCell
                    let stakeholder = self.arrayStakeholders[row]
                    
                    //Selected Color
                    let selectedColor = UIView()
                    selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
                    cell.selectedBackgroundView = selectedColor
                    
                    cell.lblName.text = "\(stakeholder.fullName)"
                    var strJobPosition = ""
                    if let jobPosition = stakeholder.jobPosition?.value {
                        
                        strJobPosition = jobPosition
                    }
                    let strCompanyName = stakeholder.companyName 
                    if (strCompanyName != "" && strJobPosition != "")
                    {
                        let attributedPosition = NSMutableAttributedString(string: "\(strJobPosition) / \(strCompanyName)")
                        attributedPosition.setBoldAndColor("/ \(stakeholder.companyName)", color: UIColor.grayCloudy(), size: Constants.FontSize.PostText)
                        cell.lblPosition.attributedText = attributedPosition
                    }
                    else
                    {
                        let attributedPosition = NSMutableAttributedString(string: "\(strJobPosition)\(strCompanyName)")
                        cell.lblPosition.attributedText = attributedPosition
                    }
                    //hide Rating indicators
                    cell.viewRatingContainer.isHidden = true
                    
                    cell.imgActor.setImageWith(URL(string: stakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
                    cell.addGestureRecognizer(ActorTapGestureRecognizer(actor: stakeholder, onTap: { actor in
                        
                        //Enabled or Disabled
                        guard actor.isEnable == true else {
                            
                            MessageManager.shared.showBar(title: actor.fullName, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                            
                            return
                        }
                        
                        let vcStakeholderDetail : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                        vcStakeholderDetail.stakeholder = actor
                        self.navigationController?.pushViewController(vcStakeholderDetail, animated: true)
                        
                    }))
                    
                    return cell
            }))
        }
        
        /** Attatchments */
        if self.arrayAttachments.count > 0
        {
            datasource.append(SHMSectionDataSource(sectionTitle: String(arraySectionTitles[3]),
                rowsInSection: {
                    return self.arrayAttachments.count
                },
                cellForRow: { (row) in
                    
                    self.attachmentSelected = self.arrayAttachments[row]
                    
                    let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier:"Cell")
                    cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
                    
                    //Selected Color
                    let selectedColor = UIView()
                    selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
                    cell.selectedBackgroundView = selectedColor
                    
                    //fill the cell
                    cell.textLabel?.text = self.attachmentSelected!.name
                    cell.imageView?.image = UIImage(named: "iconFile_\(self.arrayAttachments[row].type.lowercased().getIconAttachmentName())")
                    
                    cell.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
                        
                        guard self.arrayAttachments[row].file.isEmpty == false else {
                            return
                        }
                        
                        LibraryAPI.shared.attachmentBO.downloadFileFromLink(URL(string: self.arrayAttachments[row].file)!, onDownload: { (url) -> () in
                            self.attachmentSelected!.localUrl = url
                            self.showQuickLoock()
                            }, onError: { error in
                                
                                MessageManager.shared.showBar(title: "Error",
                                    subtitle: "Error.TryAgain".localized(),
                                    type: .error,
                                    fromBottom: false)
                        })
                    }))
                    
                    return cell
            }))
        }
    }
    
    // MARK: QUICK LOOK (QLPreviewControllerDataSource)
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int
    {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem
    {
        if let resourceURL = attachmentSelected?.localUrl {
            
            return resourceURL as QLPreviewItem
        }
        else
        {
            MessageManager.shared.showBar(title: "Error",
                                                            subtitle: "Error.TryAgain".localized(),
                                                            type: .error,
                                                            fromBottom: false)
            return URL(string: "") as! QLPreviewItem
        }

    }
}
