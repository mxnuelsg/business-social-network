//
//  HomeViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 11/11/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class HomeViewController_iPad: HomeViewController
{
    //MARK: OUTLETS & VARIABLES
    var vcCalendar: CalendarViewController!
    var vcPostIpad: NewPostViewController_iPad!
    
    @IBOutlet weak var viewGotoCalendar: UIView!
    @IBOutlet weak var btnGotoCalendar: UIButton!
    @IBOutlet weak var lblHome_iPad: UILabel!
    
    var barBtnActions: UIBarButtonItem!
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        self.loadConfig()
        
        guard let user = LibraryAPI.shared.currentUser else {
            return
        }
        
        if let lastLogin = user.lastLogin {
            let secsPassed = Int(Date().timeIntervalSince(lastLogin))
            
            if secsPassed > 60 * 60 * 24 * Constants.TimeInterval.SessionAliveLimit {
                logout()
                return
            }
        }
        
        if user.isLoggedIn == true {
            loadFeedList()
//            self.vcPostIpad.loadStakeholders()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "goToSearch_iPad:"), object: nil)
    }
    
    //MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        super.prepare(for: segue, sender: sender)

        if segue.identifier! == "segueID_CalendarViewController"
        {
            self.vcCalendar = segue.destination as! CalendarViewController
            self.vcCalendar.activeBlueForIpad = true
        }
        
        if segue.identifier! == "segueID_NewPostViewController"
        {
          self.vcPostIpad = segue.destination as! NewPostViewController_iPad
        }
        
        if segue.identifier! == "segueID_PostsTableViewController" {
            vcPostsTable = segue.destination as! PostTableViewController_iPad
        }
        
        
    }
    
    //MARK: LOAD CONFIG
    override func loadConfig()
    {
        super.loadConfig()
        self.localize()
        //add notification for ipad global search
        NotificationCenter.default.addObserver(self, selector:#selector(self.goToSearch_iPad(_:)), name: NSNotification.Name(rawValue: "goToSearch_iPad"), object: nil)
        
        //Calendar Footer
        self.viewGotoCalendar.setBorder()
        self.btnGotoCalendar.layer.cornerRadius = 3
        self.btnGotoCalendar.clipsToBounds = true
        self.btnGotoCalendar.addTarget(self, action: #selector(HomeViewController.goToCalendar), for: .touchUpInside)
        
        //Feed List
        self.vcPostsTable.setBorder()
        
        //Calendar
        if self.vcCalendar != nil
        {
            self.vcCalendar.configureForEmbed()
        }
        
        //New Post
        if self.vcPostIpad != nil
        {
            self.vcPostIpad.onViewControllerClosed = { [weak self] in
                
                self?.loadFeedList()
                self?.vcCalendar.loadFullCalendar()
            }
        }
        
        //Login Success load calendar and new post
        onLoginSuccess = { [weak self] in
//            self?.vcPostIpad.loadStakeholders()
            self?.vcCalendar.loadFullCalendar()
            
        }
    }
    
    fileprivate func localize()
    {
        self.lblHome_iPad.text = "CALENDAR".localized()
        self.btnGotoCalendar.setTitle("GO TO CALENDAR".localized(), for: UIControlState())
    }
    //MARK: ACTIONS
    func showActions(_ sender: UIButton)
    {
        let menuActions = ActionsMenu_iPad()
        menuActions.addAction(ActionsMenu_iPad.MenuAction(title: "Go to Stakeholder List".localized(), imageName: "User", action: { [weak self] in
            self?.goToStakeHolders()
        }))
        menuActions.addAction(ActionsMenu_iPad.MenuAction(title: "Go to Project List".localized(), imageName: "Project", action: { [weak self] in
            self?.goToProyects()
        }))
        menuActions.addAction(ActionsMenu_iPad.MenuAction(title: "Go to Calendar".localized(), imageName: "Calendar", action: { [weak self] in
            self?.goToCalendar()
        }))
        menuActions.addAction(ActionsMenu_iPad.MenuAction(title: "Search".localized(), imageName: "Search", action: { [weak self] in
            self?.goToSearch()
        }))
    
        menuActions.openFromBarButtonItem(barBtnActions, inController: self)
    }

    //MARK: OVERRIDE FUNCTIONS (heritage)
    override func setupNavigation(animated: Bool)
    {
        super.setupNavigation(animated: animated)
        
        let barBtnGlobalSearch = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(HomeViewController.goToSearch as (HomeViewController) -> () -> ()))
        navigationItem.rightBarButtonItems = [barBtnGlobalSearch]
        
//        barBtnActions = ActionsMenu_iPad.getActionsButton(target: self, action: "showActions:")
//        navigationItem.rightBarButtonItem = barBtnActions
    }
    
    func goToSearch_iPad(_ notification: Notification)
    {
        let searchKeyWord = notification.userInfo!["SearchKeyWord"] as! String
        print("WE BETTER START SEARCHING FOR \(searchKeyWord)")
        let vcSearch = Storyboard.getInstanceOf(GlobalSearchViewController_iPad.self)
        vcSearch.searchKeyWord = searchKeyWord
        navigationController?.pushViewController(vcSearch, animated: true)
    }

}
