//
//  HomeViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/13/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import UserNotifications

class HomeViewController: MenuOptionViewController, UITabBarDelegate
{
    var vcPostsTable: PostsTableViewController!
    var postsBackup: [Post]?
    var currentPage: Int = 1
    var loadPreviousFromRow: Int?
    var isSearching = false
    var onLoginSuccess: (() -> ())?
    
    //Tab bar
    @IBOutlet weak var barItemNewPost: UITabBarItem!
    @IBOutlet weak var barItemStakeholder: UITabBarItem!
    @IBOutlet weak var barItemProjects: UITabBarItem!
    @IBOutlet weak var barItemCalendar: UITabBarItem!
    @IBOutlet weak var barItemSearch: UITabBarItem!
    
    // MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        
        guard let user = LibraryAPI.shared.currentUser else {
            return
        }
        
        if let lastLogin = user.lastLogin {
            
            let secsPassed = Int(Date().timeIntervalSince(lastLogin))
            if secsPassed > 60 * 60 * 24 * Constants.TimeInterval.SessionAliveLimit {
                
                self.logout()
                return
            }
        }
        
        if user.isLoggedIn == true
        {
            self.loadFeedList()
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        //Reset Badge icon
        self.removeBadgeIcon()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        if LibraryAPI.shared.currentUser == nil
            || LibraryAPI.shared.currentUser!.isLoggedIn == false
            || LibraryAPI.shared.currentUser!.role == UserRole.none
        {
            
            if (UserDefaultsManager.getModeEnviroment() == 3)
            {
                performSegue(withIdentifier: "LoginViewController", sender: nil)
                
            }
            else
            {
                //LOGIN
                let vcLoginCemex: LoginCemexController = Storyboard.getInstanceFromStoryboard("LoginCemex")
                vcLoginCemex.onCemexLoginSuccess = { [weak self] in
                    
                    self?.initializeHomeScreen()
                }
                
                self.present(vcLoginCemex, animated: true, completion: nil)
            }
        }
        else
        {
            if LibraryAPI.shared.pushNotificationSilentObject.count > 0
            {
                if let id = LibraryAPI.shared.pushNotificationSilentObject["Id"] as! Int!, let type = LibraryAPI.shared.pushNotificationSilentObject["Type"] as! Int!
                {
                    let bodyMessage = (LibraryAPI.shared.pushNotificationSilentObject["aps"] as! [AnyHashable: Any])["alert"] as! String
                    
                    //Selecting Type
                    switch type
                    {
                    case PushNotificationType.publishCard.rawValue, PushNotificationType.stakeholderEdited.rawValue:

                        let stakeholderEdited = Stakeholder(isDefault: true)
                        stakeholderEdited.id = id
                        
                        //Workflow iPad / iPhone
                        if DeviceType.IS_ANY_IPAD == true
                        {
                            //iPad
                            let vcStakeholderDetail = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
                            vcStakeholderDetail.stakeholder = stakeholderEdited
                            
                            self.navigationController?.pushViewController(vcStakeholderDetail, animated: true)
                        }
                        else
                        {
                            //iPhone
                            let vcStakeholderDetail : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                            vcStakeholderDetail.stakeholder = stakeholderEdited
                            
                            self.navigationController?.pushViewController(vcStakeholderDetail, animated: true)
                        }
                    case PushNotificationType.projectEdited.rawValue:

                        let projectEdited = Project(isDefault: true)
                        projectEdited.id = id
                        
                        if DeviceType.IS_ANY_IPAD == true
                        {
                            //iPad
                            let vcProjectDetail = Storyboard.getInstanceOf(ProjectDetailViewController_iPad.self)
                            vcProjectDetail.project = projectEdited
                            
                            self.navigationController?.pushViewController(vcProjectDetail, animated: true)
                        }
                        else
                        {
                            //iPhone
                            let vcProjectDetail : DetailProjectViewController = Storyboard.getInstanceFromStoryboard("Projects")
                            vcProjectDetail.project = projectEdited
                            
                            self.navigationController?.pushViewController(vcProjectDetail, animated: true)
                        }
                    case PushNotificationType.inbox.rawValue:

                        let inbox = InboxMessage()
                        inbox.id = id
                        
                        if DeviceType.IS_ANY_IPAD == true
                        {
                            //iPad
                            let vcInboxDetail = Storyboard.getInstanceOf(InboxDetailTableViewController_iPad.self)
                            vcInboxDetail.message = inbox
                            
                            self.navigationController?.pushViewController(vcInboxDetail, animated: true)
                        }
                        else
                        {
                            //iPhone
                            let vcInboxDetail = Storyboard.getInstanceOf(InboxDetailTableViewController.self)
                            vcInboxDetail.message = inbox
                            
                            self.navigationController?.pushViewController(vcInboxDetail, animated: true)
                        }
                    case PushNotificationType.localManagerMail.rawValue:
                        
                        let alert = UIAlertController(title: "Notification".localized(), message:bodyMessage, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Close".localized(), style: .cancel, handler:nil))
                        
                        self.present(alert, animated: true, completion: nil)
                    default:
                        let post = Post()
                        post.id = id
                        
                        if DeviceType.IS_ANY_IPAD == true
                        {
                            //iPad
                            let vcPostDetail = Storyboard.getInstanceOf(PostDetailViewController_iPad.self)
                            vcPostDetail.post = post
                            
                            self.navigationController?.pushViewController(vcPostDetail, animated: true)
                        }
                        else
                        {
                            //iPhone
                            let vcPostDetail = Storyboard.getInstanceOf(PostDetailViewController.self)
                            vcPostDetail.post = post
                            
                            self.navigationController?.pushViewController(vcPostDetail, animated: true)
                        }
                        break
                    }
                }
                
                //Clean object
                LibraryAPI.shared.pushNotificationSilentObject = [:]
            }
        }
        
    }
    
    deinit
    {
        print("Deinit: HomeViewController")
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "goToInbox"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "goToStakeHolders"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "goToProyects"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "goToCalendar"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "goToSearch:"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "goToSettings"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "goToProfile"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "logout"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "reloadHomeFeed"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "inboxReceived"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "goToFeedback"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "performLogout"), object: nil)
        
        //Reports
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "goToRelations"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "goToMyPosts"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "goToConnections"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "goToStakeholderConnect"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "goToGeography"), object: nil)
    }
    
    // MARK: CONFIG
    func loadConfig()
    {
        //Localizations
        self.localize()
        
        //Navigation
        setupNavigation(animated: false)
        
        //Notifications
        NotificationCenter.default.addObserver(self, selector:#selector(self.goToInbox), name: NSNotification.Name(rawValue: "goToInbox"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.goToStakeHolders), name: NSNotification.Name(rawValue: "goToStakeHolders"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.goToProyects), name: NSNotification.Name(rawValue: "goToProyects"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.goToCalendar), name: NSNotification.Name(rawValue: "goToCalendar"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.goToSearch(_:)), name: NSNotification.Name(rawValue: "goToSearch"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.goToSettings), name: NSNotification.Name(rawValue: "goToSettings"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.goToProfile), name: NSNotification.Name(rawValue: "goToProfile"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.logout), name: NSNotification.Name(rawValue: "logout"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.inboxReceived), name: NSNotification.Name(rawValue: "inboxReceived"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.goToFeedback), name: NSNotification.Name(rawValue: "goToFeedback"), object: nil)
        
        //Reports
        NotificationCenter.default.addObserver(self, selector:#selector(self.goToRelations), name: NSNotification.Name(rawValue: "goToRelations"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.goToMyPosts), name: NSNotification.Name(rawValue: "goToMyPosts"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.goToConnections), name: NSNotification.Name(rawValue: "goToConnections"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.goToStakeholderConnect), name: NSNotification.Name(rawValue: "goToStakeholderConnect"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.goToGeography), name: NSNotification.Name(rawValue: "goToGeography"), object: nil)
        
        //On Settings changed
        NotificationCenter.default.addObserver(self, selector:#selector(self.reloadFeed), name: NSNotification.Name(rawValue: "reloadHomeFeed"), object: nil)
        
        //Logout listener
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "performLogout"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.performLogout), name: NSNotification.Name(rawValue: "performLogout"), object: nil)
        
        //Loading message
        self.vcPostsTable.displayBackgroundMessage("Loading...".localized(), subMessage: "")
        
        //Refresh control
        self.vcPostsTable.enableRefreshAction({
            
            self.loadFeedList()
        })

        // LOAD MORE PAGINATION
        self.vcPostsTable.willDisplayLastRow = { [weak self] row in
            
            self?.loadPreviousFromRow = row
            self?.loadPreviousFeed()
        }
        
        LibraryAPI.shared.userBO.onLanguageChanged = { didChange in
            
            //Language
            let message = "The language changes will take place next time you load the app. Would you like to close the app now?".localized()
            let alert = UIAlertController(title:"Language Change".localized(), message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:"No".localized(), style: .cancel, handler:nil))
            alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (action) in
                
                exit(0)
            }))
            let delayTime = DispatchTime.now() + Double(Int64(5.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    fileprivate func localize()
    {
        if DeviceType.IS_ANY_IPAD == false
        {
            self.barItemNewPost.title = "Geographies".localized()
            self.barItemStakeholder.title = "Stakeholders".localized()
            self.barItemProjects.title = "Projects".localized()
            self.barItemCalendar.title = "Calendar".localized()
            self.barItemSearch.title = "Search".localized()
        }
    }
    
    func setupNavigation(animated: Bool)
    {
        self.title = "HOME".localized()
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "MenuLogo"))
        
        // NEW MESSAGE
        if DeviceType.IS_ANY_IPAD == false
        {
            let barBtnCompose = UIBarButtonItem(image: UIImage(named: "iconNewPost"), style: .plain, target: self, action: #selector(HomeViewController.createNewPost))
            navigationItem.rightBarButtonItems = [barBtnCompose]
        }
        
        // LOGO/MENU BUTTON
        navigationItem.setHidesBackButton(false, animated: animated)
        
        //Lateral Menu
        if self.revealViewController() != nil
        {
            let button: UIButton = UIButton(type: .custom)
            button.setImage(UIImage(named: "iconMenu"), for: UIControlState())
            button.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControlEvents.touchUpInside)
            button.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
            button.clipsToBounds = false
            
            let barButton = UIBarButtonItem(customView: button)
            navigationItem.setLeftBarButtonItems([barButton], animated: true)
            
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    //MARK: WEB SERVICE
    func loadFeedList()
    {
        LibraryAPI.shared.feedBO.getFeed(onSuccess: { posts in
            
            let backupLanguage = UserDefaults.standard.value(forKey: Constants.UserKey.BackupLanguage) as? String
            let locale = Locale.preferredLanguages[0].components(separatedBy: "-")[0]
            
            if backupLanguage != locale
            {
                LibraryAPI.shared.customLocale.loadTranslations()
            }
            
            self.vcPostsTable.refreshed()
            self.vcPostsTable.posts = posts
            self.vcPostsTable.reload()
            
            }) { error in
                
                MessageManager.shared.showBar(title: "Error",
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
                
                self.vcPostsTable.refreshed()
                self.vcPostsTable.tableView.displayBackgroundMessage("Error",
                    subMessage: "Pull To Refresh".localized())
        }
    }
    
    
    //MARK: ACTIONS
    func inboxReceived()
    {
        if let user = LibraryAPI.shared.currentUser, user.isLoggedIn == true && user.role == UserRole.vipUser {
            
                self.inboxBadge?.text = "\(LibraryAPI.shared.currentUser!.unReadInbox)"
        }
    }
    
    func reloadFeed()
    {
        self.loadFeedList()
    }
    
    func loadPreviousFeed()
    {
        guard let last = self.vcPostsTable.posts.last else {
            return
        }
        
        //Get next page
        LibraryAPI.shared.feedBO.getFeedPreviousTo(postId: last.id, onSuccess: { posts in
            
            var newIndexPaths = [IndexPath]()
            let nextRow = self.vcPostsTable.posts.count
            
            for newRow in nextRow..<(nextRow + posts.count)
            {
                newIndexPaths.append(IndexPath(row: newRow, section: 0))
            }
            
            self.vcPostsTable.posts += posts
            self.vcPostsTable.tableView.insertRows(at: newIndexPaths, with: .bottom)
            
            }) { error in

                 MessageManager.shared.showBar(title: "Error",
                    subtitle: "Error.TryAgain".localized(),
                    type: .error,
                    fromBottom: false)
        }
    }
    
    @objc func performLogout() {
        self.navigationController?.childViewControllers.forEach({$0.dismiss(animated: true, completion: nil)})
        self.navigationController?.popToRootViewController(animated: true)
        
        self.logout()
    }
    // MARK: TABBAR DELEGATE (FeedTableCell)
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        //Selected item color equal not selected color
        tabBar.tintColor = UIColor.colorForSelectedTabBArItem()
        
        switch item.tag
        {
        case 1:
            self.goToStakeHolders()
        case 2:
            self.goToProyects()
        case 3:
            self.goToGeography()
        case 4:
            self.goToCalendar()
        case 5:
            self.goToSearch()
        default:
            break
        }
    }
    
    // MARK: ACTIONS NAVIGATOR MENU
    func createNewPost()
    {
        let vcNewPost = NewPostViewController()
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = (DeviceType.IS_ANY_IPAD) ?  UIModalPresentationStyle.formSheet :  UIModalPresentationStyle.overFullScreen
        
        present(navController, animated: true, completion: nil)
    }
    
    func goToStakeHolders()
    {
        let vcStakeholders = (DeviceType.IS_ANY_IPAD == true) ? Storyboard.getInstanceOf(StakeholdersViewController_iPad.self) : Storyboard.getInstanceOf(StakeholdersViewController.self)
        navigationController?.pushViewController(vcStakeholders, animated: true)
    }
    
    func goToProyects()
    {
        let vcProjects = (DeviceType.IS_ANY_IPAD == true) ? Storyboard.getInstanceOf(ProjectsViewController_iPad.self) : Storyboard.getInstanceOf(ProjectsViewController.self)
        navigationController?.pushViewController(vcProjects, animated: true)
    }
    
    func goToCalendar()
    {
        if DeviceType.IS_ANY_IPAD == true
        {
            let vcCalendar = Storyboard.getInstanceOf(CalendarViewController_iPad.self)
            navigationController?.pushViewController(vcCalendar, animated: true)
        }
        else
        {
            let vcCalendar = Storyboard.getInstanceOf(CalendarViewController.self)
            vcCalendar.shouldShowColorLabelDescription = true
            navigationController?.pushViewController(vcCalendar, animated: true)
        }    
    }
    
    func goToSearch(_ notification: Notification)
    {
        let searchKeyWord = notification.userInfo!["SearchKeyWord"] as! String
        print("WE BETTER START SEARCHING FOR \(searchKeyWord)")
        let vcSearch = Storyboard.getInstanceOf(GlobalSearchViewController.self)
        vcSearch.searchKeyWord = searchKeyWord
        navigationController?.pushViewController(vcSearch, animated: true)
    }
    
    func goToSearch()
    {
        if DeviceType.IS_ANY_IPAD == true
        {
            let vcSearch = Storyboard.getInstanceOf(GlobalSearchViewController_iPad.self)
            navigationController?.pushViewController(vcSearch, animated: true)
        }
        else
        {
            let vcSearch = Storyboard.getInstanceOf(GlobalSearchViewController.self)
            navigationController?.pushViewController(vcSearch, animated: true)
        }
    }
    
    func goToSettings()
    {
        let vcSettings = Storyboard.getInstanceOf(SettingsViewController.self)
        navigationController?.pushViewController(vcSettings, animated: true)
    }
    
    func goToInbox()
    {
        let vcInbox = (DeviceType.IS_ANY_IPAD == true) ? Storyboard.getInstanceOf(InboxViewController_iPad.self) : Storyboard.getInstanceOf(InboxViewController.self)
        navigationController?.pushViewController(vcInbox, animated: true)
    }
    
    func goToProfile()
    {
        let member = Member(isDefault:true)
        member.id = LibraryAPI.shared.currentUser!.id

        let vcProfile = (DeviceType.IS_ANY_IPAD == true) ? Storyboard.getInstanceOf(ProfileViewController_iPad.self) : Storyboard.getInstanceOf(ProfileViewController.self)
        vcProfile.member = member
        navigationController?.pushViewController(vcProfile, animated: true)
    }
    
    func goToFeedback()
    {
        let vcFeedback = Storyboard.getInstanceOf(FeedbackViewController.self)
        
        let navController = NavyController(rootViewController: vcFeedback)
        navController.modalPresentationStyle = .formSheet
        
        present(navController, animated: true, completion: nil)
    }
    
    func logout()
    {
        self.vcPostsTable.posts = []
        self.vcPostsTable.reload()
        
        //Logout & Present Login
        inboxBadge?.isHidden = true
        LibraryAPI.shared.logout()
        
        if (UserDefaultsManager.getModeEnviroment() == 3)
        {
            performSegue(withIdentifier: "LoginViewController", sender: nil)
        }
        else
        {
            //LOGIN
            let vcLoginCemex: LoginCemexController = Storyboard.getInstanceFromStoryboard("LoginCemex")
            vcLoginCemex.onCemexLoginSuccess = { [weak self] in
                
                self?.initializeHomeScreen()
            }
            self.present(vcLoginCemex, animated: true, completion: nil)
        }
    }
    
    func goToRelations()
    {
        print("RELATIONS")
        let vcRelations = Storyboard.getInstanceOf(RelationsReportSearchViewController.self)
        navigationController?.pushViewController(vcRelations, animated: true)
    }
    
    func goToMyPosts()
    {
        print("MY POSTS")
        if DeviceType.IS_ANY_IPAD == true
        {
            let vcMyPosts = Storyboard.getInstanceOf(MyPostsReportViewController_iPad.self)
            navigationController?.pushViewController(vcMyPosts, animated: true)
        }
        else
        {
            let vcMyPosts = Storyboard.getInstanceOf(MyPostsReportViewController.self)
            navigationController?.pushViewController(vcMyPosts, animated: true)
        }
        
    }
    
    func goToConnections()
    {
        print("Connections POSTS")
        if DeviceType.IS_ANY_IPAD == true
        {
            let vcConexions = Storyboard.getInstanceOf(ConnectionsReportViewController_iPad.self)
            navigationController?.pushViewController(vcConexions, animated: true)
        }
        else
        {
            let vcConexions = Storyboard.getInstanceOf(ConnectionsReportViewController.self)
            navigationController?.pushViewController(vcConexions, animated: true)
        }
    }
    
    func goToStakeholderConnect()
    {
        print("Stakeholder Connections ")
        let vcConexions = (DeviceType.IS_ANY_IPAD == true) ? Storyboard.getInstanceOf(StakeholderConnectReportViewController_iPad.self) : Storyboard.getInstanceOf(StakeholderConnectReportViewController.self)
        navigationController?.pushViewController(vcConexions, animated: true)
    }
    
    func goToGeography()
    {
        print("goToGeography")
        //Cancel previus requests
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.GetFeed)
        if DeviceType.IS_ANY_IPAD == false
        {
            let vcGeography : GeographyViewController = Storyboard.getInstanceFromStoryboard("Geography")
            navigationController?.pushViewController(vcGeography, animated: true)
        }
        else
        {
            let vcGeography : GeographyViewController_iPad = Storyboard.getInstanceFromStoryboard("GeographyPad")
            navigationController?.pushViewController(vcGeography, animated: true)
        }
    }
    
    // MARK: - NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier! == "segueID_PostsTableViewController"
        {
            vcPostsTable = segue.destination as! PostsTableViewController
        }
        else if segue.identifier! == "LoginViewController"
        {
            let vcLogin = segue.destination as! LoginViewController
            vcLogin.onLoginSuccessful = { [weak self] Void in
                
                self?.initializeHomeScreen()
            }
            inboxBadge?.frame = CGRect(x: 45, y: 3, width: 20, height: 20)
        }
    }
    
    //MARK: ON LOGIN PROCCESS SUCCESSFUL
    fileprivate func initializeHomeScreen()
    {
        //Init Load Feed
        self.loadFeedList()
        
        //Check for pusn notifications authorization
        DispatchQueue.main.async {
            
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                // Enable or disable features based on authorization.
            }
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        //Load Translations
        self.loadTranslations()
    }
    
    //MARK: TRANSLATIONS (Language)
    fileprivate func loadTranslations()
    {
        LibraryAPI.shared.customLocale.loadTranslations()
        LibraryAPI.shared.customLocale.exitApp = { [weak self] exitApp in
            
            let shouldExit = exitApp
            
            if shouldExit == true
            {
                let vcKillApp = KillAppController()
                
                let vcModal = ModalViewController(subViewController: vcKillApp)
                vcModal.closeOnTapBackground = false
                
                self?.present(vcModal, animated:true, completion: nil)
            }
        }
    }
}
