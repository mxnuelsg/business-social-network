//
//  PostTableViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 1/6/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class PostTableViewController_iPad: PostsTableViewController
{

    // MARK: - LIFE CLYCLE
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let post = postsFiltered[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedTableCell") as! FeedTableCell
        
        //Color for selected cell
        let selectedColor = UIView()
        selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
        cell.selectedBackgroundView = selectedColor
        
        cell.tvText.delegate = self
        cell.cellForPost(cell, post: post)
        cell.onStakeholderTap = { [weak self] stakeholder in
            
            //Enabled or Disabled
            guard stakeholder.isEnable == true else {
                
                MessageManager.shared.showBar(title: stakeholder.fullName, subtitle: "is disabled".localized(), type: .info, fromBottom: false)
                
                return
            }
            
            self?.pushStakeholder(stakeholder)
        }
        
        cell.onAllStakeholdersTap = { [weak self] Void in
            
            let stakeholdersTableVC = StakeholdersTableViewController.getNewInstance()
            stakeholdersTableVC.stakeholders = post.stakeholders
            
            let vcModal = ModalViewController(subViewController: stakeholdersTableVC)
            self?.present(vcModal, animated:true, completion: nil)
        }
        
        cell.onFilesTap = { [weak self] Void in
            
            let vcFileList = FileListViewController(attachments: post.attachments)
            let vcModal = ModalViewController(subViewController: vcFileList)
            vcModal.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            vcModal.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
           
            self?.present(vcModal, animated:true, completion: nil)
        }
        
        cell.onTextViewTap = { [weak self] Void in
            
            let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController_iPad.self)
            vcDetailPost.post = post
            
            self?.navigationController?.pushViewController(vcDetailPost, animated: true)
        }
        
        return cell
    }
    
    //MARK: TABLE DELEGATE
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vcDetailPost = Storyboard.getInstanceOf(PostDetailViewController_iPad.self)
        vcDetailPost.post = postsFiltered[indexPath.row]
        self.navigationController?.pushViewController(vcDetailPost, animated: true)
    }
    //MARK: OVERRIDE FOR SCROLL DELEGATE
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if bottomEdge >= scrollView.contentSize.height
        {
            guard let parent = parent as? GlobalSearchViewController_iPad else {
                
                return
            }
            
            if let pageNumberInt = parent.postPageNumber, let totalInt = parent.results?.postsTotal {
                
                let pageNumberFloat = Float(pageNumberInt)
                let totalFloat = Float(totalInt)
                if pageNumberFloat <= (totalFloat/20)
                {
                    print("page=\(pageNumberFloat) total=\((totalFloat/20))")
                    parent.postPageNumber = pageNumberInt+1
                    parent.updateType = GlobalSearchViewController_iPad.updateResultsOfType.posts
                    parent.search()
                }
            }
        }
    }
    
    // MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let identifier = segue.identifier, identifier == "ViewPostDetail" {
            
            let postDetailVC = segue.destination as! PostDetailViewController_iPad
            postDetailVC.post = postSelected
        }
        
    }

    
}
