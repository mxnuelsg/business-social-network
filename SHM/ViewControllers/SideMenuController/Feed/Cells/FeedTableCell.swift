//
//  FeedTableCell.swift
//  SHM
//
//  Created by Manuel Salinas on 7/31/15.
//  Copyright (c) 2015 Definity First. All rights reserved.
//

import UIKit

class FeedTableCell: UITableViewCell
{
    // MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var iconEvent: UIImageView!
    @IBOutlet weak var tvText: UITextView!
    @IBOutlet weak var imgActor1: UIImageView!
    @IBOutlet weak var imgActor2: UIImageView!
    @IBOutlet weak var imgActor3: UIImageView!
    @IBOutlet weak var lblDateBy: UILabel!
    @IBOutlet weak var btnFiles: UIButton!
    @IBOutlet weak var btnMoreActors: UIButton!
    @IBOutlet weak var iconPrivate: UIImageView!
    
    var onStakeholderTap: ((_ stakeholder: Stakeholder) -> ())?
    var onAllStakeholdersTap : (() -> ())?
    var onFilesTap : (() -> ())?
    var onTextViewTap : (() -> ())?
    
    // MARK: - LIFE CYCLE
    override func awakeFromNib()
    {
        /** Images */
        //self.imgActor1.layer.cornerRadius = self.imgActor1.frame.size.width / 2
        self.imgActor1.clipsToBounds = true
        self.imgActor1.setBorderWidth(0.5)
        
        //self.imgActor2.layer.cornerRadius = self.imgActor2.frame.size.width / 2
        self.imgActor2.clipsToBounds = true
        self.imgActor2.setBorderWidth(0.5)
        
        //self.imgActor3.layer.cornerRadius = self.imgActor3.frame.size.width / 2
        self.imgActor3.clipsToBounds = true
        self.imgActor3.setBorderWidth(0.5)
        
        /** Buttons */
        //self.btnMoreActors.layer.cornerRadius = self.btnMoreActors.frame.size.width / 2
        self.btnMoreActors.clipsToBounds = true
        self.btnMoreActors.isUserInteractionEnabled = false
        
        self.tvText.isEditable = false
        self.tvText.isSelectable = true
        
        self.tvText.linkTextAttributes = [ NSFontAttributeName : UIFont.boldSystemFont(ofSize: Constants.FontSize.PostText) ]
        
        self.localize()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: CELL FOR POST
    func cellForPost(_ cell: FeedTableCell, post: Post)
    {
        if post.id == 254 {
            print("Error here")
        }
        
        //Privacy
        self.iconPrivate.isHidden = (post.isPrivate == true) ? false : true
        
        /** Fill Date and Author */
        var title = post.title.getEditedTitleForPost(post, truncate: true)
       title = String(htmlEncodedString: title)!
        
        let datePost = post.createdDate?.getStringStyleMedium()
        
        if let date = datePost as String!
        {
            self.cellLabelsConf(cell, date: date, author: post.author.fullName, title: title)
        }
        else
        {
            self.cellLabelsConf(cell, date: "-", author: post.author.fullName, title: title)
        }
                
        /** Fill Files */
        self.cellAttachments(cell, files: post.totalAttachments)
        
        /** Actors */
        self.cellStakeholders(cell, stakeHolders: post.stakeholders)
        
        /** Style Text */
        var attributedString = NSMutableAttributedString(string: cell.tvText.text!)
        attributedString.setLinks(post)
        
        let suffixString = NSAttributedString(string: "...More".localized(), attributes: [
                NSFontAttributeName: UIFont.boldSystemFont(ofSize: Constants.FontSize.PostText)
            ])
        attributedString = attributedString.truncate(UIDevice.getFeedRangeToTruncate(), suffixString: suffixString)
        
        cell.tvText.attributedText = attributedString
        
        cell.tvText.removeGestureRecognizersOfType(ActionsTapGestureRecognizer.self)
        cell.tvText.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
            
            self.onTextViewTap?()
        }))
    }
    
    //MARK: CELL FOR POST AS EVENT (Calendar)
    func cellForPostEvent(_ cell: FeedTableCell, post: Post)
    {
        if post.id == 254 {
            print("Error here")
        }
        
        //Privacy
        self.iconPrivate.isHidden = (post.isPrivate == true) ? false : true
        
        /** Fill Date and Author */
        var title = post.title.getEditedTitleForPost(post, truncate: true)
        title = String(htmlEncodedString: title)!
        
        let datePost = post.createdDate?.getStringStyleMedium()
        
        if let date = datePost as String!
        {
            self.cellLabelsConf(cell, date: date, author: post.author.fullName, title: title)
        }
        else
        {
            self.cellLabelsConf(cell, date: "-", author: post.author.fullName, title: title)
        }
        
        /** Fill Files */
        self.cellAttachments(cell, files: post.totalAttachments)
        
        /** Actors */
        self.cellStakeholders(cell, stakeHolders: post.stakeholders)
        
        /** Style Text */
        var attributedString = NSMutableAttributedString(string: cell.tvText.text!)
        attributedString.setLinks(post)
        
        let suffixString = NSAttributedString(string: "...More".localized(), attributes: [
            NSFontAttributeName: UIFont.boldSystemFont(ofSize: Constants.FontSize.PostText)
            ])
        attributedString = attributedString.truncate(UIDevice.getFeedRangeToTruncateInEvent(), suffixString: suffixString)
        
        cell.tvText.attributedText = attributedString
        
        cell.tvText.removeGestureRecognizersOfType(ActionsTapGestureRecognizer.self)
        cell.tvText.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
            
            self.onTextViewTap?()
        }))
    }
    
    //MARK: TITLE & DATES
    func cellLabelsConf(_ cell: FeedTableCell, date: String, author: String, title: String)
    {
        let dateAndAuthor = ((date.isEmpty) ? "-" : date) + " " + "by".localized() + " " + ((author.isEmpty) ? "-" : author)
        
        let attributedString = NSMutableAttributedString(string: dateAndAuthor)
        attributedString.setBoldAndColor(author, color: UIColor.grayConcreto(), size: Constants.FontSize.PostSubtext)
        
        cell.lblDateBy.attributedText = attributedString
        cell.lblDateBy.adjustsFontSizeToFitWidth = true
        cell.lblDateBy.sizeToFit()
        cell.tvText.text = title
        cell.tvText.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
            
            self.onTextViewTap?()
        }))
    }
    
    //MARK: ATTACHMENTS
    func cellAttachments(_ cell: FeedTableCell, files: Int)
    {
        let hideFiles = (files > 0) ? false : true
        cell.btnFiles.isHidden = hideFiles
        cell.btnFiles.isUserInteractionEnabled = false //Demo
        
        if hideFiles == false
        {
            cell.btnFiles.setTitle(" \(files) " + (files != 1 ? "Files".localized() : "File".localized()), for: UIControlState())
            cell.btnFiles.removeGestureRecognizersOfType(ActionsTapGestureRecognizer.self)
            cell.btnFiles.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
                
                self.onFilesTap?()
            }))
        }
    }
    
    //MARK: STAKEHOLDERS
    func cellStakeholders(_ cell: FeedTableCell, stakeHolders: [Stakeholder])
    {
       
        //Actors Hidden or not configuration
        cell.imgActor1.isHidden = stakeHolders.count == 0
        cell.imgActor2.isHidden = stakeHolders.count <= 1
        cell.imgActor3.isHidden = stakeHolders.count != 3
        cell.btnMoreActors.isHidden = stakeHolders.count <= 3

        /**Fill Actors */
        //Clean previous images in cell
        cell.imgActor1.image = nil
        cell.imgActor2.image = nil
        cell.imgActor3.image = nil
        
        for (index, theStakeholder) in stakeHolders.enumerated()
        {
            if index == 0
            {
                cell.imgActor1.setImageWith(URL(string: theStakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
                cell.imgActor1.removeGestureRecognizersOfType(ActorTapGestureRecognizer.self)
                cell.imgActor1.addGestureRecognizer(ActorTapGestureRecognizer(actor: theStakeholder, onTap: {
                    (actor) -> Void in
                    self.onStakeholderTap?(actor)
                }))
            }
            else if index == 1
            {
                cell.imgActor2.setImageWith(URL(string: theStakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
                cell.imgActor2.removeGestureRecognizersOfType(ActorTapGestureRecognizer.self)
                cell.imgActor2.addGestureRecognizer(ActorTapGestureRecognizer(actor: theStakeholder, onTap: {
                    (actor) -> Void in
                    self.onStakeholderTap?(actor)
                }))
            }
            else if index == 2
            {
                if stakeHolders.count > 3
                {
                    cell.imgActor3.isHidden = true
                    
                    let totalActors: Int = stakeHolders.count - 2
                    cell.btnMoreActors.setBackgroundImage(UIImage(named: "backBlack"), for: UIControlState())
                    cell.btnMoreActors.setTitle("+\(totalActors)", for: UIControlState())
                    cell.btnMoreActors.isUserInteractionEnabled = true
                /*
                    //Uncomment this to add an action
                    reusableCell.btnMoreActors.removeGestureRecognizersOfType(ActionsTapGestureRecognizer)
                    reusableCell.btnMoreActors.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
                        self.onAllStakeholdersTap?()
                    }))
                     */
                }
                else
                {
                    cell.imgActor3.setImageWith(URL(string: theStakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
                    cell.imgActor3.removeGestureRecognizersOfType(ActorTapGestureRecognizer.self)
                    cell.imgActor3.addGestureRecognizer(ActorTapGestureRecognizer(actor: theStakeholder, onTap: {
                        (actor) -> Void in
                        
                        self.onStakeholderTap?(actor)
                    }))
                }
            }
            else
            {
                break
            }
        }
    }
    
    //MARK: SEPARATOR
    func layoutConfForCell(_ cell: FeedTableCell)
    {
        // Remove seperator inset
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset))
        {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))
        {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.responds(to: #selector(setter: UIView.layoutMargins))
        {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    // MARK: ACTIONS
    @IBAction func allStakeholdersTapped(_ sender: AnyObject)
    {
        self.onAllStakeholdersTap?()
    }
    
    
    
    // MARK: LOCALIZE
    fileprivate func localize()
    {
        self.lblDateBy.text = "Updated by:".localized()
        self.btnFiles.setTitle("0 File".localized(), for: UIControlState())
    }
}

