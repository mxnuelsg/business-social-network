//
//  TextViewTableCell.swift
//  SHM
//
//  Created by Manuel Salinas on 8/26/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class TextViewTableCell: UITableViewCell, UITextViewDelegate
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var tvText: UITextView!
    
    var onTextChange:((_ text: String) -> ())?
    
    //MARK: LIFE CYCLE
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        tvText.delegate = self
        tvText.linkTextAttributes = [
            NSFontAttributeName : UIFont.boldSystemFont(ofSize: Constants.FontSize.PostText)
        ]
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: DELEGATE
    func textViewDidChange(_ textView: UITextView)
    {
        if let textChanged = onTextChange
        {
            textChanged(textView.text)
        }
    }
}
