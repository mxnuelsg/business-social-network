//
//  PRFeedPageViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 11/14/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class PRFeedPageViewController: UIViewController {

    //MARK: VARIABLES AND OUTLETS
    @IBOutlet weak var containerFeed: UIView!
    @IBOutlet weak var containerAttachments: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var lastRegisteredPostId = 0
    var projectId = 0 {
        didSet {
            self.getProjectFeed()
        }
    }
    
    var vcPosts: PostsTableViewController?
    var vcAttachments: FileListViewController?
    var posts = [Post]() {
        didSet{
            self.vcPosts?.posts = self.posts
            self.vcPosts?.reload()
        }
    }
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        if posts.count > 0
        {
            self.vcPosts?.posts = self.posts
            self.vcPosts?.reload()
            self.loadAttachments()
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    

    //MARK: CONFIGS
    fileprivate func loadConfig()
    {
        self.segmentedControl.setTitle("Post".localized(), forSegmentAt: 0)
        self.segmentedControl.setTitle("Attachments".localized(), forSegmentAt: 1)
        self.containerFeed.isHidden = false
        self.containerAttachments.isHidden = true
    }
    
    //MARK: WEB SERVICES
    func getProjectFeed()
    {
        var parameters:[String:Any] = [:]
        parameters["projectId"] = self.projectId
        parameters["pageSize"] = 20
        parameters["type"] = 1
        parameters["postId"] = self.lastRegisteredPostId
        
        self.vcPosts?.displayBackgroundMessage("Loading...".localized(), subMessage: String())
        LibraryAPI.shared.stakeholderBO.getFeedEditingStakeholder(parameters: parameters, onSuccess: { (feed) in
            self.vcPosts?.dismissBackgroundMessage()
            self.vcPosts?.refreshControl?.setLastUpdate()
            self.vcPosts?.refreshControl?.endRefreshing()
            guard feed.posts.count != 0 else {
                
                if self.posts.count == 0
                {
                    self.vcPosts?.displayBackgroundMessage("No data".localized(), subMessage: String())
                }
                return
            }
            //Update posts and attachments table
            let posts = self.posts + feed.posts
            self.posts = posts
            self.loadAttachments()
            
            //Update last registered post id
            self.lastRegisteredPostId = feed.posts.last?.id ?? self.lastRegisteredPostId
            //Dismiss table message
            self.vcPosts?.dismissBackgroundMessage()
        }) { (error) in
            
            self.vcPosts?.refreshControl?.endRefreshing()
            self.vcPosts?.posts.removeAll()
            self.vcPosts?.tableView.reloadData()
            self.vcPosts?.displayBackgroundMessage("An error ocurred".localized(), subMessage: "Pull to refresh".localized())            
        }
    }
    
    //MARK: ACTIONS
    @IBAction func onDidChangeSegment(_ sender: Any)
    {
        if self.segmentedControl.selectedSegmentIndex == 0
        {
            self.containerFeed.isHidden = false
            self.containerAttachments.isHidden = true
        }
        else
        {
            self.containerFeed.isHidden = true
            self.containerAttachments.isHidden = false
        }
    }
    
    fileprivate func loadAttachments()
    {
        var attachments = [Attachment]()
        for post in self.posts
        {
            attachments.append(contentsOf: post.attachments)
        }
        self.vcAttachments = FileListViewController(attachments: attachments)
        if let vcFile = self.vcAttachments {
            if self.containerAttachments != nil
            {
                self.containerAttachments.addSubViewController(vcFile, parentVC: self)
                self.vcAttachments?.tableFiles.reloadData()
            }
            
        }
    }
    
    func refreshFeedTable()
    {
        self.lastRegisteredPostId = 0
        self.posts.removeAll()
        self.vcPosts?.posts.removeAll()
        self.getProjectFeed()
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "Posts"
        {
            if let post = segue.destination as? PostsTableViewController {
                
                self.vcPosts = post
                self.vcPosts?.refreshControl = UIRefreshControl()
                self.vcPosts?.refreshControl?.setLastUpdate()
                self.vcPosts?.refreshControl?.addTarget(self, action: #selector(self.refreshFeedTable), for: UIControlEvents.valueChanged)
                self.vcPosts?.willDisplayLastRow = { row in
                    
                    print("Last row")
                    self.getProjectFeed()
                }
            }
        }
    }
}
