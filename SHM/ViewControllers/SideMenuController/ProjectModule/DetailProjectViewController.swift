//
//  DetailProjectViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 11/14/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class DetailProjectViewController: UIViewController
{
    //MARK: OUTLETS AND VARIABLES
    var project = Project(isDefault:true) {
        didSet{
            if self.isViewLoaded == true {
                
                self.loadProjectInfo()
            }
        }
    }
    var vcPagesController: ProjectDetailPagesViewController?
    
    //HEADER
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblProjectSiteTitle: UILabel!    
    @IBOutlet weak var lblUpdateBy: UILabel!
    @IBOutlet weak var btnFiles: UIButton!
    @IBOutlet weak var lblPageTitle: UILabel!
    @IBOutlet weak var btnFollow: UIButton!
    
    @IBOutlet weak var btnEnable: UIButton!
    
    //TAB BAR
    @IBOutlet weak var tabBar: UITabBar!
    lazy var tabBarItems: [UITabBarItem] = {
        
        let icon1 = UITabBarItem(title:"Project Post".localized(), image: UIImage(named: "NewPost"), tag: 0)
        let icon2 = UITabBarItem(title: "Edit".localized(), image: UIImage(named: "EditUser"), tag: 1)
        let icon3 = UITabBarItem(title: "Search".localized(), image: UIImage(named: "Search"), tag: 2)
        return [icon1, icon2, icon3]
    }()
    lazy var tabBarItems_Calendar: [UITabBarItem] = {
        
        let icon1 = UITabBarItem(title:"Project Post".localized(), image: UIImage(named: "NewPost"), tag: 0)
        let icon2 = UITabBarItem(title: "Edit".localized(), image: UIImage(named: "EditUser"), tag: 1)
        let icon3 = UITabBarItem(title: "Search".localized(), image: UIImage(named: "Search"), tag: 2)
        let icon4 = UITabBarItem(title: "List".localized(), image: UIImage(named: "DayView"), tag: 3)

        return [icon1, icon2, icon4, icon3]
    }()
    lazy var tabBarItems_CalendarList: [UITabBarItem] = {
        
        let icon1 = UITabBarItem(title:"Project Post".localized(), image: UIImage(named: "NewPost"), tag: 0)
        let icon2 = UITabBarItem(title: "Edit".localized(), image: UIImage(named: "EditUser"), tag: 1)
        let icon3 = UITabBarItem(title: "Search".localized(), image: UIImage(named: "Search"), tag: 2)
        let icon4 = UITabBarItem(title: "Calendar".localized(), image: UIImage(named: "Calendar"), tag: 4)
        
        return [icon1, icon2, icon4, icon3]
    }()
    
    //SEARCH BAR
    fileprivate lazy var searchBar: AutoSearchBar = AutoSearchBar()
    
    //CLOSURES
    var onProjectWasEdited:(()->())?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    deinit {
        print("deinit DetailProjectViewController")
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()    
    }
    
    //MARK: CONFIGS
    fileprivate func loadConfig()
    {
        //Header
        self.headerView.backgroundColor = UIColor(red: 0.21, green: 0.24, blue: 0.26, alpha: 1.0)
        //Nav bar
        self.setupNavigation(animated: false)
        
        //files
        self.btnFiles.setTitle(" 0 File".localized(), for: UIControlState())
        self.btnFiles.isUserInteractionEnabled = false
        self.lblUpdateBy.text = "Updated by:".localized()
        self.lblPageTitle.text = "DETAIL".localized()
        
        //Follow and enable buttons
        self.btnFollow.layer.cornerRadius = 2
        self.btnFollow.clipsToBounds = true
        self.btnFollow.setTitle("...", for: UIControlState())
        //Btn Disable
        self.updateEnableButtonAppearence()
        //Tab bar
        self.tabBar.delegate = self
        self.tabBar.setItems(tabBarItems, animated: false)
        
        //Search
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        searchBar.initialize(onSearchText: { [weak self] (text) in
            
            let currentPage = self?.pageControl.currentPage ?? 0
            if (self != nil) {
                self!.updateSearch(text: text, page: currentPage)
            }
            }, onCancel: { [weak self] in
                
                let currentPage = self?.pageControl.currentPage ?? 0
                self?.searchBar.text = ""
                self?.updateSearch(text: "", page: currentPage)
                self?.setupNavigation(animated: true)
        })
        
    }
    
    //MARK: WEB SERVICES
    private func projectEnabled(_ enable: Bool)
    {
        MessageManager.shared.showLoadingHUD()
        LibraryAPI.shared.projectBO.enable(self.project, enable: enable, onSuccess: { isEnable in
            MessageManager.shared.hideHUD()
            //Refresh project List
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadProjectList"), object: nil)
            
            let status = isEnable == true ? "has been enabled".localized() : "has been disabled".localized()
            MessageManager.shared.showBar(title: "Info".localized(),
                                          subtitle: "This Project" + " " + status,
                                          type: .info,
                                          fromBottom: false)
            
            
            self.project.isEnable = isEnable
            
            self.updateEnableButtonAppearence()
            self.updatePostActionEnableStatus()
            
        }, onError: { error in
            MessageManager.shared.hideHUD()
            MessageManager.shared.showBar(title: "Error",
                                          subtitle: "Error.TryAgain".localized(),
                                          type: .error,
                                          fromBottom: false)
        })
    }
    //MARK: ACTIONS
    fileprivate func loadProjectInfo()
    {
        //Load header info
        self.lblTitle.text = self.project.title
        
        //Site
        if let siteTitle = self.project.site?.title {
            
            self.lblProjectSiteTitle.isHidden = false
            self.lblProjectSiteTitle.text = "⚑  \(siteTitle)"
        }
        else
        {
            self.lblProjectSiteTitle.isHidden = true
        }
        //Updates by
        let strBy = "by".localized()
        self.lblUpdateBy.text = ((self.project.modifiedDate?.showStringFormat("MM/dd/yyyy")) ?? "-") + " \(strBy) " + (self.project.author?.fullName ?? "-")
        
        //files
        self.btnFiles.setTitle(" \(self.project.totalAttachments) " + (self.project.totalAttachments != 1 ? "Files".localized() : "File".localized()), for: .normal)
        
        //BUTTONS APPEAREANCE
        self.updateFollowButtonAppearence()
        self.updateEnableButtonAppearence()
        self.updatePostActionEnableStatus()
    }
    fileprivate func updateFollowButtonAppearence()
    {
        //Follow button
        if self.project.followingStatus == true
        {
            self.btnFollow.setTitle("︎︎︎︎︎✔︎ " + "Following".localized(), for: UIControlState())
        }
        else
        {
            self.btnFollow.setTitle("Follow".localized(), for: UIControlState())
        }
    }
    fileprivate func updateEnableButtonAppearence()
    {
        if LibraryAPI.shared.currentUser?.role == .globalManager || self.project.coordinator.id == LibraryAPI.shared.currentUser?.id
        {
            //Project status
            if self.project.isEnable == true
            {
                self.btnEnable.setTitle("✔︎ " + " " + "Enabled".localized(), for: .normal)
            }
            else
            {
                self.btnEnable.setTitle("Enable".localized(), for: .normal)
            }
            
            self.btnEnable.layer.cornerRadius = 2
            self.btnEnable.clipsToBounds = true
            self.btnEnable.isHidden = false
        }
        else
        {
            self.btnEnable.isHidden = true
        }
    }
    
    fileprivate func updatePostActionEnableStatus()
    {
        self.tabBar.items?.forEach({ (item) in
            if item.tag == 0
            {
                item.isEnabled = self.project.isEnable
            }
        })
    }
    @IBAction func updateFollowStatus(_ sender: Any)
    {
        if project.followingStatus == false
        {
            MessageManager.shared.showLoadingHUD()
            LibraryAPI.shared.projectBO.followProject(project.id, onSuccess: {
                (success) -> () in
            
                MessageManager.shared.hideHUD()
                //Refresh Project List
                NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadProjectList"), object: nil)
                
                MessageManager.shared.showBar(title: "Info".localized(),
                                              subtitle: "Now, you're following this project".localized(),
                                              type: .info,
                                              fromBottom: false)
                
                self.project.followingStatus = true
                self.updateFollowButtonAppearence()
                
            }) {error in
                MessageManager.shared.hideHUD()
                MessageManager.shared.showBar(title: "Error",
                                              subtitle: "Error.TryAgain".localized(),
                                              type: .error,
                                              fromBottom: false)
            }
        }
        else
        {
            let vcConfirmUnfollow = UIAlertController(title: "Would you like to stop following this project?".localized(), message: project.title, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let actionUnfollow = UIAlertAction(title: "Unfollow".localized(), style: UIAlertActionStyle.destructive, handler: {
                (action) in
                
                MessageManager.shared.showLoadingHUD()
                LibraryAPI.shared.projectBO.unfollowProject(self.project.id, onSuccess: {
                    (success) -> () in
                    
                    MessageManager.shared.hideHUD()
                    //Refresh Project List
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadProjectList"), object: nil)
                    
                    MessageManager.shared.showBar(title: "Info".localized(),
                                                  subtitle: "Now, you're not following this project".localized(),
                                                  type: .info,
                                                  fromBottom: false)
                    
                    self.project.followingStatus = false
                    self.updateFollowButtonAppearence()
                    
                }) {error in
                    MessageManager.shared.hideHUD()
                    MessageManager.shared.showBar(title: "Error",
                                                  subtitle: "Error.TryAgain".localized(),
                                                  type: .error,
                                                  fromBottom: false)                                        
                }
            })
            
            let actionCancel = UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler: nil)
            
            vcConfirmUnfollow.addAction(actionUnfollow)
            vcConfirmUnfollow.addAction(actionCancel)
            
            self.present(vcConfirmUnfollow, animated: true, completion: nil)
        }
    }
    
    @IBAction func updateEnableStatus(_ sender: Any)
    {
        if self.project.isEnable == true
        {
            let vcConfirmUnfollow = UIAlertController(title: "Would you like to disable?".localized(), message: self.project.title, preferredStyle: .alert)
            let actionUnfollow = UIAlertAction(title: "Disable".localized(), style: .destructive, handler: {
                (action) in
                
                //Disable
                self.projectEnabled(false)
                
            })
            
            let actionCancel = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil)
            
            vcConfirmUnfollow.addAction(actionUnfollow)
            vcConfirmUnfollow.addAction(actionCancel)
            
            self.present(vcConfirmUnfollow, animated: true, completion: nil)
        }
        else
        {
            //Enable
            self.projectEnabled(true)
        }
    }
    
    fileprivate func updateTabBarItems(page: Int)
    {
        self.searchBar.cancelSearch()
        self.pageControl.currentPage = page
        if page == 0
        {
            self.lblPageTitle.text = "DETAIL".localized()
            self.tabBar.setItems(tabBarItems, animated: false)
        }
        else if page == 1
        {
            self.lblPageTitle.text = "FEED".localized()
            self.tabBar.setItems(tabBarItems, animated: false)
        }
        else if page == 2
        {
            self.lblPageTitle.text = "CALENDAR".localized()
            if self.vcPagesController?.vcPRCalendarPage?.containerCalendar.isHidden == true
            {
                self.tabBar.setItems(tabBarItems_CalendarList, animated: false)
            }
            else
            {
                self.tabBar.setItems(tabBarItems_Calendar, animated: false)
            }
        }
    }
    
    func setupNavigation(animated: Bool)
    {
        title = "PROJECT".localized()
        navigationItem.setHidesBackButton(false, animated: animated)
        navigationItem.fadeOutTitleView()
    }
    
    fileprivate func presentNewPost()
    {
        //New Post
        let vcNewPost = NewPostViewController()
        vcNewPost.projectTagged = self.project
        vcNewPost.onViewControllerClosed = {
            
            self.vcPagesController?.vcPRFeedPage?.refreshFeedTable()
            self.vcPagesController?.vcPRCalendarPage?.reloadCalendar()
        }
        
        let navController = NavyController(rootViewController: vcNewPost)
        navController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        present(navController, animated: true, completion:nil)
    }
    
    func showProjectEditionController()
    {
        //Indicator
        MessageManager.shared.showLoadingHUD()
        LibraryAPI.shared.projectBO.getCurrentUserSiteMappings({ (sites) in
            
            MessageManager.shared.hideHUD()
            var rootVC : UIViewController
            let vcEditProject = Storyboard.getInstanceOf(ProjectNewViewController.self)
            self.project.geographies = self.vcPagesController?.vcPRDetailPage?.vcGeographies?.clusterItems ?? [ClusterItem]()
            self.project.stakeholders = self.vcPagesController?.vcPRDetailPage?.vcStakeholders?.stakeholders ?? [Stakeholder]()
            vcEditProject.project = self.project
            vcEditProject.sites = sites
            vcEditProject.currentSite = self.project.site
            vcEditProject.onProjectEdited = { [weak self] Void in
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                    
                    self?.reloadProjectDetail()
                }
                
                self?.onProjectWasEdited?()
            }
            
            rootVC = vcEditProject
            self.navigationController?.pushViewController(rootVC, animated: true)
        })
        { (error) in
            
            MessageManager.shared.hideHUD()
        }
    }
    
    fileprivate func reloadProjectDetail()
    {
        self.vcPagesController?.setProjectId()
    }
    //MARK: SEARCH BAR
    func updateSearch(text: String, page: Int)
    {
        print("\(page)-- \(text)")
        if page == 0
        {
            self.vcPagesController?.vcPRDetailPage?.vcSummary?.searchText(text)
            self.vcPagesController?.vcPRDetailPage?.vcGeographies?.filterByText(text)
            self.vcPagesController?.vcPRDetailPage?.vcStakeholders?.filterByText(text)
            
        }
        else if page == 1
        {
            self.vcPagesController?.vcPRFeedPage?.vcPosts?.filterByText(text)
            self.vcPagesController?.vcPRFeedPage?.vcAttachments?.filterByText(text)
        }
        else if page == 2
        {
            self.vcPagesController?.vcPRCalendarPage?.vcCalendar?.filterByText(text)
            self.vcPagesController?.vcPRCalendarPage?.vcCalendarList?.filterByText(text)
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "Pages"
        {
            if let pages = segue.destination as? ProjectDetailPagesViewController {
                
                self.vcPagesController = pages
                self.vcPagesController = pages
                self.vcPagesController?.projectId = self.project.id
                self.vcPagesController?.onDidChangePage = { [weak self] page in
                    
                    self?.updateTabBarItems(page:page)
                }
                self.vcPagesController?.onDidLoadProjectLight = { [weak self] project in

                    self?.project = project
                }
            }
        }
    }
}

extension DetailProjectViewController: UITabBarDelegate
{
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        if item.tag == 0
        {
            self.presentNewPost()
        }
        else if item.tag == 1
        {
            self.showProjectEditionController()
        }
        else if item.tag == 2
        {
            self.searchBar.showInNavigationItem(self.navigationItem, animated: true)
        }
        else if item.tag == 3
        {
            self.tabBar.setItems(tabBarItems_CalendarList, animated: false)
            self.vcPagesController?.vcPRCalendarPage?.containerCalendar.isHidden = true
            self.vcPagesController?.vcPRCalendarPage?.containerList.isHidden = false
        }
        else if item.tag == 4
        {
            self.tabBar.setItems(tabBarItems_Calendar, animated: false)
            self.vcPagesController?.vcPRCalendarPage?.containerCalendar.isHidden = false
            self.vcPagesController?.vcPRCalendarPage?.containerList.isHidden = true
        }
    }
}
