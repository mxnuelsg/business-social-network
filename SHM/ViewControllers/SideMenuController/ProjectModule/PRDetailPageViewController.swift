//
//  PRDetailPageViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 11/14/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class PRDetailPageViewController: UIViewController
{
    //MARK: OUTLETS AND VARIABLES
    var vcGeographies:GeoTableViewController?
    @IBOutlet weak var containerGeographies: UIView!
    
    var vcStakeholders: ProjectDetailsStakeholdersTableViewController?
    @IBOutlet weak var containerStakeholders: UIView!
    
    var vcSummary: ProjectDetailTableViewController?
    @IBOutlet weak var containerResume: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var projectId = 0 {
        didSet {
            self.loadProjectDetailLight()
            self.loadStakeholdersByProject()
            self.loadGeographiesByProject()            
        }
    }
    
    //Closures
    var onDidLoadProjectLight:((_ project: Project)->())?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    //MARK: LOADS
    fileprivate func loadConfig()
    {
        self.segmentedControl.setTitle("Summary".localized(), forSegmentAt: 0)
        self.segmentedControl.setTitle("Stakeholders".localized(), forSegmentAt: 1)
        self.segmentedControl.setTitle("Geographies".localized(), forSegmentAt: 2)
        
        self.setTabsVisibility()
    }
    
    
    @IBAction func onTabIndexChanged(_ sender: Any)
    {
        self.setTabsVisibility()
    }
    
    //MARK: WEB SERVICE
    fileprivate func loadProjectDetailLight()
    {
        MessageManager.shared.showLoadingHUD()
        LibraryAPI.shared.projectBO.getProjectDetailLight(self.projectId, onSuccess: { (project) in
            
            MessageManager.shared.hideHUD()
            self.onDidLoadProjectLight?(project)
            self.vcSummary?.project = project
            self.vcSummary?.tableView.reloadData()
            
        }) { (error) in
            
            MessageManager.shared.hideHUD()
        }
    }
    
    @objc fileprivate func loadStakeholdersByProject()
    {
        LibraryAPI.shared.stakeholderBO.getStakeholdersByProject( self.projectId, onSuccess: { stakeholders in
            
            //UI update
            self.vcStakeholders?.tableView.dismissBackgroundMessage()
            self.vcStakeholders?.refreshControl?.setLastUpdate()
            self.vcStakeholders?.refreshControl?.endRefreshing()
            if stakeholders.count == 0
            {
                self.vcStakeholders?.tableView.displayBackgroundMessage("No data available".localized(), subMessage: "Pull to refresh".localized())
            }
            self.vcStakeholders?.stakeholders = stakeholders
            self.vcStakeholders?.stakeholdersBackup = stakeholders
            self.vcStakeholders?.tableView.reloadData()
        }) { (error) in
            
            self.vcStakeholders?.stakeholders?.removeAll()
            self.vcStakeholders?.tableView.reloadData()
            self.vcStakeholders?.refreshControl?.endRefreshing()
            self.vcStakeholders?.tableView.displayBackgroundMessage("An error ocurred".localized(), subMessage: "Pull to refresh".localized())
        }
    }
    
    @objc fileprivate func loadGeographiesByProject()
    {
        LibraryAPI.shared.geographyBO.getGeographiesByProject(self.projectId, onSuccess: { (geographies) in
            
            //UI update
            self.vcGeographies?.tableView.dismissBackgroundMessage()
            self.vcGeographies?.refreshControl?.setLastUpdate()
            self.vcGeographies?.refreshControl?.endRefreshing()
            if geographies.count == 0
            {
                self.vcGeographies?.tableView.displayBackgroundMessage("No data available".localized(), subMessage: "Pull to refresh".localized())
            }
            
            print(geographies.count)
            self.vcGeographies?.clusterItems = geographies
            self.vcGeographies?.backupClusterItems = geographies
        }) { (error) in
            self.vcGeographies?.clusterItems.removeAll()
            self.vcGeographies?.tableView.reloadData()
            self.vcGeographies?.refreshControl?.endRefreshing()
            self.vcGeographies?.tableView.displayBackgroundMessage("An error ocurred".localized(), subMessage: "Pull to refresh".localized())
        }
    }
    //MARK: ACTIONS
    fileprivate func setTabsVisibility()
    {
        let tab =  self.segmentedControl.selectedSegmentIndex
        switch tab
        {
        case 0:
            self.containerResume.isHidden = false
            self.containerStakeholders.isHidden = true
            self.containerGeographies.isHidden = true
        case 1:
            self.containerResume.isHidden = true
            self.containerStakeholders.isHidden = false
            self.containerGeographies.isHidden = true
        case 2:
            self.containerResume.isHidden = true
            self.containerStakeholders.isHidden = true
            self.containerGeographies.isHidden = false
        default:
            break
        }
        
    }
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "Summary"
        {
            if let summary = segue.destination as? ProjectDetailTableViewController {
                
                self.vcSummary = summary
                self.vcSummary?.tableView.isScrollEnabled = true
            }
        }
        else if segue.identifier == "Stakeholders"
        {
            if let stakeholders = segue.destination as? ProjectDetailsStakeholdersTableViewController {
                
                self.vcStakeholders = stakeholders                
                self.vcStakeholders?.refreshControl = UIRefreshControl()
                self.vcStakeholders?.refreshControl?.setLastUpdate()
                self.vcStakeholders?.refreshControl?.addTarget(self, action: #selector(self.loadStakeholdersByProject), for: UIControlEvents.valueChanged)
            }
        }
        else if segue.identifier == "Geographies"
        {
            if let geographies = segue.destination as? GeoTableViewController {
                
                self.vcGeographies = geographies
                self.vcGeographies?.refreshControl = UIRefreshControl()
                self.vcGeographies?.refreshControl?.setLastUpdate()
                self.vcGeographies?.refreshControl?.addTarget(self, action: #selector(self.loadGeographiesByProject), for: UIControlEvents.valueChanged)
            }
        }
        
    }
}
