//
//  PRCalendarPageViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 11/14/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class PRCalendarPageViewController: UIViewController
{
    //MARK: VARIABLES AND OUTLETS
    var vcCalendar: EmbededCalendarViewController?
    var vcCalendarList: EmbededCalendarTableViewController?
    @IBOutlet weak var containerCalendar: UIView!
    @IBOutlet weak var containerList: UIView!
    var projectId = 0 {
        didSet{
         
            self.reloadCalendar()
        }
    }
    var calendarItems = [CalendarItem]() {
        didSet {
            self.loadCalendars()
        }
    }
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.getProjectCalendarEvents(Date())
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: CONFIGS
    fileprivate func loadConfig()
    {
        self.containerList.isHidden = true
        self.containerCalendar.isHidden = false
    }
    
    //MARK: WEB SERVICES
    fileprivate func getProjectCalendarEvents(_ date: Date)
    {
        //Cancel previous request
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.GetCalendarEvents)
        
        //Object to request
        var wsObject: [String : Any]  = [:]
        wsObject["StakeholderId"] = 0
        wsObject["ProjectId"] = self.projectId
        wsObject["Month"] = date.month
        wsObject["Year"] = date.year
        
        LibraryAPI.shared.calendarBO.getEvents(wsObject, onSuccess: { (CalendarItems) in
            
            self.calendarItems = CalendarItems
        }) { (error) in
            
        }
    }
    
    //MARK: ACTIONS
    fileprivate func loadCalendars()
    {
        //LOAD CALENDAR VIEW
        self.vcCalendar?.arrayCalendarItems = self.calendarItems
        self.vcCalendar?.calendar.reloadData()
    }
    
    func reloadCalendar()
    {
        self.vcCalendar?.ID = self.projectId
        self.vcCalendarList?.ID = self.projectId
    }
    
    func updateCalendarViews(date: Date)
    {
        //Update Calendar View
        self.getProjectCalendarEvents(date)
        
        //Update Calendar List View
        self.vcCalendarList?.ultimateDate = date.getStartOfDay().convertToRequestString()
        self.vcCalendarList?.arrayCalendarList.removeAll()
        self.vcCalendarList?.loadCalendarListById()
    }
    
    func flipCalendarView()
    {
        if self.containerList.isHidden == false
        {
            self.containerList.isHidden = true
            self.containerCalendar.isHidden = false
        }
        else
        {
            self.containerList.isHidden = false
            self.containerCalendar.isHidden = true
        }
    }
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "calendar"
        {
            if let calendar = segue.destination as? EmbededCalendarViewController {
                
                self.vcCalendar = calendar
                self.vcCalendar?.calendarType = .ProjectDetail
                self.vcCalendar?.ID = self.projectId
                self.vcCalendar?.onMonthChanged = {[weak self] date in
                    
                    self?.updateCalendarViews(date: date)
                }
            }
        }
        else if segue.identifier == "List"
        {
            if let list = segue.destination as? EmbededCalendarTableViewController {
                
                self.vcCalendarList = list
                self.vcCalendarList?.ultimateDate = Date().firstDayOfMonth().getStartOfDay().convertToRequestString()
                self.vcCalendarList?.contentType = .ProjectDetail
                self.vcCalendarList?.ID = self.projectId
            }
        }
    }
    

}
