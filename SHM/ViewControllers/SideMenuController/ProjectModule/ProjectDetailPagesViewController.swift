//
//  ProjectDetailPagesViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 11/14/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class ProjectDetailPagesViewController: UIPageViewController
{
    //MARK: VARIABLES AND OUTLETS
    var vcPRDetailPage: PRDetailPageViewController?
    var vcPRFeedPage: PRFeedPageViewController?
    var vcPRCalendarPage: PRCalendarPageViewController?
    var viewControllerList:[UIViewController]?
    
    var projectId = 0
    
    //CLOSURES
    var onDidChangePage:((_ page: Int)->())?
    var onDidLoadProjectLight:((_ project: Project)->())?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.setProjectId()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: CONFIGS
    fileprivate func loadConfig()
    {
        self.dataSource = self
        self.delegate = self
        self.setVCList()
        if let firstViewController = self.viewControllerList?.first {
            
            self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    func setProjectId()
    {
        self.vcPRDetailPage?.projectId = self.projectId
        self.vcPRFeedPage?.projectId = self.projectId
        self.vcPRCalendarPage?.projectId = self.projectId
    }
    
    fileprivate func setVCList()
    {
        let sb = UIStoryboard(name: "Projects", bundle: nil)
        self.vcPRDetailPage = sb.instantiateViewController(withIdentifier: "PRDetailPageViewController") as? PRDetailPageViewController
        self.vcPRDetailPage?.onDidLoadProjectLight = { [weak self] project in
         
            self?.onDidLoadProjectLight?(project)
        }
        self.vcPRFeedPage = sb.instantiateViewController(withIdentifier: "PRFeedPageViewController") as? PRFeedPageViewController                
        self.vcPRCalendarPage = sb.instantiateViewController(withIdentifier: "PRCalendarPageViewController") as? PRCalendarPageViewController
        
        self.viewControllerList = [self.vcPRDetailPage!, self.vcPRFeedPage!, self.vcPRCalendarPage!]
    }
}

extension ProjectDetailPagesViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate
{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        guard let vcIndex = viewControllerList?.index(of: viewController) else {return nil}
        
        let previousIndex = vcIndex - 1
        guard previousIndex >= 0 else {return nil}
        guard (viewControllerList?.count)! > previousIndex else {return nil}
        
        
        return viewControllerList?[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        guard let vcIndex = viewControllerList?.index(of: viewController) else {return nil}
        
        let nextIndex = vcIndex + 1
        guard viewControllerList?.count != nextIndex else {return nil}
        guard (viewControllerList?.count)! > nextIndex else {return nil}
        
        return viewControllerList?[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        guard completed == true else {return}
        if let page = pageViewController.viewControllers?.first?.view.tag {
            
            self.onDidChangePage?(page)
            print(page)
        }
    }
}

