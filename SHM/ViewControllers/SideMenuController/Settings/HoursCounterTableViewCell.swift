//
//  HoursCounterTableViewCell.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 6/9/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class HoursCounterTableViewCell: UITableViewCell
{

    @IBOutlet weak var lblCounter: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    var onCounterUpdate:((_ hours:Int) -> ())?
    fileprivate var hours = 24
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    @IBAction func stepperChange(_ sender: AnyObject)
    {
        if self.stepper.value == 1
        {
            self.stepper.value = 2.0
            self.hours = 48
        }
        else
        {
            self.hours = Int (self.stepper.value) * 24
        }
        self.lblCounter.text = String(hours)
        self.onCounterUpdate?(hours)
    }
    
    func setHours(_ hrs:Int)
    {        
        if hrs % 24 == 0
        {
            self.stepper.value = Double(hrs) / 24.0
            print(self.stepper.value)
            self.hours = hrs
        }
        else
        {
            self.stepper.value = 2.0
            self.hours = 48
        }
        self.lblCounter.text = String(hours)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
