//
//  SettingsViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 8/25/15.
//  Copyright © 2015 Definity First. All rights reserved.
//


import UIKit
import Spruce

enum SettingSection: Int
{
    case pushNotification = 0
    case contactDisplayOrder = 1
    case projectDisplayOrder = 2
    case language = 3
    case stakeholderCreation = 4
    case maximunHoursDueRequest = 5
}

class SettingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var tableSettings: UITableView!
    @IBOutlet weak var navigationItemSettings: UINavigationItem!
    @IBOutlet weak var navItemSettingsFromiPad: UINavigationItem!
    
    private var arrayTitleSections = [String]()
    private var shouldSave = false
    private var isCardManagerOrGlobalManager = false

    
    let arrayContentTitle = [
        "Stakeholder update request".localized(),
        "Stakeholder edition".localized(),
        "Project edition".localized(),
        "New post".localized()
    ]
    
    let arrayOrderName = [
        "Last Name, First Name".localized(),
        "First Name, Last Name".localized()
    ]
    
    let arrayProjectsOrder = [
        "By last update".localized(),
        "By title".localized()
    ]
    
    let arrayLocalManagerSettings = [
        "Immediately".localized(),
        "Daily".localized(),
        "Weekly".localized()
    ]
    
    let arrayLanguages = [
        "English".localized(),
        "Spanish".localized()
    ]
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.loadRemoteSettings()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadHomeFeed"), object: nil)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        print("Deinit: SettingsViewController")
    }
    
    //MARK: LOAD CONFIG
    func loadConfig()
    {
        //Identify if role = global manager or card Manager
        if LibraryAPI.shared.currentUser?.role == UserRole.globalManager || LibraryAPI.shared.currentUser?.role == UserRole.cardManager
        {
            self.isCardManagerOrGlobalManager = true
        }
        
        self.navigationItemSettings.title = "SETTINGS".localized()
        
        //Table background
        self.tableSettings.backgroundColor = UIColor.groupTableViewBackground
        self.tableSettings.register(UINib(nibName: "HoursCounterTableViewCell", bundle: nil), forCellReuseIdentifier: "HoursCounterTableViewCell")
        self.tableSettings.reloadData()
    }
    
    // MARK: - WEB SERVICE
    func loadRemoteSettings()
    {
        //Indicator
        
        self.tableSettings.displayBackgroundMessage("", subMessage: "Loading Settings...".localized())
        
        LibraryAPI.shared.userBO.getUserSettings({ settings in
            
            self.tableSettings.dismissBackgroundMessage()
            
            //Setting Sections
            if LibraryAPI.shared.currentUser?.role == UserRole.localManager
            {
                self.arrayTitleSections = [
                    "SEND PUSH NOTIFICATIONS".localized(),
                    "CONTACT DISPLAY ORDER".localized(),
                    "PROJECTS ORDER".localized(),
                    "LANGUAGE".localized(),
                    "STAKEHOLDERS CREATION ALERT".localized()
                ]
            }
            else if self.isCardManagerOrGlobalManager == true
            {
                self.arrayTitleSections = [
                    "SEND PUSH NOTIFICATIONS".localized(),
                    "CONTACT DISPLAY ORDER".localized(),
                    "PROJECTS ORDER".localized(),
                    "LANGUAGE".localized(),
                    "",
                    "Settings Maximun Hours Due Request".localized()
                ]
            }
            else
            {
                self.arrayTitleSections = [
                    "SEND PUSH NOTIFICATIONS".localized(),
                    "CONTACT DISPLAY ORDER".localized(),
                    "PROJECTS ORDER".localized(),
                    "LANGUAGE".localized()
                ]
            }

            //Load Data
            LibraryAPI.shared.currentUser!.userSettings = settings
            LibraryAPI.shared.userBO.save(LibraryAPI.shared.currentUser!)
            self.tableSettings.reloadData()
            
            //Spruce framework Animation
            let sortFunction = LinearSortFunction(direction: .topToBottom, interObjectDelay: 0.1)
            self.tableSettings.spruce.animate([.fadeIn, .expand(.slightly)], sortFunction: sortFunction)

            
            }) { error in
                
                self.tableSettings.displayBackgroundMessage("Error", subMessage: "Settings not available".localized())
                
                MessageManager.shared.showBar(title: "Error",
                    subtitle: "It hasn't been possible download the user settings".localized(),
                    type: .error,
                    fromBottom: false)
        }
    }
    
    // MARK: TABLE VIEW DATASOURCE
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrayTitleSections.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return arrayTitleSections[section]
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == SettingSection.pushNotification.rawValue
        {
            return 4
        }
        else if section == SettingSection.contactDisplayOrder.rawValue
        {
            return 2
        }
        else if section == SettingSection.projectDisplayOrder.rawValue
        {
            return 2
        }
        else if section == SettingSection.language.rawValue
        {
            return 2
        }
        else if section == SettingSection.stakeholderCreation.rawValue
        {
            return self.isCardManagerOrGlobalManager == true ? 0 : 3
        }
        else if section == SettingSection.maximunHoursDueRequest.rawValue
        {
            return 1
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == SettingSection.stakeholderCreation.rawValue && self.isCardManagerOrGlobalManager == true
        {
            return CGFloat.leastNormalMagnitude
        }
        
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if LibraryAPI.shared.currentUser?.role == UserRole.localManager
        {
            if indexPath.section == SettingSection.pushNotification.rawValue
            {
                let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier:"Cell")
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                let switchNotificaiton = UISwitch()
                switchNotificaiton.onTintColor = UIColor(red: 0.105882, green: 0.333333, blue: 0.596078, alpha: 1 )
                
                switch(indexPath.row)
                {
                case 0:
                    switchNotificaiton.isOn = LibraryAPI.shared.currentUser!.userSettings.notificationCardUpdateActive
                case 1:
                    switchNotificaiton.isOn = LibraryAPI.shared.currentUser!.userSettings.notificationStakeholderEditActive
                case 2:
                    switchNotificaiton.isOn = LibraryAPI.shared.currentUser!.userSettings.notificationProjectEditActive
                case 3:
                    switchNotificaiton.isOn = LibraryAPI.shared.currentUser!.userSettings.notificationNewPostActive
                default:
                    switchNotificaiton.isOn = false
                }
                
                switchNotificaiton.tag = indexPath.row
                switchNotificaiton.addTarget(self, action:#selector(self.switchChanged(_:)), for: .valueChanged)
                
                cell.textLabel?.text = arrayContentTitle[indexPath.row]
                cell.textLabel?.adjustsFontSizeToFitWidth = true
                cell.accessoryView = switchNotificaiton
                
                return cell
            }
            else if indexPath.section == SettingSection.contactDisplayOrder.rawValue
            {
                let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier:"Cell")
                cell.selectionStyle = .none
                
                cell.textLabel?.text = self.arrayOrderName[indexPath.row]
                cell.textLabel?.adjustsFontSizeToFitWidth = true
                
                if (indexPath.row == 0  && UserDefaultsManager.getContactOrder() == false)
                    || (indexPath.row == 1  && UserDefaultsManager.getContactOrder() == true)
                {
                    cell.accessoryType = .checkmark
                }
                else
                {
                    cell.accessoryType = .none
                }
                
                return cell
            }
            else if indexPath.section == SettingSection.projectDisplayOrder.rawValue
            {
                let cell:UITableViewCell = UITableViewCell(style: .default, reuseIdentifier:"Cell")
                cell.selectionStyle = .none
                
                cell.textLabel?.text = self.arrayProjectsOrder[indexPath.row]
                cell.textLabel?.adjustsFontSizeToFitWidth = true
                
                if (indexPath.row == 0  && UserDefaultsManager.getProjectOrder() == false)
                    || (indexPath.row == 1  && UserDefaultsManager.getProjectOrder() == true)
                {
                    cell.accessoryType = .checkmark
                }
                else
                {
                    cell.accessoryType = .none
                }
                
                return cell
            }
            else if indexPath.section == SettingSection.language.rawValue
            {
                let cell:UITableViewCell = UITableViewCell(style: .default, reuseIdentifier:"Cell")
                cell.selectionStyle = .none
                
                cell.textLabel?.text = self.arrayLanguages[indexPath.row]
                cell.textLabel?.adjustsFontSizeToFitWidth = true
                
                if (indexPath.row == 0  && LibraryAPI.shared.currentUser!.userSettings.language == 1)
                    || (indexPath.row == 1  && LibraryAPI.shared.currentUser!.userSettings.language == 2)
                {
                    cell.accessoryType = .checkmark
                }
                else
                {
                    cell.accessoryType = .none
                }
                
                return cell
            }
            else
            {
                //StakeholderCreation (Section 4)
                let cell:UITableViewCell = UITableViewCell(style: .default, reuseIdentifier:"Cell")
                cell.selectionStyle = .none
                
                cell.textLabel?.text = arrayLocalManagerSettings[indexPath.row]
                cell.textLabel?.adjustsFontSizeToFitWidth = true
                
                switch indexPath.row
                {
                case 0:
                    cell.accessoryType = (LibraryAPI.shared.currentUser!.userSettings.stakeholderNotice == 0) ? .checkmark : .none
                case 1:
                    cell.accessoryType = (LibraryAPI.shared.currentUser!.userSettings.stakeholderNotice == 1) ? .checkmark : .none
                case 2:
                    cell.accessoryType = (LibraryAPI.shared.currentUser!.userSettings.stakeholderNotice == 2) ? .checkmark : .none
                default:
                    cell.accessoryType = .none
                    break
                }
                
                return cell
            }
        }
        else
        {
            if indexPath.section == SettingSection.pushNotification.rawValue
            {
                let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier:"Cell")
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                let switchNotificaiton = UISwitch()
                switchNotificaiton.onTintColor = UIColor(red: 0.105882, green: 0.333333, blue: 0.596078, alpha: 1 )
                
                switch(indexPath.row)
                {
                case 0:
                    switchNotificaiton.isOn = LibraryAPI.shared.currentUser!.userSettings.notificationCardUpdateActive
                case 1:
                    switchNotificaiton.isOn = LibraryAPI.shared.currentUser!.userSettings.notificationStakeholderEditActive
                case 2:
                    switchNotificaiton.isOn = LibraryAPI.shared.currentUser!.userSettings.notificationProjectEditActive
                case 3:
                    switchNotificaiton.isOn = LibraryAPI.shared.currentUser!.userSettings.notificationNewPostActive
                default:
                    switchNotificaiton.isOn = false
                }
                
                switchNotificaiton.tag = indexPath.row
                switchNotificaiton.addTarget(self, action:#selector(self.switchChanged(_:)), for: .valueChanged)
                
                cell.textLabel?.text = arrayContentTitle[indexPath.row]
                cell.textLabel?.adjustsFontSizeToFitWidth = true
                cell.accessoryView = switchNotificaiton
                
                return cell
            }
            else if indexPath.section == SettingSection.contactDisplayOrder.rawValue
            {
                let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier:"Cell")
                cell.selectionStyle = .none
                
                cell.textLabel?.text = arrayOrderName[indexPath.row]
                cell.textLabel?.adjustsFontSizeToFitWidth = true
                
                if (indexPath.row == 0  && UserDefaultsManager.getContactOrder() == false)
                    || (indexPath.row == 1  && UserDefaultsManager.getContactOrder() == true)
                {
                    cell.accessoryType = .checkmark
                }
                else
                {
                    cell.accessoryType = .none
                }
                
                return cell
            }
            else if indexPath.section == SettingSection.language.rawValue
            {
                let cell:UITableViewCell = UITableViewCell(style: .default, reuseIdentifier:"Cell")
                cell.selectionStyle = .none
                
                cell.textLabel?.text = self.arrayLanguages[indexPath.row]
                cell.textLabel?.adjustsFontSizeToFitWidth = true
                
                if (indexPath.row == 0  && LibraryAPI.shared.currentUser!.userSettings.language == 1)
                    || (indexPath.row == 1  && LibraryAPI.shared.currentUser!.userSettings.language == 2)
                {
                    cell.accessoryType = .checkmark
                }
                else
                {
                    cell.accessoryType = .none
                }
                
                return cell
            }

                
            else if indexPath.section == SettingSection.maximunHoursDueRequest.rawValue
            {
                if let cell = self.tableSettings.dequeueReusableCell(withIdentifier: "HoursCounterTableViewCell") as? HoursCounterTableViewCell {
                    
                    let hrs = LibraryAPI.shared.currentUser!.userSettings.appSettings.maxHoursToDueDateInRequests
                    cell.setHours(hrs)                    
                    cell.selectionStyle = .none
                    cell.onCounterUpdate = { [weak self] hours in
                        
                        cell.setHours(hours)
                        LibraryAPI.shared.currentUser!.userSettings.appSettings.maxHoursToDueDateInRequests = hours
                        self?.save()
                    }
                    
                    return cell
                }
                
                return UITableViewCell()
            }
            else
            {
                let cell:UITableViewCell = UITableViewCell(style: .default, reuseIdentifier:"Cell")
                cell.selectionStyle = .none
                
                cell.textLabel?.text = arrayProjectsOrder[indexPath.row]
                cell.textLabel?.adjustsFontSizeToFitWidth = true
                
                if (indexPath.row == 0  && UserDefaultsManager.getProjectOrder() == false)
                    || (indexPath.row == 1  && UserDefaultsManager.getProjectOrder() == true)
                {
                    cell.accessoryType = .checkmark
                }
                else
                {
                    cell.accessoryType = .none
                }
                
                return cell
            }
        }
    }

    // MARK: TABLE VIEW DELEGATE
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == SettingSection.contactDisplayOrder.rawValue
        {
            let cellSelected = tableView.cellForRow(at: indexPath)!
            
            if indexPath.row == 0
            {
                if cellSelected.accessoryType == .none
                {
                    cellSelected.accessoryType = .checkmark
                    tableView.cellForRow(at: IndexPath(row: 1, section: indexPath.section))?.accessoryType = .none
                    
                    UserDefaultsManager.setContactOrder(false)
                    self.save()
                }
            }
            else
            {
                if cellSelected.accessoryType == .none
                {
                    cellSelected.accessoryType = .checkmark
                    tableView.cellForRow(at: IndexPath(row: 0, section: indexPath.section))?.accessoryType = .none
                    
                    UserDefaultsManager.setContactOrder(true)
                    self.save()
                }
            }
        }
        
        if indexPath.section == SettingSection.projectDisplayOrder.rawValue
        {
            let cellSelected = tableView.cellForRow(at: indexPath)!
            
            if indexPath.row == 0
            {
                if cellSelected.accessoryType == .none
                {
                    cellSelected.accessoryType = .checkmark
                    tableView.cellForRow(at: IndexPath(row: 1, section: indexPath.section))?.accessoryType = .none
                    
                    UserDefaultsManager.setProjectOrder(false)
                    self.save()
                }
            }
            else
            {
                if cellSelected.accessoryType == .none
                {
                    cellSelected.accessoryType = .checkmark
                    tableView.cellForRow(at: IndexPath(row: 0, section: indexPath.section))?.accessoryType = .none
                    
                    UserDefaultsManager.setProjectOrder(true)
                    self.save()
                }
            }
        }
 ////
        if indexPath.section == SettingSection.language.rawValue
        {
            let cellSelected = tableView.cellForRow(at: indexPath)!
            
            if indexPath.row == 0
            {
                if cellSelected.accessoryType == .none
                {
                    cellSelected.accessoryType = .checkmark
                    tableView.cellForRow(at: IndexPath(row: 1, section: indexPath.section))?.accessoryType = .none
                    
                    LibraryAPI.shared.currentUser!.userSettings.language = 1
                    self.save()
                }
            }
            else
            {
                if cellSelected.accessoryType == .none
                {
                    cellSelected.accessoryType = .checkmark
                    tableView.cellForRow(at: IndexPath(row: 0, section: indexPath.section))?.accessoryType = .none
                    
                    LibraryAPI.shared.currentUser!.userSettings.language = 2
                    self.save()
                }
            }
        }
////
        
        if indexPath.section == SettingSection.stakeholderCreation.rawValue
        {
            let cellSelected = tableView.cellForRow(at: indexPath)!
            
            if indexPath.row == 0
            {
                if cellSelected.accessoryType == .none
                {
                    cellSelected.accessoryType = .checkmark
                    tableView.cellForRow(at: IndexPath(row: 1, section: indexPath.section))?.accessoryType = .none
                    tableView.cellForRow(at: IndexPath(row: 2, section: indexPath.section))?.accessoryType = .none
                    
                    LibraryAPI.shared.currentUser!.userSettings.stakeholderNotice = indexPath.row
                    LibraryAPI.shared.userBO.save(LibraryAPI.shared.currentUser!)
                    self.save()
                }
            }
            else if indexPath.row == 1
            {
                if cellSelected.accessoryType == .none
                {
                    cellSelected.accessoryType = .checkmark
                    tableView.cellForRow(at: IndexPath(row: 0, section: indexPath.section))?.accessoryType = .none
                    tableView.cellForRow(at: IndexPath(row: 2, section: indexPath.section))?.accessoryType = .none
                    
                    LibraryAPI.shared.currentUser!.userSettings.stakeholderNotice = indexPath.row
                    LibraryAPI.shared.userBO.save(LibraryAPI.shared.currentUser!)
                    self.save()
                }
            }
            else
            {
                if cellSelected.accessoryType == .none
                {
                    cellSelected.accessoryType = .checkmark
                    tableView.cellForRow(at: IndexPath(row: 0, section: indexPath.section))?.accessoryType = .none
                    tableView.cellForRow(at: IndexPath(row: 1, section: indexPath.section))?.accessoryType = .none
                    
                    LibraryAPI.shared.currentUser!.userSettings.stakeholderNotice = indexPath.row
                    LibraryAPI.shared.userBO.save(LibraryAPI.shared.currentUser!)
                    self.save()
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return CGFloat.leastNormalMagnitude
    }
    
    // MARK: ACTIONS
    func switchChanged(_ switchState: UISwitch)
    {
        switch switchState.tag
        {
        case 0:
            LibraryAPI.shared.currentUser?.userSettings.notificationCardUpdateActive = switchState.isOn
            self.save()
        case 1:
            LibraryAPI.shared.currentUser?.userSettings.notificationStakeholderEditActive = switchState.isOn
            self.save()
        case 2:
            LibraryAPI.shared.currentUser?.userSettings.notificationProjectEditActive = switchState.isOn
            save()
        case 3:
            LibraryAPI.shared.currentUser?.userSettings.notificationNewPostActive = switchState.isOn
            self.save()
        default:
            break
        }
    }
    
    func save()
    {
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        //Object to request
        let wsObject: Dictionary<String, Any>  = [
            "PushPublishedUpdate" : LibraryAPI.shared.currentUser!.userSettings.notificationCardUpdateActive,
            "PushEditProject" : LibraryAPI.shared.currentUser!.userSettings.notificationProjectEditActive,
            "PushEditStakeholder" : LibraryAPI.shared.currentUser!.userSettings.notificationStakeholderEditActive,
            "PushNewPost" : LibraryAPI.shared.currentUser!.userSettings.notificationNewPostActive,
            "StakeholderNameOrder" : UserDefaultsManager.getContactOrder() == true ? 2 : 1,
            "ProjectListOrder" : UserDefaultsManager.getProjectOrder() == true ? 2 : 1,
            "StakeholderNotice" : LibraryAPI.shared.currentUser!.userSettings.stakeholderNotice,
            "Language" : LibraryAPI.shared.currentUser!.userSettings.language,
            "AppSettings" : LibraryAPI.shared.currentUser!.userSettings.appSettings.getWSObject() ]

        
        LibraryAPI.shared.userBO.saveUserSettings(wsObject, onSuccess: { Void in
                
                //Sucess Indicator
                MessageManager.shared.showSuccessHUD()
                
                let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    
                    MessageManager.shared.hideHUD()
                    
                    let langSaved = LibraryAPI.shared.currentUser!.userSettings.language == 2 ? "es" : "en"
                    if let lang = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) {
                    
                        if lang != langSaved
                        {
                            UserDefaults.standard.setValue(langSaved, forKey: Constants.UserKey.BackupLanguage)
                            UserDefaults.standard.synchronize()
                            
                            LibraryAPI.shared.customLocale.loadTranslations()
                            LibraryAPI.shared.customLocale.exitApp = { [weak self] shouldExit in
                                
                                //Language
                                let message = "The language changes will take place next time you load the app. Would you like to close the app now?".localized()
                                let alert = UIAlertController(title:"Language Change".localized(), message: message, preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title:"No".localized(), style: .cancel, handler:nil))
                                alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (action) in
                                    
                                    exit(0)
                                }))
                                
                                self?.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    LibraryAPI.shared.userBO.save(LibraryAPI.shared.currentUser!)
            }
            
            }) { error in
                
                //Error Indicator
                MessageManager.shared.showErrorHUD()
                
                let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    
                    MessageManager.shared.hideHUD()
                    
                    MessageManager.shared.showBar(title: "Error",
                        subtitle:"There was an error trying to save user settings. Try again".localized(),
                        type: .error,
                        fromBottom: false)
                }
        }
    }
}
