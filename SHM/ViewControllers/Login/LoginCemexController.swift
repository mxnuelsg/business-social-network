//
//  LoginCemexController.swift
//  BCM
//
//  Created by Manuel Salinas on 2/10/18.
//  Copyright © 2018 Nestor Javier Hernandez Bautista. All rights reserved.
//

import UIKit
import WebKit
import SwiftMessages


enum LoginFrom
{
    case launchApp
    case expiredToken
}

//Statics
let kHome = "https://ermlogin.cemex.com/login/ce5172ae67934ae890541b84ba6beb35"
let kHome_Dev = "http://login.definity.solutions/Login/SHMQA"
let kHome_UAT = "http://login.definity.solutions/Login/SHMUAT"

let kUsername = "cx-user-login-identity"
let kCemexToken = "cx-login-access-token-identity"

class LoginCemexController: UIViewController
{
    //MARK: OUTLETS & VARIBALES
    @IBOutlet weak fileprivate var webView: WKWebView!
    
    var screenType: LoginFrom = .launchApp
    var onCemexLoginSuccess:(() -> ())?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.checkVersion()
    }

    deinit
    {
        print("Deinit: LoginCemexController")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "reloadLogin"), object: nil)
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        //Observer
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "reloadLogin"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.reloadLogin), name: NSNotification.Name(rawValue: "reloadLogin"), object: nil)

        //Backgounds
        self.webView.backgroundColor = UIColor(red:0.078856, green: 0.121084, blue: 0.195569, alpha: 1)
        self.webView.scrollView.backgroundColor = UIColor(red:0.078856, green: 0.121084, blue: 0.195569, alpha: 1)
        
        //Open web page
        let mode = UserDefaultsManager.getModeEnviroment()
        let url = URL(string: mode == 1 ? kHome_Dev : mode == 2 ? kHome_UAT : kHome)
        let request = URLRequest(url: url!)
        self.webView.navigationDelegate = self
        self.webView.load(request)
    }
    
    //MARK: ACTIONS NOFITICATION
    @objc fileprivate func reloadLogin()
    {
        let mode = UserDefaultsManager.getModeEnviroment()
        if mode < 3 {
            let url = URL(string: mode == 0 ? kHome : mode == 1 ? kHome_Dev : kHome_UAT)
            let request = URLRequest(url: url!)
            self.webView.load(request)
        }
    }
    
    //MARK: ACTIONS
    fileprivate func requestLogin(_ username: String, _ password: String)
    {
        self.view.endEditing(true)
        
        //Send Credentials
        let user = User()
        user.userName = username
        //user.uuid = UserDefaultsManager.getUUID() ?? String()
        //user.deviceToken = UserDefaultsManager.getDeviceToken() ?? String()
        //user.voipDeviceToken = UserDefaultsManager.getVoipDeviceToken() ?? String()
        user.cemexToken = password
        
        /*Temporary*/
        //user.password = "naranja123"
        
        //Call WS
        MessageManager.shared.showLoadingHUD()
        LibraryAPI.shared.userBO.doLogin(user, onSuccess: {
            
            //Sucess Indicator
            MessageManager.shared.showSuccessHUD()
            
            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                MessageManager.shared.hideHUD()
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "userDidLogin"), object: nil)
                self.onCemexLoginSuccess?()
                let delayTimeAfeterHide = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTimeAfeterHide) {
                    
                    
                    self.dismiss(animated: true, completion: nil)
                }
            }
            
        }) { error in
            
            //Error Indicator
            MessageManager.shared.showErrorHUD()
                
            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    
                    MessageManager.shared.hideHUD()
            }
            
        }
    }
    
    fileprivate func checkVersion() {
//        let isVersionUpToDate = UserDefaultsManager.getAppUpdateStatus()
//        if(isVersionUpToDate == false) {
//            
//            let alertVersion = UIAlertController(title: "NEW VERSION AVAILABLE".localized(), message: "Please click Ok to download the new version of the APP".localized(), preferredStyle: .alert)
//            let actionOK = UIAlertAction(title: "Ok".localized(), style: .cancel, handler: { (action) in
//
//                if let url = URL(string: "http://definityfirst.com") {
//                    UserDefaultsManager.saveAppUpdateStatus(isUpToDate: true)
//                    UIApplication.shared.open(url, options: [:])
//                }
//            })
//            alertVersion.addAction(actionOK)
//            self.present(alertVersion, animated: true, completion: nil)
//        }
    }
}

//MARK: WKWEBVIEW DELEGATE
extension LoginCemexController: WKNavigationDelegate
{
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void)
    {
        //Error response handle
        guard let urlResponse = navigationResponse.response as? HTTPURLResponse, let url = urlResponse.url  else {
            
            decisionHandler(.cancel)
            return
        }
        
        // * * * * * DEVELOPMENT QA
        let mode = UserDefaultsManager.getModeEnviroment()
        
        guard (mode == 0 || mode == 2) && url.absoluteString != kHome_Dev else {
            
            //Make sure It's our web site
            if url.absoluteString != kHome_Dev
            {
                let cookieStore = webView.configuration.websiteDataStore.httpCookieStore
                cookieStore.getAllCookies { cookie in
                    
                    var username = String()
                    var bcmToken = String()
                    
                    for ck in cookie
                    {
                        if ck.name == kUsername
                        {
                            username = ck.value
                        }
                        
                        if ck.name == kCemexToken
                        {
                            bcmToken = ck.value
                        }
                    }
                    
                    //Request login
                    if username.isEmpty == false && bcmToken.isEmpty == false
                    {
                        UserDefaultsManager.saveJWT(bcmToken)
                        self.requestLogin(username, bcmToken)
                    }
                    else
                    {
                        MessageManager.shared.showBar(title: "the authentication is returning an incomplete data credentials. Please contact your system administrator".localized(), type: .error, fromBottom: false)
                    }
                }
                
                decisionHandler(.cancel)
            }
            else
            {
                decisionHandler(.allow)
            }
            
            return
        }
        
        // * * * * * DEVELOPMENT UAT
        guard (mode == 0 || mode == 1) && url.absoluteString != kHome_UAT else {
            
            //Make sure It's our web site
            if url.absoluteString != kHome_UAT
            {
                let cookieStore = webView.configuration.websiteDataStore.httpCookieStore
                cookieStore.getAllCookies { cookie in
                    
                    var username = String()
                    var bcmToken = String()
                    
                    for ck in cookie
                    {
                        if ck.name == kUsername
                        {
                            username = ck.value
                        }
                        
                        if ck.name == kCemexToken
                        {
                            bcmToken = ck.value
                        }
                    }
                    
                    //Request login
                    if username.isEmpty == false && bcmToken.isEmpty == false
                    {
                        UserDefaultsManager.saveJWT(bcmToken)
                        self.requestLogin(username, bcmToken)
                    }
                    else
                    {
                        MessageManager.shared.showBar(title:"the authentication is returning an incomplete data credentials. Please contact your system administrator".localized(), type: .error, fromBottom: false)
                    }
                }
                
                decisionHandler(.cancel)
            }
            else
            {
                decisionHandler(.allow)
            }
            
            return
        }
        
        // * * * * * PRODUCTION    (Make sure It's our web site)
        if url.absoluteString != kHome
        {
            let cookieStore = webView.configuration.websiteDataStore.httpCookieStore
            cookieStore.getAllCookies { cookie in
                
                var username = String()
                var cemexToken = String()
                
                for ck in cookie
                {
                    if ck.name == kUsername
                    {
                        username = ck.value
                    }
                    
                    if ck.name == kCemexToken
                    {
                        cemexToken = ck.value
                    }
                }
                
                //Request login
                if username.isEmpty == false && cemexToken.isEmpty == false
                {
                    UserDefaultsManager.saveJWT(cemexToken)
                    self.requestLogin(username, cemexToken)
                }
                else
                {                   
                    MessageManager.shared.showBar(title: nil, subtitle: "the authentication is returning an incomplete data credentials. Please contact your system administrator".localized(), type: .error, containsIcon: false, fromBottom: false)
                }
            }
            
            decisionHandler(.cancel)
        }
        else
        {
            decisionHandler(.allow)
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!)
    {
        //TODO: Comment when production is activated
        if let url = webView.url {
         
            //Show message
            
            let title = url.absoluteString == kHome_Dev ? "BCM QA Login".localized() : url.absoluteString == kHome_UAT ? "BCM UAT Login".localized() : url.absoluteString == kHome ? "Cemex Login".localized() : "invalid web".localized()
            
                MessageManager.shared.showBar(title: title, subtitle: "has been loaded".localized(), type: .info, containsIcon: false, fromBottom: false)
        }
    }
}

//MARK: PINCH ACTION
extension LoginCemexController: UIGestureRecognizerDelegate
{
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool
    {
        //TODO: Comment when production is activated
        let vsServerList: ModalServers = Storyboard.getInstanceFromStoryboard("popups")
        vsServerList.servers = Servers.shared.servers
        vsServerList.modalPresentationStyle = .overCurrentContext

        self.present(vsServerList, animated: true, completion: nil)
        
        return true
    }
}
