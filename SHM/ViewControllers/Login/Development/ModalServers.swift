//
//  ModalServers.swift
//  BCM
//
//  Created by Manuel Salinas on 9/13/16.
//  Copyright © 2016 Nestor Javier Hernandez Bautista. All rights reserved.
//

import UIKit

class ModalServers: UIViewController
{
    //MARK: VARIABLES & OUTLETS
    @IBOutlet weak fileprivate var btnTitle: UIBarButtonItem!
    @IBOutlet weak fileprivate var tableServers: UITableView!
    @IBOutlet weak fileprivate var viewContainer: UIView!
    @IBOutlet weak fileprivate var segmentedMode: UISegmentedControl!
    
    var servers = [String]() {
        didSet{
            
            if self.tableServers !=  nil
            {
                self.tableServers.reloadData()
            }
        }
    }
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    deinit
    {
        print("Deinit: ModalServers")
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        //Titles
        self.btnTitle.title = "Servers".localized()
        
        //Segmented
        self.segmentedMode.removeAllSegments()
        self.segmentedMode.insertSegment(withTitle: "Cemex", at: 0, animated: false)
        self.segmentedMode.insertSegment(withTitle: "QA", at: 1, animated: false)
        self.segmentedMode.insertSegment(withTitle: "UAT", at: 2, animated: false)
        self.segmentedMode.insertSegment(withTitle: "Old login", at: 3, animated: false)
        
        let mode = UserDefaultsManager.getModeEnviroment()
        self.segmentedMode.selectedSegmentIndex = mode

        //Container and view
        self.view.backgroundColor = .clear
        //self.viewContainer.roundCorners([.allCorners])
        
        //Table Config
        self.tableServers.hideEmtpyCells()
        self.tableServers.reloadData()
    }
    
    //MARK: ACTIONS
    @IBAction func close(_ sender: UIBarButtonItem)
    {
        guard Servers.shared.defaultServer.trim().isEmpty == false else {
            
            MessageManager.shared.showBar(title: nil, subtitle: "select a server".localized(), type: .warning, containsIcon: false, fromBottom: false)
            
            return
        }
        
        Servers.shared.update(self.servers)
        
        self.viewContainer.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func current(_ sender: UIBarButtonItem)
    {
        let alert = UIAlertController(title: "Current Server".localized(),
                                      message: Servers.shared.defaultServer,
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Close".localized(), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func add(_ sender: UIBarButtonItem)
    {
        let alert = UIAlertController(title: "Create New Server".localized(),
                                      message: nil,
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Add".localized(), style: .default, handler: { (action) in
            
            let textF = alert.textFields![0] as UITextField
            
            guard var newServer = textF.text , newServer.isEmpty == false else {
                
                let alertEmpty = UIAlertController(title: "Warning".localized(),
                                                   message: "Empty field is not valid".localized(),
                                                   preferredStyle: .alert)
                
                alertEmpty.addAction(UIAlertAction(title: "Close".localized(), style: .default, handler: nil))
                
                self.present(alertEmpty, animated: true, completion: nil)
                
                return
            }
            
            if newServer.hasSuffix("/") == true
            {
                self.servers.append(newServer)
            }
            else
            {
                newServer += "/"
                self.servers.append(newServer)
            }
            
            Servers.shared.defaultServer = newServer
            Servers.shared.update(self.servers)
        }))
        
        alert.addTextField { textField in
            
            textField.text = "http://"
            textField.placeholder = "http://myserver.com/"
            textField.clearButtonMode = .whileEditing
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func segmentedControlValueChanged(_ sender: UISegmentedControl)
    {
        //Set url automatically
        switch sender.selectedSegmentIndex
        {
        case 0:
            Servers.shared.update(Servers.Server.Prod.rawValue)
        case 1:
            Servers.shared.update(Servers.Server.QA.rawValue)
        case 2:
            Servers.shared.update(Servers.Server.UAT.rawValue)
        default:
            break
        }
        
        //Save in User Defults
        UserDefaultsManager.saveModeEnviroment(sender.selectedSegmentIndex)
        
        //Reload Home Page
        NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadLogin"), object: nil)
        self.tableServers.reloadData()
    }
}

//MARK: TABLE VIEW DELEGATE & DATASOURCE
extension ModalServers: UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.servers.count > 0
        {
            tableView.dismissBackgroundMessage()
        }
        else
        {
            tableView.displayBackgroundMessage("No Servers".localized(),
                                               subMessage: "Press '+' to add".localized())
        }
        
        return self.servers.count
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return indexPath.row > 2 ? true : false
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        // ACTION
        let actionDelete = UITableViewRowAction(style: .destructive, title: "Delete".localized() , handler: { (action:UITableViewRowAction!, indexPath:IndexPath!) -> Void in
            
            tableView.setEditing(false, animated: true)
            
            //Action ..
            if self.servers[indexPath.row] == Servers.shared.defaultServer
            {
                Servers.shared.defaultServer = String()
            }
            
            self.servers.remove(at: indexPath.row)
            Servers.shared.update(self.servers)
        })
                
        return [actionDelete]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style:.default, reuseIdentifier:"Cell")
        let server = self.servers[indexPath.row]
        cell.selectionStyle = .blue
        cell.textLabel!.font = UIFont.boldSystemFont(ofSize: 11)
        cell.textLabel!.lineBreakMode = .byTruncatingMiddle
        cell.textLabel!.numberOfLines = 2
        
        cell.textLabel?.text = server
        cell.accessoryType = server == Servers.shared.defaultServer ? .checkmark : .none
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        Servers.shared.update(self.servers[indexPath.row])
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        // Remove seperator inset
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset))
        {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))
        {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.responds(to: #selector(setter: UIView.layoutMargins))
        {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
}
