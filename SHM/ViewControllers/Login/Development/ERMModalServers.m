//
//  ERMModalServers.m
//  ERM
//
//  Created by Manuel Salinas on 12/8/15.
//  Copyright © 2015 Manuel Salinas Gonzalez. All rights reserved.
//

#import "ERMModalServers.h"
#import "ERMServer.h"

@interface ERMModalServers () <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic, strong) NSMutableArray *arrayURLs;


@end

@implementation ERMModalServers

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.arrayURLs = [NSMutableArray arrayWithArray:[ERMServer info].arrayServers];
    }
    return self;
}

#pragma mark -
#pragma mark - LIFE CYCLE
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.containerView.layer.cornerRadius = 5;
    self.containerView.clipsToBounds = YES;
    
    [self.myTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    
    [UIView animateWithDuration:1
                     animations:^{
                         self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
                     }];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [UIView animateWithDuration:0.1
                     animations:^{
                         self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
                     }];
}

-(void)dealloc
{
    self.arrayURLs = nil;
}

#pragma mark -
#pragma mark - CANCEL ACTION
- (IBAction)cancelAction
{
    [[ERMServer info] updateServerList:self.arrayURLs];
    
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)showInfo
{
    UIAlertView *alertInfo = [[UIAlertView alloc] initWithTitle:@"Current Server"
                                                              message:[ERMServer info].defaultServer
                                                             delegate:nil
                                                    cancelButtonTitle:@"Close"
                                                    otherButtonTitles:nil];
    [alertInfo show];
}

- (IBAction)addServer
{
    UIAlertView *alertForgotPassword = [[UIAlertView alloc] initWithTitle:@"Enter a server with the character '/' at the end"
                                                                  message:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancel"
                                                        otherButtonTitles:@"Add", nil];
    
    alertForgotPassword.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    UITextField *txt = [alertForgotPassword textFieldAtIndex:0];
    txt.text = @"http://";
    txt.keyboardType = UIKeyboardTypeURL;
    [alertForgotPassword show];
}

#pragma mark -
#pragma mark - DELEGATE METHODS (UIAlertView)
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        NSString *url = [alertView textFieldAtIndex:0].text;
        
        if (self.arrayURLs.count > 0)
        {
            for (NSString *server in self.arrayURLs)
            {
                if ([server isEqualToString:url] == YES)
                {
                    UIAlertView *alertDuplicated = [[UIAlertView alloc] initWithTitle:@"Info"
                                                                            message:[NSString stringWithFormat:@"The server already exists.\nVerify the list"]
                                                                           delegate:nil
                                                                  cancelButtonTitle:@"Close"
                                                                  otherButtonTitles:nil];
                    [alertDuplicated show];
                    break;
                }
                else
                {
                    if ([url hasSuffix:@"/"] == YES)
                    {
                        [self.arrayURLs addObject:url];
                        [self.myTableView reloadData];
                        
                        break;
                    }
                    else
                    {
                        UIAlertView *alertWarning = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                                               message:@"The server hasn't the '/' at the end. Try again!"
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"Close"
                                                                     otherButtonTitles: nil];
                        [alertWarning show];
                        break;
                    }
                }
            }
        }
        else
        {
            if ([url hasSuffix:@"/"] == YES)
            {
                [self.arrayURLs addObject:url];
                [self.myTableView reloadData];
            }
            else
            {
                UIAlertView *alertWarning = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                                       message:@"The server hasn't the '/' at the end. Try again!"
                                                                      delegate:nil
                                                             cancelButtonTitle:@"Close"
                                                             otherButtonTitles: nil];
                [alertWarning show];
            }
        }
    }
}

#pragma mark -
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection: (NSInteger)section
{
    return (self.arrayURLs.count > 0) ? self.arrayURLs.count : 1;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath: (NSIndexPath *)indexPath
{
    return  (self.arrayURLs.count > 0) ? YES : NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle: (UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        // Delete the row from the data source
        [self.arrayURLs removeObjectAtIndex:indexPath.row];
        [self.myTableView reloadData];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.arrayURLs.count > 0)
    {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        //Config
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.textLabel.font = [UIFont boldSystemFontOfSize:11];
        cell.textLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        cell.textLabel.numberOfLines = 2;
        cell.textLabel.textColor = [UIColor colorWithRed: 0.133333 green: 0.419608 blue: 0.666667 alpha: 1 ];
         cell.textLabel.textAlignment = NSTextAlignmentNatural;
        
        //Text
        cell.textLabel.text = [self.arrayURLs objectAtIndex:indexPath.row];
        
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        //Config
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont systemFontOfSize:12];
        cell.textLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        cell.textLabel.numberOfLines = 1;
        cell.textLabel.textColor = [UIColor grayColor];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        
        //Text
        cell.textLabel.text = @"Nothing... 😅";
        
        return cell;
    }
}

#pragma mark -
#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.arrayURLs.count > 0)
    {
        NSLog(@"Server Selected: %@", [self.arrayURLs objectAtIndex:indexPath.row]);
        
        [[ERMServer info] updateServer:[self.arrayURLs objectAtIndex:indexPath.row]];
        [[ERMServer info] updateServerList:self.arrayURLs];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)tableView: (UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath: (NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection: (NSInteger)section
{
    return [UIView new];
}

#pragma mark -
#pragma mark - TAP GESTURE
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self cancelAction];
}



@end
