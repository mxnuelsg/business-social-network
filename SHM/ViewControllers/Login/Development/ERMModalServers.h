//
//  ERMModalServers.h
//  ERM
//
//  Created by Manuel Salinas on 12/8/15.
//  Copyright © 2015 Manuel Salinas Gonzalez. All rights reserved.
//  ************** Only for Development Usage ******************

#import <UIKit/UIKit.h>

@interface ERMModalServers : UIViewController

- (IBAction)addServer;
- (IBAction)showInfo;
- (IBAction)cancelAction;

@end
