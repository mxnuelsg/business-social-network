//
//  LoginViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 7/28/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import LocalAuthentication

class LoginViewController: UIViewController, UITextFieldDelegate
{
    @IBOutlet weak fileprivate var txtUsername: UITextField!
    @IBOutlet weak fileprivate var txtPassword: UITextField!
    @IBOutlet weak fileprivate var btnLogin: UIButton!
    @IBOutlet weak fileprivate var lblVersion: UILabel!
    @IBOutlet weak fileprivate var lblSubTitle: UILabel!
    @IBOutlet weak fileprivate var btnTouchID: UIButton!
    
    @IBOutlet var txtCommentForRequestUpdate: UITextField!
    
    var userApp = User()
    var onLoginSuccessful: (() -> ())!
    var strVersion:String?
    // MARK: - LIFE CLYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        print("Deinit: Login")
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        self.authenticateWithTouchID()
    }
    
    // MARK: INITIAL CONFIG
    func loadConfig()
    {
        //Localizations
        self.localize()
        
        //Login
        self.btnLogin.layer.cornerRadius = 3
        self.txtPassword.placeholder = "PASSWORD".localized()
        
        self.lblSubTitle.textColor = UIColor.whiteColor(0.85)
        
        let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        self.strVersion = version
        lblVersion.text = "© 2017 SHM v\(self.strVersion ?? "")"
        
        //MessageManager.shared.showBar(title: "Important".localized(), subtitle: "Ensure you have VPN enabled".localized(), type: .info, fromBottom: false)
    }
    
    func localize()
    {
        self.btnLogin.setTitle("Log In".localized(), for: UIControlState())
        self.txtUsername.placeholder = "USERNAME".localized()
        self.txtPassword.placeholder = "PASSWORD".localized()
        self.lblSubTitle.text = "Stakeholder Management".localized()
    }
    
    //MARK: ACTIONS
    @IBAction func openTouchID()
    {
        //Hide Touch ID Button
        OperationQueue.main.addOperation({
            
            UIView.animate(withDuration: 0.5, animations: {
                
                self.btnTouchID.alpha = 0
                self.btnTouchID.isUserInteractionEnabled = false
            })
        })
        
        self.authenticateWithTouchID()
    }
    
    // MARK: - WEB SERVICE
    @IBAction func accessLogin()
    {
        view.endEditing(true)
        
        guard let username = txtUsername.text, let password = txtPassword.text, username.isEmpty == false && password.isEmpty == false else {
           
            self.txtUsername.shakeAnimation()
            self.txtPassword.shakeAnimation()
            
            return
        }
        
        let user = User()
        user.userName = username
        user.password = password
        
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        //Call WS
        LibraryAPI.shared.userBO.doLogin(user, onSuccess: {
            
            //Sucess Indicator
            MessageManager.shared.showSuccessHUD()
            
            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                MessageManager.shared.hideHUD()
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "userDidLogin"), object: nil)
                
                let delayTimeAfeterHide = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTimeAfeterHide) {
                    
                    self.onLoginSuccessful()
                    self.dismiss(animated: true, completion: nil)                    
                }
            }
            
            }) { error in
                
                //Code 409 means app version is not compatible with server version
                if error.code == 409
                {
                    var urlToDownloadVersion = String()
                    
                    if let url = error.userInfo[Constants.UserKey.UrlVersion] {
                        urlToDownloadVersion = url as! String
                    }

                    MessageManager.shared.hideHUD()
                    
                    let alertVersion = UIAlertController(title: "NEW VERSION AVAILABLE".localized(), message: "Please click Ok to download the new version of the APP".localized(), preferredStyle: .alert)
                    
                    let actionOK = UIAlertAction(title: "Ok".localized(), style: .cancel, handler: { (action) in
                        
                        if let url = URL(string: "itms-services://?action=download-manifest&url=https://erm.cemex.com:8081/apps/SHM.plist") {//urlToDownloadVersion) {
                            
                    
                            if UIApplication.shared.canOpenURL(url) == true
                            {
                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                            }
                        }
                    })

                    alertVersion.addAction(actionOK)
                    
                    self.present(alertVersion, animated: true, completion: nil)
                }
                else
                {
                    //Error Indicator
                    MessageManager.shared.showErrorHUD()
                    
                    let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: delayTime) {
                        
                        MessageManager.shared.hideHUD()
                    }
                }
        }
    }
}


//MARK: PINCH ACTION
extension LoginViewController: UIGestureRecognizerDelegate
{
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool
    {
        //TODO: Comment when production is activated
        let vsServerList: ModalServers = Storyboard.getInstanceFromStoryboard("popups")
        vsServerList.servers = Servers.shared.servers
        vsServerList.modalPresentationStyle = .overCurrentContext
        
        self.present(vsServerList, animated: true, completion: nil)
        
        return true
    }
}

//MARK: TOUCH ID
extension LoginViewController
{
    func authenticateWithTouchID()
    {
        // Get the local authentication context.
        let localAuthContext = LAContext()
        let reasonText = "Authentication is required to sign in SHM".localized()
        var authError: NSError?
        
        if localAuthContext.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &authError) == false
        {
            if let error = authError {
                
                print(error.localizedDescription)
            }
            return
        }
        
        //Verify Account has been logged at least once
        guard let user = LibraryAPI.shared.currentUser else {
            return
        }
        
        guard user.userName?.isEmpty == false && user.password.isEmpty == false else {
            return
        }
        
        // Perform the Touch ID authentication
        localAuthContext.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonText, reply: { (success: Bool, error: Error?) -> Void in
            
            // Failure workflow
            if success == true
            {
                // Fallback to password authentication
                OperationQueue.main.addOperation({
                    
                    self.txtUsername.text = user.userName
                    self.txtPassword.text = user.password
                })
                
                let time = DispatchTime.now() + 1 //seconds
                DispatchQueue.main.asyncAfter(deadline: time) {
                    
                    self.accessLogin()
                }
            }
            else
            {
                
                var info = String()
                
                if let error = error {
                    
                    switch error
                    {
                    case LAError.authenticationFailed:
                        info = "Authentication failed".localized()
                    case LAError.passcodeNotSet:
                        info = "Passcode not set".localized()
                    case LAError.systemCancel:
                        //                        info = "Authentication was canceled by system".localized()
                        break
                    case LAError.userCancel:
                        //                        info = "Authentication was canceled by the user".localized()
                        break
                    case LAError.touchIDNotEnrolled:
                        info = "Authentication could not start because Touch ID has no enrolled fingers".localized()
                    case LAError.touchIDNotAvailable:
                        info = "Authentication could not start because Touch ID is not available".localized()
                    case LAError.userFallback:
                        //                        info = String()
                        break
                    default:
                        info = error.localizedDescription
                    }
                }
                
                //Show error message
                if info.isEmpty == false
                {
                    MessageManager.shared.showBar(title: "".localized(), subtitle: info, type: .info, fromBottom: false)
                }
                
                //Show Touch ID Button
                OperationQueue.main.addOperation({
                    
                    UIView.animate(withDuration: 1.0, animations: {
                        
                        self.btnTouchID.alpha = 1
                        self.btnTouchID.isUserInteractionEnabled = true
                    })
                })
            }
        })
        
    }
}



