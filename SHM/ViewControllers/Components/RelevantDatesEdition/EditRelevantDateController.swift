//
//  EditRelevantDateController.swift
//  SHM
//
//  Created by Manuel Salinas on 5/27/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class EditRelevantDateController: UIViewController
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak fileprivate var btnFreeText: UIButton!
    @IBOutlet weak fileprivate var btnSave: UIButton!
    @IBOutlet weak fileprivate var txtDateOrText: UITextField!
    @IBOutlet weak fileprivate var txtDescription: UITextField!
    
    var onRelevantDateSaved:((_ relevantDate: RelevantDate) -> ())?
    var relevantDate = RelevantDate() {
        didSet {
            
            self.isFreetext = self.relevantDate.isFreeDateString
            self.strValue = self.relevantDate.stringValue ?? String()
            self.strValueDate = self.relevantDate.date?.getFormatDateString() ?? String()
            self.strDescription = self.relevantDate.title
            self.language = self.relevantDate.language == "es" ? .spanish : .english
        }
    }
    
    var language = Language.english {
        didSet {
            
            self.relevantDate.language = self.language == .english ? "en" : "es"
        }
    }
    
    fileprivate var imgIcon = UIImage()
    fileprivate var isFreetext = false
    fileprivate var strValue = String()
    fileprivate var strValueDate = String()
    fileprivate var strDescription = String()
    fileprivate var dateSelected = Date()

    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
         self.loadConfig()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        //icon calendar
        self.txtDateOrText.rightView =  UIImageView(image: UIImage(named:"CalendarReports"))
        self.txtDateOrText.rightViewMode = .always
        
        //Text
        if self.isFreetext == true && self.strValue.trim().isEmpty == false
        {
            self.txtDateOrText.rightView = nil
            self.txtDateOrText.text = self.strValue
        }
        else if self.isFreetext == false && self.strValueDate.trim().isEmpty == false
        {
            self.txtDateOrText.rightView =  UIImageView(image: UIImage(named:"CalendarReports"))
            self.txtDateOrText.rightViewMode = .always
            
            self.txtDateOrText.text = self.strValueDate
        }
        
        if self.strDescription.trim().isEmpty == false
        {
            self.txtDescription.text = self.strDescription
        }
        
        //Placeholder
         self.txtDateOrText.placeholder = ". . ."
        self.txtDescription.placeholder = "Description".localized()
        
        //buttons
        self.btnSave.setTitle("Save".localized(), for: UIControlState())
        self.btnSave.addTarget(self, action: #selector(self.saveDate), for: .touchUpInside)
        
        self.imgIcon = self.isFreetext == true ? UIImage(named: "iconRoundSelected")! : UIImage(named: "iconRoundUnselected")!
        self.btnFreeText.setTitle(" Free Text".localized(), for: UIControlState())
        self.btnFreeText.setImage(self.imgIcon, for: UIControlState())
        self.btnFreeText.addTarget(self, action: #selector(self.selectFreeText), for: .touchUpInside)

        
        //Style
        self.txtDateOrText.setBorder()
        self.txtDescription.setBorder()
    }
    
    //MARK:  ACTIONS
    func saveDate()
    {
        self.view.endEditing(true)
        
        guard self.txtDateOrText.text?.trim().isEmpty == false else {
            
            self.txtDateOrText.shakeAnimation()
            return
        }
        
        guard self.txtDescription.text?.trim().isEmpty == false else {
            
            self.txtDescription.shakeAnimation()
            return
        }
        
        self.onRelevantDateSaved?(self.relevantDate)
        self.dismiss(animated: true, completion: nil)
    }
    
    func selectFreeText()
    {
        //clean
        self.view.endEditing(true)
        self.txtDateOrText.text = String()
        
        //change icon
        if self.isFreetext == false
        {
            self.btnFreeText.setImage(UIImage(named: "iconRoundSelected"), for: UIControlState())
            self.txtDateOrText.rightView = nil
            self.isFreetext = true
            self.dateSelected = Date()
        }
        else
        {
            self.btnFreeText.setImage(UIImage(named: "iconRoundUnselected"), for: UIControlState())
            self.txtDateOrText.rightView =  UIImageView(image: UIImage(named:"CalendarReports"))
            self.txtDateOrText.rightViewMode = .always
            self.isFreetext = false
        }
        
        //Save
        self.relevantDate.isFreeDateString = self.isFreetext
    }
    
    func dateValue(_ sender: UIDatePicker)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.locale = Locale(identifier: "en_US")
        
        let stringDateHuman = dateFormatter.string(from: sender.date)
        self.txtDateOrText.text = stringDateHuman
        self.dateSelected = sender.date
    }
    
    //MARK: HELPERS
    func getFormatDateString(_ date: Date) -> String
    {
        let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        if language == "en"
        {
            dateFormatter.locale = Locale(identifier: "en_US")
        }
        else
        {
            dateFormatter.locale = Locale(identifier: "es_MX")
        }
        
        
        return dateFormatter.string(from: date)
    }
}

//MARK: TEXTFIELD DELEGATE
extension EditRelevantDateController: UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField == self.txtDateOrText
        {
            if self.isFreetext == true
            {
                 textField.inputView = nil
            }
            else
            {
                let pickerDateTime = UIDatePicker()
                pickerDateTime.datePickerMode = .date
                pickerDateTime.backgroundColor = UIColor.groupTableViewBackground
                pickerDateTime.addTarget(self, action: #selector(self.dateValue(_:)), for: .valueChanged)
                pickerDateTime.date = Date()
                pickerDateTime.isMultipleTouchEnabled = false
                pickerDateTime.isExclusiveTouch = true
                textField.inputView = pickerDateTime
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField == self.txtDateOrText
        {
            if let text = textField.text, text.trim().isEmpty == false
            {
                if self.isFreetext == true
                {
                    self.relevantDate.stringValue = text
                }
                else
                {
                    self.relevantDate.date = self.dateSelected
                    
                }
            }
        }
        
        if textField == self.txtDescription
        {
            if let text = textField.text, text.trim().isEmpty == false
            {
                self.relevantDate.title = text
            }
        }
    }
}

