//
//  FilesListViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 8/7/15.
//  Copyright © 2015 Definity First. All rights reserved.
//Test Pull

import UIKit
import QuickLook

class FileListViewController: UIViewController
{
    @IBOutlet weak var tableFiles: UITableView!
    var activeBlueForIpad = false
    
    var attachments = [Attachment]() {
        didSet {
            if let searchText = self.searchText {
                attachmentsFiltered = LibraryAPI.shared.attachmentBO.filterAttachment(attachments, byText: searchText)
            }
            else {
                attachmentsFiltered = attachments
            }
        }
    }
    var attachmentsFiltered = [Attachment]()
    var searchText: String?
    var attachmentSelected: Attachment?
    
    // MARK: - INITIALIZERS
    init(attachments: [Attachment])
    {
        super.init(nibName: "FileListViewController", bundle: nil)
        self.attachments = attachments

    }

    required init?(coder aDecoder: NSCoder)
    {
        self.attachments = [Attachment]()
        super.init(coder: aDecoder)
    }
    
    // MARK: - LIFE CLYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //Lateral UI
        self.view.backgroundColor = (self.activeBlueForIpad == true) ? UIColor.blueSky() : UIColor.white
        self.tableFiles.backgroundColor = (self.activeBlueForIpad == true) ? UIColor.blueSky() : UIColor.white
        
        /** Loading message */
        tableFiles.hideEmtpyCells()
        tableFiles.displayBackgroundMessage("Loading...".localized(),
            subMessage: "")
        
        if attachmentsFiltered.count == 0 {
            attachmentsFiltered = attachments
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        backButtonArrow()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
    
    }
    
    // MARK: - PRIVATE METHODS
    func filterByText(_ text: String) {
        guard text.isEmpty == false else {
            if searchText != nil {
                attachmentsFiltered = attachments
                searchText = nil
                tableFiles.reloadData()
            }
            return
        }
        attachmentsFiltered = LibraryAPI.shared.attachmentBO.filterAttachment(attachments, byText: text)
        searchText = text.isEmpty ? nil : text
        tableFiles.reloadData()
    }
    
    func showQuickLoock() {
        let previewQL = QLPreviewController()
        previewQL.dataSource = self
        previewQL.currentPreviewItemIndex = 0
        
        //NavController or not
        if let navController = self.navigationController
        {
            navController.navigationBar.isTranslucent = false            
            navController.present(previewQL, animated: true, completion: nil)
        }
        else
        {
            self.present(previewQL, animated: true, completion: nil)
        }
    }
}

// MARK: QUICK LOOK (QLPreviewControllerDataSource)
extension FileListViewController: QLPreviewControllerDataSource
{
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int
    {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem
    {
        return attachmentSelected!.localUrl! as QLPreviewItem
    }
}


// MARK: TABLE VIEW DATASOURCE & DELEGATE
extension FileListViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        /** Loading final message */
        if attachmentsFiltered.count > 0 {
            tableFiles.dismissBackgroundMessage()
        }
        else
        {
            tableFiles.displayBackgroundMessage("No Attachments Found".localized(),
                subMessage: "")
        }
        
        return attachmentsFiltered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier:"Cell")
        
        let attachment = attachmentsFiltered[indexPath.row]
        
        let fileName = !attachment.name.isEmpty ? attachment.name : "Attachment".localized() + " \(indexPath.row + 1)"
        
        cell.textLabel?.text = fileName
        cell.imageView?.image = UIImage(named: "iconFile_\(attachment.type.lowercased().getIconAttachmentName())")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        attachmentSelected = attachments[indexPath.row]
        
        guard attachmentSelected!.file.isEmpty == false else {
            return
        }
        
        attachmentSelected?.file = (attachmentSelected?.file.replacingOccurrences(of: "\\", with: "/"))!
        
        //Indicator
        MessageManager.shared.showLoadingHUD()
        
        LibraryAPI.shared.attachmentBO.downloadFileFromLink(URL(string: attachmentSelected!.file)!, onDownload: { (url) -> () in
            self.attachmentSelected!.localUrl = url
            MessageManager.shared.hideHUD()
            self.showQuickLoock()
            }, onError: { error in
                
                MessageManager.shared.hideHUD()
                MessageManager.shared.showBar(title: "Error".localized(),
                    subtitle:"An error has ocurred loading the attachments list".localized(),
                    type: .error,
                    fromBottom: false)
        })
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        //Lateral UI
        cell.backgroundColor = (self.activeBlueForIpad == true) ? UIColor.blueSky() : UIColor.white
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return CGFloat.leastNormalMagnitude
    }
    
}
