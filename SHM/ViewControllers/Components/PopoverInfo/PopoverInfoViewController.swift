//
//  PopoverInfoViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 4/28/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class PopoverInfoViewController: UIViewController
{
    //MARK: OUTLETS & PROPERTIES
    @IBOutlet weak private var lblTitle: UILabel!
    @IBOutlet weak private var lblSubtitle: UILabel!
    
    var title1 = String()
    var title2 = String()
    var onDeinit:(() -> ())?
    

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        print("Deinit: PopoverInfoViewController")
        self.onDeinit?()
    }
    
    //MARK: CONFIG
    private func loadConfig()
    {
        self.lblTitle.text = self.title1
        self.lblTitle.adjustsFontSizeToFitWidth = true
        
        self.lblSubtitle.text = self.title2
        self.lblSubtitle.adjustsFontSizeToFitWidth = true
    }
    
}
