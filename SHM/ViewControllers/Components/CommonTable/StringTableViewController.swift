//
//  StringTableViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 1/11/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class StringTableViewController: UITableViewController
{
    //MARK: PROPERTIES
    var checkedOption: Int = -1
    var rowsHeight: CGFloat?
    var onSelectedOption: ((_ numberOfRow: Int) -> ())?
    var onClosed: (() -> ())?
    var isDisplayingSites = false
    var options: [String]? {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    var icons: [String]? {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    //LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.tableView.hideEmtpyCells()

        //Disable Scrolling
        self.tableView.isScrollEnabled = false
        
        //Dynamic Height Size (Avoiding last separator line with -1 value
        let sizeHeight: CGFloat = self.rowsHeight ?? 44.0
        let numberOfOptions: Int = self.options?.count ?? 0
        let totalHeight: CGFloat = sizeHeight * CGFloat(numberOfOptions) - 1
        
        self.preferredContentSize = CGSize(width: 300, height: totalHeight)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        self.onClosed?()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        guard let arrayOptions = self.options else {
            
            return 0
        }
        return arrayOptions.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")

        guard let arrayOptions = self.options else {
            
            return UITableViewCell()
        }
        
         //Color for selected cell
        let selectedColor = UIView()
        selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
        cell.selectedBackgroundView = selectedColor
        
        cell.textLabel?.textColor = UIColor.blueTag()
        cell.textLabel?.text = arrayOptions[indexPath.row]
        
        if let opc = options, let icn = icons, opc.count == icn.count
        {
            cell.imageView?.image = UIImage(named: icn[indexPath.row])
        }
        
        if self.checkedOption != -1
        {
            if self.checkedOption == indexPath.row
            {
                if self.checkedOption != 0
                {
                    cell.accessoryType = .checkmark
                }
                else if self.isDisplayingSites == true
                {
                    cell.accessoryType = .checkmark
                }
            }
            else
            {
                cell.accessoryType = .none
            }
        }
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        dismiss(animated: true) { () -> Void in
            self.onSelectedOption?(indexPath.row)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return self.rowsHeight ?? 44.0
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        // Remove seperator inset
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset))
        {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))
        {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.responds(to: #selector(setter: UIView.layoutMargins))
        {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
}
