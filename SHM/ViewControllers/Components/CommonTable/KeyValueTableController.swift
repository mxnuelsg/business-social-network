//
//  KeyValueTableController.swift
//  SHM
//
//  Created by Manuel Salinas on 5/30/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class KeyValueTableController: UITableViewController
{
    //MARK: PROPERTIES
    var rowsHeight: CGFloat?
    var concatenateKeyAndValue = false
    var onSelectedOption: ((_ selected: StringKeyValueObject) -> ())?
    var onClosed: (() -> ())?
    
    var options = [StringKeyValueObject]() {
        didSet {
            
            self.tableView.reloadData()
        }
    }
    
    var icons: [String]? {
        didSet {
            self.tableView.reloadData()
        }
    }

    //LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        self.onClosed?()
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        //Disable Scrolling
        self.tableView.hideEmtpyCells()
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.options.count > 0
        {
            tableView.dismissBackgroundMessage()
        }
        else
        {
            tableView.displayBackgroundMessage("No Content".localized(),
                                               subMessage: "")
        }
        
        return self.options.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        
        //Color for selected cell
        let selectedColor = UIView()
        selectedColor.layer.backgroundColor = UIColor.blueSky().cgColor
        cell.selectedBackgroundView = selectedColor
        
        cell.textLabel?.textColor = UIColor.blueTag()
        cell.textLabel?.text = self.concatenateKeyAndValue == true ? "\(self.options[indexPath.row].key) ( \(self.options[indexPath.row].value) )" : self.options[indexPath.row].value
        
        if let icn = icons, self.options.count == icn.count {
            
            cell.imageView?.image = UIImage(named: icn[indexPath.row])
        }
        else
        {
//            print("Icons are not the same quantity of content")
        }
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        dismiss(animated: true) { () -> Void in
            
            self.onSelectedOption?(self.options[indexPath.row])
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return self.rowsHeight ?? 44.0
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        // Remove seperator inset
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset))
        {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins))
        {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.responds(to: #selector(setter: UIView.layoutMargins))
        {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return UIView()
    }
}
