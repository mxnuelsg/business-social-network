//
//  ImageViewer.swift
//  SHM
//
//  Created by Manuel Salinas on 9/4/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ImageViewer: UIViewController, UIScrollViewDelegate
{
    @IBOutlet weak var scrollImage: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    
    var showRemoveButton = true
    var imageSelected: UIImage!
    var onRemoveThis: ((_ image: UIImage) -> ())?
    
    
    init(image:UIImage!)
    {
        imageSelected = image
        super.init(nibName:"ImageViewer", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: LYFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        loadConfig()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: INITIAL CONFIG
    func loadConfig()
    {
        /**
        Navigation Bar
        */
        //Invisible bar
        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController!.navigationBar.shadowImage = UIImage()
        navigationController!.navigationBar.isTranslucent = true
        navigationController!.navigationBar.tintColor = UIColor.white
        navigationController!.view.backgroundColor = UIColor.clear

        //Buttons
        let barBtnCancel = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.close))
        navigationItem.leftBarButtonItem = barBtnCancel
        
        if showRemoveButton == true
        {
            let barBtnRemove = UIBarButtonItem(title:"Remove".localized(), style: .plain , target: self, action: #selector(self.delete(_:)))
            barBtnRemove.tintColor = UIColor.whiteCloud()
            
            navigationItem.rightBarButtonItem = barBtnRemove
        }
        
        /**
        ScrollView
        */
        let viewWidth = view.frame.width
        let viewHeight = view.frame.height
        
        let doubleTap = ActionsTapGestureRecognizer(onTap: {
            
            if self.scrollImage.zoomScale > 1.0
            {
                self.scrollImage.setZoomScale(1.0, animated: true)
            }
            else
            {
                self.scrollImage.setZoomScale(3.0, animated: true)
            }
        })
        
        doubleTap.numberOfTapsRequired = 2
        
        scrollImage.delegate = self
        scrollImage.frame = CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight)
        scrollImage.alwaysBounceVertical = false
        scrollImage.alwaysBounceHorizontal = false
        scrollImage.showsVerticalScrollIndicator = true
        scrollImage.flashScrollIndicators()
        
        scrollImage.minimumZoomScale = 1.0
        scrollImage.maximumZoomScale = 10.0
        scrollImage.addGestureRecognizer(doubleTap)
        
        /**
        UIIMageView
        */
        imageView.image = imageSelected
        imageView.contentMode = UIViewContentMode.scaleAspectFit
    }
    
    // MARK: UIScrollViewDelegate Methods
    func viewForZooming(in scrollView: UIScrollView) -> UIView?
    {
        return self.imageView
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        navigationController!.navigationBar.isHidden = (scrollImage.zoomScale > 1.0) ? true : false
    }
    
    // MARK: ACTIONS
    func close()
    {
        dismiss(animated: true, completion: nil)
    }
    
    func delete()
    {
        dismiss(animated: true) { () -> Void in
            if let removeThisPicture = self.onRemoveThis
            {
                removeThisPicture(self.imageView.image!)
            }
        }
    }
    

}
