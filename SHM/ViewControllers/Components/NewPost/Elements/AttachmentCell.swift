//
//  AttachmentCell.swift
//  SHM
//
//  Created by Manuel Salinas on 9/3/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class AttachmentCell: UICollectionViewCell
{
    @IBOutlet weak var imageAttachment: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!

    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

}
