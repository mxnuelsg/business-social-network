//
//  CalendarPickerViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 8/27/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class CalendarPickerViewController: UIViewController
{
    // MARK: - CONTROLS, OUTLETS & VARIABLES
    @IBOutlet weak var pickerDate: UIDatePicker!
    @IBOutlet weak var switchAllDay: UISwitch!
    @IBOutlet weak var AllDayTitle: UIBarButtonItem!
    
    @IBOutlet weak var btnDone: UIBarButtonItem!
    var isPickerForNewPost: Bool = true
    var onDateSelected: ((_ date: Event) -> ())?
    var onDate: ((_ dateSelected: Date) -> ())?
    
    var hidden: Bool! {
        didSet {
            view.isHidden = self.hidden
            
            if let hide = self.hidden
            {
                if hide
                {
                    self.pickerDate.date = Date()
                    self.switchAllDay.setOn(true, animated: true)
                    self.pickerDate.datePickerMode = UIDatePickerMode.date
                }
            }
        }
    }

    init()
    {
        super.init(nibName: "CalendarPickerViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - LIFE CLYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        configView()
        switchAllDay.addTarget(self, action: #selector(self.stateChanged(_:)), for: UIControlEvents.valueChanged)
        self.AllDayTitle.title = "All Day".localized()
        self.btnDone.title = "Done".localized()        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
//        print(self.btnDone.title)
    }
    
    deinit
    {
        print("adios calendar")
    }
    // MARK: - CONFIG VIEW
    func configView()
    {
        if isPickerForNewPost == false
        {
            AllDayTitle.isEnabled = false
            AllDayTitle.tintColor = UIColor.clear
            switchAllDay.isHidden = true
            pickerDate.datePickerMode = UIDatePickerMode.date
        }
    }
    
    // MARK: - ACTIONS
    func stateChanged(_ switchState: UISwitch)
    {
        pickerDate.datePickerMode = (switchState.isOn) ? UIDatePickerMode.date : UIDatePickerMode.dateAndTime
    }
    
    @IBAction func doneAction()
    {
        if isPickerForNewPost
        {
            if let selectedDate = onDateSelected
            {
                let event = Event()
                event.id = 0
                event.date = pickerDate.date
                event.allDay = switchAllDay.isOn
                
                selectedDate(event)
            }
            hidden = (DeviceType.IS_ANY_IPAD) ? false : true
        }
        else
        {
            if let selectedDate = onDate
            {
                selectedDate(pickerDate.date)
            }
            dismiss(animated: true, completion: nil)
        }
    }
}
