//
//  NewPostModal.swift
//  SHM
//
//  Created by Manuel Salinas on 2/9/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class NewPostModal: UIViewController//, UIPopoverControllerDelegate, UIPopoverPresentationControllerDelegate
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak var tvText: TextViewWithoutMenu!
    @IBOutlet weak var collectionAttachments: UICollectionView!
    @IBOutlet weak var constraintTextviewBottom: NSLayoutConstraint!
    
    var activityIndicator = UIActivityIndicatorView()
    var barBtnActivity: UIBarButtonItem!
    var btnPrivacy: UIButton!
    var btnAt: UIButton!
    var btnCat: UIButton!
    var btnAttach: UIButton!
    var barBtnDone: UIBarButtonItem!
    
    let limitOfChracters = 8000
    var isPrivate = false
    var onViewControllerClosed : (() -> ())?
    
    var arrayStakeholders = [Stakeholder]()
    var arrayStakeholdersAdded = [(Stakeholder, Range<String.Index>)]()
    
    var arrayProjects = [Project]()
    var arrayProjectsAdded = [(Project, Range<String.Index>)]()
    
    var arrayGeographies = [ClusterItem]()
    var arrayGeographiesAdded = [(ClusterItem, Range<String.Index>)]()
    
    var arrayDateAdded = [(Event, Range<String.Index>)]()
    var arrayAttachments = [AttachmentWithToken]()
    
    var currentMentionChar: String?
    var currentMentionCharRange: Range<String.Index>?
    
    //Default Tagged
    var stakeholderTagged: Stakeholder?
    var projectTagged: Project?
    var dateTagged: Date?
    var geographyTagged: ClusterItem?
    var didPostGeography: (()->())?
    //Identify Special words
    var typeMentionsAndHashtags : WordManager!
    
    //Subviews
    var vcStakeholdersTable = Storyboard.getInstanceOf(ContactSearchViewController.self)
    var vcProjectTable = Storyboard.getInstanceOf(ProjectSearchViewController.self)
    var vcCalendar = CalendarPickerViewController()
    var vcGeoTable: GeoTableViewController! = UIStoryboard(name: "Geography", bundle: nil).instantiateViewController(withIdentifier: "GeoTableViewController") as! GeoTableViewController
    
    //Attachment counter
    var uploadCount = 1
    
    //WebService execution flags
    var isExecutingStakeholderService = false
    var isExecutingProjectsService = false
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.loadStakeholders()
        self.enableSpecialTextRecognizer()
        self.placeholder(true)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.tvText.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        self.reloadConstraints()
        super.viewWillTransition(to: size, with: coordinator)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: LOAD CONFIG
    func loadConfig()
    {
        self.title = "NEW POST".localized()
        self.reloadConstraints()
        
        //Navigation Bar Buttons
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
        self.barBtnActivity = UIBarButtonItem(customView: self.activityIndicator)
        
        let barBtnCancel = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.stop, target: self, action: #selector(self.close))
        self.barBtnDone = UIBarButtonItem(title: "POST".localized(), style: UIBarButtonItemStyle.done, target: self,  action: #selector(self.savePost))
        
        //Adding to NavBar
        navigationItem.leftBarButtonItem = barBtnCancel
        navigationItem.rightBarButtonItem = self.barBtnDone
        
        //Toolbar for Keyboard
        let toolbarKey: UIToolbar! = UIToolbar()
        toolbarKey.barTintColor = UIColor.whiteCloud()
        toolbarKey.tintColor = UIColor.blackAsfalto()
        
        //Buttons
        self.btnAt = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        self.btnAt.setTitle("＠", for: UIControlState())
        self.btnAt.setTitleColor(UIColor.blackAsfalto(), for: UIControlState())
        self.btnAt.titleLabel!.font = UIFont.systemFont(ofSize: 30)
        self.btnAt.layer.borderColor = UIColor.blackAsfalto().cgColor
        self.btnAt.layer.borderWidth = 1
        self.btnAt.layer.cornerRadius = 3
        self.btnAt.addTarget(self, action: #selector(self.addAt), for: UIControlEvents.touchUpInside)
        
        self.btnCat = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        self.btnCat.setTitle("#", for: UIControlState())
        self.btnCat.setTitleColor(UIColor.blackAsfalto(), for: UIControlState())
        self.btnCat.titleLabel!.font = UIFont.systemFont(ofSize: 30)
        self.btnCat.layer.borderColor = UIColor.blackAsfalto().cgColor
        self.btnCat.layer.borderWidth = 1
        self.btnCat.layer.cornerRadius = 3
        self.btnCat.addTarget(self, action: #selector(self.addHashtag), for: UIControlEvents.touchUpInside)
        
        let btnPercent = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        btnPercent.setTitle("%", for: UIControlState())
        btnPercent.setTitleColor(UIColor.blackAsfalto(), for: UIControlState())
        btnPercent.titleLabel!.font = UIFont.systemFont(ofSize: 30)
        btnPercent.layer.borderColor = UIColor.blackAsfalto().cgColor
        btnPercent.layer.borderWidth = 1
        btnPercent.layer.cornerRadius = 3
        btnPercent.addTarget(self, action: #selector(self.addEvent), for: UIControlEvents.touchUpInside)
        
        let btnGeography = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        btnGeography.setTitle("!", for: UIControlState())
        btnGeography.setTitleColor(UIColor.blackAsfalto(), for: UIControlState())
        btnGeography.titleLabel!.font = UIFont.systemFont(ofSize: 30)
        btnGeography.layer.borderColor = UIColor.blackAsfalto().cgColor
        btnGeography.layer.borderWidth = 1
        btnGeography.layer.cornerRadius = 3
        btnGeography.addTarget(self, action: #selector(self.addGeography), for: UIControlEvents.touchUpInside)
        
        self.btnPrivacy = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        self.btnPrivacy.setImage(UIImage(named: "iconPublicBlack"), for: UIControlState())
        self.btnPrivacy.titleLabel!.font = UIFont.systemFont(ofSize: 30)
        self.btnPrivacy.layer.borderColor = UIColor.blackAsfalto().cgColor
        self.btnPrivacy.layer.borderWidth = 1
        self.btnPrivacy.layer.cornerRadius = 3
        self.btnPrivacy.addTarget(self, action: #selector(self.visibilityPost), for: UIControlEvents.touchUpInside)
        self.btnPrivacy.tag = 1
        
        self.btnAttach = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        self.btnAttach.setImage(UIImage(named: "iconAttach"), for: UIControlState())
        self.btnAttach.titleLabel!.font = UIFont.systemFont(ofSize: 30)
        self.btnAttach.layer.borderColor = UIColor.blackAsfalto().cgColor
        self.btnAttach.layer.borderWidth = 1
        self.btnAttach.layer.cornerRadius = 3
        self.btnAttach.addTarget(self, action: #selector(self.addAttachment), for: UIControlEvents.touchUpInside)
        
        //Toolbar items
        let barBtnMention = UIBarButtonItem(customView: btnAt)
        let barBtnHashtag = UIBarButtonItem(customView: btnCat)
        let barBtnEvent = UIBarButtonItem(customView: btnPercent)
        let barBtnGeography = UIBarButtonItem(customView: btnGeography)
        let barBtnAttachment =  UIBarButtonItem(customView: btnAttach)
        let barBtnSecurity =  UIBarButtonItem(customView: btnPrivacy)
        
        toolbarKey.items =
            [
                barBtnMention,
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
                barBtnHashtag,
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
                barBtnEvent,
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
                barBtnGeography,
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
                barBtnSecurity,
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
                barBtnAttachment
        ]
        
        toolbarKey.sizeToFit()
        self.tvText.inputAccessoryView = toolbarKey
        
        //Collection View (Attachments)
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection =  UICollectionViewScrollDirection.horizontal
        layout.minimumInteritemSpacing = 4
        layout.minimumLineSpacing = 4
        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        layout.itemSize = CGSize(width: 50, height: 50)
        
        if let collectionViewAttachments = self.collectionAttachments {
            collectionViewAttachments.collectionViewLayout = layout
            collectionViewAttachments.dataSource = self
            collectionViewAttachments.delegate = self
            collectionViewAttachments.allowsMultipleSelection = false
            collectionViewAttachments.register(UINib(nibName: "AttachmentCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
            collectionViewAttachments.backgroundColor = UIColor.clear
        }
        
        //Popovers & content
        self.vcStakeholdersTable.isSearchingSH = true
        self.vcStakeholdersTable.willBeShowedInNewPost = true
        self.vcStakeholdersTable.modalPresentationStyle = .popover
        self.vcStakeholdersTable.preferredContentSize = CGSize(width: 280,height: 280)
        self.vcStakeholdersTable.onModalViewControllerClosed = {
                
                self.tvText.resignFirstResponder()
                self.tvText.becomeFirstResponder()
        }
        
        self.vcProjectTable.willBeShowedInNewPost = true
        self.vcProjectTable.modalPresentationStyle = .popover
        self.vcProjectTable.preferredContentSize = CGSize(width: 280,height: 280)
        self.vcProjectTable.onModalViewControllerClosed = {
            
            self.tvText.resignFirstResponder()
            self.tvText.becomeFirstResponder()
        }
        
        self.vcCalendar.modalPresentationStyle = .popover
        self.vcCalendar.preferredContentSize = CGSize(width: 280,height: 280)
        
        //Geography Table
        self.vcGeoTable.modalPresentationStyle = .popover
        self.vcGeoTable.preferredContentSize = CGSize(width: 280,height: 280)
    }
    
    // MARK: LOAD DATA
    func loadStakeholders()
    {
        self.isExecutingStakeholderService = true
        
        //Indicator
        self.navigationItem.rightBarButtonItem = barBtnActivity
        activityIndicator.startAnimating()
        
        LibraryAPI.shared.stakeholderBO.getStakeholdersLight(onSuccess:{
            (stakeholders) -> () in
            
            //Indicator
            self.activityIndicator.stopAnimating()
            self.navigationItem.rightBarButtonItem = self.barBtnDone
            
            self.isExecutingStakeholderService = false
            self.arrayStakeholders = stakeholders
            self.loadProjects()
            
            //Inactive state
            self.btnAt.alpha = (self.arrayStakeholders.count > 0) ? 1 : 0.5
            
            }) { error in
                
                //Indicator
                self.activityIndicator.stopAnimating()
                self.navigationItem.rightBarButtonItem = self.barBtnDone
                
                self.isExecutingStakeholderService = false
                self.loadProjects()
                
                //Inactive state
                self.btnAt.alpha = (self.arrayStakeholders.count > 0) ? 1 : 0.5
                
                MessageManager.shared.showBar(title: "Error",
                    subtitle: "An Error has ocurred trying to load stakeholder list".localized(),
                    type: .error,
                    fromBottom: false)
        }
    }
    
    func loadProjects()
    {
        self.isExecutingProjectsService = true
        
        //Indicator
        self.navigationItem.rightBarButtonItem = barBtnActivity
        activityIndicator.startAnimating()
        
        LibraryAPI.shared.projectBO.getProjectsLightList(onSuccess: {
            (projects) -> () in
            
            //Indicator
            self.activityIndicator.stopAnimating()
            self.navigationItem.rightBarButtonItem = self.barBtnDone
            
            self.isExecutingProjectsService = false
            self.arrayProjects = projects
            self.searchPreTags()
            
            //Inactive state
            self.btnCat.alpha = (self.arrayProjects.count > 0) ? 1 : 0.5
            
            }) { error in
                
                //Indicator
                self.activityIndicator.stopAnimating()
                self.navigationItem.rightBarButtonItem = self.barBtnDone
                
                self.isExecutingProjectsService = false
                
                //Inactive state
                self.btnCat.alpha = (self.arrayProjects.count > 0) ? 1 : 0.5
                
                MessageManager.shared.showBar(title: "Error",
                    subtitle: "An Error has ocurred trying to load project list".localized(),
                    type: .error,
                    fromBottom: false)
        }
    }
    
    // MARK: PRE TAGS
    func searchPreTags()
    {
        //Search for Stakeholder tag
        if let stakeholderTag = self.stakeholderTagged as Stakeholder!
        {
            self.tvText.text = stakeholderTag.fullName
            
            let rangeOfFullName = self.tvText.text.range(of: stakeholderTag.fullName, options:NSString.CompareOptions.backwards)
            let finalText: String! = self.tvText.text.replacingCharacters(in: rangeOfFullName!, with:"\(stakeholderTag.fullName), ")
            
            //Verify Number of characters
            if finalText.characters.count > self.limitOfChracters
            {
                MessageManager.shared.showBar(title: "Warning".localized(),
                    subtitle:"You have exceeded the limit of characters".localized(),
                    type: .warning,
                    fromBottom: false)
                
                self.tvText.text = ""
            }
            else
            {
                self.tvText.text = finalText
                
                let tupleStakeholder = (stakeholder: stakeholderTag, range: rangeOfFullName!)
                self.arrayStakeholdersAdded.append(tupleStakeholder)
                self.filterStakeholders_Remove()
                
                self.textViewDidChange(self.tvText)
                self.perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self.tvText, afterDelay: 0.1)
            }
        }
        
        //Search for Project tag
        if let projectTag = projectTagged as Project!
        {
            //Setting Text
            tvText.text = projectTag.title
            
            let rangeOfTitle = tvText.text.range(of: projectTag.title, options:NSString.CompareOptions.backwards)
            let finalText: String! = tvText.text.replacingCharacters(in: rangeOfTitle!, with:"\(projectTag.title), ")
            
            //Verify Number of characters
            if finalText.characters.count > self.limitOfChracters
            {
                MessageManager.shared.showBar(title: "Warning".localized(),
                    subtitle: "You have exceeded the limit of characters".localized(),
                    type: .warning,
                    fromBottom: false)
                
                self.tvText.text = ""
            }
            else
            {
                self.tvText.text = finalText
                
                let tupleProject = (project: projectTag, range: rangeOfTitle!)
                self.arrayProjectsAdded.append(tupleProject)
                self.filterProjects_Remove()
                
                self.textViewDidChange(self.tvText)
                self.perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self.tvText, afterDelay: 0.1)
            }
        }
        
        //Geographies
        if let geography = geographyTagged {
            
            tvText.text = geography.titleComplete
            
            let rangeOfTitle = tvText.text.range(of: geography.titleComplete, options:NSString.CompareOptions.backwards)
            let finalText: String! = tvText.text.replacingCharacters(in: rangeOfTitle!, with:"\(geography.titleComplete), ")
            
            //Verify Number of characters
            if finalText.characters.count > (limitOfChracters) 
            {
                MessageManager.shared.showBar(title: "Warning".localized(),
                                              subtitle: "You have exceeded the limit of characters".localized(),
                                              type: .warning,
                                              fromBottom: false)
            }
            else
            {
                let rangeOfTitle = (finalText.range(of: geography.titleComplete, options:NSString.CompareOptions.backwards)?.lowerBound)!..<finalText.index((finalText.endIndex), offsetBy: -3)
                
                self.tvText.text = finalText
                
                let tupleGeography = (geography: geography, range: rangeOfTitle)
                self.arrayGeographiesAdded.append(tupleGeography)
                self.vcGeoTable.geographiesAdded.append(geography)
                self.textViewDidChange((self.tvText))
                
                self.dismissGeographyPopover()
                self.perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self.tvText, afterDelay: 0.1)
            }
        }
        
        if let dateTag = self.dateTagged
        {
            let dateString = dateTag.getDateStringForPost()
            self.tvText.text = dateString
            
            let rangeOfTitle = self.tvText.text.range(of: dateString, options:NSString.CompareOptions.backwards)
            let finalText: String! = self.tvText.text.replacingCharacters(in: rangeOfTitle!, with:"\(dateString), ")
            
            if finalText.characters.count > self.limitOfChracters
            {
                MessageManager.shared.showBar(title: "Warning".localized(),
                    subtitle: "You have exceeded the limit of characters".localized(),
                    type: .warning,
                    fromBottom: false)
                tvText.text = ""
            }
            else
            {
                self.tvText.text = finalText
                let date = Event()
                date.date = dateTag
                date.id = 0
                date.allDay = true
                
                let tupleEvent = (date: date, range: rangeOfTitle!)
                self.arrayDateAdded.append(tupleEvent)
                
                self.textViewDidChange(self.tvText)
                self.vcCalendar.view.isHidden = true
                perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self.tvText, afterDelay: 0.1)
            }
        }

    }
    
    //MARK: FILTERS
    func filterStakeholders_Remove()
    {
        for tag in self.arrayStakeholdersAdded
        {
            for (index, stake) in self.arrayStakeholders.enumerated() where tag.0.id == stake.id
            {
                self.arrayStakeholders.remove(at: index)
                print("SH removido id = \(stake.id) \n \(stake.fullName)")
            }
        }
    }
    
    func filterStakeholders_Restore(_ stakeholder: Stakeholder)
    {
        self.arrayStakeholders.append(stakeholder)
        print("SH agrego id = \(stakeholder.id)")
    }
    
    func filterGeographies_Remove()
    {
        for tag in self.arrayGeographiesAdded
        {
            for (index, geo) in self.arrayGeographies.enumerated() where tag.0.id == geo.id
            {
                self.arrayGeographies.remove(at: index)
                print("GEO removido id = \(geo.id) \n \(geo.title)")
            }
        }
    }
    
    func filterGeographies_Restore(_ geography: ClusterItem)
    {
        self.arrayGeographies.append(geography)
        print("GEO agrego id = \(geography.id)")
    }
    
    func filterProjects_Remove()
    {
        for tag in self.arrayProjectsAdded
        {
            for (index, proj) in self.arrayProjects.enumerated() where tag.0.id == proj.id
            {
                self.arrayProjects.remove(at: index)
                print("P removido id = \(proj.id)")
            }
        }
    }
    
    func filterProjects_Restore(_ project: Project)
    {
        self.arrayProjects.append(project)
        print("P agrego id = \(project.id)")
    }
    
    // MARK: ACTIONS
    func reloadConstraints()
    {
        if (UIDeviceOrientationIsPortrait(UIDevice.current.orientation))
        {
            self.constraintTextviewBottom.constant = 210
        }
        else
        {
            self.constraintTextviewBottom.constant = 370
        }
    }
    
    func placeholder(_ yesNo:Bool)
    {
        if yesNo == true
        {
            let attributedString = NSMutableAttributedString(string:"Post something about @stakeholders #projects and %dates".localized())
            attributedString.setPlaceholderHomePost(["@stakeholders".localized(), "#projects".localized(), "%dates".localized()])
            self.tvText.attributedText = attributedString
            self.tvText.hasPlaceholder = yesNo
        }
        else
        {
            self.tvText.text = String()
            self.tvText.attributedText = nil
            self.tvText.hasPlaceholder = yesNo
        }
    }
    
    func close()
    {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func savePost()
    {
        guard arrayStakeholdersAdded.count > 0 || arrayProjectsAdded.count > 0 || arrayDateAdded.count > 0 || arrayGeographiesAdded.count > 0  else {
            
            MessageManager.shared.showBar(title: "Info".localized(),
                                          subtitle: "It's necessary include a mention (stakeholder / project / geography) or a date".localized(),
                                          type: .info,
                                          fromBottom: false)
            
            return
        }
        
        //Indicator
        self.navigationItem.rightBarButtonItem = self.barBtnActivity
        self.activityIndicator.startAnimating()
        
        LibraryAPI.shared.feedBO.publicNewPost(parameters: self.createRequest(), onSuccess: {
            () -> () in
            
            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                //Indicator
                self.activityIndicator.stopAnimating()
                self.navigationItem.rightBarButtonItem = self.barBtnDone
                self.didPostGeography?()
                //Reload Feed
                self.onViewControllerClosed?()
                NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadHomeFeed"), object: nil)
                
                self.dismiss(animated: true) { () -> Void in
                    
                    MessageManager.shared.showBar(title: "Success".localized(),
                        subtitle:"The post's been created".localized(),
                        type: .success,
                        fromBottom: false)
                }
            }
            
            }) { error in
                
                let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    
                    //Indicator
                    self.activityIndicator.stopAnimating()
                    self.navigationItem.rightBarButtonItem = self.barBtnDone
                    
                    MessageManager.shared.showBar(title: "Error".localized(),
                                                                    subtitle: "There was a problem. The new post hasn't been posted".localized(),
                        type: .error,
                        fromBottom: false)
                }
        }
    }
    
    func createRequest() -> [String : Any]
    {
        //Array Dates
        var arrayDates = [[String : Any]]()
        
        //Body
        var bodyText = self.tvText.text
        
        for text in self.arrayStakeholdersAdded
        {
            bodyText = bodyText?.replacingOccurrences(of: text.0.fullName, with: "{{s=\(text.0.id)}}")
        }
        
        for (index, tuple) in self.arrayDateAdded.enumerated()
        {
            tuple.0.id = index + 1 //set an id
            if tuple.0.allDay == true {
                bodyText = bodyText?.replacingOccurrences(of: tuple.0.date.getDateStringForPost(), with: "{{d=\(tuple.0.id)}}")
            }
            else {
                bodyText = bodyText?.replacingOccurrences(of: tuple.0.date.getDateTimeStringForPost(), with: "{{d=\(tuple.0.id)}}")
            }
            
            arrayDates.append(tuple.0.getWSObject())
        }
        
        for text in self.arrayProjectsAdded
        {
            bodyText = bodyText?.replacingOccurrences(of: text.0.title, with: "{{p=\(text.0.id)}}")
        }
        
        //Geographies
        for text in arrayGeographiesAdded
        {
            bodyText = bodyText?.replacingOccurrences(of: text.0.titleComplete, with: "{{g=\(text.0.id)}}")
        }

        
        //Attachments
        var arrayPhotos = [[String : Any]]()
        
        for attachment in self.arrayAttachments
        {
            arrayPhotos.append(["Token" : attachment.token])
        }
        
        var wsObject: [String : Any]  = [:]
        wsObject["Body"] = bodyText
        wsObject["Dates"] = arrayDates
        wsObject["Attachments"] = arrayPhotos
        wsObject["IsPrivate"] = Int(NSNumber(value:isPrivate))
        
        return wsObject
    }
    
    //MARK: ACTION KEYBOARD
    func addAttachment()
    {
        let alertSheet = UIAlertController(title:nil, message:nil, preferredStyle: .actionSheet)
        alertSheet.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler:nil))
        
        alertSheet.addAction(UIAlertAction(title: "Take Photo".localized(), style: .default, handler: {
            (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                imagePicker.allowsEditing = false
                imagePicker.modalPresentationStyle = .overCurrentContext
                
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        alertSheet.addAction(UIAlertAction(title: "Choose Existing".localized(), style: .default, handler: {
            (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) {
                
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                imagePicker.allowsEditing = false
                imagePicker.modalPresentationStyle = .overCurrentContext
                
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        self.present(alertSheet, animated: true, completion: nil)
    }
    
    func addAt()
    {
        if self.isExecutingStakeholderService == true || self.isExecutingProjectsService == true
        {
            MessageManager.shared.showBar(title: "Info".localized(),
                subtitle: "Loading stakeholder list...".localized(),
                type: .info,
                fromBottom: false)
        }
        else
        {
            if self.isExecutingStakeholderService == false
                && self.isExecutingProjectsService == false
                && self.arrayStakeholders.count == 0
            {
                let alert = UIAlertController(title:"Warning".localized(),
                                              message: "It wasn't possible to load the stakeholder list".localized(),
                                              preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Close".localized(), style: .cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "try again".localized(), style: .default, handler: { [weak self]
                    (action) in
                    
                    self?.loadStakeholders()
                    
                    }))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                self.tvText.text = self.tvText.text + "@"
            }
        }
    }
    
    func addAtbyKeyboard() -> Bool
    {
        if self.isExecutingStakeholderService == true || self.isExecutingProjectsService == true
        {
            MessageManager.shared.showBar(title: "Info".localized(),
                                          subtitle: "Loading stakeholder list...".localized(),
                                          type: .info,
                                          fromBottom: false)
            return false
        }
        return true
    }
    
    func addHashtag()
    {
        if self.isExecutingStakeholderService == true || self.isExecutingProjectsService == true
        {
            MessageManager.shared.showBar(title: "Info".localized(),
                subtitle: "Loading project list...".localized(),
                type: .info,
                fromBottom: false)
        }
        else
        {
            if self.isExecutingStakeholderService == false
                && self.isExecutingProjectsService == false
                && self.arrayProjects.count == 0
            {
                let alert = UIAlertController(title:"Warning".localized(),
                                              message: "It wasn't possible to load the project list".localized(),
                                              preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Close".localized(), style: .cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "try again".localized(), style: .default, handler: { [weak self]
                    (action) in
                    
                    self?.loadProjects()
                    
                    }))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                self.tvText.text = self.tvText.text + "#"
            }
        }
    }
    
    func addHashtagByKeyboard() -> Bool
    {
        if self.isExecutingProjectsService == true || self.isExecutingStakeholderService == true
        {
            MessageManager.shared.showBar(title: "Info".localized(),
                                          subtitle: "Loading project list...".localized(),
                                          type: .info,
                                          fromBottom: false)
            return false
        }
        return true
    }
    
    func addEvent()
    {
        self.tvText.text = self.tvText.text + "%"
    }
    
    func addGeography()
    {
        self.tvText.text = self.tvText.text + "!"
        self.textViewDidChange(self.tvText)
    }
    
    func visibilityPost()
    {
        self.btnPrivacy.tag = (self.btnPrivacy.tag == 1) ? 0 : 1
        self.isPrivate = (self.btnPrivacy.tag == 1) ? false : true
        
        let iconName = (self.btnPrivacy.tag == 0) ? "iconPrivate" : "iconPublic"
        let imageIcon: UIImage! =  UIImage(named: iconName)
        
        if self.btnPrivacy.tag == 0
        {
            self.btnPrivacy.setImage(UIImage(named: "iconPrivateBlack"), for: UIControlState())

            MessageManager.shared.showBar(title: "Status".localized(),
                                          subtitle: "Private Post".localized(),
                                          type: MessageType.info,
                                          containsIcon: false,
                                          fromBottom: false)
        }
        else
        {
            self.btnPrivacy.setImage(UIImage(named: "iconPublicBlack"), for: UIControlState())
            MessageManager.shared.showBar(title: "Status".localized(),
                                          subtitle: "Public Post".localized(),
                                          type: MessageType.info,
                                          containsIcon: false,
                                          fromBottom: false)
        }
    }
    
    // MARK: LOAD FUNCTIONALITY
    func enableSpecialTextRecognizer()
    {
        self.typeMentionsAndHashtags = WordManager(text: "") {
            
            (text, magicWord, charRange, searchWordRange) in
            
            
            
            if text.characters.count == 0
            {
                /*var mentionCharRange = rangeOfText
                mentionCharRange.lowerBound = <#T##Collection corresponding to your index##Collection#>.index(mentionCharRange.lowerBound, offsetBy: -1)
                self.currentMentionChar = magicWord.rawValue
                self.currentMentionCharRange = mentionCharRange*/
                self.currentMentionChar = magicWord.rawValue
                self.currentMentionCharRange = charRange
            }
            
            if magicWord == SpecialWord.Mention
            {
                let arrayStakeHoldersFilter: [Stakeholder]!
                
                if text == ""
                {
                    arrayStakeHoldersFilter = self.arrayStakeholders
                }
                else
                {
                    arrayStakeHoldersFilter = self.arrayStakeholders.filter({
                        
                       ($0.fullName as NSString).localizedCaseInsensitiveContains(text) || ($0.aliasString as NSString).localizedCaseInsensitiveContains(text)
                    })
                }
                
                //Present or not Popover
                if arrayStakeHoldersFilter.count > 0
                {
                    self.presentStakeholderPopover()
                }
                else
                {
                    self.dismissStakeholderPopover()
                }
                
                
                //Filter Table
                self.vcStakeholdersTable.loadStakeholders(arrayStakeHoldersFilter)
                if let stakeholderResultsTable = self.vcStakeholdersTable.tvResults {
                
                    stakeholderResultsTable.reloadData()
                }
                
                self.vcStakeholdersTable.addContactToInstance = {
                    (contact) in
                    
                    //Setting Text
                    print(contact.fullName)
                    let replacementString = (String(contact.fullName) ?? String()) + ", "
                    let finalText: String = self.tvText.text.replacingCharacters(in: searchWordRange, with: replacementString)
                    
                    //Verify Number of characters
                    if finalText.characters.count > self.limitOfChracters
                    {
                        MessageManager.shared.showBar(title: "Warning".localized(),
                            subtitle: "You have exceeded the limit of characters".localized(),
                            type: .warning,
                            fromBottom: false)
                    }
                    else
                    {
                        let rangeOfFullName = (finalText.range(of: contact.fullName, options:NSString.CompareOptions.backwards)?.lowerBound)!..<finalText.index((finalText.endIndex), offsetBy: -3)
                        
                        self.tvText.text = finalText
                        
                        let tupleStakeholder = (stakeholder: contact as! Stakeholder, range: rangeOfFullName)
                        self.arrayStakeholdersAdded.append(tupleStakeholder)
                        self.filterStakeholders_Remove()
                        
                        self.textViewDidChange(self.tvText)
                        self.dismissStakeholderPopover()
                        self.perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self.tvText, afterDelay: 0.1)
                    }
                }
            }
            else if magicWord == SpecialWord.Hashtag
            {
                let arrayProjects: [Project]!
                
                if text == ""
                {
                    arrayProjects = self.arrayProjects
                }
                else
                {
                    arrayProjects =  self.arrayProjects.filter({
                        ($0.title as NSString).localizedCaseInsensitiveContains(text)
                    })
                }
                
                //Present or not Popover
                if arrayProjects.count > 0
                {
                    self.presentProjectsPopover()
                }
                else
                {
                    self.dismissProjectsPopover()
                }
                
                
                //Filter Table
                self.vcProjectTable.loadProjects(arrayProjects)
                if let projectsResultTable = self.vcProjectTable.tvResults {
                    
                    projectsResultTable.reloadData()
                }
                self.vcProjectTable.onProjectSelected = {
                    (selectedProject) in
                    
                    //Setting Text
                    //rangeOfText.lowerBound = <#T##Collection corresponding to your index##Collection#>.index(rangeOfText.lowerBound, offsetBy: -1)
                    
                    let finalText: String! = self.tvText.text.replacingCharacters(in: searchWordRange, with:"\(selectedProject.title), ")
                    
                    //Verify Number of characters
                    if finalText.characters.count > self.limitOfChracters
                    {
                        MessageManager.shared.showBar(title: "Warning".localized(),
                            subtitle: "You have exceeded the limit of characters".localized(),
                            type: .warning,
                            fromBottom: false)
                    }
                    else
                    {
                        let rangeOfTitle = (finalText.range(of: selectedProject.title, options:NSString.CompareOptions.backwards)?.lowerBound)!..<finalText.index((finalText.endIndex), offsetBy: -3)
                        
                        self.tvText.text = finalText
                        
                        let tupleProject = (project: selectedProject, range: rangeOfTitle)
                        self.arrayProjectsAdded.append(tupleProject)
                        self.filterProjects_Remove()
                        
                        self.textViewDidChange(self.tvText)
                        
                        self.dismissProjectsPopover()
                        self.perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self.tvText, afterDelay: 0.1)
                    }
                }
            }
            else if magicWord == SpecialWord.Geography
            {
                guard text.characters.first != " " else {
                    
                    self.dismissGeographyPopover()
                    return
                }
                self.vcGeoTable.geoQuery.getAllWithPagination()
                if text == ""
                {
                    self.vcGeoTable.geoQuery.searchText = "A"
                }
                else
                {
                    self.vcGeoTable.clusterItems.removeAll()
                    self.vcGeoTable.geoQuery.searchText = text
                }
                self.activityIndicator.startAnimating()
                self.navigationItem.rightBarButtonItem = self.barBtnActivity
                self.vcGeoTable.geoListMode = .all
                self.vcGeoTable.ownerModule = .newPost
                self.presentGeographyPopover()
                self.vcGeoTable.refreshTable()
                
                self.vcGeoTable.onDidLoadGeographies = {[weak self] numberOfitems in
                    
                    //UI-> Update visibility of table and focus textView
                    self?.activityIndicator.stopAnimating()
                    self?.navigationItem.rightBarButtonItem = self?.barBtnDone
                }
                self.vcGeoTable.onDidSelectGeography = {[weak self] geography in
                    
                    let finalText: String! = self?.tvText.text.replacingCharacters(in: searchWordRange, with:"\(geography.titleComplete), ")
                    
                    //Verify Number of characters
                    if finalText.characters.count > (self?.limitOfChracters) ?? 0
                    {
                        MessageManager.shared.showBar(title: "Warning".localized(),
                                                      subtitle: "You have exceeded the limit of characters".localized(),
                                                      type: .warning,
                                                      fromBottom: false)
                    }
                    else
                    {
                        let rangeOfTitle = (finalText.range(of: geography.titleComplete, options:NSString.CompareOptions.backwards)?.lowerBound)!..<finalText.index((finalText.endIndex), offsetBy: -3)
                        
                        self?.tvText.text = finalText
                        
                        let tupleGeography = (geography: geography, range: rangeOfTitle)
                        self?.arrayGeographiesAdded.append(tupleGeography)
                        self?.vcGeoTable.geographiesAdded.append(geography)
                        self?.textViewDidChange((self?.tvText)!)
                        
                        self?.dismissGeographyPopover()
                        self?.perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self?.tvText, afterDelay: 0.1)
                    }
                }
            }
            else if magicWord == SpecialWord.CalendarDate
            {
                //Present or not Popover
                if text.characters.count == 0
                {
                    self.presentDatePopover()
                }
                else
                {
                    self.dismissDatePopover()
                }
                
                
                self.vcCalendar.onDateSelected = {
                    (date) in
                    
                    //Setting Text  
                    let dateString: String! = (date.allDay) ? date.date.getDateStringForPost() : "\(date.date.getDateTimeStringForPost())"
                    let replacementString = dateString + ", "
                    let finalText: String! = self.tvText.text.replacingCharacters(in: searchWordRange, with: replacementString)
                    
                    //Verify Number of characters
                    if finalText.characters.count > self.limitOfChracters
                    {
                        MessageManager.shared.showBar(title: "Warning".localized(),
                            subtitle: "You have exceeded the limit of characters".localized(),
                            type: .warning,
                            fromBottom: false)
                    }
                    else
                    {
                        //Avoiding duplicated dates
                        for event in self.arrayDateAdded
                        {
                            let eventString: String! = event.0.allDay ? event.0.date.getDateStringForPost() : "\(event.0.date.getDateTimeStringForPost())"
                            
                            if eventString == dateString
                            {
                                MessageManager.shared.showBar(title: "Info".localized(),
                                    subtitle:"This date already has been selected".localized(),
                                    type: .info,
                                    fromBottom: false)
                                
                                self.textViewDidChange(self.tvText)
                                self.perform(#selector(UITextViewDelegate.textViewDidChange(_:)), with: self.tvText, afterDelay: 0.1)
                                
                                return
                            }
                        }
                        
                        let rangeOfDate = (finalText.range(of: dateString, options:NSString.CompareOptions.backwards)?.lowerBound)!..<finalText.index((finalText.endIndex), offsetBy: -3)
                        
                        self.tvText.text = finalText
                        
                        let tupleEvent = (date: date, range: rangeOfDate)
                        self.arrayDateAdded.append(tupleEvent)
                        
                        self.textViewDidChange(self.tvText)
                        self.perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self.tvText, afterDelay: 0.1)
                        self.dismissDatePopover()
                    }
                }
            }
        }
    }
    
    // MARK: UTILITIES
    func suddenDeletionCheck(_ text: String)
    {
        if text.isEmpty == true
        {
            print("sudden deletion")
            
            //Remove stakeholders selected and restore arrayStakeholders
            while arrayStakeholdersAdded.count > 0
            {
                if let stakeholderAdded = arrayStakeholdersAdded.last {
                    
                    filterStakeholders_Restore(stakeholderAdded.0)
                    arrayStakeholdersAdded.removeLast()
                }
            }
            
            //Remove Projects selected and restore arrayStakeholders
            while arrayProjectsAdded.count > 0
            {
                if let projectAdded = arrayProjectsAdded.last {
                    
                    filterProjects_Restore(projectAdded.0)
                    arrayProjectsAdded.removeLast()
                }
            }
            
            //Remove Events if any
            while arrayDateAdded.count > 0
            {
                arrayDateAdded.removeLast()
            }
        }
    }
    
    //MARK: POPOVER'S PRESENTATION AND DISMISSAL FUNCTIONS
    func presentStakeholderPopover()
    {
        if let currentPopover = self.presentedViewController {
            
            currentPopover.dismiss(animated: true, completion: {
            
                self.presentStakeholderTVC()
            })
        }
        else
        {
            self.presentStakeholderTVC()
        }
    }
    
    private func presentStakeholderTVC()
    {
        self.present(self.vcStakeholdersTable, animated: true, completion: nil)
        let stakeholderPopover = self.vcStakeholdersTable.popoverPresentationController
        stakeholderPopover?.permittedArrowDirections = .any
        stakeholderPopover?.sourceView = self.view
        stakeholderPopover?.sourceRect = self.tvText.frame
        stakeholderPopover?.backgroundColor = UIColor.grayCloudy()
    }
    func dismissStakeholderPopover()
    {
        self.vcStakeholdersTable.dismiss(animated: true, completion: nil)
    }
    
    func presentProjectsPopover()
    {
        if let currentPopover = self.presentedViewController {
            
            currentPopover.dismiss(animated: true, completion: {
            
                self.presentProjectTVC()
            })
        }
        else
        {
            self.presentProjectTVC()
        }
    }
    
    private func presentProjectTVC()
    {
        self.present(self.vcProjectTable, animated: true, completion: nil)
        let projectsPopover = self.vcProjectTable.popoverPresentationController
        projectsPopover?.permittedArrowDirections = .any
        projectsPopover?.sourceView = self.view
        projectsPopover?.sourceRect = self.tvText.frame
        projectsPopover?.backgroundColor = UIColor.grayCloudy()
    }
    
    func dismissProjectsPopover()
    {
        self.vcProjectTable.dismiss(animated: true, completion: nil)
    }
    
    func presentGeographyPopover()
    {
        if let currentPopover = self.presentedViewController {
            
            currentPopover.dismiss(animated: true, completion: {
                
                self.presentGeographyTVC()
            })
        }
        else
        {
            self.presentGeographyTVC()
        }
    }
    
    private func presentGeographyTVC()
    {
        self.present(self.vcGeoTable, animated: true, completion: nil)
        let projectsPopover = self.vcGeoTable.popoverPresentationController
        projectsPopover?.permittedArrowDirections = .any
        projectsPopover?.sourceView = self.view
        projectsPopover?.sourceRect = self.tvText.frame
        projectsPopover?.backgroundColor = UIColor.grayCloudy()
    }
    
    func dismissGeographyPopover()
    {
        self.vcGeoTable.dismiss(animated: true, completion: nil)
    }

    
    func presentDatePopover()
    {
        if let currentPopover = self.presentedViewController {
            
            currentPopover.dismiss(animated: true, completion: {
            
                self.presentDateTVC()
            })
        }
        else
        {
            self.presentDateTVC()
        }
    }
    
    private func presentDateTVC()
    {
        self.present(self.vcCalendar, animated: true, completion: nil)
        let datePopover = self.vcCalendar.popoverPresentationController
        datePopover?.permittedArrowDirections = .any
        datePopover?.sourceView = self.view
        datePopover?.sourceRect = self.tvText.frame
        datePopover?.backgroundColor = UIColor.grayCloudy()
    }
    func dismissDatePopover()
    {
        self.vcCalendar.dismiss(animated: true, completion: nil)
    }
}

// MARK: TEXTVIEW DELEGATE
extension NewPostModal: UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if self.tvText.hasPlaceholder == true
        {
            self.placeholder(false)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if self.tvText.text.trim().isEmpty
        {
            self.placeholder(true)
        }
    }
    
    func textViewDidChange(_ textView: UITextView)
    {
        //Set Style
        let attributedString = NSMutableAttributedString(string:textView.text)
        attributedString.setColorAndBold(arrayStakeholdersAdded, projects: self.arrayProjectsAdded, geographies: arrayGeographiesAdded, events: self.arrayDateAdded)
        textView.attributedText = attributedString
        
        //Check for sudden deletion
        self.suddenDeletionCheck(textView.text.trim())
        
        //Verify Mentions added
        var arrayStakeholderUpdated = arrayStakeholdersAdded
        for tuple in self.arrayStakeholdersAdded
        {
            //Remove it!
            if textView.text.containsString(tuple.0.fullName) == false
            {
                var rangeWithoutTheLastCharacter = tuple.1
                //rangeWithoutTheLastCharacter.upperBound = <#T##Collection corresponding to your index##Collection#>.index(rangeWithoutTheLastCharacter.upperBound, offsetBy: -1)
                
                if  rangeWithoutTheLastCharacter.upperBound <= textView.text.endIndex
                {
                    let finalText: String! = textView.text.replacingCharacters(in: rangeWithoutTheLastCharacter, with:"")
                    //Refresh Style
                    let attributedString = NSMutableAttributedString(string:finalText)
                    attributedString.setColorAndBold(arrayStakeholdersAdded, projects: self.arrayProjectsAdded, geographies: arrayGeographiesAdded, events: self.arrayDateAdded)
                    textView.attributedText = attributedString
                }
                arrayStakeholderUpdated = arrayStakeholderUpdated.filter({$0.0.id != tuple.0.id})
                filterStakeholders_Restore(tuple.0)
            }
        }
        arrayStakeholdersAdded = arrayStakeholderUpdated
        
        //Verify Projects added
        var arrayProjectsUpdated = arrayProjectsAdded
        for tuple in self.arrayProjectsAdded
        {
            if textView.text.containsString(tuple.0.title) == false
            {
                //Remove it!
                var rangeWithoutTheLastCharacter = tuple.1
                //rangeWithoutTheLastCharacter.upperBound = <#T##Collection corresponding to your index##Collection#>.index(rangeWithoutTheLastCharacter.upperBound, offsetBy: -1)
                
                if rangeWithoutTheLastCharacter.upperBound <= textView.text.endIndex
                {
                    let finalText: String! = textView.text.replacingCharacters(in: rangeWithoutTheLastCharacter, with:"")
                    //Refresh Style
                    let attributedString = NSMutableAttributedString(string:finalText)
                    attributedString.setColorAndBold(arrayStakeholdersAdded, projects: self.arrayProjectsAdded, geographies: arrayGeographiesAdded, events: self.arrayDateAdded)
                    textView.attributedText = attributedString
                }
                arrayProjectsUpdated = arrayProjectsUpdated.filter({$0.0.id != tuple.0.id})
                filterProjects_Restore(tuple.0)
            }
        }
        arrayProjectsAdded = arrayProjectsUpdated
        
        //Verify Geographies added
        var arrayGeographiesUpdated = arrayGeographiesAdded
        let cursorPosition = textView.position(from: textView.beginningOfDocument, offset: (textView.selectedRange.toRange()?.lowerBound)!)
        let offsetPosition = textView.offset(from: textView.beginningOfDocument, to: cursorPosition!)
        print("cursor: \(offsetPosition)")
        for tuple in self.arrayGeographiesAdded
        {
            //Remove it!
            var shouldRemove = false
            if cursorPosition == textView.endOfDocument
            {
                
                shouldRemove = textView.text.containsString(tuple.0.titleComplete) == false || ((textView.text.range(of: textView.text)?.upperBound)! <= tuple.1.upperBound)
            }
            else
            {
                shouldRemove = textView.text.containsString(tuple.0.titleComplete) == false
            }
            if shouldRemove == true
            {
                let rangeWithoutTheLastCharacter = tuple.1
                if  rangeWithoutTheLastCharacter.upperBound <= textView.text.endIndex
                {
                    let finalText: String! = textView.text.replacingCharacters(in: rangeWithoutTheLastCharacter, with:"")
                    //Refresh Style
                    let attributedString = NSMutableAttributedString(string:finalText)
                    attributedString.setColorAndBold(arrayStakeholdersAdded, projects: self.arrayProjectsAdded, geographies: arrayGeographiesAdded, events: self.arrayDateAdded)
                    textView.attributedText = attributedString
                }
                arrayGeographiesUpdated = arrayGeographiesUpdated.filter({$0.0.id != tuple.0.id})
                self.vcGeoTable.geographiesAdded = self.vcGeoTable.geographiesAdded.filter({$0.id != tuple.0.id})
                filterGeographies_Restore(tuple.0)
            }
        }
        arrayGeographiesAdded = arrayGeographiesUpdated
        
        //Verify Events added
        var arrayDateUpdated = arrayDateAdded
        for tuple in self.arrayDateAdded
        {
            let dateString: String! = (tuple.0.allDay) ? tuple.0.date.getDateStringForPost() : "\(tuple.0.date.getDateTimeStringForPost())"
            
            if textView.text.containsString(dateString) == false
            {
                //Remove it!
                var rangeWithoutTheLastCharacter = tuple.1
                //rangeWithoutTheLastCharacter.upperBound = <#T##Collection corresponding to your index##Collection#>.index(rangeWithoutTheLastCharacter.upperBound, offsetBy: -1)
                
                if rangeWithoutTheLastCharacter.upperBound <= textView.text.endIndex
                {
                    let finalText: String! = textView.text.replacingCharacters(in: rangeWithoutTheLastCharacter, with:"")
                    //Refresh Style
                    let attributedString = NSMutableAttributedString(string:finalText)
                    attributedString.setColorAndBold(arrayStakeholdersAdded, projects: self.arrayProjectsAdded, geographies: arrayGeographiesAdded, events: self.arrayDateAdded)
                    textView.attributedText = attributedString
                }
                arrayDateUpdated = arrayDateUpdated.filter({$0.0.date != tuple.0.date})
            }
        }
        arrayDateAdded = arrayDateUpdated
        
        //Elements Showed
        if let currentMentionCharRange = self.currentMentionCharRange {
            
            if textView.attributedText.string.characters.endIndex < currentMentionCharRange.upperBound
                || textView.attributedText.string.substring(with: currentMentionCharRange) != self.currentMentionChar!
            {
                self.dismissStakeholderPopover()
                self.dismissProjectsPopover()
                self.dismissDatePopover()
                self.dismissGeographyPopover()
            }
        }
        
//        changeDynamicSize(textView)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        //        if text == "\n"{ return false  }
        if text == "@"
        {
            return self.addAtbyKeyboard()
        }
        else if text == "#"
        {
            return self.addHashtagByKeyboard()
        }

        if textView.text.characters.count + (text.characters.count - range.length) <= limitOfChracters { return true}
        
        return false
    }
    
    func textViewDidChangeSelection(_ textView: UITextView)
    {
//        self.typeMentionsAndHashtags.text = textView.text
//        changeDynamicSize(textView)
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        self.perform(#selector(self.processMagicWord(_:)), with: textView.text, afterDelay: 1.5)
    }
    
    func processMagicWord(_ text: String)
    {
        typeMentionsAndHashtags.text = text
    }
}

// MARK: IMAGEPICKER VIEW METHODS
extension NewPostModal: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        self.dismiss(animated: true) { () -> Void in
            
//            self.positionForAtacchments()
            
            if let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
            {
                self.activityIndicator.startAnimating()
                self.btnAttach.isEnabled = false
                self.btnAttach.alpha = 0.5
                
                //Create Request Object
                var wsObject: [String : Any]  = [:]
                wsObject["OldFile"] = String()
                wsObject["VersionId"] = 1
                
                var imageData: Data?
                var validImage: UIImage?
                
                if selectedImage.size.width > 500
                {
                    validImage = selectedImage.resizeToWidth(500.0)
                }
                else
                {
                    validImage = selectedImage
                }
                
                imageData = UIImagePNGRepresentation(validImage!)
                
                self.uploadCount += 1
                let localUrl = validImage!.saveToTempDirectory("mobileUpload\(self.uploadCount).png")
                
                LibraryAPI.shared.attachmentBO.uploadFile(fileSize: imageData!.count, fileURL: localUrl, onSuccess: {
                    (token) in
                    
                    self.activityIndicator.stopAnimating()
                    self.btnAttach.isEnabled = true
                    self.btnAttach.alpha = 1
                    
                    let attachPicture = AttachmentWithToken(token: token, image: validImage!)
                    self.arrayAttachments.append(attachPicture)
                    self.collectionAttachments.reloadData()
                    
                    MessageManager.shared.showStatusBar(title: "Attachment Uploaded".localized(),
                        type: .success,
                        containsIcon: false)
                    
                    }, onError: { error in
                        
                        self.activityIndicator.stopAnimating()
                        self.btnAttach.isEnabled = true
                        self.btnAttach.alpha = 1
                        
                        MessageManager.shared.showBar(title: "Error".localized(),
                            subtitle: "There was an error trying to save an attachment".localized(),
                            type: .error,
                            fromBottom: false)
                })
            }
        }
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: NSErrorPointer?, contextInfo:UnsafeRawPointer)
    {
        if error != nil
        {
            let alert = UIAlertController(title: "Save Failed".localized(),
                                          message: "Failed to save image".localized(),
                                          preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "OK".localized(),  style: .cancel, handler: nil)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion:nil)
        }
    }
}

// MARK: COLLECTION VIEW METHODS
extension NewPostModal: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrayAttachments.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! AttachmentCell
        
        cell.imageAttachment.image = arrayAttachments[indexPath.row].picture
        cell.btnDelete.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
            
            self.arrayAttachments = self.arrayAttachments.filter({ $0 !== self.arrayAttachments[indexPath.row] })
            self.collectionAttachments.reloadData()
            
        }))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let vcViewer = ImageViewer(image: arrayAttachments[indexPath.row].picture)
        vcViewer.onRemoveThis = {
            (image) in
            
            self.arrayAttachments = self.arrayAttachments.filter({ $0.picture !== image })
            self.collectionAttachments.reloadData()
        }
        
        let navController = UINavigationController(rootViewController: vcViewer)
        navController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        navController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        self.present(navController, animated: true, completion: nil)
    }
 
}
