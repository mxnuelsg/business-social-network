//
//  NewPostViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 8/11/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import MobileCoreServices
import QuickLook

class NewPostViewController: UIViewController, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource
{
    
    @IBOutlet weak var tvText: TextViewWithoutMenu!
    @IBOutlet weak var collectionViewAttachments: UICollectionView!
    @IBOutlet weak var constraintTextViewBottom: NSLayoutConstraint!
    @IBOutlet weak var constraintCollectionViewAttachments: NSLayoutConstraint!
    
    var activityIndicator = UIActivityIndicatorView()
    var barBtnActivity: UIBarButtonItem!
    var keyboardFrame: CGRect! = CGRect(x: 0, y: 0, width: 320, height: 260)
    
    var btnPrivacy: UIButton!
    var btnAt: UIButton!
    var btnCat: UIButton!
    var btnAttach: UIButton!
    var barBtnDone: UIBarButtonItem!
    let limitOfChracters = 2000
    var limitHeightTextview: CGFloat!
    var isPrivate: Bool = false
    
    var onViewControllerClosed : (() -> ())?
    
    var arrayStakeholders = [Stakeholder]()
    var arrayStakeholdersAdded = [(Stakeholder, Range<String.Index>)]()
    
    var arrayProjects = [Project]()
    var arrayProjectsAdded = [(Project, Range<String.Index>)]()
    
    var arrayGeographies = [ClusterItem]()
    var arrayGeographiesAdded = [(ClusterItem, Range<String.Index>)]()
    
    var arrayDateAdded = [(Event, Range<String.Index>)]()
    var arrayAttachments = [AttachmentWithToken]()
    
    var currentMentionChar: String?
    var currentMentionCharRange: Range<String.Index>?
    
    //Default Tagged
    var stakeholderTagged: Stakeholder?
    var projectTagged: Project?
    var dateTagged: Date?
    var geographyTagged: ClusterItem?
    var didPostGeography: (()->())?
    //Identify Special words
    var typeMentionsAndHashtags : WordManager!
    
    //Subviews
    var vcStakeholdersTable: ContactSearchViewController! = Storyboard.getInstanceOf(ContactSearchViewController.self)
    var vcProjectTable: ProjectSearchViewController! = Storyboard.getInstanceOf(ProjectSearchViewController.self)
    var vcCalendar: CalendarPickerViewController! = CalendarPickerViewController()
    var vcGeoList: GeoListViewController! = UIStoryboard(name: "Geography", bundle: nil).instantiateViewController(withIdentifier: "GeoListViewController") as! GeoListViewController
    
    
    //Attachment counter
    var uploadCount = 1
    
    //WebService execution flags
    var isExecutingStakeholderService = false
    var isExecutingProjectsService = false
    
    fileprivate var showLimitCharactersMsg = true
    
    // MARK: - LIFE CLYCLE
    init()
    {
        super.init(nibName: "NewPostViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.loadStakeholders()
        
        /*Add SubViews*/
        //Table Stakeholder
        self.vcStakeholdersTable.isSearchingSH = true
        self.vcStakeholdersTable.willBeShowedInNewPost = true
        self.view.addSubview(vcStakeholdersTable.view)
        self.vcStakeholdersTable.view.isHidden = true
        self.vcStakeholdersTable.onModalViewControllerClosed = {
            
            self.tvText.resignFirstResponder()
            self.tvText.becomeFirstResponder()
        }
        
        //Table Projects
        self.vcProjectTable.willBeShowedInNewPost = true
        self.view.addSubview(vcProjectTable.view)
        self.vcProjectTable.view.isHidden = true
        self.vcProjectTable.onModalViewControllerClosed = {
            
            self.tvText.resignFirstResponder()
            self.tvText.becomeFirstResponder()
        }
        
        //Geography Table
        self.vcGeoList.willBeShowedInNewPost = true
        let frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.vcGeoList.view.frame.height)
        self.vcGeoList.view.frame = frame
        self.view.addSubview(vcGeoList.view)
        self.vcGeoList.view.isHidden = true
        
        //Calendar PIcker
        view.addSubview(vcCalendar.view)
        vcCalendar.view.isHidden = true
        
        //Load functionality
        enableSpecialTextRecognizer()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        tvText.becomeFirstResponder()
    }
    
    override func viewDidLayoutSubviews()
    {
        positionForAtacchments()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        view.endEditing(true)
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.GetProjectsLight)
        LibraryAPI.shared.requestBO.cancelRequest(service: Constants.RemoteService.GetStakeholderLight)
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        vcProjectTable = nil
        vcStakeholdersTable = nil
        //        vcCalendar = nil
    }
    
    // MARK: INITIAL CONFIG
    func loadConfig()
    {
        /**
        View Title
        */
        title = "NEW POST".localized()
        tvText.spellCheckingType = UITextSpellCheckingType.yes
        tvText.autocorrectionType = UITextAutocorrectionType.yes
        
        /**
        Navigation Bar
        */
        //Buttons
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
        barBtnActivity = UIBarButtonItem(customView: activityIndicator)
        
        
        let barBtnCancel = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.close))
        barBtnDone = UIBarButtonItem(title: "POST".localized(), style: .done, target: self,  action: #selector(self.savePost))
        
        //Adding to NavBar
        navigationItem.leftBarButtonItem = barBtnCancel
        navigationItem.rightBarButtonItem = barBtnDone
        
        
        /**
         Toolbar for Keyboard
         */
         //Toolbar
        let toolbarKey: UIToolbar! = UIToolbar()
        toolbarKey.barTintColor = UIColor.whiteCloud()
        toolbarKey.tintColor = UIColor.blackAsfalto()
        
        //Buttons
        btnAt = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        btnAt.setTitle("＠", for: UIControlState())
        btnAt.setTitleColor(UIColor.blackAsfalto(), for: UIControlState())
        btnAt.titleLabel!.font = UIFont.systemFont(ofSize: 30)
        btnAt.layer.borderColor = UIColor.blackAsfalto().cgColor
        btnAt.layer.borderWidth = 1
        btnAt.layer.cornerRadius = 3
        btnAt.addTarget(self, action: #selector(self.addAt), for: UIControlEvents.touchUpInside)
        
        btnCat = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        btnCat.setTitle("#", for: UIControlState())
        btnCat.setTitleColor(UIColor.blackAsfalto(), for: UIControlState())
        btnCat.titleLabel!.font = UIFont.systemFont(ofSize: 30)
        btnCat.layer.borderColor = UIColor.blackAsfalto().cgColor
        btnCat.layer.borderWidth = 1
        btnCat.layer.cornerRadius = 3
        btnCat.addTarget(self, action: #selector(self.addHashtag), for: UIControlEvents.touchUpInside)
        
        let btnPercent = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        btnPercent.setTitle("%", for: UIControlState())
        btnPercent.setTitleColor(UIColor.blackAsfalto(), for: UIControlState())
        btnPercent.titleLabel!.font = UIFont.systemFont(ofSize: 30)
        btnPercent.layer.borderColor = UIColor.blackAsfalto().cgColor
        btnPercent.layer.borderWidth = 1
        btnPercent.layer.cornerRadius = 3
        btnPercent.addTarget(self, action: #selector(self.addEvent), for: UIControlEvents.touchUpInside)

        let btnGeography = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        btnGeography.setTitle("!", for: UIControlState())
        btnGeography.setTitleColor(UIColor.blackAsfalto(), for: UIControlState())
        btnGeography.titleLabel!.font = UIFont.systemFont(ofSize: 30)
        btnGeography.layer.borderColor = UIColor.blackAsfalto().cgColor
        btnGeography.layer.borderWidth = 1
        btnGeography.layer.cornerRadius = 3
        btnGeography.addTarget(self, action: #selector(self.addGeography), for: UIControlEvents.touchUpInside)

        
        btnPrivacy = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        btnPrivacy.setImage(UIImage(named: "iconPublicBlack"), for: UIControlState())
        btnPrivacy.titleLabel!.font = UIFont.systemFont(ofSize: 30)
        btnPrivacy.layer.borderColor = UIColor.blackAsfalto().cgColor
        btnPrivacy.layer.borderWidth = 1
        btnPrivacy.layer.cornerRadius = 3
        btnPrivacy.addTarget(self, action: #selector(self.visibilityPost), for: UIControlEvents.touchUpInside)
        btnPrivacy.tag = 1
        
        btnAttach = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        btnAttach.setImage(UIImage(named: "iconAttach"), for: UIControlState())
        btnAttach.titleLabel!.font = UIFont.systemFont(ofSize: 30)
        btnAttach.layer.borderColor = UIColor.blackAsfalto().cgColor
        btnAttach.layer.borderWidth = 1
        btnAttach.layer.cornerRadius = 3
        btnAttach.addTarget(self, action: #selector(self.addAttachment), for: UIControlEvents.touchUpInside)
        
        //Toolbar items
        let barBtnMention = UIBarButtonItem(customView: btnAt)
        let barBtnHashtag = UIBarButtonItem(customView: btnCat)
        let barBtnEvent = UIBarButtonItem(customView: btnPercent)
        let barBtnGeography = UIBarButtonItem(customView: btnGeography)
        let barBtnAttachment =  UIBarButtonItem(customView: btnAttach)
        let barBtnSecurity =  UIBarButtonItem(customView: btnPrivacy)
        
        toolbarKey.items =
            [
                barBtnMention,
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
                barBtnHashtag,
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
                barBtnEvent,
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
                barBtnGeography,
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
                barBtnSecurity,
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
                barBtnAttachment
        ]
        
        toolbarKey.sizeToFit()
        
        tvText.inputAccessoryView = toolbarKey
        
        //Keyboard Notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
         //CollectionView: Do any additional setup after loading the view, typically from a nib.
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection =  UICollectionViewScrollDirection.horizontal
        layout.minimumInteritemSpacing = 4
        layout.minimumLineSpacing = 4
        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        layout.itemSize = CGSize(width: 50, height: 50)
        
        if let collectionViewAttachments = collectionViewAttachments {
            collectionViewAttachments.collectionViewLayout = layout
//            collectionViewAttachments.dataSource = self
//            collectionViewAttachments.delegate = self
            collectionViewAttachments.allowsMultipleSelection = false
            collectionViewAttachments.register(UINib(nibName: "AttachmentCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
            collectionViewAttachments.backgroundColor = UIColor.clear
        }
    }
    
    // MARK: - COLLECTION VIEW POSITION
    func positionForAtacchments()
    {
        //iPhone 4 = -157
        //iPhone 5 = - 69
        //iphone 6 = + 30
        //iPhone 6 plus = + 79
        
        /*
        autocorrecto abierto = 302
        autocorrecto cerrado = 269
        Dif = 33
        */
        
        let autoCorrectorOpen = (keyboardFrame.size.height >= 297) ? true : false
        
        if DeviceType.IS_IPHONE_4_OR_LESS
        {
            constraintCollectionViewAttachments.constant = (autoCorrectorOpen) ? 65 : 94
        }
        else if DeviceType.IS_IPHONE_5
        {
            constraintCollectionViewAttachments.constant = (autoCorrectorOpen) ? 153 : 182
        }
        else if DeviceType.IS_IPHONE_6
        {
            constraintCollectionViewAttachments.constant = (autoCorrectorOpen) ? 247 : 280
            
        }
        else if DeviceType.IS_IPHONE_6PLUS
        {
            constraintCollectionViewAttachments.constant = (autoCorrectorOpen) ? 303 : 338
        }
        else
        {
            //Ipad does't work yet
            guard constraintCollectionViewAttachments != nil else {
             
                return
            }
            
            constraintCollectionViewAttachments.constant = (autoCorrectorOpen) ? 303 : 338
        }
    }
    
    // MARK: LOAD DATA
    func loadStakeholders()
    {
        self.isExecutingStakeholderService = true
        
        //Indicator
        self.navigationItem.rightBarButtonItem = barBtnActivity
        activityIndicator.startAnimating()
        
        LibraryAPI.shared.stakeholderBO.getStakeholdersLight(onSuccess:{
            (stakeholders) -> () in
            
            //Indicator
            self.activityIndicator.stopAnimating()
            self.navigationItem.rightBarButtonItem = self.barBtnDone
            
            self.isExecutingStakeholderService = false
            self.arrayStakeholders = stakeholders
            self.loadProjects()
            
            //Inactive state
            self.btnAt.alpha = (self.arrayStakeholders.count > 0) ? 1 : 0.5
            
            }) { error in
                
                //Indicator
                self.activityIndicator.stopAnimating()
                self.navigationItem.rightBarButtonItem = self.barBtnDone
                
                self.isExecutingStakeholderService = false
                self.loadProjects()
                
                //Inactive state
                self.btnAt.alpha = (self.arrayStakeholders.count > 0) ? 1 : 0.5
                guard error.code != -999 else {return}
                MessageManager.shared.showBar(title: "Error".localized(),
                    subtitle: "An Error has ocurred trying to load stakeholder list".localized(),
                    type: .error,
                    fromBottom: false)
        }
    }
    
    func loadProjects()
    {
        self.isExecutingProjectsService = true
        
        //Indicator
        self.navigationItem.rightBarButtonItem = barBtnActivity
        activityIndicator.startAnimating()
        
        LibraryAPI.shared.projectBO.getProjectsLightList(onSuccess: {
            (projects) -> () in
            
            //Indicator
            self.activityIndicator.stopAnimating()
            self.navigationItem.rightBarButtonItem = self.barBtnDone
            
            self.isExecutingProjectsService = false
            self.arrayProjects = projects
            self.searchPreTags()
            
            //Inactive state
            self.btnCat.alpha = (self.arrayProjects.count > 0) ? 1 : 0.5
            
            }) { error in
                
                //Indicator
                self.activityIndicator.stopAnimating()
                self.navigationItem.rightBarButtonItem = self.barBtnDone
                
                self.isExecutingProjectsService = false
                
                //Inactive state
                self.btnCat.alpha = (self.arrayProjects.count > 0) ? 1 : 0.5
                guard error.code != -999 else {return}
                MessageManager.shared.showBar(title: "Error".localized(),
                    subtitle: "An Error has ocurred trying to load project list".localized(),
                    type: .error,
                    fromBottom: false)
        }
    }
    
    //MARK: FILTERS
    func filterStakeholders_Remove()
    {
        for tag in arrayStakeholdersAdded
        {
            for (index, stake) in arrayStakeholders.enumerated() where tag.0.id == stake.id
            {
                arrayStakeholders.remove(at: index)
                print("SH removido id = \(stake.id) \n \(stake.fullName)")
            }
        }
    }
    
    func filterStakeholders_Restore(_ stakeholder: Stakeholder)
    {
        arrayStakeholders.append(stakeholder)
        print("SH agrego id = \(stakeholder.id)")
    }
    
    func filterProjects_Remove()
    {
        for tag in arrayProjectsAdded
        {
            for (index, proj) in arrayProjects.enumerated() where tag.0.id == proj.id
            {
                arrayProjects.remove(at: index)
                print("P removido id = \(proj.id)")
            }
        }
    }
    
    func filterProjects_Restore(_ project: Project)
    {
        arrayProjects.append(project)
        print("P agrego id = \(project.id)")
    }
    
    func filterGeographies_Remove(geographies: [ClusterItem]) -> [ClusterItem]
    {
        var items = geographies
        for tag in self.arrayGeographiesAdded
        {
            for (index, geo) in items.enumerated() where tag.0.id == geo.id
            {
                items.remove(at: index)
                print("GEO removido id = \(geo.id) \n \(geo.title)")
            }
        }
        
        return items
    }
    
    func filterGeographies_Restore(_ geography: ClusterItem)
    {
        self.arrayGeographies.append(geography)
        print("GEO agrego id = \(geography.id)")
    }
    
    // MARK: PRE TAGS
    func searchPreTags()
    {
        //Search for Stakeholder tag
        if let stakeholderTag = stakeholderTagged as Stakeholder!
        {
            tvText.text = stakeholderTag.fullName
            
            let rangeOfFullName = tvText.text.range(of: stakeholderTag.fullName, options:NSString.CompareOptions.backwards)
            let finalText: String! = tvText.text.replacingCharacters(in: rangeOfFullName!, with:"\(stakeholderTag.fullName), ")
            
            //Verify Number of characters
            if finalText.characters.count > self.limitOfChracters
            {
                MessageManager.shared.showBar(title: "Warning".localized(),
                    subtitle:"You have exceeded the limit of characters".localized(),
                    type: .warning,
                    fromBottom: false)
                
                tvText.text = ""
            }
            else
            {
                tvText.text = finalText
                
                let tupleStakeholder = (stakeholder: stakeholderTag, range: rangeOfFullName!)
                arrayStakeholdersAdded.append(tupleStakeholder)
                filterStakeholders_Remove()
                
                textViewDidChange(self.tvText)
                vcStakeholdersTable.view.isHidden = true
                perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self.tvText, afterDelay: 0.1)
            }
        }
        
        //Search for Project tag
        if let projectTag = projectTagged as Project!
        {
            //Setting Text
            tvText.text = projectTag.title
            
            let rangeOfTitle = tvText.text.range(of: projectTag.title, options:NSString.CompareOptions.backwards)
            let finalText: String! = tvText.text.replacingCharacters(in: rangeOfTitle!, with:"\(projectTag.title), ")
            
            //Verify Number of characters
            if finalText.characters.count > self.limitOfChracters
            {
                MessageManager.shared.showBar(title: "Warning".localized(),
                    subtitle: "You have exceeded the limit of characters".localized(),
                    type: .warning,
                    fromBottom: false)
                
                tvText.text = ""
            }
            else
            {
                tvText.text = finalText
                
                let tupleProject = (project: projectTag, range: rangeOfTitle!)
                arrayProjectsAdded.append(tupleProject)
                filterProjects_Remove()
                
                textViewDidChange(self.tvText)
                vcProjectTable.view.isHidden = true
                perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self.tvText, afterDelay: 0.1)
            }
        }
        
        //Geographies
        if let geography = geographyTagged {
            
            tvText.text = geography.titleComplete
            
            let rangeOfTitle = tvText.text.range(of: geography.titleComplete, options:NSString.CompareOptions.backwards)
            let finalText: String! = tvText.text.replacingCharacters(in: rangeOfTitle!, with:"\(geography.titleComplete), ")
            
            //Verify Number of characters
            if finalText.characters.count > (limitOfChracters)
            {
                MessageManager.shared.showBar(title: "Warning".localized(),
                                              subtitle: "You have exceeded the limit of characters".localized(),
                                              type: .warning,
                                              fromBottom: false)
            }
            else
            {
                let rangeOfTitle = (finalText.range(of: geography.titleComplete, options:NSString.CompareOptions.backwards)?.lowerBound)!..<finalText.index((finalText.endIndex), offsetBy: -3)
                
                self.tvText.text = finalText
                
                let tupleGeography = (geography: geography, range: rangeOfTitle)
                self.arrayGeographiesAdded.append(tupleGeography)
                self.vcGeoList.vcGeoTable.geographiesAdded.append(geography)
                self.textViewDidChange((self.tvText))
                                
                self.perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self.tvText, afterDelay: 0.1)
            }
        }
        
        if let dateTag = self.dateTagged
        {
            let dateString = dateTag.getDateStringForPost()
            self.tvText.text = dateString
            
            let rangeOfTitle = self.tvText.text.range(of: dateString, options:NSString.CompareOptions.backwards)
            let finalText: String! = self.tvText.text.replacingCharacters(in: rangeOfTitle!, with:"\(dateString), ")
            
            if finalText.characters.count > self.limitOfChracters
            {
                MessageManager.shared.showBar(title: "Warning".localized(),
                    subtitle: "You have exceeded the limit of characters".localized(),
                    type: .warning,
                    fromBottom: false)
                tvText.text = ""
            }
            else
            {
                self.tvText.text = finalText
                let date = Event()
                date.date = dateTag
                date.id = 0
                date.allDay = true
                
                let tupleEvent = (date: date, range: rangeOfTitle!)
                self.arrayDateAdded.append(tupleEvent)
                
                self.textViewDidChange(self.tvText)
                self.vcCalendar.view.isHidden = true
                 perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self.tvText, afterDelay: 0.1)
            }
        }
    }
    
    // MARK: KEYBOARD NOTIFICATION
    func keyboardWillShow(_ notification: Notification)
    {
        guard constraintTextViewBottom != nil else {
            
            return
        }
        var info = notification.userInfo!
        keyboardFrame = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        let rectHeight = keyboardFrame.size.height
        constraintTextViewBottom.constant = rectHeight + 54//8.0
        
        //Reload position for attachments
        positionForAtacchments()
        
        //Resize textview and go to bottom
        changeDynamicSize(tvText)
    }
    
    // MARK: LOAD FUNCTIONALITY
    func enableSpecialTextRecognizer()
    {
        typeMentionsAndHashtags = WordManager(text: "") {
            (text, magicWord, charRange, searchWordRange) in
                                    
            if text.characters.count == 0
            {
                self.currentMentionChar = magicWord.rawValue
                self.currentMentionCharRange = charRange
            }
            
            if magicWord == SpecialWord.Mention
            {
                let arrayStakeHoldersFilter: [Stakeholder]!
                
                if text == ""
                {
                    arrayStakeHoldersFilter = self.arrayStakeholders
                }
                else
                {
                    arrayStakeHoldersFilter = self.arrayStakeholders.filter({
                        
                        ($0.fullName as NSString).localizedCaseInsensitiveContains(text) || ($0.aliasString as NSString).localizedCaseInsensitiveContains(text)
                    })
                }
                
                self.vcStakeholdersTable.view.isHidden = (arrayStakeHoldersFilter.count > 0) ? false : true
                
                //Filter Table
                self.vcStakeholdersTable.loadStakeholders(arrayStakeHoldersFilter)
                if let stakeholdersResultTable = self.vcStakeholdersTable.tvResults {
                
                    stakeholdersResultTable.reloadData()
                }
                self.vcStakeholdersTable.addContactToInstance = {
                    (contact) in
                    
                    //Setting Text
                  //demo  rangeOfText.lowerBound = text.index(rangeOfText.lowerBound, offsetBy: -1)
                    print(contact.fullName)
                    let replacementString = contact.fullName + ", "
                    let finalText: String = self.tvText.text.replacingCharacters(in: searchWordRange, with: replacementString)
                    
                    //Verify Number of characters
                    if finalText.characters.count > self.limitOfChracters
                    {
                        MessageManager.shared.showBar(title: "Warning".localized(),
                            subtitle: "You have exceeded the limit of characters".localized(),
                            type: .warning,
                            fromBottom: false)
                    }
                    else
                    {
                        //let rangeOfFullName = finalText.range(of: contact.fullName, options:NSString.CompareOptions.backwards)
                        let rangeOfFullName = (finalText.range(of: contact.fullName, options:NSString.CompareOptions.backwards)?.lowerBound)!..<finalText.index((finalText.endIndex), offsetBy: -3)

                        self.tvText.text = finalText
                        
                        let tupleStakeholder = (stakeholder: contact as! Stakeholder, range: rangeOfFullName)
                        self.arrayStakeholdersAdded.append(tupleStakeholder)
                        self.filterStakeholders_Remove()
                        
                        self.textViewDidChange(self.tvText)
                        self.vcStakeholdersTable.view.isHidden = true
                        self.perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self.tvText, afterDelay: 0.1)
                    }
                }
            }
            else if magicWord == SpecialWord.Hashtag
            {
                let arrayProjects: [Project]!
                
                if text == ""
                {
                    arrayProjects = self.arrayProjects
                }
                else
                {
                    arrayProjects =  self.arrayProjects.filter({
                        ($0.title as NSString).localizedCaseInsensitiveContains(text)
                    })
                }
                
                self.vcProjectTable.view.isHidden = (arrayProjects.count > 0) ? false : true
                
                //Filter Table
                self.vcProjectTable.loadProjects(arrayProjects)
                if let projectsResultTable = self.vcProjectTable.tvResults {
                
                    projectsResultTable.reloadData()
                }
                self.vcProjectTable.onProjectSelected = {
                    (selectedProject) in
                    
                    //Setting Text
                   //demo rangeOfText.lowerBound = text.index(rangeOfText.lowerBound, offsetBy: -1)
                    
                    let finalText: String! = self.tvText.text.replacingCharacters(in: searchWordRange, with:"\(selectedProject.title), ")
                    
                    //Verify Number of characters
                    if finalText.characters.count > self.limitOfChracters
                    {
                        MessageManager.shared.showBar(title: "Warning".localized(),
                            subtitle: "You have exceeded the limit of characters".localized(),
                            type: .warning,
                            fromBottom: false)
                    }
                    else
                    {
                        //let rangeOfTitle = finalText.range(of: selectedProject.title, options:NSString.CompareOptions.backwards)
                        let rangeOfTitle = (finalText.range(of: selectedProject.title, options:NSString.CompareOptions.backwards)?.lowerBound)!..<finalText.index((finalText.endIndex), offsetBy: -3)
                        
                        self.tvText.text = finalText
                        
                        let tupleProject = (project: selectedProject, range: rangeOfTitle)
                        self.arrayProjectsAdded.append(tupleProject)
                        self.filterProjects_Remove()
                        
                        self.textViewDidChange(self.tvText)
                        
                        self.vcProjectTable.view.isHidden = true
                        self.perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self.tvText, afterDelay: 0.1)
                    }
                }
            }
            else if magicWord == SpecialWord.CalendarDate
            {
                self.vcCalendar.hidden = (text.characters.count == 0) ? false : true
                self.changeDynamicSize(self.tvText)
                
                self.vcCalendar.onDateSelected = {
                    (date) in
                    
                    //Setting Text
                   //demo rangeOfText.lowerBound = text.index(rangeOfText.lowerBound, offsetBy: -1)
                    
                    let dateString: String! = (date.allDay) ? date.date.getDateStringForPost() : "\(date.date.getDateTimeStringForPost())"
                    let replacementString = dateString + ", "
                    let finalText: String! = self.tvText.text.replacingCharacters(in: searchWordRange, with: replacementString)
                    
                    //Verify Number of characters
                    if finalText.characters.count > self.limitOfChracters
                    {
                        MessageManager.shared.showBar(title: "Warning".localized(),
                            subtitle: "You have exceeded the limit of characters".localized(),
                            type: .warning,
                            fromBottom: false)
                    }
                    else
                    {
                        //Avoiding duplicated dates
                        for event in self.arrayDateAdded
                        {
                            let eventString: String! = event.0.allDay ? event.0.date.getDateStringForPost() : "\(event.0.date.getDateTimeStringForPost())"
                            
                            if eventString == dateString
                            {
                                MessageManager.shared.showBar(title: "Info".localized(),
                                    subtitle:"This date already has been selected".localized(),
                                    type: .info,
                                    fromBottom: false)
                                
                                self.textViewDidChange(self.tvText)
                                self.perform(#selector(UITextViewDelegate.textViewDidChange(_:)), with: self.tvText, afterDelay: 0.1)
                                
                                return
                            }
                        }
                        //let rangeOfFullName = (finalText.range(of: contact.fullName, options:NSString.CompareOptions.backwards)?.lowerBound)!..<finalText.index((finalText.endIndex), offsetBy: -3)
                        //let rangeOfDate = finalText.range(of: dateString, options:NSString.CompareOptions.backwards)
                        let rangeOfDate = (finalText.range(of: dateString, options:NSString.CompareOptions.backwards)?.lowerBound)!..<finalText.index((finalText.endIndex), offsetBy: -3)
                        self.tvText.text = finalText
                        
                        let tupleEvent = (date: date, range: rangeOfDate)
                        self.arrayDateAdded.append(tupleEvent)
                        
                        self.textViewDidChange(self.tvText)
                        self.perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self.tvText, afterDelay: 0.1)
                    }
                }
            }
            else if magicWord == SpecialWord.Geography
            {
                guard text != " " else {
                    
                    self.hideGeographyTable()
                    return
                }
                self.vcGeoList.vcGeoTable.geoQuery.getAllWithPagination()
                if text == ""
                {
                    self.vcGeoList.vcGeoTable.geoQuery.searchText = "A"
                }
                else
                {
                    self.vcGeoList.vcGeoTable.clusterItems.removeAll()
                    self.vcGeoList.vcGeoTable.geoQuery.searchText = text
                }
                self.activityIndicator.startAnimating()
                self.navigationItem.rightBarButtonItem = self.barBtnActivity
                self.vcGeoList.vcGeoTable.geoListMode = .all
                self.vcGeoList.vcGeoTable.refreshTable()
                
                self.vcGeoList.vcGeoTable.onDidLoadGeographies = {[weak self] numberOfitems in
                                                            
                    //UI-> Update visibility of table and focus textView
                    self?.activityIndicator.stopAnimating()
                    self?.navigationItem.rightBarButtonItem = self?.barBtnDone
                    self?.vcGeoList.view.isHidden = false
                    self?.changeDynamicSize(self?.tvText)
                    self?.tvText.scrollToBottom()
                }
                self.vcGeoList.vcGeoTable.onDidSelectGeography = {[weak self] geography in
                
                    let finalText: String! = self?.tvText.text.replacingCharacters(in: searchWordRange, with:"\(geography.titleComplete), ")
                    
                    //Verify Number of characters
                    if finalText.characters.count > (self?.limitOfChracters) ?? 0
                    {
                        MessageManager.shared.showBar(title: "Warning".localized(),
                                                      subtitle: "You have exceeded the limit of characters".localized(),
                                                      type: .warning,
                                                      fromBottom: false)
                    }
                    else
                    {
                        let rangeOfTitle = (finalText.range(of: geography.titleComplete, options:NSString.CompareOptions.backwards)?.lowerBound)!..<finalText.index((finalText.endIndex), offsetBy: -3)
                        
                        self?.tvText.text = finalText
                        
                        let tupleGeography = (geography: geography, range: rangeOfTitle)
                        self?.arrayGeographiesAdded.append(tupleGeography)
                        self?.vcGeoList.vcGeoTable.geographiesAdded.append(geography)
                        self?.textViewDidChange((self?.tvText)!)
                        
                        self?.vcGeoList.view.isHidden = true
                        self?.perform(#selector(UITextViewDelegate.textViewDidChangeSelection(_:)), with: self?.tvText, afterDelay: 0.1)
                    }
                }
            }
        }
    }
    
    // MARK: ACTIONS
    func close()
    {
        dismiss(animated: true) { () -> Void in
            //here more code if you want
        }
    }
    
    func savePost()
    {
        guard arrayStakeholdersAdded.count > 0 || arrayProjectsAdded.count > 0 || arrayDateAdded.count > 0 || arrayGeographiesAdded.count > 0  else
        {
            MessageManager.shared.showBar(title: "Info".localized(),
                                                            subtitle: "It's necessary include a mention (stakeholder / project / geography) or a date".localized(),
                                                            type: .info,
                                                            fromBottom: false)
            return
        }
        
        //Indicator
        self.navigationItem.rightBarButtonItem = barBtnActivity
        activityIndicator.startAnimating()
        
        LibraryAPI.shared.feedBO.publicNewPost(parameters: createRequest(), onSuccess: {
            () -> () in
            
            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                //Indicator
                self.activityIndicator.stopAnimating()
                self.navigationItem.rightBarButtonItem = self.barBtnDone
                self.didPostGeography?()
                //Reload Feed
                self.onViewControllerClosed?()
                NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadHomeFeed"), object: nil)
                
                self.dismiss(animated: true) { () -> Void in
                    
                    MessageManager.shared.showBar(title: "Success".localized(),
                        subtitle:"The post's been created".localized(),
                        type: .success,
                        fromBottom: false)
                }
            }
            
            }) { error in
                
                let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    
                    //Indicator
                    self.activityIndicator.stopAnimating()
                    self.navigationItem.rightBarButtonItem = self.barBtnDone
                    
                    MessageManager.shared.showBar(title: "Error",
                        subtitle: "There was a problem. The new post hasn't been posted".localized(),
                        type: .error,
                        fromBottom: false)
                }
        }
    }
    
    func createRequest() -> [String : Any]
    {
        //Array Dates
        var arrayDates = [[String : Any]]()
        
        //Body
        var bodyText = tvText.text
        
        //Stakeholders
        for text in arrayStakeholdersAdded
        {
            bodyText = bodyText?.replacingOccurrences(of: text.0.fullName, with: "{{s=\(text.0.id)}}")
        }
        //Projects
        for text in arrayProjectsAdded
        {
            bodyText = bodyText?.replacingOccurrences(of: text.0.title, with: "{{p=\(text.0.id)}}")
        }
        //Geographies
        for text in arrayGeographiesAdded
        {
            bodyText = bodyText?.replacingOccurrences(of: text.0.titleComplete, with: "{{g=\(text.0.id)}}")
        }
        //Dates
        for (index, tuple) in arrayDateAdded.enumerated()
        {
            tuple.0.id = index + 1 //set an id
            if tuple.0.allDay == true {
                bodyText = bodyText?.replacingOccurrences(of: tuple.0.date.getDateStringForPost(), with: "{{d=\(tuple.0.id)}}")
            }
            else {
                bodyText = bodyText?.replacingOccurrences(of: tuple.0.date.getDateTimeStringForPost(), with: "{{d=\(tuple.0.id)}}")
            }
            
            arrayDates.append(tuple.0.getWSObject())
        }
        
        
        
        
        //Attachments
        var arrayPhotos = [[String : Any]]()
        
        for attachment in arrayAttachments
        {
            arrayPhotos.append(["Token" : attachment.token])
        }
        
        var wsObject: [String : Any]  = [:]
        wsObject["Body"] = bodyText
        wsObject["Dates"] = arrayDates
        wsObject["Attachments"] = arrayPhotos
        wsObject["IsPrivate"] = Int(NSNumber(value: isPrivate))
        
        return wsObject
    }
    
    //MARK: ACTION KEYBOARD
    func addAttachment()
    {
        let alertSheet = UIAlertController(title:nil, message:nil, preferredStyle: .actionSheet)
        alertSheet.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler:nil))
        
        alertSheet.addAction(UIAlertAction(title: "Take Photo".localized(), style: UIAlertActionStyle.default, handler: {
            (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                imagePicker.allowsEditing = false
                imagePicker.modalPresentationStyle = .overCurrentContext
                
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        alertSheet.addAction(UIAlertAction(title: "Choose Existing".localized(), style: UIAlertActionStyle.default, handler: {
            (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) {
                
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                imagePicker.allowsEditing = false
                imagePicker.modalPresentationStyle = .overCurrentContext
                
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        self.present(alertSheet, animated: true, completion: nil)
    }
    
    func addAtbyKeyboard() -> Bool
    {
        if self.isExecutingStakeholderService == true || self.isExecutingProjectsService == true
        {
            MessageManager.shared.showBar(title: "Info".localized(),
                                          subtitle: "Loading stakeholder list...".localized(),
                                          type: .info,
                                          fromBottom: false)
            return false
        }
        return true
    }
    
    func addAt() 
    {
        if self.isExecutingStakeholderService == true || self.isExecutingProjectsService == true
        {
            MessageManager.shared.showBar(title: "Info".localized(),
                subtitle: "Loading stakeholder list...".localized(),
                type: .info,
                fromBottom: false)
        }
        else
        {
            if self.isExecutingStakeholderService == false && self.isExecutingProjectsService == false && arrayStakeholders.count == 0
            {
                let alert = UIAlertController(title:"Warning".localized(),
                                              message: "It wasn't possible to load the stakeholder list".localized(),
                                              preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Close".localized(), style: .cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "try again".localized(), style: .default, handler: { [weak self]
                    (action) in
                    
                    self?.loadStakeholders()
                    
                    }))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                self.tvText.text = self.tvText.text + "@"
            }
        }
    }
    
    func addHashtagByKeyboard() -> Bool
    {
        if self.isExecutingProjectsService == true || self.isExecutingStakeholderService == true
        {
            MessageManager.shared.showBar(title: "Info".localized(),
                                          subtitle: "Loading project list...".localized(),
                                          type: .info,
                                          fromBottom: false)
            return false
        }
        return true
    }
    
    func addHashtag()
    {
        if self.isExecutingProjectsService == true || self.isExecutingStakeholderService == true
        {
            MessageManager.shared.showBar(title: "Info".localized(),
                subtitle: "Loading project list...".localized(),
                type: .info,
                fromBottom: false)
        }
        else
        {
            if self.isExecutingStakeholderService == false && self.isExecutingProjectsService == false && arrayProjects.count == 0
            {
                let alert = UIAlertController(title:"Warning".localized(),
                message: "It wasn't possible to load the project list".localized(),
                preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Close".localized(), style: .cancel, handler:nil))
                alert.addAction(UIAlertAction(title: "try again".localized(), style: .default, handler: { [weak self]
                    (action) in
                    
                    self?.loadProjects()
                    
                    }))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                self.tvText.text = self.tvText.text + "#"
            }
        }
    }
    
    func addEvent()
    {
        self.tvText.text = self.tvText.text + "%"
    }
    
    func addGeography()
    {
        self.tvText.text = self.tvText.text + "!"
        self.textViewDidChange(self.tvText)
    }
    
    func visibilityPost()
    {
        btnPrivacy.tag = (btnPrivacy.tag == 1) ? 0 : 1
        isPrivate = (btnPrivacy.tag == 1) ? false : true
        
//        let iconName = (btnPrivacy.tag == 0) ? "iconPrivate" : "iconPublic"
//        let imageIcon: UIImage! =  UIImage(named: iconName)
        
        if btnPrivacy.tag == 0
        {
            btnPrivacy.setImage(UIImage(named: "iconPrivateBlack"), for: UIControlState())
            
            MessageManager.shared.showBar(title: "Status".localized(),
                                          subtitle: "Private Post".localized(),
                                          type: .info,
                                          fromBottom: false)
            
        }
        else
        {
            btnPrivacy.setImage(UIImage(named: "iconPublicBlack"), for: UIControlState())
            
            MessageManager.shared.showBar(title: "Status".localized(),
                                          subtitle: "Public Post".localized(),
                                          type: .info,
                                          fromBottom: false)
        }
    }
    
    //MARK: DYNAMIC SIZE
    func changeDynamicSize(_ textView: UITextView!)
    {
        limitHeightTextview = self.collectionViewAttachments.frame.origin.y - 2
        
        //        if limitHeightTextview == nil && textView.frame.size.height != 151
        //        {
        //            limitHeightTextview = textView.frame.size.height
        //            print(limitHeightTextview)@
        //        }
        
        //Fill Dynamic Frame
        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        
        var rectDynamicFrame: CGRect! = textView.frame
        rectDynamicFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        
        //Verifing limitHeightTextview isn't nil
        if limitHeightTextview != nil
        {
            //Texview max size
            if rectDynamicFrame.height > (limitHeightTextview - 5)
            {
                rectDynamicFrame.size.height = collectionViewAttachments.frame.origin.y - 5
            }
            
            
            //Texview resize to show tables
            if vcStakeholdersTable.view.isHidden == true && vcProjectTable.view.isHidden == true && vcCalendar.view.isHidden == true && vcGeoList.view.isHidden == true
            {
                textView.frame = rectDynamicFrame;
            }
            else
            {
                rectDynamicFrame.size.height = 30.5
                textView.frame = rectDynamicFrame
            }
            
            //Size for Tables
            vcStakeholdersTable.view.frame = CGRect(x: 0,y: textView.frame.size.height, width: ScreenSize.SCREEN_WIDTH , height: limitHeightTextview + 25)
            vcProjectTable.view.frame = CGRect(x: 0, y: textView.frame.size.height, width: ScreenSize.SCREEN_WIDTH , height: limitHeightTextview + 25)
            vcCalendar.view.frame = CGRect(x: 0, y: textView.frame.size.height, width: ScreenSize.SCREEN_WIDTH , height: limitHeightTextview + 25)
        }
    }
    
    // MARK: UTILITIES
    fileprivate func hideGeographyTable()
    {
        vcGeoList.view.isHidden = true
        self.vcGeoList.vcGeoTable.cancelSearchRequests()
        self.activityIndicator.stopAnimating()
        self.navigationItem.rightBarButtonItem = self.barBtnDone
    }
    
    func suddenDeletionCheck(_ text: String)
    {
        if text.isEmpty == true
        {
            print("sudden deletion")
            
            //Remove stakeholders selected and restore arrayStakeholders
            while arrayStakeholdersAdded.count > 0
            {
                if let stakeholderAdded = arrayStakeholdersAdded.last {
                    
                    filterStakeholders_Restore(stakeholderAdded.0)
                    arrayStakeholdersAdded.removeLast()
                }
            }
            
            //Remove Projects selected and restore arrayStakeholders
            while arrayProjectsAdded.count > 0
            {
                if let projectAdded = arrayProjectsAdded.last {
                    
                    filterProjects_Restore(projectAdded.0)
                    arrayProjectsAdded.removeLast()
                }
            }
            
            //Remove Events if any
            while arrayDateAdded.count > 0
            {
                arrayDateAdded.removeLast()
            }
            
            //Remove Geographies
            self.vcGeoList.vcGeoTable.geographiesAdded.removeAll()
        }
    }

    // MARK: TEXTVIEW DELEGATE
    func textViewDidChange(_ textView: UITextView)
    {
        //Set Style
        let attributedString = NSMutableAttributedString(string:textView.text)
        attributedString.setColorAndBold(arrayStakeholdersAdded, projects: arrayProjectsAdded, geographies: arrayGeographiesAdded, events: arrayDateAdded)
        textView.attributedText = attributedString
        
        //Check for sudden deletion
        self.suddenDeletionCheck(textView.text.trim())
        
        //Verify Mentions added
        var arrayStakeholderUpdated = arrayStakeholdersAdded
        for tuple in arrayStakeholdersAdded
        {
            //Remove it!
            if textView.text.containsString(tuple.0.fullName) == false
            {
                let rangeWithoutTheLastCharacter = tuple.1
                if  rangeWithoutTheLastCharacter.upperBound <= textView.text.endIndex
                {
                    let finalText: String! = textView.text.replacingCharacters(in: rangeWithoutTheLastCharacter, with:"")
                    //Refresh Style
                    let attributedString = NSMutableAttributedString(string:finalText)
                    attributedString.setColorAndBold(arrayStakeholdersAdded, projects: arrayProjectsAdded, geographies: arrayGeographiesAdded, events: arrayDateAdded)
                    textView.attributedText = attributedString
                }
                arrayStakeholderUpdated = arrayStakeholderUpdated.filter({$0.0.id != tuple.0.id})
                filterStakeholders_Restore(tuple.0)
            }
        }
        arrayStakeholdersAdded = arrayStakeholderUpdated
        
        //Verify Projects added
        var arrayProjectsUpdated = arrayProjectsAdded
        for tuple in arrayProjectsAdded
        {
            if textView.text.containsString(tuple.0.title) == false
            {
                //Remove it!
                let rangeWithoutTheLastCharacter = tuple.1
                if rangeWithoutTheLastCharacter.upperBound <= textView.text.endIndex
                {
                    let finalText: String! = textView.text.replacingCharacters(in: rangeWithoutTheLastCharacter, with:"")
                    //Refresh Style
                    let attributedString = NSMutableAttributedString(string:finalText)
                    attributedString.setColorAndBold(arrayStakeholdersAdded, projects: arrayProjectsAdded, geographies: arrayGeographiesAdded, events: arrayDateAdded)
                    textView.attributedText = attributedString
                }
                arrayProjectsUpdated = arrayProjectsUpdated.filter({$0.0.id != tuple.0.id})
                filterProjects_Restore(tuple.0)
            }
        }
        arrayProjectsAdded = arrayProjectsUpdated
        
        //Verify Geographies added
        var arrayGeographiesUpdated = arrayGeographiesAdded
        let cursorPosition = textView.position(from: textView.beginningOfDocument, offset: (textView.selectedRange.toRange()?.lowerBound)!)
        let offsetPosition = textView.offset(from: textView.beginningOfDocument, to: cursorPosition!)
        print("cursor: \(offsetPosition)")
        for tuple in self.arrayGeographiesAdded
        {
            //Remove it!
            var shouldRemove = false
            if cursorPosition == textView.endOfDocument
            {
                
                shouldRemove = textView.text.containsString(tuple.0.titleComplete) == false || ((textView.text.range(of: textView.text)?.upperBound)! <= tuple.1.upperBound)
            }
            else
            {
                shouldRemove = textView.text.containsString(tuple.0.titleComplete) == false
            }
            if shouldRemove == true
            {
                let rangeWithoutTheLastCharacter = tuple.1
                if  rangeWithoutTheLastCharacter.upperBound <= textView.text.endIndex
                {
                    let finalText: String! = textView.text.replacingCharacters(in: rangeWithoutTheLastCharacter, with:"")
                    //Refresh Style
                    let attributedString = NSMutableAttributedString(string:finalText)
                    attributedString.setColorAndBold(arrayStakeholdersAdded, projects: self.arrayProjectsAdded, geographies: arrayGeographiesAdded, events: self.arrayDateAdded)
                    textView.attributedText = attributedString
                }
                arrayGeographiesUpdated = arrayGeographiesUpdated.filter({$0.0.id != tuple.0.id})
                self.vcGeoList.vcGeoTable.geographiesAdded = self.vcGeoList.vcGeoTable.geographiesAdded.filter({$0.id != tuple.0.id})
                filterGeographies_Restore(tuple.0)
            }
        }
        arrayGeographiesAdded = arrayGeographiesUpdated
        
        //Verify Events added
        var arrayDateUpdated = arrayDateAdded
        for tuple in arrayDateAdded
        {
            let dateString: String! = (tuple.0.allDay) ? tuple.0.date.getDateStringForPost() : "\(tuple.0.date.getDateTimeStringForPost())"
            
            if textView.text.containsString(dateString) == false
            {
                //Remove it!
                let rangeWithoutTheLastCharacter = tuple.1
              //demo  rangeWithoutTheLastCharacter.upperBound = textView.text.index(rangeWithoutTheLastCharacter.upperBound, offsetBy: -1)
                
                if rangeWithoutTheLastCharacter.upperBound <= textView.text.endIndex
                {
                    let finalText: String! = textView.text.replacingCharacters(in: rangeWithoutTheLastCharacter, with:"")
                    //Refresh Style
                    let attributedString = NSMutableAttributedString(string:finalText)
                    attributedString.setColorAndBold(arrayStakeholdersAdded, projects: arrayProjectsAdded, geographies: arrayGeographiesAdded, events: arrayDateAdded)
                    textView.attributedText = attributedString
                }
                arrayDateUpdated = arrayDateUpdated.filter({$0.0.date != tuple.0.date})
            }
        }
        arrayDateAdded = arrayDateUpdated
        
        //Elements Showed
        if let currentMentionCharRange = currentMentionCharRange {
            
            if textView.attributedText.string.characters.endIndex < currentMentionCharRange.upperBound
                || textView.attributedText.string.substring(with: currentMentionCharRange) != currentMentionChar!
            {
                vcStakeholdersTable.view.isHidden = true
                vcProjectTable.view.isHidden = true
                vcCalendar.hidden = true
                self.hideGeographyTable()
            }
        }
        
        changeDynamicSize(textView)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if text == "@"
        {
            return self.addAtbyKeyboard()
        }
        else if text == "#"
        {
            return self.addHashtagByKeyboard()
        }
        
        if textView.text.characters.count + (text.characters.count - range.length) <= limitOfChracters
        {
            self.showLimitCharactersMsg = true
            return true
        }
        
        if(self.showLimitCharactersMsg == true)
        {
            self.showLimitCharactersMsg = false
            MessageManager.shared.showBar(title: "Warning".localized(),
                                                            subtitle:"You have exceeded the limit of characters".localized(),
                                                            type: .warning,
                                                            fromBottom: false)
        }
        
        return false
    }
    
    func textViewDidChangeSelection(_ textView: UITextView)
    {
        typeMentionsAndHashtags.text = textView.text
        
        changeDynamicSize(textView)
    }
    
    // MARK: IMAGE PICKER DELEGATE
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        dismiss(animated: true) { () -> Void in
            
            self.positionForAtacchments()
            
            if let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
            {
                self.activityIndicator.startAnimating()
                self.btnAttach.isEnabled = false
                self.btnAttach.alpha = 0.5
                
                //Create Request Object
                var wsObject: [String : Any]  = [:]
                wsObject["OldFile"] = String()
                wsObject["VersionId"] = 1
                
                var imageData: Data?
                var validImage: UIImage?
                
                if selectedImage.size.width > 500 {
                    validImage = selectedImage.resizeToWidth(500.0)
                }
                else {
                    validImage = selectedImage
                }
                
                imageData = UIImagePNGRepresentation(validImage!)
                
                self.uploadCount += 1
                let localUrl = validImage!.saveToTempDirectory("mobileUpload\(self.uploadCount).png")
                
                LibraryAPI.shared.attachmentBO.uploadFile(fileSize: imageData!.count, fileURL: localUrl, onSuccess: {
                    (token) in
                    
                    self.activityIndicator.stopAnimating()
                    self.btnAttach.isEnabled = true
                    self.btnAttach.alpha = 1
                    
                    let attachPicture = AttachmentWithToken(token: token, image: validImage!)
                    self.arrayAttachments.append(attachPicture)
                    self.collectionViewAttachments.reloadData()
                    
                    MessageManager.shared.showStatusBar(title: "Attachment Uploaded".localized(),
                        type: .success,
                        containsIcon: false)
                    
                    }, onError: { error in
                        
                        self.activityIndicator.stopAnimating()
                        self.btnAttach.isEnabled = true
                        self.btnAttach.alpha = 1
                        
                        MessageManager.shared.showBar(title: "Error".localized(),
                            subtitle: "There was an error trying to save an attachment".localized(),
                            type: .error,
                            fromBottom: false)
                })
            }
        }
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: NSErrorPointer?, contextInfo:UnsafeRawPointer)
    {
        if error != nil
        {
            let alert = UIAlertController(title: "Save Failed".localized(),
                                          message: "Failed to save image".localized(),
                                          preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "OK".localized(),  style: .cancel, handler: nil)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: { () -> Void in
                self.positionForAtacchments()
            })
        }
    }
    
    // MARK: CollectionView Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrayAttachments.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! AttachmentCell
        
        cell.imageAttachment.image = arrayAttachments[indexPath.row].picture
        cell.btnDelete.addGestureRecognizer(ActionsTapGestureRecognizer(onTap: {
            
            self.arrayAttachments = self.arrayAttachments.filter({ $0 !== self.arrayAttachments[indexPath.row] })
            self.collectionViewAttachments.reloadData()
            
        }))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let vcViewer = ImageViewer(image: arrayAttachments[indexPath.row].picture)
        vcViewer.onRemoveThis = {
            (image) in
            self.arrayAttachments = self.arrayAttachments.filter({ $0.picture !== image })
            self.collectionViewAttachments.reloadData()
        }
        
        let navController = UINavigationController(rootViewController: vcViewer)
        navController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        navController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        present(navController, animated: true, completion: nil)
    }
}
