//
//  KillAppController.swift
//  SHM
//
//  Created by Manuel Salinas on 7/2/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class KillAppController: UIViewController
{
    //MARK: OuTLETS & PROPERTIES
    @IBOutlet weak fileprivate var lblTitle: UILabel!
    @IBOutlet weak fileprivate var lblCounter: UILabel!
    @IBOutlet weak fileprivate var ivCloud: UIImageView!
    @IBOutlet weak fileprivate var btnClose: UIButton!
    
    fileprivate var seconds = 0
    fileprivate var timer = Timer()
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.lblTitle.text = "New settings have been downloaded, SHM needs to close".localized()
        
        self.btnClose.setTitle("Accept".localized(), for: UIControlState())
        self.btnClose.cornerRadius()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MRK: ACTIONS
    @IBAction func startGhostProtocol()
    {
        
        self.btnClose.isEnabled = false
        self.btnClose.alpha = 0.85
        
        UIView.animate(withDuration: 1.0, animations: {
            
            self.ivCloud.alpha = 0
            
            }, completion: { finished in
                
                self.seconds = 3
                OperationQueue.main.addOperation({
                    
                    self.lblCounter.text = "\(self.seconds)"
                })
                
                
                UIView.animate(withDuration: 0.5, animations: {
                    
                    self.lblCounter.alpha = 1
                    
                    }, completion: { finished in
                        
                        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.countDown), userInfo: nil, repeats: true)
                })
        })
    }
    
    func countDown()
    {
        self.seconds -= 1
        
        OperationQueue.main.addOperation({
            
            self.lblCounter.text = "\(self.seconds)"
        })
        
        if self.seconds == 0
        {
            self.timer.invalidate()
            
            //kill
            exit(0)
        }
    }

}
