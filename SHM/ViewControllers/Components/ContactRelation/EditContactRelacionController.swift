//
//  EditContactRelacionController.swift
//  SHM
//
//  Created by Manuel Salinas on 5/30/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class EditContactRelacionController: UIViewController
{
    // MARK: - OUTLETS AND PROPERTIES
    @IBOutlet weak fileprivate var imgContact: UIImageView!
    @IBOutlet weak fileprivate var txtRelationSelector: UITextField!
    @IBOutlet weak fileprivate var lblContactName: UILabel!
    @IBOutlet weak fileprivate var lblContactJob: UILabel!
    @IBOutlet weak fileprivate var btnDeleteContact: UIButton!
    @IBOutlet weak fileprivate var lblRelationDescription: UILabel!
    
    var isNewRelation = false
    var contact: Contact
    var onSaveRelation: ((_ contactRelationed: Contact) -> ())?
    var onRelationDelete:((_ contactRelationed: Contact) -> ())?
    var types: [KeyValueObjectWithKeyValueArray]?
    var compSelector: Int = 0
    var rowSelector: Int = 0
    var defaultRelationStr : String?
    
    // MARK: - LIFE CYCLE
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        self.contact = Contact(isDefault: true)
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        self.contact = Contact(isDefault: true)
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: - CONFIG
    fileprivate func loadConfig()
    {
        // Bar Button Items
        let barBtnSave = UIBarButtonItem(title: "Save".localized(), style: .plain, target: self, action:#selector(self.save))
        navigationItem.rightBarButtonItem = barBtnSave
        
        //Contact data
        self.imgContact.setImageWith(URL(string: contact.thumbnailUrl), placeholderImage: UIImage(named: "defaultperson"))
        self.imgContact.setBorder()
        self.lblContactName.text = contact.fullName
        let strJobPosition = contact.jobPosition?.value ?? ""
        let strCompanyName = contact.companyName
        self.lblContactJob.attributedText = NSMutableAttributedString().setJobPositionCompany(strJobPosition, company: strCompanyName)
        
        self.lblRelationDescription.text =  "Relationship".localized()
        self.lblRelationDescription.textAlignment = NSTextAlignment.center
        self.txtRelationSelector.sizeToFit()
        self.txtRelationSelector.setBorder(UIColor.blueBelizeAlphy())
        
        let imgCombo = UIImage(named: "iconCombo")
        self.txtRelationSelector.rightView = UIImageView(image: imgCombo)
        self.txtRelationSelector.rightViewMode = UITextFieldViewMode.always

        
        //Edit configs for new and old contacts
        if(self.isNewRelation)
        {
            self.title = "ADD A RELATION".localized()
            self.btnDeleteContact.isHidden = true
            self.txtRelationSelector.placeholder = "Choose Relation".localized()
        }
        else
        {
            self.title = "EDIT RELATION".localized()
            let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
            
            guard let rel1 = self.contact.relationType1?.value, let rel2 = self.contact.relationType2?.value, let rel1_ES = self.contact.relationType1?.valueEs, let rel2_ES = self.contact.relationType2?.valueEs else {
            
                return
            }
            
            
            self.txtRelationSelector.text = !(language == "es") ? "\(rel1) - \(rel2)" : "\(rel1_ES) - \(rel2_ES)"
        }
    }
    
    // MARK: TOOLBAR KEYBOARD
    func loadToolBar() -> UIToolbar
    {
        let toolBarKeyboard = UIToolbar()
        toolBarKeyboard.barStyle = UIBarStyle.blackOpaque;
        toolBarKeyboard.barTintColor = UIColor.colorForNavigationController()
        
        let barBtnDone = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action:#selector(self.closeKeyboard))
        barBtnDone.tintColor = UIColor.white
        
        toolBarKeyboard.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            barBtnDone]
        
        toolBarKeyboard.sizeToFit()
        
        return toolBarKeyboard
    }
    
    func closeKeyboard()
    {
        view.endEditing(true)
    }
    
    // MARK: ACTION BUTTONS
    func save()
    {
        if self.contact.relationType1?.value.isEmpty == true
        {
            MessageManager.shared.showBar(title: "Warning".localized(),
                                                            subtitle: "You haven't chosen a relationship".localized(),
                                                            type: .warning,
                                                            fromBottom: false)
        }
        else
        {
            self.onSaveRelation?(contact)
        }
    }
    
    func close()
    {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func deleteContact(_ sender: AnyObject)
    {
        if self.onRelationDelete != nil
        {
            onRelationDelete? (contact)
        }
    }
}

// MARK: - TEXTFIELD DELEGATE
extension EditContactRelacionController: UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        navigationItem.rightBarButtonItem?.isEnabled = false
        
        textField.inputAccessoryView = loadToolBar()
        
        let pickerRelationType = UIPickerView()
        pickerRelationType.dataSource = self
        pickerRelationType.delegate = self
        pickerRelationType.backgroundColor = UIColor.groupTableViewBackground
        pickerRelationType.showsSelectionIndicator = true
        pickerRelationType.isMultipleTouchEnabled = false
        pickerRelationType.isExclusiveTouch = true
        textField.inputView  = pickerRelationType
        
        //Default Relation
        if let type = self.types {
            
            let language =  Locale.current.identifier.replacingOccurrences(of: "_", with: "-")
            self.txtRelationSelector.text = !(language == "es-US" || language == "es-MX" || language == "es_ES" || language == "es_419") ? "\(type[0].value) - \(type[0].keyValues[0].value)" : "\(type[0].valueEs) - \(type[0].keyValues[0].valueEs)"
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        guard let relation = types else {
            
            return
        }
        
        //Set Relations
        let relationT1 = KeyValueObject()
        relationT1.key = relation[compSelector].key
        relationT1.value = relation[compSelector].value
        relationT1.valueEs = relation[compSelector].valueEs

        let relationT2 = KeyValueObject()
        relationT2.key = relation[compSelector].keyValues[rowSelector].key
        relationT2.value = relation[compSelector].keyValues[rowSelector].value
        relationT2.valueEs = relation[compSelector].keyValues[rowSelector].valueEs
        
        self.contact.relationType1 = relationT1
        self.contact.relationType2 = relationT2
        
        navigationItem.rightBarButtonItem?.isEnabled = true
    }
}

// MARK: - PICKER DELEGATE & DATA SOURCE
extension EditContactRelacionController: UIPickerViewDataSource, UIPickerViewDelegate
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        guard let relation = types else {
            
            return 0
        }
        
        if component == 0
        {
            return relation.count
        }
        else
        {
            guard relation.count > 0 else {
                
                return relation.count
            }
            return relation[compSelector].keyValues.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
        guard let relation = types else {
            
            return ""
        }
        
        if component == 0
        {
            return !(language == "es") ?  relation[row].value : relation[row].valueEs
        }
        else
        {
            self.txtRelationSelector.text = !(language == "es") ? "\(relation[compSelector].value) - \(relation[compSelector].keyValues[row].value)" : "\(relation[compSelector].valueEs) - \(relation[compSelector].keyValues[row].valueEs)"
            
            return !(language == "es") ? relation[compSelector].keyValues[row].value : relation[compSelector].keyValues[row].valueEs
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        guard let relation = self.types else {
            
            return
        }
        
        if component == 0
        {
            compSelector = row
            let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
            self.txtRelationSelector.text = !(language == "es") ? "\(relation[compSelector].value) - \(relation[compSelector].keyValues[0].value)" : "\(relation[compSelector].valueEs) - \(relation[compSelector].keyValues[0].valueEs)"
            
            pickerView.reloadComponent(1)
        }
        else
        {
            rowSelector = row
            let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
            self.txtRelationSelector.text = !(language == "es") ? "\(relation[compSelector].value) - \(relation[compSelector].keyValues[row].value)" : "\(relation[compSelector].valueEs) - \(relation[compSelector].keyValues[row].valueEs)"
        }
    }

}
