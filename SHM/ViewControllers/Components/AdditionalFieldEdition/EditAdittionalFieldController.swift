//
//  EditAdittionalFieldController.swift
//  SHM
//
//  Created by Manuel Salinas on 5/26/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class EditAdittionalFieldController: UIViewController
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak fileprivate var btnSave: UIButton!
    @IBOutlet weak fileprivate var tvContent: UITextView!
    @IBOutlet weak fileprivate var txtDescription: UITextField!
    
    var onAdittionalFieldSaved:((_ field: StringKeyValueObject) -> ())?
    var language = Language.english {
        didSet {
            
            self.additionalField.language = self.language == .english ? "en" : "es"
        }
    }
    
    var additionalField = StringKeyValueObject() {
        didSet {
            
            self.strDescription = self.additionalField.key
            self.strContent = self.additionalField.value
            self.language = self.additionalField.language == "es" ? .spanish : .english
        }
    }
    
    //Private
    fileprivate var strDescription = String()
    fileprivate var strContent = String()

    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        //Text
        self.txtDescription.placeholder = "Description".localized()
        
        if self.strDescription.trim().isEmpty == false
        {
            self.txtDescription.text = self.strDescription
        }
        
        if self.strContent.trim().isEmpty == false
        {
            self.tvContent.text = self.strContent
        }
        
        self.btnSave.setTitle("✚ Add Field".localized(), for: UIControlState())
        self.btnSave.addTarget(self, action: #selector(self.saveAdditionalField), for: .touchUpInside)
        
        //Style
        self.txtDescription.setBorder()
        self.tvContent.setBorder()
        
        //Round
        self.btnSave.cornerRadius()
        self.tvContent.cornerRadius()
        self.txtDescription.cornerRadius()
    }
    
    //MARK: ACTIONS
    func saveAdditionalField()
    {
        self.view.endEditing(true)
        
        guard self.txtDescription.text?.trim().isEmpty == false else {
            
            self.txtDescription.shakeAnimation()
            return
        }
        
        guard self.tvContent.text?.trim().isEmpty == false else {
            
            self.tvContent.shakeAnimation()
            return
        }
        
        self.onAdittionalFieldSaved?(additionalField)
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: TEXTFIELD DELEGATE
extension EditAdittionalFieldController: UITextFieldDelegate
{
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField == self.txtDescription
        {
            if let text = textField.text, text.trim().isEmpty == false
            {
                self.additionalField.key = text
            }
        }
    }
}

//MARK: TEXTVIEW DELEGATE
extension EditAdittionalFieldController: UITextViewDelegate
{
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView == self.tvContent
        {
            if let text = textView.text, text.trim().isEmpty == false
            {
                self.additionalField.value = text
            }
        }
    }
}
