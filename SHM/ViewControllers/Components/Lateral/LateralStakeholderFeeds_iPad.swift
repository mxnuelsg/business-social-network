//
//  LateralStakeholderFeeds_iPad.swift
//  SHM
//
//  Created by Manuel Salinas on 5/13/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

private enum PageDot: Int
{
    case recent
    case backup
    case attachment
}

class LateralStakeholderFeeds_iPad: UIViewController
{
    //MARK: OUTLETS & VARIABLES
    @IBOutlet weak var scrollLateral: UIScrollView!
    @IBOutlet weak var containerFeedRecent: UIView!
    @IBOutlet weak var containerFeedBackup: UIView!
    @IBOutlet weak var containerAttachments: UIView!
    
    //Controllers
    var vcFeedRecent: FeedsTable!
    var vcFeedBackup: FeedsTable!
    var vcFeedAttachments: FeedsTable!
    
    //Pages
    var currentPage = 0
    var onPageDidChange: ((_ page:Int) -> ())?
    var stakeholder = Stakeholder(isDefault: true) {
        didSet {
            
            self.loadFeeds()
        }
    }
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: LOAD CONFIG
    func loadConfig()
    {
        //Sky Background
        self.scrollLateral.backgroundColor = UIColor.blueSky()
        
        //Borders
        self.vcFeedRecent.setBorder()
        self.vcFeedBackup.setBorder()
        self.vcFeedAttachments.setBorder()
    }
    
    //MARK: ACTIONS
    func loadFeeds()
    {
        switch self.currentPage
        {
        case PageDot.recent.rawValue:
            self.vcFeedRecent.posts = self.stakeholder.posts
        case PageDot.backup.rawValue:
            self.getFeed(self.stakeholder.id, type: PageDot.backup)
        case PageDot.attachment.rawValue:
            self.getFeed(self.stakeholder.id, type: PageDot.attachment)
        default:
            break
        }
    }
    
    //MARK: WEB SERVICE
    fileprivate func getFeed(_ stakeholderId: Int, type: PageDot)
    {
        //Request Object
        var wsObject: [String : Any]  = [:]
        wsObject["PostId"] = 0
        wsObject["StakeholderId"] = self.stakeholder.id
        wsObject["Type"] = type == .backup ? 2 : 3
        
        //Indicator
        if type == .backup
        {
            if self.vcFeedBackup.posts.count == 0
            {
                self.vcFeedBackup.displayBackgroundMessage("Loading...".localized(),
                                                           subMessage: "")
            }
        }
        else
        {
            if self.vcFeedAttachments.posts.count == 0
            {
                self.vcFeedAttachments.displayBackgroundMessage("Loading...".localized(),
                                                                subMessage: "")
            }
        }
        
        LibraryAPI.shared.stakeholderBO.getFeedEditingStakeholder(parameters: wsObject, onSuccess: {
            (feed) in
            
            //Remove Indicator
            if type == .backup
            {
                self.vcFeedBackup.dismissBackgroundMessage()
                self.vcFeedBackup.posts = feed.posts
            }
            else
            {
                self.vcFeedAttachments.dismissBackgroundMessage()
                self.vcFeedAttachments.posts = feed.posts
            }
            
        }) { (error) in
            
            //Remove Indicator
            if type == .backup
            {
                self.vcFeedBackup.dismissBackgroundMessage()
            }
            else
            {
                self.vcFeedAttachments.dismissBackgroundMessage()
            }
            
            MessageManager.shared.showBar(title: "Error",
                                                            subtitle: "Error.TryAgain".localized(),
                                                            type: .error,
                                                            fromBottom: false)
        }
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let id = segue.identifier, id == "RecentFeed"
        {
            self.vcFeedRecent = segue.destination as! FeedsTable
            self.vcFeedRecent.type = .recents
        }
        
        if let id = segue.identifier, id  == "BackupFeed"
        {
            self.vcFeedBackup = segue.destination as! FeedsTable
            self.vcFeedBackup.type = .backups
        }
        
        if let id = segue.identifier, id  == "AttachmentsFeed"
        {
            self.vcFeedAttachments = segue.destination as! FeedsTable
            self.vcFeedAttachments.type = .attachments
        }
    }
}

//MARK: SCROLL DELEGATE
extension LateralStakeholderFeeds_iPad: UIScrollViewDelegate
{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let width = scrollView.frame.size.width;
        let page = Int((scrollView.contentOffset.x + (0.5 * width)) / width);
        
        if self.currentPage != page
        {
            self.currentPage = page
            self.onPageDidChange?(self.currentPage)
            self.loadFeeds()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if self.onPageDidChange != nil
        {
//            self.onPageDidChange!(page:self.currentPage)
        }
    }
}
