//
//  LateralCalendarFeed_iPad.swift
//  SHM
//
//  Created by Manuel Salinas on 11/24/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import Spruce

class LateralCalendarFeed_iPad: UIViewController
{
    //MARK: OUTLETS & VARIABLES
    @IBOutlet weak var scrollLateral: UIScrollView!
    @IBOutlet weak var viewGoCalendarContainer: UIView!
    @IBOutlet weak var segmentedController: UISegmentedControl!
    @IBOutlet weak var containerFeedFiles: UIView!
    @IBOutlet weak var containerAttachments: UIView!
    
    @IBOutlet weak var btnGoToCalendar: UIButton!
    //Controllers
    var vcCalendar: CalendarViewController!
    var vcFeedList: PostsTableViewController!
    var vcFile: FileListViewController!
    
    //Pages
    var currentPage = 0
    var onPageDidChange: ((_ page:Int) -> ())?
    var onGoCalendar: (() -> ())?
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: LOAD CONFIG
    func loadConfig()
    {
        self.localize()
        //Scroll background
        self.viewGoCalendarContainer.setBorder()
        
        //Sky Background
        self.vcFeedList.tableView.backgroundColor = UIColor.blueSky()
        self.vcFile.tableFiles.backgroundColor = UIColor.blueSky()
        
        self.setBorder()
        self.scrollLateral.backgroundColor = UIColor.blueSky()
        
        self.btnGoToCalendar.layer.cornerRadius = 3
        self.btnGoToCalendar.clipsToBounds = true
        
        //Calendar
        if self.vcCalendar != nil
        {
            self.vcCalendar.configureForEmbed()
        }
        
        //Segmented
        self.segmentedController.selectedSegmentIndex = 0
        self.segmentedController.addTarget(self, action: #selector(self.segmentedControlHandler(_:)), for: .valueChanged)
    }
    
    func localize()
    {
        self.btnGoToCalendar.setTitle("GO TO CALENDAR".localized(), for: UIControlState())
        self.segmentedController.setTitle("FEED".localized(), forSegmentAt: 0)
        
        self.segmentedController.setTitle("ATTACHMENTS".localized(), forSegmentAt: 1)
        
    }
    // MARK: - ACTIONS
    @IBAction func goToCalendar()
    {
        self.onGoCalendar?()
    }
    
    //MARK: WEB SERVICES
    func loadFeedForProject(_ project: Project!)
    {
        //Indicators
        self.vcFeedList.displayBackgroundMessage("Loading...".localized(), subMessage: "")
        self.vcFile.tableFiles.displayBackgroundMessage("Loading...".localized(), subMessage: "")
        
        LibraryAPI.shared.feedBO.getProjectFeed(project, onSuccess: { posts in
            
            self.vcFeedList.posts = posts
            self.vcFeedList.tableView?.reloadData()
            
            //Spruce framework Animation
            DispatchQueue.main.async {
                
                self.vcFeedList.tableView.spruce.animate([.contract(.moderately)], animationType: SpringAnimation(duration: 0.7))
            }
            
            //Empty State
            self.vcFeedList.posts.count == 0 ? self.vcFeedList.displayBackgroundMessage("No posts".localized(), subMessage: "") : self.vcFeedList.dismissBackgroundMessage()
            
        }) { error in
            
            self.vcFeedList.tableView.dismissBackgroundMessage()
            
            //Empty State
            if self.vcFeedList.posts.count == 0
            {
                self.vcFeedList.tableView.displayBackgroundMessage("Error".localized(), subMessage: "An Error has ocurred trying to load the Feed".localized())
            }
        }
        
        //Attachments
        LibraryAPI.shared.attachmentBO.getFilesByProjectId(project.id, onSuccess: { files in
            
            self.vcFile.attachments = files
            self.vcFile.tableFiles.reloadData()
            
            //Spruce framework Animation
            DispatchQueue.main.async {
                
                self.vcFile.tableFiles.spruce.animate([.contract(.moderately)], animationType: SpringAnimation(duration: 0.7))
            }
            
            //Empty State
            self.vcFile.attachments.count == 0 ? self.vcFile.tableFiles.displayBackgroundMessage("No Attachments Found".localized(), subMessage: "") : self.vcFile.tableFiles.dismissBackgroundMessage()
            
        }) { error in
            
            self.vcFile.tableFiles.dismissBackgroundMessage()
            
            //Empty State
            if self.vcFile.attachments.count == 0
            {
                self.vcFile.tableFiles.displayBackgroundMessage("Error".localized(), subMessage: "An error has ocurred loading the attachments list".localized())
            }
        }
    }
    
    func loadFeedForStakeholder(_ stakeholder: Stakeholder!)
    {
        //Indicators
        self.vcFeedList.displayBackgroundMessage("Loading...".localized(), subMessage: "")
        self.vcFile.tableFiles.displayBackgroundMessage("Loading...".localized(),  subMessage: "")
        
        LibraryAPI.shared.feedBO.getStakeholderFeed(stakeholder, onSuccess: { posts in
            
            self.vcFeedList.posts = posts
            self.vcFeedList.tableView.reloadData()
            
            //Spruce framework Animation
            DispatchQueue.main.async {
                
                self.vcFeedList.tableView.spruce.animate([.contract(.moderately)], animationType: SpringAnimation(duration: 0.7))
            }
            
            //Empty State
            self.vcFeedList.posts.count == 0 ? self.vcFeedList.displayBackgroundMessage("No posts".localized(), subMessage: "") : self.vcFeedList.dismissBackgroundMessage()
            
        }) { error in
            
            self.vcFeedList.tableView.dismissBackgroundMessage()
            
            //Empty State
            if self.vcFeedList.posts.count == 0
            {
                self.vcFeedList.tableView.displayBackgroundMessage("Error".localized(), subMessage: "An Error has ocurred trying to load the Feed".localized())
            }
        }
        
        //Attachments
        LibraryAPI.shared.attachmentBO.getFilesByStakeholderId(stakeholder.id, onSuccess: { files in
            
            self.vcFile.attachments = files
            self.vcFile.tableFiles.reloadData()
            
            //Spruce framework Animation
            DispatchQueue.main.async {
                
                self.vcFile.tableFiles.spruce.animate([.contract(.moderately)], animationType: SpringAnimation(duration: 0.7))
            }
            
            //Empty State
            self.vcFile.attachments.count == 0 ? self.vcFile.tableFiles.displayBackgroundMessage("No Attachments Found".localized(), subMessage: "") : self.vcFile.tableFiles.dismissBackgroundMessage()
            
            
        }) { error in
            
            self.vcFile.tableFiles.dismissBackgroundMessage()
            
            //Empty State
            if self.vcFile.attachments.count == 0
            {
                self.vcFile.tableFiles.displayBackgroundMessage("Error".localized(), subMessage: "An error has ocurred loading the attachments list".localized())
            }
        }
    }
    
    //MARK: LOAD CALENDAR
    func loadCalendarForStakeholder(_ stakeholder: Stakeholder!)
    {
        self.vcCalendar.loadByStakeholderId = stakeholder.id
        self.vcCalendar.loadFullCalendarById()
    }
    
    func loadCalendarForProject(_ project: Project!)
    {
        self.vcCalendar.loadByProjectId = project.id
        self.vcCalendar.loadFullCalendarById()
    }
    

    //MARK: SEGMENTED CONTROL ACTIONS
    func segmentedControlHandler(_ segmented: UISegmentedControl)
    {
        if segmented.selectedSegmentIndex == 0
        {
            self.containerFeedFiles.isHidden = false
            self.containerAttachments.isHidden = true
        }
        else
        {
            self.containerFeedFiles.isHidden = true
            self.containerAttachments.isHidden = false
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let id = segue.identifier, id == "segueID_CalendarViewController"
        {
            self.vcCalendar = segue.destination as! CalendarViewController
            self.vcCalendar.activeBlueForIpad = true
            self.vcCalendar.loadEventsAutomatically = false
        }
        
        if let id = segue.identifier, id  == "segueID_PostsTableViewController"
        {
            self.vcFeedList = segue.destination as! PostsTableViewController
            self.vcFeedList.activeBlueForIpad = true
        }
        
        if let id = segue.identifier, id  == "FileListViewController"
        {
            self.vcFile = segue.destination as! FileListViewController
            self.vcFile.activeBlueForIpad = true
        }
    }
}


//MARK: SCROLL DELEGATE
extension LateralCalendarFeed_iPad: UIScrollViewDelegate
{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let width = scrollView.frame.size.width;
        let page = Int((scrollView.contentOffset.x + (0.5 * width)) / width);
        
        if self.currentPage != page
        {
            self.currentPage = page
            self.onPageDidChange?(self.currentPage)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if self.onPageDidChange != nil
        {
            self.onPageDidChange!(self.currentPage)
        }
    }
}
