//
//  LateralCalendars_iPad.swift
//  SHM
//
//  Created by Manuel Salinas on 11/23/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class LateralCalendars_iPad: UIViewController
{
    //MARK: OUTLETS & VARIABLES
    @IBOutlet weak var scrollLateral: UIScrollView!
    
    var vcCalendar: CalendarViewController!
    var vcCalendarList: CalendarListViewController!
    
    //Pages
    var currentPage = 0
    var onPageDidChange: ((_ page:Int) -> ())?
    
     //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: LOAD CONFIG
    func loadConfig()
    {
        //Scroll background
        self.scrollLateral.backgroundColor = UIColor.blueSky()

        //Calendar
        if self.vcCalendar != nil
        {
            self.vcCalendar.configureForEmbed()
        }
        
        if self.vcCalendarList != nil
        {
            self.vcCalendarList.configureForEmbed()
        }
    }
    

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier! == "segueID_CalendarViewController"
        {
            self.vcCalendar = segue.destination as! CalendarViewController
            self.vcCalendar.activeBlueForIpad = true
        }
        
        if segue.identifier! == "segueID_CalendarListViewController"
        {
            self.vcCalendarList = segue.destination as! CalendarListViewController
            self.vcCalendarList.activeBlueForIpad = true
        }
    }
}

//MARK: SCROLL DELEGATE
extension LateralCalendars_iPad: UIScrollViewDelegate
{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let width = scrollView.frame.size.width;
        let page = Int((scrollView.contentOffset.x + (0.5 * width)) / width);
        
        if self.currentPage != page
        {
            self.currentPage = page
            self.onPageDidChange?(self.currentPage)
        }
    }
}
