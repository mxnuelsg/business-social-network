//
//  EditReferenceController.swift
//  SHM
//
//  Created by Manuel Salinas on 5/26/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import UIKit

class EditReferenceController: UIViewController
{
    //MARK: PROPERTIES & OUTLETS
    @IBOutlet weak fileprivate var btnPrivacy: UIButton!
    @IBOutlet weak fileprivate var btnSave: UIButton!
    @IBOutlet weak fileprivate var txtIndex: UITextField!
    @IBOutlet weak fileprivate var txtReference: UITextField!
    
    fileprivate var isPrivate = false
    var onReferenceSaved:((_ reference: StakeholderReference, _ isUrl: Bool) -> ())?
    var reference = StakeholderReference() {
        didSet{
        
            self.isPrivate = self.reference.isPrivate
            self.strOrdinalValue = "\(self.reference.ordinalIndicator)"
            self.strReference =  self.reference.value
        }
    }
    
    var language = Language.english {
        didSet {
            
            self.reference.language = self.language == .spanish ? "es" : "en"
        }
    }
    
    //Private
    fileprivate var imgIcon = UIImage()
    fileprivate var strOrdinalValue = String()
    fileprivate var strReference = String()
    
    //MARK: LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        
    }
    
    //MARK: CONFIG
    fileprivate func loadConfig()
    {
        //Text
        self.txtIndex.placeholder = "Index".localized()
        self.txtReference.placeholder =  "Reference".localized()
        
        if self.strOrdinalValue.trim().isEmpty == false
        {
            self.txtIndex.text = self.strOrdinalValue
        }
        
        if self.strOrdinalValue.trim().isEmpty == false
        {
            self.txtReference.text = self.strReference
        }
        
        self.btnSave.setTitle("Save".localized(), for: UIControlState())
        self.btnSave.addTarget(self, action: #selector(self.saveReference), for: .touchUpInside)
        
        self.imgIcon = self.isPrivate == true ? UIImage(named: "iconRoundSelected")! : UIImage(named: "iconRoundUnselected")!
        self.btnPrivacy.setTitle(" Private".localized(), for: UIControlState())
        self.btnPrivacy.setImage(self.imgIcon, for: UIControlState())
        self.btnPrivacy.addTarget(self, action: #selector(self.selectPrivacy), for: .touchUpInside)
        
        //Style
        self.txtReference.setBorder()
        self.txtIndex.setBorder()
    }
    
    //MARK: ACTIONS
    func canOpenURL(_ string: String?) -> Bool
    {
        guard let urlString = string else {return false}
        guard let url = URL(string: urlString) else {return false}
        if !UIApplication.shared.canOpenURL(url) {return false}
        
        let regEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
        return predicate.evaluate(with: string)
    }
    
    func selectPrivacy()
    {
        if self.isPrivate == false
        {
            self.btnPrivacy.setImage(UIImage(named: "iconRoundSelected"), for: UIControlState())
            self.isPrivate = true
        }
        else
        {
            self.btnPrivacy.setImage(UIImage(named: "iconRoundUnselected"), for: UIControlState())
            self.isPrivate = false
        }
        
        //Save
        self.reference.isPrivate = self.isPrivate
    }
    
    func saveReference()
    {
        self.view.endEditing(true)
        
        guard self.txtIndex.text?.trim().isEmpty == false else {
        
            self.txtIndex.shakeAnimation()
            return
        }
        
        guard self.txtReference.text?.trim().isEmpty == false else {
            
            self.txtReference.shakeAnimation()
            return
        }
        
        //Check if is url
        let text = self.txtReference.text!
        let arrayDot = text.components(separatedBy: ".")
        
        if let _ = URL(string: text) {
            
            if arrayDot.count > 1
            {
                if self.canOpenURL(text) == false
                {
                    self.txtReference.shakeAnimation()
                    
                    OperationQueue.main.addOperation({
                        
                        MessageManager.shared.showBar(title: "Info".localized(),
                            subtitle: "Your URL is not valid (It needs to start with http:// or https://)",
                            type: .info,
                            fromBottom: false)
                    })
                }
                else
                {
                    self.onReferenceSaved?(self.reference, true)
                    self.dismiss(animated: true, completion: nil)
                }
            }
            else
            {
                self.onReferenceSaved?(self.reference, false)
                self.dismiss(animated: true, completion: nil)
            }
        }
        else
        {
            //It's Text with spaces
            self.onReferenceSaved?(self.reference, false)
            self.dismiss(animated: true, completion: nil)
        }
    }
}

//MARK: TEXTFIELD DELEGATE
extension EditReferenceController: UITextFieldDelegate
{
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField == self.txtIndex
        {
            if let text = textField.text, text.trim().isEmpty == false
            {
                self.reference.ordinalIndicator = Int(text)!
            }
        }
        
        if textField == self.txtReference
        {
            if let text = textField.text, text.trim().isEmpty == false
            {
                self.reference.value = text
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == self.txtIndex
        {
            //Just Numbers
            let inverseSet = CharacterSet.decimalDigits.inverted
            let components = string.components(separatedBy: inverseSet)
            let filtered = components.joined(separator: "")
            
            return string == filtered
        }
        
        return true
    }
}
