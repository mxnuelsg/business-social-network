//
//  ContactListCell.swift
//  SHM
//
//  Created by Manuel Salinas on 8/3/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ContactListCell: UITableViewCell
{
    // MARK: - OUTLETS
    @IBOutlet weak var imgActor: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var imgFollowed: UIImageView!
    @IBOutlet weak var lblAlias: UILabel!
    @IBOutlet weak var lblSite: UILabel!
    
    @IBOutlet weak var lblAdminRating: UILabel!
    @IBOutlet weak var lblUserRating: UILabel!
    @IBOutlet weak var ratingViewContainer: UIView!
    
    
    @IBOutlet weak var viewRatingContainer: UIView!
    var didTapOnRating:(()->())?
    // MARK: - LIFE CYCLE
    override func awakeFromNib()
    {
         /** Image */
        self.imgActor.clipsToBounds = true
        
         /** Labels */
        self.lblName.numberOfLines = 2
        self.lblPosition.numberOfLines = 2
        
        self.lblName.adjustsFontSizeToFitWidth = true
        self.lblPosition.adjustsFontSizeToFitWidth = true
        self.lblSite.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: CELL TYPES
    func cellForStakeholder(_ stakeholder: Stakeholder)
    {
        //Image
        self.imgActor.setImageWith(URL(string: stakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        
        //Name
        self.lblName.text = "\(stakeholder.fullName)"
        
        //Position
        let strJobPosition = stakeholder.jobPosition?.value ?? ""
        let attributedPosition = NSMutableAttributedString().setJobPositionCompany(strJobPosition, company: stakeholder.companyName)
        self.lblPosition.attributedText = attributedPosition
        
        //Alias
        self.lblAlias.text =  stakeholder.aliasString.trim().isEmpty == false ? "Alias: \(stakeholder.aliasString)" : String()
        
        //Nationalities or  Sites
        if stakeholder.nationalities.count > 0
        {
            let nationNames = stakeholder.nationalities.map({$0.value})
            let nationalities = "⚑ \(nationNames.collapseWithCommas())"
            let siteTitleAtt = NSMutableAttributedString(string: nationalities)
            siteTitleAtt.setColor(nationalities, color: UIColor.grayConcreto())
            siteTitleAtt.setBold(nationalities, size: 11)
            self.lblSite.attributedText = siteTitleAtt
        }
        else if let site = stakeholder.site, site.title.isEmpty == false {
            
            //Sites
            let siteTitle = "⚑ \(site.title)"
            let siteTitleAtt = NSMutableAttributedString(string: siteTitle)
            siteTitleAtt.setColor(siteTitle, color: UIColor.grayConcreto())
            siteTitleAtt.setBold(siteTitle, size: 11)
            self.lblSite.attributedText = siteTitleAtt
        }
        else
        {
            self.lblSite.text = String()
        }
        
        //Rating        
        self.lblUserRating.text = (stakeholder.userRating ?? 0).getString()
        self.lblAdminRating.text = (stakeholder.adminRating ?? 0).getString()
        self.ratingViewContainer.isUserInteractionEnabled = true
        self.ratingViewContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapOnRating)))
    }
    
    func tapOnRating()
    {
        self.didTapOnRating?()
    }
    
    func cellForMember(_ member: Member)
    {
        self.cellForContact(member as Contact)
    }
    
    func cellForContact(_ contact: Contact) -> ()
    {
        self.imgActor.setImageWith(URL(string: contact.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))
        self.lblName.text = "\(contact.fullName)"
        
        let strJobPosition = contact.jobPosition?.value ?? ""
        let attributedPosition = NSMutableAttributedString().setJobPositionCompany(strJobPosition, company: contact.companyName)
        self.lblPosition.attributedText = attributedPosition
    }
}
