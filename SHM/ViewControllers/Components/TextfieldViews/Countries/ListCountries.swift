//
//  ListCountries.swift
//  SHM
//
//  Created by Manuel Salinas on 9/28/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ListCountries: UITableViewController
{
    var arrayCountries = [KeyValueObject]()
    var arrayCountriesSelected = [KeyValueObject]()
    var isMultiselect = false
    var onSelectedCountries: ((_ countries:[KeyValueObject]) -> ())?
    var onSelectedCountry: ((_ country:KeyValueObject) -> ())?
    var language = Language.english {
        didSet {
    
            self.tableView.reloadData()
        }
    }

    
    init (list: [KeyValueObject])
    {
        self.arrayCountries = list
        super.init(nibName: "ListCountries", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView.reloadData()
        tableView.allowsMultipleSelection = isMultiselect
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayCountries.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell = UITableViewCell(style:.default, reuseIdentifier:"Cell")
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        cell.textLabel?.textColor = UIColor.blackAsfalto()
        
        cell.textLabel?.text = language == .english ? arrayCountries[indexPath.row].value : arrayCountries[indexPath.row].valueEs
        
        if self.isMultiselect == true
        {
            if arrayCountriesSelected.contains(arrayCountries[indexPath.row])
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let cell = tableView.cellForRow(at: indexPath) else {
        
            print("The TableViewCell is not a Cell")
            return
        }
        
        if self.isMultiselect == true
        {
            if cell.accessoryType == .none
            {
                cell.accessoryType = .checkmark
                arrayCountriesSelected.append(arrayCountries[indexPath.row])
                
            }
            else
            {
                cell.accessoryType = .none
                arrayCountriesSelected = arrayCountriesSelected.filter {$0 != arrayCountries[indexPath.row] }
            }
            
            self.onSelectedCountries?(self.arrayCountriesSelected)
        }
        else
        {
            self.onSelectedCountry?(self.arrayCountries[indexPath.row])
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return UIView()
    }
    
}
