//
//  RatingViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 8/8/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class RatingViewController: UIViewController
{
    @IBOutlet weak var btnRate: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var viewStarBar: UIView!
    
    var stakeholder = Stakeholder(isDefault: true)
    var urlProfile = String()
    var stakeholderName = String()
    var rating:Float = 0.0
    var stakeholderId: Int = -1
    var didEndRating:(()->())?
    
    @IBOutlet weak var imgStar0: UIImageView!
    @IBOutlet weak var imgStar1: UIImageView!
    @IBOutlet weak var imgStar2: UIImageView!
    @IBOutlet weak var imgStar3: UIImageView!
    @IBOutlet weak var imgStar4: UIImageView!
    
    var arrayStars: [UIImageView]?
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    fileprivate func loadConfig()
    {
        let question = "How would you rate".localized()
        self.lblRating.text = question + "\n" + "\(self.stakeholder.fullName)?"
        self.imgProfile.setImageWith(URL(string: self.stakeholder.thumbnailUrl), placeholderImage:UIImage(named: "defaultperson"))        
        self.btnRate.cornerRadius()
        self.arrayStars = [self.imgStar0,self.imgStar1,self.imgStar2,self.imgStar3,self.imgStar4]
        self.arrayStars?.forEach({ (star) in
            star.isUserInteractionEnabled = true
            let tg = UITapGestureRecognizer(target: self, action: #selector(updateStarBar(sender:)))
            star.addGestureRecognizer(tg)
        })
    }
    
    @objc fileprivate func updateStarBar(sender: Any)
    {
        if let img = (sender as? UIGestureRecognizer)?.view {
            
            self.rating = Float(img.tag + 1)
            self.fillStars(selectedStar: img.tag)
        }
    }
 
    fileprivate func fillStars(selectedStar: Int)
    {
        for i in 0..<(selectedStar + 1)
        {
            self.arrayStars?[i].image = UIImage(named:"iconStar")
        }
        for i in (selectedStar + 1)..<(self.arrayStars?.count ?? 5)
        {
            self.arrayStars?[i].image = UIImage(named:"iconStarGray")
        }
    }
    
    @IBAction func rate(_ sender: Any)
    {
        let rateRequest = RateRequest()
        rateRequest.givenRating = self.rating
        rateRequest.stakeholderId = self.stakeholder.id
        MessageManager.shared.showLoadingHUD()
        LibraryAPI.shared.stakeholderBO.rateStakeholder(request: rateRequest, onSuccess: { (rate) in
            
            MessageManager.shared.hideHUD()
            self.didEndRating?()
            self.dismiss(animated: true, completion: nil)
            
        }) { (error) in
            
            MessageManager.shared.hideHUD()
        }
    }
    
    @IBAction func cancel(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
