//
//  RatingStarBar.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 8/9/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class RatingStarBar: UIView
{
    
    @IBOutlet var contentView: UIView!
    
    

    override init(frame: CGRect)
    {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        Bundle.main.loadNibNamed("RatingStarBar", owner: self, options: nil)
//        addSubview(self.contentView)
//        self.contentView.frame = self.bounds
//        self.contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        self.contentView.backgroundColor = .white
//        self.loadConfig()
    }

    fileprivate func loadConfig()
    {
        
    }
    
    override func draw(_ rect: CGRect)
    {
        let w = self.frame.width * 0.15
        var space = CGFloat(0.0)
        for i in 0...4
        {
            let img = UIImageView(frame: CGRect(x: space, y: 0, width: w, height: w))
            img.image = UIImage(named: "iconStar")
            space =  space + w + (w * 0.5)
            self.addSubview(img)
        }
    }

}
