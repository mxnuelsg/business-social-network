//
//  RatingViewController_iPad.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 8/15/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class RatingViewController_iPad: UIViewController
{
    var size = CGSize(width: 230,height: 50)
    var arrayStars = [UIImageView]()
    var stakeholder: Stakeholder? = Stakeholder(isDefault: true)
    var rating:Float = 0.0
    var didEndRating:(()->())?
    var didSelectRating:((Float)->())?
    var shouldSendRating = true
    
    class func present(inController controller: UIViewController, stakeholder: Stakeholder? = nil, sourceView: UIView, shouldSendRating: Bool = true, onSelectRating: ((Float)->())? = nil, onEndRating: (() -> ())? = nil) {
        let vcRating = RatingViewController_iPad(stakeholder: stakeholder, sourceView: sourceView, shouldSendRating: shouldSendRating, onSelectRating: onSelectRating, onEndRating: onEndRating)
        controller.present(vcRating, animated: true, completion: nil)
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    convenience init(stakeholder: Stakeholder?, sourceView: UIView, shouldSendRating: Bool = true, onSelectRating: ((Float)->())? = nil, onEndRating: (() -> ())? = nil) {
        self.init()
        modalPresentationStyle = .popover
        preferredContentSize  = CGSize(width: 280,height: 60)
        self.stakeholder = stakeholder
        let datePopover = popoverPresentationController
        datePopover?.permittedArrowDirections = .any
        datePopover?.sourceView = sourceView
        datePopover?.sourceRect = sourceView.bounds
        datePopover?.backgroundColor = .black
        didEndRating = onEndRating
        didSelectRating = onSelectRating
        self.shouldSendRating = shouldSendRating
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        
        let w = size.width * 0.17
        var space = CGFloat(5.0)
        for i in 0...4
        {
            let img = UIImageView(frame: CGRect(x: space, y: 8, width: w, height: w))
            img.image = UIImage(named: "iconStarGray")
            img.tag = i
            img.isUserInteractionEnabled = true
            space =  space + w + (w * 0.475)
            self.arrayStars.append(img)
            self.view.addSubview(img)
        }
        
        self.arrayStars.forEach({ (star) in
            star.isUserInteractionEnabled = true
            let tg = UITapGestureRecognizer(target: self, action: #selector(updateStarBar(sender:)))
            star.addGestureRecognizer(tg)
        })
    }
    
    @objc fileprivate func updateStarBar(sender: Any)
    {
        if let img = (sender as? UIGestureRecognizer)?.view {
            
            self.rating = Float(img.tag + 1)
            self.fillStars(selectedStar: img.tag)
            self.didSelectRating?(rating)
            if shouldSendRating {
                self.rate()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    fileprivate func fillStars(selectedStar: Int)
    {
        for i in 0..<(selectedStar + 1)
        {
            self.arrayStars[i].image = UIImage(named:"iconStar")
        }
        for i in (selectedStar + 1)..<(self.arrayStars.count)
        {
            self.arrayStars[i].image = UIImage(named:"iconStarGray")
        }
    }
    
    fileprivate func rate()
    {
        let rateRequest = RateRequest()
        rateRequest.givenRating = self.rating
        rateRequest.stakeholderId = self.stakeholder!.id
        MessageManager.shared.showLoadingHUD()
        LibraryAPI.shared.stakeholderBO.rateStakeholder(request: rateRequest, onSuccess: { (rate) in
            
            MessageManager.shared.hideHUD()
            self.didEndRating?()
            self.dismiss(animated: true, completion: nil)
            
        }) { (error) in
            
            MessageManager.shared.hideHUD()
        }
    }

}
