//
//  RequestUpdateViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 10/18/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class RequestUpdateViewController: UIViewController
{
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAnonymous: UILabel!
    
    @IBOutlet weak var btnUpdate: UIButton!
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var txtComments: UITextView!
    @IBOutlet weak var ctrlSwitch: UISwitch!
    var stakeholderId: Int = 0;
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func loadConfig()
    {
        self.lblTitle.text = "Request update?".localized()
        self.lblAnonymous.text = "Is anonymous request?".localized()
        self.btnCancel.setTitle("Cancel".localized(), for: .normal)
        self.btnUpdate.setTitle("Update".localized(), for: .normal)
        self.txtComments.delegate = self
        self.ctrlSwitch.isOn = false
        let gr = UITapGestureRecognizer(target: self, action: #selector(self.hidekeyboard))
        self.view.addGestureRecognizer(gr)
    }
    
    func hidekeyboard()
    {
        self.txtComments.resignFirstResponder()
    }

    @IBAction func UpdateRequest(_ sender: Any)
    {
        //Reques to Update
        MessageManager.shared.showLoadingHUD()
        LibraryAPI.shared.stakeholderBO.requestUpdate(
            stakeholderId: self.stakeholderId,
            comments: self.txtComments.text,
            isAnonymous: self.ctrlSwitch.isOn,
            onSuccess: { () -> () in
                MessageManager.shared.hideHUD()
                MessageManager.shared.showBar(title: "Success".localized(), subtitle: "Request was sent".localized(), type: .success, fromBottom: false)
                self.dismiss(animated: true, completion: nil)
                
        }, onError: { error in
            MessageManager.shared.hideHUD()
            MessageManager.shared.showBar(title: "Error", subtitle:"Request could not be sent at this time".localized() , type: .error, fromBottom: false)
        })
    }
    

    @IBAction func Cancel(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension RequestUpdateViewController: UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView)
    {
            textView.returnKeyType = .done
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n")
        {
            self.hidekeyboard()
            return false
        }
        
        return true
    }
}
