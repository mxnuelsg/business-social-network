//
//  ModalSquareViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 9/25/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ModalHalfViewController: UIViewController
{
    @IBOutlet weak var viewContainer: UIView!
    var subViewController: UIViewController
    
    init(subViewController: UIViewController)
    {
        self.subViewController = subViewController
        super.init(nibName: "ModalHalfViewController", bundle: nil)
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - LIFE CLYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        loadConfig()
        viewContainer.addSubViewController(subViewController, parentVC: self)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
    }
    
    // MARK: INITIAL CONFIG
    func loadConfig()
    {
        /**  Navigation Bar */
        let navbarFont = UIFont(name: "HelveticaNeue-Bold", size: 15) ?? UIFont.systemFont(ofSize: 15)
        
        navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: navbarFont, NSForegroundColorAttributeName: UIColor.white]
        
        /**  Gesture */
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action:#selector(self.close)))
        
        /**  Background */
        view.backgroundColor = UIColor.blackColor(0.7)

    }
    
    func close()
    {
        dismiss(animated: true, completion: nil)
    }


}
