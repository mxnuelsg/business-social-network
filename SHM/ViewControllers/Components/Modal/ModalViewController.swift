//
//  ModalActorsViewController.swift
//  SHM
//
//  Created by Manuel Salinas on 8/5/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ModalViewController: UIViewController
{
    //MARK: PROPERTIES & OUTLET
    @IBOutlet weak var viewContainer: UIView!
    
    var closeOnTapBackground = true
    var subViewController: UIViewController
    var onModalViewControllerClosed : (() -> ())?
    
    //MARK: LIFE CYCLE
    init(subViewController: UIViewController)
    {
        self.subViewController = subViewController
        super.init(nibName: "ModalViewController", bundle: nil)
        
        self.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        self.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
    }

    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.loadConfig()
        self.viewContainer.subviews[0].addSubViewController(subViewController, parentVC: self)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonArrow()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    deinit
    {
        print("Deinit: ModalViewController")
    }
    
    // MARK: INITIAL CONFIG
    func loadConfig()
    {
        //Navigation Bar
        let navbarFont = UIFont(name: "HelveticaNeue-Bold", size: 15) ?? UIFont.systemFont(ofSize: 15)
        self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: navbarFont, NSForegroundColorAttributeName: UIColor.white]
        
        //Blur Effect
        self.view.subviews[0].blur()
        
        if self.closeOnTapBackground == true
        {
            self.view.subviews[0].addGestureRecognizer(UITapGestureRecognizer(target: self, action:#selector(self.close)))
        }
        
        //Self and Container view
        self.viewContainer.layer.cornerRadius = 5
    }
    
    func close()
    {
        self.dismiss(animated: true) {
            
            self.onModalViewControllerClosed?()
        }
    }
}
