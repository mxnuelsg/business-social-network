//
//  LibraryAPI.swift
//  PDI
//
//  Created by Manuel Salinas Gonzalez on 3/25/15.
//  Copyright (c) 2015 PDI. All rights reserved.
//

import UIKit
import CoreLocation

class LibraryAPI
{
    var offlineModeEnabled = false
    var pushNotificationSilentObject: [AnyHashable: Any] = [:]
    var currentUser: User?
    
    lazy var stakeholderBO = StakeholderBO()
    lazy var feedBO = FeedBO()
    lazy var projectBO = ProjectBO()
    lazy var profileBO = ProfileBO()
    lazy var userBO = UserBO()
    lazy var attachmentBO = AttachmentBO()
    lazy var memberBO = MemberBO()
    lazy var inboxBO = InboxBO()
    lazy var searchBO = GlobalSearchBO()
    lazy var calendarBO = CalendarBO()
    lazy var pushNotificationsBO = PushNotificationsBO()
    lazy var reportsBO = ReportsBO()
    lazy var feedbackBO = FeedbackBO()
    lazy var requestBO = RequestBO()
    lazy var integrationBO = IntegrationBO()
    lazy var customLocale = CustomLocalize()
    lazy var geographyBO = GeographyBO()
    static let shared = LibraryAPI()
    
    func loadUser()
    {
        if let user = userBO.getUser() {
            
            currentUser = user
            
            if let cookie = user.sessionCookie {
                
                BO.SecurityHeaders = ["Cookie": cookie]
            }
        }
    }
    
    func logout()
    {
        currentUser!.isLoggedIn = false
        currentUser!.sessionCookie = nil
        userBO.save(currentUser!)
    }
}
