//
//  Constants.swift
//  SHM
//  Created by Manuel Salinas Gonzalez on 8/5/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

struct Constants
{
    static let ErrorDomain = "SHMErrorDomain"
    static let TestingDelayResponse = UInt64(0) //Seconds
    static let kMapsAPIKey = "AIzaSyAnC_ol0zhwhG0y-4F7mT_gOn8RdYRfT-Q"
    
    struct ImageKey
    {
        static let Following = "followingCheck"
        static let NotFollowing = "notFollowingCheck"
    }
    
    struct TimeInterval
    {
        static let SearchWritingDelay = 2.0 //0.4
        static let SessionAliveLimit = 1 // IN DAYS
    }
    
    struct Security
    {
        static let SecurityIV = "II+ILIZBxdpWZa2L/4WIBA=="
        static let SecurityKey = "SOf0qJsKsV2rF7QcV1IIDRyyd76pWClGAuo1AwGW/Ds="
    }
    
    struct ErrorCode
    {
        static let TransportDataError = 1
        static let ParsingDataError = 2
        static let IntegrityDataError = 3
    }
    
    struct RemoteService
    {
        //Security
        static let Login = "authentication/login"
        
        //Settings
        static let SaveSettings = "api/settings/put"
        static let GetSettings = "api/settings/get"
        
        //Posts
        static let GetFeed = "api/post/getlist"
        static let GetPostDetail = "api/post/GetById"
        static let GetPreviousFeed = "api/post/getpreviouslist"
        static let NewPost = "api/post/post"
        static let GetFeedEditingStakeholder = "api/post/getfiltered"
        static let FiledPost = "api/post/Filed"
        
        //
        static let GetMembers = "api/stakeholder/SearchMember"
        static let GetAttachments = "api/attachment/getList"
        static let GlobalSearch = "api/home/getglobalsearch"
        
        //Inbox
        static let GetInbox = "api/Message/GetMessages"
        static let GetInboxRelated = "api/Message/GetConversation"
        static let InboxMarkAsUnRead = "api/Message/MarkAsUnread"
        static let GetInboxDetail = "api/Message/Detail"
        static let DeleteInboxMessage = "api/Message/Delete"
        static let GetRecipients = "api/Message/GetRecipients"
        static let NewMessage = "api/Message/New"
        static let ReplyMessage = "api/Message/Reply"
        static let GetUnreadInbox = "api/Message/getunreadcount"
        static let GetExpirationDays = "api/Message/GetExpirationDays"
        
        //Stakeholders
        static let GetAllStakeholders = "api/Stakeholder/getAll"
        static let GetStakeholderList = "api/Stakeholder/getList"
        static let GetStakeholderLight = "api/Stakeholder/getLightList"
        static let GetActorDetail = "api/Stakeholder/getDetail"
        static let FollowStakeholder = "api/stakeholder/Follow"
        static let UnfollowStakeholder = "api/stakeholder/Unfollow"
        static let PostNewStakeholder = "api/stakeholder/Post"
        static let GetStakeholderDefaultData = "api/Stakeholder/GetNewStakeholderAddData"
        static let StakeholderRequestUpdate = "api/Stakeholder/RequestUpdate"
        static let EnableStakeholder = "api/stakeholder/Enable"
        static let DisableStakeholder = "api/stakeholder/Disable"
        static let PostCreateRequestForEdit = "api/stakeholder/GetOrCreateRequest"
        static let GetStakeholderEdit = "api/stakeholder/GetEditInfo"
        static let GetUsersToRelate = "api/stakeholder/GetUsersToRelate"
        static let PutSaveStakeholderEdited = "api/stakeholder/Put"
        static let PostUploadProfilePicture = "api/stakeholder/UploadThumbnail"
        static let rateStakeholder = "api/Stakeholder/Rate/"
        
        static let StakeholderDetailLight = "api/stakeholder/GetDetailLight"
        static let StakeholderProjects = "api/stakeholder/GetProjects"
        static let StakeholderGeographies = "api/stakeholder/GetGeographies"
        static let StakeholderRelations = "api/stakeholder/GetRelations"
        //Project
        static let GetProjectList = "api/Project/getList"
        static let GetProjectsSearch = "api/Project/getList"
        static let GetProjectsLight = "api/Project/GetListLight"
        static let GetProjectsLightComplete = "api/Project/GetListLightComplete"
        static let GetProjectDetail = "api/Project/GetDetail"
        static let PostNewProject = "api/Project/Post"
        static let PutEditProject = "api/Project/Put"
        static let FollowProject = "api/Project/Follow"
        static let UnfollowProject = "api/Project/Unfollow"
        static let GetCurrentUser = "api/project/GetCurrentUser"
        static let EnableDisableProject = "api/project/ToggleEnable"
        
        static let GetProjectDetailLight = "api/project/GetDetailLight"
        static let GetStakeholdersByProject = "api/Project/GetStakeholdersByProject"
        //Member
        static let GetMemberDetail = "api/member/GetDetail"
        static let PutEditMember = "api/member/put"
        static let PutUploadPhoto = "api/member/UploadThumbnail"
        
        //Calendar
        static let GetCalendar = "api/Calendar/GetDataMobile"
        static let GetCalendarById = "api/Calendar/GetDataMobileById"
        static let GetCalendarEvents = "api/Calendar/GetEventsMobile"
        static let GetCalendarAllEvents = "api/Calendar/GetAllEventsMobile"
        static let GetGeoCalendarEvents = "api/Calendar/GetEvents"
        
        //Attachment
        static let PostAttachmentUpload = "api/Attachment/Upload"
        
        //Push Notifications
        static let PostRegisterDevice = "api/PushNotificationRegister/RegisterDevice"
        static let PostUnregisterDevice = "api/PushNotificationRegister/UnregisterDevice"
        static let PostUnregisterDeviceFromFeedbackService = "api/PushNotificationRegister/UnregisterDeviceFromFeedbackService"
        static let PostResetBadgeIcon = "api/PushNotificationRegister/ResetBatch"
        
        //Reports
        static let ReportGetRelations = "api/report/GetRelations"
        static let ReportGetRelatedUsers = "api/report/GetRelatedUsers"
        static let ReportGetPostFilters = "api/report/GetPostFilters"
        static let ReportGetPostList = "api/report/GetPostList"
        static let ReportConexionsGetAccounts = "api/report/GetAccounts"
        static let ReportConexionsGetStakeholders = "api/report/GetStakeholders"
        static let ReportConexionsGetStakeholderConnections = "api/report/GetStakeholderConnections"
        static let ReportConexionsGetNodeInfo = "api/report/GetNodeInfo"
        static let ReportConexionsGetAccount = "api/report/GetAccount"
        
        //Feedback
        static let PostFeedback = "api/feedback/PostFeedback"
        
        //Requests
        static let GetRequest = "api/request/get"
        static let PostRequestPublish = "api/request/publish"
        
        //Translation
        static let getTranslationsiOS = "api/Attachment/GetTranslationsiOS"
        
        //Integration
        static let postMitigations = "api/public/GetMitigationMeasures"
        static let getWhos = "api/public/GetWhos"
        
        //Geography
        static let getFollowedGeographies = "api/geography/GetFollowingList"
        static let getFollowedGeographiesLight = "/api/Geography/GetFollowingListLight"
        static let getGeographiesLight = "api/geography/GetAllLight"
        static let getGeographiesByProject = "api/project/GetGeographiesByProject"
        static let getAvailableGeographies = "api/Geography/GetAvailableGeographies"
        static let followGeography = "api/geography/Follow"
        static let unfollowGeography = "api/geography/unfollow"
        static let getGeographyDetail = "api/Geography/GetDetail"
        static let getProjectsByGeoId = "api/Geography/GetProjectsByGeography"
        static let getStakeholdersByGeoId = "api/Geography/GetStakeholdersByGeography"
        static let getPostsByGeoId = "api/Geography/GetPostsByGeography"
        static let getEventsByGeoId = "api/Geography/GetEventsByGeography"
    }
    
    struct UserKey
    {
        static let IsLoggedIn = "IsLoggedIn"
        static let UserName = "UserName"
        static let Password = "Password"
        static let Cookie = "Cookie"
        static let U = "U"
        static let P = "P"
        static let DeviceToken = "DeviceToken"
        static let LastLogin = "LastLogin"
        static let Role = "Role"
        static let AId = "AId"
        static let UrlVersion = "UrlToDownloadVersion"
        static let AuthToken = "requestverificationtoken"
        
        static let NotificationOnCardUpdate = "NotificationOnCardUpdate"
        static let NotificationOnProjectEdit = "NotificationOnProjectEdit"
        static let NotificationOnStakeholderEdit = "NotificationOnStakeholderEdit"
        static let NotificationOnNewPost = "NotificationOnNewPost"
        static let TranslationsVersionEn = "TranslationsVersionEn"
        static let TranslationsVersionEs = "TranslationsVersionEs"
        static let BackupLanguage = "BackupLanguage"
    }
    
    struct FontSize
    {
        static let PostText: CGFloat = 14
        static let PostSubtext: CGFloat = 11
    }
}
