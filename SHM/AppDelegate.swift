//
//  AppDelegate.swift
//  SHM
//
//  Created by Manuel Salinas on 7/24/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit
import GoogleMaps

/*
 //Push Notification Structure
 
 {
  "aps": {
     "alert": "Hello World",
     "sound": "default",
     "badge": 100
    },
  "Type": 3,
  "Id": 516469
 }
*/

enum PushNotificationType: Int
{
    case publishCard = 1
    case projectEdited = 2
    case createdPost = 3
    case stakeholderEdited = 4
    case inbox = 5
    case localManagerMail = 6
}

enum UrlSchemeType: String
{
    case StakeholderDetail = "shd"
    case ProjectDetail = "pjd"
    case PostDetail = "pd"
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, SWRevealViewControllerDelegate
{
    var window: UIWindow?
    var ermServer: ERMServer!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        /** Menu */
        let revealMenu = self.window?.rootViewController as! SWRevealViewController
        revealMenu.delegate = self
        
        // USER CREATION OR LOAD SESSION
        LibraryAPI.shared.loadUser()
        
        //Reset Badge icon
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        //When the app was launched
        if launchOptions != nil
        {
            if let options = launchOptions![UIApplicationLaunchOptionsKey.remoteNotification] as? NSDictionary {
                
                LibraryAPI.shared.pushNotificationSilentObject = options as! [AnyHashable : Any] as [AnyHashable: Any] as [NSObject : AnyObject]
            }
        }
        
        /** APPEARANCE PROXY */
        if DeviceType.IS_ANY_IPAD == true
        {
            UISearchBar.appearance().barTintColor = UIColor.groupTableViewBackground
        }
        else
        {
            UISearchBar.appearance().barTintColor = UIColor.colorForNavigationController()
        }
        
        if DeviceType.IS_ANY_IPAD == true
        {
            UISearchBar.appearance(whenContainedInInstancesOf: [UITableViewCell.self]).barTintColor = UIColor.blueSearchBar()
            UIBarButtonItem.appearance(whenContainedInInstancesOf: [UITableViewCell.self]).tintColor = UIColor.white
            UIBarButtonItem.appearance(whenContainedInInstancesOf: [UITableViewCell.self]).tintColor = UIColor.white
        }
        else
        {
            UISearchBar.appearance(whenContainedInInstancesOf: [MenuTableViewController.self]).barTintColor = UIColor.blueSearchBar()
            UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = UIColor.white
            UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).tintColor = UIColor.white
        }
        
        //** UINavigationController Appearance*
        if DeviceType.IS_ANY_IPAD == false
        {
            UINavigationBar.appearance().barTintColor = UIColor.colorForNavigationController()
            UINavigationBar.appearance().isTranslucent  = false
            UINavigationBar.appearance().tintColor    = UIColor.white
            UINavigationBar.appearance().titleTextAttributes = [
                NSFontAttributeName: UIFont.systemFont(ofSize: 15),
                NSForegroundColorAttributeName: UIColor.white
            ]
        }
        
        //Status Bar
        UIApplication.shared.statusBarStyle = .lightContent
        
        //** UITableView Appearance *
        UITableView.appearance().backgroundColor = UIColor.white
        UIRefreshControl.appearance().backgroundColor = UIColor.grayTableBackground()
        UIRefreshControl.appearance().tintColor = UIColor.darkText
        
        //Server Singleton
        self.ermServer = ERMServer()
        
        //Servers
        UserDefaultsManager.createServerList()
        Servers.shared.update(UserDefaultsManager.getServerList())
        
        UserDefaultsManager.createDefaultServer()
        Servers.shared.update( UserDefaultsManager.getDefaultServer(),
                               list: UserDefaultsManager.getServerList())

        
        //Languages
        UserDefaultsManager.checkLanguageStatus()
        
        //Google Maps
        if Constants.kMapsAPIKey.isEmpty == true
        {
            fatalError("Please provide a Google Maps API Key using kMapsAPIKey")
        }
        
        GMSServices.provideAPIKey(Constants.kMapsAPIKey)

        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication)
    {
        //Reset Badge icon
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication)
    {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication)
    {
        //Reset Badge icon
        UIApplication.shared.applicationIconBadgeNumber = 0
        LibraryAPI.shared.pushNotificationsBO.resetBadgeIcon()
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication)
    {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    //MARK: URL MANAGMENT
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool
    {
        print(url.absoluteString)
        
        let arrayUrl = url.absoluteString.components(separatedBy: "&")
        
        if arrayUrl.count == 3
        {
            if let id = Int(arrayUrl[2]) {
                
                let type = arrayUrl[1]
                
                switch type
                {
                case UrlSchemeType.StakeholderDetail.rawValue:
                    let stakeholderEdited = Stakeholder(isDefault: true)
                    stakeholderEdited.id = id
                    
                    if DeviceType.IS_ANY_IPAD == true
                    {
                        //iPad
                        let vcStakeholderDetail = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
                        vcStakeholderDetail.stakeholder = stakeholderEdited
                        
                        let root = self.window!.rootViewController as! SWRevealViewController
                        let navHome = root.frontViewController as! NavyController
                        let vcHome = navHome.viewControllers.first as! HomeViewController
                        
                        vcHome.navigationController?.pushViewController(vcStakeholderDetail, animated: true)
                    }
                    else
                    {
                        //iPhone
                        let vcStakeholderDetail : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                        vcStakeholderDetail.stakeholder = stakeholderEdited
                        
                        let root = self.window!.rootViewController as! SWRevealViewController
                        let navHome = root.frontViewController as! NavyController
                        let vcHome = navHome.viewControllers.first as! HomeViewController
                        
                        vcHome.navigationController?.pushViewController(vcStakeholderDetail, animated: true)
                    }
                case UrlSchemeType.ProjectDetail.rawValue:
                    let projectEdited = Project(isDefault: true)
                    projectEdited.id = id
                    
                    if DeviceType.IS_ANY_IPAD == true
                    {
                        //iPad
                        let vcProjectDetail = Storyboard.getInstanceOf(ProjectDetailViewController_iPad.self)
                        vcProjectDetail.project = projectEdited
                        
                        let root = self.window!.rootViewController as! SWRevealViewController
                        let navHome = root.frontViewController as! NavyController
                        let vcHome = navHome.viewControllers.first as! HomeViewController
                        
                        vcHome.navigationController?.pushViewController(vcProjectDetail, animated: true)
                    }
                    else
                    {
                        //iPhone
                        let vcProjectDetail : DetailProjectViewController = Storyboard.getInstanceFromStoryboard("Projects")
                        vcProjectDetail.project = projectEdited
                        
                        let root = self.window!.rootViewController as! SWRevealViewController
                        let navHome = root.frontViewController as! NavyController
                        let vcHome = navHome.viewControllers.first as! HomeViewController
                        
                        vcHome.navigationController?.pushViewController(vcProjectDetail, animated: true)
                    }
                case UrlSchemeType.PostDetail.rawValue:
                    let post = Post()
                    post.id = id
                    
                    if DeviceType.IS_ANY_IPAD == true
                    {
                        //iPad
                        let vcPostDetail = Storyboard.getInstanceOf(PostDetailViewController_iPad.self)
                        vcPostDetail.post = post
                        
                        let root = self.window!.rootViewController as! SWRevealViewController
                        let navHome = root.frontViewController as! NavyController
                        let vcHome = navHome.viewControllers.first as! HomeViewController
                        
                        vcHome.navigationController?.pushViewController(vcPostDetail, animated: true)
                    }
                    else
                    {
                        //iPhone
                        let vcPostDetail = Storyboard.getInstanceOf(PostDetailViewController.self)
                        vcPostDetail.post = post
                        
                        let root = self.window!.rootViewController as! SWRevealViewController
                        let navHome = root.frontViewController as! NavyController
                        let vcHome = navHome.viewControllers.first as! HomeViewController
                        
                        vcHome.navigationController?.pushViewController(vcPostDetail, animated: true)
                    }
                default:
                    let alert = UIAlertController(title:"Info".localized(),
                                                  message:"The type of detail received is not valid".localized(),
                                                  preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Close".localized(), style: .cancel, handler:nil))
                    self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                }
            }
            else
            {
                let alert = UIAlertController(title:"Info".localized(),
                                              message:"The id received is not valid".localized(), preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title:"Close".localized(), style: .cancel, handler:nil))
                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            }
        }
        else
        {
//            let alert = UIAlertController(title:"Info".localized(),
//                                          message:"The link received is incomplete or in a wrong format".localized(),
//                                          preferredStyle: .alert)
//            
//            alert.addAction(UIAlertAction(title: "Close".localized(), style: .cancel, handler:nil))
//            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
        
        return true
    }
    
    
    // MARK: - DELEGATE METHODS (SWRevealViewControllerDelegate)
    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition)
    {
        let navHome = revealController.frontViewController as! NavyController
        let vcHome = navHome.viewControllers.first as! HomeViewController
        
        guard position == FrontViewPosition.right else
        {
            let _ = vcHome.view.subviews.map({
                $0.isUserInteractionEnabled = true
            })
            return
        }
        
        if position == FrontViewPosition.right
        {
            let _ = vcHome.view.subviews.map({
                $0.isUserInteractionEnabled = false
            })
        }
        
        revealController.frontViewController.revealViewController().panGestureRecognizer()
        revealController.frontViewController.revealViewController().tapGestureRecognizer()
    }
    
    // MARK: - PUSH NOTIFICATIONS
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        var deviceTokenString = deviceToken.description.trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
        deviceTokenString = deviceTokenString.replacingOccurrences(of: " ", with: "")
        
        //Save Token
        LibraryAPI.shared.currentUser!.deviceToken = deviceTokenString
        LibraryAPI.shared.userBO.save(LibraryAPI.shared.currentUser!)
        
        //Send token to subscribe
        LibraryAPI.shared.pushNotificationsBO.registerDevice(deviceTokenString, onSuccess: {
            () -> () in
            
            print("deviceToken subscribed")
            
            }) { error in
                
                print("ERROR Trying to subscribe deviceToken: '\(error)'")
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        MessageManager.shared.showBar(title: "Error",
            subtitle: error.localizedDescription,
            type: .error,
            fromBottom: false)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        //Reset Badge icon
        let state = application.applicationState
        LibraryAPI.shared.pushNotificationsBO.resetBadgeIcon()
        
        print("Did received remote notification '\(userInfo)'")
        
        if let id = userInfo["Id"] as! Int!, let type = userInfo["Type"] as! Int!
        {
            let bodyMessage = (userInfo["aps"] as! [AnyHashable: Any])["alert"] as! String
            
            //User should be logged
            guard LibraryAPI.shared.currentUser?.isLoggedIn == true else {
                
                return
            }
            
            //Update badge
            let badge = (userInfo["aps"] as! [AnyHashable: Any])["badge"] as! Int
            LibraryAPI.shared.currentUser?.unReadInbox = badge
            NotificationCenter.default.post(name: Notification.Name(rawValue: "inboxReceived"), object: nil)
            
            //Selecting Type
            switch type
            {
            case PushNotificationType.publishCard.rawValue, PushNotificationType.stakeholderEdited.rawValue:
                
                let stakeholderEdited = Stakeholder(isDefault: true)
                stakeholderEdited.id = id
                
                //Workflow iPad / iPhone
                if DeviceType.IS_ANY_IPAD == true
                {
                    //iPad
                    let vcStakeholderDetail = Storyboard.getInstanceOf(StakeholderDetailViewController_iPad.self)
                    vcStakeholderDetail.stakeholder = stakeholderEdited
                    
                    if state == .inactive || state == .background
                    {
                        let root = self.window!.rootViewController as! SWRevealViewController
                        let navHome = root.frontViewController as! NavyController
                        let vcHome = navHome.viewControllers.first as! HomeViewController
                        
                        vcHome.navigationController?.pushViewController(vcStakeholderDetail, animated: true)
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Notification".localized(), message:bodyMessage, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Close".localized(), style: .cancel, handler:nil))
                        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    //iPhone
                    let vcStakeholderDetail : SHDetailViewController =  Storyboard.getInstanceFromStoryboard("Stakeholder");
                    vcStakeholderDetail.stakeholder = stakeholderEdited
                    
                    if state == .inactive || state == .background
                    {
                        let root = self.window!.rootViewController as! SWRevealViewController
                        let navHome = root.frontViewController as! NavyController
                        let vcHome = navHome.viewControllers.first as! HomeViewController
                        
                        vcHome.navigationController?.pushViewController(vcStakeholderDetail, animated: true)
                        
                    }
                    else
                    {
                        let alert = UIAlertController(title:"Notification".localized(), message:bodyMessage, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Close".localized(), style: .cancel, handler:nil))
                        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                    }
                }
            case PushNotificationType.projectEdited.rawValue:
                
                let projectEdited = Project(isDefault: true)
                projectEdited.id = id
                
                //Workflow iPad / iPhone
                if DeviceType.IS_ANY_IPAD == true
                {
                    //iPad
                    let vcProjectDetail = Storyboard.getInstanceOf(ProjectDetailViewController_iPad.self)
                    vcProjectDetail.project = projectEdited
                    
                    if state == .inactive || state == .background
                    {
                        let root = self.window!.rootViewController as! SWRevealViewController
                        let navHome = root.frontViewController as! NavyController
                        let vcHome = navHome.viewControllers.first as! HomeViewController
                        
                        vcHome.navigationController?.pushViewController(vcProjectDetail, animated: true)
                    }
                    else
                    {
                        let alert = UIAlertController(title:"Notification".localized(), message:bodyMessage, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Close".localized(), style: .cancel, handler:nil))
                        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    //iPhone
                    let vcProjectDetail : DetailProjectViewController = Storyboard.getInstanceFromStoryboard("Projects")
                    vcProjectDetail.project = projectEdited
                    
                    if state == .inactive || state == .background
                    {
                        let root = self.window!.rootViewController as! SWRevealViewController
                        let navHome = root.frontViewController as! NavyController
                        let vcHome = navHome.viewControllers.first as! HomeViewController
                        
                        vcHome.navigationController?.pushViewController(vcProjectDetail, animated: true)
                    }
                    else
                    {
                        let alert = UIAlertController(title:"Notification".localized(), message:bodyMessage, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Close".localized(), style: .cancel, handler:nil))
                        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                    }
                }
            case PushNotificationType.inbox.rawValue:
                
                let inbox = InboxMessage()
                inbox.id = id
                
                /*
                var inboxUnread = 0
                
                if let totalUnreadInbox = userInfo["TotalUnreadInbox"] {
                    
                    inboxUnread = totalUnreadInbox as! Int
                }
                */
                
                //Workflow iPad / iPhone
                if DeviceType.IS_ANY_IPAD == true
                {
                    //iPad
                    let vcInboxDetail = Storyboard.getInstanceOf(InboxDetailTableViewController_iPad.self)
                    vcInboxDetail.message = inbox
                    
                    if state == .inactive || state == .background
                    {
                        let root = self.window!.rootViewController as! SWRevealViewController
                        let navHome = root.frontViewController as! NavyController
                        let vcHome = navHome.viewControllers.first as! HomeViewController
                        
                        vcHome.navigationController?.pushViewController(vcInboxDetail, animated: true)
                    }
                    else
                    {
                        let alert = UIAlertController(title:"Notification".localized(), message:bodyMessage, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title:"Close".localized(), style: .cancel, handler:nil))
                        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    //iPhone
                    let vcInboxDetail = Storyboard.getInstanceOf(InboxDetailTableViewController.self)
                    vcInboxDetail.message = inbox
                    
                    if state == .inactive || state == .background
                    {
                        let root = self.window!.rootViewController as! SWRevealViewController
                        let navHome = root.frontViewController as! NavyController
                        let vcHome = navHome.viewControllers.first as! HomeViewController
                        
                        vcHome.navigationController?.pushViewController(vcInboxDetail, animated: true)
                    }
                    else
                    {
                        let alert = UIAlertController(title:"Notification".localized(), message:bodyMessage, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title:"Close".localized(), style: .cancel, handler:nil))
                        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                        
                    }
                }
            case PushNotificationType.localManagerMail.rawValue:
                
                let alert = UIAlertController(title:"Notification".localized(), message:bodyMessage, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title:"Close".localized(), style: .cancel, handler:nil))
                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                
            default:
                //Post created (type 3)
                let post = Post()
                post.id = id
                
                //Workflow iPad / iPhone
                if DeviceType.IS_ANY_IPAD == true
                {
                    //iPad
                    let vcPostDetail = Storyboard.getInstanceOf(PostDetailViewController_iPad.self)
                    vcPostDetail.post = post
                    
                    if state == .inactive || state == .background
                    {
                        let root = self.window!.rootViewController as! SWRevealViewController
                        let navHome = root.frontViewController as! NavyController
                        let vcHome = navHome.viewControllers.first as! HomeViewController
                        
                        navHome.viewControllers.removeLast(navHome.viewControllers.count - 1)
                        vcHome.navigationController?.pushViewController(vcPostDetail, animated: true)
                    }
                    else
                    {
                        let alert = UIAlertController(title:"Notification".localized(), message:bodyMessage, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title:"Close".localized(), style: .cancel, handler:nil))
                        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    //iPhone
                    let vcPostDetail = Storyboard.getInstanceOf(PostDetailViewController.self)
                    vcPostDetail.post = post
                    
                    if state == .inactive || state == .background
                    {
                        let root = self.window!.rootViewController as! SWRevealViewController
                        let navHome = root.frontViewController as! NavyController
                        let vcHome = navHome.viewControllers.first as! HomeViewController
                        vcHome.navigationController?.pushViewController(vcPostDetail, animated: true)
                    }
                    else
                    {
                        let alert = UIAlertController(title:"Notification".localized(), message:bodyMessage, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Close".localized(), style: .cancel, handler:nil))
                        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                    }
                }
                break
            }
        }
    }
}

