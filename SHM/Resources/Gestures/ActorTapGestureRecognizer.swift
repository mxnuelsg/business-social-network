//
//  ActorTapGestureRecognizer.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 7/31/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ActorTapGestureRecognizer: UITapGestureRecognizer
{
    var tapper: ActorTapper
    
    init(actor: Stakeholder, onTap: @escaping (_ actor: Stakeholder) -> Void)
    {
        tapper = ActorTapper(actor: actor, onTap: onTap)
        super.init(target: tapper, action: #selector(ActorTapper.actorTapped))
    }
}

class ActorTapper : NSObject
{
    var actor: Stakeholder
    var onTap: ((_ actor: Stakeholder) -> Void)?
    
    init(actor: Stakeholder, onTap: @escaping (_ actor: Stakeholder) -> Void)
    {
        self.actor = actor
        self.onTap = onTap
    }
    
    func actorTapped()
    {
        onTap!(actor)
    }
}
