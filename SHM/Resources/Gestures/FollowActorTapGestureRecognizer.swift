//
//  FollowActorTapGestureRecognizer.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/5/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class FollowStakeholderTapGestureRecognizer: UITapGestureRecognizer {
    var tapAction: FollowStakeholderTapper
    
    init(stakeholder: Stakeholder, onTap: @escaping (_ stakeholder: Stakeholder) -> Void)
    {
        self.tapAction = FollowStakeholderTapper(stakeholder: stakeholder, onTap: onTap)
        super.init(target: self.tapAction, action: #selector(FollowStakeholderTapper.launchActionOnTap))
    }
}

class FollowStakeholderTapper : NSObject
{
    var stakeholder: Stakeholder
    var onTap: ((_ stakeholder: Stakeholder) -> Void)
    
    init(stakeholder: Stakeholder, onTap: @escaping ((_ stakeholder: Stakeholder) -> Void)) {
        self.stakeholder = stakeholder
        self.onTap = onTap
    }
    
    func launchActionOnTap()
    {
        onTap(stakeholder)
    }
}
