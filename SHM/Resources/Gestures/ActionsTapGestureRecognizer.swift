//
//  TapGestures.swift
//  SHM
//
//  Created by Manuel Salinas on 8/5/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ActionsTapGestureRecognizer: UITapGestureRecognizer
{
    var tapAction: ActionTap
    
    init(onTap:@escaping () -> ())
    {
        self.tapAction = ActionTap()
        self.tapAction.onTap = onTap
        super.init(target: self.tapAction, action: #selector(ActionTap.launchActionOnTap))
    }
}

class ActionTap : NSObject
{
    var onTap: (() -> ())?
    
    func launchActionOnTap()
    {
        onTap!()
    }
}

