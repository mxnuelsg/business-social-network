//
//  SHMScrollView.swift
//  SHM
//
//  Created by Manuel Salinas on 8/25/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

private var xoAssociationKey: UInt8 = 0

extension UIScrollView
{
    //MARK: REFRESHER CUSTOM CONTROL
    var refresher: ActionRefresher! {
        get {
            return objc_getAssociatedObject(self, &xoAssociationKey) as? ActionRefresher
        }
        set(newValue) {
            objc_setAssociatedObject(self, &xoAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func enableRefreshAction(_ onRefresh: @escaping () -> ())
    {
        self.refresher = ActionRefresher(onRefresh: onRefresh)
        self.refresher.refreshControl = UIRefreshControl()
        self.refresher.refreshControl?.addTarget(refresher, action: #selector(self.refresher.refresh), for: .valueChanged)
        self.refresher.refreshControl?.setLastUpdate()
        
        self.refreshControl = self.refresher.refreshControl
    }
    
    func refreshed()
    {
        self.refresher.refreshControl?.endRefreshing()
        self.refresher.refreshControl?.setLastUpdate()
    }
    
    //MARK: Go to Scroll's Top
    func scrollToTop()
    {
        self.scrollRectToVisible(CGRect(x: 0, y: 0, width: self.bounds.size.width, height: 44), animated: true)
    }
    
    //MARK: Go to Scroll's Bottom
    func scrollToBottom()
    {
        let bottomOffset: CGPoint! = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height)
        self.setContentOffset(bottomOffset, animated: true)
    }

}
