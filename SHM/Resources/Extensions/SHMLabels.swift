//
//  SHMLabels.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/18/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

extension UILabel {
    func setHTMLFromAttributedString(_ text: NSAttributedString) {
        var oldAttsRange = NSMakeRange(0, text.length)
        let oldAtts = text.attributes(at: 0, effectiveRange: &oldAttsRange)
        
        let modifiedFont = NSString(format:"<span style=\"font-family:Helvetica Neue; font-size:15\">%@</span>", text.string) as String
        
        let attrStr = try! NSMutableAttributedString(data: modifiedFont.data(using: String.Encoding.unicode)!,
            options: [
                NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                NSCharacterEncodingDocumentAttribute: String.Encoding.utf8
            ],
            documentAttributes: nil)
        
        attrStr.addAttributes(oldAtts, range: NSMakeRange(0, attrStr.length))
        
        self.attributedText = attrStr
    }
    
    func setHTMLFromString(_ text: String?, color: UIColor) {
        guard let htmlText = text else { return }
        let modifiedFont = NSString(format:"<span style=\"font-family:Helvetica Neue; font-size:16\">%@</span>", htmlText) as String
        
        let attrStr = try! NSAttributedString(data: modifiedFont.data(using: String.Encoding.unicode)!,
            options: [
                NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                NSCharacterEncodingDocumentAttribute: String.Encoding.utf8,
                NSForegroundColorAttributeName: color
            ],
            documentAttributes: nil)
        
        self.attributedText = attrStr
    }
    
    func setHTMLFromString(_ text: String?) {
        self.setHTMLFromString(text, color: UIColor.black)
    }
}
