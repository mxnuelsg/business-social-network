//
//  SHMStrings.swift
//  SHM
//
//  Created by Manuel Salinas on 7/30/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import Foundation
import UIKit
//import CryptoSwift

extension NSAttributedString
{
    class func getAttributedMainTitle(_ title: String, subtitle: String) -> NSAttributedString
    {
        let titleAttrs = [NSFontAttributeName : UIFont.systemFont(ofSize: 17),
            NSForegroundColorAttributeName: UIColor.white]
        let subtitleAttrs = [NSFontAttributeName : UIFont.systemFont(ofSize: 14),
            NSForegroundColorAttributeName: UIColor.white]
        
        let attributedTitle = NSMutableAttributedString(string: title, attributes: titleAttrs)
        let attributedSubtitle = NSMutableAttributedString(string: subtitle, attributes: subtitleAttrs)
        
        let resultingString = NSMutableAttributedString()
        resultingString.append(attributedTitle)
        resultingString.append(NSAttributedString(string: "\n"))
        resultingString.append(attributedSubtitle)
        
        return resultingString
    }
    
    class func getAttributedSecondaryTitle(_ title: String, subtitle: String?, fontSize: CGFloat) -> NSAttributedString
    {
        let titleAttrs = [NSFontAttributeName : UIFont.systemFont(ofSize: fontSize),
            NSForegroundColorAttributeName: UIColor.colorDetailTitleLabel()]
        let subtitleAttrs = [NSFontAttributeName : UIFont.systemFont(ofSize: fontSize - 2),
            NSForegroundColorAttributeName: UIColor.blueGeneralText()]
        
        let resultingString = NSMutableAttributedString()
        
        let attributedTitle = NSMutableAttributedString(string: title, attributes: titleAttrs)
        resultingString.append(attributedTitle)
        
        if let subtitle = subtitle {
            resultingString.append(NSAttributedString(string: "\n"))
            
            let attributedSubtitle = NSMutableAttributedString(string: subtitle, attributes: subtitleAttrs)
            resultingString.append(attributedSubtitle)
        }
        
        return resultingString
    }
    
    
    func setJobPositionCompany(_ position: String, company: String) -> NSMutableAttributedString
    {
        if position != "" && company != ""
        {
            let attJobPositionCompany = NSMutableAttributedString(string: "\(position) / \(company)")
            attJobPositionCompany.setColor(position, color: UIColor.black)
            attJobPositionCompany.setColor(("/ "+company), color: UIColor.grayConcreto())
            
            return attJobPositionCompany
        }
        else
        {
            var strJobInformation = position != "" ? position : ""
            strJobInformation += company != "" ? company : ""
            let attJobInformation = NSMutableAttributedString (string: strJobInformation)
            attJobInformation.setColor(strJobInformation, color: UIColor.blackAsfalto())
            attJobInformation.setBold(strJobInformation, size: 11)
            
            return attJobInformation
        }
    }
    
    func setJobPositionDepartment(_ position: String, deparment: String) -> NSMutableAttributedString
    {
        if position != "" && deparment != ""
        {
            let attJobPositionCompany = NSMutableAttributedString(string: "\(position) / \(deparment)")
            attJobPositionCompany.setColor(position, color: UIColor.white)
            attJobPositionCompany.setColor(("/ "+deparment), color: UIColor.white)
            
            return attJobPositionCompany
        }
        else
        {
            var strJobInformation = position != "" ? position : ""
            strJobInformation += deparment != "" ? deparment : ""
            let attJobInformation = NSMutableAttributedString (string: strJobInformation)
            attJobInformation.setColor(strJobInformation, color: UIColor.white)
            attJobInformation.setBold(strJobInformation, size: 11)
            
            return attJobInformation
        }
    }
}

extension NSMutableAttributedString
{
    func addPostAttributes()
    {
        self.addAttributes([
            NSFontAttributeName : UIFont.systemFont(ofSize: Constants.FontSize.PostText),
//            NSForegroundColorAttributeName : UIColor.blueGeneralText()
            ],
            range: NSMakeRange(0, self.length))
    }
    
    ///Set Stakeholders, Dates and Projects as link
    @discardableResult func setLinks(_ post: Post) -> Bool
    {
        self.addPostAttributes()
        
        /*
        p_ = Projects
        s_ = Stakeholders
        d_ = Dates
        g_ = Geographies
        */
        
        //Stakeholders
        for stakeholder in post.stakeholders
        {
            let foundRange = self.mutableString.range(of: stakeholder.fullName)
            let arrayFullname = stakeholder.name.components(separatedBy: " ")
            let firstName: String = arrayFullname.count > 0 ? arrayFullname[0] : stakeholder.name
            
            let url = "s_\(stakeholder.id)_\(stakeholder.isEnable == true ? 1 : 0)_\(firstName)"
            
            if foundRange.location != NSNotFound
            {
                self.addAttribute(NSLinkAttributeName, value: url, range: foundRange)
                self.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: Constants.FontSize.PostText), range: foundRange)
                self.addAttribute(NSForegroundColorAttributeName, value: UIColor.blueTag(), range: foundRange)
            }
        }
        
        //Dates
        for date in post.events
        {
            let dateString: String! = (date.allDay) ? date.date.getDateStringForPost() : "\(date.date.getDateTimeStringForPost())"

            let foundRange = self.mutableString.range(of: dateString)
            let url = "d_\(date.id)_\(post.id)"
            
            if foundRange.location != NSNotFound
            {
                self.addAttribute(NSLinkAttributeName, value: url, range: foundRange)
                self.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: Constants.FontSize.PostText), range: foundRange)
                self.addAttribute(NSForegroundColorAttributeName, value: UIColor.blueTag(), range: foundRange)
            }
        }
        
        //Projects
        for project in post.projects
        {
            let foundRange = self.mutableString.range(of: project.title)
            let url = "p_\(project.id)_\(project.isEnable == true ? 1 : 0)"
            
            if foundRange.location != NSNotFound
            {
                self.addAttribute(NSLinkAttributeName, value: url, range: foundRange)
                self.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: Constants.FontSize.PostText), range: foundRange)
                self.addAttribute(NSForegroundColorAttributeName, value: UIColor.blueTag(), range: foundRange)
            }
        }
        
        //Geographies
        let backup = NSMutableAttributedString(string: self.string)
        post.geographies.sort { (item1, item2) -> Bool in
            //Order array from more characters to less characters
            return item1.titleComplete.characters.count > item2.titleComplete.characters.count
        }
        for geography in post.geographies
        {
            let foundRange = backup.mutableString.range(of: geography.titleComplete, options: .literal)
            let replacement = String(repeating: " ", count: geography.titleComplete.characters.count)
            if foundRange.length != 0
            {
                backup.replaceCharacters(in: foundRange, with: NSAttributedString(string: replacement))
            }
            
            let url = "g_\(geography.id)_\(post.id)"
            
            if foundRange.location != NSNotFound
            {
                self.addAttribute(NSLinkAttributeName, value: url, range: foundRange)
                self.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: Constants.FontSize.PostText), range: foundRange)
                self.addAttribute(NSForegroundColorAttributeName, value: UIColor.blueTag(), range: foundRange)
            }
        }
        return true
    }
    
    func truncate(_ length: Int, suffixString: NSAttributedString? = nil) -> NSMutableAttributedString
    {
        if self.string.characters.count > length
        {
            let mutableAttString = NSMutableAttributedString(attributedString: self.attributedSubstring(from: NSMakeRange(0, length)))
            if let existingSuffixString = suffixString {
                mutableAttString.append(existingSuffixString)
            }
            return mutableAttString
        }
        else
        {
            return self
        }
    }
    
    ///Set Bold and color Style for String
    @discardableResult func setBoldAndColor(_ textToFind:String, color:UIColor, size:CGFloat) -> Bool
    {
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(NSForegroundColorAttributeName, value: color, range: foundRange)
            self.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: size), range: foundRange)
            return true
        }
        return false
    }
    
    ///Set Color in specific String
    @discardableResult func setColor(_ textToFind:String, color:UIColor) -> Bool
    {
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(NSForegroundColorAttributeName, value: color, range: foundRange)
            return true
        }
        return false
    }
    
    ///Set Color and font in specific String
    @discardableResult func setColor(_ textToFind:String, color:UIColor, font:UIFont) -> Bool
    {
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(NSForegroundColorAttributeName, value: color, range: foundRange)
            self.addAttribute(NSFontAttributeName, value: font, range: foundRange)
            return true
        }
        return false
    }
    
    ///Set bold in specific String
    @discardableResult func setBold(_ textToFind: String, size: CGFloat) -> Bool
    {
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound
        {
            self.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: size), range: foundRange)
            return true
        }
        
        return false
    }
    
    ///Set Underline in specific String
    @discardableResult func setUnderline(_ textToFind:String, size:CGFloat, color:UIColor, bold: Bool) -> Bool
    {
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound
        {
            self.addAttribute(NSForegroundColorAttributeName, value: color, range: foundRange)
            self.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: foundRange)
            
            if bold == true
            {
                self.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: size), range: foundRange)

            }
            else
            {
                self.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: size), range: foundRange)
            }
            
            return true
        }
        return false
    }
    
    ///Set Color and bold style to Stakeholders, Projects and Dates in New Post
    @discardableResult func setColorAndBold(_ stakeholders:[(Stakeholder, Range<String.Index>)], projects:[(Project, Range<String.Index>)], geographies: [(ClusterItem, Range<String.Index>)], events: [(Event, Range<String.Index>)]) -> Bool
    {
        let AllTextRange = NSMakeRange(0, self.mutableString.length)
        
        guard stakeholders.count > 0 || projects.count > 0 || geographies.count > 0 || events.count > 0 else {
            //TextView Default Font
            self.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 14.5), range: AllTextRange)
            return false
        }
        
        //TextView Default Font
        self.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 14.5), range: AllTextRange)
        
        
        /****** Searching special words  ******/
        //Stakeholders
        for (tupleStakeholder) in stakeholders
        {
            let foundRange = self.mutableString.range(of: tupleStakeholder.0.fullName)
            if foundRange.location != NSNotFound
            {
                self.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 15.5), range: foundRange)
//                self.addAttribute(NSBackgroundColorAttributeName, value: UIColor.blueBelizeAlphy(), range: foundRange)
                self.addAttribute(NSForegroundColorAttributeName, value: UIColor.blueTag(), range: foundRange)
            }
        }
        
        //Projects
        for (tupleProject) in projects
        {
            let foundRange = self.mutableString.range(of: tupleProject.0.title)
            
            if foundRange.location != NSNotFound
            {
                self.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 15.5), range: foundRange)
//                self.addAttribute(NSBackgroundColorAttributeName, value: UIColor.blueBelizeAlphy(), range: foundRange)
                self.addAttribute(NSForegroundColorAttributeName, value: UIColor.blueTag(), range: foundRange)
            }
        }
        
        //Geographies
        var backup = NSMutableAttributedString(string: self.string)
        for (tupleGeographie) in geographies
        {
            let foundRange = backup.mutableString.range(of: tupleGeographie.0.titleComplete)
            let replacement = String(repeating: " ", count: tupleGeographie.0.titleComplete.characters.count)
            if foundRange.length != 0
            {
                backup.replaceCharacters(in: foundRange, with: NSAttributedString(string: replacement))
            }
            if foundRange.location != NSNotFound
            {
                self.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 15.5), range: foundRange)                
                self.addAttribute(NSForegroundColorAttributeName, value: UIColor.blueTag(), range: foundRange)
            }
        }

        //Events (Dates)
        for (tupleEvent) in events
        {
            let dateString: String! = (tupleEvent.0.allDay) ? tupleEvent.0.date.getDateStringForPost() : "\(tupleEvent.0.date.getDateTimeStringForPost())"
            let foundRange = self.mutableString.range(of: dateString)
            
            if foundRange.location != NSNotFound
            {
                self.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 15.5), range: foundRange)
//                self.addAttribute(NSBackgroundColorAttributeName, value: UIColor.blueBelizeAlphy(), range: foundRange)
                self.addAttribute(NSForegroundColorAttributeName, value: UIColor.blueTag(), range: foundRange)
            }
        }
        
        return true
    }
    
    @discardableResult func highlightText(_ text: String) -> Int
    {
        let splitWords = text.characters.split{$0 == " "}.map(String.init)
        let fullRange = NSMakeRange(0, self.mutableString.length)
        var highlightCount = 0
        for word in splitWords {
            highlightCount += self.highlightWord(word, inRange: fullRange)
        }

        return highlightCount
    }
    
    func highlightWord(_ word: String, inRange: NSRange) -> Int
    {
        let rangeOfString = self.mutableString.range(of: word, options: NSString.CompareOptions.caseInsensitive, range: inRange)
        
        if rangeOfString.location != NSNotFound
        {
            self.addAttribute(NSBackgroundColorAttributeName, value: UIColor.yellow, range: rangeOfString)
            let rangeEnd = rangeOfString.location + rangeOfString.length
            let remainingRange = NSMakeRange(rangeEnd, self.mutableString.length - rangeEnd)
            return 1 + self.highlightWord(word, inRange: remainingRange)
        }
        
        return 0
    }

}

extension String
{
    var asciiValue: Int {
        let scalars = self.unicodeScalars
        return Int(scalars[scalars.startIndex].value)
    }
    
    init?(htmlEncodedString: String)
    {
        let encodedData = htmlEncodedString.data(using: String.Encoding.utf8)!
        let attributedOptions : [String: Any] = [
            NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
            NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue
        ]
        
        do {
            let attributedString = try NSAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil)
            
            self.init(attributedString.string)!
        }
        catch {
            self.init(htmlEncodedString)!    // Something gone wrong, stick with the initial string
        }
    }
    
    ///Returns True if It's a valid email
    func isValidEmail() -> Bool
    {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
    }
    
    func isValidURL(_ string: String) -> Bool
    {
        let urlRegEx = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluate(with: string)
    }
    
    func trim() -> String
    {
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    func trimPunctuation() -> String
    {
        return self.trimmingCharacters(in: CharacterSet.punctuationCharacters)
    }
    
    ///Eg:(|  hello    how are     you?       | -->  |Hello how are you|)
    func trimExcessiveSpaces() -> String
    {
        let components = self.components(separatedBy: CharacterSet.whitespaces)
        let filtered = components.filter({$0.isEmpty == false})

        return filtered.joined(separator: " ")
    }
    
    func isValidShmEmail() -> Bool
    {
        if self.lowercased().containsString("@shm.com") || self.lowercased().containsString("@mail.com")  || self.lowercased().containsString("@definityfirst.com")
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    ///Returns True if It's a valid phone number
    func isValidPhoneNumber() -> Bool
    {
         let PHONE_REGEX = "^\\d{10}$"
        
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        
        return phoneTest.evaluate(with: self)
    }
    
    ///This function return hyperlinks and truncated text for title on home and detail post
    func getEditedTitleForPost(_ post: Post, truncate: Bool) -> String
    {
        //Editing Title
        var editedTitle = post.title
        
        for theStakeholder in post.stakeholders
        {
            editedTitle = editedTitle.replacingOccurrences(of: "{{s=\(theStakeholder.id)}}", with: theStakeholder.fullName)
        }
        
        for theDate in post.events
        {
            let dateString: String! = (theDate.allDay) ? theDate.date.getDateStringForPost() : "\(theDate.date.getDateTimeStringForPost())"
            
            //Final Format
            editedTitle = editedTitle.replacingOccurrences(of: "{{d=\(theDate.id)}}", with: dateString)
        }
        
        for theProject in post.projects
        {
            editedTitle = editedTitle.replacingOccurrences(of: "{{p=\(theProject.id)}}", with: theProject.title)
        }
        
        for geoItem in post.geographies
        {
            editedTitle = editedTitle.replacingOccurrences(of: "{{g=\(geoItem.id)}}", with: geoItem.titleComplete)
        }
        
        return editedTitle
    }
    
    ///Look up a specific word in a String
    func containsString(_ string:String) -> Bool
    {
        if(self.range(of: string, options: .caseInsensitive) != nil)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func containsGeographyString(_ string:String, _ range: Range<String.Index>) -> Bool
    {
        if(self.range(of: string) != nil)
        {
            
            return true
        }
        else
        {
            return false
        }
    }
    
    ///You need to send this format yyyy-MM-dd HH:mm:ss
    func toDate() -> Date?
    {
        let dateFormateForDate = DateFormatter()
        dateFormateForDate.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormateForDate.locale = Locale(identifier: "en_US")

        let dateStringWithoutT = self.replacingOccurrences(of: "T", with: " ")
        if dateStringWithoutT.characters.count > 19 {
            let cut = (dateStringWithoutT as NSString).substring(to: 19)
            return dateFormateForDate.date(from: cut)
        }
        
        return dateFormateForDate.date(from: dateStringWithoutT)
    }
    
    func getIconAttachmentName() -> String
    {
        switch self
        {
        case "png", "jpg", "jpeg", "tiff", "tif", "gif", "bmp", "svg", ".png", ".jpg", ".jpeg", ".tiff", ".tif", ".gif", ".bmp", ".svg":
            return "Picture"
        case "pdf", ".pdf":
            return "Pdf"
        case "xls", "xlsx", "xlsm", "xltx", "xltm", "numbers", ".xls", ".xlsx", ".xlsm", ".xltx", ".xltm", ".numbers":
            return "Excel"
        case "doc", "docx", "docm", "dotm", "dotx", "pages", ".doc", ".docx", ".docm", ".dotm", ".dotx", ".pages":
            return "Word"
        case "key", "pptx", "pptm", "potx", "potm", "ppsx", "ppsm", ".key", ".pptx", ".pptm", ".potx", ".potm", ".ppsx", ".ppsm":
            return "Powerpoint"
        default:
            return "Attachment"
        }
    }
    
    @discardableResult func bold(_ size: CGFloat) -> NSMutableAttributedString
    {
        let attrs = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: size)]
        return NSMutableAttributedString(string:self, attributes:attrs)
    }
    
    var mutableAttributed: NSMutableAttributedString {
        return NSMutableAttributedString(string: self)
    }
    
    var htmlMutableAttributedString: NSMutableAttributedString
    {
        let modifiedFont = NSString(format:"<span style=\"font-family:Helvetica Neue; font-size:15\">%@</span>", self) as String
        
        let attrStr = try! NSMutableAttributedString(data: modifiedFont.data(using: String.Encoding.unicode)!,
            options: [
                NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue
            ],
            documentAttributes: nil)
        
        return attrStr
    }
    
    func localized() -> String
    {
        //check for language key and minimun version number
        if (UserDefaults.standard.value(forKey: Constants.UserKey.BackupLanguage) as? String) != nil
        {
            let version = LibraryAPI.shared.customLocale.getLanguageVersionNumber()
            if version > 0
            {
                return LibraryAPI.shared.customLocale.localizedStringForKey(self)
            }
        }
        
        //Return native localization
        return Bundle.main.localizedString(forKey: self, value: String(), table: nil)
    }
}

extension Float
{
    func getString() -> String
    {
        return String(format: "%.1f", self)
    }
}
