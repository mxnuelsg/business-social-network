//
//  SHMDevice.swift
//  SHM
//
//  Created by Manuel Salinas on 8/5/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

enum UIUserInterfaceIdiom : Int
{
    case unspecified
    case phone
    case pad
}

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_OS_8_OR_LATER         = CGFloat((UIDevice.current.systemVersion as NSString).doubleValue) >= 8.0
    
    static let IS_IPHONE                = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_IPHONE_4_OR_LESS      = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5              = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    
    static let IS_IPHONE_6              = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_ZOOMED_IPHONE_6       = (IS_IPHONE && UIScreen.main.bounds.size.height == 568.0 && IS_OS_8_OR_LATER && UIScreen.main.nativeScale > UIScreen.main.scale)
    
    static let IS_IPHONE_6PLUS          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_ZOOMED_IPHONE_6PLUS   = (IS_IPHONE && UIScreen.main.bounds.size.height == 667.0 && IS_OS_8_OR_LATER && UIScreen.main.nativeScale < UIScreen.main.scale)
    
    static let IS_IPAD                  = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
    static let IS_ANY_IPAD              = (UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0) || (UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0)
}

public extension UIDevice
{
    ///Range depending Device on Post
    class func getFeedRangeToTruncate () -> Int
    {
        if DeviceType.IS_IPHONE_4_OR_LESS || DeviceType.IS_IPHONE_5
        {
            return 115
        }
        else if DeviceType.IS_IPHONE_6
        {
            return 130
        }
        else if DeviceType.IS_IPHONE_6PLUS
        {
            return 140
        }
        else if DeviceType.IS_IPAD
        {
            return 200
        }
        else if DeviceType.IS_IPAD_PRO
        {
            return 250
        }
        else
        {
             return 130
        }
    }
    
    ///Range depending Device on Post with Event Dot (Calendars)
    class func getFeedRangeToTruncateInEvent () -> Int
    {
        if DeviceType.IS_IPHONE_4_OR_LESS || DeviceType.IS_IPHONE_5
        {
            return 100
        }
        else if DeviceType.IS_IPHONE_6
        {
            return 120
        }
        else if DeviceType.IS_IPHONE_6PLUS
        {
            return 130
        }
        else if DeviceType.IS_IPAD
        {
            return 45
        }
        else if DeviceType.IS_IPAD_PRO
        {
            return 45
        }
        else
        {
            return 100
        }
    }
    
}
