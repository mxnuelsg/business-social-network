//
//  SHMViewControllers.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 9/1/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

extension UIViewController
{
    fileprivate struct AssociatedKeys {
        static var rotationManager: RotationManager?
    }
    
    var rotationManager: RotationManager?{
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.rotationManager) as? RotationManager
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &AssociatedKeys.rotationManager, newValue as RotationManager?, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }

    func turnOnRotation(_ rotated: (() -> ())?)
    {
        rotationManager = RotationManager(rotation: rotated, rotationLandscape: nil, rotationPortrait: nil)
        NotificationCenter.default.addObserver(rotationManager!, selector: #selector(RotationManager.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    func turnOnRotation(rotatedLandscape: (() -> ())?, rotatedPortrait: (() -> ())?)
    {
        rotationManager = RotationManager(rotation: nil, rotationLandscape: rotatedLandscape, rotationPortrait: rotatedPortrait)
        NotificationCenter.default.addObserver(rotationManager!, selector: #selector(RotationManager.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    func turnOffRotation()
    {
        if let rotationManager = rotationManager
        {
            NotificationCenter.default.removeObserver(rotationManager)
        }
    }
    
    ///This function add a border to the ViewController
    func setBorder()
    {
        self.view.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        self.view.layer.borderWidth = 1
    }
    
    ///This function remove the text in your back button from navigation controller
    func backButtonArrow()
    {
        navigationItem.backBarButtonItem = UIBarButtonItem(image: UIImage(named:"iconInvisible"), style: UIBarButtonItemStyle.plain, target: self, action: nil)
    }
    
    ///This function removes the badge from the app icon in home screen
    func removeBadgeIcon()
    {
        //Reset Badge icon
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    class RotationManager : NSObject {
        var rotation: (() -> ())?
        var rotationLandscape: (() -> ())?
        var rotationPortrait: (() -> ())?
        
        init(rotation: (() -> ())?, rotationLandscape: (() -> ())?, rotationPortrait: (() -> ())?)
        {
            self.rotation = rotation
            self.rotationLandscape = rotationLandscape
            self.rotationPortrait = rotationPortrait
        }
        
        func rotated()
        {
            rotation?()
            
            if(UIDeviceOrientationIsLandscape(UIDevice.current.orientation))
            {
                rotationLandscape?()
            }
            
            if(UIDeviceOrientationIsPortrait(UIDevice.current.orientation))
            {
                rotationPortrait?()
            }
        }
    }
}
