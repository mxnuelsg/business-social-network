//
//  SHMDate.swift
//  SHM
//
//  Created by Manuel Salinas on 10/1/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

extension Date
{
    ///This function increment years ins NSDate
    func getNewDateAddingYears(_ yearsToAdd: Int) -> Date
    {
        var startDateTimeComponents = DateComponents()
        startDateTimeComponents.year = (Calendar.current as NSCalendar).components(NSCalendar.Unit.year, from: self).year! + yearsToAdd
        startDateTimeComponents.month = (Calendar.current as NSCalendar).components(NSCalendar.Unit.month, from: self).month
        startDateTimeComponents.day = (Calendar.current as NSCalendar).components(NSCalendar.Unit.day, from: self).day
        
        let startDateCalendar = Calendar(identifier: Calendar.Identifier.gregorian)
        return startDateCalendar.date(from: startDateTimeComponents)!
    }
    
    ///This function convert your current NSDate to String Style -> 2015-10-01T15:13:33Z
    func convertToRequestString() -> String
    {
        let dateFormateForDate = DateFormatter()
        dateFormateForDate.dateFormat = "yyyy-MM-dd'T'HH:mm:ssxxx"
        dateFormateForDate.locale = Locale(identifier: Locale.preferredLanguages[0])
        dateFormateForDate.timeZone = TimeZone.current
        
        return dateFormateForDate.string(from: self)
    }
    
    ///Returns Date Format as String June 9, 2015
    func getDateStringForPost() -> String
    {
        let dateFormateForDate = DateFormatter()
        dateFormateForDate.dateFormat = "MMM d, YYYY"
        dateFormateForDate.locale = Locale(identifier: Locale.preferredLanguages[0])
        dateFormateForDate.timeZone = TimeZone.current
        
        return dateFormateForDate.string(from: self)
    }
    
    ///Returns Date Format 12/28/2017
    func getFormatDateString() -> String
    {
        let language = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        if language == "en"
        {
            dateFormatter.locale = Locale(identifier: "en_US")
        }
        else
        {
            dateFormatter.locale = Locale(identifier: "es_MX")
        }
        
        
        return dateFormatter.string(from: self)
    }
    
    ///Returns Date Format as String MM/dd/yyyy HH:mm:ss
    func getDateTimeStringForPost() -> String
    {
        let dateFormateForDate = DateFormatter()
        dateFormateForDate.dateFormat = "MMM d, YYYY HH:mm"
        dateFormateForDate.locale = Locale(identifier: Locale.preferredLanguages[0])
        dateFormateForDate.timeZone = TimeZone.current
        
        return dateFormateForDate.string(from: self)
    }
    
    ///Returns Date Format as String MMMM d, YYYY
    func getStringStyleMedium() -> String
    {
        let dateFormatterForString = DateFormatter()
        dateFormatterForString.dateFormat = "MMMM d, YYYY"
        dateFormatterForString.locale = Locale(identifier: Locale.preferredLanguages[0])
        dateFormatterForString.timeZone = TimeZone.current
        
        return dateFormatterForString.string(from: self)
    }
    
    ///Returns Date Format as November 2017
    func getMonthYear() -> String
    {
        let dateFormatterForString = DateFormatter()
        dateFormatterForString.dateFormat = "MMMM YYYY"
        dateFormatterForString.locale = Locale(identifier: Locale.preferredLanguages[0])
        dateFormatterForString.timeZone = TimeZone.current
        
        return dateFormatterForString.string(from: self)
    }
    
    ///Returns Date Format as String Monday, October 21, 2017
    func getStringStyleFull() -> String
    {
        let dateFormatterForString = DateFormatter()
        dateFormatterForString.dateStyle = DateFormatter.Style.full
        dateFormatterForString.locale = Locale(identifier: Locale.preferredLanguages[0])
        dateFormatterForString.timeZone = TimeZone.current
        
        return dateFormatterForString.string(from: self)
    }
    
    func getDateTimeStyle() -> String
    {
        let dateFormatterForString = DateFormatter()
        dateFormatterForString.dateFormat = "MMM d, YYYY HH:mm"
        dateFormatterForString.locale = Locale(identifier: Locale.preferredLanguages[0])
        dateFormatterForString.timeZone = TimeZone.current
        let stringDate = dateFormatterForString.string(from: self)
        
        return stringDate
    }
    
    ///Convert a NSDate to Specific Format
    func showStringFormat(_ format:String) -> String
    {
        return self.getStringStyleMedium()
    }
    
    ///Get Today with started hour (2015-11-05 00:00:00 +0000)
    func getStartOfDay() -> Date
    {
        var calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone.current
        
        let today = calendar.startOfDay(for: self)
        
        return today
    }
    
    ///This function add hours to NSDate
    func dateByAddingHours(_ hours: Int) -> Date
    {
       return (Calendar.current as NSCalendar).date(
            byAdding: .hour,
            value: hours,
            to: self,
            options: [])!
    }
    
    ///This function add days to NSDate
    func dateByAddingDays(_ days: Int) -> Date
    {
        return (Calendar.current as NSCalendar).date(
            byAdding: .day,
            value: days,
            to: self,
            options: [])!
    }
    
    ///This function add days to NSDate
    func dateByAddingMonths(_ months: Int) -> Date
    {
        return (Calendar.current as NSCalendar).date(
            byAdding: .month,
            value: months,
            to: self,
            options: [])!
    }
    
    func firstDayOfMonth() -> Date
    {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func lastDayOfMonth() -> Date
    {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.firstDayOfMonth())!
    }
    
    func getMonth() -> Int
    {
        return Calendar.current.component(.month, from: self)
    }
    
    func getYear() -> Int
    {
        return Calendar.current.component(.year, from: self)
    }
}

//MARK: ENUMS
public extension Date
{
    
    ///Day name format.
    ///
    /// - threeLetters: 3 letter day abbreviation of day name.
    /// - oneLetter: 1 letter day abbreviation of day name.
    /// - full: Full day name.
    public enum DayNameStyle {
        case threeLetters
        case oneLetter
        case full
    }
    
    ///Month name format.
    ///
    /// - threeLetters: 3 letter month abbreviation of month name.
    /// - oneLetter: 1 letter month abbreviation of month name.
    /// - full: Full month name.
    public enum MonthNameStyle {
        case threeLetters
        case oneLetter
        case full
    }
    
}

// MARK: - INITIALIZERS
public extension Date
{
    
    ///Create a new date form calendar components.
    ///
    /// - Parameters:
    ///   - calendar: Calendar (default is current).
    ///   - timeZone: TimeZone (default is current).
    ///   - era: Era (default is current era).
    ///   - year: Year (default is current year).
    ///   - month: Month (default is current month).
    ///   - day: Day (default is today).
    ///   - hour: Hour (default is current hour).
    ///   - minute: Minute (default is current minute).
    ///   - second: Second (default is current second).
    ///   - nanosecond: Nanosecond (default is current nanosecond).
    public init?(
        calendar: Calendar? = Calendar.current,
        timeZone: TimeZone? = TimeZone.current,
        era: Int? = Date().era,
        year: Int? = Date().year,
        month: Int? = Date().month,
        day: Int? = Date().day,
        hour: Int? = Date().hour,
        minute: Int? = Date().minute,
        second: Int? = Date().second,
        nanosecond: Int? = Date().nanosecond) {
        
        var components = DateComponents()
        components.calendar = calendar
        components.timeZone = timeZone
        components.era = era
        components.year = year
        components.month = month
        components.day = day
        components.hour = hour
        components.minute = minute
        components.second = second
        components.nanosecond = nanosecond
        
        if let date = calendar?.date(from: components) {
            self = date
        } else {
            return nil
        }
    }
    
    ///Create date object from ISO8601 string.
    ///
    /// - Parameter iso8601String: ISO8601 string of format (yyyy-MM-dd'T'HH:mm:ss.SSSZ).
    public init?(iso8601String: String) {
        // https://github.com/justinmakaila/NSDate-ISO-8601/blob/master/NSDateISO8601.swift
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let date = dateFormatter.date(from: iso8601String) {
            self = date
        } else {
            return nil
        }
    }
    
    ///Create new date object from UNIX timestamp.
    ///
    /// - Parameter unixTimestamp: UNIX timestamp.
    public init(unixTimestamp: Double) {
        self.init(timeIntervalSince1970: unixTimestamp)
    }
    
}

//MARK: PROPERTIES
extension Date
{
    ///User’s current calendar.
    public var calendar: Calendar {
        return Calendar.current
    }
    
    ///Era.
    public var era: Int {
        
        return calendar.component(.era, from: self)
    }
    
    ///Year.
    public var year: Int {
        get {
            return calendar.component(.year, from: self)
        }
        set {
            self = Date(calendar: calendar, timeZone: timeZone, era: era, year: newValue, month: month, day: day, hour: hour, minute: minute, second: second, nanosecond: nanosecond) ?? self
        }
    }
    
    ///Quarter.
    public var quarter: Int {
        
        return calendar.component(.quarter, from: self)
    }
    
    ///Month.
    public var month: Int {
        get {
            return calendar.component(.month, from: self)
        }
        set {
            self = Date(calendar: calendar, timeZone: timeZone, era: era, year: year, month: newValue, day: day, hour: hour, minute: minute, second: second, nanosecond: nanosecond) ?? self
        }
    }
    
    ///Week of year.
    public var weekOfYear: Int {
        return calendar.component(.weekOfYear, from: self)
    }
    
    ///Week of month.
    public var weekOfMonth: Int {
        return calendar.component(.weekOfMonth, from: self)
    }
    
    ///Weekday.
    public var weekday: Int {
        return calendar.component(.weekday, from: self)
    }
    
    ///Day.
    public var day: Int {
        get {
            return calendar.component(.day, from: self)
        }
        set {
            self = Date(calendar: calendar, timeZone: timeZone, era: era, year: year, month: month, day: newValue, hour: hour, minute: minute, second: second, nanosecond: nanosecond) ?? self
        }
    }
    
    ///Hour.
    public var hour: Int {
        get {
            return calendar.component(.hour, from: self)
        }
        set {
            self = Date(calendar: calendar, timeZone: timeZone, era: era, year: year, month: month, day: day, hour: newValue, minute: minute, second: second, nanosecond: nanosecond) ?? self
        }
    }
    
    ///Minutes.
    public var minute: Int {
        get {
            return calendar.component(.minute, from: self)
        }
        set {
            self = Date(calendar: calendar, timeZone: timeZone, era: era, year: year, month: month, day: day, hour: hour, minute: newValue, second: second, nanosecond: nanosecond) ?? self
        }
    }
    
    ///Seconds.
    public var second: Int {
        get {
            return calendar.component(.second, from: self)
        }
        set {
            self = Date(calendar: calendar, timeZone: timeZone, era: era, year: year, month: month, day: day, hour: hour, minute: minute, second: newValue, nanosecond: nanosecond) ?? self
        }
    }
    
    ///Nanoseconds.
    public var nanosecond: Int {
        
        return calendar.component(.nanosecond, from: self)
    }
    
    ///Check if date is in future.
    public var isInFuture: Bool {
        
        return self > Date()
    }
    
    ///Check if date is in past.
    public var isInPast: Bool {
        
        return self < Date()
    }
    
    ///Check if date is in today.
    public var isInToday: Bool {
        
        return self.day == Date().day && self.month == Date().month && self.year == Date().year
    }
    
    ///Check if date is in yesterday.
    public var isInYesterday: Bool {
        
        return self.adding(.day, value: 1).isInToday
    }
    
    ///Check if date is in tomorrow.
    public var isInTomorrow: Bool {
        
        return self.adding(.day, value: -1).isInToday
    }
    
    ///Nearest five minutes to date.
    public var nearestFiveMinutes: Date {
        
        var components = Calendar.current.dateComponents([.year, .month , .day , .hour , .minute], from: self)
        
        guard let min = components.minute else {
            return self
        }
        components.minute! = min % 5 < 3 ? min - min % 5 : min + 5 - (min % 5)
        components.second = 0
        
        return Calendar.current.date(from: components) ?? self
    }
    
    ///Nearest ten minutes to date.
    public var nearestTenMinutes: Date {
        
        var components = Calendar.current.dateComponents([.year, .month , .day , .hour , .minute], from: self)
        
        guard let min = components.minute else {
            return self
        }
        components.minute? = min % 10 < 6 ? min - min % 10 : min + 10 - (min % 10)
        components.second = 0
        
        return Calendar.current.date(from: components) ?? self
    }
    
    ///Nearest quarter hour to date.
    public var nearestQuarterHour: Date {
        
        var components = Calendar.current.dateComponents([.year, .month , .day , .hour , .minute], from: self)
        
        guard let min = components.minute else {
            return self
        }
        components.minute! = min % 15 < 8 ? min - min % 15 : min + 15 - (min % 15)
        components.second = 0
        
        return Calendar.current.date(from: components) ?? self
    }
    
    ///Nearest half hour to date.
    public var nearestHalfHour: Date {
        
        var components = Calendar.current.dateComponents([.year, .month , .day , .hour , .minute], from: self)
        guard let min = components.minute else {
            return self
        }
        components.minute! = min % 30 < 15 ? min - min % 30 : min + 30 - (min % 30)
        components.second = 0
        
        return Calendar.current.date(from: components) ?? self
    }
    
    ///Nearest hour to date.
    public var nearestHour: Date {
        
        if minute >= 30
        {
            return self.end(of: .hour) ?? self
        }
        else
        {
            return self.beginning(of: .hour) ?? self
        }
    }
    
    ///Time zone used by system.
    public var timeZone: TimeZone {
        
        return self.calendar.timeZone
    }
    
    ///UNIX timestamp from date.
    public var unixTimestamp: Double {
        
        return timeIntervalSince1970
    }
}

//MARK: METHODS
extension Date
{
    //Returns true if the year is greater than 1000 (this number is just a parameter)
    func isValid() -> Bool {
        return self.year > 1000
    }
    
    ///Returns Date Format as String Dec 23, 208 12:28 AM/PM
    func getDateTime() -> String
    {
        let dateFormateForDate = DateFormatter()
        dateFormateForDate.dateFormat = "MMM d, YYYY hh:mm a"
        dateFormateForDate.locale = Locale(identifier: Locale.preferredLanguages[0])
        dateFormateForDate.timeZone = .current
        
        return dateFormateForDate.string(from: self)
    }
    
    ///Returns only the time 01:54 AM
    func getTime() -> String
    {
        let dateFormatterForString = DateFormatter()
        dateFormatterForString.dateFormat = "hh:mm a"
        dateFormatterForString.locale = Locale(identifier: Locale.preferredLanguages[0])
        dateFormatterForString.timeZone = .current
        let stringDate = dateFormatterForString.string(from: self)
        
        return stringDate
    }
    
    //Add minutes to Date
    func add(minutes: Int) -> Date
    {
        return Calendar(identifier: .gregorian).date(byAdding: .minute, value: minutes, to: self)!
    }
    
    ///Date by adding multiples of calendar component.
    /// - Parameters:
    ///   - component: component type.
    ///   - value: multiples of components to add.
    /// - Returns: original date + multiples of component added.
    public func adding(_ component: Calendar.Component, value: Int) -> Date {
        switch component {
        case .second:
            return calendar.date(byAdding: .second, value: value, to: self) ?? self
            
        case .minute:
            return calendar.date(byAdding: .minute, value: value, to: self) ?? self
            
        case .hour:
            return calendar.date(byAdding: .hour, value: value, to: self) ?? self
            
        case .day:
            return calendar.date(byAdding: .day, value: value, to: self) ?? self
            
        case .weekOfYear, .weekOfMonth:
            return calendar.date(byAdding: .day, value: value * 7, to: self) ?? self
            
        case .month:
            return calendar.date(byAdding: .month, value: value, to: self) ?? self
            
        case .year:
            return calendar.date(byAdding: .year, value: value, to: self) ?? self
            
        default:
            return self
        }
    }
    
    ///Add calendar component to date.
    ///
    /// - Parameters:
    ///   - component: component type.
    ///   - value: multiples of compnenet to add.
    public mutating func add(_ component: Calendar.Component, value: Int) {
        self = self.adding(component, value: value)
    }
    
    ///Data at the beginning of calendar component.
    ///
    /// - Parameter component: calendar component to get date at the beginning of.
    /// - Returns: date at the beginning of calendar component (if applicable).
    public func beginning(of component: Calendar.Component) -> Date? {
        switch component {
        case .second:
            return calendar.date(from: calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self))
            
        case .minute:
            return calendar.date(from: calendar.dateComponents([.year, .month, .day, .hour, .minute], from: self))
            
        case .hour:
            return calendar.date(from: calendar.dateComponents([.year, .month, .day, .hour], from: self))
            
        case .day:
            return self.calendar.startOfDay(for: self)
            
        case .weekOfYear, .weekOfMonth:
            return calendar.date(from: calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))
            
        case .month:
            return calendar.date(from: calendar.dateComponents([.year, .month], from: self))
            
        case .year:
            return calendar.date(from: calendar.dateComponents([.year], from: self))
            
        default:
            return nil
        }
    }
    
    ///Date at the end of calendar component.
    ///
    /// - Parameter component: calendar component to get date at the end of.
    /// - Returns: date at the end of calendar component (if applicable).
    public func end(of component: Calendar.Component) -> Date? {
        switch component {
        case .second:
            var date = self.adding(.second, value: 1)
            guard let after = calendar.date(from: calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)) else {
                return nil
            }
            date = after
            date.add(.second, value: -1)
            return date
            
        case .minute:
            var date = self.adding(.minute, value: 1)
            guard let after = calendar.date(from: calendar.dateComponents([.year, .month, .day, .hour, .minute], from: date)) else {
                return nil
            }
            date = after.adding(.second, value: -1)
            return date
            
        case .hour:
            var date = self.adding(.hour, value: 1)
            guard let after = calendar.date(from: calendar.dateComponents([.year, .month, .day, .hour], from: date)) else {
                return nil
            }
            date = after.adding(.second, value: -1)
            return date
            
        case .day:
            var date = self.adding(.day, value: 1)
            date = date.calendar.startOfDay(for: date)
            date.add(.second, value: -1)
            return date
            
        case .weekOfYear, .weekOfMonth:
            var date = self
            guard let beginningOfWeek = calendar.date(from: calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: date)) else {
                return nil
            }
            date = beginningOfWeek.adding(.day, value: 7).adding(.second, value: -1)
            return date
            
        case .month:
            var date = self.adding(.month, value: 1)
            guard let after = calendar.date(from: calendar.dateComponents([.year, .month], from: date)) else {
                return nil
            }
            date = after.adding(.second, value: -1)
            return date
            
        case .year:
            var date = self.adding(.year, value: 1)
            guard let after = calendar.date(from: calendar.dateComponents([.year], from: date)) else {
                return nil
            }
            date = after.adding(.second, value: -1)
            return date
            
        default:
            return nil
        }
    }
    
    ///Day name from date.
    ///
    /// - Parameter Style: style of day name (default is DayNameStyle.full).
    /// - Returns: day name string (example: W, Wed, Wednesday).
    public func dayName(ofStyle style: DayNameStyle = .full) -> String {
        // http://www.codingexplorer.com/swiftly-getting-human-readable-date-nsdateformatter/
        let dateFormatter = DateFormatter()
        var format: String {
            switch style {
            case .oneLetter:
                return "EEEEE"
            case .threeLetters:
                return "EEE"
            case .full:
                return "EEEE"
            }
        }
        dateFormatter.setLocalizedDateFormatFromTemplate(format)
        return dateFormatter.string(from: self)
    }
    
    ///Month name from date.
    ///
    /// - Parameter Style: style of month name (default is MonthNameStyle.full).
    /// - Returns: month name string (example: D, Dec, December).
    public func monthName(ofStyle style: MonthNameStyle = .full) -> String {
        // http://www.codingexplorer.com/swiftly-getting-human-readable-date-nsdateformatter/
        let dateFormatter = DateFormatter()
        var format: String {
            switch style {
            case .oneLetter:
                return "MMMMM"
            case .threeLetters:
                return "MMM"
            case .full:
                return "MMMM"
            }
        }
        dateFormatter.setLocalizedDateFormatFromTemplate(format)
        return dateFormatter.string(from: self)
    }
}

