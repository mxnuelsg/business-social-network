//
//  SHMUITableViews.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 9/11/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import Foundation
import ObjectiveC

extension UITableView
{
    func displayBackgroundMessage(_ message: String, subMessage: String?) {
        let lblEmptyMessage = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 63))
        lblEmptyMessage.textAlignment = NSTextAlignment.center
        lblEmptyMessage.attributedText = NSAttributedString.getAttributedSecondaryTitle(message, subtitle: subMessage, fontSize: 16)
        lblEmptyMessage.numberOfLines = 3
        self.backgroundView = lblEmptyMessage
//        self.backgroundColor = (DeviceType.IS_ANY_IPAD == true) ? UIColor.clearColor() : UIColor.white
    }
    
    func hideEmtpyCells()
    {
        self.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    func dismissBackgroundMessage() {
        self.backgroundView = nil
    }
}

private var xoAssociationKey: UInt8 = 0

extension UITableViewController
{
     //MARK: REFRESHER CUSTOM CONTROL
    var refresher: ActionRefresher! {
        get {
            return objc_getAssociatedObject(self, &xoAssociationKey) as? ActionRefresher
        }
        set(newValue) {
            objc_setAssociatedObject(self, &xoAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func enableRefreshAction(_ onRefresh: @escaping () -> ())
    {
        self.refresher = ActionRefresher(onRefresh: onRefresh)
        self.refresher.refreshControl = UIRefreshControl()
        self.refresher.refreshControl?.backgroundColor =  .white
        self.refresher.refreshControl?.addTarget(refresher, action: #selector(self.refresher.refresh), for: .valueChanged)
        self.refresher.refreshControl?.setLastUpdate()
        
        self.refreshControl = self.refresher.refreshControl
    }
    
    func refreshed()
    {
        self.refresher.refreshControl?.endRefreshing()
        self.refresher.refreshControl?.setLastUpdate()
    }

    //MARK: SHOW AND HIDE MESSAGE IN BACKGROUND
    func displayBackgroundMessage(_ message: String, subMessage: String?)
    {
        tableView.displayBackgroundMessage(message, subMessage: subMessage)
    }
    
    func dismissBackgroundMessage()
    {
        self.tableView.dismissBackgroundMessage()
    }
    
   }
