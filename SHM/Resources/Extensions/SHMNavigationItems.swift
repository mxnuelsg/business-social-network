//
//  SHMNavigationItems.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 10/19/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

extension UINavigationItem {
    func fadeOutTitleView() {
        if let titleView = titleView {
            titleView.alpha = 1
            
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                titleView.alpha = 0
                }, completion: { (animated) -> Void in
                    self.titleView = nil
            })
        }
    }
}
