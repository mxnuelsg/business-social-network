//
//  SHMCollectionView.swift
//  SHM
//
//  Created by Manuel Salinas on 1/12/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

import Foundation
import ObjectiveC

extension UICollectionView
{
    func displayBackgroundMessage(_ message: String, subMessage: String?)
    {
        let lblEmptyMessage = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 63))
        lblEmptyMessage.textAlignment = NSTextAlignment.center
        lblEmptyMessage.attributedText = NSAttributedString.getAttributedSecondaryTitle(message, subtitle: subMessage, fontSize: 16)
        lblEmptyMessage.numberOfLines = 3
        self.backgroundView = lblEmptyMessage
        self.backgroundColor = (DeviceType.IS_ANY_IPAD == true) ? UIColor.clear : UIColor.white
    }
    
    func dismissBackgroundMessage()
    {
        self.backgroundView = nil
    }
}
