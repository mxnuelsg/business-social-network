//
//  SHMColors.swift
//  SHM
//
//  Created by Manuel Salinas on 7/27/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import Foundation
import UIKit

extension UIColor
{
     ///Default color for Navigation controller
    class func colorForNavigationController() -> UIColor
    {
//        return UIColor(red:20/255, green: 62/255, blue: 94/255, alpha: 1)
        return UIColor(red: 0.086275, green: 0.274510 ,blue: 0.411765, alpha: 1 )
    }
    
    class func colorForMenuOptionSelected() -> UIColor
    {
        return UIColor(red:0.168627, green:0.517647, blue:0.823529 , alpha:0.1)
    }
    
    class func colorForStatusBar() -> UIColor
    {
        return UIColor(red:0.066667, green:0.196078, blue:0.270588, alpha: 1)
    }
    
    class func blackColor(_ alpha: CGFloat) -> UIColor
    {
        return UIColor(red:0, green:0, blue:0, alpha:alpha)
    }
    
    class func whiteColor(_ alpha: CGFloat) -> UIColor
    {
        return UIColor(red:1.000000, green:1.000000, blue:1.000000, alpha:alpha)
    }
    
    class func grayCloudy() -> UIColor
    {
        return UIColor(red: 0.603922, green: 0.674510, blue: 0.729412, alpha: 1)
    }
    
    class func blueBelize() -> UIColor
    {
        return UIColor(red: 0.133333, green: 0.419608, blue: 0.666667, alpha: 1)
    }
    
    class func blueBelizeAlphy() -> UIColor
    {
        return UIColor(red: 0.133333, green: 0.419608, blue: 0.666667, alpha: 0.75)
    }
    
    class func blackAsfalto() -> UIColor
    {
        return UIColor(red: 0.156863, green: 0.219608, blue: 0.294118, alpha: 1 )
    }
    
    class func whiteCloud() -> UIColor
    {
        return UIColor(red: 0.905882, green: 0.925490, blue: 0.929412, alpha: 1 )
    }
    
    class func colorForSelectedTabBArItem() -> UIColor
    {
        return UIColor(red: 0.423529, green: 0.474510, blue: 0.478431, alpha: 0.8)
    }
    
    class func colorDetailTitleLabel() -> UIColor
    {
        return UIColor(red: 136/255, green: 157/255, blue: 172/255, alpha: 1)
    }
    
    class func grayConcreto() -> UIColor
    {
        return UIColor(red: 0.513726, green: 0.580392, blue: 0.584314, alpha: 1)
    }
    
    class func graySectionSeparator() -> UIColor
    {
        return UIColor(red: 210/255, green: 214/255, blue: 222/255, alpha: 1)
    }
    
    class func greenBirthday() -> UIColor
    {
        return UIColor(red:0.231373, green: 0.568627, blue: 0.160784, alpha: 1 )
    }
    
    class func yellowPost() -> UIColor
    {
        return UIColor(red:244 / 255, green: 189 / 255, blue: 79/255, alpha: 1 )
    }
    
    class func blueImportantDate() -> UIColor
    {
        return UIColor(red:0.133333, green: 0.419608, blue: 0.666667, alpha: 1  )
    }
    
    ///Default color for Errors
    class func errorColor() -> UIColor
    {
        return UIColor(red:0.768627, green: 0.000000, blue: 0.117647, alpha: 1)
    }
    
    ///Default color for Success actions
    class func successColor() -> UIColor
    {
        return UIColor(red:0.117647, green: 0.694118, blue: 0.541176, alpha: 1)
    }
    
    ///Default color for information
    class func infoColor() -> UIColor
    {
        return UIColor(red: 0.054902, green: 0.466667, blue: 0.866667, alpha: 1)
    }
    
    ///Default color for warnings
    class func warningColor() -> UIColor
    {
        return UIColor(red: 1.000000, green: 0.968627, blue: 0.643137, alpha: 1 )
    }
    
    class func noEditableColor() -> UIColor
    {
        return UIColor(red: 0.979487, green: 0.979487, blue: 0.979487, alpha: 1)
    }

    class func blueColorNewPost() -> UIColor {
        return UIColor(red:20/255, green: 62/255, blue: 94/255, alpha: 1)
    }
    
    class func grayTableBackground() -> UIColor {
        return UIColor(red: 211/255, green: 214/255, blue: 221/255, alpha: 1)
    }
    
    class func blueGeneralText() -> UIColor {
        return UIColor(red: 12/255, green: 21/255, blue: 30/255, alpha: 1)
    }
    
    class func blueUnreadBullet() -> UIColor {
        return UIColor(red: 29/255, green: 155/255, blue: 246/255, alpha: 1)
    }
    
    class func blueSearchBar() -> UIColor {
        return UIColor(red: 21/255, green: 35/255, blue: 51/255, alpha: 1)
    }
    
    class func blueSideMenu() -> UIColor {
        return UIColor(red: 0.054901, green: 0.105882, blue: 0.1529411, alpha: 1)
    }
    
    class func greenFollowAction() -> UIColor
    {
//        return UIColor(red: 64/255, green: 128/255, blue: 0, alpha: 1)
        
        return UIColor(red: 0.141176, green: 0.635294, blue: 0.305882, alpha: 1)
    }
    
    class func redUnfollowAction() -> UIColor
    {
        return UIColor(red:0.831373, green: 0.000000, blue: 0.109804, alpha: 1)
    }
    
    class func blueUnderlinedTag() -> UIColor {
        return UIColor(red: 210/255, green: 233/255, blue: 251/255, alpha: 1)
    }
    
    class func blueTag() -> UIColor
    {
        return UIColor(red: 0.168627, green: 0.517647, blue: 0.823529, alpha: 1)
    }
    
    class func blueSky() -> UIColor
    {
        return UIColor(red: 0.956863, green: 0.980392, blue: 0.996078, alpha: 1)
    }
    
    class func greenFamily() -> UIColor
    {
        return UIColor(red: 0.341176, green: 0.666666, blue: 0.282352, alpha: 1)
    }
    
    class func yellowFriends() -> UIColor
    {
        return UIColor(red: 0.925490, green: 0.65490, blue: 0.164705, alpha: 1)
    }
    
    class func orangeWork() -> UIColor
    {
        return UIColor(red: 0.984313, green: 0.4313725, blue: 0.290196, alpha: 1)
    }
    
    class func grayRelationReport() -> UIColor
    {
        return UIColor(red: 235/255, green: 240/255, blue: 241/255, alpha: 1)
    }
}
