//
//  SHMArrays.swift
//  SHM
//
//  Created by Manuel Salinas on 9/2/16.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

///Add values
func +=<K, V> (left: inout [K : V], right: [K : V])
{
    for (k, v) in right
    {
        left[k] = v
    }
}

///Convert an array of strings to string separated by commas
extension Sequence where Iterator.Element == String
{
    func collapseWithCommas() -> String
    {
        var collapsedString = String()
        let values = self.map({ $0 })
        collapsedString += values.joined(separator: ", ")
        
        return collapsedString
    }
}
