//
//  SHMTextFields.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 10/9/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

extension UITextField {
    /// Returns true if empty and animates the TextField with a shake, returns false if has text
    func isEmpty(withShake shake: Bool) -> Bool {
        if let text = self.text
        {
            if text.isEmpty
            {
                if shake {
                    self.shakeAnimation()
                }
                return true
            }
        }
        
        return false
    }
}
