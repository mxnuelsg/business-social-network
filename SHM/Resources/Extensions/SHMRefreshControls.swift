//
//  SHMRefreshControls.swift
//  SHM
//
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

extension UIRefreshControl
{
    func setLastUpdate()
    {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMM d, h:mm a"
        let title = "Last update".localized() + ": \(dateFormat.string(from: Date()))"
        let attrs = [NSForegroundColorAttributeName : UIColor.darkText]
        
        attributedTitle = NSAttributedString(string: title, attributes: attrs)
    }
}
