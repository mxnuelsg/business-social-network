//
//  SHMViews.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/4/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

extension UIView
{
    func addSubViewController(_ childVC: UIViewController, parentVC: UIViewController)
    {
        self.addSubViewController(childVC, parentVC: parentVC, margin: 0)
    }
    
    func addSubViewController(_ childVC: UIViewController, parentVC: UIViewController, margin: Int)
    {
        childVC.view.translatesAutoresizingMaskIntoConstraints = false
        parentVC.addChildViewController(childVC)
        self.addSubview(childVC.view)
        childVC.didMove(toParentViewController: parentVC)
        
        let viewsDict = ["cview":childVC.view]
        
        let constraintsH = NSLayoutConstraint.constraints(withVisualFormat: "H:|-\(margin)-[cview]-\(margin)-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict)
        let constraintsV = NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(margin)-[cview]-\(margin)-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict)
        
        self.addConstraints(constraintsH)
        self.addConstraints(constraintsV)
    }
    
    func removeGestureRecognizersOfType<T: UITapGestureRecognizer>(_ type: T.Type)
    {
        guard let gestureRecognizers = self.gestureRecognizers else {
            
            return
        }
        
        for case let gesture in gestureRecognizers where type(of: gesture) == type {
            
            self.removeGestureRecognizer(gesture)
        }
    }
    
    ///This function add a border to the ViewController
    func setBorder()
    {
        self.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        self.layer.borderWidth = 1
    }
    
    func setBlackBorder()
    {
        self.layer.borderColor = UIColor.blackAsfalto().cgColor
        self.layer.borderWidth = 1
    }
    
    func setBlueBorder()
    {
        self.layer.borderColor = UIColor.colorForNavigationController().cgColor
        self.layer.borderWidth = 1
    }
    
    func setBorder(_ color: UIColor)
    {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = 1
    }
    
    func setBorderWidth(_ width: CGFloat)
    {
        self.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        self.layer.borderWidth = width
    }
    
    func clearBorder()
    {
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.borderWidth = 0
    }
    
    func cornerRadius()
    {
        self.layer.cornerRadius = 3
        self.clipsToBounds = true
    }
    
    func round()
    {
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
    }
    
    func addShadow()
    {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 2, height: 1)
        self.layer.shadowRadius = 2.5
        self.layer.masksToBounds = false
    }
    
    ///Blur Style Dark
    func blur()
    {
        let visuaEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
        visuaEffectView.frame = bounds
        visuaEffectView.autoresizingMask = [UIViewAutoresizing.flexibleWidth , UIViewAutoresizing.flexibleHeight]
        visuaEffectView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(visuaEffectView)
    }
    
    ///Blur Style Light
    func blurLight()
    {
        let visuaEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.light))
        visuaEffectView.frame = bounds
        visuaEffectView.autoresizingMask = [UIViewAutoresizing.flexibleWidth , UIViewAutoresizing.flexibleHeight]
        visuaEffectView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(visuaEffectView)
    }
    
    ///Animations to make a view shaking. Useful when something is wrong
    func shakeAnimation()
    {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 3
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 5, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 5, y: self.center.y))
        self.layer.add(animation, forKey: "position")

    }
    
    // Will add constraints to fix the margins of a childView embedded in the currentView instance, negative values will void the constraints
    func fixChildView(_ childView: UIView, margins: UIEdgeInsets)
    {
        // Leading
        if margins.left >= 0 {
            addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: childView, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: margins.left))
        }
        // Trailing
        if margins.right >= 0 {
            addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: childView, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: margins.right))
        }
        // Top
        if margins.top >= 0 {
            addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: childView, attribute: NSLayoutAttribute.top, multiplier: 1, constant: margins.top))
        }
        // Bottom
        if margins.bottom >= 0 {
            addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: childView, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: margins.bottom))
        }
    }
    
    func fixCenterYChildView(_ childView: UIView) {
        addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: childView, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
    }
    
    func fixSize(_ size: CGSize) {
        addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: size.width))
        addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: size.height))
    }
}
