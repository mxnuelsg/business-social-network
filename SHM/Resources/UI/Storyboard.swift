//
//  Storyboard.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/14/15.
//  Modified by Manuel Salinas on 10/05/16.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class Storyboard: NSObject
{
    class func getInstanceOf<T: UIViewController>(_ type: T.Type) -> T
    {
        let storyboard = "\(T.self)".hasSuffix("_iPad") ? UIStoryboard(name: "MainPad", bundle: nil) : UIStoryboard(name: "Main", bundle: nil)
    
        return storyboard.instantiateViewController(withIdentifier: "\(T.self)") as! T
    }
    
    class func getInstanceFromStoryboard<T: UIViewController>(_ storyboard: String) -> T
    {
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        
        return storyboard.instantiateViewController(withIdentifier: "\(T.self)") as! T
    }
}

