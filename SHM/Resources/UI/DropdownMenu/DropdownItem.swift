//
//  DropdownItem.swift
//  DropdownMenu
//
//  Created by Suric on 16/5/27.
//  Copyright © 2016年 teambition. All rights reserved.
//

import UIKit

public enum DropdownItemStyle: Int
{
    case `default`
    case highlight
}

open class DropdownItem
{
    open var image: UIImage?
    open var title: String
    open var style: DropdownItemStyle
    open var accessoryImage: UIImage?
    open var level = 0
    open var isSelected = false
    public init(image: UIImage? = nil, title: String, style: DropdownItemStyle = .default, accessoryImage: UIImage? = nil)
    {
        self.image = image
        self.title = title
        self.style = style
        self.accessoryImage = accessoryImage
    }
    
    func getTitle() -> String
    {
        switch self.level
        {
        case 0:
            return self.title
        case 1:
            return "  \(self.title)"
        case 2:
            return "     \(self.title)"
        default:
            return String()
        }
    }
}
