//
//  DropdownMenu.swift
//  DropdownMenu
//
//  Created by Suric on 16/5/26.
//  Copyright © 2016年 teambition. All rights reserved.
//

import UIKit

public protocol DropdownMenuDelegate: class
{
    func dropdownMenu(_ dropdownMenu: DropdownMenu, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell?
    func dropdownMenu(_ dropdownMenu: DropdownMenu, didSelectRowAtIndexPath indexPath: IndexPath)
    func dropdownMenuCancel(_ dropdownMenu: DropdownMenu)
}

public extension DropdownMenuDelegate
{
    func dropdownMenu(_ dropdownMenu: DropdownMenu, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell? {
        return nil
    }
    
    func dropdownMenu(_ dropdownMenu: DropdownMenu, didSelectRowAtIndexPath indexPath: IndexPath) {
    }
    
    func dropdownMenuCancel(_ dropdownMenu: DropdownMenu) {
    }
}

open class DropdownMenu: UIView {
    
    fileprivate weak var navigationController: UINavigationController!
    fileprivate var items: [DropdownItem] = []
    fileprivate var selectedRow: Int
    open var tableView: UITableView!
    fileprivate var barCoverView: UIView!
    fileprivate var isShow = false

    open weak var delegate: DropdownMenuDelegate?
    open var animateDuration: TimeInterval = 0.25
    open var backgroudBeginColor: UIColor = UIColor.black.withAlphaComponent(0)
    open var backgroudEndColor = UIColor(white: 0, alpha: 0.4)
    open var rowHeight: CGFloat = 50
    open var tableViewHeight: CGFloat = 0
    open var defaultBottonMargin: CGFloat = 150
    open var textColor: UIColor = UIColor(red: 56.0/255.0, green: 56.0/255.0, blue: 56.0/255.0, alpha: 1.0)
    open var highlightColor: UIColor = UIColor(red: 3.0/255.0, green: 169.0/255.0, blue: 244.0/255.0, alpha: 1.0)
    open var tableViewBackgroundColor: UIColor = UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
    open var tableViewSeperatorColor = UIColor(red: 217.0/255.0, green: 217.0/255.0, blue: 217.0/255.0, alpha: 1.0)
    open var displaySelected: Bool = true


    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public init(navigationController: UINavigationController, items: [DropdownItem], selectedRow: Int = 0)
    {
        self.navigationController = navigationController
        self.items = items
        self.selectedRow = selectedRow
        
        super.init(frame: CGRect.zero)

        clipsToBounds = true
        setupGestureView()
        setupTableView()
        setupTopSeperatorView()
    }

    fileprivate func setupGestureView()
    {
        let gestureView = UIView()
        gestureView.backgroundColor = UIColor.clear
        addSubview(gestureView)
        gestureView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: gestureView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0)])
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: gestureView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0)])
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: gestureView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0)])
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: gestureView, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: 0)])

        gestureView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideMenu)))
    }

    fileprivate func setupTopSeperatorView() {
        let seperatorView = UIView()
        seperatorView.backgroundColor = tableViewSeperatorColor
        addSubview(seperatorView)
        seperatorView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: seperatorView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0)])
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: seperatorView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0)])
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: seperatorView, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: 0)])
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: seperatorView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0.5)])
    }

    fileprivate func setupTableView()
    {
        tableViewHeight = CGFloat(items.count) * rowHeight
        let navigationBarFrame: CGRect = navigationController.navigationBar.frame
        let maxHeight = navigationController.view.frame.height - navigationBarFrame.height + navigationBarFrame.origin.y - defaultBottonMargin
        if tableViewHeight > maxHeight {
            tableViewHeight = maxHeight
        }

        tableView = UITableView(frame: CGRect.zero, style: .grouped)
        tableView.separatorColor = tableViewSeperatorColor
        tableView.backgroundColor = tableViewBackgroundColor
        tableView?.delegate = self
        tableView?.dataSource = self
        addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: tableView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant:0)])
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: tableView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: tableViewHeight)])
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: tableView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0)])
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: tableView, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: 0)])
    }

    fileprivate func setupNavigationBarCoverView()
    {
        barCoverView = UIView()
        barCoverView.backgroundColor = UIColor.clear
        navigationController.view.addSubview(barCoverView)
        barCoverView.translatesAutoresizingMaskIntoConstraints = false

        let navigationBar = navigationController.navigationBar
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: barCoverView, attribute: .top, relatedBy: .equal, toItem: navigationBar, attribute: .top, multiplier: 1.0, constant: 0)])
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: barCoverView, attribute: .bottom, relatedBy: .equal, toItem: navigationBar, attribute: .bottom, multiplier: 1.0, constant: 0)])
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: barCoverView, attribute: .left, relatedBy: .equal, toItem: navigationBar, attribute: .left, multiplier: 1.0, constant: 0)])
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: barCoverView, attribute: .right, relatedBy: .equal, toItem: navigationBar, attribute: .right, multiplier: 1.0, constant: 0)])
        barCoverView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideMenu)))
    }

    open func showMenu(onNavigaitionView: Bool = false) {
        if isShow {
            hideMenu()
            return
        }

        isShow = true
        setupNavigationBarCoverView()

        var windowRootView: UIView
        if onNavigaitionView {
            windowRootView = navigationController.view
            windowRootView.insertSubview(self, belowSubview: navigationController.navigationBar)
        } else {
            if let rootView = UIApplication.shared.keyWindow {
                windowRootView = rootView
                windowRootView.addSubview(self)
            } else {
                windowRootView = navigationController.view
                windowRootView.insertSubview(self, belowSubview: navigationController.navigationBar)
            }
        }
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: self, attribute: .top, relatedBy: .equal, toItem: navigationController.navigationBar, attribute: .bottom, multiplier: 1.0, constant: 0)])
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: self, attribute: .bottom, relatedBy: .equal, toItem: windowRootView, attribute: .bottom, multiplier: 1.0, constant: 0)])
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: self, attribute: .left, relatedBy: .equal, toItem: windowRootView, attribute: .left, multiplier: 1.0, constant: 0)])
        NSLayoutConstraint.activate([NSLayoutConstraint.init(item: self, attribute: .right, relatedBy: .equal, toItem: windowRootView, attribute: .right, multiplier: 1.0, constant: 0)])

        backgroundColor = backgroudBeginColor
        self.tableView.frame.origin.y = -self.tableViewHeight
        UIView.animate(withDuration: animateDuration, delay: 0, options: UIViewAnimationOptions(rawValue: 7<<16), animations: {
            self.backgroundColor = self.backgroudEndColor
            self.tableView.frame.origin.y = 0
            }, completion: nil)
    }

    open func hideMenu(isSelectAction: Bool = false) {
        UIView.animate(withDuration: animateDuration, animations: {
            self.backgroundColor = self.backgroudBeginColor
            self.tableView.frame.origin.y = -self.tableViewHeight
        }, completion: { (finished) in
            if !isSelectAction {
                self.delegate?.dropdownMenuCancel(self)
            }
            self.barCoverView.removeFromSuperview()
            self.removeFromSuperview()
            self.isShow = false
        }) 
    }
}

extension DropdownMenu: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let customCell = delegate?.dropdownMenu(self, cellForRowAtIndexPath: indexPath) {
            return customCell
        }
        let item = items[indexPath.row]
        let cell = UITableViewCell(style: .default, reuseIdentifier: "dropdownMenuCell")

        switch item.style {
        case .default:
            cell.textLabel?.textColor = textColor
            if let image = item.image {
                cell.imageView?.image = image
            }
        case .highlight:
            cell.textLabel?.textColor = highlightColor
            if let image = item.image {
                let highlightImage = image.withRenderingMode(.alwaysTemplate)
                cell.imageView?.image = highlightImage
                cell.imageView?.tintColor = highlightColor
            }
        }

        cell.textLabel?.text = item.title
        cell.tintColor = highlightColor
       
        if displaySelected && indexPath.row == selectedRow
        {
            cell.accessoryType = .checkmark
        }
        else
        {
            cell.accessoryType = .none
        }

        if let accesoryImage = item.accessoryImage {
            
            cell.accessoryView = UIImageView(image: accesoryImage)
        }

        return cell
    }
}

extension DropdownMenu: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if displaySelected {
            let item = items[indexPath.row]
            if item.accessoryImage  == nil {
                let previousSelectedcell = tableView.cellForRow(at: IndexPath(row: selectedRow, section: 0))
                previousSelectedcell?.accessoryType = .none
                selectedRow = indexPath.row
                let cell = tableView.cellForRow(at: indexPath)
                cell?.accessoryType = .checkmark
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
        hideMenu(isSelectAction: true)
        delegate?.dropdownMenu(self, didSelectRowAtIndexPath: indexPath)
    }
}
