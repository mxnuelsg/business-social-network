//
//  TextfieldBlockedMenu.swift
//  SHM
//
//  Created by Manuel Salinas on 10/26/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class TextfieldBlockedMenu: UITextField
{
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool
    {
        return false
    }

}
