//
//  ReplaceAndFlipSegue.swift
//  PDI
//
//  Created by Manuel Salinas Gonzalez on 4/6/15.
//  Copyright (c) 2015 PDI. All rights reserved.
//

import UIKit

class ReplaceAndFlipSegue: UIStoryboardSegue
{
    override func perform()
    {
        let vcSource = source //as! UIViewController
        let vcDestination = destination //as! UIViewController
        let navigationController = vcSource.navigationController!
      
        UIView.beginAnimations("LeftFlip", context: nil)
        UIView.setAnimationDuration(1)
        UIView.setAnimationCurve(.easeInOut)
        UIView.setAnimationTransition(.flipFromLeft, for: vcSource.view.superview!, cache: true)
        
        UIView.commitAnimations()
        
        var controllerStack = navigationController.viewControllers
        controllerStack.removeLast()
        controllerStack.append(vcDestination)
        navigationController.setViewControllers(controllerStack, animated: true)
    }
}
