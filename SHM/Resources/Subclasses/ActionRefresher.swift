//
//  ActionRefresher.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 9/17/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class ActionRefresher: NSObject
{
    var onRefresh: () -> ()
    var refreshControl: UIRefreshControl?
    
    init(onRefresh: @escaping () -> ()) {
        
        self.onRefresh = onRefresh
    }
    
    func refresh()
    {
        self.onRefresh()
    }
}
