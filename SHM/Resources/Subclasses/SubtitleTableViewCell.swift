//
//  SubtitleTableViewCell.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/20/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class SubtitleTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
