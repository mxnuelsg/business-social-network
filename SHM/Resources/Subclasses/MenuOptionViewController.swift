//
//  MenuOptionViewController.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/13/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class MenuOptionViewController: UIViewController
{
    var inboxBadge: SwiftBadge?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let user = LibraryAPI.shared.currentUser {
            if user.isLoggedIn == true && user.role == UserRole.vipUser {
                if inboxBadge == nil {
                    inboxBadge = SwiftBadge(frame: CGRect(x: 45, y: 3, width: 20, height: 20))
                    navigationController?.navigationBar.addSubview(inboxBadge!)
                }
                
                inboxBadge!.isHidden = (LibraryAPI.shared.currentUser?.unReadInbox ?? 0) > 0 ? false : true
                inboxBadge!.frame = CGRect(x: 45, y: 3, width: 20, height: 20)
                
                LibraryAPI.shared.inboxBO.getCountForInbox({
                    (count) -> () in
                    
                    LibraryAPI.shared.currentUser?.unReadInbox = count
                    OperationQueue.main.addOperation({ () -> Void in
                        if count > 0 {
                            self.inboxBadge?.isHidden = false
                            self.inboxBadge?.text = "\(count)"
                        }
                        else {
                            self.inboxBadge?.isHidden = true
                            self.inboxBadge?.text = "0"
                        }
                    })
                    
                    }, onError: { error in
                        
                        MessageManager.shared.showBar(title: "Error".localized(),
                            subtitle: "Couldn't get unread inbox".localized(),
                            type: .error,
                            fromBottom: false)
                })
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.inboxBadge?.isHidden = true
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
