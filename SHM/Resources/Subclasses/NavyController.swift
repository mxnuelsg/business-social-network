//
//  NavigationControllerNodes.swift
//  Created by Manuel Salinas on 11/11/14.

import Foundation
import UIKit

class NavyController: UINavigationController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationBar.barTintColor = UIColor.colorForNavigationController()
        self.navigationBar.isTranslucent  = false
        self.navigationBar.tintColor    = UIColor.white
        
        let navbarFont = UIFont(name: "HelveticaNeue-Bold", size: 15) ?? UIFont.systemFont(ofSize: 15)
        self.navigationBar.titleTextAttributes = [NSFontAttributeName: navbarFont, NSForegroundColorAttributeName: UIColor.white]
    }
}

