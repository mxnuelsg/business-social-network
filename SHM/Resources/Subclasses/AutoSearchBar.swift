//
//  AutoSearchBar.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 5/17/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

class AutoSearchBar: UISearchBar {

    fileprivate final var numberOfCharsToStartSearching = 3
    
    var onSearchText: ((String) -> ())?
    var onCancel: (() -> ())?
    var onClearText: (() -> ())?
    //    var onSearchPressed: (() -> ())?
    var onBeginEditing: ((UISearchBar) -> ())?
    var onEndEditing: ((UISearchBar) -> ())?
    var shouldSearchAfterDelay = false
    var shouldSearchOnlyWithSearchButton = false
    
    var isActive: Bool {
        get {
            return isFirstResponder == true
        }
        
        set {
            if (newValue == true) {
                becomeFirstResponder()
            } else {
                dismissKeyboard()
            }
        }
    }
    
//    init() {
//        super.init(frame: CGRect.zero)
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
    
    func initialize(shouldSearchAfterDelay: Bool = false,
                    shouldSearchOnlyWithSearchButton: Bool = false,
                      onSearchText: @escaping (_ text: String) -> (),
                      onBeginEditing: ((UISearchBar) -> ())? = nil,
                      onEndEditing: ((UISearchBar) -> ())? = nil,
                      onClearText: (() -> ())? = nil,
                      //                      onSearchPressed: (() -> ())? = nil,
        onCancel: (() -> ())? = nil) {
        
        self.onSearchText = onSearchText
        self.onCancel = onCancel
        self.onClearText = onClearText
        //        self.onSearchPressed = onSearchPressed
        self.onBeginEditing = onBeginEditing
        self.onEndEditing = onEndEditing
        
        self.shouldSearchAfterDelay = shouldSearchAfterDelay
        self.shouldSearchOnlyWithSearchButton = shouldSearchOnlyWithSearchButton
        self.delegate = self
//        sizeToFit()
        tintColor = UIView().tintColor
        placeholder = "Search".localized()
        setValue("Cancel".localized(), forKey:"_cancelButtonText")
    }
    
    func showInNavigationItem(_ navigationItem: UINavigationItem, animated: Bool) {
        
        becomeFirstResponder()
        placeholder = "Search".localized()
        setValue("Cancel".localized(), forKey:"_cancelButtonText")
        
        navigationItem.setHidesBackButton(true, animated: animated)
        navigationItem.setRightBarButtonItems(nil, animated: animated)
        navigationItem.setLeftBarButtonItems(nil, animated: animated)
        navigationItem.titleView = self
        
        if animated {
            self.alpha = 0
            
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                self.alpha = 1
            })
        }
    }
    
    func cancelSearch()
    {
        resignFocus()
        self.onCancel?()
    }
    
    func dismissKeyboard() {
        resignFocus()
    }
    
    fileprivate func resignFocus() {
        resignFirstResponder()
        setShowsCancelButton(false, animated: true)
    }
    
    fileprivate func performSearchAfterDelay() {
        perform(#selector(notifySearchWithText(text:)), with: self.text!, afterDelay: Constants.TimeInterval.SearchWritingDelay)
    }
    
    @objc fileprivate func notifySearchWithText(text: String) {
        onSearchText?(text)
    }
}

//MARK: UISearchBarDelegate
extension AutoSearchBar: UISearchBarDelegate
{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        searchBar.showsCancelButton = true    
        self.onBeginEditing?(searchBar)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar)
    {
        searchBar.showsCancelButton = false
        self.onEndEditing?(searchBar)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        resignFocus()
        self.onCancel?()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        resignFocus()
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        self.onSearchText?(searchBar.text!)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchBar.text != nil && searchBar.text!.trim().characters.count == 0 && onClearText != nil) {
            onClearText?()
            
            return
        }
        
        if (self.shouldSearchOnlyWithSearchButton == false) {
            if (shouldSearchAfterDelay == true) {
                NSObject.cancelPreviousPerformRequests(withTarget: self)
                
                if (searchBar.text != nil && searchBar.text!.characters.count >= numberOfCharsToStartSearching) {
                    performSearchAfterDelay()
                }
            } else {
                onSearchText?(searchBar.text!)
            }
        }
    }
}
