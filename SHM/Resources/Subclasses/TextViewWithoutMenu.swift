//
//  TextViewWithoutMenu.swift
//  SHM
//
//  Created by Manuel Salinas on 8/19/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class TextViewWithoutMenu: UITextView
{
    var hasPlaceholder = false
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool
    {
        switch action
        {
        case #selector(UIResponderStandardEditActions.cut(_:)):
            return false
        case #selector(UIResponderStandardEditActions.delete(_:)):
            return false
//        case Selector("_transliterateChinese:"):
//            return false
//        case Selector("_showTextStyleOptions:"):
//            return false
//        case Selector("_define:"):
//            return false
//        case Selector("_addShortcut:"):
//            return false
//        case Selector("_accessibilitySpeak:"):
//            return false
//        case Selector("_accessibilitySpeakLanguageSelection:"):
//            return false
//        case Selector("_accessibilityPauseSpeaking:"):
//            return false
//        case Selector("_share:"):
//            return false
        case #selector(UIResponderStandardEditActions.makeTextWritingDirectionRightToLeft(_:)):
            return false
        case #selector(UIResponderStandardEditActions.makeTextWritingDirectionLeftToRight(_:)):
            return false
//        case Selector("_showKeyboard:"):
//            return false
//        case Selector("_promptForReplace:"):
//            return false
        case #selector(UIResponderStandardEditActions.selectAll(_:)):
            return false
        default:
            return true
        }
    }
}

