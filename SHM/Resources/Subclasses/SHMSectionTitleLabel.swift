//
//  SHMSectionTitleLabel.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/26/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

class SHMSectionTitleLabel: UILabel {
    override func drawText(in rect: CGRect)
    {
        let insets = UIEdgeInsets(top: 5, left: 8, bottom: 5, right: 8)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
}


