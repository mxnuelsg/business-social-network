//
//  Servers.swift
//  BCM
//
//  Created by Manuel Salinas on 9/13/16.
//  Copyright © 2016 Nestor Javier Hernandez Bautista. All rights reserved.
//

import UIKit
import CoreLocation

class Servers
{
    enum Server: String {
        case QA = "https://shmqa.definity.solutions/"
        //case QA = "http://10.15.5.62:8048/"
        case UAT = "https://shm.definityfirst.com/"
        case Prod = "https://shmdev.definityfirst.com/"
    }
    
    //MARK: VARIABLES
    static let shared = Servers()
    
    var defaultServer = String()
    var servers = [String]()
    var siteId = String()
    
    //MARK: PRIVATE
    fileprivate init()
    {
        //This prevents others from using the default '()' initializer for this class.
        self.reset()
    }
    
    fileprivate func buildSiteId()
    {
        let copyServer = self.defaultServer
        
        if copyServer.hasSuffix("/")
        {
            self.siteId = String(copyServer[..<copyServer.endIndex])
            //Deprecated - > copyServer.substring(to: copyServer.index(before: copyServer.endIndex))
        }
        else
        {
            self.siteId = copyServer
        }
    }
    
    //MARK: PUBLIC
    func update(_ server: String)
    {
        self.defaultServer = String()
        self.defaultServer = server
        UserDefaults.standard.setValue(self.defaultServer, forKey: Defaults.kDefaultServer.rawValue)
        UserDefaults.standard.synchronize()
        
        self.buildSiteId()
    }
    
    func update(_ list: [String])
    {
        self.servers.removeAll()
        self.servers = list

        UserDefaults.standard.setValue(self.servers, forKey: Defaults.kServers.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    func update(_ server: String, list: [String])
    {
        self.defaultServer = String()
        self.servers.removeAll()
        
        self.defaultServer = server
        self.servers = list

        self.buildSiteId()
    }
    
    func reset()
    {
        self.defaultServer = String()
        self.siteId = String()
        self.servers = [String]()
    }
}


