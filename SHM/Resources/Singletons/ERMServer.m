//
//  ERMServer.m
//  ERM
//
//  Created by Manuel Salinas on 12/9/15.
//  Copyright © 2015 Manuel Salinas Gonzalez. All rights reserved.
//

#import "ERMServer.h"

@implementation ERMServer

static ERMServer *sharedInstance = nil;

-(id)init
{
    @synchronized(self)
    {
        [self reset];
        return [super init];
    }
}

#ifndef __clang_analyzer__
+(ERMServer *)info
{
    @synchronized(self)
    {
        if(sharedInstance == nil)
        {
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}
#endif

+(id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
    {
        if(sharedInstance == nil)
        {
            sharedInstance = [super allocWithZone:zone];
            return sharedInstance;
        }
    }
    return nil;
}

+ (id) copyWithZone:(NSZone *)zone
{
    return self;
}


#pragma mark -
#pragma mark - PUBLIC METHODS
-(void)updateServer:(NSString *)server
{
    self.defaultServer = [NSString string];
    self.defaultServer = server;
    
    [[NSUserDefaults standardUserDefaults] setValue:server
                                             forKey:@"kServerDefault"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self buildSiteId];

}

-(void)updateServerList:(NSMutableArray *)serverList
{
    [self.arrayServers removeAllObjects];
    self.arrayServers = [NSMutableArray arrayWithArray:serverList];
    
    [[NSUserDefaults standardUserDefaults] setValue:serverList
                                             forKey:@"kArrayServers"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)updateServer:(NSString *)server andList:(NSMutableArray *)list
{
    self.defaultServer = [NSString string];
    [self.arrayServers removeAllObjects];
    
    self.defaultServer = server;
    self.arrayServers = [NSMutableArray arrayWithArray:list];
    
    [self buildSiteId];
}

-(void)buildSiteId
{
    NSString *copyServer = self.defaultServer;
    
    if ([copyServer hasSuffix:@"/"])
    {
        self.siteId = [copyServer substringToIndex:[copyServer length] -1];
    }
    else
    {
        self.siteId = copyServer;
    }
}

-(void)reset
{
    self.defaultServer = [NSString string];
    self.siteId = [NSString string];
    self.arrayServers = [NSMutableArray array];
}

@end
