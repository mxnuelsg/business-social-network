//
//  Messages.swift
//  Created by Manuel Salinas on 10/09/15.

import Foundation
import UIKit
import EventKit

private let sharedInstanceManager = CalendarManager()

class CalendarManager : NSObject
{
    let eventStore = EKEventStore()
    
    class var sharedInstance: CalendarManager
    {
        return sharedInstanceManager
    }

    /**
    Create an Event in Device Calendar.  If It's your 1st time, This function will show the dialog permission to calendar access.
    
    :params: String for title event and a Date object to interate in its properties
    */
    func createEvent(_ title: String, event: Event)
    {
        if EKEventStore.authorizationStatus(for: .event) != EKAuthorizationStatus.authorized
        {
            //We need to ask about permission
            eventStore.requestAccess(to: .event, completion:{
                permissionAllowed, error in
                
                guard permissionAllowed == true else
                {
                    OperationQueue.main.addOperation({
                        MessageManager.shared.showBar(title: "Info".localized(),
                            subtitle: "Calendar permission disabled. Go to device settings to enable".localized(),
                            type: .info,
                            fromBottom: false)
                    })
                    
                    return
                }
                
                //Create Event 
                let ekEvent = EKEvent(eventStore: self.eventStore)
                ekEvent.title = title
                ekEvent.calendar = self.eventStore.defaultCalendarForNewEvents
                ekEvent.isAllDay = event.allDay
                
                let startDate: Date! = event.date
                let endDate: Date! = startDate.addingTimeInterval(60 * 60) //1 Hour
                
                ekEvent.startDate = startDate
                ekEvent.endDate = endDate
                
                do {
                    try self.eventStore.save(ekEvent, span: EKSpan.thisEvent)
                    OperationQueue.main.addOperation({
                        
                        MessageManager.shared.showBar(title: "Success".localized(),
                            subtitle: "Event has been added to calendar".localized(),
                            type: .success,
                            fromBottom: false)
                    })
                }
                catch
                {
                    OperationQueue.main.addOperation({
                        
                        MessageManager.shared.showBar(title: "Error".localized(),
                            subtitle: "Operation hasn't been completed".localized(),
                            type: .error,
                            fromBottom: false)
                    })
                }
            })
        }
        else
        {
            //I already had the permission
            //Create Event
            let ekEvent = EKEvent(eventStore: self.eventStore)
            ekEvent.title = title
            ekEvent.calendar = self.eventStore.defaultCalendarForNewEvents
            ekEvent.isAllDay = event.allDay
            
            let startDate: Date! = event.date
            let endDate: Date! = startDate.addingTimeInterval(60 * 60) //1 Hour
            
            ekEvent.startDate = startDate
            ekEvent.endDate = endDate
            
            do {
                try self.eventStore.save(ekEvent, span: EKSpan.thisEvent)
                OperationQueue.main.addOperation({
                    
                    MessageManager.shared.showBar(title: "Success".localized(),
                        subtitle:"Event has been added to calendar".localized(),
                        type: .success,
                        fromBottom: false)
                })
            }
            catch
            {
                OperationQueue.main.addOperation({
                    
                    MessageManager.shared.showBar(title: "Error".localized(),
                        subtitle: "Operation hasn't been completed".localized(),
                        type: .error,
                        fromBottom: false)
                })
            }
        }
    }
    
    /**
    Create an Event in Device Calendar.  If It's your 1st time, This function will show the dialog permission to calendar access.
    
    :params: String for title event and Date to add as an event AllDay
    */
    func createEventWithString(_ title: String, date: Date)
    {
        if EKEventStore.authorizationStatus(for: .event) != EKAuthorizationStatus.authorized
        {
            //We need to ask about permission
            eventStore.requestAccess(to: .event, completion:{
                permissionAllowed, error in
                
                guard permissionAllowed == true else
                {
                    OperationQueue.main.addOperation({
                        
                        MessageManager.shared.showBar(title: "Info".localized(),
                            subtitle: "Calendar permission disabled. Go to device settings to enable".localized(),
                            type: .info,
                            fromBottom: false)
                    })
                    
                    return
                }
                
                //Create Event
                let event = EKEvent(eventStore: self.eventStore)
                event.title = title
                event.calendar = self.eventStore.defaultCalendarForNewEvents
                event.isAllDay = true
                
                let startDate: Date! = date
                let endDate: Date! = startDate.addingTimeInterval(60 * 60) //1 Hour
                
                event.startDate = startDate
                event.endDate = endDate
                
                do {
                    try self.eventStore.save(event, span: EKSpan.thisEvent)
                    OperationQueue.main.addOperation({
                        
                        MessageManager.shared.showBar(title: "Success".localized(),
                            subtitle: "Event has been added to calendar".localized(),
                            type: .success,
                            fromBottom: false)
                    })
                }
                catch
                {
                    OperationQueue.main.addOperation({
                        
                        MessageManager.shared.showBar(title: "Error".localized(),
                            subtitle: "Operation hasn't been completed".localized(),
                            type: .error,
                            fromBottom: false)
                    })
                }
            })
        }
        else
        {
            //I already had the permission
            //Create Event
            let event = EKEvent(eventStore: self.eventStore)
            event.title = title
            event.calendar = self.eventStore.defaultCalendarForNewEvents
            event.isAllDay = true
            
            let startDate: Date! = date
            let endDate: Date! = startDate.addingTimeInterval(60 * 60) //1 Hour
            
            event.startDate = startDate
            event.endDate = endDate
            
            do {
                try self.eventStore.save(event, span: EKSpan.thisEvent)
                OperationQueue.main.addOperation({
                    
                    MessageManager.shared.showBar(title: "Success".localized(),
                        subtitle: "Event has been added to calendar".localized(),
                        type: .success,
                        fromBottom: false)
                })
            }
            catch
            {
                OperationQueue.main.addOperation({
                    
                    MessageManager.shared.showBar(title: "Error".localized(),
                        subtitle: "Operation hasn't been completed".localized(),
                        type: .error,
                        fromBottom: false)
                })
            }
        }
    }

}

