//
//  CacheManager.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalezon 4/12/17.
//  Copyright © 2017 Definity First. All rights reserved.
//

import UIKit

private let sharedInstance = CacheManager()

class CacheManager: NSObject
{
    var imageCache = NSCache<AnyObject, UIImage>()
    
    class var shared: CacheManager
    {
        return sharedInstance
    }
    
    func getCachedImage(_ key: String) -> UIImage?
    {
        if let storedImage = self.imageCache.object(forKey: key as AnyObject) {
            
            return storedImage
        }
        
        return nil
    }

}
