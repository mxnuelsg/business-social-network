//
//  UserDefaultManager.swift
//  BCM
//
//  Created by Manuel Salinas on 9/26/16.
//

import Foundation
import UIKit

enum Defaults: String
{
    //Order to Show
    case kStakeholderOrder
    case kProjectOrder

    //Servers
    case kServers
    case kDefaultServer
    case kEnviromentMode
    
    //Token
    case kCemexToken
}

class UserDefaultsManager: NSObject
{
    //MARK: STAKEHOLDER ORDER
    class func setContactOrder(_ order: Bool)
    {
        UserDefaults.standard.set(order, forKey: Defaults.kStakeholderOrder.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    class func getContactOrder() -> Bool
    {
        return UserDefaults.standard.bool(forKey: Defaults.kStakeholderOrder.rawValue)
    }
    
    //MARK: PROJECT ORDER
    class func setProjectOrder(_ order: Bool)
    {
        UserDefaults.standard.set(order, forKey: Defaults.kProjectOrder.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    class func getProjectOrder() -> Bool
    {
        return UserDefaults.standard.bool(forKey: Defaults.kProjectOrder.rawValue)
    }
    
    //MARK: LANGUAGE
    //If user language is note recorded yet (App first run)
    //this function will set the default user languages parameters
    class func checkLanguageStatus()
    {
        if (UserDefaults.standard.value(forKey: Constants.UserKey.BackupLanguage) as? String) == nil
        {
            print("No Language recorded yet")
            
            //Set default language parameters
            
            //Language will be the same as the phone's native language
            let phoneLanguage = Locale.preferredLanguages[0].components(separatedBy: "-")[0]
            UserDefaults.standard.set(phoneLanguage, forKey: Constants.UserKey.BackupLanguage)
            //Languages versions will be set to 0
            UserDefaults.standard.set(0, forKey: Constants.UserKey.TranslationsVersionEs)
            UserDefaults.standard.set(0, forKey: Constants.UserKey.TranslationsVersionEn)
            
            //Commit user defaults
            UserDefaults.standard.synchronize()
        }
    }


    //MARK: SERVER LIST (DEVELOPMENT)
    class func createServerList()
    {
        if UserDefaults.standard.object(forKey: Defaults.kServers.rawValue) == nil
        {
            let defaultServers = [
                "https://shm.cemex.com/",
                "https://shmqa.definity.solutions/",
                "https://shm.definityfirst.com/",
                "https://shmdev.definityfirst.com/",
            ]
            
            UserDefaults.standard.set(defaultServers, forKey: Defaults.kServers.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
    
    class func getServerList() -> [String]
    {
        return UserDefaults.standard.object(forKey: Defaults.kServers.rawValue) as! [String]
    }
    
    //MARK: CEMEX TOKENS (JWT)
    class func saveJWT(_ cemexToken: String)
    {
        UserDefaults.standard.set(cemexToken, forKey: Defaults.kCemexToken.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    class func getJWT() -> String?
    {
        return UserDefaults.standard.value(forKey: Defaults.kCemexToken.rawValue) as? String
    }
    
    //MARK: DEFAULT SERVER
    class func createDefaultServer()
    {
        if UserDefaults.standard.object(forKey: Defaults.kDefaultServer.rawValue) == nil
        {
            let serverDefault = "https://shm.cemex.com/"
            
            UserDefaults.standard.setValue(serverDefault, forKey: Defaults.kDefaultServer.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
    
    class func getDefaultServer() -> String
    {
        return UserDefaults.standard.value(forKey: Defaults.kDefaultServer.rawValue) as! String
    }
    
    //MARK: ENVIROMENT MODE
    class func saveModeEnviroment(_ mode: Int)
    {
        UserDefaults.standard.set(mode, forKey: Defaults.kEnviromentMode.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    class func getModeEnviroment() -> Int
    {
        return UserDefaults.standard.integer(forKey: Defaults.kEnviromentMode.rawValue)
    }
        
}
