//
//  ERMServer.h
//  ERM
//
//  Created by Manuel Salinas on 12/9/15.
//  Copyright © 2015 Manuel Salinas Gonzalez. All rights reserved.
//  ************** Only for Development Usage ******************

#import <Foundation/Foundation.h>

@interface ERMServer : NSObject

@property (copy, nonatomic) NSString *defaultServer;
@property (copy, nonatomic) NSString *siteId;
@property (strong, nonatomic) NSMutableArray *arrayServers;

+(ERMServer *)info;

-(void)updateServer:(NSString *)server;
-(void)updateServerList:(NSMutableArray *)serverList;
-(void)updateServer:(NSString *)server andList:(NSMutableArray *)list;
-(void)reset;

@end
