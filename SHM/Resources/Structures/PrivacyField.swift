//
//  PrivacyField.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 9/14/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

struct PrivacyField {
    var publicField: String?
    var privateField: String?
    
    var count: Int {
        return (publicField == nil || publicField!.isEmpty ? 0 : 1) + (privateField == nil || privateField!.isEmpty ? 0 : 1)
    }
    
    subscript(index: Int) -> NSMutableAttributedString {
        if count == 1
        {
            return publicField != nil ? getPublicFieldAttributed() : getPrivateFieldAttributed()
        }
        else if count == 2 && index == 0 || index == 1
        {
            return index == 0 ? getPublicFieldAttributed() : getPrivateFieldAttributed()
        }
        else
        {
            return NSMutableAttributedString(string: "-")
        }
    }
    
    fileprivate func getPublicFieldAttributed() -> NSMutableAttributedString {
        return publicField!.htmlMutableAttributedString
    }
    
    fileprivate func getPrivateFieldAttributed() -> NSMutableAttributedString {
        let attrStr = privateField!.htmlMutableAttributedString
        
        attrStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.red, range: NSMakeRange(0, attrStr.string.characters.count))
        
        return attrStr
    }
}
