//
//  WordManager.swift
//  SHM
//
//  Created by Manuel Salinas on 8/14/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

enum SpecialWord : String
{
    case None = ""
    case Mention = "@"
    case Hashtag = "#"
    case CalendarDate = "%"
    case Geography = "!"
}

//typealias Worder = (word: String, range: Range<String.Index>)?

class SpottedWord {
    var word: String
    var range: Range<String.Index>
    var specialWord: SpecialWord?
    var mentionCharRange: Range<String.Index>
    
    init(word: String, range: Range<String.Index>, charRange: Range<String.Index>) {
        self.word = word
        self.range = range
        self.mentionCharRange = charRange
    }
}

extension String
{
    func spotAfterString(_ charStr: String) -> SpottedWord?
    {
        let charRange = self.range(of: charStr, options:NSString.CompareOptions.backwards)
        
        guard let existingCharRange = charRange else {
            return nil
        }
        
        let searchWordRange = self.index(existingCharRange.lowerBound, offsetBy: 1) ..< self.endIndex
        let searchWord = self.substring(with: searchWordRange)
        
        //This word range contains the magic char (#,@, %,!) which is neccesary for textview behaviour
        let newSearchWordRange = self.index(existingCharRange.lowerBound, offsetBy: 0) ..< self.endIndex
        
        return SpottedWord(word: searchWord, range: newSearchWordRange, charRange: existingCharRange)
    }
    
}

struct WordManager
{
    var text: String {
        didSet {
            didChangeWord()
        }
    }
    
    var shouldFilterWith:(_ text: String, _ kindOfWord: SpecialWord, _ charRange: Range<String.Index>, _ searchWordRange:Range<String.Index>) -> ()
    
    func didChangeWord()
    {
        var worders = [SpottedWord]()
        
        if let worder = text.spotAfterString("@") {
            worder.specialWord = SpecialWord.Mention
            worders.append(worder)
//            print("The user wrote an @")
        }
        
        if let worder = text.spotAfterString("#") {
            worder.specialWord = SpecialWord.Hashtag
            worders.append(worder)
//            print("The user wrote an #")
        }
        
        if let worder = text.spotAfterString("%") {
            worder.specialWord = SpecialWord.CalendarDate
            worders.append(worder)
//            print("The user wrote an %")
        }
        
        if let worder = text.spotAfterString("!") {
            worder.specialWord = SpecialWord.Geography
            worders.append(worder)
            //            print("The user wrote an %")
        }
        
        guard worders.count > 0 else { return }
        
        let currentWorder = worders.sorted(by: { $0.range.lowerBound < $1.range.lowerBound }).last
        
        shouldFilterWith(currentWorder!.word, currentWorder!.specialWord!,(currentWorder!.mentionCharRange), (currentWorder?.range)!)
    }
}

