//
//  SHMDataSource.swift
//  SHM
//
//  Created by Manuel Salinas Gonzalez on 8/26/15.
//  Copyright © 2015 Definity First. All rights reserved.
//

import UIKit

struct SHMSectionDataSource {
    var canCollapse: Bool = false
    
    fileprivate(set) internal var isCollapsed: Bool = false
    
    var sectionTitle: String
    var rowsInSection: (() -> Int)?
    var cellForRow: (_ row: Int) -> UITableViewCell
    var setupSectionHeader: ((_ view: UIView) -> ())?
    
    // HERE WE CAN ADD MORE CLOSURES FOR MORE DELEGATE BEHAVIORS
    init(sectionTitle: String, rowsInSection: (() -> Int)?, cellForRow: @escaping (_ row: Int) -> UITableViewCell) {
        self.sectionTitle = sectionTitle
        self.rowsInSection = rowsInSection
        self.cellForRow = cellForRow
    }
    
    // HERE WE CAN ADD MORE INITIALIZERS FOR MORE CUSTOM WRAPPING (one line declaration of section)
    
    // PRIVATE FUNCS
    
    mutating func collapse() {
        isCollapsed = true
    }
    
    mutating func expand() {
        isCollapsed = false
    }
}
