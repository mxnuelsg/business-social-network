//
//  CustomLocale.swift
//  SHM
//
//  Created by Infrastructure Support on 6/15/16.
//  Copyright © 2016 Definity First. All rights reserved.
//

class CustomLocalize
{
    //MARK: PROPERTIES
    let localizationBundleName = "LocalizableBundle.bundle"
    var localizationBundle: Bundle!
    var exitApp: ((_ exitApp: Bool) -> ())?
    // The index 0 is for the actual language on the device
    var locale:String {
        get {
            return UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? "en"
        }
    }
    
    // MARK: LOAD TRANSLATIONS
    func loadTranslations()
    {
        let version = UserDefaults.standard.integer(forKey: self.locale == "es" ? Constants.UserKey.TranslationsVersionEs : Constants.UserKey.TranslationsVersionEn)
        
        LibraryAPI.shared.attachmentBO.getTranslationsiOS(version,onSuccess: { response in
            
            let translation = response
            
            if translation.shouldRefresh == true && translation.file != nil
            {
                self.saveFilesOnDevice(self.locale, file: translation.file)
                self.refreshTranslations(translation.version, shouldExitApp: translation.shouldRefresh)
            }
            else
            {
                self.exitApp?(false)
            }
            
            }, onError: { error in
                
                print(error.localizedDescription)
                
                MessageManager.shared.showStatusBar(title: "Unable to check translations updates", type: .error, containsIcon: false)
                self.exitApp?(false)
        })
    }
    
    //MARK: REFRESH TRANSLATIONS
    fileprivate func refreshTranslations(_ version: Int, shouldExitApp: Bool)
    {
        UserDefaults.standard.set(version, forKey: (self.locale == "es" ? Constants.UserKey.TranslationsVersionEs : Constants.UserKey.TranslationsVersionEn))
        UserDefaults.standard.setValue(self.locale, forKey: Constants.UserKey.BackupLanguage)
        UserDefaults.standard.synchronize()
        
        self.exitApp?(shouldExitApp)
    }
    
    //MARK: GET FILES FROM WEBSERVER
    func getJson(_ urlToRequest: String)-> Data? {
        var data: Data?
        
        if let url = URL(string: urlToRequest) {
            data = try? Data(contentsOf: url)
        }
        else
        {
            data = Data()
        }
        
        return data
    }
    
    //MARK: SAVE FILES TO DISK
    fileprivate func saveFilesOnDevice(_ locale: String, file: Data? )
    {
        if let data = file {
            if locale == "es"
            {
                self.saveData(data, folder: NSString.localizedStringWithFormat("%@/%@", self.localizationBundleName, "es.lproj"), filename: "Localizable.strings")
            }
            else
            {
                self.saveData(data, folder: NSString.localizedStringWithFormat("%@/%@", self.localizationBundleName, "en.lproj"), filename: "Localizable.strings")
            }
        }
    }
    
    @discardableResult fileprivate func saveData(_ data: Data, folder: NSString, filename: NSString)-> Bool
    {
        let dataToSave = data
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let datapath = documentsDirectory?.appendingPathComponent(folder as String)
        
        if FileManager.default.fileExists(atPath: (datapath?.path)!) == false
        {
            do {
                try FileManager.default.createDirectory(atPath: (datapath?.path)!, withIntermediateDirectories: true, attributes: nil)
            }
            catch let error as NSError {
                print(error)
            }
        }
        
        let filePath = NSString.localizedStringWithFormat("%@/%@", datapath! as CVarArg, filename)
        
        if ((try? dataToSave.write(to: URL(fileURLWithPath: filePath.replacingOccurrences(of: "file://", with: "")) , options: [.atomic])) != nil) == true
        {
            let backURl = URL(fileURLWithPath: filePath.replacingOccurrences(of: "file://", with: ""))
            self.addSkipBackupAttributeToItemAtUrl(backURl)
            
        }
        else
        {
            // Error when saving the file
            print("Error saving the localization file")
            
            MessageManager.shared.showBar(title: "Error",
                                                            subtitle: "Error.TryAgain".localized(),
                                                            type: .error,
                                                            fromBottom: false)
        }
        
        return true
    }
    
    @discardableResult fileprivate func addSkipBackupAttributeToItemAtUrl(_ URL: Foundation.URL)-> Bool
    {
        assert(FileManager.default.fileExists(atPath: URL.path))
        
        do {
            try (URL as NSURL).setResourceValue(NSNumber(value: true as Bool), forKey: URLResourceKey.isExcludedFromBackupKey)
            return true
        }
        catch let error as NSError {
            print(error)
        }
        
        return false
    }
    
    func getLanguageVersionNumber() -> Int
    {
        //Update current language placeholder: locale
        //let phoneLanguage = Locale.preferredLanguages[0].components(separatedBy: "-")[0]
        //self.locale = UserDefaults.standard.string(forKey: Constants.UserKey.BackupLanguage) ?? phoneLanguage
        
        switch self.locale
        {
        case "es":
            
            return UserDefaults.standard.integer(forKey:  Constants.UserKey.TranslationsVersionEs)
        case "en":
            
            return UserDefaults.standard.integer(forKey: Constants.UserKey.TranslationsVersionEn)
        default:
            
            //English will be default
            return 0
        }
    }
    
    //MARK: CREATE NEW BUNDLE
    fileprivate func createNewBundle()
    {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let dataPath = documentsDirectory?.appendingPathComponent(self.localizationBundleName)
        let pathForResource = Bundle(path: (dataPath?.path)!)?.path(forResource: self.locale, ofType: "lproj")
        
        if let path = pathForResource {
            self.localizationBundle = Bundle(path: path)
        }
        else
        {
            self.localizationBundle = Bundle.main
        }
    }
    
    //MARK: GET BUNDLE
    fileprivate func getBundle()-> Bundle
    {
        if self.localizationBundle == nil
        {
            self.createNewBundle()
        }
        
        return self.localizationBundle!
    }
    
    //MARK: localized String For Key
    func localizedStringForKey(_ key: String)-> String
    {
        return self.getBundle().localizedString(forKey: key, value: nil, table: nil)
    }
}
